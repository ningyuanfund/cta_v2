import ccxt

from lspr.TradeEnv.func import *


class Trader:
    def __init__(self, api_key, secret, con):
        if con:
            self.exchange = ccxt.bitfinex({
                            'apiKey': api_key,
                            'secret': secret
            })
            self.exchange.load_markets()
            self.exchange2 = ccxt.bitfinex2({
                            'apiKey': api_key,
                            'secret': secret
            })
            self.exchange2.load_markets()

    # 下单
    def place_order(self, order_type, buy_or_sell, symbol, price, amount):
        """
        下单
        :param order_type: limit, market
        :param buy_or_sell: buy, sell
        :param symbol: 买卖品种 BTCUSD
        :param price: 当market订单的时候，price无效
        :param amount: 买卖量
        :return:
        """
        print('本次下单信息为：', order_type, buy_or_sell, symbol, price, amount)
        order_symbol = symbol

        print(order_symbol)
        for i in range(5):
            # try:
            # 限价单
            if order_type == 'limit':
                # 买
                if buy_or_sell == 'buy':
                    order_info = self.exchange.create_limit_buy_order(symbol, amount, price, {'type': 'limit'})  # 买单
                # 卖
                elif buy_or_sell == 'sell':
                    order_info = self.exchange.create_limit_sell_order(symbol, amount, price, {'type': 'limit'})  # 卖单
            # 市价单
            elif order_type == 'market':
                # 买
                if buy_or_sell == 'buy':
                    order_info = self.exchange.create_market_buy_order(symbol, amount, {'type': 'market'})  # 买单
                # 卖
                elif buy_or_sell == 'sell':
                    order_info = self.exchange.create_market_sell_order(symbol, amount, {'type': 'market'})  # 卖单
            else:
                order_info = {}

            print('下单成功：', order_type, buy_or_sell, symbol, price, amount)
            print('下单信息：', order_info, '\n')
            lets_sleep()
            return order_info

            # except Exception as e:
            #     print('下单报错，3s后重试', e)
            #     time.sleep(3)

        print('下单报错次数过多，下单线程终止')
        content = '执行下单报错，已终止线程。下单信息为：%s, %s, %s, %s, %s' % (symbol, buy_or_sell, order_type, price, amount)
        send_dingding_msg(content=content)
        exit()

    # 下OCO的单
    def place_oco_order(self, symbol, open_amount, stop_profit_price, stop_loss_price, side):
        """
        :param symbol:ETHUSD -> trade_symbol: ETHUSD
        :param open_amount:
        :param stop_profit_price:
        :param stop_loss_price:
        :param side: 如果是开多头仓位，则止盈与止损都是sell，所以side是sell；如果是开空头仓位，则止盈与止损都是buy，所以side是buy
        :return:
        """

        trade_symbol = symbol
        # 止盈止损OCO
        for i in range(5):
            try:
                order_info = self.exchange.private_post_order_new(params={'symbol': trade_symbol,
                                                                          'amount': str(open_amount),
                                                                          'price': stop_profit_price,
                                                                          'side': side,
                                                                          'type': 'limit',
                                                                          'ocoorder': True,
                                                                          'buy_price_oco': stop_loss_price})
                print('止盈止损成功：', )
                print('下单信息：', order_info, '\n')
                lets_sleep()
                return order_info
            except Exception as e:
                print('下单报错，1s后重试', e)
                time.sleep(5)

    # 撤掉本symbol现有的所有单
    def cancel_symbol_order(self, symbol):
        """
        :param symbol: ETHUSD -> status symbol: ethusd
        :return:
        """
        status_symbol = symbol.lower()  # ethusd
        print('撤掉%s所有的单' % symbol)

        info = self.exchange.private_post_orders()

        order_id_list = []
        for order in info:
            if order['symbol'] == status_symbol:
                order_id_list.append(order['id'])
        print('需要撤', order_id_list)
        if len(order_id_list) > 0:
            cancel_info = self.exchange.private_post_order_cancel_multi(params={'order_ids': order_id_list})
            print(cancel_info)
            lets_sleep()

    # 根据order id撤单
    def cancel_order(self, order_id):
        cancel_info = self.exchange.cancel_order(id=str(order_id))
        return cancel_info
