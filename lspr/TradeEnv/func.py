import json
import time
from datetime import datetime

import requests


# 发送钉钉消息，id填上使用的机器人的id
def send_dingding_msg(content, robot_id='e7ae7cc48dd987152cf748e1cc2973a722a01577653faad975e90b357bd6b084'):
    try:
        msg = {
            "msgtype": "text",
            "text": {"content": content + '\n' + datetime.now().strftime("%m-%d %H:%M:%S")}}
        headers = {"Content-Type": "application/json;charset=utf-8"}
        url = 'https://oapi.dingtalk.com/robot/send?access_token=' + robot_id
        body = json.dumps(msg)
        requests.post(url, data=body, headers=headers)
        print('成功发送钉钉')
    except Exception as e:
        print("发送钉钉失败:", e)


# 睡眠大师
def lets_sleep():
    time.sleep(5)
