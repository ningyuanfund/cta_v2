import pandas as pd

from lspr.TradeEnv.Fetcher import Fetcher
from lspr.TradeEnv.Trader import Trader
from lspr.main.util import ACCOUNT_ROOT


class TradeEnv:
    def __init__(self, api_key, secret, account_num, con):
        self._trader = Trader(api_key=api_key, secret=secret, con=con)
        self._fetcher = Fetcher(api_key=api_key, secret=secret, con=con)
        self.account = self._init_account(account_num)

    def _init_account(self, account_num):
        """
        初始化账户(读取本地信息)
        :param account_num: int
        :return:
        """
        account = []
        account_df = pd.read_csv(ACCOUNT_ROOT)
        empty = True
        for i in range(account_num):
            account.append({'check_time': account_df['check_time'].iloc[i],
                            'opened': account_df['opened'].iloc[i],
                            'open_price': account_df['open_price'].iloc[i],
                            'stop_loss': account_df['stop_loss'].iloc[i],
                            'stop_profit': account_df['stop_profit'].iloc[i],
                            'use_money': account_df['use_money'].iloc[i],
                            'now_hold': account_df['now_hold'].iloc[i],
                            'stop_order_id': account_df['stop_order_id'].iloc[i]})
            empty = empty & (account_df['opened'].iloc[i] == 0)

        if empty:
            account = self.rebalance_all(account_num=account_num, inplace=False)

        return account

    def order(self, order_type, buy_or_sell, symbol, price, amount):
        """
        下单
        :param order_type: limit, market
        :param buy_or_sell: buy, sell
        :param symbol: 买卖品种 BTCUSD
        :param price: 当market订单的时候，price无效
        :param amount: 买卖量
        :return:
        """
        return self._trader.place_order(order_type=order_type,
                                        buy_or_sell=buy_or_sell,
                                        symbol=symbol,
                                        price=price,
                                        amount=amount)

    def oco_order(self, symbol, open_amount, stop_profit_price, stop_loss_price, side):
        """
        下OCO的止盈止损单
        :param symbol:'ETHUSD'
        :param open_amount:
        :param stop_profit_price:
        :param stop_loss_price:
        :param side: 如果是开多头仓位，则止盈与止损都是sell，所以side是sell；如果是开空头仓位，则止盈与止损都是buy，所以side是buy
        :return:
        """
        return self._trader.place_oco_order(symbol=symbol,
                                            open_amount=open_amount,
                                            stop_profit_price=stop_profit_price,
                                            stop_loss_price=stop_loss_price,
                                            side=side)

    def cancel_symbol_order(self, symbol):
        """
        撤销某个币的所有单
        :param symbol: 'ETHUSD'
        :return:
        """
        return self._trader.cancel_symbol_order(symbol=symbol)

    def cancel_order(self, order_id):
        """
        根据id撤单
        :param order_id:
        :return:
        """
        return self._trader.cancel_order(order_id=order_id)

    def order_avg_price(self, order_id):
        """
        根据order id 查询订单平均成交价
        :param order_id: str
        :return:
        """
        return self._fetcher.get_avg_price(order_id=order_id)

    def symbol_opposite_price(self, symbol):
        """
        获取对手价
        :param symbol: 'BTCUSD'
        :return:
        """
        return self._fetcher.get_opposite_price(symbol=symbol)

    def symbol_latest_data(self, symbol, time_interval):
        """
        获取某个symbol最近1000根K线
        :param symbol: 'BTCUSD'
        :param time_interval: '1h'
        :return:
        """
        return self._fetcher.get_latest_data(symbol=symbol,
                                             time_interval=time_interval)

    def order_status(self, order_id):
        """

        :param order_id:
        :return:
        """
        return self._fetcher.get_order_status(order_id=order_id)

    def rebalance_all(self, account_num, inplace):
        """
        rebalance 所有账户
        :param account_num:
        :param inplace:
        :return:
        """
        margin_balance = self._fetcher.get_margin_balance()
        # margin_balance = 1000
        account = []
        for i in range(account_num):
            account.append({'check_time': i, 'opened': 0, 'open_price': 0, 'stop_loss': 0, 'stop_profit': 0,
                            'use_money': float(margin_balance) / account_num, 'now_hold': 0, 'stop_order_id': 0})
        print('=== NOW ACCOUNT ===')
        print(pd.DataFrame(account))
        if inplace:
            self.account = account
        else:
            return account
