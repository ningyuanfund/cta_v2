from lspr.main.MainEnv import MainEnv

if __name__ == '__main__':

    api_key = 'kaSx747CCm23VFvtp31P9i81tOK3STdHsr9ZJLjTbDP'
    secret = 'M7TgT6T4HvrwBjxMh1mTRMvtLWooQ7vdSO0CFjD6xu0'
    symbol = 'BTCUSD'
    main_env = MainEnv(api_key=api_key,
                       secret=secret,
                       para={'account_num': 24, 'strategy_para': []},
                       trade=True)

    while True:
        try:
            target_time = main_env.next_run_time('60T')
            main_env.sleep_til_target_time(target_time=target_time)
            signal = main_env.gen_signal(symbol=symbol, target_time=target_time)
            main_env.change_position(signal=signal, symbol=symbol)
            main_env.account_to_local()
        except Exception as e:
            print(e)
            raise Exception
