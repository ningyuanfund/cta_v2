import time
from datetime import datetime, timedelta

import pandas as pd

from lspr.StrategyEnv.StrategyEnv import StrategyEnv
from lspr.TradeEnv.TradeEnv import TradeEnv
from lspr.main.util import ACCOUNT_ROOT


class MainEnv:
    def __init__(self, api_key, secret, para, trade):
        """
        :param api_key:
        :param secret:
        :param para:{'account_num': 24, 'strategy_para': [1, 2, 3]}
        """
        self.strategy_env = StrategyEnv(para=para)
        self.trade_env = TradeEnv(api_key=api_key, secret=secret, account_num=para['account_num'], con=trade)

    @staticmethod
    def next_run_time(time_interval, ahead_time=1):
        """
        :param time_interval 运行周期时间 建议不要小于5min
        :param ahead_time 当小于1s时 自动进入下个周期
        :return:
        """
        if time_interval.endswith('T'):
            now_time = datetime.now()
            time_interval = int(time_interval.strip('T'))

            target_min = (int(now_time.minute / time_interval) + 1) * time_interval
            if target_min < 60:
                target_time = now_time.replace(minute=target_min, second=0, microsecond=0)
            else:
                if now_time.hour == 23:
                    target_time = now_time.replace(hour=0, minute=0, second=0, microsecond=0)
                    target_time += timedelta(days=1)
                else:
                    target_time = now_time.replace(hour=now_time.hour + 1, minute=0, second=0, microsecond=0)

            # sleep直到靠近目标时间之前
            if (target_time - datetime.now()).seconds < ahead_time + 1:
                print('距离target_time不足', ahead_time, '秒，下下个周期再运行')
                target_time += timedelta(minutes=time_interval)
            print('下次运行时间', target_time)
            return target_time
        else:
            exit('time_interval doesn\'t end with T')

    def gen_signal(self, symbol, target_time):
        signal = []
        max_tries = 6
        for i in range(max_tries):
            data = self.trade_env.symbol_latest_data(symbol=symbol, time_interval='1h')
            _temp = data[data['candle_begin_time'] == target_time]
            if _temp.empty:
                print('NO NEW DATA')
                time.sleep(5)
            else:
                data = data[data['candle_begin_time'] < target_time].copy()
                break
            if i == max_tries - 1:
                raise Exception('NO NEW DATA')
        print('=== GEN SIGNAL ===')
        target_position = self.strategy_env.position(data=data)
        print(target_position)
        for i in range(len(target_position)):
            target_pos = target_position[i]['should_open']
            stop_loss = target_position[i]['stop_loss']
            stop_profit = target_position[i]['stop_profit']
            open_price = target_position[i]['open_price']
            now_pos = self.trade_env.account[i]['opened']

            if (now_pos == 0) & (target_pos == 1):
                signal.append({'check_time': i, 'signal': 'long_open', 'target_pos': target_pos,
                               'stop_loss': stop_loss, 'stop_profit': stop_profit, 'open_price': open_price})
            elif (now_pos == 0) & (target_pos == -1):
                signal.append({'check_time': i, 'signal': 'short_open', 'target_pos': target_pos,
                               'stop_loss': stop_loss, 'stop_profit': stop_profit, 'open_price': open_price})
            elif (now_pos == 1) & (target_pos == 0):
                signal.append({'check_time': i, 'signal': 'long_close', 'target_pos': target_pos,
                               'stop_loss': stop_loss, 'stop_profit': stop_profit, 'open_price': open_price})
            elif (now_pos == -1) & (target_pos == 0):
                signal.append({'check_time': i, 'signal': 'short_close', 'target_pos': target_pos,
                               'stop_loss': stop_loss, 'stop_profit': stop_profit, 'open_price': open_price})
            else:
                pass
        return signal

    # 调仓
    def change_position(self, symbol, signal):
        for sig in signal:
            # 跳仓部分
            oppo_pirce = self.trade_env.symbol_opposite_price(symbol=symbol)

            # 开仓，记录开仓方向，开仓价格，开仓数量，止盈止损价格，oco订单号
            if sig['signal'] == 'long_open':
                amount = round(self.trade_env.account[sig['check_time']]['use_money'] / oppo_pirce, 3) * 3  # 3倍杠杆
                self.trade_env.account[sig['check_time']]['now_hold'] = amount
                self.trade_env.account[sig['check_time']]['open_price'] = oppo_pirce
                self.trade_env.account[sig['check_time']]['opened'] = 1
                side = 'buy'

            elif sig['signal'] == 'short_open':
                amount = round(self.trade_env.account[sig['check_time']]['use_money'] / oppo_pirce, 3) * 3  # 3倍杠杆
                self.trade_env.account[sig['check_time']]['now_hold'] = -amount
                self.trade_env.account[sig['check_time']]['open_price'] = oppo_pirce
                self.trade_env.account[sig['check_time']]['opened'] = -1
                side = 'sell'

            # 平仓，计算平仓损益，将开仓方向，开仓数量，开仓价格，oco订单号重置
            elif sig['signal'] == 'short_close':
                amount = abs(self.trade_env.account[sig['check_time']]['now_hold'])
                self.trade_env.account[sig['check_time']]['use_money'] += \
                    (amount * (self.trade_env.account[sig['check_time']]['open_price'] - oppo_pirce))
                self.trade_env.account[sig['check_time']]['now_hold'] = 0
                self.trade_env.account[sig['check_time']]['opened'] = 0
                self.trade_env.account[sig['check_time']]['open_price'] = 0
                self.trade_env.account[sig['check_time']]['stop_order_id'] = 0
                self.trade_env.account[sig['check_time']]['stop_loss'] = 0
                self.trade_env.account[sig['check_time']]['stop_profit'] = 0
                side = 'buy'

            elif sig['signal'] == 'long_close':
                amount = abs(self.trade_env.account[sig['check_time']]['now_hold'])
                self.trade_env.account[sig['check_time']]['use_money'] += \
                    (amount * (oppo_pirce - self.trade_env.account[sig['check_time']]['open_price']))
                self.trade_env.account[sig['check_time']]['opened'] = 0
                self.trade_env.account[sig['check_time']]['now_hold'] = 0
                self.trade_env.account[sig['check_time']]['open_price'] = 0
                self.trade_env.account[sig['check_time']]['stop_order_id'] = 0
                self.trade_env.account[sig['check_time']]['stop_loss'] = 0
                self.trade_env.account[sig['check_time']]['stop_profit'] = 0
                side = 'sell'
            else:
                side = 'buy'
                amount = 0

            self.trade_env.order(order_type='market',
                                 buy_or_sell=side,
                                 symbol=symbol,
                                 price=oppo_pirce,
                                 amount=amount)

            # 止盈止损部分
            if sig['signal'] == 'long_open':
                oco_info = self.trade_env.oco_order(symbol=symbol,
                                                    open_amount=amount,
                                                    stop_profit_price=sig['stop_profit'],
                                                    stop_loss_price=sig['stop_loss'],
                                                    side='sell')
                self.trade_env.account[sig['check_time']]['stop_order_id'] = oco_info['id']
                self.trade_env.account[sig['check_time']]['stop_loss'] = sig['stop_loss']
                self.trade_env.account[sig['check_time']]['stop_profit'] = sig['stop_profit']

            elif sig['signal'] == 'short_open':
                oco_info = self.trade_env.oco_order(symbol=symbol,
                                                    open_amount=amount,
                                                    stop_profit_price=sig['stop_profit'],
                                                    stop_loss_price=sig['stop_loss'],
                                                    side='buy')
                self.trade_env.account[sig['check_time']]['stop_order_id'] = oco_info['id']
                self.trade_env.account[sig['check_time']]['stop_loss'] = sig['stop_loss']
                self.trade_env.account[sig['check_time']]['stop_profit'] = sig['stop_profit']

            elif sig['signal'] in ['long_open', 'short_open']:
                # 撤单
                self.trade_env.cancel_order(order_id=self.trade_env.account[sig['check_time']]['stop_order_id'])

    @staticmethod
    def sleep_til_target_time(target_time):
        while True:
            if datetime.now() < target_time - timedelta(seconds=300):
                time.sleep(120)
                continue
            else:
                break
        # 直到下次运行的时间再启动
        time.sleep(max(0, (target_time - datetime.now()).seconds))

        # 在靠近目标时间时
        while True:
            if datetime.now() < target_time:
                continue
            else:
                break

    def refresh(self, symbol):
        # 检查有持仓的账户stop_order还在不在，不在的话说明被吃了，同步账户状态
        for account in self.trade_env.account:
            stop_order_info = self.trade_env.order_status(order_id=account['stop_order_id'])
            if account['now_hold'] != 0:
                # 有持仓，未撤单
                if stop_order_info['is_live']:
                    pass
                # 有持仓，已经撤单
                else:
                    print("%s POSITION NAKED!" % account['check_time'])
                    if account['now_hold'] > 0:
                        side = 'sell'
                    else:
                        side = 'buy'
                    # 补下
                    self.trade_env.oco_order(symbol=symbol,
                                             open_amount=account['now_hold'],
                                             stop_loss_price=account['stop_loss'],
                                             stop_profit_price=account['stop_profit'],
                                             side=side)
            else:
                # 无持仓，未撤单
                if stop_order_info['is_live']:
                    self.trade_env.cancel_order(order_id=account['stop_order_id'])
                # 无持仓，已撤单
                else:
                    pass

    # 同步记录
    def account_to_local(self):
        account_info = self.trade_env.account
        account_df = pd.DataFrame(account_info)
        account_df.to_csv(ACCOUNT_ROOT)

