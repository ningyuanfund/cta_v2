import time

import requests
from dateutil.relativedelta import relativedelta
from sklearn.ensemble import RandomForestRegressor

from lspr.main.util import *


def my_resample(dfa,freq='1H'):
    df = dfa.copy()
    df = my_todatetime(df)

    df_week = df.resample(rule=freq,
                          on='candle_begin_time',
                          label='right',
                          closed='left',
                          ).agg({'open': 'first',
                                 'high': 'max',
                                 'low': 'min',
                                 'close': 'last',
                                 'volume': 'sum',
                                 })
    df_week.reset_index( inplace=True)
    df_week.fillna(method='ffill', inplace=True)
    return df_week


def my_todatetime(df,time_col='candle_begin_time'):
    df['candle_begin_time'] = pd.to_datetime(df['candle_begin_time'])
    df_resampled = df.resample(rule='1T',
                               on='candle_begin_time',
                               label='left',
                               closed='left').agg({'open': 'first',
                                                   'high': 'max',
                                                   'low': 'min',
                                                   'close': 'last',
                                                   'volume': 'sum'})
    df_filled = df_resampled.copy()
    df_filled.fillna(method='ffill', inplace=True)
    df_filled['volume'] = df_resampled['volume']  # 交易量不得补齐
    df = df_filled
    df.reset_index(inplace=True)
    df[time_col] = pd.to_datetime(df[time_col])
    return df


# 读取schema中所有表名
def rsql_tablename(schemaname='public',engine=engine_sr):
    sql = "select tablename from pg_tables where schemaname='%s'" % schemaname
    table_df = pd.read_sql(sql, con=engine)
    return table_df


def get_longShortPositionRatio(coin, unit_type):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
        'Accept-Language': 'zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2',
        'accept-encoding': 'gzip, deflate, br',
        'Content-Type': 'application/json',
        'accept': '*/*',
    }
    # 合约持仓量,多空比例
    unitType_dict = {
        '5T': 0,
        '1H': 1,
        '1D': 2,
    }
    url = 'https://www.okex.com/v3/futures/pc/market/longShortPositionRatio/%s?currency=%s&unitType=%s' % (
        coin, coin, unitType_dict[unit_type])
    r = requests.get(url, headers=headers).text
    r_dict = eval(r)
    data_dict = r_dict['data']
    timestamp_list = data_dict['timestamps']  # 时间戳
    ratio_list = data_dict['ratios']
    temp_dict = {
        'candle_begin_time': timestamp_list,
        'ratio': ratio_list,
    }
    df = pd.DataFrame(temp_dict)
    df['candle_begin_time'] = df['candle_begin_time'].apply(
        lambda x: time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(x / 1000)))
    print(df)
    return df


def refresh_longShortPositionRatio(coin, unit_type,engine=engine_emotion):
    table_name = 'longShortPositionRatio_%s_%s' % (coin, unit_type)
    sql = 'select * from public."%s"' % table_name
    df_old = pd.read_sql_query(sql, con=engine)
    df_now = get_longShortPositionRatio(coin, unit_type)
    df = pd.concat([df_old, df_now], keys='candle_begin_time')
    df.drop_duplicates(subset=['candle_begin_time'], inplace=True)
    df.reset_index(inplace=True, drop=True)
    return df


def refreh_form_sr(symbol='BTC',engine_loc=engine_lc,engine_svr=engine_sr):
    sql_cmd = 'select * from cta_test."%sUSD1T"'%symbol
    df_old = pd.read_sql(sql_cmd,con=engine_loc)
    df_old.sort_values(by='candle_begin_time',inplace=True)
    df_old.reset_index(inplace=True,drop=True)
    date_latest = pd.to_datetime(df_old.at[df_old.shape[0]-1,'candle_begin_time'])-relativedelta(days=1)
    df_table_sr = rsql_tablename(schemaname='public',engine=engine_svr)

    # 获取增量更新节点
    df_table_sr['symbol'] = df_table_sr['tablename'].str[9:-16]
    df_table_sr['date'] = df_table_sr['tablename'].str[-10:]
    df_table_sr['date'] = pd.to_datetime(df_table_sr['date'])
    df_table_append = df_table_sr[df_table_sr['date']>date_latest][df_table_sr['symbol']==symbol]
    df_table_append.reset_index(drop=True,inplace=True)

    # 获取增量数据
    def sort_df(temp):
        df_list = []
        for i in range(len(temp)):
            table_name = temp['tablename'].at[i]
            print(table_name)
            sql_cmd = 'select * from "%s"' % table_name
            df = pd.read_sql(sql_cmd, con=engine_sr)
            df_list.append(df)
        df_summed = pd.concat(df_list)
        return df_summed
    df_append = sort_df(df_table_append)

    # 拼接，去重
    df_all = pd.concat([df_old,df_append],axis=0,sort=False)
    df_all.drop_duplicates('candle_begin_time',keep='first',inplace=True)
    df_all.sort_values(by='candle_begin_time',inplace=True)
    return df_all


def refresh_local_data(symbol, engine_loc=engine_lc, engine_svr=engine_sr):
    """
    按以下优先级增量更新数据：本地-->服务器-->bfx api
    :param symbol:
    :param engine_loc:
    :param engine_svr:
    :return:
    """
    print('更新K线数据')
    try:
        df_all = refreh_form_sr()
    except:
        sql_cmd = 'select * from cta_test."%sUSD1T"' % symbol
        df_all= pd.read_sql(sql_cmd, con=engine_loc)
        df_all.sort_values(by='candle_begin_time', inplace=True)
        df_all.reset_index(inplace=True,drop=True)


    # 通过交易所补齐最新K线
    exchange = ccxt.bitfinex({'rateLimit': 10000, 'enableRateLimit': True})
    exchange.load_markets()

    # 获取到目前为止的数据1m
    def get_candle_from_now(df, refresh_symbol, now, from_timestamp):
        '''

        :param exchange:btf
        :param refresh_symbol: ETH/USDT
        :param local_data_path:
        :param now: 当前交易所时间,ms
        :param from_timestamp: 同步开始的时间，即本地记录最后的时间
        :return:
        '''

        print('当前交易所时间为', pd.to_datetime(now, unit='ms'))

        print('从', pd.to_datetime(from_timestamp, unit='ms'), '开始同步')

        while int(from_timestamp) < now:
            try:

                # 从服务器获取k线数据
                ohlcvs = exchange.fetch_ohlcv(refresh_symbol, '1m', from_timestamp)
                ohlcvs_df = pd.DataFrame(ohlcvs)
                ohlcvs_df.columns = ['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']
                ohlcvs_df['candle_begin_time'] = pd.to_datetime(ohlcvs_df['candle_begin_time'], unit='ms')
                # 保存到df
                df = df.append(ohlcvs_df)

                # 去重，防止有重复k线
                df.drop_duplicates(['candle_begin_time'], keep='last', inplace=True)
                df.reset_index(inplace=True,drop=True)
                synchronized_time = time.mktime(time.strptime(str(df.at[len(df) - 1, 'candle_begin_time']+relativedelta(hours=8)), '%Y-%m-%d %H:%M:%S')) * 1000
                print(refresh_symbol + ' 已同步至', pd.to_datetime(synchronized_time, unit='ms'))

                # 下次获取的since时间必须是当前结束的时间
                from_timestamp = synchronized_time

            except Exception as e:
                print(e)
                print('报错，30秒后重连')
                time.sleep(30)
        df['candle_begin_time'] = pd.to_datetime(df['candle_begin_time'], unit='ms')
        df_all.drop_duplicates('candle_begin_time', keep='first', inplace=True)
        df.reset_index(inplace=True, drop=True)
        return df

    now = exchange.milliseconds()
    sychronized_time = time.mktime(time.strptime(str(df_all.iloc[-1]['candle_begin_time']),'%Y-%m-%d %H:%M:%S'))*1000
    from_timestamp = sychronized_time
    df_all = get_candle_from_now(df_all,'%s/USDT' % symbol,now, from_timestamp)
    df_all = df_all[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']]
    df_all.to_sql('%sUSD1T'%symbol,con=engine_lc,schema='cta_test',if_exists='replace')
    return df_all


def gen_data(df_price, symbol='BTC', type='6H', refresh=True, engine_lspr=engine_emotion):
    '''
     生成最新数据，用于训练模型和预测
    :param symbol:
    :param type:
    :param refresh: 为True时，获取最新数据并保存在本地
    :param engine_lspr:lspr存量数据位制
    :param engine_loc:本地数据位制
    :return:
    '''

    if refresh:
        try:
            # 将lspr更新至最新
            df_lspr = refresh_longShortPositionRatio(symbol, '1H')
        except Exception as e :
            print(e)
            df_lspr = pd.read_sql('select * from "longShortPositionRatio_%s_1H"' % symbol, con=engine_lspr)
    else:
        df_lspr = pd.read_sql('select * from "longShortPositionRatio_%s_1H"' % symbol, con=engine_lspr)

    df_lspr['candle_begin_time']=pd.to_datetime(df_lspr['candle_begin_time'])
    # print(df_lspr)

    # 将df_price中的时间转化为北京时间
    for i in range(df_price.shape[0]):
        df_price.at[i,'candle_begin_time'] = df_price.at[i,'candle_begin_time'] + relativedelta(hours=8)

    df_lspr = df_lspr.merge(df_price,on='candle_begin_time',how='inner')
    df_lspr['lspr'] = df_lspr['ratio']

    # 潜在的因子，大部分没用
    shift_window = int(type[:-1])
    df_lspr['lspr_pct_chg'] = 100*(df_lspr['ratio']/df_lspr['ratio'].shift(1)-1)
    df_lspr['lspr_ma5'] = df_lspr['ratio'].rolling(5).mean()
    df_lspr['lspr_ma10'] = df_lspr['ratio'].rolling(10).mean()
    df_lspr['lspr_ma20'] = df_lspr['ratio'].rolling(20).mean()
    df_lspr['lspr_ma30'] = df_lspr['ratio'].rolling(30).mean()
    df_lspr['lspr_ma50'] = df_lspr['ratio'].rolling(50).mean()
    df_lspr['next_close_pct_chg'] = 100*(df_lspr['close'].shift(-shift_window)/df_lspr['close']-1)
    df_lspr['next_close_pct_chgm1'] = df_lspr['next_close_pct_chg'].shift(1)

    # 未来一段时间内的最大值，用于训练模型
    for i in range(len(df_lspr)):
        df_temp = df_lspr[min(i+1,len(df_lspr)-1):min(i+1+shift_window,len(df_lspr)-1)]
        df_lspr.at[i,'next_low_pct_chg'] = 100*(df_temp['low'].min()/df_lspr.at[i,'close'] -1 )
        df_lspr.at[i,'next_high_pct_chg'] = 100*(df_temp['high'].max()/df_lspr.at[i,'close'] -1 )
    return df_lspr


def rf_forecast(df_price, ob_period='24H', symbol='BTC', win2loss=2):
    '''
    df_price:[candle begin time, open, high, low, close, volume]
    candle begin time是格林威治时间，时间格式，index是数字，1H线，1000行

    :param ob_period: 24H
    :param symbol:
    :param win2loss: 可以开仓的预测盈亏比
    :return: dict
    '''

    # 获取最新lspr和最新bfx价格数据,用于训练森林和预测
    df = gen_data(df_price, symbol=symbol, type=ob_period)
    df_all = df.copy()
    df_all = df_all[['candle_begin_time', 'lspr', 'lspr_ma5', 'lspr_ma20', 'lspr_ma30',
                     'next_low_pct_chg', 'next_high_pct_chg', 'next_close_pct_chg'
                     ]]

    # 生成训练数据，anchor用于判断是否对齐
    def give_train_data(df_all, ob_period, i):
        end_index = i - int(ob_period[:-1])
        df_train = df_all[50:end_index]
        print('训练集大小：', df_train.shape[0])
        ftr_list = ['lspr', 'lspr_ma5',
                    ]
        X = df_train[ftr_list].values
        y_close = df_train['next_close_pct_chg'].values
        y_low = df_train['next_low_pct_chg'].values
        y_high = df_train['next_high_pct_chg'].values
        X_test = df_all[end_index + 1:][ftr_list]
        fore_cast_time = df_all[end_index + 1:]['candle_begin_time']
        anchor = df_all[end_index + 1:]['lspr']
        return X, y_close, y_low, y_high, X_test, fore_cast_time, anchor
    X, y_close, y_low, y_high, X_test, fore_cast_time, anchor = give_train_data(df_all, ob_period, len(df_all) - 1)

    # 两片森林，每片2000棵树
    forest_low = RandomForestRegressor(n_estimators=2000, random_state=0, n_jobs=-1).fit(X, y_low.astype('int'))
    forest_high = RandomForestRegressor(n_estimators=2000, random_state=0, n_jobs=-1).fit(X, y_high.astype('int'))

    # 将预测值填入df
    df_forecast = pd.DataFrame({'candle_begin_time': fore_cast_time,
                                'lspr': anchor,
                                'low_in24h': forest_low.predict(X_test),
                                'high_in24h': forest_high.predict(X_test)})
    df_forecast.reset_index(inplace=True, drop=True)

    # 以字典给出最近24小时的信号
    signal_dict = {}
    for i in range(24):
        key = str(df_forecast.at[i, 'candle_begin_time'])
        position_state = {}
        pre_low = df_forecast.at[i, 'low_in24h']
        pre_high = df_forecast.at[i, 'high_in24h']
        should_open_short = pre_low / pre_high < -win2loss
        should_open_long = pre_high / abs(pre_low) > win2loss
        if should_open_short:
            position_state['position'] = -1
            position_state['stop_profit'] = pre_low
            position_state['stop_loss'] = pre_high
        elif should_open_long:
            position_state['position'] = 1
            position_state['stop_profit'] = pre_high
            position_state['stop_loss'] = pre_low
        else:
            position_state['position'] = 0
            position_state['stop_profit'] = 0
            position_state['stop_loss'] = 0

        signal_dict[key] = position_state

    return signal_dict

def positioin_score(pre_high ,pre_low):
    score = 0
    fliter_condition = (pre_high >0 and pre_low <0)
    if fliter_condition:
        abs_high = abs(pre_high)
        abs_low = abs(pre_low)

        abs_max = max(abs_high ,abs_low)
        abs_min = min(abs_high ,abs_low)

        w2l = abs_max /abs_low

        if abs_max  > 2 and abs_min > 1: # 此时w2l是有效的，且预测波动较大，注重w2l 。第一优先级
            score = w2l + 10000

        elif abs_max > 2 and abs_min <= 1: # 此时预测的绝对值较小方过小，w2l无效，但预测得到的波动较大。第二优先级
            score = abs_max + 5000

        elif abs_max <= 2 and abs_min > 1: # 此时w2l有效，但预测的波动较小，注重波动。第三优先级
            score = abs_max + 2500

        elif abs_max <= 2 and abs_min <=1:  # 此时w2l无效，预测的波动也较小。第四优先级
            score = abs_max + 1250
        else:
            pass
    else:
        pass

    return score