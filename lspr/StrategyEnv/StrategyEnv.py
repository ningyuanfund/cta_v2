import pandas as pd

from lspr.StrategyEnv.SignalGenerator import rf_forecast


class StrategyEnv:
    def __init__(self, para):
        self.para = para  # {'account_num': 24, 'strategy_para': [1, 2, 3]}

    def fake_position_formatter(self, strategy_sig, now_price):
        signal = []
        for i in range(self.para['account_num']):
            signal.append({'check_time': i, 'should_open': 0, 'stop_loss': 0, 'stop_profit': 0})
        return signal

    @staticmethod
    def position_formatter(strategy_sig, now_price, now_time):
        for tm in strategy_sig.keys():
            if pd.to_datetime(now_time) == pd.to_datetime(tm):
                now_pos = strategy_sig[tm]
                break

        signal = [{'check_time': int(pd.to_datetime(now_time).strftime('%H')),
                   'should_open': now_pos['position'],
                   'stop_loss': now_price * (1 + now_pos['stop_loss'] / 100),
                   'stop_profit': now_price * (1 + now_pos['stop_profit'] / 100),
                   'open_price': now_price,
                   }]

        return signal

    def position(self, data):
        now_price = data['close'].iloc[-1]
        now_time = data['candle_begin_time'].iloc[-1]
        strategy_sig = rf_forecast(df_price=data)
        print(strategy_sig)

        return self.position_formatter(strategy_sig=strategy_sig, now_price=now_price, now_time=now_time)

