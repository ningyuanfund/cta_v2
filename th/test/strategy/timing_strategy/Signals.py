import pandas as pd
import numpy as np
from datetime import timedelta
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)




# 贪婪恐惧因子策略
def signal_FG_factor(df,para):
    '''
    贪婪恐惧因子是一个介于0到100的数，0为极度恐惧，100为极度贪婪
    F_open_list=[i for i in range(0, 50, 2)]    # 恐惧时开多
    F_close_list=[i for i in range(0, 50, 2)]   # 恐惧释放时平多
    G_open_list = [i for i in range(50, 100, 2)]    # 贪婪时开空
    G_close_list = [i for i in range(50, 100, 2)]   # 贪婪释放时平空
    :param df:
    :param para: e.x.:[10, 24, 88, 50]
    :return:
    '''

    # 从参数列表提取参数
    F_open = para[0]
    F_close = para[1]
    G_open = para[2]
    G_close = para[3]

    # 将获取的指数填写到交易历史数据中
    index_all = pd.read_csv(r'D:\Work\cta_v2\th\Get_Fear&Greed_index\index.csv')
    index_all['candle_begin_time'] = pd.to_datetime(index_all['timestamp'], unit='s')
    df = pd.merge(df, index_all, how='left', on='candle_begin_time')
    df = df[['candle_begin_time', 'close', 'value']]
    df.fillna(method='ffill', inplace=True)
    df.fillna(method='bfill', inplace=True)

    # ===找出做多信号
    condition0 = df['value'] <= F_open  # 当前指数 <= 开多标准
    df.loc[condition0, 'signal_long'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做多平仓信号
    condition0 = df['value'] > F_close  # 当前指数 > 平多标准
    df.loc[condition0,'signal_long'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===找出做空信号
    condition0 = df['value'] > G_open  # 当前指数 > 开空标准
    df.loc[condition0, 'signal_short'] = -1  # 将产生做空信号的那根K线的signal设置为-1，-1代表做空

    # ===找出做空平仓信号
    condition0 = df['value'] < G_close  # 当前指数 < 平空标准
    df.loc[condition0, 'signal_short'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓
    # df.drop_duplicates(subset=['signal_long', 'signal_short'], inplace=True)

    # ===合并做多做空信号，去除重复信号
    df['signal'] = df[['signal_long', 'signal_short']].sum(axis=1, skipna=True)  # 若你的pandas版本是最新的，请使用下面一行代码代替本行
    # df['signal'] = df[['signal_long', 'signal_short']].sum(axis=1, min_count=1, skipna=True)  # 若你的pandas版本是最新的，请使用本行代码代替上面一行

    temp = df[df['signal'].notnull()][['signal']]
    temp = temp[temp['signal'] != temp['signal'].shift(1)]
    df['signal'] = temp['signal']

    # ===由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。
    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    return df



















# 简单布林线策略
def signal_bolling(df, para=[100, 2]):
    """
    布林线中轨：n天收盘价的移动平均线
    布林线上轨：n天收盘价的移动平均线 + m * n天收盘价的标准差
    布林线上轨：n天收盘价的移动平均线 - m * n天收盘价的标准差
    当收盘价由下向上穿过上轨的时候，做多；然后由上向下穿过下轨的时候，平仓。
    当收盘价由上向下穿过下轨的时候，做空；然后由下向上穿过上轨的时候，平仓。
    :param df:  原始数据
    :param para:  参数，[n, m]
    :return:
    """

    # ===计算指标
    n = para[0]
    m = para[1]

    # 计算均线
    df['median'] = df['close'].rolling(n, min_periods=1).mean()

    # 计算上轨、下轨道
    df['std'] = df['close'].rolling(n, min_periods=1).std(ddof=0)  # ddof代表标准差自由度
    df['upper'] = df['median'] + m * df['std']
    df['lower'] = df['median'] - m * df['std']

    # ===找出做多信号
    condition1 = df['close'] > df['upper']  # 当前K线的收盘价 > 上轨
    condition2 = df['close'].shift(1) <= df['upper'].shift(1)  # 之前K线的收盘价 <= 上轨
    df.loc[condition1 & condition2, 'signal_long'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做多平仓信号
    condition1 = df['close'] < df['median']  # 当前K线的收盘价 < 中轨
    condition2 = df['close'].shift(1) >= df['median'].shift(1)  # 之前K线的收盘价 >= 中轨
    df.loc[condition1 & condition2, 'signal_long'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===找出做空信号
    condition1 = df['close'] < df['lower']  # 当前K线的收盘价 < 下轨
    condition2 = df['close'].shift(1) >= df['lower'].shift(1)  # 之前K线的收盘价 >= 下轨
    df.loc[condition1 & condition2, 'signal_short'] = -1  # 将产生做空信号的那根K线的signal设置为-1，-1代表做空

    # ===找出做空平仓信号
    condition1 = df['close'] > df['median']  # 当前K线的收盘价 > 中轨
    condition2 = df['close'].shift(1) <= df['median'].shift(1)  # 之前K线的收盘价 <= 中轨
    df.loc[condition1 & condition2, 'signal_short'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓
    # df.drop_duplicates(subset=['signal_long', 'signal_short'], inplace=True)

    # ===合并做多做空信号，去除重复信号
    df['signal'] = df[['signal_long', 'signal_short']].sum(axis=1, skipna=True)  # 若你的pandas版本是最新的，请使用下面一行代码代替本行
    # df['signal'] = df[['signal_long', 'signal_short']].sum(axis=1, min_count=1, skipna=True)  # 若你的pandas版本是最新的，请使用本行代码代替上面一行

    temp = df[df['signal'].notnull()][['signal']]
    temp = temp[temp['signal'] != temp['signal'].shift(1)]
    df['signal'] = temp['signal']
    df.drop(['median', 'std', 'upper', 'lower', 'signal_long', 'signal_short'], axis=1, inplace=True)

    # ===由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。
    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    return df


# 带止损的布林线策略
def signal_bolling_with_stop_lose(df, para=[100, 2, 5]):
    """
    布林线中轨：n天收盘价的移动平均线
    布林线上轨：n天收盘价的移动平均线 + m * n天收盘价的标准差
    布林线上轨：n天收盘价的移动平均线 - m * n天收盘价的标准差
    当收盘价由下向上穿过上轨的时候，做多；然后由上向下穿过下轨的时候，平仓。
    当收盘价由上向下穿过下轨的时候，做空；然后由下向上穿过上轨的时候，平仓。

    另外，当价格往亏损方向超过百分之stop_lose的时候，平仓止损。
    :param df:  原始数据
    :param para:  参数，[n, m, stop_lose]
    :return:
    """

    # ===计算指标
    n = para[0]
    m = para[1]

    stop_loss_pct = para[2]
    stop_profit_pct = stop_loss_pct * 1000 # 不止盈

    para_moving_stop_profit = [para[3], para[4]]

    # 移动止盈参数，若均不为零，则采取移动止盈方法
    moving_stop_profit_pct = para_moving_stop_profit[0]
    moving_stop_loss_para_pct = para_moving_stop_profit[1]

    # 计算均线
    n = int(n)
    df['median'] = df['close'].rolling(n, min_periods=1).mean()

    # 计算上轨、下轨道
    df['std'] = df['close'].rolling(n, min_periods=1).std(ddof=0)  # ddof代表标准差自由度
    df['upper'] = df['median'] + m * df['std']
    df['lower'] = df['median'] - m * df['std']

    # 计算固定上轨、下轨，该轨道不随标准差变动，固定为1%
    df['upper_fixed'] = df['median']*(1+0.02)
    df['lower_fixed'] = df['median']*(1-0.02)

    # ===找出做多信号
    condition1 = df['close'] > df['upper']  # 当前K线的收盘价 > 上轨
    condition0 = df['close'] > df['upper_fixed']  # 当前K线的收盘价 > 固定上轨
    condition2 = df['close'].shift(1) <= df['upper'].shift(1)  # 之前K线的收盘价 <= 上轨
    df.loc[condition1 & condition2 & condition0, 'signal_long'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做多平仓信号
    condition1 = df['close'] < df['median']  # 当前K线的收盘价 < 中轨
    condition2 = df['close'].shift(1) >= df['median'].shift(1)  # 之前K线的收盘价 >= 中轨
    df.loc[condition1 & condition2, 'signal_long'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===找出做空信号
    condition1 = df['close'] < df['lower'] # 当前K线的收盘价 < 下轨
    condition0 = df['close'] < df['lower_fixed'] # 当前K线的收盘价 < 固定下轨
    condition2 = df['close'].shift(1) >= df['lower'].shift(1)  # 之前K线的收盘价 >= 下轨
    df.loc[condition1 & condition2 & condition0, 'signal_short'] = -1  # 将产生做空信号的那根K线的signal设置为-1，-1代表做空

    # ===找出做空平仓信号
    condition1 = df['close'] > df['median']  # 当前K线的收盘价 > 中轨
    condition2 = df['close'].shift(1) <= df['median'].shift(1)  # 之前K线的收盘价 <= 中轨
    df.loc[condition1 & condition2, 'signal_short'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_lose_price': None}  # 用于记录之前交易信号，以及止损价格

    # ===防止因为没有产生signal而报错
    df['signal'] = np.nan
    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None}  # 用于记录之前交易信号，以及止损价格

    # ===逐行遍历df，考察每一行的交易信号
    if moving_stop_profit_pct * moving_stop_loss_para_pct == 0:
        # 不启动移动止盈时的信号发出方法
        for i in range(df.shape[0]):
            # 如果之前是空仓
            if info_dict['pre_signal'] == 0:
                # 当本周期有做多信号
                if df.at[i, 'signal_long'] == 1:
                    df.at[i, 'signal'] = 1  # 将真实信号设置为1
                    # 记录当前状态
                    pre_signal = 1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
                # 当本周期有做空信号
                elif df.at[i, 'signal_short'] == -1:
                    df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                    # 记录相关信息
                    pre_signal = -1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
                # 无信号
                else:
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 如果之前是多头仓位
            elif info_dict['pre_signal'] == 1:
                # 当本周期有平多仓信号，或者需要止损
                if (df.at[i, 'signal_long'] == 0) or (df.at[i, 'close'] < info_dict['stop_lose_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None}

                # 当本周期有平多仓并且还要开空仓
                if df.at[i, 'signal_short'] == -1:
                    df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                    # 记录相关信息
                    pre_signal = -1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

            # 如果之前是空头仓位
            elif info_dict['pre_signal'] == -1:
                # 当本周期有平空仓信号，或者需要止损
                if (df.at[i, 'signal_short'] == 0) or (df.at[i, 'close'] > info_dict['stop_lose_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None}

                # 当本周期有平空仓并且还要开多仓
                if df.at[i, 'signal_long'] == 1:
                    df.at[i, 'signal'] = 1  # 将真实信号设置为1
                    # 记录相关信息
                    pre_signal = 1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

            # 其他情况
            else:
                raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错！')

    # 启动移动止盈时的信号发出方法
    else:
        # 逐行遍历df，考察每一行的交易信号
        for i in range(df.shape[0]):
            # 如果之前是空仓
            if info_dict['pre_signal'] == 0:
                # 当本周期有做多信号
                if df.at[i, 'signal_long'] == 1:
                    df.at[i, 'signal'] = 1  # 将真实信号设置为1
                    # 记录当前状态
                    pre_signal = 1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。也可以用下周期的开盘价df.at[i+1, 'open']，但是此时需要注意i等于最后一个i时，取i+1会报错
                    stop_profit_price = df.at[i, 'close'] * (1 + stop_profit_pct / 100)
                    open_price = df.at[i, 'close']
                    # 初始最大盈利为空值
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price,
                                 'stop_profit_price': stop_profit_price, 'profit_max': -0.0001,
                                 'open_price': open_price}
                # 当本周期有做空信号
                elif df.at[i, 'signal_short'] == -1:
                    df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                    # 记录相关信息
                    pre_signal = -1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    stop_profit_price = df.at[i, 'close'] * (1 - stop_profit_pct / 100)
                    open_price = df.at[i, 'close']
                    # 初始最大盈利为空值
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price,
                                 'stop_profit_price': stop_profit_price, 'profit_max': -0.00001,
                                 'open_price': open_price}
                # 无信号
                else:
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None, 'open_price': None}

            # 如果之前是多头仓位
            elif info_dict['pre_signal'] == 1:

                # 计算盈利
                open_price = info_dict['open_price']
                now_price = df.at[i, 'close']
                profit = (now_price - open_price) / open_price

                # 更新最大盈利
                if profit > info_dict['profit_max']:
                    info_dict['profit_max'] = profit

                # 当本周期有平多仓信号，或者需要止损
                if (df.at[i, 'signal_long'] == 0):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None, 'open_price': None}

                # 止损
                elif (df.at[i, 'low'] < info_dict['stop_lose_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None, 'open_price': None}

                # 止盈
                elif (df.at[i, 'high'] > info_dict['stop_profit_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None, 'open_price': None}


                # 当盈利足够大时，开始启用移动止盈.当利润回吐到达一定比例时，止盈
                elif (info_dict['profit_max'] > (stop_loss_pct * moving_stop_loss_para_pct / 100)):
                    if (profit - info_dict['profit_max']) / (info_dict['profit_max']) < -moving_stop_profit_pct:
                        df.at[i, 'signal'] = 0  # 将真实信号设置为0
                        info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                     'profit_max': None, 'open_price': None}

                # 当本周期有平多仓并且还要开空仓
                elif df.at[i, 'signal_short'] == -1:
                    df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                    # 记录相关信息
                    pre_signal = -1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    stop_profit_price = df.at[i, 'close'] * (1 - stop_profit_pct / 100)
                    open_price = df.at[i, 'close']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price,
                                 'stop_profit_price': stop_profit_price, 'profit_max': -0.00001,
                                 'open_price': open_price}


            # 如果之前是空头仓位
            elif info_dict['pre_signal'] == -1:

                # 计算盈利
                open_price = info_dict['open_price']
                now_price = df.at[i, 'close']
                profit = (open_price - now_price) / open_price

                # 更新最大盈利
                if profit > info_dict['profit_max']:
                    info_dict['profit_max'] = profit

                # 当本周期有平空仓信号，或者需要止损
                if (df.at[i, 'signal_short'] == 0):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None,
                                 'open_price': None}

                # 止损
                elif (df.at[i, 'high'] > info_dict['stop_lose_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None,
                                 'open_price': None}

                # 止盈
                elif (df.at[i, 'low'] < info_dict['stop_profit_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None,
                                 'open_price': None}

                # 当盈利足够大时，开始启用移动止盈.当利润回吐到达一定比例时，止盈
                elif (info_dict['profit_max'] > (stop_loss_pct * moving_stop_loss_para_pct / 100)):
                    if (profit - info_dict['profit_max']) / (info_dict['profit_max']) < -moving_stop_profit_pct:
                        df.at[i, 'signal'] = 0  # 将真实信号设置为0
                        info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                     'profit_max': None, 'open_price': None}

                # 当本周期有平空仓并且还要开多仓
                elif df.at[i, 'signal_long'] == 1:
                    df.at[i, 'signal'] = 1  # 将真实信号设置为1
                    # 记录相关信息
                    pre_signal = 1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    stop_profit_price = df.at[i, 'close'] * (1 + stop_profit_pct / 100)
                    open_price = df.at[i, 'close']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price,
                                 'stop_profit_price': stop_profit_price, 'profit_max': -0.00001,
                                 'open_price': open_price}


            # 其他情况
            else:
                raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错')

    # 将无关的变量删除
    df.drop(['median', 'std', 'upper', 'lower','upper_fixed', 'lower_fixed', 'signal_long', 'signal_short'], axis=1, inplace=True)

    # ===由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。
    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    return df


# ===移动平均线策略
# 简单移动均线策略
def signal_moving_average(df, para=[5, 60]):
    """
    简单的移动平均线策略
    当短期均线由下向上穿过长期均线的时候，买入；然后由上向下穿过的时候，卖出。
    :param df:  原始数据
    :param para:  参数，[ma_short, ma_long]
    :return:
    """

    # ===计算指标
    ma_short = para[0]
    ma_long = para[1]

    # 计算均线
    df['ma_short'] = df['close'].rolling(ma_short, min_periods=1).mean()
    df['ma_long'] = df['close'].rolling(ma_long, min_periods=1).mean()

    # ===找出买入信号
    condition1 = df['ma_short'] > df['ma_long']  # 短期均线 > 长期均线
    condition2 = df['ma_short'].shift(1) <= df['ma_long'].shift(1)  # 之前的短期均线 <= 长期均线
    df.loc[condition1 & condition2, 'signal'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出卖出信号
    condition1 = df['ma_short'] < df['ma_long']  # 短期均线 < 长期均线
    condition2 = df['ma_short'].shift(1) >= df['ma_long'].shift(1)  # 之前的短期均线 >= 长期均线
    df.loc[condition1 & condition2, 'signal'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    df.drop(['ma_short', 'ma_long'], axis=1, inplace=True)

    # ===由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。
    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    return df


# 带止损的震荡布林线策略·改
def signal_bolling_tremor_with_stop_lose(df, para=[100, 2, 5]):
    """
    布林线中轨：n天收盘价的移动平均线
    布林线上轨：n天收盘价的移动平均线 + m * n天收盘价的标准差
    布林线上轨：n天收盘价的移动平均线 - m * n天收盘价的标准差

    收盘价下穿上轨做空，到中轨平仓。上穿下轨做多，到中轨平仓。
    当且仅当上一次触及了中轨，才判断下一次是否要开仓
    因此产生信号之后，先fillna，把重复出现的信号全部覆盖掉。称之为预设仓位。当预设仓位与下一个预设仓位不同时，将其视为开仓平仓信号。

    另外，当价格往亏损方向超过百分之stop_lose的时候，平仓止损。
    :param df:  原始数据
    :param para:  参数，[n, m, stop_lose]
    :return:
    """

    # ===计算指标
    n = para[0]
    m = para[1]
    stop_loss_pct = para[2]

    # 计算均线
    df['median'] = df['close'].rolling(n, min_periods=1).mean()

    # 计算上轨、下轨道
    df['std'] = df['close'].rolling(n, min_periods=1).std(ddof=0)  # ddof代表标准差自由度
    df['upper'] = df['median'] + m * df['std']
    df['lower'] = df['median'] - m * df['std']

    # ===找出做多信号，上穿下轨，但没有穿过中轨
    condition1 = df['close'] > df['lower']  # 当前K线的收盘价 > 下轨
    condition2 = df['close'].shift(1) <= df['lower'].shift(1)  # 之前K线的收盘价 <= 下轨
    condition3 = df['close'] < df['median']  # 当前K线的收盘价 < 中轨
    df.loc[condition1 & condition2 & condition3, 'signal_long'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做多平仓信号，上穿中轨
    condition1 = df['close'] > df['median']  # 当前K线的收盘价 > 中轨
    condition2 = df['close'].shift(1) <= df['median'].shift(1)  # 之前K线的收盘价 <= 中轨
    df.loc[condition1 & condition2, 'signal_long'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===找出做空信号，下穿上轨，但没有穿过中轨
    condition1 = df['close'] < df['upper']  # 当前K线的收盘价 < 上轨
    condition2 = df['close'].shift(1) >= df['upper'].shift(1)  # 之前K线的收盘价 >= 下轨
    condition3 = df['close'] > df['median']  # 当前K线的收盘价 > 中轨
    df.loc[condition1 & condition2 & condition3, 'signal_short'] = -1  # 将产生做空信号的那根K线的signal设置为-1，-1代表做空

    # ===找出做空平仓信号，下穿中轨
    condition1 = df['close'] < df['median']  # 当前K线的收盘价 < 中轨
    condition2 = df['close'].shift(1) >= df['median'].shift(1)  # 之前K线的收盘价 >= 中轨
    df.loc[condition1 & condition2, 'signal_short'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===处理重复出现的信号，保证只有触及中轨之后，才进行下一次判断
    df['pre_signal_long'] = df['signal_long'].fillna(method='ffill')
    df['pre_signal_short'] = df['signal_short'].fillna(method='ffill')

    # 将初步产生的signal fillna之后，重复出现的信号全部被覆盖，称为预设仓位。
    # 仅有预设仓位与上一行不同的时候，才被视为真正的信号。
    df.loc[df['pre_signal_long'] != df['pre_signal_long'].shift(1), 'signal_long_true'] = df['pre_signal_long']  # 保留预设仓位与前一行不同的做多信号，为真实的做多信号
    df.loc[df['pre_signal_short'] != df['pre_signal_short'].shift(1), 'signal_short_true'] = df['pre_signal_short']  # 保留预设仓位与前一行不同的做空信号，为真实的做空信号
    df['signal_long'] = df['signal_long_true']
    df['signal_short'] = df['signal_short_true']
    df.drop(['pre_signal_long', 'pre_signal_short', 'signal_long_true', 'signal_short_true'], axis=1, inplace=True)


    # ===防止因为没有产生signal而报错
    df['signal'] = np.nan
    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_lose_price': None}  # 用于记录之前交易信号，以及止损价格

    # 逐行遍历df，考察每一行的交易信号
    for i in range(df.shape[0]):
        if i == (df.shape[0]-1):
            break
        # 如果之前是空仓
        if info_dict['pre_signal'] == 0:
            # 当本周期有做多信号
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录当前状态
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i+1, 'open'] * (1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。也可以用下周期的开盘价df.at[i+1, 'open']，但是此时需要注意i等于最后一个i时，取i+1会报错
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

            # 当本周期有做空信号
            elif df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i+1, 'open'] * (1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 无信号
            else:
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

        # 如果之前是多头仓位
        elif info_dict['pre_signal'] == 1:
            # 当本周期有平多仓信号，或者需要止损
            if (df.at[i, 'signal_long'] == 0) or (df.at[i, 'close'] < info_dict['stop_lose_price']):  # 最低价低于止损价

                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平多仓并且还要开空仓
            if df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 如果之前是空头仓位
        elif info_dict['pre_signal'] == -1:
            # 当本周期有平空仓信号，或者需要止损
            if (df.at[i, 'signal_short'] == 0) or (df.at[i, 'close'] > info_dict['stop_lose_price']):  # 最高价高于止损价

                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平空仓并且还要开多仓
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录相关信息
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 其他情况
        else:
            raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错')

    # 将无关的变量删除
    # df.drop(['median', 'std', 'upper', 'lower', 'signal_long', 'signal_short'], axis=1, inplace=True)

    # ===由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。

    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    return df


# dual thrust
def signal_dual_thrust_with_stop_lose(df, para=[200, 0.7, 0.7, 2, '15T']):

    '''
    N日High的最高价HH, N日Close的最低价LC;
    N日Close的最高价HC，N日Low的最低价LL;
    Range = Max(HH-LC,HC-LL)
    上轨 = 前一个 Open + upper_medium * Range
    下轨 = 前一个 Open + lower_medium * Range
    突破下轨，空仓开空，或多头平多开空
    突破上轨，空仓开多，或空头平空开多
    :param df:
    :param para:[rolling期数, 上轨距离参数, 下轨距离参数, 止损比例]
    :return:
    '''

    rolling_period = int(para[0])
    upper_open = para[1]
    lower_open = para[2]
    stop_loss_pct = para[3]

    # 计算 N 日最高价的最大值 HH
    df['hh'] = df['high'].rolling(rolling_period, min_periods=1).max()
    # 计算 N 日收盘价的最小值 LC
    df['lc'] = df['close'].rolling(rolling_period, min_periods=1).min()
    # 计算 N 日收盘价的最大值 HC
    df['hc'] = df['close'].rolling(rolling_period, min_periods=1).max()
    # 计算 N 日最低价的最小值 LL
    df['ll'] = df['low'].rolling(rolling_period, min_periods=1).min()

    # 计算 range，上下轨距离前一根k线开盘价的距离
    condition1 = (df['hh'] - df['lc']) > (df['hc'] - df['ll'])
    condition2 = (df['hh'] - df['lc']) <= (df['hc'] - df['ll'])

    # Range = Max(HH-LC,HC-LL)
    df.loc[condition1, 'range'] = df['hh'] - df['lc']
    df.loc[condition2, 'range'] = df['hc'] - df['ll']

    # 根据 range 计算上下轨
    df['upper'] = df['open'].shift() + upper_open * df['range']
    df['lower'] = df['open'].shift() - lower_open * df['range']

    # 突破上轨，开多信号（或平空开多）
    condition1 = df['close'] > df['upper']
    condition2 = df['close'].shift(1) <= df['upper'].shift(1)
    df.loc[condition1 & condition2, 'signal_long'] = 1

    # 突破下轨，开空信号（或平多开空）
    condition1 = df['close'] < df['lower']
    condition2 = df['close'].shift(1) >= df['lower'].shift(1)
    df.loc[condition1 & condition2, 'signal_short'] = -1

    # ===防止因为没有产生signal而报错
    df['signal'] = np.nan
    stop_times = 0
    close_times = 0
    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_lose_price': None}  # 用于记录之前交易信号，以及止损价格

    # 逐行遍历df，考察每一行的交易信号
    for i in range(df.shape[0]):
        if i == df.shape[0]:
            break
        # 如果之前是空仓
        if info_dict['pre_signal'] == 0:
            # 当本周期有做多信号
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录当前状态
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i+1, 'open'] * (1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。也可以用下周期的开盘价df.at[i+1, 'open']，但是此时需要注意i等于最后一个i时，取i+1会报错
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

            # 当本周期有做空信号
            elif df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i+1, 'open'] * (1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 无信号
            else:
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

        # 如果之前是多头仓位
        elif info_dict['pre_signal'] == 1:
            # 当本周期需要止损
            if df.at[i, 'close'] < info_dict['stop_lose_price']:  # 最低价低于止损价
                if df.at[i, 'low'] < info_dict['stop_lose_price']:
                    stop_times += 1
                else:
                    close_times += 1

                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平多仓并且还要开空仓
            if df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
                close_times += 1

        # 如果之前是空头仓位
        elif info_dict['pre_signal'] == -1:
            # 当本周期需要止损
            if df.at[i, 'close'] > info_dict['stop_lose_price']:  # 最高价高于止损价
                if df.at[i, 'high'] > info_dict['stop_lose_price']:
                    stop_times += 1
                else:
                    close_times += 1

                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平空仓并且还要开多仓
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录相关信息
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
                close_times += 1

        # 其他情况
        else:
            raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错')

    # 将无关的变量删除
    # df.drop(['median', 'std', 'upper', 'lower', 'signal_long', 'signal_short'], axis=1, inplace=True)

    # ===由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。

    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    return df


# 海龟交易法则
def signal_turtle_close_open_long_short_with_stop_lose(df, para=[20, 10, 5]):
    """
    海龟交易法则的方式开仓和平仓，但是加一个止损，就是开仓之后，相比于开始的价格下跌一定比例，止损。
    :return:

    改进思路，短期跌幅过大，涨幅过大就不能买入了
    """

    n1 = int(para[0])
    n2 = int(para[1])
    stop_loss_pct = float(para[2])

    df['open_close_high'] = df[['open', 'close']].max(axis=1)
    df['open_close_low'] = df[['open', 'close']].min(axis=1)
    # 最近n1日的最高价、最低价
    df['n1_high'] = df['open_close_high'].rolling(n1).max()
    df['n1_low'] = df['open_close_low'].rolling(n1).min()
    # 最近n2日的最高价、最低价
    df['n2_high'] = df['open_close_high'].rolling(n2).max()
    df['n2_low'] = df['open_close_low'].rolling(n2).min()

    # ===找出做多信号
    # 当天的收盘价 > n1日的最高价，做多
    condition = (df['close'] > df['n1_high'].shift(1))
    # 将买入信号当天的signal设置为1
    df.loc[condition, 'signal_long'] = 1
    # ===找出做多平仓
    # 当天的收盘价 < n2日的最低价，多单平仓
    condition = (df['close'] < df['n2_low'].shift(1))
    # 将卖出信号当天的signal设置为0
    df.loc[condition, 'signal_long'] = 0

    # ===找出做空信号
    # 当天的收盘价 < n1日的最低价，做空
    condition = (df['close'] < df['n1_low'].shift(1))
    df.loc[condition, 'signal_short'] = -1
    # ===找出做空平仓
    # 当天的收盘价 > n2日的最高价，做空平仓
    condition = (df['close'] > df['n2_high'].shift(1))
    # 将卖出信号当天的signal设置为0
    df.loc[condition, 'signal_short'] = 0

    info_dict = {
        'pre_signal': 0,
        'stop_lose_price': None,
    }

    df['signal'] = np.nan

    for i in range(df.shape[0]):
        # 如果目前是空仓
        if info_dict['pre_signal'] == 0:
            # 有做多信号
            if (df.at[i, 'signal_long'] == 1):
                df.at[i, 'signal'] = 1  # 真实信号开仓
                # 记录相关信息
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 - stop_loss_pct / 100)  # 止损价格
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            elif (df.at[i, 'signal_short'] == -1):
                df.at[i, 'signal'] = -1  # 真实信号开仓
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 + stop_loss_pct / 100)  # 止损价格
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 无信号
            else:
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

        # 如果目前是多头仓位
        elif info_dict['pre_signal'] == 1:
            # 有平仓信号，或者需要止损
            if (df.at[i, 'signal_long'] == 0) or (df.at[i, 'close'] < info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 真实信号平仓
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 平仓并且还开空仓
            if df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 真实信号开仓
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 + stop_loss_pct / 100)  # 止损价格
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 如果目前是空头仓位
        elif info_dict['pre_signal'] == -1:
            # 有平仓信号，或者需要止损
            if (df.at[i, 'signal_short'] == 0) or (df.at[i, 'close'] > info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 真实信号平仓
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 平仓并且开多仓
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 真实信号开仓
                # 记录相关信息
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 - stop_loss_pct / 100)  # 止损价格
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 其他情况
        else:
            print('不可能出现其他情况，报错')
            exit()

    # 将无关的变量删除
    # df.drop(['open_close_high', 'open_close_high', 'n1_high', 'n1_low', 'n2_high', 'n2_low', 'signal_long',
    #          'signal_short'], axis=1, inplace=True)

    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    # print(df[df['candle_begin_time'] > pd.to_datetime('2018-03-21 13:30:00')])
    # exit()
    return df


# R breaker 策略
def signal_r_breaker_with_stop_lose(df, para=[0.35, 1.07, 0.07, 0.25, 3]):
    '''
    在空仓的情况下，价格超过突破买入价，采取趋势策略，即在该点位开仓做多
    在空仓的情况下，价格跌破突破卖出价，采取趋势策略，即在该点位开仓做空
    最高价超过观察卖出价，价格回落，跌破反转卖出价，采取反转策略，即在该点位（反手、开仓）做空
    最低价低于观察买入价，价格反弹，超过反转买入价，采取反转策略，即在该点位（反手、开仓）做多

    最高价 > 观察卖出价，收盘价 < 反转卖出价时，开空（如持有多头仓位，平多开空）
    最低价 < 观察买入价，收盘价 > 反转买入价时，开多（如持有空头仓位，平空开多）
    if 空仓，最高价 > 突破买入价，做多
    if 空仓，最低价 < 突破卖出价，做空
    '''

    observe_rate = para[0]  # 观察买入、观察卖出价的计算系数
    reverse_rate1 = para[1]  # 反转买入、反转卖出价的计算系数
    reverse_rate2 = para[2]  # 反转买入、反转卖出价的计算系数
    break_rate = para[3]  # 突破买入、突破卖出价的计算系数
    stop_loss_pct = para[4]  # 止损比例


    df['date'] = df['candle_begin_time'].dt.date

    def f1(x):

        yesterday = x.iloc[1]['date'] - timedelta(days=1)
        df_yesterday = df[df['date'] == yesterday].copy()

        if len(df_yesterday) != 0:
            yesterday_high = df_yesterday['high'].max()  # 昨天的最高价
            yesterday_low = df_yesterday['low'].min()  # 昨天的最低价
            yesterday_close = df_yesterday.iloc[-1]['close']  # 昨天的收盘价

            # 观察卖出价 = 昨天的最高价 + 0.35 * (昨天的收盘价 – 昨天的最低价)
            x['observe_sell'] = yesterday_high + observe_rate * (yesterday_close - yesterday_low)
            # 观察买入价 = 昨天的最低价 - 0.35 * (昨天的最高价 – 昨天的最低价)
            x['observe_buy'] = yesterday_low - observe_rate * (yesterday_high - yesterday_close)
            # 反转卖出价 = 1.07 / 2 * (昨天的最高价 + 昨天的最低价) – 0.07 * 昨天的最低价
            x['reverse_sell'] = reverse_rate1 / 2 * (yesterday_high + yesterday_low) - reverse_rate2 * yesterday_low
            # 反转买入价 = 1.07 / 2 * (昨天的最高价 + 昨天的最低价) – 0.07 * 昨天的最高价
            x['reverse_buy'] = reverse_rate1 / 2 * (yesterday_high + yesterday_low) - reverse_rate2 * yesterday_high
            # 突破买入价 = 观察卖出价 + 0.25 * (观察卖出价 – 观察买入价)
            x['break_buy'] = x['observe_sell'] + break_rate * (x['observe_sell'] - x['observe_buy'])
            # 突破卖出价 = 观察买入价 – 0.25 * (观察卖出价 – 观察买入价)
            x['break_sell'] = x['observe_buy'] - break_rate * (x['observe_sell'] - x['observe_buy'])

            # 盘中做空信号
            # 日内最高价超过观察卖出价后，盘中价格出现回落，且进一步跌破反转卖出价构成的支撑
            # 线时，采取反转策略，即在该点位（反手、开仓）做空；
            condition = x['high'] > x['observe_sell']
            if x[condition].index.min() > 0:
                up_break_index = x[condition].index.min()
                condition1 = x.index > up_break_index
                condition2 = x['low'] < x['reverse_sell']
                x.loc[condition1 & condition2, 'signal_short'] = -1

            # 盘中做多信号
            #日内最低价低于观察买入价后，盘中价格出现反弹，且进一步超过反转买入价构成的阻力
            # 线时，采取反转策略，即在该点位（反手、开仓）做多；
            condition = x['low'] < x['observe_buy']
            if x[condition].index.min() > 0:
                down_break_index = x[condition].index.min()
                condition1 = x.index > down_break_index
                condition2 = x['high'] > x['reverse_buy']
                x.loc[condition1 & condition2, 'signal_long'] = 1

        return x

    df = df.groupby('date').apply(f1)


    # ===防止因为没有产生signal而报错
    df['signal'] = np.nan

    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_lose_price': None}  # 用于记录之前交易信号，以及止损价格

    # 逐行遍历df，考察每一行的交易信号
    for i in range(df.shape[0]):
        # 如果之前是空仓
        if info_dict['pre_signal'] == 0:

            # ===先加上在空仓情况下的特殊开仓情况
            # 在空仓的情况下，价格跌破突破卖出价，采取趋势策略，即在该点位开仓做空
            if df.at[i, 'low'] < df.at[i, 'break_sell']:
                df.at[i, 'signal_short'] = -1
            # 在空仓的情况下，价格超过突破买入价，采取趋势策略，即在该点位开仓做多
            if df.at[i, 'high'] > df.at[i, 'break_buy']:
                df.at[i, 'signal_long'] = 1

            # 当本周期有做多信号
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录当前状态
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。也可以用下周期的开盘价df.at[i+1, 'open']，但是此时需要注意i等于最后一个i时，取i+1会报错
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

            # 当本周期有做空信号
            elif df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 无信号
            else:
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

        # 如果之前是多头仓位
        elif info_dict['pre_signal'] == 1:
            # 当本周期有平多仓信号，或者需要止损
            if (df.at[i, 'signal_long'] == 0) or (df.at[i, 'low'] < info_dict['stop_lose_price']):  # 最低价低于止损价
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平多仓并且还要开空仓
            if df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 如果之前是空头仓位
        elif info_dict['pre_signal'] == -1:
            # 当本周期有平空仓信号，或者需要止损
            if (df.at[i, 'signal_short'] == 0) or (df.at[i, 'high'] > info_dict['stop_lose_price']):  # 最高价高于止损价
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平空仓并且还要开多仓
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录相关信息
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 其他情况
        else:
            raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错')


    # 将无关的变量删除
    # df.drop(['median', 'std', 'upper', 'lower', 'signal_long', 'signal_short'], axis=1, inplace=True)

    # ===由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。

    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    return df


# 均线收缩
def signal_ma_converge_with_stop_lose(df, para=[20, 100, 3, 5]):
    '''
    连续k次收缩，则认为发出一次卖空信号
    连续k次扩大，则认为发出一次买多信号
    :param df:
    :param para:
    :return:
    '''
    ma_short = int(round(para[0],0))
    ma_long = int(round(para[1],0))
    k = int(round(para[2]))
    stop_loss_pct = para[3]

    # 计算均线
    df['ma_short'] = df['close'].rolling(ma_short, min_periods=1).mean()
    df['ma_long'] = df['close'].rolling(ma_long, min_periods=1).mean()

    df['gap'] = df['ma_short'] - df['ma_long']

    # 连续k个gap变大才做多，连续k个gap变小才做空

    # ===找出做多信号
    long_condition = True
    for i in range(int(k)):
        condition = df['gap'].shift(i) > df['gap'].shift(i + 1)
        long_condition = long_condition & condition

    df.loc[long_condition, 'signal_long'] = 1

    # ===找出做空信号
    short_condition = True
    for i in range(int(k)):
        condition = df['gap'].shift(i) < df['gap'].shift(i + 1)
        short_condition = short_condition & condition

    df.loc[short_condition, 'signal_short'] = -1

    # ===防止因为没有产生signal而报错
    df['signal'] = np.nan

    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_lose_price': None}  # 用于记录之前交易信号，以及止损价格

    # 逐行遍历df，考察每一行的交易信号
    for i in range(df.shape[0]):
        # 如果之前是空仓
        if info_dict['pre_signal'] == 0:
            # 当本周期有做多信号
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录当前状态
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                            1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。也可以用下周期的开盘价df.at[i+1, 'open']，但是此时需要注意i等于最后一个i时，取i+1会报错
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

            # 当本周期有做空信号
            elif df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                            1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 无信号
            else:
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

        # 如果之前是多头仓位
        elif info_dict['pre_signal'] == 1:
            # 当本周期有平多仓信号，或者需要止损
            if (df.at[i, 'signal_long'] == 0) or (df.at[i, 'low'] < info_dict['stop_lose_price']):  # 最低价低于止损价
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平多仓并且还要开空仓
            if df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                            1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 如果之前是空头仓位
        elif info_dict['pre_signal'] == -1:
            # 当本周期有平空仓信号，或者需要止损
            if (df.at[i, 'signal_short'] == 0) or (df.at[i, 'high'] > info_dict['stop_lose_price']):  # 最高价高于止损价
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平空仓并且还要开多仓
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录相关信息
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                            1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 其他情况
        else:
            raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错')

    # 将无关的变量删除
    # df.drop(['median', 'std', 'upper', 'lower', 'signal_long', 'signal_short'], axis=1, inplace=True)

    # ===由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。

    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0
    return df


# MACD策略
def signal_macd_with_stop_lose(df, para=[12, 26, 9, 5]):
    """
    简单的MACD策略
    DIF线：m天收盘价的加权移动平均线 - n天收盘价的加权移动平均线
    DEA线：DIF的p日加权移动均线
    MACD：（DIF - DEA) * 2
    DIF和MACD大于0，做多；DIF小于0，平多仓。
    DIF和MACD小于0，做空；DIF大于0，平空仓。
    :param df:  原始数据
    :param para:  参数，[m, n, p]
    :return:
    """

    # ===计算指标
    m = para[0]
    n = para[1]
    p = para[2]
    stop_loss_pct = para[3]

    # 计算短期和长期指数移动平均线
    df['EMA_m'] = df['close'].ewm(span=m, adjust=False).mean()
    df['EMA_n'] = df['close'].ewm(span=n, adjust=False).mean()
    # 计算DIF和DEA
    df['DIF'] = df['EMA_m'] - df['EMA_n']
    df['DEA'] = df['DIF'].ewm(span=p, adjust=False).mean()
    # 计算MACD
    df['MACD'] = (df['DIF'] - df['DEA']) * 2

    # ===找出做多信号
    condition1 = df['DIF'] > 0
    condition2 = df['MACD'] > 0
    df.loc[condition1 & condition2, 'signal_long'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做多平仓信号
    df.loc[df['DIF'] < 0, 'signal_long'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===找出做空信号
    condition1 = df['DIF'] < 0
    condition2 = df['MACD'] < 0
    df.loc[condition1 & condition2, 'signal_short'] = -1  # 将产生做空信号的那根K线的signal设置为-1，-1代表做空

    # ===找出做空平仓信号
    df.loc[df['DIF'] > 0, 'signal_short'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_lose_price': None}  # 用于记录之前交易信号，以及止损价格

    # 逐行遍历df，考察每一行的交易信号
    for i in range(df.shape[0]):
        # 如果之前是空仓
        if info_dict['pre_signal'] == 0:
            # 当本周期有做多信号
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录当前状态
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。也可以用下周期的开盘价df.at[i+1, 'open']，但是此时需要注意i等于最后一个i时，取i+1会报错
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 当本周期有做空信号
            elif df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 无信号
            else:
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

        # 如果之前是多头仓位
        elif info_dict['pre_signal'] == 1:
            # 当本周期有平多仓信号，或者需要止损
            if (df.at[i, 'signal_long'] == 0) or (df.at[i, 'close'] < info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平多仓并且还要开空仓
            if df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 如果之前是空头仓位
        elif info_dict['pre_signal'] == -1:
            # 当本周期有平空仓信号，或者需要止损
            if (df.at[i, 'signal_short'] == 0) or (df.at[i, 'close'] > info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平空仓并且还要开多仓
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录相关信息
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 其他情况
        else:
            raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错！')

    # 将无关的变量删除
    df.drop(['EMA_m', 'EMA_n', 'signal_long', 'signal_short'], axis=1, inplace=True)

    # =====由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。
    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    return df


# kdj策略
def signal_kdj_with_stop_lose(df, para=[9, 20, 80]):
    """
    简单的KDJ策略

    当KDJ都小于p且K线上穿D线的时候，做多；当J线大于100的时候，平多仓
    当KDJ都大于q且K线下穿D线的时候，做空；当J线小于0的时候，平空仓
    :param df:  原始数据
    :param para:  参数，[n, p, q]
    :return:
    """

    # ===计算指标
    n = para[0]
    p = para[1]
    q = para[2]

    # 计算n日的未成熟随机指标rsv
    df['max'] = df['high'].rolling(window=n, min_periods=1).max()
    df['min'] = df['low'].rolling(window=n, min_periods=1).min()
    df['rsv'] = (df['close'] - df['min']) / (df['max'] - df['min']) * 100

    # 计算K、D、J
    df['K'] = df['rsv'].ewm(com=2, adjust=False).mean()
    df['D'] = df['K'].ewm(com=2, adjust=False).mean()
    df['J'] = 3 * df['K'] - 2 * df['D']

    df.drop(['max', 'min', 'rsv'], axis=1, inplace=True)

    # ===找出做多信号
    condition1 = (df['K'] < p) & (df['D'] < p) & (df['J'] < p)   # KDJ都小于p
    condition2 = df['K'].shift(1) < df['D'].shift(1)             # 之前K线 < D线
    condition3 = df['K'] > df['D']                               # 当前K线 > D线
    df.loc[condition1 & condition2 & condition3, 'signal_long'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做多平仓信号
    df.loc[df['J'] > 100, 'signal_long'] = 0               # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===找出做空信号
    condition1 = (df['K'] > q) & (df['D'] > q) & (df['J'] > q)   # KDJ都大于q
    condition2 = df['K'].shift(1) > df['D'].shift(1)             # 之前K线 > D线
    condition3 = df['K'] < df['D']                               # 当前K线 < D线
    df.loc[condition1 & condition2 & condition3, 'signal_short'] = -1  # 将产生做空信号的那根K线的signal设置为-1，-1代表做空

    # ===找出做空平仓信号
    df.loc[df['J'] < 0, 'signal_short'] = 0               # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_lose_price': None}  # 用于记录之前交易信号，以及止损价格
    stop_loss_pct = 5

    # 逐行遍历df，考察每一行的交易信号
    for i in range(df.shape[0]):
        # 如果之前是空仓
        if info_dict['pre_signal'] == 0:
            # 当本周期有做多信号
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录当前状态
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。也可以用下周期的开盘价df.at[i+1, 'open']，但是此时需要注意i等于最后一个i时，取i+1会报错
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 当本周期有做空信号
            elif df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 无信号
            else:
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

        # 如果之前是多头仓位
        elif info_dict['pre_signal'] == 1:
            # 当本周期有平多仓信号，或者需要止损
            if (df.at[i, 'signal_long'] == 0) or (df.at[i, 'close'] < info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平多仓并且还要开空仓
            if df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 如果之前是空头仓位
        elif info_dict['pre_signal'] == -1:
            # 当本周期有平空仓信号，或者需要止损
            if (df.at[i, 'signal_short'] == 0) or (df.at[i, 'close'] > info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平空仓并且还要开多仓
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录相关信息
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 其他情况
        else:
            raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错！')

    df.drop(['K', 'D', 'J', 'signal_long', 'signal_short'], axis=1, inplace=True)

    # ===由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。
    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    return df


# emv策略
def signal_emv_with_stop_lose(df, para=[20, 23, 5]):
    """
    简单的emv策略
    当短期均线由下向上穿过长期均线的时候，买入；然后由上向下穿过的时候，卖出。
    :param df:  原始数据
    :param para:  参数，[short, long, stop_loss_pct]
    :return:
    """

    # ===计算指标
    m = para[0]
    n = para[1]
    stop_loss_pct = para[2]

    # 计算emv
    df['em'] = ((df['high'] + df['low']) / 2 - (df['high'].shift(1) + df['low'].shift(1)) / 2) * (
    df['high'] - df['low']) / df['volume']
    df['emv'] = df['em'].rolling(window=m, min_periods=1).mean()
    df['maemv'] = df['emv'].rolling(window=n, min_periods=1).mean()

    # ===空仓开多，或空头平空开多
    condition1 = df['emv'] > df['maemv']                      # 短期均线 > 长期均线
    condition2 = df['emv'].shift(1) <= df['maemv'].shift(1)   # 之前的短期均线 <= 长期均线
    df.loc[condition1 & condition2, 'signal_long'] = 1             # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===空仓开空，或多头平多开空
    condition1 = df['emv'] < df['maemv']                      # 短期均线 < 长期均线
    condition2 = df['emv'].shift(1) >= df['maemv'].shift(1)   # 之前的短期均线 >= 长期均线
    df.loc[condition1 & condition2, 'signal_short'] = -1            # 将产生平仓信号当天的signal设置为0，0代表平仓

    df.drop(['em', 'emv', 'maemv'], axis=1, inplace=True)

    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_lose_price': None}  # 用于记录之前交易信号，以及止损价格

    # 逐行遍历df，考察每一行的交易信号
    for i in range(df.shape[0]):
        # 如果之前是空仓
        if info_dict['pre_signal'] == 0:
            # 当本周期有做多信号
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录当前状态
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。也可以用下周期的开盘价df.at[i+1, 'open']，但是此时需要注意i等于最后一个i时，取i+1会报错
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 当本周期有做空信号
            elif df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 无信号
            else:
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

        # 如果之前是多头仓位
        elif info_dict['pre_signal'] == 1:
            # 当本周期有平多仓信号，或者需要止损
            if (df.at[i, 'signal_long'] == 0) or (df.at[i, 'close'] < info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平多仓并且还要开空仓
            if df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 如果之前是空头仓位
        elif info_dict['pre_signal'] == -1:
            # 当本周期有平空仓信号，或者需要止损
            if (df.at[i, 'signal_short'] == 0) or (df.at[i, 'close'] > info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平空仓并且还要开多仓
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录相关信息
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 其他情况
        else:
            raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错！')

    # ===由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。
    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    return df


# ATR策略
def signal_atr_with_stop_lose(df, para=[14, 2, 5]):
    """
    ATR通道策略
    当收盘价突破上轨，做多；收盘价回复到均线位置，平仓
    当收盘价突破下轨，做空；收盘价回复到均线位置，平仓
    :param df:  原始数据
    :param para:  参数，[window, bias, stop_loss_pct]
    :return:
    """

    # ===计算指标
    n = para[0]
    b = para[1]
    stop_loss_pct = para[2]

    # 计算emv
    df['c1'] = df['high'] - df['low']
    df['c2'] = abs(df['high'] - df['close'].shift(1))
    df['c3'] = abs(df['low'] - df['close'].shift(1))
    df.dropna(inplace=True)
    df.reset_index(drop=True, inplace=True)

    df['tr'] = df[['c1', 'c2', 'c3']].max(axis=1)
    df['atr'] = df['tr'].rolling(window=n, min_periods=1).mean()
    df['avg_price'] = df['close'].rolling(window=n, min_periods=1).mean()
    df['up'] = df['avg_price'] + df['atr'] * b
    df['down'] = df['avg_price'] - df['atr'] * b

    # ===找出买入信号
    df.loc[df['close'] > df['up'], 'signal_long'] = 1             # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做多平仓信号
    condition1 = df['close'] < df['avg_price'] * 1.02
    condition2 = df['close'] > df['avg_price'] * 0.98              # 收盘价回复到均线位置平仓
    df.loc[condition1 & condition2, 'signal_long'] = 0             # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===找出做空信号
    df.loc[df['close'] < df['down'], 'signal_short'] = -1
    # ===找出做空平仓
    df.loc[condition1 & condition2, 'signal_short'] = 0

    # ===防止因为没有产生signal而报错
    df['signal'] = np.nan
    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_lose_price': None}  # 用于记录之前交易信号，以及止损价格

    # 逐行遍历df，考察每一行的交易信号
    for i in range(df.shape[0]):
        # 如果之前是空仓
        if info_dict['pre_signal'] == 0:
            # 当本周期有做多信号
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录当前状态
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。也可以用下周期的开盘价df.at[i+1, 'open']，但是此时需要注意i等于最后一个i时，取i+1会报错
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 当本周期有做空信号
            elif df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 无信号
            else:
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

        # 如果之前是多头仓位
        elif info_dict['pre_signal'] == 1:
            # 当本周期有平多仓信号，或者需要止损
            if (df.at[i, 'signal_long'] == 0) or (df.at[i, 'close'] < info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平多仓并且还要开空仓
            if df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 如果之前是空头仓位
        elif info_dict['pre_signal'] == -1:
            # 当本周期有平空仓信号，或者需要止损
            if (df.at[i, 'signal_short'] == 0) or (df.at[i, 'close'] > info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平空仓并且还要开多仓
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录相关信息
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 其他情况
        else:
            raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错！')

    # 将无关的变量删除
    df.drop(['c1', 'c2', 'c3', 'tr'], axis=1, inplace=True)

    # ===由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。
    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    return df


# CCI策略
def signal_cci_with_stop_lose(df, para=[14, 100, 10, 5]):
    """
    CCI策略
    当收盘价由下向上突破-100时，平空做多
    当收盘价由上向下突破100时；平多做空
    :param df:  原始数据
    :param para:  参数，[window, bands, stop_profit_pct, stop_loss_pct]
    :return:
    """

    # ===传入参数
    n = para[0]
    b = para[1]
    stop_profit_pct = para[2]
    stop_loss_pct = para[3]

    # 计算cci指标
    df['tp'] = (df['high'] + df['low'] + df['close']) / 3
    df['ma'] = df['tp'].rolling(window=n, min_periods=1).mean()
    df['md'] = abs(df['close'] - df['ma']).rolling(window=n, min_periods=1).mean()
    df['cci'] = (df['tp'] - df['ma']) / df['md'] / 0.015

    # ===找出买入信号
    df.loc[(df['cci'].shift(1) < -b) & (df['cci'] > -b), 'signal_long'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做空信号
    df.loc[(df['cci'].shift(1) > b) & (df['cci'] < b), 'signal_short'] = -1

    # ===防止因为没有产生signal而报错
    df['signal'] = np.nan
    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_profit_price': None, 'stop_lose_price': None}  # 用于记录之前交易信号，以及止盈止损价格

    # 逐行遍历df，考察每一行的交易信号
    for i in range(df.shape[0]):
        # 如果之前是空仓
        if info_dict['pre_signal'] == 0:
            # 当本周期有做多信号
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录当前状态
                pre_signal = 1  # 信号
                stop_profit_price = df.at[i, 'close'] * (1 + stop_profit_pct / 100)
                stop_lose_price = df.at[i, 'close'] * (
                    1 - stop_loss_pct / 100)          # 以本周期的收盘价乘以一定比例作为止损价格。
                info_dict = {'pre_signal': pre_signal, 'stop_profit_price': stop_profit_price,
                             'stop_lose_price': stop_lose_price}

            # 当本周期有做空信号
            elif df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_profit_price = df.at[i, 'close'] * (1 - stop_profit_pct / 100)
                stop_lose_price = df.at[i, 'close'] * (
                    1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_profit_price': stop_profit_price,
                             'stop_lose_price': stop_lose_price}

            # 无信号
            else:
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_profit_price': None, 'stop_lose_price': None}

        # 如果之前是多头仓位
        elif info_dict['pre_signal'] == 1:
            # 当本周期有平多仓信号，或者需要止盈止损
            if (df.at[i, 'signal_long'] == 0) or (df.at[i, 'close'] > info_dict['stop_profit_price']) or (
                        df.at[i, 'close'] < info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_profit_price': None, 'stop_lose_price': None}

            # 当本周期有平多仓并且还要开空仓
            if df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_profit_price = df.at[i, 'close'] * (1 - stop_profit_pct / 100)
                stop_lose_price = df.at[i, 'close'] * (
                    1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_profit_price': stop_profit_price,
                             'stop_lose_price': stop_lose_price}

        # 如果之前是空头仓位
        elif info_dict['pre_signal'] == -1:
            # 当本周期有平空仓信号，或者需要止盈止损
            if (df.at[i, 'signal_short'] == 0) or (df.at[i, 'close'] < info_dict['stop_profit_price']) or (
                        df.at[i, 'close'] > info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_profit_price': None, 'stop_lose_price': None}

            # 当本周期有平空仓并且还要开多仓
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录相关信息
                pre_signal = 1  # 信号
                stop_profit_price = df.at[i, 'close'] * (1 + stop_profit_pct / 100)
                stop_lose_price = df.at[i, 'close'] * (
                    1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_profit_price': stop_profit_price,
                             'stop_lose_price': stop_lose_price}

        # 其他情况
        else:
            raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错！')

    # 将无关的变量删除
    df.drop(['tp', 'ma', 'md'], axis=1, inplace=True)

    # ===由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。
    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    return df


# 唐奇安通道策略
def signal_dc_with_stop_lose(df, para=[20, 5]):
    """
    上轨 = 过去n天最高价的最大值
    下轨 = 过去n天最低价的最小值
    突破下轨，空仓开空，或多头平多开空
    突破上轨，空仓开多，或空头平空开多
    :param df:
    :param para:
    :return:
    """
    n = para[0]
    stop_loss_pct = para[1]

    # 计算上下轨
    df['up'] = df['high'].rolling(window=n, min_periods=1).max()
    df['down'] = df['low'].rolling(window=n, min_periods=1).min()

    # 收盘价突破上轨：开多信号，或平空开多信号
    df.loc[df['close'] > df['up'].shift(1), 'signal_long'] = 1

    # 收盘价突破下轨：开空信号，或平多开空信号
    df.loc[df['close'] < df['down'].shift(1), 'signal_short'] = -1

    # ===防止因为没有产生signal而报错
    df['signal'] = np.nan
    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_lose_price': None}  # 用于记录之前交易信号，以及止损价格

    # 逐行遍历df，考察每一行的交易信号
    for i in range(df.shape[0]):
        # 如果之前是空仓
        if info_dict['pre_signal'] == 0:
            # 当本周期有做多信号
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录当前状态
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                    1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 当本周期有做空信号
            elif df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                    1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 无信号
            else:
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

        # 如果之前是多头仓位
        elif info_dict['pre_signal'] == 1:
            # 当本周期有平多仓信号，或者需要止损
            if (df.at[i, 'signal_long'] == 0) or (df.at[i, 'close'] < info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平多仓并且还要开空仓
            if df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                    1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 如果之前是空头仓位
        elif info_dict['pre_signal'] == -1:
            # 当本周期有平空仓信号，或者需要止损
            if (df.at[i, 'signal_short'] == 0) or (df.at[i, 'close'] > info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平空仓并且还要开多仓
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录相关信息
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                    1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 其他情况
        else:
            raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错！')

    # ===由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。
    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    return df


# 带移动止盈功能的CMO策略
def signal_cmo_with_stop_lose(df, para=[20, 5005,5,0,0]):
    """
    CMO大于等于n，平空开多；CMO小于等于n，平多开空
    :param df:
    :param para:
    :param para_moving_stop_profit: 传入参数非[0,0]时，启动移动止盈
    :return:
    """

    m = int(para[0])
    n = int(para[1])
    stop_loss_pct = para[2]
    stop_profit_pct = stop_loss_pct*10
    try:
        para_moving_stop_profit = [para[3],para[4]]
    except:
        para_moving_stop_profit = [0,0]

    # 移动止盈参数，若均不为零，则采取移动止盈方法
    moving_stop_profit_pct = para_moving_stop_profit[0]
    moving_stop_loss_para_pct = para_moving_stop_profit[1]

    df['momentum'] = df['close'] - df['close'].shift(1)
    df['up'] = np.where(df['momentum'] > 0, df['momentum'], 0)
    df['dn'] = np.where(df['momentum'] < 0, abs(df['momentum']), 0)

    df['up_sum'] = df['up'].rolling(window=m,min_periods=1).sum()
    df['dn_sum'] = df['dn'].rolling(window=m,min_periods=1).sum()
    df['cmo'] = (df['up_sum']-df['dn_sum'])/(df['up_sum']+df['dn_sum'])*100

    df.loc[df['cmo'] >= n,'signal_long']=1 #开多或平空开多
    df.loc[df['cmo'] <= -n,'signal_short']= -1 #开空或平多开空

    # ===防止因为没有产生signal而报错
    df['signal'] = np.nan
    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None}  # 用于记录之前交易信号，以及止损价格

    # ===逐行遍历df，考察每一行的交易信号
    if moving_stop_profit_pct * moving_stop_loss_para_pct == 0:
        # 不启动移动止盈时的信号发出方法
        for i in range(df.shape[0]):
            # 如果之前是空仓
            if info_dict['pre_signal'] == 0:
                # 当本周期有做多信号
                if df.at[i, 'signal_long'] == 1:
                    df.at[i, 'signal'] = 1  # 将真实信号设置为1
                    # 记录当前状态
                    pre_signal = 1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
                # 当本周期有做空信号
                elif df.at[i, 'signal_short'] == -1:
                    df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                    # 记录相关信息
                    pre_signal = -1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
                # 无信号
                else:
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 如果之前是多头仓位
            elif info_dict['pre_signal'] == 1:
                # 当本周期有平多仓信号，或者需要止损
                if (df.at[i, 'signal_long'] == 0) or (df.at[i, 'close'] < info_dict['stop_lose_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None}

                # 当本周期有平多仓并且还要开空仓
                if df.at[i, 'signal_short'] == -1:
                    df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                    # 记录相关信息
                    pre_signal = -1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

            # 如果之前是空头仓位
            elif info_dict['pre_signal'] == -1:
                # 当本周期有平空仓信号，或者需要止损
                if (df.at[i, 'signal_short'] == 0) or (df.at[i, 'close'] > info_dict['stop_lose_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None}

                # 当本周期有平空仓并且还要开多仓
                if df.at[i, 'signal_long'] == 1:
                    df.at[i, 'signal'] = 1  # 将真实信号设置为1
                    # 记录相关信息
                    pre_signal = 1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

            # 其他情况
            else:
                raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错！')

    # 启动移动止盈时的信号发出方法
    else:
        # 逐行遍历df，考察每一行的交易信号
        for i in range(df.shape[0]):
            # 如果之前是空仓
            if info_dict['pre_signal'] == 0:
                # 当本周期有做多信号
                if df.at[i, 'signal_long'] == 1:
                    df.at[i, 'signal'] = 1  # 将真实信号设置为1
                    # 记录当前状态
                    pre_signal = 1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                                1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。也可以用下周期的开盘价df.at[i+1, 'open']，但是此时需要注意i等于最后一个i时，取i+1会报错
                    stop_profit_price = df.at[i, 'close'] * (1 + stop_profit_pct / 100)
                    open_price = df.at[i, 'close']
                    # 初始最大盈利为空值
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price,
                                 'stop_profit_price': stop_profit_price, 'profit_max': -0.0001,
                                 'open_price': open_price}
                # 当本周期有做空信号
                elif df.at[i, 'signal_short'] == -1:
                    df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                    # 记录相关信息
                    pre_signal = -1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                                1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    stop_profit_price = df.at[i, 'close'] * (1 - stop_profit_pct / 100)
                    open_price = df.at[i, 'close']
                    # 初始最大盈利为空值
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price,
                                 'stop_profit_price': stop_profit_price, 'profit_max': -0.00001,
                                 'open_price': open_price}
                # 无信号
                else:
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None, 'open_price': None}

            # 如果之前是多头仓位
            elif info_dict['pre_signal'] == 1:

                # 计算盈利
                open_price = info_dict['open_price']
                now_price = df.at[i, 'close']
                profit = (now_price - open_price) / open_price

                # 更新最大盈利
                if profit > info_dict['profit_max']:
                    info_dict['profit_max'] = profit

                # 当本周期有平多仓信号，或者需要止损
                if (df.at[i, 'signal_long'] == 0):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None, 'open_price': None}

                # 止损
                elif (df.at[i, 'low'] < info_dict['stop_lose_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None, 'open_price': None}

                # 止盈
                elif (df.at[i, 'high'] > info_dict['stop_profit_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None, 'open_price': None}


                # 当盈利足够大时，开始启用移动止盈.当利润回吐到达一定比例时，止盈
                elif (info_dict['profit_max'] > (stop_loss_pct * moving_stop_loss_para_pct / 100)):
                    if (profit - info_dict['profit_max']) / (info_dict['profit_max']) < -moving_stop_profit_pct:
                        df.at[i, 'signal'] = 0  # 将真实信号设置为0
                        info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                     'profit_max': None, 'open_price': None}

                # 当本周期有平多仓并且还要开空仓
                elif df.at[i, 'signal_short'] == -1:
                    df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                    # 记录相关信息
                    pre_signal = -1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                                1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    stop_profit_price = df.at[i, 'close'] * (1 - stop_profit_pct / 100)
                    open_price = df.at[i, 'close']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price,
                                 'stop_profit_price': stop_profit_price, 'profit_max': -0.00001,
                                 'open_price': open_price}


            # 如果之前是空头仓位
            elif info_dict['pre_signal'] == -1:

                # 计算盈利
                open_price = info_dict['open_price']
                now_price = df.at[i, 'close']
                profit = (open_price - now_price) / open_price

                # 更新最大盈利
                if profit > info_dict['profit_max']:
                    info_dict['profit_max'] = profit

                # 当本周期有平空仓信号，或者需要止损
                if (df.at[i, 'signal_short'] == 0):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None,
                                 'open_price': None}

                # 止损
                elif (df.at[i, 'high'] > info_dict['stop_lose_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None,
                                 'open_price': None}

                # 止盈
                elif (df.at[i, 'low'] < info_dict['stop_profit_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None,
                                 'open_price': None}

                # 当盈利足够大时，开始启用移动止盈.当利润回吐到达一定比例时，止盈
                elif (info_dict['profit_max'] > (stop_loss_pct * moving_stop_loss_para_pct / 100)):
                    if (profit - info_dict['profit_max']) / (info_dict['profit_max']) < -moving_stop_profit_pct:
                        df.at[i, 'signal'] = 0  # 将真实信号设置为0
                        info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                     'profit_max': None, 'open_price': None}

                # 当本周期有平空仓并且还要开多仓
                elif df.at[i, 'signal_long'] == 1:
                    df.at[i, 'signal'] = 1  # 将真实信号设置为1
                    # 记录相关信息
                    pre_signal = 1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                                1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    stop_profit_price = df.at[i, 'close'] * (1 + stop_profit_pct / 100)
                    open_price = df.at[i, 'close']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price,
                                 'stop_profit_price': stop_profit_price, 'profit_max': -0.00001,
                                 'open_price': open_price}


            # 其他情况
            else:
                raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错')

    # ===由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。
    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0
    # df.to_csv(r'E:\cta_v2\ckt\tables\cmo\EOSUSD\result.csv')
    return df







def bolling_macro_flexible_leverage(df,how=[7, 20]):
    try:
        df.drop(['gap'],axis=1, inplace=True)
    except:
        pass
    df_daily = df.copy()
    df_daily = df_daily.resample(rule='1D',
                            on='candle_begin_time',
                            label='left',
                            closed='left').agg({'open': 'first',
                                                'high': 'max',
                                                'low': 'min',
                                                'close': 'last',
                                                'volume': 'sum'})
    df_daily.fillna(method='ffill', inplace=True)
    df_daily['ma_short'] = df_daily['close'].rolling(how[0]).mean()
    df_daily['ma_long'] = df_daily['close'].rolling(how[1]).mean()
    df_daily['gap'] = df_daily['ma_short']/df_daily['ma_long']-1
    df_daily = df_daily[['gap']]
    df_daily.reset_index(inplace=True)
    df_daily.rename({'index': 'candle_begin_time'}, inplace=True)
    df_daily['candle_begin_time'] = pd.to_datetime(df_daily['candle_begin_time'], format="%Y-%m-%d %H:%M:%S")
    df = df.merge(df_daily, on='candle_begin_time', how='outer')
    print(df)
    df['gap'].fillna(method='ffill', inplace=True)
    return df


def bolling_macro_ADX_flexible_leverage(df, how=[12]):

    rolling_period = how[0]
    try:
        df.drop(['ADX'], axis=1, inplace=True)
    except:
        pass
    df_daily = df.copy()
    df_daily = df_daily.resample(rule='1H',
                            on='candle_begin_time',
                            label='left',
                            closed='left').agg({'open': 'first',
                                                'high': 'max',
                                                'low': 'min',
                                                'close': 'last',
                                                'volume': 'sum'})
    df_daily.fillna(method='ffill', inplace=True)
    df_daily.reset_index(inplace=True)
    df_daily['close_mt1'] = df_daily['close'].shift(1)
    df_daily['high_mt1'] = df_daily['high'].shift(1)
    df_daily['low_mt1'] = df_daily['low'].shift(1)
    print(df_daily)

    # 计算TR
    def cal_TR(x, y, z):
        TR = max(abs(x - y), abs(x - z), abs(y - z))
        return TR

    def cal_plusDM(x, y):
        return max(x-y, 0)

    def cal_minusDM(x, y):
        return max(y-x, 0)

    for i in range(df_daily.shape[0]):
        df_daily.at[i, 'TR'] = cal_TR(df_daily.at[i, 'high'], df_daily.at[i, 'low'], df_daily.at[i, 'close_mt1'])
        df_daily.at[i, 'plusDM'] = cal_plusDM(df_daily.at[i, 'high'], df_daily.at[i, 'high_mt1'])
        df_daily.at[i, 'minusDM'] = cal_minusDM(df_daily.at[i, 'low'], df_daily.at[i, 'low_mt1'])

    for columns in ['TR', 'plusDM', 'minusDM']:
        df_daily['%s_%s' % (columns, str(rolling_period))] = df_daily[columns].rolling(rolling_period).mean()

    df_daily['plusDI'] = df_daily['plusDM_%s' % str(rolling_period)]/df_daily['TR_%s' % str(rolling_period)]
    df_daily['minusDI'] = df_daily['minusDM_%s' % str(rolling_period)]/df_daily['TR_%s' % str(rolling_period)]

    df_daily['ADX'] = abs(df_daily['plusDI'] - df_daily['minusDI'])/(df_daily['minusDI']+df_daily['plusDI'])*100
    df_daily.reset_index(inplace=True)
    df_daily.rename({'index': 'candle_begin_time'}, inplace=True)
    df_daily['candle_begin_time'] = pd.to_datetime(df_daily['candle_begin_time'], format="%Y-%m-%d %H:%M:%S")
    df_daily = df_daily[['candle_begin_time','ADX']]
    df = df.merge(df_daily, on='candle_begin_time', how='outer')
    df['ADX'].fillna(method='ffill',inplace=True)
    # df.to_csv(r'D:\Work\cta_v2\fifty-five-data\output\bolling_with_macro_ADX\ETHUSD\1H\local_data\adx_sample.csv')
    # exit()
    return df



# 布林线策略-配合宏观趋势确定杠杆率
def signal_bolling_with_macro_with_stop_lose(df, para=[100, 2, 5, 7, 30]):
    """
    布林线中轨：n天收盘价的移动平均线
    布林线上轨：n天收盘价的移动平均线 + m * n天收盘价的标准差
    布林线上轨：n天收盘价的移动平均线 - m * n天收盘价的标准差
    当收盘价由下向上穿过上轨的时候，做多；然后由上向下穿过下轨的时候，平仓。
    当收盘价由上向下穿过下轨的时候，做空；然后由下向上穿过上轨的时候，平仓。

    另外，当价格往亏损方向超过百分之stop_lose的时候，平仓止损。
    :param df:  原始数据
    :param para:  参数，[n, m, stop_lose]
    :return:
    """

    # ===计算指标
    n = int(para[0])
    m = para[1]

    stop_loss_pct = para[2]
    stop_profit_pct = stop_loss_pct * 10

    para_moving_stop_profit = [para[3], para[4]]

    # 移动止盈参数，若均不为零，则采取移动止盈方法
    moving_stop_profit_pct = para_moving_stop_profit[0]
    moving_stop_loss_para_pct = para_moving_stop_profit[1]

    # 计算均线
    df['median'] = df['close'].rolling(int(n), min_periods=1).mean()

    # 计算上轨、下轨道
    df['std'] = df['close'].rolling(n, min_periods=1).std(ddof=0)  # ddof代表标准差自由度
    df['upper'] = df['median'] + m * df['std']
    df['lower'] = df['median'] - m * df['std']

    # 计算固定上轨、下轨，该轨道不随标准差变动，固定为2%
    df['upper_fixed'] = df['median']*(1)
    df['lower_fixed'] = df['median']*(1)

    # ===找出做多信号
    condition1 = df['close'] > df['upper']  # 当前K线的收盘价 > 上轨
    condition0 = df['close'] > df['upper_fixed']  # 当前K线的收盘价 > 固定上轨
    condition2 = df['close'].shift(1) <= df['upper'].shift(1)  # 之前K线的收盘价 <= 上轨
    df.loc[condition1 & condition2 & condition0, 'signal_long'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做多平仓信号
    condition1 = df['close'] < df['median']  # 当前K线的收盘价 < 中轨
    condition2 = df['close'].shift(1) >= df['median'].shift(1)  # 之前K线的收盘价 >= 中轨
    df.loc[condition1 & condition2, 'signal_long'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===找出做空信号
    condition1 = df['close'] < df['lower'] # 当前K线的收盘价 < 下轨
    condition0 = df['close'] < df['lower_fixed'] # 当前K线的收盘价 < 固定下轨
    condition2 = df['close'].shift(1) >= df['lower'].shift(1)  # 之前K线的收盘价 >= 下轨
    df.loc[condition1 & condition2 & condition0, 'signal_short'] = -1  # 将产生做空信号的那根K线的signal设置为-1，-1代表做空

    # ===找出做空平仓信号
    condition1 = df['close'] > df['median']  # 当前K线的收盘价 > 中轨
    condition2 = df['close'].shift(1) <= df['median'].shift(1)  # 之前K线的收盘价 <= 中轨
    df.loc[condition1 & condition2, 'signal_short'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_lose_price': None}  # 用于记录之前交易信号，以及止损价格

    # ===防止因为没有产生signal而报错
    df['signal'] = np.nan
    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None}  # 用于记录之前交易信号，以及止损价格

    # ===逐行遍历df，考察每一行的交易信号
    if moving_stop_profit_pct * moving_stop_loss_para_pct == 0:
        # 不启动移动止盈时的信号发出方法
        for i in range(df.shape[0]):
            # 如果之前是空仓
            if info_dict['pre_signal'] == 0:
                # 当本周期有做多信号
                if df.at[i, 'signal_long'] == 1:
                    df.at[i, 'signal'] = 1  # 将真实信号设置为1
                    # 记录当前状态
                    pre_signal = 1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
                # 当本周期有做空信号
                elif df.at[i, 'signal_short'] == -1:
                    df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                    # 记录相关信息
                    pre_signal = -1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
                # 无信号
                else:
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 如果之前是多头仓位
            elif info_dict['pre_signal'] == 1:
                # 当本周期有平多仓信号，或者需要止损
                if (df.at[i, 'signal_long'] == 0) or (df.at[i, 'close'] < info_dict['stop_lose_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None}

                # 当本周期有平多仓并且还要开空仓
                if df.at[i, 'signal_short'] == -1:
                    df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                    # 记录相关信息
                    pre_signal = -1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

            # 如果之前是空头仓位
            elif info_dict['pre_signal'] == -1:
                # 当本周期有平空仓信号，或者需要止损
                if (df.at[i, 'signal_short'] == 0) or (df.at[i, 'close'] > info_dict['stop_lose_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None}

                # 当本周期有平空仓并且还要开多仓
                if df.at[i, 'signal_long'] == 1:
                    df.at[i, 'signal'] = 1  # 将真实信号设置为1
                    # 记录相关信息
                    pre_signal = 1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

            # 其他情况
            else:
                raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错！')

    # 启动移动止盈时的信号发出方法
    else:
        # 逐行遍历df，考察每一行的交易信号
        for i in range(df.shape[0]):
            # 如果之前是空仓
            if info_dict['pre_signal'] == 0:
                # 当本周期有做多信号
                if df.at[i, 'signal_long'] == 1:
                    df.at[i, 'signal'] = 1  # 将真实信号设置为1
                    # 记录当前状态
                    pre_signal = 1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。也可以用下周期的开盘价df.at[i+1, 'open']，但是此时需要注意i等于最后一个i时，取i+1会报错
                    stop_profit_price = df.at[i, 'close'] * (1 + stop_profit_pct / 100)
                    open_price = df.at[i, 'close']
                    # 初始最大盈利为空值
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price,
                                 'stop_profit_price': stop_profit_price, 'profit_max': -0.0001,
                                 'open_price': open_price}
                # 当本周期有做空信号
                elif df.at[i, 'signal_short'] == -1:
                    df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                    # 记录相关信息
                    pre_signal = -1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    stop_profit_price = df.at[i, 'close'] * (1 - stop_profit_pct / 100)
                    open_price = df.at[i, 'close']
                    # 初始最大盈利为空值
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price,
                                 'stop_profit_price': stop_profit_price, 'profit_max': -0.00001,
                                 'open_price': open_price}
                # 无信号
                else:
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None, 'open_price': None}

            # 如果之前是多头仓位
            elif info_dict['pre_signal'] == 1:

                # 计算盈利
                open_price = info_dict['open_price']
                now_price = df.at[i, 'close']
                profit = (now_price - open_price) / open_price

                # 更新最大盈利
                if profit > info_dict['profit_max']:
                    info_dict['profit_max'] = profit

                # 当本周期有平多仓信号，或者需要止损
                if (df.at[i, 'signal_long'] == 0):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None, 'open_price': None}

                # 止损
                elif (df.at[i, 'low'] < info_dict['stop_lose_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None, 'open_price': None}

                # 止盈
                elif (df.at[i, 'high'] > info_dict['stop_profit_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None, 'open_price': None}


                # 当盈利足够大时，开始启用移动止盈.当利润回吐到达一定比例时，止盈
                elif (info_dict['profit_max'] > (stop_loss_pct * moving_stop_loss_para_pct / 100)):
                    if (profit - info_dict['profit_max']) / (info_dict['profit_max']) < -moving_stop_profit_pct:
                        df.at[i, 'signal'] = 0  # 将真实信号设置为0
                        info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                     'profit_max': None, 'open_price': None}

                # 当本周期有平多仓并且还要开空仓
                elif df.at[i, 'signal_short'] == -1:
                    df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                    # 记录相关信息
                    pre_signal = -1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    stop_profit_price = df.at[i, 'close'] * (1 - stop_profit_pct / 100)
                    open_price = df.at[i, 'close']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price,
                                 'stop_profit_price': stop_profit_price, 'profit_max': -0.00001,
                                 'open_price': open_price}


            # 如果之前是空头仓位
            elif info_dict['pre_signal'] == -1:

                # 计算盈利
                open_price = info_dict['open_price']
                now_price = df.at[i, 'close']
                profit = (open_price - now_price) / open_price

                # 更新最大盈利
                if profit > info_dict['profit_max']:
                    info_dict['profit_max'] = profit

                # 当本周期有平空仓信号，或者需要止损
                if (df.at[i, 'signal_short'] == 0):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None,
                                 'open_price': None}

                # 止损
                elif (df.at[i, 'high'] > info_dict['stop_lose_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None,
                                 'open_price': None}

                # 止盈
                elif (df.at[i, 'low'] < info_dict['stop_profit_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None,
                                 'open_price': None}

                # 当盈利足够大时，开始启用移动止盈.当利润回吐到达一定比例时，止盈
                elif (info_dict['profit_max'] > (stop_loss_pct * moving_stop_loss_para_pct / 100)):
                    if (profit - info_dict['profit_max']) / (info_dict['profit_max']) < -moving_stop_profit_pct:
                        df.at[i, 'signal'] = 0  # 将真实信号设置为0
                        info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                     'profit_max': None, 'open_price': None}

                # 当本周期有平空仓并且还要开多仓
                elif df.at[i, 'signal_long'] == 1:
                    df.at[i, 'signal'] = 1  # 将真实信号设置为1
                    # 记录相关信息
                    pre_signal = 1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    stop_profit_price = df.at[i, 'close'] * (1 + stop_profit_pct / 100)
                    open_price = df.at[i, 'close']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price,
                                 'stop_profit_price': stop_profit_price, 'profit_max': -0.00001,
                                 'open_price': open_price}


            # 其他情况
            else:
                raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错')

    # 将无关的变量删除
    df.drop(['median', 'std', 'upper', 'lower','upper_fixed', 'lower_fixed', 'signal_long', 'signal_short'], axis=1, inplace=True)

    # ===由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。
    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    return df



# 布林线策略-配合宏观趋势确定杠杆率
def signal_bolling_with_macro_ADX_with_stop_lose(df, para=[100, 2, 5, 7, 30]):
    """
    布林线中轨：n天收盘价的移动平均线
    布林线上轨：n天收盘价的移动平均线 + m * n天收盘价的标准差
    布林线上轨：n天收盘价的移动平均线 - m * n天收盘价的标准差
    当收盘价由下向上穿过上轨的时候，做多；然后由上向下穿过下轨的时候，平仓。
    当收盘价由上向下穿过下轨的时候，做空；然后由下向上穿过上轨的时候，平仓。

    另外，当价格往亏损方向超过百分之stop_lose的时候，平仓止损。
    :param df:  原始数据
    :param para:  参数，[n, m, stop_lose]
    :return:
    """

    # ===计算指标
    n = int(para[0])
    m = para[1]

    stop_loss_pct = para[2]
    stop_profit_pct = stop_loss_pct * 10

    para_moving_stop_profit = [para[3], para[4]]

    # 移动止盈参数，若均不为零，则采取移动止盈方法
    moving_stop_profit_pct = para_moving_stop_profit[0]
    moving_stop_loss_para_pct = para_moving_stop_profit[1]

    # 计算均线
    df['median'] = df['close'].rolling(int(n), min_periods=1).mean()

    # 计算上轨、下轨道
    df['std'] = df['close'].rolling(n, min_periods=1).std(ddof=0)  # ddof代表标准差自由度
    df['upper'] = df['median'] + m * df['std']
    df['lower'] = df['median'] - m * df['std']

    # 计算固定上轨、下轨，该轨道不随标准差变动，固定为2%
    df['upper_fixed'] = df['median']*(1)
    df['lower_fixed'] = df['median']*(1)

    # ===找出做多信号
    condition1 = df['close'] > df['upper']  # 当前K线的收盘价 > 上轨
    condition0 = df['close'] > df['upper_fixed']  # 当前K线的收盘价 > 固定上轨
    condition2 = df['close'].shift(1) <= df['upper'].shift(1)  # 之前K线的收盘价 <= 上轨
    df.loc[condition1 & condition2 & condition0, 'signal_long'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做多平仓信号
    condition1 = df['close'] < df['median']  # 当前K线的收盘价 < 中轨
    condition2 = df['close'].shift(1) >= df['median'].shift(1)  # 之前K线的收盘价 >= 中轨
    df.loc[condition1 & condition2, 'signal_long'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===找出做空信号
    condition1 = df['close'] < df['lower'] # 当前K线的收盘价 < 下轨
    condition0 = df['close'] < df['lower_fixed'] # 当前K线的收盘价 < 固定下轨
    condition2 = df['close'].shift(1) >= df['lower'].shift(1)  # 之前K线的收盘价 >= 下轨
    df.loc[condition1 & condition2 & condition0, 'signal_short'] = -1  # 将产生做空信号的那根K线的signal设置为-1，-1代表做空

    # ===找出做空平仓信号
    condition1 = df['close'] > df['median']  # 当前K线的收盘价 > 中轨
    condition2 = df['close'].shift(1) <= df['median'].shift(1)  # 之前K线的收盘价 <= 中轨
    df.loc[condition1 & condition2, 'signal_short'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_lose_price': None}  # 用于记录之前交易信号，以及止损价格

    # ===防止因为没有产生signal而报错
    df['signal'] = np.nan
    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None}  # 用于记录之前交易信号，以及止损价格

    # ===逐行遍历df，考察每一行的交易信号
    if moving_stop_profit_pct * moving_stop_loss_para_pct == 0:
        # 不启动移动止盈时的信号发出方法
        for i in range(df.shape[0]):
            # 如果之前是空仓
            if info_dict['pre_signal'] == 0:
                # 当本周期有做多信号
                if df.at[i, 'signal_long'] == 1:
                    df.at[i, 'signal'] = 1  # 将真实信号设置为1
                    # 记录当前状态
                    pre_signal = 1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
                # 当本周期有做空信号
                elif df.at[i, 'signal_short'] == -1:
                    df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                    # 记录相关信息
                    pre_signal = -1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
                # 无信号
                else:
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 如果之前是多头仓位
            elif info_dict['pre_signal'] == 1:
                # 当本周期有平多仓信号，或者需要止损
                if (df.at[i, 'signal_long'] == 0) or (df.at[i, 'close'] < info_dict['stop_lose_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None}

                # 当本周期有平多仓并且还要开空仓
                if df.at[i, 'signal_short'] == -1:
                    df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                    # 记录相关信息
                    pre_signal = -1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

            # 如果之前是空头仓位
            elif info_dict['pre_signal'] == -1:
                # 当本周期有平空仓信号，或者需要止损
                if (df.at[i, 'signal_short'] == 0) or (df.at[i, 'close'] > info_dict['stop_lose_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None}

                # 当本周期有平空仓并且还要开多仓
                if df.at[i, 'signal_long'] == 1:
                    df.at[i, 'signal'] = 1  # 将真实信号设置为1
                    # 记录相关信息
                    pre_signal = 1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

            # 其他情况
            else:
                raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错！')

    # 启动移动止盈时的信号发出方法
    else:
        # 逐行遍历df，考察每一行的交易信号
        for i in range(df.shape[0]):
            # 如果之前是空仓
            if info_dict['pre_signal'] == 0:
                # 当本周期有做多信号
                if df.at[i, 'signal_long'] == 1:
                    df.at[i, 'signal'] = 1  # 将真实信号设置为1
                    # 记录当前状态
                    pre_signal = 1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。也可以用下周期的开盘价df.at[i+1, 'open']，但是此时需要注意i等于最后一个i时，取i+1会报错
                    stop_profit_price = df.at[i, 'close'] * (1 + stop_profit_pct / 100)
                    open_price = df.at[i, 'close']
                    # 初始最大盈利为空值
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price,
                                 'stop_profit_price': stop_profit_price, 'profit_max': -0.0001,
                                 'open_price': open_price}
                # 当本周期有做空信号
                elif df.at[i, 'signal_short'] == -1:
                    df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                    # 记录相关信息
                    pre_signal = -1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    stop_profit_price = df.at[i, 'close'] * (1 - stop_profit_pct / 100)
                    open_price = df.at[i, 'close']
                    # 初始最大盈利为空值
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price,
                                 'stop_profit_price': stop_profit_price, 'profit_max': -0.00001,
                                 'open_price': open_price}
                # 无信号
                else:
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None, 'open_price': None}

            # 如果之前是多头仓位
            elif info_dict['pre_signal'] == 1:

                # 计算盈利
                open_price = info_dict['open_price']
                now_price = df.at[i, 'close']
                profit = (now_price - open_price) / open_price

                # 更新最大盈利
                if profit > info_dict['profit_max']:
                    info_dict['profit_max'] = profit

                # 当本周期有平多仓信号，或者需要止损
                if (df.at[i, 'signal_long'] == 0):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None, 'open_price': None}

                # 止损
                elif (df.at[i, 'low'] < info_dict['stop_lose_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None, 'open_price': None}

                # 止盈
                elif (df.at[i, 'high'] > info_dict['stop_profit_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None, 'open_price': None}


                # 当盈利足够大时，开始启用移动止盈.当利润回吐到达一定比例时，止盈
                elif (info_dict['profit_max'] > (stop_loss_pct * moving_stop_loss_para_pct / 100)):
                    if (profit - info_dict['profit_max']) / (info_dict['profit_max']) < -moving_stop_profit_pct:
                        df.at[i, 'signal'] = 0  # 将真实信号设置为0
                        info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                     'profit_max': None, 'open_price': None}

                # 当本周期有平多仓并且还要开空仓
                elif df.at[i, 'signal_short'] == -1:
                    df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                    # 记录相关信息
                    pre_signal = -1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    stop_profit_price = df.at[i, 'close'] * (1 - stop_profit_pct / 100)
                    open_price = df.at[i, 'close']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price,
                                 'stop_profit_price': stop_profit_price, 'profit_max': -0.00001,
                                 'open_price': open_price}


            # 如果之前是空头仓位
            elif info_dict['pre_signal'] == -1:

                # 计算盈利
                open_price = info_dict['open_price']
                now_price = df.at[i, 'close']
                profit = (open_price - now_price) / open_price

                # 更新最大盈利
                if profit > info_dict['profit_max']:
                    info_dict['profit_max'] = profit

                # 当本周期有平空仓信号，或者需要止损
                if (df.at[i, 'signal_short'] == 0):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None,
                                 'open_price': None}

                # 止损
                elif (df.at[i, 'high'] > info_dict['stop_lose_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None,
                                 'open_price': None}

                # 止盈
                elif (df.at[i, 'low'] < info_dict['stop_profit_price']):
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                 'profit_max': None,
                                 'open_price': None}

                # 当盈利足够大时，开始启用移动止盈.当利润回吐到达一定比例时，止盈
                elif (info_dict['profit_max'] > (stop_loss_pct * moving_stop_loss_para_pct / 100)):
                    if (profit - info_dict['profit_max']) / (info_dict['profit_max']) < -moving_stop_profit_pct:
                        df.at[i, 'signal'] = 0  # 将真实信号设置为0
                        info_dict = {'pre_signal': 0, 'stop_lose_price': None, 'stop_profit_price': None,
                                     'profit_max': None, 'open_price': None}

                # 当本周期有平空仓并且还要开多仓
                elif df.at[i, 'signal_long'] == 1:
                    df.at[i, 'signal'] = 1  # 将真实信号设置为1
                    # 记录相关信息
                    pre_signal = 1  # 信号
                    stop_lose_price = df.at[i, 'close'] * (
                            1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                    stop_profit_price = df.at[i, 'close'] * (1 + stop_profit_pct / 100)
                    open_price = df.at[i, 'close']
                    info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price,
                                 'stop_profit_price': stop_profit_price, 'profit_max': -0.00001,
                                 'open_price': open_price}


            # 其他情况
            else:
                raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错')

    # 将无关的变量删除
    df.drop(['median', 'std', 'upper', 'lower','upper_fixed', 'lower_fixed', 'signal_long', 'signal_short'], axis=1, inplace=True)

    # ===由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。
    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    return df