import numpy as np
import pandas as pd
import os
import ast
import itertools
import th.test.strategy.timing_strategy.Signals as sigs
from datetime import timedelta
from dateutil.relativedelta import relativedelta

import warnings
from multiprocessing import Pool
from matplotlib import pyplot as plt, dates, font_manager
from th.cq_sdk.path import get_data_path, get_project_path
from th.test.strategy.timing_strategy.equity_curve import equity_curve_with_long_and_short, find_change_time, equity_curve_delicay,equity_curve_delicay_for_rolling_test


warnings.simplefilter(action='ignore')
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 99000)


# ===具体进行回测
def func(all_data, strategy_name, freq, para, draw_method, method_para):

    method_para['p3'] = para[2]  # 止损参数
    # 计算交易信号
    function_str = 'sigs.signal_%s_with_stop_lose(all_data.copy(), para)' % strategy_name
    df = eval(function_str)

    # 计算资金曲线
    # df = equity_curve_with_long_and_short(df, leverage_rate=3, c_rate=3.0 / 1000)
    df = equity_curve_delicay(df, method=draw_method, method_para=method_para)

    # 计算年化收益
    annual_return = (df['equity_curve'].iloc[-1] / df['equity_curve'].iloc[0]) ** (
        '1 days 00:00:00' / (df['candle_begin_time'].iloc[-1] - df['candle_begin_time'].iloc[0]) * 365) - 1

    # 计算最大回撤
    max_drawdown = (1 - df['equity_curve'] / df['equity_curve'].expanding().max()).max()

    # 计算年化收益回撤比
    ratio = annual_return / max_drawdown
    print(freq, para, method_para, '收益：', df.iloc[-1]['equity_curve'], '年化收益回撤比：', ratio)
    # print(str(round(ratio,1)),end = ' ')
    # print(' ',end=' ')
    # 存储数据（多进程模式下如果保存成dataframe后面会有重复值）
    return str(freq) + '_' + str(para) + '_' + str(df.iloc[-1]['equity_curve']) + '_' + str(
        max_drawdown) + '_' + str(ratio) + '_'+str(method_para)


# ===生成训练期x个月内最优参数的时间点列表，方便循环回测时读取
def gen_train_date(sys_para):
    test_period = sys_para['test_period']  # 参数回测期长度
    use_period = sys_para['use_period']  # 参数使用期长度
    begin_date = pd.to_datetime(sys_para['begin_date'])  # 整体回测开始时间（训练集的最早时间）
    test_end_date = pd.to_datetime(sys_para['begin_date']) + relativedelta(months=use_period)
    train_end_date = pd.to_datetime(sys_para['begin_date']) + relativedelta(months=test_period)

    # 回测的时间点列表
    train_date_list = []
    # while begin_date <= pd.to_datetime(sys_para['end_date']) - relativedelta(months=test_period):
    while train_end_date <= pd.to_datetime(sys_para['end_date']):
        # 生成训练开始时间、训练结束时间、测试开始时间
        train_date_list.append({'train_begin_date': begin_date,
                               'train_end_date': begin_date + relativedelta(months=test_period),
                                'test_end_date': train_end_date + relativedelta(months=use_period),
                                })
        begin_date += relativedelta(months=use_period)
        test_end_date += relativedelta(months=use_period)
        train_end_date += relativedelta(months=use_period)
        # print(test_end_date,use_period,train_end_date,begin_date)


    # 如果每轮回测都要使用所有历史数据
    if sys_para['if_all_data']:
        for date_info in train_date_list:
            date_info['train_begin_date'] = pd.to_datetime(sys_para['begin_date'])
    print(train_date_list)

    return train_date_list


# ===对回测期的数据进行回测，返回最优参数
def train(df, strategy_name, freq, para_list, train_info, rtn_path, draw_method, method_para_list):
    pool = Pool(processes=3)  # 开辟进程池
    result = []

    for para in para_list:
        for method_para in method_para_list:
            result.append(pool.apply_async(func, (df, strategy_name, freq, para, draw_method, method_para,)))

    pool.close()  # 关闭进程池
    pool.join()  # 等待开辟的所有进程执行完后，主进程才继续往下执行

    A = pd.DataFrame()
    for i, res in enumerate(result):
        A.loc[i, '参数'] = res.get().split('_')[1]
        A.loc[i, '时间级别'] = res.get().split('_')[0]
        A.loc[i, '策略收益'] = float(res.get().split('_')[2])
        A.loc[i, '最大回撤'] = float(res.get().split('_')[3])
        A.loc[i, '年化收益/最大回撤'] = float(res.get().split('_')[4])
        A.loc[i, '仓位配置参数'] = res.get().split('_')[5]

    temp = A['参数'].str[1:-1].str.split(',', expand=True)
    rtn = pd.concat([temp, A.iloc[:, 1:]], axis=1)
    # rtn.drop([6], inplace=True)

    # print(rtn, para_names)
    rtn.columns = para_names
    rtn_path = rtn_path + '/para_record'

    if not os.path.exists(rtn_path):
        os.makedirs(rtn_path)

    # 用于保存完整的参数排名结果
    # rtn.to_csv(rtn_path + '/%s.csv' % train_info['train_end_date'].strftime('%Y-%m'), encoding='utf_8_sig')

    # 剔除重复收益率的项
    A.drop_duplicates(subset=['策略收益'], keep='first', inplace=True)

    try:
        # 最大回撤限制
        # A = A[(A['最大回撤'] < 0.2) & (A['策略收益'] > 1)]

        # 按收益率排序
        A.sort_values(by='策略收益', ascending=False, inplace=True)

        # 剔除收益率后95%的项
        A = A.iloc[:int(len(A)*0.95)]
        A.reset_index(drop=True, inplace=True)
        print(A)
        # 最优参数转换回列表格式,在收益率最好的策略中选择收益回撤比最大的
        top_para_str = A.sort_values(by='年化收益/最大回撤', ascending=False)[:1]['参数'].values[0][1:-1].split(',')
        top_method_para_str = A.sort_values(by='年化收益/最大回撤', ascending=False)[:1]['仓位配置参数'].values[-1]

        top_method_para = ast.literal_eval(top_method_para_str)

        top_para = []
        for para in top_para_str:
            try:
                top_para.append(float(para))
            except:
                top_para.append(15)
    except:
        top_para = [170.0, 999, 4.0, 0.0, 0.0, 15]
        top_method_para = {'crate': 0.005, 'standardleverage': 1.5, 'p1': 20, 'p3': 4}

    return top_para, top_method_para

# ===绘图
def gen_NV_graph(pair,test_period,rtn_path,title_content):

    title = ''
    for keys in title_content:
        title += '%s:%s\n'%(keys,str(title_content[keys]))

    # 读取最完整的那个回测df，要包含日期、开高收低、以及需要画图的数据

    draw_df = pd.read_csv(rtn_path+'\%s_%sequity_curve.csv' % (str(test_period),str(sys_para['use_period'])))

    draw_df['candle_begin_time'] = pd.to_datetime(draw_df['candle_begin_time'])
    draw_df['candle_begin_time'] = draw_df['candle_begin_time'].apply(lambda x: dates.date2num(x) * 1440)

    draw_df = draw_df[['candle_begin_time', 'close', 'equity_curve']].copy()
    data_mat = draw_df.as_matrix()

    fig, ax = plt.subplots(figsize=(1100 / 72, 400 / 72))  # 图片的大小，这个可以随便调，反正画出来的是矢量图

    # =====下面这段是为了让横轴刻度显示时间，以及调整横轴刻度的格式
    from matplotlib.ticker import Formatter
    class MyFormatter(Formatter):
        def __init__(self, dates, fmt='%Y-%m-%d-%H:%M:%S'):
            self.dates = dates
            self.fmt = fmt

        def __call__(self, x, pos=0):
            'Return the label for time x at position pos'
            ind = int(np.round(x))
            # ind就是x轴的刻度数值，不是日期的下标

            return dates.num2date(ind / 1440).strftime(self.fmt)

    formatter = MyFormatter(data_mat[:, 0])
    ax.xaxis.set_major_formatter(formatter)

    for label in ax.get_xticklabels():
        label.set_rotation(15)
        label.set_horizontalalignment('right')

    draw_df['time'] = draw_df['candle_begin_time']
    draw_df.set_index('candle_begin_time', inplace=True)
    draw_df.sort_values(ascending=True, by='time', inplace=True)
    # =====横轴可读调整结束

    ax2 = ax.twinx()

    close, = ax.plot(draw_df['close'].shift(1), color='#A50021')
    equity_curve, = ax2.plot(draw_df['equity_curve'].shift(1), color='#0000CC')

    myfont = font_manager.FontProperties(size=20)  # 微软雅黑字体
    plt.legend([close, equity_curve], [pair + 'Price', title], loc='upper left', prop=myfont)
    plt.draw()
    plt.savefig(rtn_path+'\\%s_%s_%s_equity_curve.png' % (str(test_period), str(sys_para['use_period']), str(title_content['feature'])))


# ===主程序
def main(pair, strategy_name, para_list, para_names, sys_para, test_period,draw_method,method_para_list):

    global update_time

    begin_date = sys_para['begin_date']  # 整体回测开始时间（使用的数据的最早时间）
    print(begin_date)

    # 设置路径
    if sys_para['if_all_data']:
        data_para = 'all_data'
    else:
        data_para = 'local_data'

    # 设定路径，如果没有这个路径的话就生成
    path = get_data_path('output', strategy_name, '%s' % pair)
    if not os.path.exists(path):
        os.makedirs(path)

    # 根据不同的时间周期进行回测
    for freq in sys_para['freq']:

        rtn_path = get_data_path('output', strategy_name, '%s' % pair, freq, data_para)  # 用于保存完整的参数排名结果
        if not os.path.exists(rtn_path):
            os.makedirs(rtn_path)

        # 导入总数据
        # all_data = pd.read_hdf(get_data_path('input', '%s.h5' % pair), key=freq)  # 数据文件存放路径
        all_data = pd.read_csv(get_data_path('input', '%s_%s.csv' % (pair, str(freq))), index_col=0)# 数据文件存放路径
        all_data['candle_begin_time'] = pd.to_datetime(all_data['candle_begin_time'])
        all_data = all_data.resample(rule='1H',
                                     on='candle_begin_time',
                                     label='left',
                                     closed='left').agg({'open': 'first',
                                                         'high': 'max',
                                                         'low': 'min',
                                                         'close': 'last',
                                                         'volume': 'sum'})
        all_data.fillna(method='ffill', inplace=True)
        all_data.reset_index(inplace=True)

        # 计算仓位配置的base
        all_data = sigs.bolling_macro_ADX_flexible_leverage(all_data)
        print(all_data[:200])

        # 选取时间段
        all_data = all_data[all_data['candle_begin_time'] >= pd.to_datetime(begin_date)].copy()
        all_data.reset_index(inplace=True, drop=True)

        # 生成回测期、使用期的日期列表
        data_begin_date = all_data.iloc[0]['candle_begin_time']  # 可用数据的最早时间
        data_end_date = all_data.iloc[-1]['candle_begin_time']  # 可用数据的最晚时间

        if data_begin_date > pd.to_datetime(sys_para['begin_date']):
            print('可用数据仅开始于', pd.to_datetime(data_begin_date).strftime('%Y-%m-%d'))
            sys_para['begin_date'] = pd.to_datetime(data_begin_date).strftime('%Y-%m-%d')

        sys_para['end_date'] = data_end_date.strftime('%Y-%m-%d')
        train_list = gen_train_date(sys_para)

        # 回测出每个月月初往前数12个月的最优参数
        top_para_record = {'参数训练开始': [],
                           '参数训练结束': [],
                           '参数': []}

        for train_info in train_list:
            print('本轮回测开始时间为', train_info['train_begin_date'])
            print('本轮回测结束时间为', train_info['train_end_date'])

            # 截取训练数据
            condition1 = all_data['candle_begin_time'] >= pd.to_datetime(train_info['train_begin_date'])
            condition2 = all_data['candle_begin_time'] < pd.to_datetime(train_info['train_end_date'])
            train_df = all_data[condition1 & condition2].copy()
            train_df.reset_index(inplace=True, drop=True)

            # 回测计算最优参数（输出内容待优化）
            top_para, top_method_para = train(train_df, strategy_name, freq, para_list, train_info, rtn_path, draw_method, method_para_list)
            train_info['best_para_trained'] = (top_para, top_method_para)
            print('寻找到的最优参数为', (top_para, top_method_para))

            # 记录最优参数
            top_para_record['参数训练开始'].append(train_info['train_begin_date'])
            top_para_record['参数训练结束'].append(train_info['train_end_date'])
            top_para_record['参数'].append(top_para)
            top_para_record['仓位配置参数'] = str(top_method_para)

            print('*' * 20)
        
        # 输出每个月的最优参数
        top_para_df = pd.DataFrame(top_para_record)
        top_para_df.to_csv(rtn_path + r'/top_para_record.csv', encoding='utf_8_sig')


        # 根据最优参数计算每个月初往后的开仓、平仓、止损点
        trade_record = []
        for train_info in train_list:
            P1 = train_info['best_para_trained']  # 每个月应该使用的最优参数
            para = P1[0]
            method_para_best = P1[1]
            use_period_list = [] # 参数使用的月份
            use_period = sys_para['use_period']
            while use_period > 0:
                use_period_list.append(pd.to_datetime(train_info['test_end_date']) - relativedelta(months=use_period))
                use_period-=1

            # 用该月的最优参数进行回测，找到开仓、平仓、止损点
            monthly_trade_record = find_change_time(all_data, para, strategy_name, use_period_list, freq)
            print(monthly_trade_record)
            if len(monthly_trade_record) > 0:
            # 如果不是第一个月，就需要对交易进行筛选
                try:
                    if train_list.index(train_info) > 0:
                        print(train_list)
                        print(train_list[train_list.index(train_info) - 1])
                        last_trade_end_time = train_list[train_list.index(train_info) - 1]['trade_record'][-1]['trade_end_time']
                        # 检验产生的所有交易，去除不符合要求的（交易开始时间早于前一个月的最后一笔交易的结束时间）
                        for trade in monthly_trade_record:
                            if trade['trade_begin_time'] > last_trade_end_time:
                                trade_record.append(trade)
                    else:
                        trade_record += monthly_trade_record
                except:
                    pass

            train_info['trade_record'] = monthly_trade_record

        # 根据交易记录生成信号df
        trade_df_list = []
        for trade in trade_record:
            trade_df_list.append([trade['trade_begin_time'], trade['long_or_short']])
            trade_df_list.append([trade['trade_end_time'], 0])
        trade_df = pd.DataFrame(trade_df_list, columns=['candle_begin_time', 'signal'])

        trade_df.sort_values(by='candle_begin_time', inplace=True)
        trade_df.drop_duplicates(subset='candle_begin_time', keep='last', inplace=True)

        # 输出交易记录
        trade_df.to_csv(rtn_path + r'/trade.csv')

        # 将信号df与原始数据合并
        all_data = all_data.merge(trade_df, how='outer', on='candle_begin_time')

        # 生成pos，后续计算资金曲线
        all_data['pos'] = all_data['signal'].shift()
        all_data['pos'].fillna(method='ffill', inplace=True)
        all_data['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

        all_data = sigs.bolling_macro_ADX_flexible_leverage(all_data)

        start_equity = 10000
        # 初始化持仓状态
        stat_dict = {'profolio_value': start_equity, 'coin_value': start_equity, 'pos': 0.0, 'start_time': pd.NaT, 'open_price': 0,'change_mark':0}
        for train_info in train_list:
            P1 = train_info['best_para_trained']  # 每个月应该使用的最优参数
            method_para_best = P1[1]
            use_period_list = []  # 参数使用的月份
            use_period = sys_para['use_period']
            while use_period > 0:
                use_period_list.append(pd.to_datetime(train_info['test_end_date']) - relativedelta(months=use_period))
                use_period -= 1
            print(use_period_list)

            all_data, stat_dict = equity_curve_delicay_for_rolling_test(all_data, stat_dict, use_period_list, method=draw_method, method_para=method_para_best)

        all_data.at[0, 'equity_curve'] = 1
        all_data.fillna(method='ffill', inplace=True)

        all_data['equity_change'] = all_data['equity_curve'].pct_change(1)
        df = all_data
        print(df)
        print('最终收益为', df.iloc[-1]['equity_curve'])
        df.to_csv(rtn_path+'\%s_%sequity_curve.csv' % (str(test_period),str(sys_para['use_period'])))
        gen_NV_graph(pair, test_period, rtn_path,title_content)

# 当前月份前推n个月是什么时候
def test_begin_date(begin_date='2019-03-01',back=4):
    '''
    '2018-03-01',训练集4个月，就是2017年11月开始训练
    :param begin_date:
    :param back:
    :return:
    '''
    begin_date_list= begin_date.split('-')

    mod = int(begin_date_list[1])-back

    if mod>0 and mod<10:
        begin_date_list[1] = '0'+str(mod)
    elif mod >= 10:
        begin_date_list[1] = str(mod)
    elif mod<=0:
        begin_date_list[0] = str(int(begin_date_list[0])-1)
        begin_date_list[1] = str(12+mod)

    begin_date = ''
    for i in begin_date_list:
        begin_date+=i+'-'
    begin_date=begin_date[:-1]

    return begin_date




if __name__ == '__main__':

    # 策略名称
    strategy_name = 'bolling_with_macro_ADX'

    # 资金曲线绘制方式
    draw_method = 'macro_ADX'

    # 这边需要手动输入参数名称
    para_names = ['window', 'nstd', '止损比例', 'e', 'd', '时间级别', '时间级别*','策略收益', '最大回撤', '年化收益/最大回撤','仓位配置参数']

    for test_period in [1]:
        title_content = {}
        title_content['strategy'] = strategy_name
        title_content['feature'] = draw_method
        title_content['test_period'] = test_period
        # 回测系统参数
        sys_para = {'test_period': test_period,  # 回测期长度，m个月
                    'use_period': 1,  # 使用期长度，n个月
                    'begin_date_run': '2016-05-01',  # 整体回测开始的时间，即使用数据的最早时间，如果早于数据最早的时间，则会自动使用数据最早的时间
                    'if_all_data': False,  # 如果为True，每轮回测都将使用所有历史数据，但至少一年
                    'freq': ['1H']
                    }

        title_content['begin_date_run'] = sys_para['begin_date_run']
        title_content['freq'] = sys_para['freq']
        title_content['use_period'] = sys_para['use_period']

        sys_para['begin_date'] = test_begin_date(sys_para['begin_date_run'], sys_para['test_period']) #调整使用数据的最早时间，以便从begin_date_run开始回测

        # 待测参数组合。每次回测都相同
        moving_stop_profit = False  # 默认打开移动止盈
        e_list = [5, 7, 11]
        d_list = [0.3, 0.4, 0.8]
        K_min = 100
        K_max = 240
        K_step = 5
        n_list = [i for i in range(K_min, K_max, K_step)]  # [i for i in range(80,200,5)]
        part_name = str(K_min) + '_' + str(K_max) + '_' + str(K_step)  # 用于分布计算，K线周期为从25到60，step为5
        std_begin = 1
        std_end = 5
        std_step = 0.2
        k1_list = [i/(1/std_step) for i in range(int(std_begin/std_step), int(std_end/std_step))]#[i / 2 for i in range(5, 8)]
        stop_loss_begin = 3
        stop_loss_end = 6
        stop_loss_step = 1
        stop_loss_pct_list = range(stop_loss_begin, stop_loss_end, stop_loss_step) # [5, 7, 9]  # 5%  4 
        title_content['para_range'] = str({'n': [K_min,K_max,K_step],'std':[std_begin,std_end,std_step],'stop_loss':[stop_loss_begin,stop_loss_end,stop_loss_step]})


        # 生成参数列表
        para_list = []
        para_moving_stop_profit_list = []
        for n in n_list:
            for k1 in k1_list:
                if moving_stop_profit:
                    for stop_loss_rate in stop_loss_pct_list:
                        # 生成移动止盈参数列
                        for e in e_list:
                            for d in d_list:
                                para_moving_stop_profit_list.append([e, d])
                                para_list.append([n, k1, stop_loss_rate, e, d, 'H'])
                else:
                    for stop_loss_rate in stop_loss_pct_list:
                        para_moving_stop_profit_list = [[0, 0]]
                        para_list.append([n, k1, stop_loss_rate, 0, 0, 'H'])


        def deleteDuplicatedElementFromList2(list):
            resultList = []
            for item in list:
                if not item in resultList:
                    resultList.append(item)
            return resultList

        para_list = deleteDuplicatedElementFromList2(para_list)

        c_rate_list = [0.005]
        standard_leverage_list = [3]
        p1_list = [1] #[0.03, 0.05, 0.07, 0.1, 0.15, 0.25]
        method_para_list = []
        for c_rate in c_rate_list:
            for standard_leverage in standard_leverage_list:
                for p1 in p1_list:
                    method_para_list.append({'crate': c_rate, 'standardleverage': standard_leverage, 'p1': p1})
        method_para_list = deleteDuplicatedElementFromList2(method_para_list)

        title_content['feature_range'] = str({'c_rate':c_rate_list,'p0':standard_leverage_list,'p1':p1_list})

        # 生成参数列表
        for pair in ['ETHUSD', 'BTCUSD']:  # 币种
            print('\n====%s====' % pair)
            main(pair, strategy_name, para_list, para_names, sys_para, test_period, draw_method, method_para_list)