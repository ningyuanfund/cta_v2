import pandas as pd
from program.cq_sdk.path import *
import os

pd.set_option('expand_frame_repr', False)

strategy_name = 'turtle'
pair = 'ETHUSD'
freq = '15T'
data = 'local_data'
para_record = {}
para_list = ['n1', 'n2', '止损比例']

rtn_path = get_data_path('output', strategy_name, '%s' % pair, freq, data, 'para_record')


# 按照不同的规则给参数重新排序
rule = '年化收益/最大回撤'
for roots, dirs, files in os.walk(rtn_path):
    if files:
        for file_name in files:
            file_path = rtn_path + '\\' + file_name
            df = pd.read_csv(file_path)
            # 按照新规则选出最优参数
            df.sort_values(by=rule, inplace=True, ascending=False)
            df.reset_index(inplace=True, drop=True)
            this_period_para = []  # 本段时间里的最优参数
            for para in para_list:
                this_period_para.append(df.iloc[0][para])
            para_record[pd.to_datetime(file_name.strip('.csv'))] = this_period_para
print(para_record)