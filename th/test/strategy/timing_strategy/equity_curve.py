import pandas as pd
from datetime import timedelta
import th.test.strategy.timing_strategy.Signals as sigs
import time

pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1500)


# ===画资金曲线
def equity_curve_with_long_and_short(df, leverage_rate=3, c_rate=3.0/1000, min_margin_rate=0.15):
    """

    :param df:  带有signal和pos的原始数据
    :param leverage_rate:  bfx交易所最多提供3倍杠杆，leverage_rate可以在(0, 3]区间选择
    :param c_rate:  手续费
    :param min_margin_rate:  低保证金比例，必须占到借来资产的15%
    :return:
    """

    # =====基本参数
    init_cash = 100  # 初始资金
    min_margin = init_cash * leverage_rate * min_margin_rate  # 最低保证金

    # =====根据pos计算资金曲线
    # ===计算涨跌幅
    df['change'] = df['close'].pct_change(1)  # 根据收盘价计算涨跌幅
    df['buy_at_open_change'] = df['close'] / df['open'] - 1  # 从今天开盘买入，到今天收盘的涨跌幅
    df['sell_next_open_change'] = df['open'].shift(-1) / df['close'] - 1  # 从今天收盘到明天开盘的涨跌幅
    df.at[len(df) - 1, 'sell_next_open_change'] = 0

    # ===选取开仓、平仓条件
    condition1 = df['pos'] != 0
    condition2 = df['pos'] != df['pos'].shift(1)
    open_pos_condition = condition1 & condition2

    condition1 = df['pos'] != 0
    condition2 = df['pos'] != df['pos'].shift(-1)
    close_pos_condition = condition1 & condition2

    # ===对每次交易进行分组
    df.loc[open_pos_condition, 'start_time'] = df['candle_begin_time']
    df['start_time'].fillna(method='ffill', inplace=True)
    df.loc[df['pos'] == 0, 'start_time'] = pd.NaT

    # ===计算仓位变动
    # 开仓时仓位
    df.loc[open_pos_condition, 'position'] = init_cash * leverage_rate * (1 + df['buy_at_open_change'])  # 建仓后的仓位

    # 开仓后每天的仓位的变动
    group_num = len(df.groupby('start_time'))

    if group_num > 1:
        t = df.groupby('start_time').apply(lambda x: x['close'] / x.iloc[0]['close'] * x.iloc[0]['position'])
        t = t.reset_index(level=[0])
        df['position'] = t['close']
    elif group_num == 1:
        t = df.groupby('start_time')[['close', 'position']].apply(lambda x: x['close'] / x.iloc[0]['close'] * x.iloc[0]['position'])
        df['position'] = t.T.iloc[:, 0]

    # 每根K线仓位的最大值和最小值，针对最高价和最低价
    df['position_max'] = df['position'] * df['high'] / df['close']
    df['position_min'] = df['position'] * df['low'] / df['close']

    # 平仓时仓位
    df.loc[close_pos_condition, 'position'] *= (1 + df.loc[close_pos_condition, 'sell_next_open_change'])

    # ===计算每天实际持有资金的变化
    # 计算持仓利润
    df['profit'] = (df['position'] - init_cash * leverage_rate) * df['pos']  # 持仓盈利或者损失

    # 计算持仓利润最小值
    df.loc[df['pos'] == 1, 'profit_min'] = (df['position_min'] - init_cash * leverage_rate) * df['pos']  # 最小持仓盈利或者损失
    df.loc[df['pos'] == -1, 'profit_min'] = (df['position_max'] - init_cash * leverage_rate) * df['pos']  # 最小持仓盈利或者损失

    # 计算实际资金量
    df['cash'] = init_cash + df['profit']  # 实际资金
    df['cash'] -= init_cash * leverage_rate * c_rate  # 减去建仓时的手续费
    df['cash_min'] = df['cash'] - (df['profit'] - df['profit_min'])  # 实际最小资金
    df.loc[df['cash_min'] < 0, 'cash_min'] = 0  # 如果有小于0，直接设置为0
    df.loc[close_pos_condition, 'cash'] -= df.loc[close_pos_condition, 'position'] * c_rate  # 减去平仓时的手续费

    # ===判断是否会爆仓
    _index = df[df['cash_min'] <= min_margin].index

    if len(_index) > 0:
        # print('有爆仓')
        df.loc[_index, '强平'] = 1
        df['强平'] = df.groupby('start_time')['强平'].fillna(method='ffill')
        df.loc[(df['强平'] == 1) & (df['强平'].shift(1) != 1), 'cash_强平'] = df['cash_min']  # 此处是有问题的
        df.loc[(df['pos'] != 0) & (df['强平'] == 1), 'cash'] = None
        df['cash'].fillna(value=df['cash_强平'], inplace=True)
        df['cash'] = df.groupby('start_time')['cash'].fillna(method='ffill')
        df.drop(['强平', 'cash_强平'], axis=1, inplace=True)  # 删除不必要的数据

    # ===计算资金曲线
    df['equity_change'] = df['cash'].pct_change()
    df.loc[open_pos_condition, 'equity_change'] = df.loc[open_pos_condition, 'cash'] / init_cash - 1  # 开仓日的收益率
    df['equity_change'].fillna(value=0, inplace=True)
    df['equity_curve'] = (1 + df['equity_change']).cumprod()

    # ===判断资金曲线是否有负值，有的话后面都置成0
    if len(df[df['equity_curve'] < 0]) > 0:
        neg_start = df[df['equity_curve'] < 0].index[0]
        df.loc[neg_start:, 'equity_curve'] = 0

    # ===删除不必要的数据
    df.drop(['change', 'buy_at_open_change', 'sell_next_open_change', 'position', 'position_max',
             'position_min', 'profit', 'profit_min', 'cash', 'cash_min'], axis=1, inplace=True)

    return df


# ===找开仓平仓点
def find_change_time(df, para, strategy_name, use_period_list, freq):
    """

    :param df:  带有signal和pos的原始数据
    :param leverage_rate:  bfx交易所最多提供3倍杠杆，leverage_rate可以在(0, 3]区间选择
    :param c_rate:  手续费
    :param min_margin_rate:  低保证金比例，必须占到借来资产的15%
    :return:
    """

    if freq == '15T':
        time_minus = timedelta(minutes=15)
    elif freq == '30T':
        time_minus = timedelta(minutes=30)
    elif freq == '1H':
        time_minus = timedelta(minutes=60)
    elif freq == '1D':
        time_minus = timedelta(days=1)

    function_str = 'sigs.signal_%s_with_stop_lose(df.copy(), para)' % strategy_name
    df = eval(function_str)

    # 先生成信号再截取k线，防止开头k线不够
    df = df[df['candle_begin_time'] >= use_period_list[0]]
    df.reset_index(inplace=True, drop=True)

    # ===选取开仓、平仓条件
    condition1 = df['pos'] != 0
    condition2 = df['pos'] != df['pos'].shift(1)
    open_pos_condition = condition1 & condition2

    # ===对每次交易进行分组
    df.loc[open_pos_condition, 'start_time'] = df['candle_begin_time']
    df['start_time'].fillna(method='ffill', inplace=True)
    df.loc[df['pos'] == 0, 'start_time'] = pd.NaT

    use_period_list_real = []
    for i in use_period_list:
        use_period_list_real.append(i.strftime('%Y-%m'))

    trade_record = []  # 找到这个月往后的交易记录

    def f1(x):
        # 筛选开始时间在本年本月内的交易
        condition = x.iloc[0]['candle_begin_time'].strftime('%Y-%m') in use_period_list_real
        if condition:
            # 需要得到信号发出的时间而不是开仓的时间，所以此处的开仓时间需要减掉一根k线的时间
            trade_record.append({'trade_begin_time': x.iloc[0]['candle_begin_time'] - time_minus,
                                 'trade_end_time': x.iloc[-1]['candle_begin_time'],
                                 'long_or_short': x.iloc[0]['pos'],
                                 })

    df.groupby('start_time').filter(f1)  # filter解决apply首行重复

    return trade_record






def cal_pos_value(pos, open_margin, coin_mounts, open_price, now_price):
    '''
    根据方向计算,持仓个数计算仓位价值
    :param pos:
    :param open_value:
    :param open_price:
    :param now_price:
    :param leverage:
    :return:
    '''
    profit = 0
    if pos == 1.0:  # 如果开多
       profit = coin_mounts*(now_price-open_price)
    elif pos == -1.0:
       profit = coin_mounts * (open_price - now_price)
    pos_value = open_margin + profit
    pos_value = max(0, pos_value)

    return pos_value


def cal_coin_mounts(margin, now_price, leverage,c_rate):
    '''

    :param usable:
    :param leverage:
    :return:
    '''
    coin_amounts = margin*leverage/(now_price+now_price*c_rate*leverage)
    # print('消耗%sUSD' % str(margin))
    # print('购入%s个币' % str(coin_amounts))
    return coin_amounts


def cal_pos_close_value(pos, open_margin, coin_mounts, open_price, close_price, c_rate):
    '''
    计算平仓时能够取回的资金
    :param pos:
    :param open_value:
    :param coin_mounts:
    :param open_price:
    :param close_price:
    :param c_rate:
    :return:
    '''

    profit = 0
    if pos == 1.0:  # 如果开多
        profit = coin_mounts * (close_price - open_price)
    elif pos == -1.0:
        profit = coin_mounts * (open_price - close_price)
    pos_value = open_margin + profit
    pos_value = max(0, pos_value)
    pos_value = pos_value*(1-c_rate)

    # print('平仓回收%sUSD'% str(pos_value))
    return pos_value

def open_pos(pos,df,index,stat_dict,leverage,c_rate):
    '''
    开仓
    :param pos:
    :return:
    '''
    stat_dict['pos'] = pos
    stat_dict['coin_amounts'] = cal_coin_mounts(stat_dict['coin_value'], df.at[index, 'open'], leverage,
                                                c_rate)

    stat_dict['open_margin'] = stat_dict['coin_value']
    stat_dict['open_price'] = df.at[index, 'open']
    stat_dict['coin_value'] = cal_pos_value(pos, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                            stat_dict['open_price'], df.at[index, 'close'])
    stat_dict['start_time'] = df.at[index, 'candle_begin_time']
    stat_dict['change_mark'] = 1
    return stat_dict



def K_method_time_based_portfolio_OCO(df, index, method_para, stat_dict):
    '''
    构造资金组合动态调整
    :param method_para:
    :return:
    '''

    # 用于计算下一根K线的参数
    c_rate = method_para['crate']
    leverage = method_para['leverage']
    bond = method_para['p1']
    rbl_period = method_para['p2']
    oco_stop_loss = method_para['p3']

    # 新一根K线指示的仓位和信号
    pos = df.at[index, 'pos']

    # 前一根K线结束时的账户状态
    stat_pos = stat_dict['pos']

    # 默认未调仓
    stat_dict['change_mark'] = 0

    # 计算当前账户状态
    if index == 0:# 如果是第一根，则分配资金
        stat_dict['bond_value'] = stat_dict['profolio_value']*bond
        stat_dict['coin_value'] = stat_dict['profolio_value']*(1-bond)
        stat_dict['rebalance_time'] = df.at[index, 'candle_begin_time']

    else: #如果不是第一根，则开始计算
        try:
            if stat_pos == 0.0:  # 如果不持仓，则判断是否开仓
                # 这一根不为0，开仓
                if pos != 0:
                    stat_dict = open_pos(pos, df, index, stat_dict, leverage, c_rate)

            elif stat_pos == 1.0:

                if pos == 1.0:# 无操作
                    stat_dict['pos'] = 1.0
                    stat_dict['coin_value'] = cal_pos_value(pos, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                           stat_dict['open_price'], df.at[index, 'close'])

                if pos == 0.0: #平多
                    stat_dict['change_mark'] = 1
                    stat_dict['start_time'] = pd.NaT
                    stat_dict['pos'] = 0.0
                    # # 如果最低价导致止损，那么采用最大亏损法计算账户价值
                    # min_value = cal_pos_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                    #                                        stat_dict['open_price'], df.at[index, 'low'])
                    #
                    # condition00 = (min_value-stat_dict['open_margin'])/stat_dict['open_margin'] < -oco_stop_loss
                    condition00 = False

                    if condition00:
                        stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'], stat_dict['open_price'], stat_dict['open_price']*(1-oco_stop_loss), c_rate)
                    else:
                        # 如果开盘价平仓，则正常计算
                        stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'], stat_dict['open_price'], df.at[index, 'close'], c_rate)
                    stat_dict['coin_amounts'] = 0

                if pos == -1.0: #平多开空

                    stat_dict['start_time'] = df.at[index, 'candle_begin_time']
                    stat_dict['pos'] = -1.0
                    # # 如果最低价导致止损，那么采用最大亏损法计算账户价值
                    # min_value = cal_pos_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                    #                           stat_dict['open_price'], df.at[index, 'low'])
                    #
                    # condition00 = (min_value - stat_dict['open_margin']) / stat_dict['open_margin'] < -oco_stop_loss
                    condition00 = False
                    if condition00:
                        stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                                      stat_dict['open_price'],
                                                                      stat_dict['open_price'] * (1 - oco_stop_loss), c_rate)
                    else:
                        # 如果开盘价平仓，则正常计算
                        stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                                      stat_dict['open_price'], df.at[index, 'close'],
                                                                      c_rate)
                    # 开空
                    stat_dict = open_pos(pos, df, index, stat_dict, leverage, c_rate)


            elif stat_pos == -1.0:
                if pos == -1.0:    # 无操作
                    stat_dict['pos'] = -1.0
                    stat_dict['coin_value'] = cal_pos_value(pos, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                           stat_dict['open_price'], df.at[index, 'close'])

                if pos == 0.0:  # 平空
                    stat_dict['change_mark'] = 1
                    stat_dict['start_time'] = pd.NaT
                    stat_dict['pos'] = 0.0
                    # # 如果最高价导致止损，那么采用最大亏损法计算账户价值
                    # min_value = cal_pos_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                    #                           stat_dict['open_price'], df.at[index, 'high'])
                    #
                    # condition00 = ((min_value - stat_dict['open_margin']) / stat_dict['open_margin']) < -oco_stop_loss
                    condition00 = False

                    if condition00:
                        stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                                      stat_dict['open_price'],
                                                                      stat_dict['open_price'] * (1 + oco_stop_loss), c_rate)
                    else:
                        # 如果开盘价平仓，则正常计算
                        stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                                      stat_dict['open_price'], df.at[index, 'close'],
                                                                      c_rate)
                    stat_dict['coin_amounts'] = 0

                if pos == 1.0: #平空开多
                    stat_dict['start_time'] = df.at[index, 'candle_begin_time']
                    stat_dict['pos'] = 1.0
                    # # 如果最低价导致止损，那么采用最大亏损法计算账户价值
                    # min_value = cal_pos_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                    #                           stat_dict['open_price'], df.at[index, 'high'])
                    #
                    # condition00 = (min_value - stat_dict['open_margin']) / stat_dict['open_margin'] < -oco_stop_loss
                    condition00 = False

                    if condition00:
                        stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                                      stat_dict['open_price'],
                                                                      stat_dict['open_price'] * (1 + oco_stop_loss), c_rate)
                    else:
                        # 如果开盘价平仓，则正常计算
                        stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                                      stat_dict['open_price'], df.at[index, 'close'],
                                                                      c_rate)

                    # 开多
                    stat_dict = open_pos(1, df, index, stat_dict, leverage, c_rate)
        except Exception as e:
            print(stat_dict)
            print(e)
            #如果出现问题，则延续上一根K线的状态
            print(df.at[index, 'candle_begin_time'])
            # print('ignore one K line')
            pass


    # 检查是否应该再平衡账户
    # 必须是不持仓的状态。
    last_change_time = time.mktime(time.strptime(str(stat_dict['rebalance_time']), "%Y-%m-%d %H:%M:%S"))
    now_change_time = time.mktime(time.strptime(str(df.at[index, 'candle_begin_time']), "%Y-%m-%d %H:%M:%S"))

    condition02 = stat_dict['pos'] == 0.0
    condition03 = (now_change_time - last_change_time) > rbl_period*3600

    if condition02 and condition03:
        # print('rebalance!')
        profolio_value = stat_dict['bond_value'] + stat_dict['coin_value']
        stat_dict['bond_value'] = profolio_value*bond
        stat_dict['coin_value'] = profolio_value*(1-bond)
        stat_dict['rebalance_time'] = df.at[index, 'candle_begin_time']

    stat_dict['profolio_value'] = stat_dict['bond_value'] + stat_dict['coin_value']

    return stat_dict



def K_method_time_based_portfolio_OCO_for_rolling_test(df, index, method_para, stat_dict):
    '''
    构造资金组合动态调整
    :param method_para:
    :return:
    '''

    # 用于计算下一根K线的参数
    c_rate = method_para['crate']
    leverage = method_para['leverage']
    bond = method_para['p1']
    rbl_period = method_para['p2']
    oco_stop_loss = method_para['p3']

    # 新一根K线指示的仓位和信号
    pos = df.at[index, 'pos']

    # 前一根K线结束时的账户状态
    stat_pos = stat_dict['pos']

    # 默认未调仓
    stat_dict['change_mark'] = 0

    # 计算当前账户状态
    if 'rebalance_time' not in stat_dict.keys():# 如果不存在rebalance_time这个key，则分配资金
        stat_dict['bond_value'] = stat_dict['profolio_value']*bond
        stat_dict['coin_value'] = stat_dict['profolio_value']*(1-bond)
        stat_dict['rebalance_time'] = df.at[index, 'candle_begin_time']

    else: #如果不是第一根，则开始计算
        try:
            if stat_pos == 0.0:  # 如果不持仓，则判断是否开仓
                # 这一根不为0，开仓
                if pos != 0:
                    stat_dict = open_pos(pos, df, index, stat_dict, leverage, c_rate)

            elif stat_pos == 1.0:

                if pos == 1.0:# 无操作
                    stat_dict['pos'] = 1.0
                    stat_dict['coin_value'] = cal_pos_value(pos, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                           stat_dict['open_price'], df.at[index, 'close'])

                if pos == 0.0: #平多
                    stat_dict['change_mark'] = 1
                    stat_dict['start_time'] = pd.NaT
                    stat_dict['pos'] = 0.0
                    # 如果最低价导致止损，那么采用最大亏损法计算账户价值
                    min_value = cal_pos_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                           stat_dict['open_price'], df.at[index, 'low'])

                    condition00 = (min_value-stat_dict['open_margin'])/stat_dict['open_margin'] < -oco_stop_loss
                    if condition00:
                        stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'], stat_dict['open_price'], stat_dict['open_price']*(1-oco_stop_loss), c_rate)
                    else:
                        # 如果开盘价平仓，则正常计算
                        stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'], stat_dict['open_price'], df.at[index, 'close'], c_rate)
                    stat_dict['coin_amounts'] = 0

                if pos == -1.0: #平多开空

                    stat_dict['start_time'] = df.at[index, 'candle_begin_time']
                    stat_dict['pos'] = -1.0
                    # 如果最低价导致止损，那么采用最大亏损法计算账户价值
                    min_value = cal_pos_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                              stat_dict['open_price'], df.at[index, 'low'])

                    condition00 = (min_value - stat_dict['open_margin']) / stat_dict['open_margin'] < -oco_stop_loss
                    if condition00:
                        stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                                      stat_dict['open_price'],
                                                                      stat_dict['open_price'] * (1 - oco_stop_loss), c_rate)
                    else:
                        # 如果开盘价平仓，则正常计算
                        stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                                      stat_dict['open_price'], df.at[index, 'close'],
                                                                      c_rate)
                    # 开空
                    stat_dict = open_pos(pos, df, index, stat_dict, leverage, c_rate)


            elif stat_pos == -1.0:
                if pos == -1.0:    # 无操作
                    stat_dict['pos'] = -1.0
                    stat_dict['coin_value'] = cal_pos_value(pos, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                           stat_dict['open_price'], df.at[index, 'close'])

                if pos == 0.0:  # 平空
                    stat_dict['change_mark'] = 1
                    stat_dict['start_time'] = pd.NaT
                    stat_dict['pos'] = 0.0
                    # 如果最高价导致止损，那么采用最大亏损法计算账户价值
                    min_value = cal_pos_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                              stat_dict['open_price'], df.at[index, 'high'])

                    condition00 = ((min_value - stat_dict['open_margin']) / stat_dict['open_margin']) < -oco_stop_loss
                    if condition00:
                        stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                                      stat_dict['open_price'],
                                                                      stat_dict['open_price'] * (1 + oco_stop_loss), c_rate)
                    else:
                        # 如果开盘价平仓，则正常计算
                        stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                                      stat_dict['open_price'], df.at[index, 'close'],
                                                                      c_rate)
                    stat_dict['coin_amounts'] = 0

                if pos == 1.0: #平空开多
                    stat_dict['start_time'] = df.at[index, 'candle_begin_time']
                    stat_dict['pos'] = 1.0
                    # 如果最低价导致止损，那么采用最大亏损法计算账户价值
                    min_value = cal_pos_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                              stat_dict['open_price'], df.at[index, 'high'])

                    condition00 = (min_value - stat_dict['open_margin']) / stat_dict['open_margin'] < -oco_stop_loss
                    if condition00:
                        stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                                      stat_dict['open_price'],
                                                                      stat_dict['open_price'] * (1 + oco_stop_loss), c_rate)
                    else:
                        # 如果开盘价平仓，则正常计算
                        stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                                      stat_dict['open_price'], df.at[index, 'close'],
                                                                      c_rate)

                    # 开多
                    stat_dict = open_pos(1, df, index, stat_dict, leverage, c_rate)
        except:
            #如果出现问题，则延续上一根K线的状态

            print('ignore one K line')
            pass


    # 检查是否应该再平衡账户
    # 必须是不持仓的状态。
    last_change_time = time.mktime(time.strptime(str(stat_dict['rebalance_time']), "%Y-%m-%d %H:%M:%S"))
    now_change_time = time.mktime(time.strptime(str(df.at[index, 'candle_begin_time']), "%Y-%m-%d %H:%M:%S"))

    condition02 = stat_dict['pos'] == 0.0
    condition03 = (now_change_time - last_change_time) > rbl_period*3600

    if condition02 and condition03:
        # print('rebalance!')
        profolio_value = stat_dict['bond_value'] + stat_dict['coin_value']
        stat_dict['bond_value'] = profolio_value*bond
        stat_dict['coin_value'] = profolio_value*(1-bond)
        stat_dict['rebalance_time'] = df.at[index, 'candle_begin_time']

    stat_dict['profolio_value'] = stat_dict['bond_value'] + stat_dict['coin_value']

    return stat_dict




def K_method_macro_trend(df,index,method_para,stat_dict):
    '''
    根据宏观趋势确定杠杆率
    :param df:
    :param index:
    :param method_para:
    :param stat_dict:
    :return:
    '''
    limit = method_para['p1']
    standard_leverage = method_para['standardleverage']

    # 新一根K线指示的仓位和信号
    pos = df.at[index, 'pos']

    # 手续费
    c_rate = method_para['crate']

    # 当前双均线距离
    gap = df.at[index, 'gap']

    try:
        if gap >= limit:
            leverage_ratio = 2
        elif df.at[index, 'gap'] <= -limit:
            leverage_ratio = 1/2
        else:
            leverage_ratio = 1
    except:
        leverage_ratio = 1


    real_leverage = standard_leverage * leverage_ratio**pos

    # 前一根K线结束时的账户状态
    stat_pos = stat_dict['pos']

    # 默认未调仓
    stat_dict['change_mark'] = 0

    # 计算当前账户状态
    if stat_pos == 0.0:  # 如果不持仓，则判断是否开仓

        # 这一根不为0，开仓
        if pos != 0:

            stat_dict = open_pos(pos, df, index, stat_dict, real_leverage, c_rate)

    elif stat_pos == 1.0:

        if pos == 1.0:# 无操作
            stat_dict['pos'] = 1.0
            stat_dict['coin_value'] = cal_pos_value(pos, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                   stat_dict['open_price'], df.at[index, 'close'])

        if pos == 0.0: #平多
            stat_dict['change_mark'] = 1
            stat_dict['start_time'] = pd.NaT
            stat_dict['pos'] = 0.0
            stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'], stat_dict['open_price'], df.at[index, 'close'], c_rate)
            stat_dict['coin_amounts'] = 0

        if pos == -1.0: #平多开空
            stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                              stat_dict['open_price'], df.at[index, 'close'],
                                                              c_rate)
            # 开空
            stat_dict = open_pos(pos, df, index, stat_dict, real_leverage, c_rate)

    elif stat_pos == -1.0:
        if pos == -1.0:    # 无操作
            stat_dict['pos'] = -1.0
            stat_dict['coin_value'] = cal_pos_value(pos, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                   stat_dict['open_price'], df.at[index, 'close'])

        if pos == 0.0:  # 平空
            stat_dict['change_mark'] = 1
            stat_dict['start_time'] = pd.NaT
            stat_dict['pos'] = 0.0
            stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                              stat_dict['open_price'], df.at[index, 'close'],
                                                              c_rate)
            stat_dict['coin_amounts'] = 0

        if pos == 1.0: #平空开多
            stat_dict['start_time'] = df.at[index, 'candle_begin_time']
            stat_dict['pos'] = 1.0
             # 如果开盘价平仓，则正常计算
            stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                              stat_dict['open_price'], df.at[index, 'close'],
                                                              c_rate)

            # 开多
            stat_dict = open_pos(1, df, index, stat_dict, real_leverage, c_rate)
    stat_dict['profolio_value'] = stat_dict['coin_value']
    stat_dict['bond_value'] = 0
    return stat_dict


def K_method_macro_trend_for_rolling_test(df, index, method_para, stat_dict):
    '''
    根据宏观趋势确定杠杆率
    :param df:
    :param index:
    :param method_para:
    :param stat_dict:
    :return:
    '''
    stat_dict['change_mark'] = 0
    limit = method_para['p1']
    standard_leverage = method_para['standardleverage']

    # 新一根K线指示的仓位和信号
    pos = df.at[index, 'pos']

    # 手续费
    c_rate = method_para['crate']

    # 当前双均线距离
    gap = df.at[index, 'gap']

    try:
        if gap >= limit:
            leverage_ratio = 2
        elif df.at[index, 'gap'] <= -limit:
            leverage_ratio = 1/2
        else:
            leverage_ratio = 1
    except:
        leverage_ratio = 1


    real_leverage = standard_leverage * leverage_ratio**pos

    # 前一根K线结束时的账户状态
    stat_pos = stat_dict['pos']

    # 默认未调仓
    stat_dict['change_mark'] = 0

    # 计算当前账户状态
    if stat_pos == 0.0:  # 如果不持仓，则判断是否开仓
        stat_dict['start_time'] = pd.NaT
        # 这一根不为0，开仓
        if pos != 0:

            stat_dict = open_pos(pos, df, index, stat_dict, real_leverage, c_rate)

    elif stat_pos == 1.0:

        if pos == 1.0:# 无操作
            stat_dict['pos'] = 1.0
            stat_dict['coin_value'] = cal_pos_value(pos, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                   stat_dict['open_price'], df.at[index, 'close'])

        if pos == 0.0: #平多
            stat_dict['change_mark'] = 1
            stat_dict['start_time'] = pd.NaT
            stat_dict['pos'] = 0.0
            stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'], stat_dict['open_price'], df.at[index, 'close'], c_rate)
            stat_dict['coin_amounts'] = 0

        if pos == -1.0: #平多开空
            stat_dict['change_mark'] = 1
            stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                              stat_dict['open_price'], df.at[index, 'close'],
                                                              c_rate)
            # 开空
            stat_dict = open_pos(pos, df, index, stat_dict, real_leverage, c_rate)

    elif stat_pos == -1.0:
        if pos == -1.0:    # 无操作
            stat_dict['pos'] = -1.0
            stat_dict['coin_value'] = cal_pos_value(pos, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                   stat_dict['open_price'], df.at[index, 'close'])

        if pos == 0.0:  # 平空
            stat_dict['change_mark'] = 1
            stat_dict['start_time'] = pd.NaT
            stat_dict['pos'] = 0.0
            stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                              stat_dict['open_price'], df.at[index, 'close'],
                                                              c_rate)
            stat_dict['coin_amounts'] = 0

        if pos == 1.0: #平空开多
            stat_dict['change_mark'] = 1
            stat_dict['start_time'] = df.at[index, 'candle_begin_time']
            stat_dict['pos'] = 1.0
             # 如果开盘价平仓，则正常计算
            stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                              stat_dict['open_price'], df.at[index, 'close'],
                                                              c_rate)

            # 开多
            stat_dict = open_pos(1, df, index, stat_dict, real_leverage, c_rate)
    stat_dict['profolio_value'] = stat_dict['coin_value']
    stat_dict['bond_value'] = 0
    return stat_dict

def K_method_macro_ADX_for_rolling_test(df, index, method_para, stat_dict):
    '''
    根据宏观趋势确定杠杆率
    :param df:
    :param index:
    :param method_para:
    :param stat_dict:
    :return:
    '''
    stat_dict['change_mark'] = 0
    limit = method_para['p1']
    standard_leverage = method_para['standardleverage']

    # 新一根K线指示的仓位和信号
    pos = df.at[index, 'pos']

    # 手续费
    c_rate = method_para['crate']

    # 当前双均线距离
    ADX = df.at[index, 'ADX']


    real_leverage = ADX / 100 * standard_leverage
    real_leverage = 3


    # 前一根K线结束时的账户状态
    stat_pos = stat_dict['pos']

    # 默认未调仓
    stat_dict['change_mark'] = 0

    # 计算当前账户状态
    if stat_pos == 0.0:  # 如果不持仓，则判断是否开仓
        stat_dict['start_time'] = pd.NaT
        # 这一根不为0，开仓
        if pos != 0:

            stat_dict = open_pos(pos, df, index, stat_dict, real_leverage, c_rate)

    elif stat_pos == 1.0:

        if pos == 1.0:# 无操作
            stat_dict['pos'] = 1.0
            stat_dict['coin_value'] = cal_pos_value(pos, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                   stat_dict['open_price'], df.at[index, 'close'])

        if pos == 0.0: #平多
            stat_dict['change_mark'] = 1
            stat_dict['start_time'] = pd.NaT
            stat_dict['pos'] = 0.0
            stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'], stat_dict['open_price'], df.at[index, 'close'], c_rate)
            stat_dict['coin_amounts'] = 0

        if pos == -1.0: #平多开空
            stat_dict['change_mark'] = 1
            stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                              stat_dict['open_price'], df.at[index, 'close'],
                                                              c_rate)
            # 开空
            stat_dict = open_pos(pos, df, index, stat_dict, real_leverage, c_rate)

    elif stat_pos == -1.0:
        if pos == -1.0:    # 无操作
            stat_dict['pos'] = -1.0
            stat_dict['coin_value'] = cal_pos_value(pos, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                   stat_dict['open_price'], df.at[index, 'close'])

        if pos == 0.0:  # 平空
            stat_dict['change_mark'] = 1
            stat_dict['start_time'] = pd.NaT
            stat_dict['pos'] = 0.0
            stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                              stat_dict['open_price'], df.at[index, 'close'],
                                                              c_rate)
            stat_dict['coin_amounts'] = 0

        if pos == 1.0: #平空开多
            stat_dict['change_mark'] = 1
            stat_dict['start_time'] = df.at[index, 'candle_begin_time']
            stat_dict['pos'] = 1.0
             # 如果开盘价平仓，则正常计算
            stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                              stat_dict['open_price'], df.at[index, 'close'],
                                                              c_rate)

            # 开多
            stat_dict = open_pos(1, df, index, stat_dict, real_leverage, c_rate)
    stat_dict['profolio_value'] = stat_dict['coin_value']
    stat_dict['bond_value'] = 0
    return stat_dict


def K_method_macro_ADX(df,index,method_para,stat_dict):
    '''
    根据宏观趋势确定杠杆率
    :param df:
    :param index:
    :param method_para:
    :param stat_dict:
    :return:
    '''
    stat_dict['change_mark'] = 0
    limit = method_para['p1']
    standard_leverage = method_para['standardleverage']

    # 新一根K线指示的仓位和信号
    pos = df.at[index, 'pos']

    # 手续费
    c_rate = method_para['crate']

    # 当前双均线距离
    ADX = df.at[index, 'ADX']

    # real_leverage = ADX / 100 * standard_leverage
    real_leverage = 3



    # 前一根K线结束时的账户状态
    stat_pos = stat_dict['pos']

    # 默认未调仓
    stat_dict['change_mark'] = 0

    # 计算当前账户状态
    if stat_pos == 0.0:  # 如果不持仓，则判断是否开仓

        # 这一根不为0，开仓
        if pos != 0:

            stat_dict = open_pos(pos, df, index, stat_dict, real_leverage, c_rate)

    elif stat_pos == 1.0:

        if pos == 1.0:# 无操作
            stat_dict['pos'] = 1.0
            stat_dict['coin_value'] = cal_pos_value(pos, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                   stat_dict['open_price'], df.at[index, 'close'])

        if pos == 0.0: #平多
            stat_dict['change_mark'] = 1
            stat_dict['start_time'] = pd.NaT
            stat_dict['pos'] = 0.0
            stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'], stat_dict['open_price'], df.at[index, 'close'], c_rate)
            stat_dict['coin_amounts'] = 0

        if pos == -1.0: #平多开空
            stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                              stat_dict['open_price'], df.at[index, 'close'],
                                                              c_rate)
            # 开空
            stat_dict = open_pos(pos, df, index, stat_dict, real_leverage, c_rate)

    elif stat_pos == -1.0:
        if pos == -1.0:    # 无操作
            stat_dict['pos'] = -1.0
            stat_dict['coin_value'] = cal_pos_value(pos, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                   stat_dict['open_price'], df.at[index, 'close'])

        if pos == 0.0:  # 平空
            stat_dict['change_mark'] = 1
            stat_dict['start_time'] = pd.NaT
            stat_dict['pos'] = 0.0
            stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                              stat_dict['open_price'], df.at[index, 'close'],
                                                              c_rate)
            stat_dict['coin_amounts'] = 0

        if pos == 1.0: #平空开多
            stat_dict['start_time'] = df.at[index, 'candle_begin_time']
            stat_dict['pos'] = 1.0
             # 如果开盘价平仓，则正常计算
            stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                              stat_dict['open_price'], df.at[index, 'close'],
                                                              c_rate)

            # 开多
            stat_dict = open_pos(1, df, index, stat_dict, real_leverage, c_rate)
    stat_dict['profolio_value'] = stat_dict['coin_value']
    stat_dict['bond_value'] = 0
    return stat_dict


def K_method_VaR(df, index,method_para,stat_dict):
    '''
    根据宏观趋势确定杠杆率
    :param df:
    :param index:
    :param method_para:
    :param stat_dict:
    :return:
    '''

    real_leverage = df.at[index,'level']

    # 新一根K线指示的仓位和信号
    pos = df.at[index, 'pos']

    # 手续费
    c_rate = method_para['crate']

    # 前一根K线结束时的账户状态
    stat_pos = stat_dict['pos']

    # 默认未调仓
    stat_dict['change_mark'] = 0

    # 计算当前账户状态
    if stat_pos == 0.0:  # 如果不持仓，则判断是否开仓

        # 这一根不为0，开仓
        if pos != 0:
            stat_dict = open_pos(pos, df, index, stat_dict, real_leverage, c_rate)
            # print(stat_dict)

    elif stat_pos == 1.0:

        if pos == 1.0:  # 无操作
            stat_dict['pos'] = 1.0
            stat_dict['coin_value'] = cal_pos_value(pos, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                    stat_dict['open_price'], df.at[index, 'close'])

        if pos == 0.0:  # 平多
            stat_dict['change_mark'] = 1
            stat_dict['start_time'] = pd.NaT
            stat_dict['pos'] = 0.0
            stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                          stat_dict['open_price'], df.at[index, 'close'], c_rate)
            stat_dict['coin_amounts'] = 0

        if pos == -1.0:  # 平多开空
            stat_dict['coin_value'] = cal_pos_close_value(1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                          stat_dict['open_price'], df.at[index, 'close'],
                                                          c_rate)
            # 开空
            stat_dict = open_pos(pos, df, index, stat_dict, real_leverage, c_rate)

    elif stat_pos == -1.0:
        if pos == -1.0:  # 无操作
            stat_dict['pos'] = -1.0
            stat_dict['coin_value'] = cal_pos_value(pos, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                    stat_dict['open_price'], df.at[index, 'close'])

        if pos == 0.0:  # 平空
            stat_dict['change_mark'] = 1
            stat_dict['start_time'] = pd.NaT
            stat_dict['pos'] = 0.0
            stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                          stat_dict['open_price'], df.at[index, 'close'],
                                                          c_rate)
            stat_dict['coin_amounts'] = 0

        if pos == 1.0:  # 平空开多
            stat_dict['start_time'] = df.at[index, 'candle_begin_time']
            stat_dict['pos'] = 1.0
            # 如果开盘价平仓，则正常计算
            stat_dict['coin_value'] = cal_pos_close_value(-1, stat_dict['open_margin'], stat_dict['coin_amounts'],
                                                          stat_dict['open_price'], df.at[index, 'close'],
                                                          c_rate)

            # 开多
            stat_dict = open_pos(1, df, index, stat_dict, real_leverage, c_rate)
    stat_dict['profolio_value'] = stat_dict['coin_value']
    stat_dict['bond_value'] = 0
    return stat_dict


def equity_curve_delicay(df, method='time_based_portfolio_OCO', method_para={'c_rate':0.003,'leverage':3,'p1':0.7 ,'p2':24*300,'p3':0.05}):
    '''
    逐行计算资金曲线，通过前一K线的状态，根据method计算下一根K线的状态。
    :param df:
    :param method:
    :param method_para:
    :return:
    '''
    df.fillna(method='ffill', inplace=True)
    # 初始资金
    start_equity = 10000
    # 初始化持仓状态
    stat_dict = {'profolio_value': start_equity,'coin_value':start_equity, 'pos': 0.0, 'start_time': pd.NaT, 'open_price': 0}

    # 遍历更新
    for i in range(df.shape[0]):
        stat_dict = eval('K_method_%s(df, i, method_para, stat_dict)' % method)
        df.at[i, 'start_time'] = stat_dict['start_time']
        df.at[i, 'equity_curve'] = stat_dict['profolio_value']/start_equity
        df.at[i, 'coin_value'] = stat_dict['coin_value']
    df.at[0, 'equity_curve'] = 1
    df.fillna(method='ffill', inplace=True)
    df['equity_change'] = df['equity_curve'].pct_change(1)
    return df




def equity_curve_delicay_for_rolling_test(df,stat_dict,work_period,method='time_based_portfolio_OCO', method_para={'c_rate':0.003,'leverage':3,'p1':0.7 ,'p2':24*300,'p3':0.05}):
    '''
    逐行计算资金曲线，通过前一K线的状态，根据method计算下一根K线的状态。
    :param df:
    :param method:
    :param method_para:
    :return:
    '''
    df.fillna(method='ffill', inplace=True)
    start_equity = 10000
    def valid_period(df, i, work_period):
        for month in work_period:
            month_str = str(month)[:7]
            K_time = str(df.at[i, 'candle_begin_time'])
            if month_str in K_time:
                return True
        return False

    # 遍历更新
    for i in range(df.shape[0]):
        if valid_period(df, i, work_period):
            print(df.at[i, 'candle_begin_time'])
            stat_dict = eval('K_method_%s_for_rolling_test(df, i, method_para, stat_dict)' % method)
            print(stat_dict)
            df.at[i, 'method_para'] = str(method_para)
            df.at[i, 'start_time'] = stat_dict['start_time']
            df.at[i, 'equity_curve'] = stat_dict['profolio_value']/start_equity
            df.at[i, 'coin_value'] = stat_dict['coin_value']
    return df, stat_dict

