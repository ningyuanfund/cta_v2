import pandas as pd
import program.chen_ketong.strategy.timing_strategy.Signals as sigs
from datetime import timedelta
import os, warnings, itertools, numpy as np
from matplotlib import pyplot as plt, dates, font_manager


warnings.simplefilter(action='ignore', category=FutureWarning)
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)

# ====需要手动设置的参数====
strategy_name = 'turtle'  # 策略名称，即函数名中的策略名称
rule_type = '1H'  # 最优参数的时间级别
end_timedelta = timedelta(minutes=15)  # 根据不同的k线级别，这里的结束时间可能要加上不同的分钟数，15,30,60等。
pair = 'ETHUSD'
test_start_time = pd.to_datetime('2018-01-01')
data = 'local_data'
# ========================

# 生成表格的基础地址，最后带上策略名以区分不同策略生成的表格
output_path = 'E:\\re_test\\fifty-five-data\\output\\%s\\%s\\%s\\%s\\' % (strategy_name, pair, rule_type, data)

if not os.path.exists(output_path):
    os.makedirs(output_path)

# 生成输出表格的文件路径
equity_curve_path = output_path + r'equity_curve.csv'  # 回测结果。不需要展示，但是是生成后续表格的基础
record_path = output_path + r'record.csv'  # 不用于展示的交易记录
show_record_path = output_path + r'record_show.csv'  # 用于展示的交易记录
monthly_return_path = output_path + r'monthly_return_show.csv'  # 用于展示的每月交易记录
evaluation_path = output_path + r'evaluation_show.csv'  # 用于展示的策略评价
fig_path = output_path + r'fig.png'

# 读取资金曲线
df = pd.read_csv(equity_curve_path, parse_dates=['candle_begin_time'])
df = df[df['candle_begin_time'] >= test_start_time].copy()
df.reset_index(inplace=True, drop=True)


# ======= 生成交易记录 =========
i = 0
rtn_record = pd.DataFrame(columns=[['start_time', 'end_time', 'long_or_short', 'yield_rate']])
leverage = 3


def f1(x):
    start_time = x.iloc[0]['candle_begin_time']
    end_time = x.iloc[-1]['candle_begin_time']
    long_or_short = x.iloc[0]['pos']

    # 有可能某次交易开仓之后立刻平仓，或者在同一行里同时有开仓信号和平仓信号，需要单独处理，所以有len(x) = 1的情况
    if len(x) > 1:
        yield_rate = x.iloc[-1]['equity_curve']/(x.iloc[0]['equity_curve'] / (1 + x.iloc[0]['equity_change'])) - 1
    else:
        yield_rate = x.iloc[0]['equity_change']
    rtn_record.loc[start_time, 'start_time'] = start_time

    rtn_record.loc[start_time, 'end_time'] = pd.to_datetime(end_time) + end_timedelta
    rtn_record.loc[start_time, 'long_or_short'] = str(long_or_short)[:-2]
    rtn_record.loc[start_time, 'yield_rate'] = yield_rate


df.groupby('start_time').apply(f1)

# 生成可供网页展示的表格
rtn_record = rtn_record.reset_index(drop=True)
output_rtn_summary = rtn_record.copy()
output_rtn_summary.columns = ['开始时间', '结束时间', '做多或做空', '本次交易收益率']
output_rtn_summary['本次交易收益率'] = output_rtn_summary['本次交易收益率'].apply(lambda x: format(x, '.2%'))

output_rtn_summary.set_index('开始时间', inplace=True)

output_rtn_summary.to_csv(show_record_path, encoding='utf_8_sig')

rtn_record.to_csv(record_path)


# ======= 生成每月收益和策略评价 =========
def calc_index(equity, trade):
    """
    :param equity: 带资金曲线的df
    :param trade: 每笔交易的df
    :return:
    """
    # 新建一个dataframe保存回测指标
    results = pd.DataFrame()

    # 计算累积净值
    results.loc[0, '累积净值'] = round(equity['equity_curve'].iloc[-1], 2)

    # 计算年化收益
    annual_return = (equity['equity_curve'].iloc[-1] / equity['equity_curve'].iloc[0]) ** (
        '1 days 00:00:00' / (equity['candle_begin_time'].iloc[-1] - equity['candle_begin_time'].iloc[0]) * 365) - 1
    results.loc[0, '年化收益'] = str(round(annual_return*100, 2)) + ' %'

    # 计算最大回撤
    max_drawdown = (1 - equity['equity_curve'] / equity['equity_curve'].expanding().max()).max()
    results.loc[0, '最大回撤'] = format(max_drawdown, '.2%')

    # 年化收益/回撤比
    results.loc[0, '年化收益/回撤比'] = round(annual_return / max_drawdown, 2)

    # 计算持仓时间
    trade['持仓时间'] = trade['end_time'] - trade['start_time']
    results.loc[0, '盈利笔数'] = len(trade.loc[trade['yield_rate'] > 0])  # 盈利笔数
    results.loc[0, '亏损笔数'] = len(trade.loc[trade['yield_rate'] < 0])  # 亏损笔数
    results.loc[0, '胜率'] = format(len(trade.loc[trade['yield_rate'] > 0]) / len(trade), '.2%')  # 胜率
    results.loc[0, '每笔交易平均盈亏'] = format(trade['yield_rate'].mean(), '.2%')  # 每笔交易平均盈亏
    results.loc[0, '盈亏收益比'] = round(trade.loc[trade['yield_rate'] > 0]['yield_rate'].mean() / \
                              trade.loc[trade['yield_rate'] < 0][
                                  'yield_rate'].mean() * (-1), 2)  # 盈亏比
    results.loc[0, '单笔最大盈利'] = format(trade['yield_rate'].max(), '.2%')  # 单笔最大盈利
    results.loc[0, '单笔最大亏损'] = format(trade['yield_rate'].min(), '.2%')  # 单笔最大亏损

    max_days, max_seconds = trade['持仓时间'].max().days, trade['持仓时间'].max().seconds
    max_hours = max_seconds // 3600
    max_minute = (max_seconds - max_hours * 3600) // 60
    results.loc[0, '单笔最长持有时间'] = str(max_days) + ' 天 ' + str(max_hours) + ' 小时 ' + str(max_minute) + ' 分钟'  # 单笔最长持有时间

    min_days, min_seconds = trade['持仓时间'].min().days, trade['持仓时间'].min().seconds
    min_hours = min_seconds // 3600
    min_minute = (min_seconds - min_hours * 3600) // 60
    results.loc[0, '单笔最短持有时间'] = str(min_days) + ' 天 ' + str(min_hours) + ' 小时 ' + str(min_minute) + ' 分钟'  # 单笔最短持有时间

    mean_days, mean_seconds = trade['持仓时间'].mean().days, trade['持仓时间'].mean().seconds
    mean_hours = mean_seconds // 3600
    mean_minute = (mean_seconds - mean_hours * 3600) // 60
    results.loc[0, '平均持仓周期'] = str(mean_days) + ' 天 ' + str(mean_hours) + ' 小时 ' + str(mean_minute) + ' 分钟'  # 平均持仓周期

    results.loc[0, '最大连续盈利笔数'] = max(
        [len(list(v)) for k, v in itertools.groupby(np.where(trade['yield_rate'] > 0, 1, np.nan))])  # 最大连续盈利笔数
    results.loc[0, '最大连续亏损笔数'] = max(
        [len(list(v)) for k, v in itertools.groupby(np.where(trade['yield_rate'] < 0, 1, np.nan))])  # 最大连续亏损笔数

    # 计算每月收益率
    equity.set_index('candle_begin_time', inplace=True)
    monthly_return = equity[['equity_change']].resample(rule='M').apply(lambda x: (1 + x).prod() - 1)

    return results, monthly_return


equity = df.copy()
trade = pd.read_csv(record_path, parse_dates=['start_time', 'end_time'])

results, monthly_return = calc_index(equity, trade)

# == 将monthly return 转换为可在网页展示的样式
monthly_return.reset_index(inplace=True)
monthly_return['equity_change'] = monthly_return['equity_change'].apply(lambda x: format(x, '.2%'))
monthly_return['candle_begin_time'] = monthly_return['candle_begin_time'].dt.strftime('%Y/%m')

monthly_return.columns = ['月份', '收益率']
monthly_return.set_index('月份', inplace=True)

monthly_return.to_csv(monthly_return_path, encoding='utf_8_sig')

results = results.T.reset_index()
results.columns = ['指标', '结果']

results.to_csv(evaluation_path, index=False, encoding='utf_8_sig')


# ======= 绘制累计净值的图片 =========

# 读取最完整的那个回测df，要包含日期、开高收低、以及需要画图的数据
draw_df = df.copy()


draw_df['candle_begin_time'] = pd.to_datetime(draw_df['candle_begin_time'])
draw_df['candle_begin_time'] = draw_df['candle_begin_time'].apply(lambda x: dates.date2num(x)*1440)

draw_df = draw_df[['candle_begin_time', 'close', 'equity_curve']].copy()
data_mat = draw_df.as_matrix()

fig, ax = plt.subplots(figsize=(800 / 72, 400 / 72))  # 图片的大小，这个可以随便调，反正画出来的是矢量图

# =====下面这段是为了让横轴刻度显示时间，以及调整横轴刻度的格式
from matplotlib.ticker import Formatter
class MyFormatter(Formatter):
    def __init__(self, dates, fmt='%Y-%m-%d-%H:%M:%S'):
        self.dates = dates
        self.fmt = fmt

    def __call__(self, x, pos=0):
        'Return the label for time x at position pos'
        ind = int(np.round(x))
        # ind就是x轴的刻度数值，不是日期的下标

        return dates.num2date(ind / 1440).strftime(self.fmt)

formatter = MyFormatter(data_mat[:, 0])
ax.xaxis.set_major_formatter(formatter)

for label in ax.get_xticklabels():
    label.set_rotation(15)
    label.set_horizontalalignment('right')

draw_df.set_index('candle_begin_time', inplace=True)
# =====横轴可读调整结束

ax2 = ax.twinx()

close, = ax.plot(draw_df['close'].shift(1), color='#A50021')
equity_curve, = ax2.plot(draw_df['equity_curve'].shift(1), color='#0000CC')

myfont = font_manager.FontProperties(fname='C:/Windows/Fonts/msyh.ttf', size=20)  # 微软雅黑字体
plt.legend([close, equity_curve], [pair + '价格', '累计净值'], loc='upper left', prop=myfont)
plt.savefig(fig_path)

