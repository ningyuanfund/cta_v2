def real_time_signal_dual_thrust_with_stop_lose(now_pos, stop_lose_price, df, para=[200, 0.7, 0.7, 2, '15T']):

    '''
    N日High的最高价HH, N日Close的最低价LC;
    N日Close的最高价HC，N日Low的最低价LL;
    Range = Max(HH-LC,HC-LL)
    上轨 = 前一个 Open + upper_medium * Range
    下轨 = 前一个 Open + lower_medium * Range
    突破下轨，空仓开空，或多头平多开空
    突破上轨，空仓开多，或空头平空开多
    :param df:
    :param para:[rolling期数, 上轨距离参数, 下轨距离参数, 止损比例]
    :return:
    '''
    rolling_period = para[0]
    upper_open = para[1]
    lower_open = para[2]
    close_price = df.iloc[-1]['close']

    # 只截取必要的部分，减少计算量。稍微留一点冗余
    df = df[-(rolling_period + 3):].copy()

    # 计算 N 日最高价的最大值 HH
    df['hh'] = df['high'].rolling(rolling_period).max()
    # 计算 N 日收盘价的最小值 LC
    df['lc'] = df['close'].rolling(rolling_period).min()
    # 计算 N 日收盘价的最大值 HC
    df['hc'] = df['close'].rolling(rolling_period).max()
    # 计算 N 日最低价的最小值 LL
    df['ll'] = df['low'].rolling(rolling_period).min()


    # 计算 range，上下轨距离前一根k线开盘价的距离
    condition1 = (df['hh'] - df['lc']) > (df['hc'] - df['ll'])
    condition2 = (df['hh'] - df['lc']) <= (df['hc'] - df['ll'])

    # Range = Max(HH-LC,HC-LL)
    df.loc[condition1, 'range'] = df['hh'] - df['lc']
    df.loc[condition2, 'range'] = df['hc'] - df['ll']

    # 根据 range 计算上下轨
    df['upper'] = df['open'].shift() + upper_open * df['range']
    df['lower'] = df['open'].shift() - lower_open * df['range']

    # 现在是空头仓位
    if now_pos == -1:
        # 查看是否需要平空开多
        if close_price > df.iloc[-2]['upper']:
            target_pos = 1
        # 查看是否需要止损
        elif close_price > stop_lose_price:
            target_pos = 0
        else:
            target_pos = -1

    # 现在是多头仓位
    elif now_pos == 1:
        # 查看是否需要平多开空
        if close_price < df.iloc[-2]['lower']:
            target_pos = -1
        # 查看是否需要止损
        elif close_price < stop_lose_price:
            target_pos = 0
        else:
            target_pos = 1

    # 现在无仓位
    elif now_pos == 0:
        # 查看是否需要开多
        if close_price > df.iloc[-2]['upper']:
            target_pos = 1
        # 查看是否需要开空
        elif close_price < df.iloc[-2]['lower']:
            target_pos = -1
        else:
            target_pos = 0

    # 不可能有第四种情况
    else:
        raise ValueError('当前仓位变量now_pos只能是1， -1， 0，但目前是', now_pos)

    return target_pos


