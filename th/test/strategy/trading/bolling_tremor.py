# 带止损的震荡布林线策略
def real_time_signal_bolling_tremor_with_stop_lose(now_pos, stop_lose_price, df, para=[100, 2, 5]):
    """
    布林线中轨：n天收盘价的移动平均线
    布林线上轨：n天收盘价的移动平均线 + m * n天收盘价的标准差
    布林线上轨：n天收盘价的移动平均线 - m * n天收盘价的标准差

    收盘价下穿上轨做空，到中轨平仓。上穿下轨做多，到中轨平仓。
    当且仅当上一次触及了中轨，才判断下一次是否要开仓

    另外，当价格往亏损方向超过百分之stop_lose的时候，平仓止损。
    :param now_pos:
    :param stop_lose_price:
    :param df:
    :param para:
    :return:
    """
    # ===计算指标
    n = para[0]
    m = para[1]
    close_price = df.iloc[-1]['close']
    close_price_before = df.iloc[-2]['close']

    # 只截取必要的部分，减少计算量。稍微留一点冗余
    df = df[-(n + 3):].copy()

    # 计算均线
    df['median'] = df['close'].rolling(n).mean()

    # 计算上轨、下轨道
    df['std'] = df['close'].rolling(n).std(ddof=0)  # ddof代表标准差自由度
    df['upper'] = df['median'] + m * df['std']
    df['lower'] = df['median'] - m * df['std']

    # 现在是多头仓位
    if now_pos == 1:
        # 依次计算是否需要平仓、止损
        if close_price > df.iloc[-2]['median']:
            target_pos = 0
        elif close_price < stop_lose_price:
            target_pos = 0
        else:
            target_pos = 1

    # 现在是空头仓位
    elif now_pos == -1:
        # 依次计算是否需要平仓、止损
        if close_price < df.iloc[-2]['median']:
            target_pos = 0
        elif close_price > stop_lose_price:
            target_pos = 0
        else:
            target_pos = -1

    # 现在无仓位
    elif now_pos == 0:

        # 开多信号，上穿下轨
        condition1 = close_price_before < df.iloc[-2]['lower']
        condition2 = close_price > df.iloc[-2]['lower']

        # 开空信号，下穿上轨
        condition3 = close_price_before > df.iloc[-2]['upper']
        condition4 = close_price < df.iloc[-2]['upper']
        if condition1 & condition2:
            target_pos = 1
        elif condition3 & condition4:
            target_pos = -1
        else:
            target_pos = 0
    else:
        raise ValueError('当前仓位变量now_pos只能是1， -1， 0，但目前是', now_pos)

    return target_pos
