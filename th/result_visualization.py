# coding=utf-8

def miaomiaomiao():
    import pandas as pd
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    # ---------------------------------------------------------------------
    #csv文件格式：
    '''
    columns = ['return' 'period' 'K1' 'stpL' 'mov_stpLX' 'mov_stpLX_pct']
    
    '''
    # ---------------------------------------------------------------------
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    # 结果_参数地址

    df = pd.read_csv(r'D:\Work\back-trace\rtn.csv')
    print(df)
    # 控制止损，移动止盈倍数，移动止盈百分比，Y轴为期数，X为另一个变量
    # stpL = 3
    # mov_stpLX = 5
    # mov_stpLX_pct = 0.1
    # # 将满足条件的点纳入新的dateframe
    # col_name_list = df.columns.values
    # print(col_name_list)
    # df_xy = pd.DataFrame(columns=col_name_list)
    # for i in range(len(df)):
    #     condition01 = df.at[i,'stpL'] == stpL
    #     condition02 = df.at[i,'mov_stpLX'] == mov_stpLX
    #     condition03 = df.at[i,'mov_stpLX_pct'] == mov_stpLX_pct
    #     if condition02 and condition03 and condition01:
    #         index = len(df_xy)
    #         for col_name in col_name_list:
    #             df_xy.at[index,col_name] = df.at[i,col_name]
    Y = df['open_limit'].tolist()
    X = df['close_limit'].tolist()
    Z = df['return'].tolist()
    ax.scatter(X,Y,Z,c='y')
    ax.set_xlabel('close')
    ax.set_ylabel('open')
    ax.set_zlabel('return')
    plt.show()
    print(df)




miaomiaomiao()
# plt.show()