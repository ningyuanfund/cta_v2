# coding=utf-8
import re
import requests
import pandas as pd
import datetime
from matplotlib import pyplot as plt, dates, font_manager


def refresh_FG_index():
    # 伪装自己的身份
    headers = {'User-Agent': 'Mozilla/'
                            '5.0 (Windows NT 10.0; WOW64) AppleWebKit/'
                            '537.36 (KHTML, like Gecko) Chrome/'
                            '50.0.2661.102 Safari/537.36'}

    # 读取网站提供的所有历史指数
    r = requests.get('https://api.alternative.me/fng/?limit=999',
                     headers=headers)

    # 把获取的网页做一点整理
    # 把换行符，空格，制表符都删除
    r = r.text.replace('\n', '').replace(' ', '').replace('	', '')
    # 用正则表达式将上一步获得的文本形式的数据转化为列表
    data_list = re.findall(r'{(.+?)}', r)
    # 第一个数据格式不太对，稍微修改一下
    data_list[0] = data_list[0][len(r'"name":"FearandGreedIndex",'
                                    r'"data":[{'):]

    # 把列表中的信息放到dataframe里
    df = pd.DataFrame()
    for data in data_list:
        index = len(df)
        try:
            df.at[index, 'timestamp'] = int((re.findall('timestamp":"(.+?)"',
                                                       data)[0]))
            df.at[index, 'value'] = re.findall('value":"(.+?)"',
                                              data)[0]
            df.at[index, 'strtime'] = datetime.datetime.\
                utcfromtimestamp((df.at[index, 'timestamp'])).\
                strftime("%Y-%m-%d")
        except:
            print('skip!')
    # 保存在本地
    df.to_csv(r'D:\Work\cta_v2\th\Get_Fear&Greed_index\index.csv')
    return df

df = refresh_FG_index()
df.sort_values(by='timestamp',ascending=False,inplace= True)
print(df)

fig = plt.figure(figsize=(12,8))
ax = fig.add_subplot(111)
y = df['value']

ax.plot(X, y, 'o', label="FGI")
plt.show()
