# coding=utf-8
import pandas as pd
import os
from th.Factor import Functions as fct
import pickle
import statsmodels.api as sm
from matplotlib import pyplot as plt
import warnings
from dateutil.relativedelta import relativedelta
import time
global exchange


warnings.simplefilter(action='ignore', category=FutureWarning)
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)


def rank_to_position(rank,how='momentum'):
    '''

    :param rank: 分位数排名
    :param how: 反转还是动量
    :return:
    '''
    if how =='momentum':
        rank_dict = {2:1,0:-1}
    elif how == 'turn_over':
        rank_dict = {2:-1,0:1}

    position = rank_dict[rank]

    return position

def cal_rtn(last_price, now_price, last_position, now_position, c_rate):
    '''

    :param last_price:
    :param now_price:
    :param last_position:
    :param now_position:
    :param c_rate:
    :return:
    '''

    rtn = 0
    if last_position == None:
        if now_position != None:
            rtn = -c_rate  # 上期空仓本期持仓，说明支付了手续费和滑点

    if last_position == 1:
        if now_position == 1:
            adj_last_price = last_price * (1 + c_rate)
            rtn = now_price / adj_last_price - 1  # 此处没有调仓，收益率无需考虑手续费和滑点

        # 以下计算有点问题，动量时，倾向于高估费用，低估收益
        # 反转时，会低估费用，高估收益
        if now_position == None:
            adj_now_price = now_price * (1 - c_rate)
            adj_last_price = last_price * (1 + c_rate)
            rtn = adj_now_price / adj_last_price - 1  # 调仓了，需要支付手续费和滑点
        if now_position == -1:
            adj_now_price = now_price * (1 - c_rate)
            adj_last_price = last_price * (1 + c_rate)
            rtn = adj_now_price / adj_last_price - 1  # 调仓了，需要支付手续费和滑点
            rtn -= c_rate  # 两份操作，磨损+1

    if last_position == -1:
        if now_position == -1:
            adj_last_price = last_price * (1 - c_rate)
            rtn = -now_price / adj_last_price + 1  # 此处没有调仓，收益率无需考虑手续费和滑点
        if now_position == None:
            adj_now_price = now_price * (1 + c_rate)
            adj_last_price = last_price * (1 - c_rate)
            rtn = -adj_now_price / adj_last_price + 1  # 调仓了，需要支付手续费和滑点
        if now_position == 1:
            adj_now_price = now_price * (1 + c_rate)
            adj_last_price = last_price * (1 - c_rate)
            rtn = -adj_now_price / adj_last_price + 1  # 调仓了，需要支付手续费和滑点
            rtn -= c_rate  # 两份操作，磨损+1
    # print(rtn)
    return rtn


def gen_equity_curve(from_date, end_date, momentum_period, adj_period, quantile_info, mode, c_rate):

    data_path = r'D:\Work\MSZQ\data\hourlydata\\%s\\'%exchange
    coin_list = fct.eachFile(data_path)

    # 生成包含1h信息的dataframe
    df_all = pd.DataFrame(columns=['time'])
    for coin in coin_list:
        filetitle = coin
        coin = coin.split('_')[0]
        file_path = data_path + '%s.csv' % filetitle
        f = open(file_path)
        df_coin = pd.read_csv(f)
        df_coin.rename(columns={'close': coin, 'vol_to': '%s_amount' % coin}, inplace=True)
        df_coin.drop(labels=['pair_id', 'symbol', 'timestamp', 'open', 'high', 'low', 'vol_from', 'vol_from_d',
                             'vol_to_d'], axis=1, inplace=True)
        df_all = df_all.merge(df_coin, on='time', how='outer')
    df_all['time'] = pd.to_datetime(df_all['time'], format="%Y-%m-%d %H:%M:%S")
    df_all.sort_values(by='time', inplace=True)
    df_all.set_index('time', drop=True, inplace=True)


    # 计算观察期内每个币种的交易量
    df_all_columns_list = df_all.columns.values.tolist()
    for df_all_columns in df_all_columns_list:
        if 'amount' in df_all_columns:
            df_all['%s_rolling' % df_all_columns] = df_all[df_all_columns].rolling(momentum_period).sum()

    # 计算动量
    date_range = pd.date_range(from_date, end_date, freq=str(adj_period) + 'H')
    df_rtn = pd.DataFrame()
    for date in date_range:
        # 本期的index,动量计算期的index，调仓期的index
        date_str = str(date)
        mt_based_date_str = str(pd.to_datetime(date_str) - relativedelta(hours=momentum_period))
        last_adj_date = str(pd.to_datetime(date_str) - relativedelta(hours=adj_period))
        mt_df = pd.DataFrame()

        # 选取观察时满足交易量要求的币种组成新的df_all
        amount_dict = {}
        amount_list = []
        df_all_columns_list = df_all.columns.values.tolist()
        for df_all_columns in df_all_columns_list:
            if 'rolling' in df_all_columns:
                try:
                    sum = str(int(df_all.loc[date_str][df_all_columns]))
                    # print(sum, df_all_columns)
                    if int(sum) > 1000: # 至少大于1w美元
                        amount_dict[sum] = df_all_columns.split('_')[0]
                        amount_list.append(int(sum))

                except:
                    pass
        amount_list.sort(reverse=True)
        # print(amount_list)
        try:
            amount_list = amount_list[:30]
        except:
            pass # 不足30个

        qualifed_columns = []
        for i in amount_list:
            # print(len(amount_list))
            qualifed_columns.append(amount_dict[str(i)])
        df_all_qf = df_all[qualifed_columns]

        # print('qualified coins:', qualifed_columns, len(qualifed_columns))
        # 计算观察期每个币种的动量，并分组
        for coin in qualifed_columns:
            date_price = df_all_qf.loc[date_str][coin]
            mt_based_date_price = df_all_qf.loc[mt_based_date_str][coin]
            try:
                len0 = mt_df.shape[0]
                mt = (date_price - mt_based_date_price) / mt_based_date_price
                mt_df.at[len0, 'coin'] = coin
                mt_df.at[len0, 'mt'] = mt
            except:
                pass
        mt_group = mt_df['mt'].quantile(quantile_info)
        condition0 = mt_df['mt'] < mt_group[quantile_info[0]]
        condition1 = (mt_df['mt'] >= mt_group[quantile_info[0]]) & (mt_df['mt'] < mt_group[quantile_info[1]])
        condition2 = mt_df['mt'] >= mt_group[quantile_info[1]]
        condition21 = mt_df['mt'] > 0  # 收益必须大于0才做多
        mt_df.loc[condition0, 'mt_group'] = 0
        mt_df.loc[condition1, 'mt_group'] = 1
        mt_df.loc[condition2 & condition21, 'mt_group'] = 2

        # 找到满足条件的币种,计算date期仓位
        for coin in coin_list:
            coin = coin.split('_')[0]
            try:
                condition3 = mt_df['coin'] == coin
                group_rank = int(mt_df.loc[condition3, 'mt_group'])
                # print(group_rank)
                df_all.at[date, '%s_position' % coin] = rank_to_position(group_rank, how=mode)
                # print(df_all.at[date, '%s_position' % coin])
            except:
                pass


    for date in date_range:
        # 本期的index,动量计算期的index，调仓期的index
        date_str = str(date)
        mt_based_date_str = str(pd.to_datetime(date_str) - relativedelta(hours=momentum_period))
        last_adj_date = str(pd.to_datetime(date_str) - relativedelta(hours=adj_period))
        # 计算本期每个币种分配的资金比例
        short_counts = 0
        long_counts = 0
        for coin in coin_list:
            coin = coin.split('_')[0]
            try:
                coin_position = df_all.loc[date, '%s_position' % coin]
                if coin_position == -1:
                    short_counts += 1
                elif coin_position == 1:
                    long_counts += 1
                    # print(date, coin, coin_position)
            except:
                pass
        # print(long_counts)
        # 计算本期收益，考虑调仓次数
        agg_rtn = 0
        short_rtn = 0
        long_rtn = 0
        for coin in coin_list:
            coin = coin.split('_')[0]
            try:
                last_price = df_all.loc[last_adj_date, coin]
                now_price = df_all.loc[date, coin]
                last_position = df_all.loc[last_adj_date, '%s_position' % coin]
                now_position = df_all.loc[date, '%s_position' % coin]
                if now_position or last_price != None:
                    if last_position == -1:
                        coin_rtn = cal_rtn(last_price, now_price, last_position, now_position, c_rate)
                        # print(coin_rtn)
                        short_rtn += coin_rtn
                    elif last_position == 1:
                        coin_rtn = cal_rtn(last_price, now_price, last_position, now_position, c_rate)
                        # print(coin_rtn)
                        long_rtn += coin_rtn
                    # print(date,coin,coin_rtn)
            except:
                pass
        # short_rtn = short_rtn / short_counts
        if long_counts > 0:
            long_rtn = long_rtn / long_counts
        else:
            long_rtn = -0.00001


        # ----------------------------------# 多空双开
        # rtn = (short_rtn + long_rtn) / 2

        # ---------------------------------# 不做空，只做多--------
        rtn = long_rtn
        # --------------------------------------------------------

        df_rtn.at[date, 'rtn'] = rtn
    df_rtn.reset_index(inplace=True)
    df_rtn.at[0, 'equity'] = 1
    for i in range(len(df_rtn)):
        if i > 0:
            df_rtn.at[i, 'equity'] = df_rtn.at[i-1, 'equity']*(1+df_rtn.at[i,'rtn'])

    df_rtn.rename(columns={'equity': str(from_date) + '_equity'}, inplace=True)
    # df_rtn.reset_index(inplace=True)
    df_rtn.rename(columns={'index': 'time'}, inplace=True)
    return df_rtn

'''
momentum_period = 5
adj_period = 5
quantile_info = (0.3, 0.7)
mode = 'turn_over'
c_rate = 0.003
'''






def main(para, from_date, end_date,quantile_info, mode, c_rate):



    momentum_period = para[0]
    adj_period = para[0]
    change_time = para[1]

    # 计算出包含开始时点和结束时点的列表
    period_counts = int(momentum_period/change_time)
    real_start_time = pd.to_datetime(from_date) + relativedelta(hours=momentum_period)
    real_start_list0 = pd.date_range(from_date, real_start_time, freq=str(change_time) + 'H')[:-1]

    # print(real_start_list0,len(real_start_list0),period_counts)

    df_all = pd.DataFrame(index=pd.date_range(from_date,end_date,freq='1H'))
    df_all.reset_index(inplace=True)
    df_all.rename(columns={'index': 'time'}, inplace=True)


    for real_start_date in real_start_list0:
        df = gen_equity_curve(real_start_date, end_date,momentum_period,adj_period,quantile_info, mode, c_rate)
        df_all = df_all.merge(df, on='time', how='outer')
    df_all.fillna(method='ffill',inplace=True)
    df_all.fillna(method='bfill',inplace=True)
    # print(df_all)
    df_colums_list = df_all.columns.values.tolist()

    df_all['average_nv'] = 0
    equity_counts = 0
    for colums in df_colums_list:
        if 'equity' in colums:
            equity_counts += 1
            df_all['average_nv'] = df_all['average_nv'] + df_all[colums]
    print(df_all)
    df_all['average_nv'] = df_all['average_nv']/equity_counts
    df_btc = pd.read_csv(r'D:\Work\MSZQ\data\hourlydata\Bitfinex\BTC_USD.csv')
    df_btc = df_btc[['time', 'close']]
    df_btc['time'] = pd.to_datetime(df_btc['time'], format="%Y-%m-%d %H:%M:%S")
    print(df_btc)
    df_all = df_all.merge(df_btc, on='time', how='inner')
    df_all = df_all[['time', 'average_nv', 'close']]
    df_all.reset_index(inplace=True, drop=False)
    df_all['rlt'] = df_all['close']/df_all.at[0, 'close']
    print(df_all)
    # 作图
    x = df_all['time']
    y = df_all['average_nv']
    #
    y1 = df_all['rlt']

    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(111)
    ax.plot(x, y, 'b-', label='equity')
    ax.plot(x, y1, 'r-', label='BTC_rlt')
    ax.legend(loc="best")

    title = '%s_%s_%s_moment_period_%s_change_period%s_c_rate_%s' % (mode.upper(), from_date.replace(':', '-'), end_date.replace(':', '-'), str(momentum_period), str(change_time),str(c_rate))
    plt.title(title)
    pigpath = r'D:\Work\民生证券\通证的因子分析\result_curve\%s\%s_%s\long_larger_than0_valuetop30_c0.3\\' % (exchange, from_date.replace(':', '-'),end_date.replace(':', '-'))
    if not os.path.exists(pigpath):
        os.makedirs(pigpath)
    plt.savefig(pigpath+'%s.png' % title)
    plt.close()
    # exit()




from_date = '2019-01-01 10:00:00'
end_date = '2019-04-19 22:00:00'
quantile_info = (0.3, 0.7)
mode = 'momentum'
exchange = 'Coinbase'

# 回测用的参数列表
# 例如，持仓时间是168小时，调仓频率为24小时。
# 那么就相当于把开始时将资金拆分成7个账户，在t时刻使用该账户剩余资金，根据t-168到t的动量来调仓。
# 账户之间不共享资金。
para_dict = ['持仓时间', '调仓频率']
hold_time_list = [168, 144, 96, 48]
change_time_list = [168]
para_list = []

for hold_time in hold_time_list:
    for change_time in change_time_list:
        para_list.append([hold_time, change_time])

for para in para_list:
        main(para, from_date, end_date, quantile_info, mode, c_rate=0.003)