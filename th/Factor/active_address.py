from statsmodels.regression.quantile_regression import QuantReg
from matplotlib.ticker import FuncFormatter
import pandas as pd
import statsmodels.api as sm
from matplotlib import pyplot as plt
import warnings
import re

warnings.simplefilter(action='ignore', category=FutureWarning)
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)


coin = 'eth'
data_path = r'D:\Work\back-trace\inputdata\factor_data\%s.csv' %(coin)
df = pd.read_csv(data_path)

df = df[['date','price(USD)','activeAddresses']]

df['date'] = pd.to_datetime(df['date'])

# df = df.resample(rule='1W',on='date',label='left').agg({'price(USD)': 'last', 'activeAddresses': 'mean'})

df['rtn'] = df['price(USD)'].diff()/df['price(USD)'].shift()
df['act_add_changement'] = df['activeAddresses'].diff()/df['activeAddresses'].shift()

# df['act_add_changement'] = df['act_add_changement'].shift(-1)

df.dropna(how='any',inplace=True)
X = df['act_add_changement']
Y = df['rtn']

df_reg = pd.DataFrame()

for i in range(1236)[1:]:
    x = X[i:i+300]
    y = Y[i:i+300]
    RLM = sm.RLM(y, sm.add_constant(x), M=sm.robust.norms.HuberT()).fit()
    RLM.summary()
    regresult = str(RLM.summary())
    # print(regresult)
    try:
        result_contst = re.findall('const                  (.+?)      ',regresult)[0]
        result_factor = re.findall('act_add_changement     (.+?)\n',regresult)[0].split(' ')
        real_list = []
        for ele in result_factor:
            if len(ele)>0:
                real_list.append(ele)
        result = real_list
        date_begin = str(df.at[i, 'date'])
        date_end = str(df.at[i + 300, 'date'])
        index = date_begin + '_' + date_end
        df_reg.loc[index, 'coef'] = result[0]
        df_reg.loc[index, 'P'] = result[3]
        df_reg.loc[index, 'const'] = result_contst

    except:
        print('next!')

df_reg.to_csv('D:\Work\民生证券\通证的因子分析\\result.csv')
print(df_reg)