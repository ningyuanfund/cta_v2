# coding=utf-8
import pandas as pd
import os
import matplotlib.ticker as ticker
import pickle
# import statsmodels.api as sm
from matplotlib import pyplot as plt
import warnings
from sqlalchemy import create_engine


warnings.simplefilter(action='ignore', category=FutureWarning)
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)


def eachFile(filepath):
    pathDir =  os.listdir(filepath)
    coin_list=[]
    for allDir in pathDir:
        child = os.path.join('%s%s' % (filepath, allDir))
        coin_list.append(allDir.split('.')[0])
        # print(child) # .decode('gbk')是解决中文显示乱码问题
    return coin_list

# print(eachFile('D:\Work\民生证券\通证的因子分析\datafromcoinmetrics\\all\\'))
def del_file(path):
    ls = os.listdir(path)
    for i in ls:
        c_path = os.path.join(path, i)
        if os.path.isdir(c_path):
            del_file(c_path)
        else:
            os.remove(c_path)

# 清洗数据，排除空值
def wash_data(coin_list):
    for coin in coin_list:
        path = r'D:\Work\民生证券\通证的因子分析\datafromcoinmetrics\all\\%s.csv' % (coin)
        f=open(path)
        df = pd.read_csv(f)
        len0 = len(df)
        df.dropna(how='any',inplace=True)
        df.reset_index(inplace=True)
        len1 = len(df)
        print('%s delete %s line(s)'%(coin,str(len0-len1)))
        df.to_csv(path)


# pickle0 = pickle.load(open(r'D:\Work\TZT研究院\小币种数据20190403\result.bin', 'rb'))    # 截至20190329的各币种市值与排名数据
# cryptos = pickle.load(open(r'D:\Work\TZT研究院\小币种数据20190403\cryptos.bin', 'rb'))  # 币种编号


def seek_day_rank(pickle0,rank=20,date='2017-01-12'):
    '''
    找到指定日期市值排名的币，若该排名是稳定币，则顺延至下一位,在该天的排名种，每次都要往后推延
    :return:str
    '''

    down = 0 #由于碰到了稳定币而向后推的位数

    def seek_rank_include_stable(pickle0,rank,date):
        for index in cryptos: #从币表中取出一个币的编号，准备进行遍历
            if index != None:
                coin_index = index[0]
                coin_name = index[1]
                coin_date_info = pickle0[coin_index]
                if coin_date_info != None:
                    for i in coin_date_info: #读取pickle所有该币的数据,检查这个币在当天的排名是否满足要求
                        if len(i) > 0:
                            i_date = str(i[0])[:10]
                            i_rank = i[-1]
                            condition0 = (date == i_date)
                            condition1 = (rank == i_rank)
                            if condition0:
                                if condition1:
                                    return coin_name
                                else:
                                    break
                        else:
                            pass
                else:
                    pass
            else:
                pass

    seeked_result = seek_rank_include_stable(pickle0,rank,date) # 这是可能包含稳定币的结果
    stable_coin_list = ['USDT', 'GUSD', 'PAX', 'USDC', 'DAI']     # 稳定币列表
    rank_copy = rank
    while True:
        if seeked_result in stable_coin_list:
            rank_copy += 1
            down+=1
            seeked_result = seek_rank_include_stable(pickle0,rank_copy,date)
            adj_rank = rank - down
        else:
            adj_rank = rank - down
            return seeked_result,down,adj_rank


def seek_price_from_cmt(loc=r'D:\Work\民生证券\通证的因子分析\datafromcoinmetrics\all\\',date='2017-01-12',coin='btc'):
    '''

    :param loc:
    :param date:
    :param coin:
    :return:
    '''
    path = loc+coin+'.csv'
    f = open(path)
    df = pd.read_csv(f)
    condition0 = df['date'] == date
    price = list(df.loc[condition0,'price(USD)'])[0]
    # print(price)
    return price



def updated_local_from_database(password = 'postgresql://postgres:thomas@119.28.89.58:5432/multicoin',filepath='D:\Work\民生证券\通证的因子分析\\bgf_data\\'):
    engine = create_engine(password)
    sql = "select tablename from pg_tables where schemaname='public'"
    df_tables = pd.read_sql(sql, con=engine)
    exist_file_list = eachFile(filepath)
    should_file_list = list(df_tables['tablename'])

    for should_file in should_file_list:
        if should_file not in exist_file_list:
            print('fetching file %s ....'%(should_file))
            data_name = should_file
            sql = 'SELECT * FROM public."%s"'%(data_name)
            df = pd.read_sql_query(sql, engine)
            df.to_csv(filepath+'%s.csv'%(should_file.replace('/', '_')))


def merge_to_one(coin,filepath='D:\Work\民生证券\通证的因子分析\\bgf_data\\'):

    df_all = pd.DataFrame()
    exist_file_list = eachFile(filepath)
    for exist_file in exist_file_list:
        if coin in exist_file:
            f= open(filepath+'%s.csv'%(exist_file))
            df = pd.read_csv(f)
            df_all = pd.concat([df,df_all])
    df_all.sort_values(by='candle_begin_time',inplace=True)
    df_all.drop_duplicates(subset=['candle_begin_time'],keep='first',inplace=True)
    df_all.reset_index(inplace=True)
    df_all.rename(columns={'candle_begin_time':'date', 'volume':'marketcap(USD)', 'close':'price(USD)'}, inplace = True)
    df_all = df_all[['date','marketcap(USD)','price(USD)']]
    df_all['date'] = pd.to_datetime(df_all['date'],format="%Y-%m-%d %H:%M:%S")
    df_all=df_all.resample(rule='1h',on='date',label='left',closed='left').agg({'marketcap(USD)': 'first',
                                                                    'price(USD)': 'first'})
    df_all=df_all.fillna(method='ffill')
    print(df_all)
    df_all.to_csv(filepath+'%s.csv'%(coin.lower()))

# for coin in ['bat', 'bch', 'bsv', 'btc', 'btg', 'dash', 'eos', 'etc', 'eth', 'ltc', 'neo', 'omg', 'xmr', 'xrp', 'zec']:
#     try:
#         merge_to_one(coin.upper())
#     except:
#         pass

def rank_to_position(rank,how='momentum'):
    '''

    :param rank: 分位数排名
    :param how: 反转还是动量
    :return:
    '''
    if how =='momentum':
        rank_dict = {2:1,0:-1}
    elif how == 'turn_over':
        rank_dict = {2:-1,0:1}

    position = rank_dict[rank]

    return position

def cal_rtn(last_price, now_price, last_position, now_position, c_rate):
    '''

    :param last_price:
    :param now_price:
    :param last_position:
    :param now_position:
    :param c_rate:
    :return:
    '''

    rtn = 0
    if last_position == None:
        if now_position != None:
            rtn = -c_rate  # 上期空仓本期持仓，说明支付了手续费和滑点

    if last_position == 1:
        if now_position == 1:
            adj_last_price = last_price * (1 + c_rate)
            rtn = now_price / adj_last_price - 1  # 此处没有调仓，收益率无需考虑手续费和滑点

        # 以下计算有点问题，动量时，倾向于高估费用，低估收益
        # 反转时，会低估费用，高估收益
        if now_position == None:
            adj_now_price = now_price * (1 - c_rate)
            adj_last_price = last_price * (1 + c_rate)
            rtn = adj_now_price / adj_last_price - 1  # 调仓了，需要支付手续费和滑点
        if now_position == -1:
            adj_now_price = now_price * (1 - c_rate)
            adj_last_price = last_price * (1 + c_rate)
            rtn = adj_now_price / adj_last_price - 1  # 调仓了，需要支付手续费和滑点
            rtn -= c_rate  # 两份操作，磨损+1

    if last_position == -1:
        if now_position == -1:
            adj_last_price = last_price * (1 - c_rate)
            rtn = -now_price / adj_last_price + 1  # 此处没有调仓，收益率无需考虑手续费和滑点
        if now_position == None:
            adj_now_price = now_price * (1 + c_rate)
            adj_last_price = last_price * (1 - c_rate)
            rtn = -adj_now_price / adj_last_price + 1  # 调仓了，需要支付手续费和滑点
        if now_position == 1:
            adj_now_price = now_price * (1 + c_rate)
            adj_last_price = last_price * (1 - c_rate)
            rtn = -adj_now_price / adj_last_price + 1  # 调仓了，需要支付手续费和滑点
            rtn -= c_rate  # 两份操作，磨损+1
    # print(rtn)
    return rtn


def cal_VaR(df_data, freq, alpha, method='histroy'):
    df_data['time'] = df_data['candle_begin_time'].apply(lambda x: str(x)[:10])
    df_data['candle_begin_time'] = pd.to_datetime(df_data['candle_begin_time'])
    df_data = df_data.resample(rule=freq,
                               on='candle_begin_time',
                               label='left',
                               closed='left').agg({'open': 'first',
                                                   'high': 'max',
                                                   'low': 'min',
                                                   'close': 'last',
                                                   'volume': 'sum',
                                                   'equity_curve': 'last',
                                                   'time': 'last'})
    df_data['diff_pct'] = df_data['equity_curve'].diff()/df_data['equity_curve'].shift()
    df_data.reset_index(inplace=True)
    def get_var(df_data, alpha):
        alpha_dict = df_data[df_data['diff_pct'] != 0]['diff_pct'].quantile((alpha))
        return float(alpha_dict)
    df_1 = df_data.copy()
    for index in range(len(df_data)):
        if index > 101:
            df = df_1.iloc[index-100:index]
            # print(get_var(df, 0.01))
            df_data.at[index, 'VaR_0.01'] = min(get_var(df, 0.01), -0.00001)

    for index in range(len(df_data)):
        if index > 101:
            df = df_1.iloc[index-100:index]
            # print(get_var(df, 0.05))
            df_data.at[index, 'VaR_0.05'] = min(get_var(df, 0.05), -0.00001)

    df_data.fillna(method='ffill', inplace=True)


    # aim = 0.1
    # df_data['position'] = df_data['VaR'].apply(lambda x: min(-aim/x, 3))
    # data1 = df_data[df_data['diff_pct'] != 0]['diff_pct']
    # plt.hist(data1, label='log_cta_return', histtype='stepfilled', color='b', alpha=0.3, bins=30)

    data3 = df_data['diff_pct']
    # data4 = df_data['position']
    data2 = df_data['VaR_0.05']
    data5 = df_data['VaR_0.01']

    data_time = pd.to_datetime(df_data['candle_begin_time'])
    fig, ax = plt.subplots(1, 1)
    ax.plot(data_time, data2,color='r')
    ax.plot(data_time,data3,color='b')
    ax.plot(data_time,data5,color='y')

    # ax.xaxis.set_major_locator(ticker.MultipleLocator(int(len(data_time) / 10)))
    plt.xticks(rotation=45)
    plt.show()
    print(df_data)


# filename = r'D:\Work\cta_v2_backup\fifty-five-data\input\1_1equity_curve.csv'
# freq = '1D'
# alpha = 0.05
# df_data = pd.read_csv(filename, index_col=0)

# cal_VaR(df_data, freq, alpha, method='histroy')