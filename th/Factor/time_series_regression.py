# coding=utf-8
import pandas as pd

pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)


pair = 'BTCUSD'
rule_type = '15T'

# 历史交易数据
df = pd.read_hdf(r'D:\Work\cta_v2\fifty-five-data\input\%s.h5' % pair, key=rule_type)
df = df[df['candle_begin_time'] >= pd.to_datetime('2018-02-01')]

# 将获取的指数填写到交易历史数据中
index_all = pd.read_csv(r'D:\Work\cta_v2\th\Get_Fear&Greed_index\index.csv')
index_all['candle_begin_time'] = pd.to_datetime(index_all['timestamp'], unit='s')
df = pd.merge(df, index_all, how='left', on='candle_begin_time')

df = df.resample(rule='1D',
                on='candle_begin_time',
                label='left',
                closed='left').agg({'open': 'first',
                                    'high': 'max',
                                    'low': 'min',
                                    'close': 'last',
                                    'volume': 'sum',
                                    'value': 'first'})

df.fillna(method='ffill', inplace=True)
df.fillna(method='bfill', inplace=True)

df['change_rate'] = df['close'].diff(3)/df['close']
value_std = df['value'].std()
value_mean = df['value'].mean()
df['std_value'] = (df['value'] - value_mean)/value_std
df.reset_index(inplace=True)
df.to_csv(r'D:\Work\cta_v2\th\Get_Fear&Greed_index\result.csv')
print(df)

