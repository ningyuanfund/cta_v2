
import pickle
import pandas as pd
import statsmodels.api as sm
from matplotlib import pyplot as plt
import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)


result = pickle.load(open(r'D:\Work\TZT研究院\小币种数据20190403\result.bin', 'rb'))    # 截至20190329的各币种市值与排名数据
cryptos = pickle.load(open(r'D:\Work\TZT研究院\小币种数据20190403\cryptos.bin', 'rb'))  # 币种编号

# 整理每个币种每天的排名
column_list = []
for i in cryptos:     # i：(1, 'BTC', 'Bitcoin', '', 0.0, 0.0, '')
    if i != None:
        column_list.append(str(i[0])+'_'+i[1])

df_name = locals()

# 生成所有币种每天的排名dataframe，index为时间戳，column为币种名称，再按照时间戳merge
column_list_real = []
for i in column_list:
    index = int(i.split('_')[0])
    cypto_name = i.split('_')[1]
    name = 'df_'+cypto_name
    print(index,name)

    df_name[name] = pd.DataFrame(columns=[cypto_name])    # 创建包含某币种数据的dataframe
    cypto_data = result[index]

    try:
        for info in cypto_data:
            time_stamp = info[0]
            rank = info[2]

            df_name[name].loc[str(time_stamp),cypto_name] = int(rank)

        df_name[name].reset_index(inplace=True)
        df_name[name].rename(columns={'index': 'timestamp'},inplace=True)
        # print(type(df_name[name]))
        column_list_real.append(name)  # 生成有效的币种列表

    except Exception as e:
        print(e)

df_rank_daily = df_BTC
for name in column_list_real:
    try:
        df_rank_daily = pd.merge(df_rank_daily, df_name[name], on='timestamp', how='outer')
    except Exception as e:
        print(e)

df_rank_daily.to_csv(r'D:\Work\TZT研究院\小币种数据20190403\rank_daily.csv')
print(df_rank_daily)



