# coding=utf-8
import pandas as pd
import os
from th.Factor import Functions as fct
import pickle
# import statsmodels.api as sm
from matplotlib import pyplot as plt
import warnings
from dateutil.relativedelta import relativedelta
import time
global exchange, volume_rank
import random

warnings.simplefilter(action='ignore', category=FutureWarning)
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)


def random_color():
    color_arr = ['1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
    color = ""
    for i in range(6):
        color += color_arr[random.randint(0, 14)]
    return "#" + color

def gen_relative_graph(df_rtn, momentum_period, change_time,c_rate,group_code_list):

    # 文件名
    pigpath = r'D:\Work\民生证券\通证的因子分析\result_curve_delicacy\%s\%s_%s\long_larger_than0_valuetop%s_over%s\\' % (
        exchange, from_date.replace(':', '-'), end_date.replace(':', '-'), str(volume_rank), str(momentun_limit))
    if not os.path.exists(pigpath):
        os.makedirs(pigpath)
    title = '%s_%s_%s_moment_period_%s_change_period%s_c_rate_%s' % (
        mode.upper(), from_date.replace(':', '-'), end_date.replace(':', '-'), str(momentum_period), str(change_time),
        str(c_rate))
    df_rtn.to_csv(pigpath+'%s组%s.csv' % (str(len(group_code_list)),title))


    df_btc = pd.read_csv(r'D:\Work\MSZQ\data\hourlydata\Bitfinex\BTC_USD.csv')
    df_btc.rename(columns={'time': 'date'}, inplace=True)
    df_btc = df_btc[['date', 'close']]
    df_btc['date'] = pd.to_datetime(df_btc['date'], format="%Y-%m-%d %H:%M:%S")
    df_rtn.reset_index(inplace=True)
    df_rtn['date'] = pd.to_datetime(df_rtn['date'], format="%Y-%m-%d %H:%M:%S")
    df_rtn = df_rtn.merge(df_btc, on='date', how='inner')
    df_rtn.reset_index(inplace=True)
    df_rtn['BTC_rtn'] = df_rtn['close']/df_rtn.at[0, 'close']

    # 分组作图
    ybtc = df_rtn['BTC_rtn']
    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(111)
    x = df_rtn['date']
    ax.plot(x, ybtc, 'r-', label='BTC_rlt')
    names = locals()
    for group_code in group_code_list:
        names['y_%s' % str(group_code)] = df_rtn['net_value%s' % str(group_code)]
        ax.plot(x, names['y_%s' % str(group_code)],color= random_color(),label='y_%s' % str(group_code))
    ax.legend(loc="best")


    plt.title(title)
    plt.savefig(pigpath + '%s组%s.png' % (str(len(group_code_list)),title))
    plt.close()
    print(df_rtn)



# 采取逐小时根据持仓量和方向计算收益的方式生成净值曲线，最大限度模拟实盘
def gen_equity_df(from_date, end_date, momentum_period, quantile_info, mode, c_rate, momentun_limit,group_code):

    data_path = r'D:\Work\MSZQ\data\hourlydata\\%s\\' % exchange
    coin_list = fct.eachFile(data_path)

    # 生成包含  1h信息的dataframe
    df_all = pd.DataFrame(columns=['time'])
    for coin in coin_list:
        filetitle = coin
        coin = coin.split('_')[0]
        file_path = data_path + '%s.csv' % filetitle
        f = open(file_path)
        df_coin = pd.read_csv(f)
        df_coin.rename(columns={'close': coin, 'vol_to': '%s_amount' % coin}, inplace=True)
        df_coin.drop(labels=['pair_id', 'symbol', 'timestamp', 'open', 'high', 'low', 'vol_from', 'vol_from_d',
                             'vol_to_d'], axis=1, inplace=True)
        df_all = df_all.merge(df_coin, on='time', how='outer')
    df_all['time'] = pd.to_datetime(df_all['time'], format="%Y-%m-%d %H:%M:%S")
    # df_all = df_all[df_all['time']>=pd.to_datetime(from_date)][df_all['time']<=pd.to_datetime(end_date)]
    df_all['time'] = df_all['time'].apply(lambda x: str(x))
    df_all.sort_values(by='time', inplace=True)
    df_all.set_index('time', drop=True, inplace=True)

    # 计算观察期内每个币种的交易量
    df_all_columns_list = df_all.columns.values.tolist()
    for df_all_columns in df_all_columns_list:
        if 'amount' in df_all_columns:
            df_all['%s_rolling' % df_all_columns] = df_all[df_all_columns].rolling(momentum_period).sum()
    # print(df_all[-200:])
    # 计算动量，以及每期应当持有的币种
    hold_info = {}
    date_range = pd.date_range(from_date, end_date, freq=str(momentum_period) + 'H')
    for date in date_range:
        print(date)
        # 本期的index,动量计算期的index，调仓期的index
        date_str = str(date)
        mt_based_date_str = str(pd.to_datetime(date_str) - relativedelta(hours=momentum_period))
        last_adj_date = str(pd.to_datetime(date_str) - relativedelta(hours=momentum_period))
        mt_df = pd.DataFrame()

        # 选取观察时满足交易量要求的币种组成新的df_all
        amount_dict = {}
        amount_list = []
        df_all_columns_list = df_all.columns.values.tolist()
        for df_all_columns in df_all_columns_list:
            if 'rolling' in df_all_columns:
                try:
                    sum = str(int(df_all.loc[date_str][df_all_columns]))
                    print(df_all_columns,sum)
                    # print(sum, df_all_columns)
                    if int(sum) > 100:  # 至少大于1w美元
                        amount_dict[sum] = df_all_columns.split('_')[0]
                        amount_list.append(int(sum))

                except Exception as e:
                    print(e)

        amount_list.sort(reverse=True)
        print(amount_list)
        try:
            amount_list = amount_list[:volume_rank]
        except:
            pass  # 不足30个

        qualifed_columns = []
        for i in amount_list:
            # print(len(amount_list))
            qualifed_columns.append(amount_dict[str(i)])
        # print(qualifed_columns)
        # print(df_all.columns.values.tolist())
        df_all_qf = df_all[qualifed_columns]

        print('qualified coins:', qualifed_columns, len(qualifed_columns))
        # 计算观察期每个币种的动量，并分组,计算每期应当持有的币种
        for coin in qualifed_columns:
            date_price = df_all_qf.loc[date_str][coin]
            mt_based_date_price = df_all_qf.loc[mt_based_date_str][coin]
            try:
                len0 = mt_df.shape[0]
                mt = (date_price - mt_based_date_price) / mt_based_date_price
                mt_df.at[len0, 'coin'] = coin
                mt_df.at[len0, 'mt'] = mt
            except:
                pass

        mt_group = mt_df['mt'].quantile(quantile_info)

        try:
            condition5 = mt_df['mt'] > momentun_limit  # 收益必须大于2%才做多
            condition0 = mt_df['mt'] < mt_group[quantile_info[0]]
            condition_counts=1
            condition1 = (mt_df['mt'] >= mt_group[quantile_info[0]]) & (mt_df['mt'] < mt_group[quantile_info[1]])
            condition_counts=2
            condition2 = (mt_df['mt'] >= mt_group[quantile_info[1]]) & (mt_df['mt'] < mt_group[quantile_info[2]])
            condition_counts=3
            condition3 = (mt_df['mt'] >= mt_group[quantile_info[2]]) & (mt_df['mt'] < mt_group[quantile_info[3]])
            condition_counts=4
            condition4 = mt_df['mt'] >= mt_group[quantile_info[3]]
        except:
            pass

        try:
            mt_df.loc[condition0 & condition5, 'mt_group'] = 0
            mt_df.loc[condition1 & condition5, 'mt_group'] = 1
            mt_df.loc[condition2 & condition5, 'mt_group'] = 2
            mt_df.loc[condition3 & condition5, 'mt_group'] = 3
            mt_df.loc[condition4 & condition5, 'mt_group'] = 4
        except:
            mt_df.loc[(mt_df['mt'] >= mt_group[quantile_info[condition_counts-1]]) & condition5,'mt_group'] = condition_counts
            pass
        # 得出本期应当持多的币种列表

        coin_should_hold = []
        for i in range(mt_df.shape[0]):
            if mt_df.at[i, 'mt_group'] == group_code:
                coin_should_hold.append(mt_df.at[i, 'coin'])
        hold_info[str(date)] = coin_should_hold

    # 每期期末，根据下期的持仓字典，调整持仓，计算当前资产价值
    USD_start = 100000   # 初始一万美元
    df_rtn = pd.DataFrame({'date': list(hold_info.keys())})
    df_rtn.at[0, 'equity'] = USD_start
    df_rtn.at[0, 'USD'] = 0
    df_rtn.set_index(keys='date', drop=True, inplace=True)
    print(df_rtn)
    for date_key in hold_info:
        hold_coin_list = hold_info[date_key]
        # 如果是第一行，初始化各币种数量
        if date_key == from_date:
            if len(hold_coin_list) == 0:
                hold_coin_list = ['BTC']
                hold_info[date_key] = ['BTC']
            print('start')
            usd_share = USD_start/len(hold_coin_list)  # 全仓干
            # print(usd_share)
            # 计算每个币种买多少个
            # print(hold_coin_list)
            for hold_coin in hold_coin_list:
                df_rtn.at[date_key, hold_coin] = usd_share/df_all.at[date_key, hold_coin]*(1-c_rate)

            # 计算当期资产总值
            all_rights = 0
            for hold_coin in hold_coin_list:
                all_rights += df_rtn.at[date_key, hold_coin] * df_all.at[date_key, hold_coin]
            df_rtn.at[date_key, 'equity'] = all_rights
            print(df_rtn)

        else:
            # ------------调仓-------------------
            # 计算总价值，用于rebalance
            last_adj_date = str(pd.to_datetime(date_key) - relativedelta(hours=momentum_period))
            last_hold_coin_list = hold_info[last_adj_date]
            all_rights = df_rtn.at[last_adj_date, 'USD']

            if len(last_hold_coin_list) > 0:
                for hold_coin in last_hold_coin_list:
                    print(hold_coin, df_all.at[date_key, hold_coin], df_all.at[last_adj_date, hold_coin])
                    all_rights += df_rtn.at[last_adj_date, hold_coin] * df_all.at[date_key, hold_coin]
                    print(all_rights)
            else:
                all_rights = df_rtn.at[last_adj_date, 'equity']
            print('all_rights:', all_rights)

            sell_dict = {}
            buy_dict = {}

            print(hold_coin_list)
            if len(hold_coin_list) > 0:
                usd_share = all_rights/len(hold_coin_list)
            else:
                usd_share = df_rtn.at[last_adj_date, 'equity']

            # 先计算减仓币种
            for coin in last_hold_coin_list:
                if coin not in hold_coin_list:
                    sell_dict[coin] = df_rtn.at[last_adj_date, coin]
                    df_rtn.at[date_key, coin] = 0
                else:
                    coin_value = df_rtn.at[last_adj_date, coin] * df_all.at[date_key, coin]
                    diff = coin_value - usd_share
                    if diff > 0:
                        sell_dict[coin] = diff/df_all.at[date_key, coin]
                        df_rtn.at[date_key, coin] = df_rtn.at[last_adj_date, coin] - sell_dict[coin]
                    else:
                        df_rtn.at[date_key, coin] = df_rtn.at[last_adj_date, coin]

            # 卖出币回收usd
            usd_take_back = 0
            for coin in sell_dict:
                usd_take_back += df_all.at[date_key, coin] * sell_dict[coin] * (1 - c_rate)
            print(sell_dict, usd_take_back)
            # 计算加仓币种
            last_adj_date = str(pd.to_datetime(date_key) - relativedelta(hours=momentum_period))
            last_hold_coin_list = hold_info[last_adj_date]
            print(last_adj_date, last_hold_coin_list)
            print(date_key, hold_coin_list)
            for coin in hold_coin_list:
                if coin not in last_hold_coin_list:
                    buy_dict[coin] = usd_share/df_all.at[date_key, coin]
                    df_rtn.at[date_key, coin] = buy_dict[coin]
                else:
                    coin_value = df_rtn.at[last_adj_date, coin] * df_all.at[date_key, coin]
                    diff = coin_value - usd_share
                    if diff < 0:
                        buy_dict[coin] = -diff/df_all.at[date_key, coin]
                        df_rtn.at[date_key, coin] = buy_dict[coin]
                    else:
                        df_rtn.at[date_key, coin] = df_rtn.at[last_adj_date,coin]
            # 买入币消耗usd
            usd_used = 0
            for coin in buy_dict:
                usd_used += df_all.at[date_key, coin] * buy_dict[coin] * (1 + c_rate)
            print(buy_dict, usd_used)
            usd_amount = df_rtn.at[last_adj_date, 'USD'] + usd_take_back - usd_used
            df_rtn.at[date_key, 'USD'] = usd_amount

            all_rights = usd_amount
            for coin in hold_coin_list:
                all_rights += df_rtn.at[date_key, coin] * df_all.at[date_key, coin]
            df_rtn.at[date_key, 'equity'] = all_rights

    df_rtn.rename(columns={'equity': '%s_equity' % from_date}, inplace=True)
    df_rtn.reset_index(inplace=True)
    df_rtn = df_rtn[['date', '%s_equity' % from_date]]
    return df_rtn




from_date = '2018-01-01 00:00:00'
end_date = '2019-02-28 00:00:00'
quantile_info_list = [(0.2,0.4,0.6,0.8)] #最多支持分五组
mode = 'momentum'
momentun_limit = 0.00
exchange = 'Coinbase'
volume_rank = 20
momentum_period = 168
c_rate = 0.003

for quantile_info in quantile_info_list:
    df_result_all = pd.DataFrame()
    for momentum_period in [168]:
                for change_time in [168]:
                    for c_rate in [0.000]:
                        for group_code in range(len(quantile_info)+1) :
                            # start point
                            from_date_list = list(pd.date_range(from_date, str(pd.to_datetime(from_date) + relativedelta(hours=momentum_period)), freq=str(change_time) + 'H'))[:-1]

                            # first df
                            df = gen_equity_df(from_date, end_date, momentum_period, quantile_info, mode, c_rate, momentun_limit,group_code)
                            df['equity'] = df['%s_equity' % from_date]
                            for from_date_0 in from_date_list[1:]:
                                from_date_0 = str(from_date_0)
                                df_0 = gen_equity_df(from_date_0, end_date, momentum_period, quantile_info, mode, c_rate, momentun_limit,group_code)
                                df = df.merge(df_0, on='date', how='outer')

                            df['date'] = pd.to_datetime(df['date'],format="%Y-%m-%d %H:%M:%S")
                            df.sort_values(by='date', inplace=True)

                            # process combined df
                            df.fillna(method='ffill', inplace=True)
                            df.fillna(method='bfill', inplace=True)
                            df_colums_list = df.columns.values.tolist()
                            equity_counts = 0
                            for column_name in df_colums_list:
                                if 'equity' in column_name:
                                    equity_counts += 1
                                    df['equity'] = df['equity'] + df[column_name]
                            df['equity'] = df['equity']/equity_counts
                            df['net_value'] = df['equity']/df.at[0, 'equity']
                            df = df[['date', 'net_value']]
                            df_for_merge = df.copy()
                            df_for_merge.rename(columns={'net_value':'net_value%s'%str(group_code)},inplace=True)
                            print(df_for_merge)
                            if len(df_result_all)<2:# 第一次合并前应该是空的
                                df_result_all=df_for_merge
                            else:
                                df_result_all = df_result_all.merge(df_for_merge,how='outer',on='date')

                            print(df_result_all)
    print(df_result_all)
    gen_relative_graph(df_result_all, momentum_period, change_time, c_rate, range(len(quantile_info)+1))


