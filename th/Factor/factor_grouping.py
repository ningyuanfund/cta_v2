# coding=utf-8
import pandas as pd
import os
from th.Factor import Functions as fct
import pickle
import statsmodels.api as sm
from matplotlib import pyplot as plt
import warnings
from dateutil.relativedelta import relativedelta


warnings.simplefilter(action='ignore', category=FutureWarning)
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)
fct.del_file(r'D:\Work\民生证券\通证的因子分析\grouped\\')

from_date = '2018-01-01 00:00:00'
end_date = '2019-04-09 00:00:00'
adj_period_list = [1, 2, 3, 5, 7, 10, 24, 48, 72]
for coin_pool in [15]:
    for adj_period in adj_period_list:

        # -----------------------------------分组

        filepath = r'D:\Work\民生证券\通证的因子分析\datafromcoinmetrics\all\\'
        coin_list = fct.eachFile(filepath)

        name = locals()

        # 将所有数据读入内存
        for coin in coin_list:
            try:
                path = r'D:\Work\民生证券\通证的因子分析\datafromcoinmetrics\all\\%s.csv' % (coin)
                f = open(path)
                df = pd.read_csv(f)
                df.set_index('date',inplace=True)
                name['df_' + coin] = df[['marketcap(USD)', 'price(USD)']]
                # print(name['df_' + coin])
            except:
                pass

        # 构建日期
        date_range = pd.date_range(from_date,end_date,freq=str(adj_period)+'H')

        for date in date_range:
            df_group = pd.DataFrame()
            date = str(date)

            # 只有市值前三十才纳入交易
            for coin in coin_list:
                # print(date,coin)
                try:
                    cm = name['df_' + coin].loc[str(date),'marketcap(USD)']
                    price = name['df_' + coin].loc[str(date),'price(USD)']

                    # 为获得动量，必须读前一个观察期的价格
                    mt_base_date = str(pd.to_datetime(date) - relativedelta(hours=adj_period))
                    last_price = name['df_' + coin].loc[mt_base_date,'price(USD)']

                    len0 = df_group.shape[0]
                    df_group.at[len0,'coin'] = coin
                    df_group.at[len0,'cm'] = cm
                    df_group.at[len0,'price(USD)'] = price
                    df_group.at[len0,'last_price(USD)'] = last_price
                    df_group['momentumn'] = (df_group['price(USD)']-df_group['last_price(USD)'])/df_group['last_price(USD)']

                except:
                    pass
            # print(df_group)
            # 过滤市值在coin_pool名以外的币种,如果不足coin_pool种，则取coin_pool种
            df_group.sort_values(by='cm', ascending=False, inplace=True)
            df_group.reset_index(drop=True, inplace=True)
            try:
                df_group = df_group[:coin_pool]
            except:
                df_group = df_group

            # 市值按照中位数分为大小两组
            quat_cm = df_group['cm'].quantile(0.5)
            condition0 = df_group['cm'] < quat_cm
            condition1 = df_group['cm'] >= quat_cm
            df_group.loc[condition0, 'cm_group'] = 0
            df_group.loc[condition1, 'cm_group'] = 1

            # 其余变量按照（0.3，0.7）分为三组
            para_list = ['momentumn']
            para_name = locals()
            for para in para_list:
                para_name['quant_'+para] = df_group[para].quantile((0.3, 0.7))

                condition0 = df_group[para] < para_name['quant_'+para][0.3]
                condition1 = (df_group[para] >= para_name['quant_'+para][0.3]) & (df_group[para] < para_name['quant_'+para][0.7])
                condition2 = df_group[para] >= para_name['quant_' + para][0.7]
                df_group.loc[condition0, 'quant_'+para] = 0
                df_group.loc[condition1, 'quant_'+para] = 1
                df_group.loc[condition2, 'quant_'+para] = 2

            df_group.to_csv(r'D:\Work\民生证券\通证的因子分析\grouped\\%s.csv' % (date.replace(':','_')))

        # ---------------------------------------------分组完成

        # ---------------------------------------------计算因子值,[市值，动量，链上交易，活跃地址数]
        # 小市值高动量：[0,2,,]
        # 大市值低活跃地址数:[1,,,0]

        # 根据给定二维组合计算持仓快照
        def rtn_two_axis(dict={'cm_group': 1, 'quant_momentumn': 2}):
            '''
            :param dict: 二维组合
            :return: 每个调仓日持有的品种和价格,上一个调仓日持有的品种和他们现在的价格

            '''

            # 需要返还维度信息
            factor_info = {}
            # 创建持仓字典
            hold_dict = {}
            date_range = pd.date_range(from_date,end_date,freq=str(adj_period)+'H')
            for date in date_range:
                date = str(date)
                path = r'D:\Work\民生证券\通证的因子分析\grouped\\%s.csv' % (date.replace(':','_'))
                f = open(path)
                df_group = pd.read_csv(f)

                # 找到本期同时满足二维条件的品种，写入字典
                condition_name = locals()
                a = 'a'
                for key in dict:
                    if len(str(dict[key])) > 0:
                        factor_info[key] = dict[key]
                        condition_name['condition'+a] = (df_group[key] == dict[key])
                        # print(conditiona)
                        a = 'b'
                qualified_coin = list(df_group.loc[condition_name['conditiona'] & condition_name['conditionb'],'coin'])
                if len(qualified_coin)>0:# 如果存在满足条件的币
                    # 创建持仓字典
                    coin_dict_now = {}
                    for coin in qualified_coin:
                        condition_coin_price = df_group['coin'] == coin
                        price = list(df_group.loc[condition_coin_price,'price(USD)'])[0]
                        coin_dict_now[coin] = price
                else:
                    coin_dict_now = {}

                # 找到上期持仓的币种本期的价格
                try:
                    # 获取上一期的持仓
                    last_hold_updated_dict = {}
                    last_date =str(pd.to_datetime(date) - relativedelta(hours=adj_period))
                    # 观测上一期的调仓结果
                    last_hold = hold_dict[last_date]['next_period']
                    for coin in last_hold:
                        # 查询现在的价格
                        last_hold_updated_dict[coin] = fct.seek_price_from_cmt(date =date,coin=coin)
                except:
                    last_hold_updated_dict={}

                # 将本期持仓和上期持仓一同写入调仓日信息
                hold_dict[date] = {'last_hold_updated':last_hold_updated_dict,'next_period':coin_dict_now}

            print(hold_dict)
            return hold_dict


        # 根据持仓字典计算收益率
        def gen_rtn(hold_dict):
            '''

            :param hold_dict:
            :return:
            '''
            rtn_dict = {}
            date_range = pd.date_range(from_date,end_date,freq=str(adj_period)+'H')
            for date in date_range:
                try:
                    date = str(date)
                    last_date = str(pd.to_datetime(date) - relativedelta(hours=adj_period))
                    # 计算组合收益率
                    now_price = hold_dict[date]['last_hold_updated']
                    begin_price = hold_dict[last_date]['next_period']


                    coin_counts = len(now_price)
                    agg_rtn = 0
                    for coin in now_price:
                        rtn = now_price[coin]/begin_price[coin]-1
                        agg_rtn += rtn
                    group_rtn = agg_rtn/coin_counts
                    rtn_dict[date] = group_rtn
                except:
                    rtn_dict[date] = 0
            return rtn_dict


        # 根据组合收益率计算因子收益率
        def factor_rtn(rtn_dict_list,how='avg'):
            '''
            如果是avg，则计算列表中的每周平均收益；如为minus，则计算列表[0] - 列表[1]的收益
            :param rtn_dict_list:
            :param how: avg,minus
            :return:dict
            '''
            factor_rtn_dict = {}
            date_range = pd.date_range(from_date,end_date,freq=str(adj_period)+'H')
            if how == 'avg':
                for date in date_range:
                    date = str(date)
                    agg_rtn = 0
                    group_counts = len(rtn_dict_list)
                    for rtn_dict in rtn_dict_list:
                        agg_rtn += rtn_dict[date]
                    avg_rtn = agg_rtn/group_counts
                    factor_rtn_dict[date] = avg_rtn

            if how == 'minus':
                for date in date_range:
                    date = str(date)
                    minued = rtn_dict_list[0]
                    subtractor = rtn_dict_list[1]
                    factor_rtn_dict[date] = (minued[date] - subtractor[date])/2
            return factor_rtn_dict


        # 根据收益率字典计算净值曲线
        def gen_equity_curve(rtn_dict,show=False):
            '''

            :param rtn_dict:
            :return:
            '''


            df = pd.DataFrame()

            # 填写第一行初始化
            for date in rtn_dict:
                begin_date = str(pd.to_datetime(date) - relativedelta(hours=adj_period))
                df.at[begin_date, 'equity'] = 1
                df.at[begin_date,'rtn'] = 0
                break

            for date in rtn_dict:
                try:
                    df.loc[date,'rtn'] = rtn_dict[date]
                    last_date = str(pd.to_datetime(date) - relativedelta(hours=adj_period))
                    df.loc[date,'equity'] = df.loc[last_date,'equity']*(1+df.loc[date,'rtn'])
                except:
                    pass

            if show == True:
                # 作图
                df.reset_index(inplace=True)
                df.rename(columns={'index':'date'},inplace=True)
                print(df)
                x = df['date']
                y = df['equity']

                fig = plt.figure(figsize=(12, 8))
                ax = fig.add_subplot(111)
                ax.plot(x, y, 'b-', label='equity')
                ax.legend(loc="best")

                plt.show()

            return df


        # 市值-动量分组
        SH = gen_rtn(rtn_two_axis(dict={'cm_group': 0, 'quant_momentumn': 2}))
        SN0 = gen_rtn(rtn_two_axis(dict={'cm_group': 0, 'quant_momentumn': 1}))
        SL = gen_rtn(rtn_two_axis(dict={'cm_group': 0, 'quant_momentumn': 0}))
        BH = gen_rtn(rtn_two_axis(dict={'cm_group': 1, 'quant_momentumn': 2}))
        BN0 = gen_rtn(rtn_two_axis(dict={'cm_group': 1, 'quant_momentumn': 1}))
        BL = gen_rtn(rtn_two_axis(dict={'cm_group': 1, 'quant_momentumn': 0}))

        # gen_equity_curve(factor_rtn([SH, BH], how='avg'), show=True)


        # # 市值-活跃地址分组
        # SA = gen_rtn(rtn_two_axis(dict={'cm_group':0,'quant_momentumn':'','quant_txVolume':'','quant_activeAddresses':2}))
        # SN1 = gen_rtn(rtn_two_axis(dict={'cm_group':0,'quant_momentumn':'','quant_txVolume':'','quant_activeAddresses':1}))
        # SD = gen_rtn(rtn_two_axis(dict={'cm_group':0,'quant_momentumn':'','quant_txVolume':'','quant_activeAddresses':0}))
        # BA = gen_rtn(rtn_two_axis(dict={'cm_group':1,'quant_momentumn':'','quant_txVolume':'','quant_activeAddresses':2}))
        # BN1= gen_rtn(rtn_two_axis(dict={'cm_group':1,'quant_momentumn':'','quant_txVolume':'','quant_activeAddresses':1}))
        # BD = gen_rtn(rtn_two_axis(dict={'cm_group':1,'quant_momentumn':'','quant_txVolume':'','quant_activeAddresses':0}))
        # #
        # # 市值-链上交易分组
        # SG = gen_rtn(rtn_two_axis(dict={'cm_group':0,'quant_momentumn':'','quant_txVolume':2,'quant_activeAddresses':''}))
        # SN2 = gen_rtn(rtn_two_axis(dict={'cm_group':0,'quant_momentumn':'','quant_txVolume':1,'quant_activeAddresses':''}))
        # ST = gen_rtn(rtn_two_axis(dict={'cm_group':0,'quant_momentumn':'','quant_txVolume':0,'quant_activeAddresses':''}))
        # BG = gen_rtn(rtn_two_axis(dict={'cm_group':1,'quant_momentumn':'','quant_txVolume':2,'quant_activeAddresses':''}))
        # BN2= gen_rtn(rtn_two_axis(dict={'cm_group':1,'quant_momentumn':'','quant_txVolume':1,'quant_activeAddresses':''}))
        # BT = gen_rtn(rtn_two_axis(dict={'cm_group':1,'quant_momentumn':'','quant_txVolume':0,'quant_activeAddresses':''}))


        # 单边

        # S_oneside = factor_rtn([SH,BH],how='avg')
        # gen_equity_curve(S_oneside,show = True)


        # # 对冲
        # GMT = factor_rtn([factor_rtn([SG,BG],how='avg'),factor_rtn([ST,BT],how='avg')],how='minus')
        # gen_equity_curve(GMT).to_csv(r'D:\Work\民生证券\通证的因子分析\result\%sGMT2017010120190325.csv'%(str(coin_pool)))
        #
        # AMD = factor_rtn([factor_rtn([SA,BA],how='avg'),factor_rtn([SD,BD],how='avg')],how='minus')
        # gen_equity_curve(AMD).to_csv(r'D:\Work\民生证券\通证的因子分析\result\%sAMD2017010120190325.csv'%(str(coin_pool)))

        HLM = factor_rtn([factor_rtn([SL,BL],how='avg'),factor_rtn([SH,BH],how='avg')],how='minus')
        gen_equity_curve(HLM, show=False).to_csv(r'D:\Work\民生证券\通证的因子分析\result\%sLMHadj_%s0.3_0.7.csv' % (str(coin_pool), str(adj_period)))

        # SMBtv = factor_rtn([factor_rtn([SG,SN2,ST],how='avg'),factor_rtn([BG,BN2,BT],how='avg')],how='minus')
        # SMBaa = factor_rtn([factor_rtn([SA,SN1,SD],how='avg'),factor_rtn([BA,BN1,BD],how='avg')],how='minus')
        # SMBmt = factor_rtn([factor_rtn([SH,SN0,SL],how='avg'),factor_rtn([BH,BN0,BL],how='avg')],how='minus')

        # SMB = factor_rtn([SMBmt,SMBaa,SMBtv],how='avg')
        # gen_equity_curve(SMB).to_csv(r'D:\Work\民生证券\通证的因子分析\result\%sSMB2017010120190325.csv'%(str(coin_pool)))
