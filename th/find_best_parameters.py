import pandas as pd
import os
import time
from th.Functions import transfer_to_period_data
import th.Signals as sigs
from th.equity_curve import equity_curve_with_long_and_short
from th.equity_curve import equity_curve_delicay
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)

# 构建参数候选组合
'''
    m = para[0]  # 观察期数
    n = para[1]  # std倍数
    stop_loss_pct = para[2]  # 止损比例
    [10, 20, 3,[3,0.1]]
'''


# ======需要手动设置的参数===== 
strategy_name = 'bolling'  # 策略名称 
para_name_list = ['N', 'K1','止损比例', '时间级别']  # 总共的参数
output_columns = ['排名', 'N', 'K1', '止损比例', '时间级别', '策略收益']  # 在总共参数基础上，前面加排名，后面加策略收益  
para_moving_stop_profit_list = ['stop_loss_pct的倍数e','从最高点回撤的比例d']
moving_stop_profit = False #默认打开移动止盈
e_list = [3, 4, 5, 6, 7]
d_list = [0.1, 0.2, 0.3, 0.4, 0.5]
K_min = 50
K_max = 130
K_step = 5
n_list = [i for i in range(K_min,K_max,K_step)] #[i for i in range(80,200,5)]
part_name = str(K_min)+'_'+str(K_max)+'_'+str(K_step) # 用于分布计算，K线周期为从25到60，step为5
k1_list = [i/2 for i in range(3, 8)]
stop_loss_pct_list = [i for i in range(3, 10, 2)]  # 5%  4 
rule_type_list = ['15T']
pair = 'ETHUSD'


# 生成参数列表
para_list = []
para_moving_stop_profit_list = []
for n in n_list:
    for k1 in k1_list:
            for stop_loss_rate in stop_loss_pct_list:
                if moving_stop_profit:
                    # 生成移动止盈参数列
                    for e in e_list:
                        for d in d_list:
                            para_moving_stop_profit_list.append([e,d])
                            para_list.append([n, k1, stop_loss_rate,e,d, 'T'])
                else:
                    para_moving_stop_profit_list = [[0, 0]]
                    para_list.append([n, k1, stop_loss_rate, 0, 0, 'T'])



# 设定路径，如果没有这个路径的话就生成
path = r'D:\Work\back-trace\%s\%s' % (strategy_name, pair)
# path = r'E:\cta_v2\ckt\tables\%s\%s' % (strategy_name, pair)
if not os.path.exists(path):
    os.makedirs(path)

output_path = r'D:\Work\back-trace\%s\%s' % (strategy_name, pair)  # 生成表格的基础地址，最后带上策略名以区分不同策略生成的表格
if not os.path.exists(output_path):
    os.makedirs(output_path)
rtn_path = output_path + r'rtn'+'.csv'  # 用于保存完整的参数排名结果
print(rtn_path)
rtn_top_30_path = output_path + r'rtn_top_30_'+'.csv'  # 用于展示的30个最佳参数

start_time = time.clock()
para_group_counts = len(para_list)
total_counts = len(para_list)
# 遍历所有参数组合
rtn = pd.DataFrame()
for rule_type in rule_type_list:

    all_data = pd.read_hdf(r'D:\Work\cta_v2\fifty-five-data\input\%s.h5' % pair, key=rule_type)
    all_data = all_data.resample(rule=rule_type,
                            on='candle_begin_time',
                            label='left',
                            closed='left').agg({'open': 'first',
                                                'high': 'max',
                                                'low': 'min',
                                                'close': 'last',
                                                'volume': 'sum'})

    all_data.reset_index(inplace=True)
    all_data = all_data[all_data['candle_begin_time'] >= pd.to_datetime('2019-02-28')]
    all_data = all_data[all_data['candle_begin_time'] <= pd.to_datetime('2019-03-31')]
    all_data.reset_index(drop=True, inplace=True)
    print(all_data)

    for para in para_list:
        para[-1] = rule_type

        now_time = time.clock()
        spent_time = now_time - start_time
        avg_spent_time = spent_time / (total_counts - para_group_counts + 1)
        predict_need_time = round(avg_spent_time * para_group_counts / 60, 2)

        # 剩余多少组参数需要检验
        para_group_counts -= 1

        show_process = str(total_counts - para_group_counts) + '/' + str(total_counts) + ' 已耗时：' + str(
            round(spent_time / 60, 2)) + 'min 预计剩余时间： ' + str(predict_need_time) + 'min'

        # 计算交易信号
        function_name = 'sigs.signal_%s_with_stop_lose(all_data.copy(), para)' % strategy_name
        df = eval(function_name)

        # 计算资金曲线
        # df = equity_curve_with_long_and_short(df, leverage_rate=2, c_rate=3.0/1000, min_margin_rate=0.15)
        df = equity_curve_delicay(df, method='time_based_portfolio_OCO', method_para={'c_rate':0.003,'leverage':3,'p1':0.7 ,'p2':24*300,'p3':0.05})
        print(para, '策略最终收益：', df.iloc[-1]['equity_curve'],show_process)

        # df.to_csv('C:\\Users\\HP\\Desktop\\timing_strategy\\data\\output_data\\ckt.csv')

        # 存储数据
        rtn.loc[str(para), 'return'] = df.iloc[-1]['equity_curve']

        if para_group_counts % 50 == 0:
            rtn.to_csv(rtn_path, encoding='utf_8_sig')
            print('进度已保存！')

        # 将参数信息也保存
        for i in range(len(para_name_list)):
            rtn.loc[str(para), para_name_list[i]] = para[i]




# 生成网页展示的表格并输出
rtn_top_10 = rtn.sort_values(by='return', ascending=False)[:30]
rtn_top_10.reset_index(inplace=True)
rtn_top_10['return'] = round(rtn_top_10['return'], 2)

output_df = pd.DataFrame(columns=output_columns)

output_df[para_name_list] = rtn_top_10[para_name_list]
output_df['排名'] = output_df.index + 1
output_df['策略收益'] = rtn_top_10['return']
output_df.set_index('排名', inplace=True)

print(rtn.sort_values(by='return', ascending=False))

rtn.to_csv(rtn_path, encoding='utf_8_sig')
output_df.to_csv(rtn_top_30_path, encoding='utf_8_sig')