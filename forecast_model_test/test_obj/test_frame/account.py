
class MarginAccount:
    def __init__(self, init_cash, max_leverage):
        self.cash = init_cash
        self.max_leverage = max_leverage
        self.tradable_balance = self.cash * max_leverage
        self.position = []

    def __repr__(self):
        return '=' * 10 + '\nCASH: {0}\nMAX LEVERAGE: {1}\nTRADABLE BALANCE: {2}\nPOSITION: {3}\n'.\
            format(self.cash, self.max_leverage, self.tradable_balance, self.position) + '=' * 10

    def order(self, price, amount, symbol, order_type):
        symbol_position = {}
        amount = round(amount, 3)
        price = round(price, 4)
        for pos in self.position:
            if pos['symbol'] == symbol:
                symbol_position = pos.copy()

        # 该 symbol 未持仓
        if symbol_position == {}:
            if price * abs(amount) > self.tradable_balance:
                raise Exception('Not enough balance!')

            if not abs(amount) > 0:
                raise Exception('Minimum amount 0.001')
            self.position.append(
                {
                    'symbol': symbol,
                    'order_type': order_type,
                    'open_price': price,
                    'amount': amount
                }
            )
            self.tradable_balance -= abs(amount) * price
        # 该 symbol 有持仓
        else:
            # 加仓
            if amount * symbol_position['amount'] > 0:
                if amount * price > self.tradable_balance:
                    raise Exception('Not enough balance!')

                if not abs(amount) > 0:
                    raise Exception('Minimum amount 0.001')
                avg_price = (symbol_position['open_price'] * abs(symbol_position['amount']) + price * abs(amount)) / \
                            (abs(symbol_position['amount'] + abs(amount)))
                symbol_position = {
                    'symbol': symbol,
                    'order_type': order_type,
                    'open_price': avg_price,
                    'amount': symbol_position['amount'] + amount
                }
                # 更新持仓信息
                for i in range(len(self.position)):
                    if self.position[i]['symbol'] == symbol_position['symbol']:
                        self.position[i] = symbol_position

            # 减仓
            else:

                if not abs(amount) > 0:
                    raise Exception('Minimum amount 0.001')

                # 完全平仓
                if abs(symbol_position['amount']) == abs(amount):
                    profit = symbol_position['amount'] * (price - symbol_position['price'])
                    self.cash += profit
                    self.tradable_balance = self.cash * self.max_leverage

                    for i in range(len(self.position)):
                        if self.position[i]['symbol'] == symbol:
                            self.position[i] = {}

                # 单纯减仓
                elif abs(symbol_position['amount']) > abs(amount):
                    profit = amount * (price - symbol_position['price'])
                    self.cash += profit
                    self.tradable_balance += profit * self.max_leverage

                    for i in range(len(self.position)):
                        if self.position[i]['symbol'] == symbol:
                            self.position[i]['amount'] += amount

                # 反向开仓
                else:
                    profit = symbol_position['amount'] * (price - symbol_position['price'])
                    self.cash += profit
                    self.tradable_balance = self.cash * self.max_leverage

                    remain_amount = amount - symbol_position['amount']
                    if not abs(remain_amount) > 0:
                        raise Exception('Minimum amount 0.001')

                    if price * remain_amount > self.tradable_balance:
                        raise Exception('Not enough balance!')

                    for i in range(len(self.position)):
                        if self.position[i]['symbol'] == symbol:
                            self.position[i] = {
                                'symbol': symbol,
                                'order_type': order_type,
                                'open_price': price,
                                'amount': remain_amount
                            }
                    self.tradable_balance -= abs(remain_amount) * price
