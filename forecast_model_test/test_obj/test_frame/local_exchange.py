import pandas as pd

from forecast_model_test.test_obj.test_frame.account import MarginAccount


class LocalExchange:
    def __init__(self, api_key, secret, init_cash, max_leverage=3):
        self.api_key = api_key
        self.secret = secret
        self._margin_account = MarginAccount(init_cash=init_cash, max_leverage=max_leverage)
        self.data = self._init_data()

    @property
    def margin_account(self):
        return self._margin_account

    @staticmethod
    def _init_data():
        data = pd.read_csv(r'E:\cta_v2\forecast_model_test\data\funding_rate_prem.csv')
        data['candle_begin_time'] = pd.to_datetime(data['candle_begin_time'])
        return data

    @staticmethod
    def _refresh_data():
        return NotImplemented

    def get_period_data(self, begin_time, end_time):
        """

        :param begin_time: datetime
        :param end_time: datetime
        :return:
        """
        cond1 = self.data['candle_begin_time'] >= begin_time
        cond2 = self.data['candle_begin_time'] < end_time
        data = self.data.loc[cond1 & cond2]
        data.reset_index(drop=True, inplace=True)
        return data

    def get_now_price(self, now_time):
        price = self.data.loc[self.data['candle_begin_time'] == now_time, 'open'].values
        return price

    def order(self, price, amount, symbol, order_type):
        self._margin_account.order(price=price, amount=amount, symbol=symbol, order_type=order_type)

    def position(self):
        return self._margin_account.position

    def cash(self):
        return self._margin_account.cash

    def account(self):
        print(self._margin_account)
        return self._margin_account

    def total_equity(self, now_time):
        now_price = self.get_now_price(now_time=now_time)
        float_pnl = (now_price - self._margin_account.position['price']) * self._margin_account.position['amount']
        return self._margin_account.cash + float_pnl
