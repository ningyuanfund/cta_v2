
class TradingCalender:
    def __init__(self, begin, end):
        """

        :param begin: datetime
        :param end: datetime
        """
        self.begin = begin
        self.end = end
        self.now = begin

    def next_period(self, period):
        """

        :param period: timedelta(days=1)
        :return:
        """
        self.now += period

    def end_of_test(self):
        return self.now > self.end
