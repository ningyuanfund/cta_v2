from datetime import timedelta, datetime

import pandas as pd
from sqlalchemy import create_engine

eg = create_engine('postgresql://postgres:thomas@localhost/bitfinex_local')
pd.set_option('expand_frame_repr', False)

# local_data = pd.read_csv(r'E:\data\BTC_usdtprem_fundingratio.csv')
# local_data = local_data[['candle_begin_time', 'fundingRate', 'prem']].copy()
# local_data['candle_begin_time'] = pd.to_datetime(local_data['candle_begin_time'])

sql = 'select * from "cta_test"."BTCUSD"'
price_data = pd.read_sql_query(sql, con=eg)
price_data = price_data[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()
price_data['candle_begin_time'] = pd.to_datetime(price_data['candle_begin_time'])
#
# data = local_data.merge(price_data, on='candle_begin_time', how='outer')
#
# data = data.loc[data['candle_begin_time'] >= pd.to_datetime('2017-01-01')].copy()
# data.sort_values(by='candle_begin_time', inplace=True)
# data.reset_index(inplace=True, drop=True)
# data = data[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume', 'fundingRate', 'prem']].copy()
# data.to_csv(r'E:\cta_v2\forecast_model_test\data\funding_rate_prem.csv')

print(price_data)

ma = 200
n = 2
gap_h = 5

price_data['future_rtn'] = price_data['close'].shift(-gap_h * 12) / price_data['close'] - 1
price_data['mid'] = price_data['close'].rolling(ma).mean()
price_data['upper'] = price_data['close'].rolling(ma).mean() + price_data['close'].rolling(ma).std(ddof=1) * n
price_data['lower'] = price_data['close'].rolling(ma).mean() - price_data['close'].rolling(ma).std(ddof=1) * n

cond1 = price_data['close'] < price_data['mid']
cond2 = price_data['close'] >= price_data['mid']

price_data.loc[cond1, 'bolling'] = (price_data['close'] - price_data['mid']) / (price_data['lower'] - price_data['mid'])
price_data.loc[cond2, 'bolling'] = (price_data['close'] - price_data['mid']) / (price_data['upper'] - price_data['mid'])

price_data.to_csv(r'C:\Users\sycam\Desktop\test.csv')