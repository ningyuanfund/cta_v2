def real_time_signal_turtle_close_open_long_short_with_stop_lose(now_pos, stop_lose_price, df, para=[20, 10, 15]):
    n1 = int(para[0])
    n2 = int(para[1])
    close_price = df.iloc[-1]['close']

    # 现在是多头仓位
    if now_pos == 1:
        # 依次计算是否要开空、平仓、止损
        df['open_close_low'] = df[['open', 'close']].min(axis=1)
        df['n1_low'] = df['open_close_low'].rolling(n1).min()
        df['n2_low'] = df['open_close_low'].rolling(n2).min()
        if close_price < df.iloc[-2]['n1_low']:
            target_pos = -1
        elif close_price < df.iloc[-2]['n2_low']:
            target_pos = 0
        elif close_price < stop_lose_price:
            target_pos = 0
        else:
            target_pos = 1

    # 现在是空头仓位
    elif now_pos == -1:
        # 依次计算是否要开空、平仓、止损
        df['open_close_high'] = df[['open', 'close']].max(axis=1)
        df['n1_high'] = df['open_close_high'].rolling(n1).max()
        df['n2_high'] = df['open_close_high'].rolling(n2).max()
        if close_price > df.iloc[-2]['n1_high']:
            target_pos = 1
        elif close_price > df.iloc[-2]['n2_high']:
            target_pos = 0
        elif close_price > stop_lose_price:
            target_pos = 0
        else:
            target_pos = -1
    # 现在无仓位
    elif now_pos == 0:
        df['open_close_high'] = df[['open', 'close']].max(axis=1)
        df['open_close_low'] = df[['open', 'close']].min(axis=1)
        df['n1_high'] = df['open_close_high'].rolling(n1).max()
        df['n1_low'] = df['open_close_low'].rolling(n1).min()

        if close_price > df.iloc[-2]['n1_high']:
            target_pos = 1
        elif close_price < df.iloc[-2]['n1_low']:
            target_pos = -1
        else:
            target_pos = 0
    # 其他情况报错
    else:
        raise ValueError('当前仓位变量now_pos只能是1， -1， 0，但目前是', now_pos)

    return target_pos

import datetime
print(datetime.datetime.now().seconds)