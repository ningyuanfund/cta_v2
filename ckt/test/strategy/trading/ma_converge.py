# 均线收缩
def real_time_signal_ma_converge_with_stop_lose(now_pos, stop_lose_price, df, para=[20, 100, 3, 5]):
    """
    快线和慢线的间距定义为gap
    gap 连续k次收缩，则认为发出一次卖空信号
    gap 连续k次扩大，则认为发出一次买多信号

    """
    ma_short = para[0]
    ma_long = para[1]
    k = para[2]
    close_price = df.iloc[-1]['close']

    # 只截取必要的部分，减少计算量。稍微留一点冗余
    df = df[-(ma_long + k + 2):].copy()

    # 计算均线
    df['ma_short'] = df['close'].rolling(ma_short).mean()
    df['ma_long'] = df['close'].rolling(ma_long).mean()
    df['gap'] = df['ma_short'] - df['ma_long']


    # 现在是多头仓位
    if now_pos == 1:
        # 做空信号
        short_condition = True
        for i in range(k):
            condition = df.iloc[-i - 1]['gap'] < df.iloc[-i - 2]['gap']
            short_condition = short_condition & condition
        # 需要开空
        if short_condition:
            target_pos = -1
        # 需要止损
        elif close_price < stop_lose_price:
            target_pos = 0
        else:
            target_pos = 1

    # 现在是空头仓位
    elif now_pos == -1:
        # 做多信号
        long_condition = True
        for i in range(k):
            condition = df.iloc[-i - 1]['gap'] > df.iloc[-i - 2]['gap']
            long_condition = long_condition & condition
        # 需要开多
        if long_condition:
            target_pos = 1
        # 需要止损
        elif close_price > stop_lose_price:
            target_pos = 0
        else:
            target_pos = -1

    # 现在无仓位
    elif now_pos == 0:
        # 做多信号
        long_condition = True
        for i in range(k):
            condition = df.iloc[-i - 1]['gap'] > df.iloc[-i - 2]['gap']
            long_condition = long_condition & condition

        # 做空信号
        short_condition = True
        for i in range(k):
            condition = df.iloc[-i - 1]['gap'] < df.iloc[-i - 2]['gap']
            short_condition = short_condition & condition

        if long_condition:
            target_pos = 1
        elif short_condition:
            target_pos = -1
        else:
            target_pos = 0

    else:
        raise ValueError('当前仓位变量now_pos只能是1， -1， 0，但目前是', now_pos)

    return target_pos

import pandas as pd
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)

all_data = pd.read_hdf('C:\\Users\\HP\\Desktop\\timing_strategy\\data\\input_data\\ETHUSD.h5', key='30T')
all_data = all_data[all_data['candle_begin_time'] >= pd.to_datetime('2017-01-01')]
all_data = all_data[all_data['candle_begin_time'] <= pd.to_datetime('2018-11-21 06:30:00')]

all_data.reset_index(drop=True, inplace=True)


target_pos = real_time_signal_ma_converge_with_stop_lose(now_pos=-1, stop_lose_price=500, df=all_data, para=[290, 460, 7, 5])

print(target_pos)
