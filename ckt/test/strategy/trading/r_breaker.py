# R breaker 策略
from datetime import timedelta
def real_time_signal_r_breaker_with_stop_lose(now_pos, stop_lose_price, df, para=[0.35, 1.07, 0.07, 0.25, 3]):
    '''
    在空仓的情况下，价格超过突破买入价，采取趋势策略，即在该点位开仓做多
    在空仓的情况下，价格跌破突破卖出价，采取趋势策略，即在该点位开仓做空
    最高价超过观察卖出价，价格回落，跌破反转卖出价，采取反转策略，即在该点位（反手、开仓）做空
    最低价低于观察买入价，价格反弹，超过反转买入价，采取反转策略，即在该点位（反手、开仓）做多

    最高价 > 观察卖出价，收盘价 < 反转卖出价时，开空（如持有多头仓位，平多开空）
    最低价 < 观察买入价，收盘价 > 反转买入价时，开多（如持有空头仓位，平空开多）
    if 空仓，最高价 > 突破买入价，做多
    if 空仓，最低价 < 突破卖出价，做空
    '''

    observe_rate = para[0]  # 观察买入、观察卖出价的计算系数
    reverse_rate1 = para[1]  # 反转买入、反转卖出价的计算系数
    reverse_rate2 = para[2]  # 反转买入、反转卖出价的计算系数
    break_rate = para[3]  # 突破买入、突破卖出价的计算系数
    close_price = df.iloc[-1]['close']

    df['date'] = df['candle_begin_time'].dt.date

    today = df.iloc[-1]['date']
    yesterday = today - timedelta(days=1)
    df_today = df[df['date'] == today].copy()
    df_yesterday = df[df['date'] == yesterday].copy()

    yesterday_high = df_yesterday['high'].max()  # 昨天的最高价
    yesterday_low = df_yesterday['low'].min()  # 昨天的最低价
    yesterday_close = df_yesterday.iloc[-1]['close']  # 昨天的收盘价

    # 观察卖出价 = 昨天的最高价 + 0.35 * (昨天的收盘价 – 昨天的最低价)
    observe_sell = yesterday_high + observe_rate * (yesterday_close - yesterday_low)
    # 观察买入价 = 昨天的最低价 - 0.35 * (昨天的最高价 – 昨天的最低价)
    observe_buy = yesterday_low - observe_rate * (yesterday_high - yesterday_close)
    # 反转卖出价 = 1.07 / 2 * (昨天的最高价 + 昨天的最低价) – 0.07 * 昨天的最低价
    reverse_sell = reverse_rate1 / 2 * (yesterday_high + yesterday_low) - reverse_rate2 * yesterday_low
    # 反转买入价 = 1.07 / 2 * (昨天的最高价 + 昨天的最低价) – 0.07 * 昨天的最高价
    reverse_buy = reverse_rate1 / 2 * (yesterday_high + yesterday_low) - reverse_rate2 * yesterday_high
    # 突破买入价 = 观察卖出价 + 0.25 * (观察卖出价 – 观察买入价)
    break_buy = observe_sell + break_rate * (observe_sell - observe_buy)
    # 突破卖出价 = 观察买入价 – 0.25 * (观察卖出价 – 观察买入价)
    break_sell = observe_buy - break_rate * (observe_sell - observe_buy)

    # 现在是多头仓位
    if now_pos == 1:
        today_high = df_today['high'].max()
        condition1 = today_high > observe_sell
        condition2 = close_price < reverse_sell
        # 日内最高价超过观察卖出价后，盘中价格出现回落，且进一步跌破反转卖出价构成的支撑线时，采取反转策略
        # 即在该点位（反手、开仓）做空；
        if condition1 & condition2:
            target_pos = -1
        # 需要止损
        elif close_price < stop_lose_price:
            target_pos = 0
        else:
            target_pos = 1

    # 现在是空头仓位
    elif now_pos == -1:
        today_low = df_today['low'].min()
        condition1 = today_low < observe_buy
        condition2 = close_price > reverse_buy
        # 日内最低价低于观察买入价后，盘中价格出现反弹，且进一步超过反转买入价构成的阻力线时，采取反转策略
        # 即在该点位（反手、开仓）做多；
        if condition1 & condition2:
            target_pos = 1
        # 需要止损
        elif close_price > stop_lose_price:
            target_pos = 0
        else:
            target_pos = -1

    # 现在无仓位
    elif now_pos == 0:
        # 空仓情况下，盘中价格超过突破买入
        if close_price > break_buy:
            target_pos = 1
        # 空仓情况下，盘中价格跌破突破卖出
        elif close_price < break_sell:
            target_pos = -1
        else:
            target_pos = 0
    else:
        raise ValueError('当前仓位变量now_pos只能是1， -1， 0，但目前是', now_pos)

    return target_pos


