import pandas as pd
from ckt.cq_sdk.path import get_data_path, get_project_path

pd.set_option('expand_frame_repr', False)

symbol_list = ['BTCUSD', 'ETHUSD', 'EOSUSD', 'LTCUSD', 'XRPUSD']

for symbol in symbol_list:
    data_path = get_data_path('input', '%s.h5' % symbol)
    df_1h = pd.read_hdf(data_path, key='1H')
    df_1d = df_1h.resample(rule='1D', on='candle_begin_time', base=0, label='left', closed='left').agg(
        {
            'open': 'first',
            'high': 'max',
            'low': 'min',
            'close': 'last',
            'volume': 'sum',
        }
    )
    df_1d.dropna(subset=['open'], inplace=True)
    df_1d = df_1d[df_1d['volume'] > 0]
    df_1d.reset_index(inplace=True)
    df_1d.to_hdf(get_data_path('input', '%s.h5' % symbol), key='1D')
    print(df_1d)