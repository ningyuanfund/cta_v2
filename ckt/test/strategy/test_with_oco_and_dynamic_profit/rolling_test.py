import numpy as np
import pandas as pd
import os
import itertools
import ckt.test.strategy.test_with_oco_and_dynamic_profit.Signals as sigs
from datetime import timedelta
from dateutil.relativedelta import relativedelta

import warnings
from multiprocessing import Pool
from matplotlib import pyplot as plt, dates, font_manager
from ckt.cq_sdk.path import get_data_path, get_project_path
from ckt.test.strategy.test_with_oco_and_dynamic_profit.equity_curve import *

warnings.simplefilter(action='ignore')
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)


# ===具体进行回测
def func(all_data, strategy_name, freq, para):
    # 计算交易信号
    function_str = 'sigs.signal_%s_with_stop_lose(all_data.copy(), para)' % strategy_name
    signal_df = eval(function_str)

    """
    stop_loss_pct = all_para['stop_loss_pct']  # 止损比例，没有除以100
    stop_profit_pct = all_para['stop_profit_pct']  # 止盈比例，没有除以100
    dynamic_trigger_pct = all_para['dynamic_trigger']  # 动态止盈触发比例，没有除以100
    dynamic_stop_pct = all_para['dynamic_stop_pct']  # 动态止盈回撤比例，没有除以100
    [n, m, stop_lose_pct, dynamic_trigger_pct, dynamic_stop_pct]

    """
    all_para = {'stop_loss_pct': para[-3],
                'stop_profit_pct': para[-3] * 10,
                'dynamic_trigger': para[-2],
                'dynamic_stop_pct': para[-1]}

    checked_df = check_oco_and_dynamic_stop_profit(signal_df, all_para)

    # 计算资金曲线
    df = equity_curve_with_oco_and_dynamic_stop_profit(checked_df, leverage_rate=3, c_rate=3.0 / 1000)

    # 计算年化收益
    annual_return = (df['equity_curve'].iloc[-1] / df['equity_curve'].iloc[0]) ** (
        '1 days 00:00:00' / (df['candle_begin_time'].iloc[-1] - df['candle_begin_time'].iloc[0]) * 365) - 1

    # 计算最大回撤
    max_drawdown = (1 - df['equity_curve'] / df['equity_curve'].expanding().max()).max()

    # 计算年化收益回撤比
    ratio = annual_return / max_drawdown
    print(freq, para, '最优参数收益：', df.iloc[-1]['equity_curve'], '最优参数年化收益回撤比：', ratio)

    # 存储数据（多进程模式下如果保存成dataframe后面会有重复值）
    return str(freq) + ':' + str(para) + ':' + str(df.iloc[-1]['equity_curve']) + ':' + str(
        max_drawdown) + ':' + str(ratio)


# ===生成训练期x个月内最优参数的时间点列表，方便循环回测时读取
def gen_train_date(sys_para):
    test_period = sys_para['test_period']  # 参数回测期长度
    use_period = sys_para['use_period']  # 参数使用期长度
    begin_date = pd.to_datetime(sys_para['begin_date'])  # 整体回测开始时间（训练集的最早时间）

    # 回测的时间点列表
    train_date_list = []
    while begin_date <= pd.to_datetime(sys_para['end_date']) - relativedelta(months=test_period):

        # 生成训练开始时间、训练结束时间、测试开始时间
        train_date_list.append({'train_begin_date': begin_date,
                               'train_end_date': begin_date + relativedelta(months=test_period),
                                'test_end_date': begin_date + relativedelta(months=test_period + use_period),
                                })
        begin_date += relativedelta(months=use_period)

    # 如果每轮回测都要使用所有历史数据
    if sys_para['if_all_data']:
        for date_info in train_date_list:
            date_info['train_begin_date'] = pd.to_datetime(sys_para['begin_date'])

    return train_date_list


# ===对回测期的数据进行回测，返回最优参数
def train(df, strategy_name, freq, para_list, train_info, rtn_path, process_num):
    pool = Pool(processes=4)  # 开辟进程池
    result = []
    print('训练开始')
    for para in para_list:
        # result.append(func(df, strategy_name, freq, para))
        result.append(pool.apply_async(func, (df, strategy_name, freq, para,)))

    pool.close()  # 关闭进程池
    pool.join()  # 等待开辟的所有进程执行完后，主进程才继续往下执行

    print('训练完成')
    A = pd.DataFrame()
    for i, res in enumerate(result):
        A.loc[i, '参数'] = res.get().split(':')[1]
        A.loc[i, '时间级别'] = res.get().split(':')[0]
        A.loc[i, '策略收益'] = float(res.get().split(':')[2])
        A.loc[i, '最大回撤'] = float(res.get().split(':')[3])
        A.loc[i, '年化收益/最大回撤'] = float(res.get().split(':')[4])

    temp = A['参数'].str[1:-1].str.split(',', expand=True)
    rtn = pd.concat([temp, A.iloc[:, 1:]], axis=1)

    rtn.columns = para_names
    rtn_path = rtn_path + '/para_record'

    if not os.path.exists(rtn_path):
        os.makedirs(rtn_path)

    rtn.to_csv(rtn_path + '/plain.csv', encoding='utf_8_sig')  # 用于保存完整的参数排名结果

    # 最优参数转换回列表格式
    top_para_str = A.sort_values(by='策略收益', ascending=False)[:1]['参数'].values[0][1:-1].split(',')

    top_para = []
    for para in top_para_str:
        top_para.append(float(para))

    return top_para


# ===滚动回测主程序
def rolling_main(pair, strategy_name, para_list, para_names, sys_para, process_num):

    global update_time

    begin_date = sys_para['begin_date']  # 整体回测开始时间（使用的数据的最早时间）

    # 设置路径
    if sys_para['if_all_data']:
        data_para = 'all_data'
    else:
        data_para = 'local_data'

    # 设定路径，如果没有这个路径的话就生成
    path = get_data_path('output', strategy_name, '%s' % pair)
    if not os.path.exists(path):
        os.makedirs(path)

    # 根据不同的时间周期进行回测
    for freq in sys_para['freq']:

        rtn_path = get_data_path('oco_dynamic_output', strategy_name, '%s' % pair, freq, data_para)  # 用于保存完整的参数排名结果
        if not os.path.exists(rtn_path):
            os.makedirs(rtn_path)

        # 导入总数据
        all_data = pd.read_hdf(get_data_path('input', '%s.h5' % pair), key=freq)  # 数据文件存放路径

        # 选取时间段
        all_data = all_data[all_data['candle_begin_time'] >= pd.to_datetime(begin_date)].copy()
        all_data.reset_index(inplace=True, drop=True)

        # 生成回测期、使用期的日期列表
        data_begin_date = all_data.iloc[0]['candle_begin_time']  # 可用数据的最早时间
        data_end_date = all_data.iloc[-1]['candle_begin_time']  # 可用数据的最晚时间

        if data_begin_date > pd.to_datetime(sys_para['begin_date']):
            print('可用数据仅开始于', pd.to_datetime(data_begin_date).strftime('%Y-%m-%d'))
            sys_para['begin_date'] = pd.to_datetime(data_begin_date).strftime('%Y-%m-%d')

        sys_para['end_date'] = data_end_date.strftime('%Y-%m-%d')
        train_list = gen_train_date(sys_para)

        # 回测出每个月月初往前数12个月的最优参数
        top_para_record = {'参数训练开始': [],
                           '参数训练结束': [],
                           '参数': []}

        for train_info in train_list:
            print('本轮回测开始时间为', train_info['train_begin_date'])
            print('本轮回测结束时间为', train_info['train_end_date'])

            # 截取训练数据
            condition1 = all_data['candle_begin_time'] >= pd.to_datetime(train_info['train_begin_date'])
            condition2 = all_data['candle_begin_time'] < pd.to_datetime(train_info['train_end_date'])
            train_df = all_data[condition1 & condition2].copy()
            train_df.reset_index(inplace=True, drop=True)

            # 回测计算最优参数（输出内容待优化）
            top_para = train(train_df, strategy_name, freq, para_list, train_info, rtn_path, process_num)
            train_info['best_para_trained'] = top_para
            print('寻找到的最优参数为', top_para)

            # 记录最优参数
            top_para_record['参数训练开始'].append(train_info['train_begin_date'])
            top_para_record['参数训练结束'].append(train_info['train_end_date'])
            top_para_record['参数'].append(top_para)

            print('*' * 20)

        # 输出每个月的最优参数
        top_para_df = pd.DataFrame(top_para_record)
        top_para_df.to_csv(rtn_path + r'/top_para_record.csv', encoding='utf_8_sig')

        # 根据最优参数计算每个月初往后的开仓、平仓、止损点
        trade_record = []
        for train_info in train_list:
            para = train_info['best_para_trained']  # 每个月应该使用的最优参数

            # 用该月的最优参数进行回测，找到开仓、平仓、止损点
            monthly_trade_record = find_change_time(all_data, para, strategy_name, train_info['train_end_date'], freq)
            print(monthly_trade_record)

            # 如果不是第一个月，就需要对交易进行筛选
            if train_list.index(train_info) > 0:
                last_trade_end_time = train_list[train_list.index(train_info) - 1]['trade_record'][-1]['trade_end_time']

                # 检验产生的所有交易，去除不符合要求的（交易开始时间早于前一个月的最后一笔交易的结束时间）
                for trade in monthly_trade_record:
                    if trade['trade_begin_time'] > last_trade_end_time:
                        trade_record.append(trade)
            else:
                trade_record += monthly_trade_record

            train_info['trade_record'] = monthly_trade_record

        # 根据交易记录生成信号df
        trade_df_list = []
        for trade in trade_record:
            trade_df_list.append([trade['trade_begin_time'], trade['long_or_short']])
            trade_df_list.append([trade['trade_end_time'], 0])
        trade_df = pd.DataFrame(trade_df_list, columns=['candle_begin_time', 'signal'])

        trade_df.sort_values(by='candle_begin_time', inplace=True)
        trade_df.drop_duplicates(subset='candle_begin_time', keep='last', inplace=True)

        # 输出交易记录
        trade_df.to_csv(rtn_path + r'/trade.csv')

        # 将信号df与原始数据合并
        all_data = all_data.merge(trade_df, how='outer', on='candle_begin_time')

        # 生成pos，后续计算资金曲线
        all_data['pos'] = all_data['signal'].shift()
        all_data['pos'].fillna(method='ffill', inplace=True)
        all_data['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

        # 计算资金曲线
        df = equity_curve_with_long_and_short(all_data, leverage_rate=3, c_rate=3.0 / 1000)
        print('最终收益为', df.iloc[-1]['equity_curve'])

        df.to_csv(rtn_path + r'/equity_curve.csv')


# ===非滚动回测主程序
def plain_main(pair, strategy_name, para_list, para_names, sys_para, process_num):

    begin_date = sys_para['begin_date']  # 整体回测开始时间（使用的数据的最早时间）
    end_date = sys_para['end_date']
    data_para = 'plain'

    # 设定路径，如果没有这个路径的话就生成
    path = get_data_path('output', strategy_name, '%s' % pair)
    if not os.path.exists(path):
        os.makedirs(path)

    # 根据不同的时间周期进行回测
    for freq in sys_para['freq']:

        rtn_path = get_data_path('output', strategy_name, '%s' % pair, freq, data_para)  # 用于保存完整的参数排名结果
        if not os.path.exists(rtn_path):
            os.makedirs(rtn_path)

        # 导入总数据
        all_data = pd.read_hdf(get_data_path('input', '%s.h5' % pair), key=freq)  # 数据文件存放路径

        all_data['candle_begin_time'] = pd.to_datetime(all_data['candle_begin_time'])

        # 选取时间段
        all_data = all_data[all_data['candle_begin_time'] >= pd.to_datetime(begin_date)].copy()
        all_data = all_data[all_data['candle_begin_time'] <= pd.to_datetime(end_date)].copy()

        all_data.reset_index(inplace=True, drop=True)

        train_info = {'train_end_date': ''}
        # 回测计算最优参数（输出内容待优化）
        top_para = train(all_data, strategy_name, freq, para_list, train_info, rtn_path, process_num)
        print('寻找到的最优参数为', top_para)


if __name__ == '__main__':

    # 策略名称
    strategy_name = 'bolling'

    # 这边需要手动输入参数名称
    para_names = ['n', 'm', '止损比例', '动态触发比例', '动态止盈比例', '时间级别', '策略收益', '最大回撤', '年化收益/最大回撤']

    # 最大进程数量
    process_num = 4

    # 回测系统参数
    sys_para = {'test_period': 2,  # 回测期长度，12个月
                'use_period': 1,  # 使用期长度，1个月
                'begin_date': '2017-10-01',  # 整体回测开始的时间，即使用数据的最早时间，如果早于数据最早的时间，则会自动使用数据最早的时间
                'end_date': '2019-05-01',
                'if_all_data': False,  # 如果为True，每轮回测都将使用所有历史数据，但至少一年
                'freq': ['1D']
                }

    # 待测参数组合。每次回测都相同
    n_list = range(10, 41, 1)
    m_list = [i/10 for i in range(10, 31)]


    # OCO和移动止盈的参数
    """
    stop_loss_pct = all_para['stop_loss_pct']  # 止损比例，没有除以100
    stop_profit_pct = all_para['stop_profit_pct']  # 止盈比例，没有除以100
    dynamic_trigger_pct = all_para['dynamic_trigger']  # 动态止盈触发比例，没有除以100
    dynamic_stop_pct = all_para['dynamic_stop_pct']  # 动态止盈回撤比例，没有除以100
    """
    stop_loss_pct_list = range(1, 10)

    dynamic_trigger_pct_list = range(95, 96)
    dynamic_stop_pct_list = [i/10 for i in range(4, 5)]

    # 生成参数列表
    para_list = []
    for n in n_list:
        for m in m_list:
            if n > m:
                for stop_lose_pct in stop_loss_pct_list:
                    for dynamic_trigger_pct in dynamic_trigger_pct_list:
                        for dynamic_stop_pct in dynamic_stop_pct_list:
                            stop_profit_pct = stop_lose_pct * 100000000000000000000000000
                            # 触发止盈必须比触发动态止盈更困难
                            if stop_profit_pct > dynamic_trigger_pct:
                                para_list.append([n, m, stop_lose_pct, dynamic_trigger_pct, dynamic_stop_pct])

    for pair in ['BTCUSD', 'ETHUSD']:  # 币种训练完成
        print('\n====%s====' % pair)
        rolling_main(pair, strategy_name, para_list, para_names, sys_para, process_num)
        # plain_main(pair, strategy_name, para_list, para_names, sys_para, process_num)
