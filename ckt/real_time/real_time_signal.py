import_signal = 'imported'

# ===海龟交易法则的实盘信号
def real_time_signal_turtle_without_stop_lose(now_pos, stop_lose_price, df, para):
    n1 = int(para[0])
    n2 = int(para[1])
    close_price = df.iloc[-1]['close']

    # 现在是多头仓位
    if now_pos == 1:
        # 依次计算是否要开空、平仓、止损
        df['open_close_low'] = df[['open', 'close']].min(axis=1)
        df['n1_low'] = df['open_close_low'].rolling(n1).min()
        df['n2_low'] = df['open_close_low'].rolling(n2).min()
        if close_price < df.iloc[-2]['n1_low']:
            target_pos = -1
        elif close_price < df.iloc[-2]['n2_low']:
            target_pos = 0
        # elif close_price < stop_lose_price:
        #     target_pos = 0
        else:
            target_pos = 1

    # 现在是空头仓位
    elif now_pos == -1:
        # 依次计算是否要开空、平仓、止损
        df['open_close_high'] = df[['open', 'close']].max(axis=1)
        df['n1_high'] = df['open_close_high'].rolling(n1).max()
        df['n2_high'] = df['open_close_high'].rolling(n2).max()
        if close_price > df.iloc[-2]['n1_high']:
            target_pos = 1
        elif close_price > df.iloc[-2]['n2_high']:
            target_pos = 0
        # elif close_price > stop_lose_price:
        #     target_pos = 0
        else:
            target_pos = -1
    # 现在无仓位
    elif now_pos == 0:
        df['open_close_high'] = df[['open', 'close']].max(axis=1)
        df['open_close_low'] = df[['open', 'close']].min(axis=1)
        df['n1_high'] = df['open_close_high'].rolling(n1).max()
        df['n1_low'] = df['open_close_low'].rolling(n1).min()

        if close_price > df.iloc[-2]['n1_high']:
            target_pos = 1
        elif close_price < df.iloc[-2]['n1_low']:
            target_pos = -1
        else:
            target_pos = 0
    # 其他情况报错
    else:
        raise ValueError('当前仓位变量now_pos只能是1， -1， 0，但目前是', now_pos)
    print(df)
    return target_pos


# ===不带止损的趋势布林线策略
def real_time_signal_bolling_without_stop_lose(now_pos, stop_lose_price, df, para):
    """
    布林线中轨：n天收盘价的移动平均线
    布林线上轨：n天收盘价的移动平均线 + m * n天收盘价的标准差
    布林线上轨：n天收盘价的移动平均线 - m * n天收盘价的标准差

    收盘价下穿上轨做空，到中轨平仓。上穿下轨做多，到中轨平仓。
    当且仅当上一次触及了中轨，才判断下一次是否要开仓

    另外，当价格往亏损方向超过百分之stop_lose的时候，平仓止损。
    :param now_pos:
    :param stop_lose_price:
    :param df:
    :param para:
    :return:
    """

    print(para)
    # ===计算指标
    n = int(para[0])
    m = para[1]
    close_price = df.iloc[-1]['close']
    close_price_before = df.iloc[-2]['close']

    # 只截取必要的部分，减少计算量。稍微留一点冗余
    df = df[-(n + 3):].copy()

    # 计算均线
    df['median'] = df['close'].rolling(n).mean()

    # 计算上轨、下轨道
    df['std'] = df['close'].rolling(n).std(ddof=0)  # ddof代表标准差自由度
    df['upper'] = df['median'] + m * df['std']
    df['lower'] = df['median'] - m * df['std']
    print(df)

    # 现在是多头仓位
    if now_pos == 1:
        # 依次计算是否需要平仓
        if close_price < df.iloc[-1]['lower']:
            target_pos = -1
        elif close_price < stop_lose_price:
            target_pos = 0
        elif close_price < df.iloc[-1]['mid']:
            target_pos = 0
        else:
            target_pos = 1

    # 现在是空头仓位
    elif now_pos == -1:
        # 依次计算是否需要平仓
        if close_price > df.iloc[-1]['upper']:
            target_pos = 1
        elif close_price > stop_lose_price:
            target_pos = 0
        elif close_price > df.iloc[-1]['mid']:
            target_pos = 0
        else:
            target_pos = -1

    # 现在无仓位
    elif now_pos == 0:

        # 开多信号，上穿上轨
        condition2 = close_price > df.iloc[-1]['upper']

        # 开空信号，下穿下轨
        condition4 = close_price < df.iloc[-1]['lower']
        if condition2:
            target_pos = 1
        elif condition4:
            target_pos = -1
        else:
            target_pos = 0
    else:
        raise ValueError('当前仓位变量now_pos只能是1， -1， 0，但目前是', now_pos)

    return target_pos


# ===带止损的趋势布林线策略
def real_time_signal_bolling_with_stop_lose(now_pos, stop_lose_price, df, para):
    """
    布林线中轨：n天收盘价的移动平均线
    布林线上轨：n天收盘价的移动平均线 + m * n天收盘价的标准差
    布林线上轨：n天收盘价的移动平均线 - m * n天收盘价的标准差

    收盘价下穿上轨做空，到中轨平仓。上穿下轨做多，到中轨平仓。
    当且仅当上一次触及了中轨，才判断下一次是否要开仓

    另外，当价格往亏损方向超过百分之stop_lose的时候，平仓止损。
    :param now_pos:
    :param stop_lose_price:
    :param df:
    :param para:
    :return:
    """
    print(para)
    # ===计算指标
    n = int(para[0])
    m = para[1]
    close_price = df.iloc[-1]['close']
    close_price_before = df.iloc[-2]['close']

    # 只截取必要的部分，减少计算量。稍微留一点冗余
    df = df[-(n + 3):].copy()

    # 计算均线
    df['median'] = df['close'].rolling(n).mean()

    # 计算上轨、下轨道
    df['std'] = df['close'].rolling(n).std(ddof=0)  # ddof代表标准差自由度
    df['upper'] = df['median'] + m * df['std']
    df['lower'] = df['median'] - m * df['std']
    print(df)

    # 现在是多头仓位
    if now_pos == 1:
        # 依次计算是否需要平仓
        if close_price < df.iloc[-1]['lower']:
            target_pos = -1
        elif close_price < stop_lose_price:
            target_pos = 0
        elif close_price < df.iloc[-1]['median']:
            target_pos = 0
        else:
            target_pos = 1

    # 现在是空头仓位
    elif now_pos == -1:
        # 依次计算是否需要平仓
        if close_price > df.iloc[-1]['upper']:
            target_pos = 1
        elif close_price > stop_lose_price:
            target_pos = 0
        elif close_price > df.iloc[-1]['median']:
            target_pos = 0
        else:
            target_pos = -1

    # 现在无仓位
    elif now_pos == 0:

        # 开多信号，上穿上轨
        condition2 = df['close'].iloc[-1] > df.iloc[-1]['upper']

        # 开空信号，下穿下轨
        condition4 = df['close'].iloc[-1] < df.iloc[-1]['lower']
        if condition2:
            target_pos = 1
        elif condition4:
            target_pos = -1
        else:
            target_pos = 0
    else:
        raise ValueError('当前仓位变量now_pos只能是1， -1， 0，但目前是', now_pos)
    print(df)
    return target_pos
