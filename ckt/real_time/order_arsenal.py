from ckt.real_time.global_var import *

market = get_market()

if market == 'bitfinex':
    from ckt.real_time.bitfinex_specified.function import *
elif market == 'tonghuashun':
    from ckt.real_time.tonghuashun_specified.function import *


# ===对手价往里2% 调仓
def inner_2pct_change(account, symbol, signal, opposite_price, start_time, leverage, money_available):
    # 确定调仓的方向，对手价
    if signal in ['long_open', 'short_close']:
        buy_or_sell = 'buy'

        price = opposite_price['lowest_ask']  # 卖盘对手价
    elif signal in ['short_open', 'long_close']:
        buy_or_sell = 'sell'
        price = opposite_price['highest_bid']  # 买盘对手价

    # 确定调仓的量
    # 如果是平仓信号
    if signal.endswith('close'):

        # 查看当前持仓量
        now_amount = check_now_amount_for_closing(account, symbol)

        # 平仓函数
        close_info = inner_2pct_close_position(account, symbol, buy_or_sell, price, now_amount)

        now_time = pd.to_datetime(time.time(), unit='s') + timedelta(hours=8)
        return {'order_info': close_info, 'symbol': symbol, 'type': 'close', 'start_time': start_time,
                'end_time': now_time, 'signal': signal}

    elif signal.endswith('open'):

        # 开仓函数，需要杠杆倍数
        open_info = inner_2pct_open_position(account, symbol, buy_or_sell, price, leverage, money_available)
        now_time = pd.to_datetime(time.time(), unit='s') + timedelta(hours=8)
        return {'order_info': open_info, 'symbol': symbol, 'type': 'open', 'start_time': start_time,
                'end_time': now_time, 'signal': signal}


# ===对手价往里2% 开仓
def inner_2pct_open_position(account, symbol, buy_or_sell, price, leverage, money_available):

    exchange = account['exchange']
    order_type = 'limit'

    # 下单价格，对手价往里2%
    if buy_or_sell == 'buy':
        order_price = price * 1.02
    elif buy_or_sell == 'sell':
        order_price = price * 0.98

    # 计算开仓量
    open_amount = round(money_available * leverage / order_price, 2)

    # 订单信息
    order_info_list= []

    # 下单
    order_info_list.append(place_order(exchange, order_type, buy_or_sell, symbol, price, open_amount))
    print('开仓完成', order_info_list)

    return {'order_list': order_info_list, 'opposite_price': price}


# ===对手价往里2% 平仓
def inner_2pct_close_position(account, symbol, buy_or_sell, price, now_amount):
    exchange = account['exchange']
    order_type = 'limit'

    # 下单价格，对手价往里2%
    if buy_or_sell == 'buy':
        order_price = price * 1.02
        holding_position = 'short'
    elif buy_or_sell == 'sell':
        order_price = price * 0.98
        holding_position = 'long'

    # 订单信息
    order_info_list = []

    # 下单
    order_info_list.append(place_order(exchange, order_type, buy_or_sell, symbol, order_price, now_amount))
    print('平仓完成', order_info_list)

    return {'order_list': order_info_list, 'holding_position': holding_position, 'opposite_price': price}


# ===对手价往里2%，带OCO 调仓
def inner_2pct_with_oco_change(account, symbol, signal, opposite_price, start_time, leverage, money_available, oco_para):
    # 确定调仓的方向，对手价
    if signal in ['long_open', 'short_close']:
        buy_or_sell = 'buy'
        price = opposite_price['lowest_ask']  # 卖盘对手价
    elif signal in ['short_open', 'long_close']:
        buy_or_sell = 'sell'
        price = opposite_price['highest_bid']  # 买盘对手价

    # 确定调仓的量
    # 如果是平仓信号
    if signal.endswith('close'):

        # 查看当前持仓量
        now_amount = check_now_amount_for_closing(account, symbol)

        # 平仓函数
        close_info = inner_2pct_close_with_oco_position(account, symbol, buy_or_sell, price, now_amount)

        now_time = pd.to_datetime(time.time(), unit='s') + timedelta(hours=8)
        return {'order_info': close_info, 'symbol': symbol, 'type': 'close', 'start_time': start_time,
                'end_time': now_time, 'signal': signal}

    elif signal.endswith('open'):

        # 开仓函数，需要杠杆倍数
        open_info = inner_2pct_open_with_oco_position(account, symbol, buy_or_sell, price, leverage, money_available, oco_para)
        now_time = pd.to_datetime(time.time(), unit='s') + timedelta(hours=8)
        return {'order_info': open_info, 'symbol': symbol, 'type': 'open', 'start_time': start_time,
                'end_time': now_time, 'signal': signal}


# ===对手价往里2% 开仓
def inner_2pct_open_with_oco_position(account, symbol, buy_or_sell, price, leverage, money_available, oco_para):
    stop_profit_pct = oco_para['stop_profit_pct']
    stop_loss_pct = oco_para['stop_loss_pct']

    exchange = account['exchange']
    order_type = 'limit'

    # 下单价格，对手价往里2%
    if buy_or_sell == 'buy':
        order_price = price * 1.02
        stop_profit_price = str(round(price * (1 + stop_profit_pct), 8))[:5]  # 多头止盈价格
        stop_loss_price = str(round(price * (1 - stop_loss_pct), 8))[:5]  # 多头止损价格

    elif buy_or_sell == 'sell':
        order_price = price * 0.98
        stop_profit_price = str(round(price * (1 - stop_profit_pct), 8))[:5]  # 空头止盈价格
        stop_loss_price = str(round(price * (1 + stop_loss_pct), 8))[:5]  # 空头止损价格

    # 计算开仓量
    open_amount = round(money_available * leverage / order_price, 2)

    # 订单信息
    order_info_list= []

    # 建仓下单
    order_info_list.append(place_order(exchange, order_type, buy_or_sell, symbol, price, open_amount))

    # OCO下单
    order_info = place_oco_order(account, symbol, open_amount, stop_profit_price, stop_loss_price, side=buy_or_sell)
    print('OCO下单完成：', order_info)
    print('开仓完成', order_info_list)

    return {'order_list': order_info_list, 'opposite_price': price}


# ===对手价往里2% 平仓
def inner_2pct_close_with_oco_position(account, symbol, buy_or_sell, price, now_amount):
    exchange = account['exchange']
    order_type = 'limit'

    # 下单价格，对手价往里2%
    if buy_or_sell == 'buy':
        order_price = price * 1.02
        holding_position = 'short'
    elif buy_or_sell == 'sell':
        order_price = price * 0.98
        holding_position = 'long'

    # 订单信息
    order_info_list = []

    # 平仓下单
    order_info_list.append(place_order(exchange, order_type, buy_or_sell, symbol, order_price, now_amount))

    # 撤掉本标的所有的单（OCO肯定有大于等于1个单剩余）
    cancel_symbol_order(account, symbol)

    print('平仓完成', order_info_list)

    return {'order_list': order_info_list, 'holding_position': holding_position, 'opposite_price': price}
