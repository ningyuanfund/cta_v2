import json
from websocket import create_connection
ws = create_connection("wss://api.bitfinex.com/ws", timeout=10)
ws.send(json.dumps({
    "event": "subscribe",
    "channel": "ticker",
    "pair": "BTCUSD",
}))


while True:
    result = ws.recv()
    result = json.loads(result)
    print("Received '%s'" % result)

