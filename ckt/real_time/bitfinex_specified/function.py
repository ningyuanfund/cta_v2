import json
import requests
import pandas as pd
from datetime import timedelta, datetime
import time

path = 'bitfinex_specified'


# ===sleep
def lets_sleep():
    time.sleep(30)


# ===查询仓位，如果空仓，则重置可用资金信息（使用了交易所API）
def check_position(account, symbol_info):

    now_position = account['exchange2'].private_post_auth_r_positions()

    # 读取本地保存的仓位信息
    position_df = pd.read_csv(path + '\\position_info.csv', encoding='utf_8_sig', index_col='symbol')

    if now_position == []:
        time.sleep(3)
        print('当前无持仓，将重置本地记录的各可用资金数量')

        # 获取账户内资金
        margin_info = account['exchange'].private_post_margin_infos()
        margin_balance = margin_info[0]['margin_balance']
        print('当前账户权益为', margin_balance)

        # 重置可用资金量，不需要的币可用资金量为0
        position_df['开仓前可用资金量'] = 0
        # 计算每个品种的可开数量，并重置其他信息
        for symbol in symbol_info:
            # 该品种的可使用资金
            balance_for_each = symbol_info[symbol]['proportion'] * float(margin_balance)

            position_df.loc[symbol, '开仓前可用资金量'] = balance_for_each
            position_df.loc[symbol, '开仓价'] = 0
            position_df.loc[symbol, '持仓浮动盈亏'] = 0
            position_df.loc[symbol, '持仓量'] = 0

        # 存储到本地
        position_df.to_csv(path + '\\position_info.csv')
        print(position_df)

    else:
        print('当前有持仓，根据交易所返回数据更新本地仓位信息')

        for symbol in symbol_info:
            # 先把本地持仓信息初始化
            position_df.loc[symbol, '开仓价'] = 0
            position_df.loc[symbol, '持仓浮动盈亏'] = 0
            position_df.loc[symbol, '持仓量'] = 0
            for position in now_position:
                # 如果有持仓信息
                if position[0].endswith(symbol.split('/')[0] + symbol.split('/')[1]):
                    # 该品种的可使用资金将不变，只有平仓时才会变
                    position_df.loc[symbol, '开仓价'] = position[3]
                    position_df.loc[symbol, '持仓浮动盈亏'] = position[6]
                    position_df.loc[symbol, '持仓量'] = position[2]

        # 获取账户内资金
        margin_info = account['exchange'].private_post_margin_infos()
        margin_balance = margin_info[0]['margin_balance']
        print('当前账户内可用资金为', margin_balance)

        position_df.to_csv(path + '\\position_info.csv')
        print(position_df)
    lets_sleep()


# ===获取某个品种的最新数据(使用了交易所API)
def get_latest_data(account, symbol, time_interval, para):

    exchange = account['exchange2']
    limit = 500  # 获取最大数量

    while True:
        try:
            content = exchange.fetch_ohlcv(symbol=symbol, timeframe=time_interval, limit=limit)
            break
        except Exception as e:
            print(e)
            time.sleep(5)

    df = pd.DataFrame(content, dtype=float)
    df.rename(columns={0: 'MTS', 1: 'open', 2: 'high', 3: 'low', 4: 'close', 5: 'volume'}, inplace=True)
    df['candle_begin_time'] = pd.to_datetime(df['MTS'], unit='ms')
    df['candle_begin_time_GMT8'] = df['candle_begin_time'] + timedelta(hours=8)
    df = df[['candle_begin_time', 'candle_begin_time_GMT8', 'open', 'high', 'low', 'close', 'volume']]
    print(df)

    lets_sleep()
    return df


# ===具体下单（使用了交易所API）
def place_order(exchange, order_type, buy_or_sell, symbol, price, amount):
    """
    下单
    :param exchange: 交易所
    :param order_type: limit, market
    :param buy_or_sell: buy, sell
    :param symbol: 买卖品种
    :param price: 当market订单的时候，price无效
    :param amount: 买卖量
    :return:
    """
    print('本次下单信息为：', order_type, buy_or_sell, symbol, price, amount)
    order_symbol = symbol.split('/')[0] + symbol.split('/')[1]

    print(order_symbol)
    for i in range(5):
        # try:
        # 限价单
        if order_type == 'limit':
            # 买
            if buy_or_sell == 'buy':
                order_info = exchange.create_limit_buy_order(symbol, amount, price, {'type': 'limit'})  # 买单
            # 卖
            elif buy_or_sell == 'sell':
                order_info = exchange.create_limit_sell_order(symbol, amount, price, {'type': 'limit'})  # 卖单
        # 市价单
        elif order_type == 'market':
            # 买
            if buy_or_sell == 'buy':
                order_info = exchange.create_market_buy_order(symbol, amount, {'type': 'market'})  # 买单
            # 卖
            elif buy_or_sell == 'sell':
                order_info = exchange.create_market_sell_order(symbol, amount, {'type': 'market'})  # 卖单
        else:
            pass

        print('下单成功：', order_type, buy_or_sell, symbol, price, amount)
        print('下单信息：', order_info, '\n')
        lets_sleep()
        return order_info

        # except Exception as e:
        #     print('下单报错，3s后重试', e)
        #     time.sleep(3)

    print('下单报错次数过多，下单线程终止')
    content = '执行下单报错，已终止线程。下单信息为：%s, %s, %s, %s, %s' % (symbol, buy_or_sell, order_type, price, amount)
    send_dingding_msg(content=content)
    exit()


# ===获取对手价（使用了交易所API）
def get_opposite_price(account, symbol):

    exchange = account['exchange2']
    get_symbol = 't' + symbol.split('/')[0] + symbol.split('/')[1]

    price_info = exchange.public_get_ticker_symbol(params={'symbol': get_symbol})

    opposite_price = {'highest_bid': price_info[0], 'lowest_ask': price_info[2]}

    lets_sleep()
    return opposite_price


# ===整理策略报表并发送（使用了交易所API）
def send_report(account):

    margin_info = account['exchange'].private_post_margin_infos()
    margin_balance = margin_info[0]['margin_balance']

    position_df = pd.read_csv(path + '\\position_info.csv')
    content = '当前账户权益为' + str(margin_balance) + '\n'
    for i in range(len(position_df)):
        content += position_df.at[i, 'symbol'] + '\n'
        content += '开仓前可用资金量: ' + str(position_df.at[i, '开仓前可用资金量']) + '\n'
        content += '开仓价: ' + str(position_df.at[i, '开仓价']) + '\n'
        content += '持仓量: ' + str(position_df.at[i, '持仓量']) + '\n'
        content += '持仓浮动盈亏: ' + str(position_df.at[i, '持仓浮动盈亏']) + '\n'
        content += '=' * 10 + '\n'
    send_dingding_msg(content)
    lets_sleep()


# ===发送钉钉消息，id填上使用的机器人的id
'e7ae7cc48dd987152cf748e1cc2973a722a01577653faad975e90b357bd6b084'
def send_dingding_msg(content, robot_id='e7ae7cc48dd987152cf748e1cc2973a722a01577653faad975e90b357bd6b084'):
    try:
        msg = {
            "msgtype": "text",
            "text": {"content": content + '\n' + datetime.now().strftime("%m-%d %H:%M:%S")}}
        headers = {"Content-Type": "application/json;charset=utf-8"}
        url = 'https://oapi.dingtalk.com/robot/send?access_token=' + robot_id
        body = json.dumps(msg)
        requests.post(url, data=body, headers=headers)
        print('成功发送钉钉')
    except Exception as e:
        print("发送钉钉失败:", e)


# ===根据订单号获取平均成交价格（使用了交易所API）
def get_avg_price(account, order_id):

    order_execution = account['exchange'].private_post_order_status(params={'order_id': int(order_id)})
    avg_price = float(order_execution['avg_execution_price'])  # 订单平均成交价格
    lets_sleep()

    return avg_price


# ===下单之后，检查五次，撤单
def place_check_cancel(exchange, order_type, buy_or_sell, symbol, price, amount):
    print('本次下单信息为：', order_type, buy_or_sell, symbol, price, '需下数量：', amount)

    for i in range(5):
        try:
            # 限价单
            if order_type == 'limit':
                # 买
                if buy_or_sell == 'buy':
                    order_info = exchange.create_limit_buy_order(symbol, amount, price, {'type': 'limit'})  # 买单
                # 卖
                elif buy_or_sell == 'sell':
                    order_info = exchange.create_limit_sell_order(symbol, amount, price, {'type': 'limit'})  # 卖单
            # 市价单
            elif order_type == 'market':
                # 买
                if buy_or_sell == 'buy':
                    order_info = exchange.create_market_buy_order(symbol, amount, {'type': 'market'})  # 买单
                # 卖
                elif buy_or_sell == 'sell':
                    order_info = exchange.create_market_sell_order(symbol, amount, {'type': 'market'})  # 卖单
            else:
                pass

            print('下单成功：', order_type, buy_or_sell, symbol, price, amount)
            print('下单信息：', order_info, '\n')

            try:
                count = 5
                order_id = order_info['id']
                for i in range(i):
                    order = exchange.fetch_order(id=order_id)
                    if order['statsus'] == 'closed':
                        print('已完全成交')
                        break
                    elif i == count-1:
                        cancel_info = exchange.cancel_order(id=order_id)
                        print('下单未完全成交，已撤单')
                    else:
                        print('未完全成交，5秒后再次查看')
                        time.sleep(10)
            except:
                pass
            lets_sleep()
            return exchange.fetch_order(id=order_id)

        except Exception as e:
            print('下单报错，3s后重试', e)
            time.sleep(3)

    print('下单报错次数过多，下单线程终止')
    content = '执行下单报错，已终止线程。下单信息为：%s, %s, %s, %s, %s' % (symbol, buy_or_sell, order_type, price, amount)
    send_dingding_msg(content=content)
    exit()


# ===查看当前交易所某个特定币的持仓量(使用了交易所API)
def check_now_amount_for_closing(account, symbol):
    exchange2 = account['exchange2']
    # 根据交易所的持仓量判断平仓数量
    now_position = exchange2.private_post_auth_r_positions()
    now_amount = 0
    for position in now_position:
        # 如果有持仓信息
        if position[0].endswith(symbol.split('/')[0] + symbol.split('/')[1]):
            now_amount = abs(position[2])  # 当前持仓量
    if now_amount == 0:
        print('警告，将要平仓的币持仓量为0，程序有误！！！')
        print('警告，将要平仓的币持仓量为0，程序有误！！！')
        print('警告，将要平仓的币持仓量为0，程序有误！！！')
    lets_sleep()
    return now_amount


# ===下OCO的单（使用了交易所API）
def place_oco_order(account, symbol, open_amount, stop_profit_price, stop_loss_price, side):
    '''

    :param account:
    :param symbol:ETH/USD -> trade_symbol: ETHUSD
    :param open_amount:
    :param stop_profit_price:
    :param stop_loss_price:
    :param side: 如果是开多头仓位，则止盈与止损都是sell，所以side是sell；如果是开空头仓位，则止盈与止损都是buy，所以side是buy
    :return:
    '''

    exchange = account['exchange']
    trade_symbol = symbol.replace('/', '')
    # 止盈止损OCO
    for i in range(5):
        try:
            order_info = exchange.private_post_order_new(params={'symbol': trade_symbol,
                                                                 'amount': str(open_amount),
                                                                 'price': stop_profit_price,
                                                                 'side': side,
                                                                 'type': 'limit',
                                                                 'ocoorder': True,
                                                                 'buy_price_oco': stop_loss_price})
            print('止盈止损成功：', )
            print('下单信息：', order_info, '\n')
            lets_sleep()
            return order_info
        except Exception as e:
            print('下单报错，1s后重试', e)
            time.sleep(5)


# ===撤掉本symbol现有的所有单
def cancel_symbol_order(account, symbol):
    '''

    :param account:
    :param symbol: ETH/USD -> status symbol: ethusd
    :return:
    '''
    exchange = account['exchange']
    status_symbol = symbol.replace('/', '').lower()  # ethusd
    print('撤掉%s所有的单' % symbol)

    info = exchange.private_post_orders()

    order_id_list = []
    for order in info:
        if order['symbol'] == status_symbol:
            order_id_list.append(order['id'])
    print('需要撤', order_id_list)
    if order_id_list != []:
        cancel_info = exchange.private_post_order_cancel_multi(params={'order_ids': order_id_list})
        print(cancel_info)
        lets_sleep()