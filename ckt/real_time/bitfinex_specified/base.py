import ccxt

# apiKey = 'scxIZEVc7hwB8GH7Qt9YDxYhGyrMTf5hS5lPMHV22Te'
# secret = 'B7DWq8E7GhnKhvjE8IjSzFjpMlfHGKkFBjiCbYnsVWI'
#
apiKey = 'YaMtirnvqoL5XxgEqWvDfLtB0bVYTXWUAF4FKQrh28l'
secret = 'KbktyiO8H3m0ZpAnqkTL5BBIASzcnkJN6KOZlaWBiqV'

# 账户设置
account = {
        'exchange': ccxt.bitfinex(
                    {
                        'apiKey': apiKey,
                        'secret': secret,
                    }),
        'exchange2': ccxt.bitfinex2(
                    {
                        'apiKey': apiKey,
                        'secret': secret,
                    }),
        }

account['exchange'].load_markets()
account['exchange2'].load_markets()

# 手动设置各个品种的资金使用比例，其他参数将从配置文件中读取
proportion_info = {
    'BTC/USD': {'proportion': 0.333, },
    'ETH/USD': {'proportion': 0.333, },
    'EOS/USD': {'proportion': 0.333, },
    'LTC/USD': {'proportion': 0.0, },
}

# 手动设置策略
strategy_dict = {
    'BTC/USD': 'bolling',
    'ETH/USD': 'bolling',
    'EOS/USD': 'bolling',
    'LTC/USD': 'bolling',
}

# 手动设置下单方式
order_method_dict = {
    'BTC/USD': 'inner_2pct',
    'ETH/USD': 'inner_2pct',
    'EOS/USD': 'inner_2pct',
    'LTC/USD': 'inner_2pct',
}

stop_para_dict = {
    'BTC/USD': {'stop_profit_pct': 20, 'stop_loss_pct': 5},
    'ETH/USD': {'stop_profit_pct': 20, 'stop_loss_pct': 5},
    'EOS/USD': {'stop_profit_pct': 20, 'stop_loss_pct': 5},
    'LTC/USD': {'stop_profit_pct': 20, 'stop_loss_pct': 5},
}


if __name__ == '__main__':
    # from ckt.real_time.global_var import *
    # set_market('bitfinex')
    # from ckt.real_time.public_function import *
    # exchange2 = account['exchange2']
    # exchange = account['exchange']
    import datetime
    import pandas as pd
    print(pd.to_datetime(datetime.datetime.now()).strftime('%H'))
    print(account['exchange2'].private_post_auth_r_positions())
    # df = get_latest_data(account, symbol, '1d', [20, 2, 5])
    # order_info = exchange.create_limit_buy_order(symbol, amount, price, {'type': 'exchange limit'})
    # print(order_info)

    # info = place_order(exchange, 'market', 'sell', 'LTC/USD', 100, 76.28427)
    # print(info)
    # info = exchange.cancel_order(id=22621107518)
    # print(info)

    # position = exchange.private_post_positions()
    # print(position)
    # close_info = exchange.private_post_position_close(params={'position_id':140058704})
    # print(close_info)
    # # now_position = account['exchange2'].private_post_auth_r_positions()
    #
    # position = exchange.private_post_positions()
    # print(position)
    #
    # position_info = exchange2.private_post_auth_r_positions()  # 从bfx交易所获取账户的持仓信息
    #
    # position_info = pd.DataFrame(position_info)
    # position_info.rename(columns={0: '交易对', 1: '状态', 2: '持仓量',
    #                               3: '成本价格', 4: '借币利息', 5: 'unknown1',
    #                               6: '损益', 7: '损益比例', 8: '爆仓价格', 9: 'unknown2'}, inplace=True)
    #
    # if len(position_info):
    #     position_info.drop(['状态', 'unknown1', 'unknown2'], axis=1, inplace=True)
    #
    # print(position_info)
