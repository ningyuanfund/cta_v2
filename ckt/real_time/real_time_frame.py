from ckt.real_time.global_var import *

import sys
sys.path.append(r'C:/Users/Administrator/Desktop/cta_v2/ckt//')

market = 'bitfinex'  # 使用哪个市场在此处决定
set_market(market)


if market == 'bitfinex':
    from ckt.real_time.public_function import *
    from ckt.real_time.bitfinex_specified.base import *
elif market == 'tonghuashun':
    from ckt.real_time.public_function import *
    from ckt.real_time.tonghuashun_specified.base import *


# ===主程序
def main():

    # 读取和设置参数
    symbol_info = set_para(proportion_info, strategy_dict, order_method_dict)

    # 如果是空仓，就重置可用资金信息(每个币种下次开仓可以使用的资金总量)；如果有持仓，把本地信息和交易所信息同步
    check_position(account, symbol_info)

    # 计算下次运行的时间，每5分钟运行一次，但不一定所有的品种都交易
    target_time = next_run_time('5T')

    # 每分钟更新持仓信息
    while True:
        if datetime.now() < target_time - timedelta(seconds=300):
            print('\n=====检查本地仓位信息并更新=====')
            check_position(account, symbol_info)  # 每分钟更新本地仓位信息
            time.sleep(120)
            continue
        else:
            break

    # 直到下次运行的时间再启动
    time.sleep(max(0, (target_time - datetime.now()).seconds))

    # 在靠近目标时间时
    while True:
        if datetime.now() < target_time:
            continue
        else:
            break

    # 根据每个品种的时间周期，查看这一轮是不是需要交易，返回本轮需要检查信号的symbol
    symbol_list = check_symbol(symbol_info)
    print('=' * 20)

    # 对需要运行的品种获取数据，并且读取当前仓位，计算目标仓位
    now_and_target_position_dict = get_data_and_cal_target_position(account, symbol_info, symbol_list, target_time)
    print(now_and_target_position_dict)
    # 根据目标仓位以及当前仓位，查看当前是否需要调仓，计算信号
    signal_dict = cal_signal(now_and_target_position_dict)

    if market == 'tonghuashun':
        # 根据信号dict进行循环调仓
        cycle_frame(account, symbol_info, signal_dict, order_method_dict)
    else:
        # 根据信号dict进行多线程调仓
        multi_threading_frame(account, symbol_info, signal_dict, order_method_dict)

    # 检查止损
    stop_signal_dict = check_stop(account, stop_para_dict)
    stop_signal_dict['time'] = time.time()
    print(stop_signal_dict)
    if stop_para_dict != {}:
        if market == 'tonghuashun':
            # 根据信号dict进行循环调仓
            cycle_frame(account, symbol_info, stop_signal_dict, order_method_dict)
        else:
            # 根据信号dict进行多线程调仓
            multi_threading_frame(account, symbol_info, stop_signal_dict, order_method_dict)

    # 再次检查仓位
    check_position(account, symbol_info)

    if pd.to_datetime(datetime.now()).strftime('%H') == '22':
        # 发送策略报表
        send_report(account)

    print('=' * 10, '本周期循环结束', '=' * 10)


if __name__ == '__main__':
    while True:
        try:
            main()
            time.sleep(10)
        except Exception as e:
            print('系统报错', e, '10秒后重试')
            send_dingding_msg(content='系统报错' + str(e))
            # raise Exception(e)
            time.sleep(10)
        # main()


"""
主程序
main加上try
next run time改成1D **
休息时间加长 300 120 **

public function
144行 symbol time interval改成1h **

function
lets sleep加长 30s **

config
时间周期改成1D **

base
下单方式改为inner_2pct_oco
"""
