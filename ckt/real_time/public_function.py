from ckt.real_time.order_arsenal import *

if market == 'bitfinex':
    from ckt.real_time.bitfinex_specified.base import *
elif market == 'tonghuashun':
    from ckt.real_time.tonghuashun_specified.base import *

market = get_market()

if market == 'bitfinex':
    from ckt.real_time.bitfinex_specified.function import *
elif market == 'tonghuashun':
    from ckt.real_time.tonghuashun_specified.function import *

from ckt.real_time.thread_get_result import *
from ckt.real_time.real_time_signal import *
pd.set_option('expand_frame_repr', False)
print(import_signal)


# ===读取和设置参数
def set_para(symbol_info, strategy_dict, order_method_dict):
    '''
    :return:

    {'ETH/USD': {'proportion': 0.5, 'para': [5.0, 2.0, 1.1], 'leverage': 3.0, 'time_interval': '5T',
                 'strategy': 'turtle', 'order_method': 'oco'},
     'EOS/USD': {'proportion': 0.3, 'para': [5.0, 2.0, 2.0], 'leverage': 2.0, 'time_interval': '5T',
                 'strategy': 'bolling', 'order_method': 'oco'}}
    '''

    # 从配置文件中读取参数
    config_df = pd.read_csv(path + '\\config.csv', encoding='utf_8')
    config_df.columns = ['symbol', 'leverage', 'para', 'time_interval']
    config_df.set_index('symbol', inplace=True)

    for key in symbol_info:
        # 读取策略参数
        para = config_df.loc[key, 'para'][1:-1].split(',')
        symbol_info[key]['para'] = [float(x) for x in para]
        # 读取杠杆倍数
        symbol_info[key]['leverage'] = config_df.loc[key, 'leverage']
        # 读取时间级别
        symbol_info[key]['time_interval'] = config_df.loc[key, 'time_interval']
        # 读取策略
        symbol_info[key]['strategy'] = strategy_dict[key]
        # 读取下单方式
        symbol_info[key]['order_method'] = order_method_dict[key]

    print('当前参数为：')
    print(config_df)
    print('启用的品种为：', list(symbol_info.keys()))

    return symbol_info


# ===查看哪些品种这一轮需要交易
def check_symbol(symbol_info):

    now_minute = str(datetime.now().minute)
    now_hour = str(datetime.now().hour)
    print('当前为UTC+8', now_hour, '时', now_minute, '分')
    check_dict = {'5T': ['0', '5', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55'],
                  '15T': ['0', '15', '30', '45'],
                  '30T': ['0', '30'],
                  '1H': ['0'],
                  '1D': ['0']
                  }

    checked_list = []
    for symbol in symbol_info:
        if symbol_info[symbol]['time_interval'] == '1D':
            if now_hour == '8':
                checked_list.append(symbol)
        elif now_minute in check_dict[symbol_info[symbol]['time_interval']]:
            checked_list.append(symbol)
    print('本周期需要检验的有：', checked_list)

    return checked_list


# ===下次运行的时间
def next_run_time(time_interval, ahead_time=1):
    """
        :param time_interval 运行周期时间 建议不要小于5min
        :param ahead_time 当小于1s时 自动进入下个周期
        :return:
        """
    if time_interval.endswith('T'):
        now_time = datetime.now()
        time_interval = int(time_interval.strip('T'))

        target_min = (int(now_time.minute / time_interval) + 1) * time_interval
        if target_min < 60:
            target_time = now_time.replace(minute=target_min, second=0, microsecond=0)
        else:
            if now_time.hour == 23:
                target_time = now_time.replace(hour=0, minute=0, second=0, microsecond=0)
                target_time += timedelta(days=1)
            else:
                target_time = now_time.replace(hour=now_time.hour + 1, minute=0, second=0, microsecond=0)

        # sleep直到靠近目标时间之前
        if (target_time - datetime.now()).seconds < ahead_time + 1:
            print('距离target_time不足', ahead_time, '秒，下下个周期再运行')
            target_time += timedelta(minutes=time_interval)
        print('下次运行时间', target_time)
        return target_time
    else:
        exit('time_interval doesn\'t end with T')


# ===获取所有币种的最新数据，并且计算目标仓位
def get_data_and_cal_target_position(account, symbol_info, symbol_list, run_time):

    now_and_target_position_dict = {}

    # 读取本地保存的仓位信息
    position_df = pd.read_csv(path + '\\position_info.csv', encoding='utf_8_sig')
    position_df.columns = ['symbol', '开仓前可用资金量', '开仓价', '持仓浮动盈亏', '持仓量']
    position_df.set_index('symbol', inplace=True)

    for symbol in symbol_list:
        strategy_name = symbol_info[symbol]['strategy']

        # 这里写的太死了
        if symbol_info[symbol]['time_interval'] in ['5T', '15T', '30T']:
            symbol_time_interval = symbol_info[symbol]['time_interval'].strip('T') + 'm'
            lag_minute = int(symbol_time_interval.strip('m'))
        elif symbol_info[symbol]['time_interval'] == '1H':
            symbol_time_interval = '1h'
            lag_minute = 60
        elif symbol_info[symbol]['time_interval'] == '1D':
            symbol_time_interval = '1d'
            lag_minute = 24 * 60

        para = symbol_info[symbol]['para']

        max_try_amount = 5

        # 获取数据
        for i in range(max_try_amount):
            # 获取最新数据
            df = get_latest_data(account, symbol, symbol_time_interval, para)
            # 判断是否包含最新的数据
            print(run_time)
            # _temp = df[df['candle_begin_time_GMT8'] == (run_time - timedelta(minutes=5))]
            _temp = df[df['candle_begin_time_GMT8'] == run_time]
            if _temp.empty:
                print('获取数据不包含最新的数据，重新获取')
                # send_dingding_msg('获取不到最新数据，重新获取')
                time.sleep(3)
                if i == (max_try_amount - 1):
                    period_df = pd.DataFrame()
                    # send_dingding_msg('没有获取到' + str(symbol) + '的数据，该货币跳过本周期')
            else:
                df = df[df['candle_begin_time'] < pd.to_datetime(run_time)]  # 去除target_time周期的数据
                period_df = df.resample(rule=symbol_info[symbol]['time_interval'],
                                        on='candle_begin_time', base=0, label='left',
                                        closed='left').agg(
                    {'open': 'first',
                     'high': 'max',
                     'low': 'min',
                     'close': 'last',
                     'volume': 'sum',
                     })
                period_df = period_df[:-1].copy()
                period_df = period_df.fillna(method='ffill')
                break

        # 读取当前仓位
        if position_df.loc[symbol, '持仓量'] > 0:
            now_pos = 1
        elif position_df.loc[symbol, '持仓量'] < 0:
            now_pos = -1
        else:
            now_pos = 0

        # 如果有持仓，就计算止损价，否则将设置为0
        stop_lose_pct = symbol_info[symbol]['para'][-1]/100  # 止损价是para的最后一位
        open_price = position_df.loc[symbol, '开仓价']  # 本地的仓位信息中会保存开仓价，如果该品种没有仓位将是0

        # 计算止损价
        if now_pos == 1:
            stop_lose_price = open_price * (1 - stop_lose_pct)
        elif now_pos == -1:
            stop_lose_price = open_price * (1 + stop_lose_pct)
        else:
            stop_lose_price = 0
        print(symbol, now_pos, stop_lose_price)
        print(symbol_info)
        if len(period_df) != 0:
            if order_method_dict[symbol] == 'inner_2pct_oco':
                # 根据当前仓位、实盘信号函数计算目标仓位
                function_str = 'real_time_signal_%s_without_stop_lose(now_pos, stop_lose_price, period_df, para)' % strategy_name
                target_pos = eval(function_str)
            elif order_method_dict[symbol] == 'inner_2pct':
                function_str = 'real_time_signal_%s_with_stop_lose(now_pos, stop_lose_price, period_df, para)' % strategy_name
                target_pos = eval(function_str)

            # 把该品种的目标仓位保存在字典里
            now_and_target_position_dict[symbol] = {'now_pos': now_pos, 'target_pos': target_pos}
        else:
            now_and_target_position_dict[symbol] = {'now_pos': now_pos, 'target_pos': now_pos}

    return now_and_target_position_dict


# ===根据当前仓位和目标仓位，计算信号
def cal_signal(now_and_target_position_dict):

    signal_dict = {}
    for symbol in now_and_target_position_dict:
        now_pos = now_and_target_position_dict[symbol]['now_pos']
        target_pos = now_and_target_position_dict[symbol]['target_pos']

        # 根据当前、目标仓位计算信号。信号list中需要先平仓信号，再开仓信号
        if now_pos == 1 and target_pos == 0:  # 平多
            signal_dict[symbol] = ['long_close']
        elif now_pos == -1 and target_pos == 0:  # 平空
            signal_dict[symbol] = ['short_close']
        elif now_pos == 0 and target_pos == 1:  # 开多
            signal_dict[symbol] = ['long_open']
        elif now_pos == 0 and target_pos == -1:  # 开空
            signal_dict[symbol] = ['short_open']
        elif now_pos == 1 and target_pos == -1:  # 平多，开空
            signal_dict[symbol] = ['long_close', 'short_open']
        elif now_pos == -1 and target_pos == 1:  # 平空，开多
            signal_dict[symbol] = ['short_close', 'long_open']
        else:
            signal_dict[symbol] = []

        # 牛市做空，最为致命
        # if 'short_open' in signal_dict[symbol]:
        #     signal_dict[symbol].remove('short_open')

    print('本周期调仓信号为', signal_dict)

    # 记录调仓信号的时间
    signal_record_df = pd.read_csv(path + '\\signal_record.csv', encoding='utf_8_sig')
    now_time = pd.to_datetime(time.time(), unit='s') + timedelta(hours=8)

    if signal_dict != {}:
        for symbol in signal_dict:
            for signal in signal_dict[symbol]:
                signal_record_df.loc[symbol, 'time'] = now_time
                signal_record_df.loc[symbol, 'symbol'] = symbol
                signal_record_df.loc[symbol, 'type'] = signal

    signal_record_df = signal_record_df[['time', 'symbol', 'type']]
    signal_record_df.to_csv(path + '\\signal_record.csv', encoding='utf_8_sig')
    signal_dict['time'] = now_time

    return signal_dict


# ===调仓
def change_position(account, symbol, signal_list, leverage, start_time, money_available, order_method):

    # 获取对手价
    opposite_price = get_opposite_price(account, symbol)
    print(signal_list)
    # 依次处理所有信号
    for signal in signal_list:
        # # 确定调仓的方向，对手价
        # if signal in ['long_open', 'short_close']:
        #     buy_or_sell = 'buy'
        #     price = opposite_price['lowest_ask']  # 卖盘对手价
        # elif signal in ['short_open', 'long_close']:
        #     buy_or_sell = 'sell'
        #     price = opposite_price['highest_bid']  # 买盘对手价
        #
        # # 确定调仓的量
        # # 如果是平仓信号
        # if signal.endswith('close'):
        #     # 平仓函数
        #     close_info = close_position(account, symbol, buy_or_sell, price)
        #
        #     now_time = pd.to_datetime(time.time(), unit='s') + timedelta(hours=8)
        #     return {'order_info': close_info, 'symbol': symbol, 'type': 'close', 'start_time': start_time, 'end_time': now_time, 'signal': signal}
        #
        # elif signal.endswith('open'):
        #
        #     # 开仓函数，需要杠杆倍数
        #     open_info = open_position(account, symbol, buy_or_sell, price, leverage, money_available)
        #     now_time = pd.to_datetime(time.time(), unit='s') + timedelta(hours=8)
        #     return {'order_info': open_info, 'symbol': symbol, 'type': 'open', 'start_time': start_time, 'end_time': now_time, 'signal': signal}

        if order_method == 'inner_2pct':
            return inner_2pct_change(account, symbol, signal, opposite_price, start_time, leverage, money_available)
        elif order_method == 'inner_2pct_oco':
            oco_para = get_oco_para(symbol)
            return inner_2pct_with_oco_change(account, symbol, signal, opposite_price, start_time, leverage, money_available, oco_para)
        elif order_method == 'fake':
            return {'order_info': {'order_list': [], 'opposite_price': 0}, 'symbol': symbol, 'type': 'open', 'start_time': start_time,
                    'end_time': '', 'signal': signal}


# ===把交易、持仓信息记录在本地。由于要和本地文件以及df交互，这部分需要在子线程外
def record_down(account, change_position_list):

    # 读取交易记录
    trade_record_df = pd.read_csv(path + '\\trade_record.csv', encoding='utf_8_sig', index_col=['order_id'])
    # 读取持仓
    position_df = pd.read_csv(path + '\\position_info.csv', encoding='utf_8_sig', index_col='symbol')

    # 每一次调仓的信息
    position_change_content = ''
    open_position_change_content = '=== KhaZix Msg ===\n'
    for change in change_position_list:
        order_list = change['order_info']['order_list']
        symbol = change['symbol']
        signal = change['signal']
        signal_time = change['start_time']
        fulfill_time = change['end_time']

        # 每一次调仓中的每一次下单
        for order in order_list:
            order_id = order['id']
            exe_amount = abs(float(order['amount']))  # 成交总量
            avg_price = get_avg_price(account, order_id)  # 平均成交价
            order_info = trade_record(account, symbol, order_id, avg_price, exe_amount, signal)

            trade_record_df.loc[order_id, 'symbol'] = order_info['symbol']
            trade_record_df.loc[order_id, 'buy_or_sell'] = order_info['buy_or_sell']
            trade_record_df.loc[order_id, 'avg_price'] = order_info['avg_price']
            trade_record_df.loc[order_id, 'amount'] = order_info['amount']
            trade_record_df.loc[order_id, 'fee'] = order_info['fee']

            # 如果是平仓，修改该币种的可用资金
            if change['type'] == 'close':
                open_price = position_df.loc[symbol, '开仓价']
                close_price = avg_price
                # 如果是多头平仓，那么就应该下卖单。那么损益就应该是 exe_amount * (close_price - open_price)
                # 如果是空头平仓，那么就应该下买单。那么损益就应该是 exe_amount * (open_price - close_price)
                pl = 0
                if signal == 'long_close':
                    pl = exe_amount * (close_price - open_price)
                elif signal == 'short_close':
                    pl = exe_amount * (open_price - close_price)

                position_df.loc[symbol, '开仓前可用资金量'] += pl

                # 添加报告
                position_change_content += symbol + '平仓！' + '\n'
                position_change_content += '原持仓方向：' + change['order_info']['holding_position'] + '\n'
                position_change_content += '平均平仓价格为：' + str(round(float(avg_price), 2)) + '\n'
                position_change_content += '理论平仓价格为：' + str(change['order_info']['opposite_price']) + '\n'
                position_change_content += '平仓量为：' + str(exe_amount) + '\n'
                position_change_content += '信号出现时间：' + str(signal_time)[:19] + '\n'
                position_change_content += '下单完成时间：' + str(fulfill_time)[:19] + '\n'

                open_position_change_content += symbol + '平仓！' + '\n'
                open_position_change_content += '原持仓方向：' + change['order_info']['holding_position'] + '\n'
                open_position_change_content += '平均平仓价格为：' + str(round(float(avg_price), 2)) + '\n'
                open_position_change_content += '理论平仓价格为：' + str(change['order_info']['opposite_price']) + '\n'
                open_position_change_content += '信号出现时间：' + str(signal_time)[:19] + '\n'
                open_position_change_content += '下单完成时间：' + str(fulfill_time)[:19] + '\n'

            # 如果是开仓，修改本地记录
            elif change['type'] == 'open':
                position_df.loc[symbol, '开仓价'] = float(avg_price)  # 开仓价记录在本地
                position_df.loc[symbol, '持仓量'] = float(order['amount'])  # 持仓量记录在本地
                position_df.loc[symbol, '持仓浮动盈亏'] = 0

                if signal == 'long_open':
                    buy_or_sell = 'buy'
                elif signal == 'short_open':
                    buy_or_sell = 'sell'

                # 添加报告
                position_change_content += symbol + '开仓！' + '\n'
                position_change_content += '开仓方向：' + buy_or_sell + '\n'
                position_change_content += '平均开仓价格为：' + str(avg_price) + '\n'
                position_change_content += '理论开仓价格为：' + str(change['order_info']['opposite_price']) + '\n'
                position_change_content += '开仓量为：' + str(exe_amount) + '\n'
                position_change_content += '信号出现时间：' + str(signal_time)[:19] + '\n'
                position_change_content += '下单完成时间：' + str(fulfill_time)[:19] + '\n'

                open_position_change_content += symbol + '开仓！' + '\n'
                open_position_change_content += '开仓方向：' + buy_or_sell + '\n'
                open_position_change_content += '平均开仓价格为：' + str(avg_price) + '\n'
                open_position_change_content += '理论开仓价格为：' + str(change['order_info']['opposite_price']) + '\n'
                open_position_change_content += '信号出现时间：' + str(signal_time)[:19] + '\n'
                open_position_change_content += '下单完成时间：' + str(fulfill_time)[:19] + '\n'

        position_df.to_csv(path + '\\position_info.csv', encoding='utf_8_sig')
        trade_record_df.to_csv(path + '\\trade_record.csv')

    # if position_change_content == '':
    #     position_change_content = '本周期无下单！'

    if position_change_content != '':
        # 发送调仓信息
        send_dingding_msg(position_change_content)

        send_dingding_msg(open_position_change_content, robot_id='16e5d66cd153a5cdb4c7a33e505e46d21817bf444e00379a58c98e0c396ca436')


# ===记录下单信息
def trade_record(account, symbol, order_id, avg_price, exe_amount, signal):
    # request_symbol = 't' + symbol.split('/')[0] + symbol.split('/')[1]
    #
    # max_try_amount = 5
    # count = 0

    # while True:
    #     trade_info = account['exchange2'].private_post_auth_r_order_symbol_id_trades({'symbol': request_symbol,
    #                                                                                   'id': int(order_id)})
    #     print(trade_info)
    #     print(order_id)
    #     if trade_info != []:
    #         break
    #     elif count == max_try_amount:
    #         content = '获取下单信息次数过多，依然无法获取，请检查！'
    #         print(content)
    #         send_dingding_msg(content)
    #         break
    #     else:
    #         print('下单信息更新有延迟，10秒后重试')
    #         time.sleep(10)
    #         count += 1

    if signal in ['long_open', 'short_close']:
        buy_or_sell = 'buy'
    elif signal in ['short_open', 'long_close']:
        buy_or_sell = 'sell'

    fee = 0  # 手续费

    print('本次:', signal, '成交总量：', exe_amount, '本次成交均价：', avg_price, '暂时无法统计手续费')

    order_info = {'order_id': order_id,
                  'buy_or_sell': buy_or_sell,
                  'symbol': symbol,
                  'avg_price': avg_price,
                  'amount': exe_amount,
                  'fee': fee}

    return order_info


# ===多线程下单框架
def multi_threading_frame(account, symbol_info, signal_dict, order_method_dict):
    threads = []  # 全部线程

    # 生成信号的时间
    start_time = signal_dict['time']

    # 读取本地的持仓信息
    position_df = pd.read_csv(path + '\\position_info.csv', encoding='utf_8_sig', index_col='symbol')

    # 本周期的全部调仓信息
    change_position_list = []
    for symbol in symbol_info:
        # 如果本周期需要检验，而且有信号
        if symbol in signal_dict:
            if symbol != 'time':
                if signal_dict[symbol]:
                    order_method = order_method_dict[symbol]  # 下单方式
                    leverage = symbol_info[symbol]['leverage']  # 杠杆倍数

                    # 读取可用资金（这里可能会有一些不精确，因此所有品种的比例之和不能为1，需要冗余）
                    money_available = float(position_df.loc[symbol, '开仓前可用资金量'])

                    t = MyThread(change_position, args=(account, symbol, signal_dict[symbol], leverage,
                                                        start_time, money_available, order_method))
                    threads.append(t)

    # 开始线程
    for i in range(len(threads)):
        threads[i].start()

    # 等待所有线程结束再继续
    for i in range(len(threads)):
        threads[i].join()
        change_position_list.append(threads[i].get_result())

    # 把持仓信息，下单信息记录在本地，发送报表
    record_down(account, change_position_list)


# ===循环下单框架
def cycle_frame(account, symbol_info, signal_dict, order_method_dict):
    change_list = []  # 全部调仓

    # 生成信号的时间
    start_time = signal_dict['time']

    # 读取本地的持仓信息
    position_df = pd.read_csv(path + '\\position_info.csv', encoding='utf_8_sig', index_col='symbol')

    # 本周期的全部调仓信息
    change_position_list = []
    for symbol in symbol_info:
        # 如果本周期需要检验，而且有信号
        if symbol in signal_dict:
            if symbol != 'time':
                if signal_dict[symbol]:
                    order_method = symbol_info[symbol]['order_method']  # 下单方式
                    leverage = symbol_info[symbol]['leverage']  # 杠杆倍数

                    # 读取可用资金（这里可能会有一些不精确，因此所有品种的比例之和不能为1，需要冗余）
                    money_available = float(position_df.loc[symbol, '开仓前可用资金量'])

                    # change = [account, symbol, signal_dict[symbol], leverage, start_time, money_available]
                    change = {'account': account, 'symbol': symbol, 'signal_list': signal_dict[symbol],
                              'leverage': leverage, 'start_time': start_time, 'money_available': money_available}
                    change_list.append(change)

    # 开始循环
    for change in change_list:
        change_position_list.append(change_position(account=change['account'], symbol=change['symbol'],
                                                    signal_list=change['signal_list'], leverage=change['leverage'],
                                                    start_time=change['start_time'],
                                                    money_available=change['money_available'],
                                                    order_method=order_method))

    # 把持仓信息，下单信息记录在本地，发送报表
    record_down(account, change_position_list)


# ===获取OCO参数
def get_oco_para(symbol):

    oco_para = stop_para_dict[symbol]
    print(oco_para)

    return oco_para


# ===计算是否要止损
def check_stop(account, stop_para_dict):
    position_df = pd.read_csv(path + '\\position_info.csv', encoding='utf_8_sig', index_col='symbol')
    ticker_symbol = ''
    for symbol in stop_para_dict.keys():
        ticker_symbol += 't' + symbol.replace('/', '') + ','
    ticker_symbol.strip(',')

    tick_price = account['exchange2'].public_get_tickers(params={'symbols': 'tBTCUSD,tETHUSD,tEOSUSD,tXRPUSD,tLTCUSD'})
    ticker_price = {}
    for tick in tick_price:
        ticker_price[tick[0]] = tick[7]

    signal_dict = {}
    for symbol in stop_para_dict.keys():
        t_symbol = 't' + symbol.replace('/', '')
        now_price = ticker_price[t_symbol]

        if position_df['持仓量'].loc[symbol] > 0:
            stop_loss_price = position_df['开仓价'].loc[symbol] * (1 - stop_para_dict[symbol]['stop_loss_pct'] / 100)
            stop_profit_price = position_df['开仓价'].loc[symbol] * (1 + stop_para_dict[symbol]['stop_profit_pct'] / 100)
            print(symbol, stop_profit_price, stop_loss_price, now_price)
            if (now_price < stop_loss_price) and (stop_loss_price > 0):
                print(symbol, '止损', now_price, stop_loss_price)
                signal_dict[symbol] = ['long_close']
            elif (now_price > stop_profit_price) and (stop_profit_price > 0):
                print(symbol, '止盈', now_price, stop_profit_price)
                signal_dict[symbol] = ['long_close']

        elif position_df['持仓量'].loc[symbol] < 0:
            stop_loss_price = position_df['开仓价'].loc[symbol] * (1 + stop_para_dict[symbol]['stop_loss_pct'] / 100)
            stop_profit_price = position_df['开仓价'].loc[symbol] * (1 - stop_para_dict[symbol]['stop_profit_pct'] / 100)
            print(symbol, stop_profit_price, stop_loss_price, now_price)
            if now_price > stop_loss_price:
                print(symbol, '止损', now_price, stop_loss_price)
                signal_dict[symbol] = ['short_close']
            elif now_price < stop_profit_price:
                print(symbol, '止盈', now_price, stop_profit_price)
                signal_dict[symbol] = ['short_close']
    print('本周期止损信号为', signal_dict)
    return signal_dict


if __name__ == '__main__':
    signal_dict = check_stop(account, stop_para_dict)
