import easytrader

# 使用同花顺客户端进行下单
user = easytrader.use('ths')

# 填入交易客户端的路径。即安装目录下的 xiadan.exe 的地址
user.connect(r'D:\同花顺\xiadan.exe')

account = {'user': user}

# 手动设置各个品种的资金使用比例，其他参数将从配置文件中读取
proportion_info = {
    # 'BTC/USD': {'proportion': 0.01, },
    159922: {'proportion': 0.6, },
}

# 手动设置策略
strategy_dict = {
    # 'BTC/USD': 'bolling,
    159922: 'turtle',
}

# 手动设置下单方式
order_method_dict = {
    # 'BTC/USD': 'inner_2pct',
    159922: 'inner_2pct',
}
