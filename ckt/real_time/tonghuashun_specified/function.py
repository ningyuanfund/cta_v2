import pandas as pd
from ckt.real_time.tonghuashun_specified.base import *
import easyquotation
import time
from datetime import timedelta, datetime
import tushare as ts
import re
import json
import requests
import numpy as np
from urllib.request import urlopen, Request

path = 'tonghuashun_specified'

pd.set_option('expand_frame_repr', False)

quotation = easyquotation.use('sina')
easyquotation.update_stock_codes()  # 更新股票代码


# ===发送钉钉消息，id填上使用的机器人的id（没有使用交易所API，但是为了 import 方便就放在这边）
def send_dingding_msg(content, robot_id=''):
    try:
        msg = {
            "msgtype": "text",
            "text": {"content": content + '\n' + datetime.now().strftime("%m-%d %H:%M:%S")}}
        headers = {"Content-Type": "application/json;charset=utf-8"}
        url = 'https://oapi.dingtalk.com/robot/send?access_token=' + robot_id
        body = json.dumps(msg)
        requests.post(url, data=body, headers=headers)
        print('成功发送钉钉')
    except Exception as e:
        print("发送钉钉失败:", e)


# ===具体下单（使用了交易所API）
def place_order(user, order_type, buy_or_sell, security, price, amount):
    '''
    下单函数，包含买和卖
    :param user:
    :param buy_or_sell:买或卖 buy/sell
    :param type: 限价或市价 limit/market
    :param price: 价格，如果市价单可以不传
    :param amount: 股数，应该为100的倍数
    :return:
    '''

    free_balance = user.balance['资金余额']
    volume = int(float(price) * amount)
    position_list = user.position

    if quotation.real(str(security)) == {}:
        raise Exception('没有查询到要操作的股票信息！')
    if volume == 0:
        raise Exception('价格或交易量不能为0！如果是限价单请传入价格。')
    if amount % 100 != 0:
        raise Exception('amount需要是100的倍数！')

    # 买入委托
    if buy_or_sell == 'buy':
        # 看账户可用资金是否足够
        if free_balance < volume:
            raise Exception('没有足够的现金进行操作！')
        if order_type == 'limit':
            order_info = user.buy(security=str(security), price=price, amount=amount)
        elif order_type == 'market':
            order_info = user.market_buy(security=str(security), amount=amount)
        else:
            raise Exception('下单方式只能为 limit 或 market！')

    # 卖出委托
    elif buy_or_sell == 'sell':
        # 看可卖出的股票数量是否足够
        sellable_amount = 0
        for position in position_list:
            if position['证券代码'] == security:
                sellable_amount = position['可用余额']
        if amount > sellable_amount:
            raise Exception('可卖出数量不足！')
        if type == 'limit':
            order_info = user.sell(security=str(security), price=price, amount=amount)
        elif type == 'market':
            order_info = user.market_sell(security=str(security), amount=amount)
        else:
            raise Exception('下单方式只能为 limit 或 market！')

    else:
        raise Exception('buy_or_sell 只能为 buy 或者 sell！')

    # 如果下单成功，返回的信息格式将是{'entrust_no': ...}，返回
    if 'entrust_no' in order_info:
        print('下单成功', order_info)
    else:
        print('下单未返回合同号，有可能有误。请检查下单客户端')

    return order_info


# ===查询仓位，如果空仓，则重置可用资金信息（使用了交易所API）
def check_position(account, symbol_info):
    user = account['user']
    now_position = user.position
    symbol_list = []
    for symbol in symbol_info:
        symbol_list.append(str(symbol))

    # 读取本地保存的仓位信息
    position_df = pd.read_csv(path + '\\position_info.csv', encoding='utf_8_sig', dtype={'symbol': 'str'})
    position_df.set_index('symbol', inplace=True)

    if now_position == []:
        print('当前无持仓，将重置本地记录的各可用资金数量')

        # 获取账户内资金
        balance_info = user.balance
        money_available = balance_info['可用金额']
        print('当前账户内可用资金为', money_available)

        # 重置可用资金量，不需要交易的股票可用资金量为0
        position_df['开仓前可用资金量'] = 0

        # 计算每只股票的可开数量，并重置其他信息
        for symbol in symbol_info:
            # 该股票的可使用资金
            balance_for_each = symbol_info[symbol]['proportion'] * float(money_available)

            position_df.loc[str(symbol), '开仓前可用资金量'] = balance_for_each
            position_df.loc[str(symbol), '开仓价'] = 0
            position_df.loc[str(symbol), '持仓浮动盈亏'] = 0
            position_df.loc[str(symbol), '持仓量'] = 0

    else:
        not_holding = True
        for position in now_position:
            if position[r'证券代码'] in symbol_list:
                not_holding = False
                break

        # 虽然不是空仓，但是持仓中没有目标股票
        if not_holding:
            print('当前无持仓，将重置本地记录的各可用资金数量')

            # 获取账户内资金
            balance_info = user.balance
            money_available = balance_info['可用金额']
            print('当前账户内可用资金为', money_available)

            # 重置可用资金量，不需要交易的股票可用资金量为0
            position_df['开仓前可用资金量'] = 0

            # 计算每只股票的可开数量，并重置其他信息
            for symbol in symbol_info:
                # 该股票的可使用资金
                balance_for_each = symbol_info[symbol]['proportion'] * float(money_available)

                position_df.loc[str(symbol), '开仓前可用资金量'] = balance_for_each
                position_df.loc[str(symbol), '开仓价'] = 0
                position_df.loc[str(symbol), '持仓浮动盈亏'] = 0
                position_df.loc[str(symbol), '持仓量'] = 0

        # 有持仓
        else:

            print('当前有持仓，根据交易所返回数据更新本地仓位信息')

            for symbol in symbol_info:
                # 先把本地持仓信息初始化
                position_df.loc[str(symbol), '开仓价'] = 0
                position_df.loc[str(symbol), '持仓浮动盈亏'] = 0
                position_df.loc[str(symbol), '持仓量'] = 0
                for position in now_position:
                    # 如果有持仓信息
                    if position['证券代码'] == symbol:
                        # 该品种的可使用资金将不变，只有平仓时才会变
                        position_df.loc[str(symbol), '开仓价'] = position['成本价']
                        position_df.loc[str(symbol), '持仓浮动盈亏'] = position['盈亏']
                        position_df.loc[str(symbol), '持仓量'] = position['股票余额']

        # 获取账户内资金
        balance_info = user.balance
        money_available = balance_info['可用金额']
        print('当前账户内可用资金为', money_available)

    # 存储到本地
    position_df.to_csv(path + '\\position_info.csv')
    print(position_df)


# ===获取行情时需要使用
def _random(n=13):
    from random import randint
    start = 10**(n-1)
    end = (10**n)-1
    return str(randint(start, end))


# ===获取某只股票的最新数据（使用了交易所API）
def get_latest_data(account, symbol, time_interval, para):
    '''
    df = get_latest_data('', 'sz159922', '5m', [10])
    :param account:
    :param symbol:
    :param para:
    :return:
    '''

    limit = int(max(para) + 5)
    max_try_amount = 5
    count = 0

    # 生成完整的代码
    full_symbol = gen_full_symbol(symbol)

    if time_interval.endswith('m') & (time_interval != '1m'):
        ktype = time_interval.strip('m')
        dataflag = 'm%s' % ktype
    elif time_interval in ['D', 'M', 'Y']:
        ktype = time_interval
        tt_k_type = {'D': 'day', 'W': 'week', 'M': 'month'}
        dataflag = tt_k_type[ktype.upper()]

    else:
        raise Exception('时间周期有误')

    kline_url = 'http://ifzq.gtimg.cn/appstock/app/kline/mkline?param=%s,m%s,,640&_var=m%s_today&r=0.%s'
    url = kline_url % (full_symbol, ktype, ktype, _random(16))

    while True:
        try:
            request = Request(url)
            lines = urlopen(request, timeout=10).read()
            count += 1
            if len(lines) < 100:
                return None
            if count == max_try_amount:
                print('get_latest_data函数获取次数过多，中止获取')
                return pd.DataFrame

        except Exception as e:
            print(e)
            time.sleep(5)

        else:
            lines = lines.decode('utf-8')
            lines = lines.split('=')[1]
            reg = re.compile(r',{"nd.*?}')
            lines = re.subn(reg, '', lines)
            js = json.loads(lines[0])

            if len(js['data'][full_symbol][dataflag]) == 0:
                return None

            if len(js['data'][full_symbol][dataflag][0]) == 6:
                df = pd.DataFrame(js['data'][full_symbol][dataflag],
                                  columns=['candle_begin_time', 'open', 'close', 'high', 'low', 'volume'])
            else:
                df = pd.DataFrame(js['data'][full_symbol][dataflag],
                                  columns=['candle_begin_time', 'open', 'close', 'high', 'low', 'volume', 'amount',
                                           'turnoverratio'])

            if ktype in ['5', '15', '30', '60']:
                df['candle_begin_time'] = df['candle_begin_time'].map(lambda x: '%s-%s-%s %s:%s' % (x[0:4], x[4:6],
                                                                                                    x[6:8], x[8:10],
                                                                                                    x[10:12]))
            for col in df.columns[1:6]:
                df[col] = df[col].astype(float)

            df = df[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']][-limit:].copy()
            df.rename(columns={'candle_begin_time': 'candle_begin_time_GMT8'}, inplace=True)
            df.reset_index(inplace=True, drop=True)
            df['candle_begin_time_GMT8'] = pd.to_datetime(df['candle_begin_time_GMT8'])
            return df


# ===获取对手价（使用了交易所API）
def get_opposite_price(account, symbol):
    '''

    :param account:
    :param symbol: '600010'
    :return:
    '''

    price_info = quotation.real(str(symbol))[str(symbol)]

    opposite_price = {'highest_bid': price_info['bid1'], 'lowest_ask': price_info['ask1']}

    return opposite_price


# ===平仓(使用了交易所API）
def close_position(account, symbol, buy_or_sell, price, order_method):
    '''

    :param account:
    :param symbol: 600010
    :param buy_or_sell:
    :param price:
    :return:
    '''

    user = account['user']

    # 下单价格，对手价往里2%
    if buy_or_sell == 'buy':
        order_price = price * 1.02
        holding_position = 'short'
    elif buy_or_sell == 'sell':
        order_price = price * 0.98
        holding_position = 'long'

    now_position = user.position
    for position in now_position:
        # 如果有持仓信息
        if position['证券代码'] == symbol:
            now_amount = abs(position['股份可用'])  # 当前持仓量

    # 订单信息
    order_info_list = []

    # 下单
    order_info_list.append(place_order(user, 'limit', buy_or_sell, symbol, order_price, now_amount))

    print('平仓完成', order_info_list)

    return {'order_list': order_info_list, 'holding_position': holding_position, 'opposite_price': price}


# ===开仓（使用了交易所API）
def open_position(account, symbol, buy_or_sell, price, leverage, money_available, order_method):

    user = account['user']

    balance = user.balance
    print(balance)

    # 下单价格，对手价往里2%
    if buy_or_sell == 'buy':
        order_price = price * 1.02
    elif buy_or_sell == 'sell':
        order_price = price * 0.98

    # 计算开仓量
    open_amount = int(money_available * leverage / order_price / 100) * 100

    # 订单信息
    order_info_list= []

    # 下单
    order_info_list.append(place_order(user, 'limit', buy_or_sell, symbol, order_price, open_amount))
    print('开仓完成', order_info_list)

    return {'order_list': order_info_list, 'opposite_price': price}


# ===整理策略报表并发送（使用了交易所API）
def send_report(account):

    user = account['user']
    balance_info = user.balance
    balance = balance_info['资金余额']

    position_df = pd.read_csv(path + '\\position_info.csv', encoding='utf_8_sig')
    content = '当前账户权益为' + str(balance) + '\n'
    for i in range(len(position_df)):
        content += str(position_df.at[i, 'symbol']) + '\n'
        content += '开仓前可用资金量: ' + str(position_df.at[i, '开仓前可用资金量']) + '\n'
        content += '开仓价: ' + str(position_df.at[i, '开仓价']) + '\n'
        content += '持仓量: ' + str(position_df.at[i, '持仓量']) + '\n'
        content += '持仓浮动盈亏: ' + str(position_df.at[i, '持仓浮动盈亏']) + '\n'
        content += '=' * 10 + '\n'
    send_dingding_msg(content)


# ===根据订单号获取平均成交价格（使用了交易所API）
def get_avg_price(account, order_id):

    user = account['user']
    today_trades = user.today_trades

    avg_price = 0
    for trade in today_trades:
        if trade['委托序号'] == order_id:
            avg_price = trade['成交价格']

    return avg_price


# ===生成完整股票代码
def gen_full_symbol(symbol):
    symbol = str(symbol)
    sh_list = ['500', '600', '601', '900', '126', '110', '510']
    sz_list = ['000', '200', '002', '300', '184', '150', '159']
    if symbol == '000300':
        return 'sh' + symbol
    elif symbol[:3] in sh_list:
        return 'sh' + symbol
    elif symbol[:3] in sz_list:
        return 'sz' + symbol
    elif symbol[:2] == '11':
        return 'sz' + symbol
    else:
        raise Exception('代码有误')
