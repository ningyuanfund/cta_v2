'''

'''

# coding = utf-8

import ccxt
import ccxt
import time
import datetime
import json
import requests
import pandas as pd
import os
import sys
from trade import cta_arsenal as ctaa

# 获取当前程序的地址
current_file = __file__

# 程序根目录地址
root_path = os.path.abspath(os.path.join(current_file, os.pardir, os.pardir))

# 输入数据根目录地址
input_data_path = os.path.abspath(os.path.join(root_path, 'data', 'input_data'))

# 输出数据根目录地址
output_data_path = os.path.abspath(os.path.join(root_path, 'data', 'output_data'))

pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 10000)


# ----------------------------------------------------

exchange = ccxt.bitfinex({'rateLimit': 10000, 'enableRateLimit': True})
exchange.load_markets()

exchange.apiKey = 'F0Pk1zkN3OksomdMZySy0ImoC6mS3tDcTTPJ9eh9PY9'
exchange.secret = 'DgRAY8rQcjgLdl3xnl3ZOomjMgIH1vwBUQonWcmRYaK'


# ===主程序一，计算和更新调仓依据
def gen_main_signal(currency_list, time_interval, para_dict, cta_name):

    # 更新K线之前，将所有目标本地文件更新至最新
    for currency in currency_list:
        try:
            para = para_dict[currency][:2]  # 策略参数 第一位为rolling期数，第二位为单侧轨道宽度（标准差个数）
            currency = currency + '/usd'  # 交易品种
            upper_symbol = currency.upper() + 'T'  # 即后面用到的price symbol ETH/USDT
            if not os.path.exists(output_data_path):
                os.makedirs(output_data_path)
            local_data_path = output_data_path + '\\' + currency[:3] + '_candles.csv'

            # 读取本地文件
            try:
                df = pd.read_csv(local_data_path)
                df = df[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']]

            except:
                df = pd.DataFrame(columns=['candle_begin_time', 'open', 'high', 'low', 'close', 'volume'])
                df.to_csv(local_data_path)

            # 最少需要的K线数量
            least_lines = int(time_interval[:-1]) * para[0]
            # 当前行数
            now_lines = len(df)
            # 同步数据到满足最小要求
            if now_lines < least_lines:
                print(currency + ' k线数量不够，继续获取')
                # 获取k线直到数量满足最小要求
                # requests参数表示要求的种类，min代表更新数据的要求是“满足最小行数”
                refresh_data(exchange, para, time_interval, upper_symbol, local_data_path)
                # df = fct.update_candle(exchange, upper_symbol, least_lines, local_data_path, 'min', time_interval)
                # df.to_csv(local_data_path)
            # ===同步数据到最新
            refresh_data(exchange, para, time_interval, upper_symbol, local_data_path)
            # fct.update_candle(exchange, upper_symbol, least_lines, local_data_path, 'min', time_interval)
            time.sleep(30)
        except Exception as e:
            print(e)
            time.sleep(10)

    # 更新完所有目标K线后，依次刷新
    while True:
        # 依次获取各个币种的K线，每次获取成功后，sleep 20s
        for currency in currency_list:
            # print(currency+'get K lines!')
            print('******************** ' + currency + ' update K lines  '+str(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))+'*******************')
            try:
                para = para_dict[currency][:2]  # 策略参数 第一位为rolling期数，第二位为单侧轨道宽度（标准差个数）
                currency = currency+'/usd'  # 交易品种
                upper_symbol = currency.upper() + 'T'  # 即后面用到的price symbol ETH/USDT

                # ===本地文件地址
                local_data_path = output_data_path + '\\' + currency[:3] + '_candles.csv'
                signal_loc = output_data_path + '\\' + currency[:3] + '_'+cta_name + '_signal.csv'

                # 获取本地文件
                df = pd.read_csv(local_data_path)

                len_before_update = len(df)
                # 要求二，本地文件的行数是time interval的整数倍分钟
                # 这里余数为1代表新的k线已经产生，旧的k线彻底完结

                # print(currency+' 获取最新K线！'+str(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))))

                # 一次获取7根，然后去重
                now = exchange.milliseconds()
                from_timestamp = now - 7 * 60000

                # 开始更新
                ohlcvs = exchange.fetch_ohlcv(upper_symbol, '1m', from_timestamp)
                ohlcvs_df = pd.DataFrame(ohlcvs)
                ohlcvs_df.columns = ['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']

                # 去重
                df = df.append(ohlcvs_df, sort=False)
                df.drop_duplicates(['candle_begin_time'], keep='last', inplace=True)
                df.reset_index(inplace=True)
                df = df[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']]


                # print(df)
                len_after_update = len(df)
                # print('here')
                added_len = len_after_update - len_before_update
                # 输出保存
                df.to_csv(local_data_path)
                # print(refresh_symbol+' 当前本地记录文件有', len(df), '行')
                # print(currency + ' 最新K线获取成功！'+str(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))))
                #
                # print(2)

                period = int(time_interval[:-1])
                #根据累计K线增量判断是否应当计算
                condition1,deduct = signal_mark(added_len,period,signal_loc)


                # print(3)
                if condition1:
                    judge_or_not = True
                else:
                    # print(currency + '距下次更新调仓标准还差' + str(period-deduct) + '根K线')
                    judge_or_not = False
                # judge_or_not = True
                if judge_or_not == True:

                    print(currency + ' 计算调仓标准！'+str(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))))

                    # 去掉最后deduct行
                    df = df[:-int(deduct)]
                    # print(4)
                    # 获得least lines +1 行，因为需要计算上一根time interval级别k线的收盘价，因此必须多一行
                    rule_type = time_interval
                    df['candle_begin_time'] = pd.to_datetime(df['candle_begin_time'], unit='ms')
                    # print(5)
                    period_df = df.resample(rule=rule_type,
                                            on='candle_begin_time',
                                            label='left',
                                            closed='left').agg({'open': 'first',
                                                                'high': 'max',
                                                                'low': 'min',
                                                                'close': 'last',
                                                                'volume': 'sum'})
                    period_df.fillna(method='ffill', inplace=True)
                    # 产生交易信号,保存到本地
                    # print(6)
                    function_name = 'ctaa.signal_%s_gen(period_df, para, signal_loc)' % cta_name
                    eval(function_name)
                    print(currency + ' 调仓标准更新成功！'+str(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))))


                #本地文件过大时，清理部分陈旧K线
                local_df = pd.read_csv(local_data_path)
                least_lines = int(time_interval[:-1]) * para[0]
                if len(local_df) > least_lines * 4:
                    local_df = local_df[-2*least_lines:]
                    local_df.to_csv(local_data_path)
                time.sleep(40)
            except Exception as e:
                print(e)
                time.sleep(10)
                print(currency+'failed!')


# ===主程序二，实时监控调仓
def main_trade(currency_list,para_dict,usage_dict,cta_name,usage):
    leverage = 3  # 杠杆倍数

    def trade(currency,para_dict,usage_dict,cta_name,usage):
        moving_stop_loc = output_data_path + r'\moving_stop_profit.csv'
        # 读取本地储存的用于判断移动止盈的数据
        profit_data = read_profit_data(moving_stop_loc)

        base_coin = 'usd'  # usd
        trade_coin = currency  # eth
        trade_symbol = trade_coin.upper() + base_coin.upper()  # ETHUSD
        status_symbol = trade_symbol.lower()  # ethusd

        moving_stop_profit_start = para_dict[currency][-1] # 盈利到达止损点的n倍
        moving_stop_profit_backdraw = para_dict[currency][-2] # 较最大盈利回撤百分比
        stop_loss_pct =para_dict[currency][-4] #价格向不利方向移动百分比

        # para_dict['eth'] = [170, 3, 0.02, 0.2, 0.1, 6]
        # 信号默认为空
        signal_list = []

        judge_or_not = True

        # 同步账户状态
        try:
            now_position, now_amount = get_now_status(exchange, status_symbol)
        except:
            print(currency + ' 无法获得账户信息，跳过')

        signal_loc = output_data_path + '\\' + currency[:3] + '_'+cta_name+'_signal.csv'

        # 读取本地调仓依据
        try:
            df = pd.read_csv(signal_loc)

        except:
            print(currency + ' 本地无调仓信号记录，跳过！')

            time.sleep(600)

        #如果有持仓，则不断监控是否应该平仓
        if now_position != 'empty':

            print(currency+' '+now_position)
            act = {}
            act['long'] = 'buy'
            act['short'] = 'sell'

            # 根据持仓状况获取交易价格
            now_price = get_price(exchange,trade_coin.upper()+'/USDT',act[now_position])


            # 开多时
            if now_position == 'long':

                #更新历史最大盈利，判断是否移动止盈
                profit_max = profit_data[currency]['max_profit']
                open_price = profit_data[currency]['open']
                profit_now = (now_price-open_price)/open_price
                # print(profit_max,profit_now)
                if profit_now > profit_max:

                    profit_data[currency]['max_profit'] = profit_now
                    write_profit_data(moving_stop_loc, profit_data, currency)
                if profit_max > moving_stop_profit_start*stop_loss_pct:# 当产生可观的盈利时，启动动态止盈,计算出动态止盈价格
                    print('动态止盈检查ON')
                    moving_stop_profit_price = open_price * (profit_max*(1-moving_stop_profit_backdraw)+1)
                    profit_data[currency]['moving_stop_price']= moving_stop_profit_price
                    write_profit_data(moving_stop_loc, profit_data, currency)
                    if now_price < moving_stop_profit_price:# 若当前价格低于移动止盈
                        signal_list = kidding_or_not(exchange, trade_coin, moving_stop_profit_price, 'long_close')
                        if signal_list != []:
                            print('执行动态止盈！')
                            send_dingding_msg('执行动态止盈！')
                else:
                    print('动态止盈检查OFF')

                # 判断是否满足平仓条件
                funtcion_name = 'ctaa.change_%s_position(exchange,now_position,trade_coin,now_price,df)' % cta_name
                signal_list = eval(funtcion_name)




            #开空时
            if now_position == 'short':

                # 更新历史最大盈利，判断是否移动止盈
                profit_max = profit_data[currency]['max_profit']
                open_price = profit_data[currency]['open']
                profit_now = (open_price - now_price) / open_price
                if profit_now > profit_max:
                    profit_data[currency]['max_profit'] = profit_now
                    write_profit_data(moving_stop_loc, profit_data, currency)
                if profit_max > moving_stop_profit_start * stop_loss_pct:  # 当产生可观的盈利时，启动动态止盈,计算出动态止盈价格
                    moving_stop_profit_price = open_price * (1-profit_max * (1 - moving_stop_profit_backdraw))
                    profit_data[currency]['moving_stop_price'] = moving_stop_profit_price
                    write_profit_data(moving_stop_loc, profit_data,currency)
                    print('动态止盈检查ON，动态止盈价：'+str(round(moving_stop_profit_price,4)))
                    if now_price < moving_stop_profit_price:  # 若当前价格低于移动止盈
                        signal_list = kidding_or_not(exchange, trade_coin, moving_stop_profit_price, 'short_close')
                        if signal_list != []:
                            print('执行动态止盈！')
                            send_dingding_msg('执行动态止盈！')
                else:
                    print('动态止盈检查OFF')

                # 判断是否满足策略平仓条件
                funtcion_name = 'ctaa.change_%s_position(exchange,now_position,trade_coin,now_price,df)' % cta_name
                signal_list = eval(funtcion_name)


        #如果不持仓，则不断监控是否应该开仓
        if now_position == 'empty':
            write_profit_data(moving_stop_loc, profit_data, currency, reset='on')
            now_price = 0

            funtcion_name = 'ctaa.change_%s_position(exchange,now_position,trade_coin,now_price,df)' % cta_name
            signal_list = eval(funtcion_name)


        #根据信号判断是否需要调仓
        if signal_list != []:

            if 'close' in signal_list[0]:
                write_profit_data(moving_stop_loc, profit_data, currency, reset='on')

            print(currency+' 需要调仓，当前：', now_position, now_amount)
            print(signal_list[0]+str(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))))
            usage_real = usage * usage_dict[currency]  # 该币种可以使用的开仓金额实际比例
            stop_loss_pct = para_dict[currency][2]  # 止损比例。用于计算止损价格
            stop_profit_pct = para_dict[currency][3]  # 止盈比例。用于计算止盈价格
            price_symbol = trade_coin.upper() + '/' + base_coin.upper() + 'T' # ETH/USDT
            ordered_price = change_position(exchange, status_symbol, trade_symbol, price_symbol, now_position, now_amount, signal_list, leverage, usage_real, stop_loss_pct, stop_profit_pct)
            # 交易完成后，需要sleep超过1分钟的时间，并进行refresh，防止在该分钟内进行多次交易。
            write_profit_data(moving_stop_loc, profit_data, currency, record_open=ordered_price)
            time.sleep(30)
        else:
            # print(currency+' 暂无需调仓！'+str(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))))
            time.sleep(10)

    while True:
        try:
            for currency in currency_list:
                print('------------------------checking ' +currency+' '+str(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time())))+'-----------------------------')
                trade(currency,para_dict,usage_dict,cta_name,usage)
                time.sleep(20)
        except Exception as e:
            print(e)


# def send_dingding_msg(content, robot_id='c03c7e51d458595d2c436e687048b509b3527e17f37e2dc8e8c936e7b691d66d'):
#     try:
#         msg = {
#             "msgtype": "text",
#             "text": {"content": content + '\n' + datetime.datetime.now().strftime("%m-%d %H:%M:%S")}}
#
#         Headers = {"Content-Type": "application/json ;charset=utf-8 "}
#         url = 'https://oapi.dingtalk.com/robot/send?access_token=' + robot_id
#         body = json.dumps(msg)
#         requests.post(url, data=body, headers=Headers)
#     except Exception as err:
#         print('钉钉发送失败', err)


def rights_dingding_notice():
    import threading
    import datetime

    #加载交易所
    exchange = ccxt.bitfinex({'rateLimit': 10000, 'enableRateLimit': True})
    exchange.load_markets()
    exchange.apiKey = 'F0Pk1zkN3OksomdMZySy0ImoC6mS3tDcTTPJ9eh9PY9'
    exchange.secret = 'DgRAY8rQcjgLdl3xnl3ZOomjMgIH1vwBUQonWcmRYaK'
    print("connect!")

    loc = output_data_path + '\\rights_from20181218.csv'
    try:
        df = pd.read_csv(loc)
        df = df[['time','right','eos','eth','xrp']]
    except:
        df = pd.DataFrame(columns=['time','right','eos','eth','xrp'])
        df.at[0,'time'] = '0000'
        df.to_csv(loc)

    def usd_amount(balance2):
        for i in range(len(balance2)):

            condition04 = (balance2[i]['currency'] == 'usd')
            condition02 = (balance2[i]['type'] == 'trading')

            if condition04 and condition02:
                usd_amounts = float(balance2[i]['amount'])
                return usd_amounts

    def th1():
        counts_before_send = 0
        while True:
            #运行一轮，增加1
            counts_before_send += 1
            try:
                #获取时间信息
                second = time.strftime('%S', time.localtime(time.time()))
                minute = time.strftime('%M', time.localtime(time.time()))
                hour = time.strftime('%H', time.localtime(time.time()))

                should_I_record = False # 默认不记录，每个小时记录一次
                # print(df)
                recorded_time = df.iloc[-1][0][-2:]  # 当最后一行的记录的时间与现在不一致，说明这个小时还没有记录过
                if hour != recorded_time:
                    should_I_record = True
                # print(1)

                # 判断是否持仓
                balance = exchange.private_post_positions()
                balance2 = exchange.fetch_balance()['info']
                # print(balance2)

                if balance == []:
                    condition01 = int(hour) == 0
                    condition00 = int(minute) == 1
                    condition30 = int(second) < 40
                    usd_amounts = 0
                    for i in range(len(balance2)):
                        condition04 = (balance2[i]['currency'] == 'usd')
                        condition02 = (balance2[i]['type'] == 'trading')
                        if condition04 and condition02:
                            usd_amounts = float(balance2[i]['amount'])
                            break
                    profit_start = 7726.08
                    profit_value = round(usd_amounts - profit_start,2)
                    profit_total_pct = str(round((profit_value/profit_start)*100,2))+'%'
                    content = '--------------例行日报------------\n'
                    content += 'POSITION: N/A\n'
                    content += 'RIGHTS：'+str(round(usd_amounts,2))+'\n'
                    content += 'PROFIT FROM 20190103: '+str(profit_value)+' ['+profit_total_pct+']'
                    print(content)

                    if should_I_record == True:
                        lastrow = len(df)
                        df.at[lastrow,'time'] = str(time.strftime('%Y-%m-%d %H',time.localtime(time.time())))
                        usd_amounts = usd_amount(balance2)
                        df.at[lastrow,'right'] = usd_amounts
                        df.at[lastrow,'eos'] = 0
                        df.at[lastrow,'eth'] = 0
                        df.at[lastrow,'xrp'] = 0
                        df.to_csv(loc)
                    if condition00 and condition01 and condition30:
                        send_dingding_msg(content,
                                          robot_id='e7ae7cc48dd987152cf748e1cc2973a722a01577653faad975e90b357bd6b084')

                    time.sleep(30)

                else:
                    account_dict = {}

                    for dict in balance:
                        symbol = dict['symbol'][:3]

                        # 方向&开仓量
                        amount = float(dict['amount'])
                        if amount >0:
                            position = 'long'
                        else:
                            position = 'short'

                        # 开仓价格&现价
                        price_open = round(float(dict['base']),4)
                        price_now =round(get_price(exchange,symbol.upper()+'/USDT','sell'),4)

                        # 使用保证金
                        deposit = round(abs(amount)*price_open/3,2)

                        # profit&pct
                        profit = round(float(dict['swap']) + float(dict['pl']), 2)
                        profit_pct = ' ['+str(round(profit/deposit*100,4))+'%]'

                        open_time = datetime.datetime.utcfromtimestamp(float(dict['timestamp'])+8*60*60).strftime("%Y-%m-%d %H:%M:%S")
                        account_dict[symbol] = [profit,open_time, position,price_open,price_now,deposit,profit_pct]

                    usd_amounts = 0
                    for i in range(len(balance2)):

                        condition04 = (balance2[i]['currency'] == 'usd')
                        condition02 = (balance2[i]['type'] == 'trading')

                        if condition04 and condition02:
                            usd_amounts = float(balance2[i]['amount'])
                            break
                    # print(usd_amounts)
                    content = 'TREND_FOLLOWING_STG:'+'\n'
                    for symbol in account_dict:
                        content += '---------------'+symbol.upper()+'--------------'+'\n'
                        content += 'OPEN TIME: '+account_dict[symbol][1]+'\n'
                        content += 'OPEN PRICE：'+str(account_dict[symbol][3])+'\n'
                        content += 'NOW PRICE：'+str(account_dict[symbol][4])+'\n'
                        content += 'POSITION: '+ account_dict[symbol][2] +'\n'
                        content += 'MARGIN: ' + str(account_dict[symbol][5]) + '\n'
                        content += 'PROFIT: ' +str(account_dict[symbol][0])+account_dict[symbol][6]+'\n'
                        usd_amounts += round(int(account_dict[symbol][0]),0)
                    content += '---------------ALL---------------'+'\n'
                    profit_start = 7726.08
                    profit_value = round(usd_amounts - profit_start,2)
                    profit_total_pct = str(round((profit_value / profit_start ) * 100, 2)) + '%'

                    content += 'RIGHTS: ' + str(round(usd_amounts,2))+'       '+'\n'
                    content += 'PROFIT FROM 20190103: ' + str(profit_value) + ' [' + profit_total_pct + ']\n'
                    content += '*************************'
                    # print(counts_before_send)

                    #每隔一个小时记录一次
                    if should_I_record == True:
                        lastrow = len(df)
                        df.at[lastrow, 'time'] = str(time.strftime('%Y-%m-%d %H', time.localtime(time.time())))
                        df.at[lastrow, 'right'] = usd_amounts
                        currency_list = ['eos','eth','xrp']
                        position_convert = {'long':1,'short':-1}
                        for currency in currency_list:
                            if currency in account_dict:
                                df.at[lastrow, currency] = position_convert[account_dict[currency][2]]
                            else:
                                df.at[lastrow, currency] = 0
                        df.to_csv(loc)

                    print(content)
                    print(str(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))))
                    # print(counts_before_send)
                    if counts_before_send > 30:
                        counts_before_send=0
                        send_dingding_msg(content,
                                          robot_id='e7ae7cc48dd987152cf748e1cc2973a722a01577653faad975e90b357bd6b084')

                    time.sleep(30)

            except Exception as e:
                print('ERROR:', e)

    t1 = threading.Thread(target=th1)
    t1.start()


# =====从交易所获取账户信息
def get_balance_info(exchange, base_coin, trade_coin):
    balance = exchange.fetch_balance()['info']
    base_coin_amount = 0
    trade_coin_amount = 0
    for info in balance:
        if (info['currency'] == base_coin) & (info['type'] == 'trading'):
            base_coin_amount = float(info['available'])
        if (info['currency'] == trade_coin) & (info['type'] == 'trading'):
            trade_coin_amount = float(info['available'])
    print('当前资产:\n', base_coin, base_coin_amount, trade_coin, trade_coin_amount)
    return base_coin_amount, trade_coin_amount


# =====下单
def place_order(exchange, order_type, buy_or_sell, symbol, price, amount):
    """
    下单
    :param exchange: 交易所
    :param order_type: limit, market
    :param buy_or_sell: buy, sell
    :param symbol: 买卖品种 ‘ETHUSD’
    :param price: 当market订单的时候，price无效 浮点字符串，如‘0.05’
    :param amount: 买卖量（开仓量） 浮点字符串，如‘0.05’
    :return:
    """

    for i in range(5):
        try:
            order_info = exchange.private_post_order_new(params={'symbol': symbol,
                                                                 'amount': amount,
                                                                 'price': price,
                                                                 'side': buy_or_sell,
                                                                 'type': order_type})

            print('下单成功：', order_type, buy_or_sell, symbol, price, amount)
            print('下单信息：', order_info, '\n')
            return order_info

        except Exception as e:
            print('下单报错，1s后重试', e)
            time.sleep(5)

    print('下单报错次数过多，程序终止')
    exit()


# =====获取最新价格数据
def get_price(exchange, symbol, buy_or_sell):
    '''
    :param exchange:
    :param symbol: 'ETH/USDT'
    :param buy_or_sell:
    :return:
    '''
    info = exchange.fetch_order_book(symbol, limit=1)
    if buy_or_sell == 'buy':
        price = info['asks'][0][0]
    else:
        price = info['bids'][0][0]
    return price


# =====钉钉发送消息
def send_dingding_msg(content, robot_id='e7ae7cc48dd987152cf748e1cc2973a722a01577653faad975e90b357bd6b084'):

    try:
        print('faq')
        msg = {
            "msgtype": "text",
            "text": {"content":  content + '\n' + datetime.datetime.now().strftime("%m-%d %H:%M:%S")}}

        Headers = {"Content-Type": "application/json ;charset=utf-8 "}
        url = 'https://oapi.dingtalk.com/robot/send?access_token=' + robot_id
        body = json.dumps(msg)
        requests.post(url, data=body, headers=Headers)
    except Exception as err:
        print('钉钉发送失败', err)


# 获取到目前为止的数据
def get_candle_from_now(exchange, refresh_symbol, local_data_path, now, from_timestamp):
    '''

    :param exchange:
    :param refresh_symbol:
    :param local_data_path:
    :param now: 当前交易所时间
    :param from_timestamp: 同步开始的时间，即本地记录最后的时间
    :return:
    '''

    root = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    sys.path.append(root + '/python')

    print('当前交易所时间为', pd.to_datetime(now, unit='ms'))

    print('从', pd.to_datetime(from_timestamp, unit='ms'), '开始同步')

    try:
        df = pd.read_csv(local_data_path)
        df = df[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']]
    except:
        df = pd.DataFrame(columns=['candle_begin_time', 'open', 'high', 'low', 'close', 'volume'])

    while int(from_timestamp) < now:

        try:
            # 从服务器获取k线数据
            ohlcvs = exchange.fetch_ohlcv(refresh_symbol, '1m', from_timestamp)
            ohlcvs_df = pd.DataFrame(ohlcvs)
            ohlcvs_df.columns = ['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']

            # 保存到df
            df = df.append(ohlcvs_df)

            # 去重，防止有重复k线
            df.drop_duplicates(['candle_begin_time'], keep='last', inplace=True)
            synchronized_time = df.iloc[len(df)-1, 0]
            print(refresh_symbol+' 已同步至', pd.to_datetime(synchronized_time, unit='ms'))

            # 下次获取的since时间必须是当前结束的时间
            from_timestamp = synchronized_time
            # 中途保存
            df.to_csv(local_data_path)

        except Exception as e:
            print(e)
            print('报错，30秒后重连')
            time.sleep(30)

    # print("kksk")
    # print(df)
    df['candle_begin_time'] = pd.to_datetime(df['candle_begin_time'], unit='ms')
    # df['candle_begin_time'] = pd.to_datetime(df['candle_begin_time'])
    df.reset_index(inplace=True, drop=True)
    # print(df)
    return df


# =====程序开始运行时，同步数据
def refresh_data(exchange, para, time_interval, refresh_symbol, local_data_path):
    '''
    如果本地数据为空，就从当前时间往回数line分钟，line=时间周期*布林线的周期参数
    如果本地数据不为空，就补到现在为止
    :param para: 布林带参数
    :param time_interval: 时间级别13T, 17T, 23T
    :param refresh_symbol: 品种 ETH/USDT
    :param local_data_path: 本地保存的数据文件路径
    :return:
    '''
    interval = int(time_interval[:-1])
    lines = para[0] * interval  # 本地文件中至少需要的行数
    # print( refresh_symbol+' 本地文件至少需要' + str(lines) + '行')
    try:
        local_df = pd.read_csv(local_data_path)
        local_df = local_df[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']]
    except:
        local_df = pd.DataFrame(columns=['candle_begin_time', 'open', 'high', 'low', 'close', 'volume'])
    now = exchange.milliseconds()
    # print(now,type(now))
    if len(local_df) > 1:
        print(refresh_symbol+' 本地文件目前有' + str(len(local_df)) + '行')
        sychronized_time = local_df.iloc[-1][0]
        print(refresh_symbol+' 本地文件最新格林威治时间', pd.to_datetime(sychronized_time, unit='ms'))
        from_timestamp = sychronized_time
    else:
        print(refresh_symbol+' 本地文件为空，可能需较长时间同步数据')
        print('同步时请勿中途退出，否则将需要重新同步')
        from_timestamp = now - lines*2*60000


    print('需同步至格林威治时间', pd.to_datetime(now, unit='ms'))
    df= get_candle_from_now(exchange, refresh_symbol, local_data_path, now, from_timestamp)
    print(refresh_symbol+' 同步完成')

    return df


# =====更新数据直到满足要求
def update_candle(exchange, refresh_symbol, least_lines, local_data_path, requests, time_interval):
    try:
        df = pd.read_csv(local_data_path)
        df = df[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']]
    except:
        df = pd.DataFrame(columns=['candle_begin_time', 'open', 'high', 'low', 'close', 'volume'])

    # 最小数据要求
    if requests =='min':
        while len(df) < least_lines:
            df = pd.read_csv(local_data_path)

            # 一次获取7根，然后去重
            now = exchange.milliseconds()
            from_timestamp = now - 7*60000

            # 开始更新
            ohlcvs = exchange.fetch_ohlcv(refresh_symbol, '1m', from_timestamp)
            ohlcvs_df = pd.DataFrame(ohlcvs)
            ohlcvs_df.columns = ['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']

            # 去重
            df = df.append(ohlcvs_df, sort=False)
            df.drop_duplicates(['candle_begin_time'], keep='last', inplace=True)
            df.reset_index(inplace=True)
            df = df[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']]

            # 输出保存
            df.to_csv(local_data_path)
            print(refresh_symbol+' 当前本地记录文件有', len(df), '行')
            time.sleep(60)

    # 整数倍数据要求
    if requests == 'div':
        now_lines = len(df)
        period = int(time_interval[:-1])
        condition1 = (now_lines % period == 1)

        while not condition1:
            time.sleep(10)
            df = pd.read_csv(local_data_path)

            # 一次获取7根，然后去重
            now = exchange.milliseconds()
            from_timestamp = now - 7 * 60000

            # 开始更新
            ohlcvs = exchange.fetch_ohlcv(refresh_symbol, '1m', from_timestamp)
            ohlcvs_df = pd.DataFrame(ohlcvs)
            ohlcvs_df.columns = ['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']

            # 去重
            df = df.append(ohlcvs_df, sort=False)
            df.drop_duplicates(['candle_begin_time'], keep='last', inplace=True)
            df.reset_index(inplace=True)
            df = df[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']]

            # 输出保存
            df.to_csv(local_data_path)
            # print(refresh_symbol+' 当前本地记录文件有', len(df), '行')

            # 更新condition
            now_lines = len(df)
            condition1 = (now_lines % period == 1)

    df.to_csv(local_data_path)
    return df


# =====获取当前持仓状态
def get_now_status(exchange, symbol):

    while True:
        try:

            position_info = exchange.private_post_positions()
            break
        except Exception as e:
            print(e)
            time.sleep(60)

    now_amount = 0
    for info in position_info:
        if info['symbol'] == symbol:
            now_amount = float(info['amount'])
    if now_amount > 0.01:
        return 'long', now_amount
    if now_amount < -0.01:
        return 'short', now_amount
    if (now_amount > -0.01) & (now_amount < 0.01):
        return 'empty', 0


# =====获取当前持仓状态,用于发送通知
def get_now_status_for_dingding(exchange, symbol):
    position_info = exchange.private_post_positions()
    balance = exchange.fetch_balance()['info']
    usd_amounts = balance[-1]['amount']
    now_amount = 0
    for info in position_info:
        if info['symbol'] == symbol:
            now_amount = float(info['amount'])
    if now_amount > 0.01:
        return 'long', now_amount, usd_amounts
    if now_amount < -0.01:
        return 'short', now_amount, usd_amounts
    if (now_amount > -0.01) & (now_amount < 0.01):
        return 'empty', 0, usd_amounts


# =====调仓
def change_position(exchange, status_symbol, trade_symbol, price_symbol, now_position, now_amount, signal_list, leverage, usage, stop_loss_pct, stop_profit_pct):
    # 顺序必须是:先平仓，再开仓，不能改。
    content = ''
    now_amount = abs(now_amount)
    trade_coin = status_symbol[:3]
    base_coin = status_symbol[-3:]
    content += '---------------' + trade_coin.upper() + '---------------' + '\n'
    content += 'TRADE　TYPE：' + signal_list[0] + '\n'
    # 撤掉所有单
    condition1 = ('long_close' in signal_list)
    condition2 = ('short_close' in signal_list)

    if condition1 or condition2:
        info = exchange.private_post_orders()
        print(info)
        order_id_list = []
        for order in info:
            if order['symbol'] == status_symbol:
                order_id_list.append(order['id'])
        print('需要撤', order_id_list)
        if order_id_list != []:
            cancel_info = exchange.private_post_order_cancel_multi(params={'order_ids': order_id_list})
            print(cancel_info)

    print(signal_list, now_position)

    # =====平仓需要平掉所有的挂单
    if 'long_close' in signal_list:
        if now_position == 'long':
            print('平多头')
            # 市价平仓，跑得快
            place_order(exchange, 'market', 'sell', trade_symbol, '1000', str(now_amount))
            # content += '多头平仓！\n'

    if 'short_close' in signal_list:
        if now_position == 'short':
            print('平空头')
            # 市价平仓，跑得快
            place_order(exchange, 'market', 'buy', trade_symbol, '1', str(now_amount))
            # content += '空头平仓！\n'

    # 平仓完成后，要更新当前的账户状态，以防下面无法进行开仓判断
    now_position, now_amount = get_now_status(exchange, status_symbol)
    print(now_position, now_amount)
    # 获取当前可用保证金
    # base_coin_amount, trade_coin_amount = get_balance_info(exchange, base_coin, trade_coin)
    base_coin_amount = margin_base(exchange)

    # =====开仓需要挂止损单和止盈限价单
    if 'short_open' in signal_list:
        if now_position == 'empty':
            print('开空头')
            # 限价开仓
            # 获取当前价格
            price = get_price(exchange, price_symbol, 'sell')

            # 计算可开数量，只用usage的币
            open_amount = base_coin_amount / price * usage * leverage

            # 下单价格为当前买一价的0.98
            order_price = str(round(price * 0.98, 8))[:5]
            stop_profit_price = str(round(price * (1-stop_profit_pct), 8))[:5]
            stop_loss_price = str(round(price * (1+stop_loss_pct), 8))[:5]

            content += 'MARGIN: ' + str(open_amount*price) +'\n'
            content += 'ORDERED PRICE: '+ order_price +'\n'
            content += 'SPP:' +stop_profit_price +'\n'
            content += 'SLP: '+ stop_loss_price +'\n'

            # 建仓单
            place_order(exchange, 'limit', 'sell', trade_symbol, order_price, str(open_amount))
            # 止盈止损OCO
            for i in range(5):
                try:
                    order_info = exchange.private_post_order_new(params={'symbol': trade_symbol,
                                                                         'amount': str(open_amount),
                                                                         'price': stop_profit_price,
                                                                         'side': 'buy',
                                                                         'type': 'limit',
                                                                         'ocoorder': True,
                                                                         'buy_price_oco': stop_loss_price})
                    print('止盈止损成功：',)
                    print('下单信息：', order_info, '\n')
                    break
                except Exception as e:
                    print('下单报错，1s后重试', e)
                    time.sleep(5)

            # content += '空头开仓！\n'

    if 'long_open' in signal_list:
        if now_position == 'empty':
            print('开多头')
            # 限价开仓
            # 获取当前价格
            price = get_price(exchange, price_symbol, 'buy')

            # 计算可开数量，只用90%的币
            open_amount = base_coin_amount / price * usage * leverage

            # 下单价格为当前卖一价的1.02
            order_price = str(round(price * 1.02, 8))[:5]
            stop_profit_price = str(round(price * (1+stop_profit_pct), 8))[:5]
            stop_loss_price = str(round(price * (1-stop_loss_pct), 8))[:5]

            content += 'MARGIN: ' + str(open_amount*price) +'\n'
            content += 'ORDERED PRICE: ' + order_price + '\n'
            content += 'SPP:' + stop_profit_price + '\n'
            content += 'SLP: ' + stop_loss_price + '\n'

            # 建仓单
            place_order(exchange, 'limit', 'buy', trade_symbol, order_price, str(open_amount))
            # 止盈止损OCO
            for i in range(5):
                try:
                    order_info = exchange.private_post_order_new(params={'symbol': trade_symbol,
                                                                         'amount': str(open_amount),
                                                                         'price': stop_profit_price,
                                                                         'side': 'sell',
                                                                         'type': 'limit',
                                                                         'ocoorder': True,
                                                                         'sell_price_oco': stop_loss_price})
                    print('止盈止损成功')
                    print('下单信息：', order_info, '\n')
                    break
                except Exception as e:
                    print('下单报错，1s后重试', e)
                    time.sleep(5)

            content += '多头开仓！\n'

    send_dingding_msg(content)
    try:
        return order_price
    except:  # 平仓的话，没有返还的价格
        order_price = 0
        return order_price


# 为防止因每次获取复数根K线导致跳过开仓信号判断，故以本地k线数据的增量为依据判断调仓
def signal_mark(add, period, signal_loc):
    '''
    :param add: int 1,2,...
    :param period: int,13,15
    :param signal_loc: location of signal
    :return:tuple(True,3)
    '''
    # 读取本地开平仓信号标记，若无，则生成文件
    # print('判断是否判断是否调仓')
    # try:
    #     df_signal = pd.read_csv(signal_loc)
    #     df_signal = df_signal[['median', 'upper', 'lower', 'add', 'lastest_price']]
    # except:
    #     df_signal = pd.DataFrame(columns=[['median', 'upper', 'lower', 'add', 'lastest_price']])
    #     for index in ['median', 'upper', 'lower', 'add', 'lastest_price']:
    #         df_signal.at[0,index] =0.00000000001


    try:
        df_signal = pd.read_csv(signal_loc,index_col=0)
    except:
        time.sleep(10)

    before_update = df_signal.iloc[0][-2]

    df_signal.iloc[0,-2] = add + before_update
    # print(df_signal)

    if  df_signal.iloc[0,-2] > period:  # 如果大于period，需要把k线削掉几根来计算
        deduct = int(round(df_signal.iloc[0,-2]-period+1))
        df_signal.iloc[0, -2] = deduct-1
        df_signal.to_csv(signal_loc)
        return True, deduct
    else:
        df_signal.to_csv(signal_loc)
        return False,df_signal.iloc[0,-2]


# 侦测到调仓信号时，需要多次读取当前价格判断是否是骗炮
def kidding_or_not(exchange, trade_coin, compare_aim, type):

    test_time = 3

    if type == 'long_open':

        while True:
            try:
                # 判断是否开多
                price_now = get_price(exchange, trade_coin.upper() + '/USDT', 'sell')
                if price_now > compare_aim:
                    test_time -= 1
                    print(type+' passed  '+str(3-test_time)+' test, '+str(test_time)+' times required before action')
                    time.sleep(10)
                    if test_time <= 0:
                        signal_list = [type]
                        break
                else:
                    print('faker!')
                    signal_list = []
                    break
            except:
                time.sleep(5)

    elif type == 'short_open':
        while True:
            try:
                # 判断是否开空
                price_now = get_price(exchange, trade_coin.upper() + '/USDT', 'buy')
                if price_now < compare_aim:
                    test_time -= 1
                    print(type+'passed '+str(3-test_time)+' test, '+str(test_time)+' times required before action')
                    time.sleep(10)
                    if test_time <= 0:
                        signal_list = [type]
                        break
                else:
                    print('faker!')
                    signal_list = []
                    break
            except:
                time.sleep(5)

    elif type == 'long_close':
        while True:
            try:
                # 判断是否平多
                price_now = get_price(exchange, trade_coin.upper() + '/USDT', 'buy')
                if price_now < compare_aim:
                    test_time -= 1
                    print(type + 'passed' + str(3 - test_time) + 'test,' + str(test_time) + 'times required before action')
                    time.sleep(10)
                    if test_time <= 0:
                        signal_list = [type]
                        break
                else:
                    print('faker!')
                    signal_list = []
                    break
            except:
                time.sleep(5)

    elif type == 'short_close':
        while True:
            try:
                # 判断是否平空
                price_now = get_price(exchange, trade_coin.upper() + '/USDT', 'sell')
                if price_now > compare_aim:
                    test_time -= 1
                    print(type + 'passed' + str(3 - test_time) + 'test,' + str(test_time) + 'times required before action')
                    time.sleep(10)
                    if test_time <= 0:
                        signal_list = [type]
                        break
                else:
                    print('faker!')
                    signal_list = []
                    break
            except:
                time.sleep(5)

    else:
        signal_list = []

    return signal_list


def margin_base(exchange):
    while True:
        try:
            # 判断是否持仓
            balance = exchange.private_post_positions()
            balance2 = exchange.fetch_balance()['info']
            break
        except:
            time.sleep(3)

    margin_base = 0

    for i in range(len(balance2)):

        condition04 = (balance2[i]['currency'] == 'usd')
        condition02 = (balance2[i]['type'] == 'trading')

        if condition04 and condition02:
            margin_base += float(balance2[i]['amount'])
            break

    if balance == []:
        pass
    else:
        for dict in balance:
            symbol = dict['symbol'][:3]

            # 方向&开仓量
            amount = float(dict['amount'])
            if amount > 0:
                position = 'long'
            else:
                position = 'short'

            # 开仓价格&现价
            price_open = round(float(dict['base']), 2)
            price_now = round(get_price(exchange, symbol.upper() + '/USDT', 'sell'), 2)

            # 使用保证金
            deposit = round(abs(amount) * price_open / 3, 2)

            # # profit&pct
            # profit = round(float(dict['swap']) + float(dict['pl']), 2)

            margin_base += (deposit)

    return margin_base


# 读取本地储存的用于判断移动止盈的数据
def read_profit_data(loc):  # 读取本地储存的用于移动止盈的数据
    try:
        df = pd.read_csv(loc)
        # print(df)
        df = df[['currency', 'open','max_profit', 'moving_stop_price']]
        # print(df)
    except:
        df = pd.DataFrame(columns=['currency', 'open', 'max_profit', 'moving_stop_price'])
        df.at[0, 'currency'] = 'eos'
        df.at[1, 'currency'] = 'eth'
        df.at[2, 'currency'] = 'xrp'
        for index in [0, 1, 2]:
            df.at[index, 'max_profit'] = -0.00001
        df.to_csv(loc)
    profit_data = {'eos': {'open': None, 'max_profit': -0.00001, 'moving_stop_price': None},
                   'eth': {'open': None, 'max_profit': -0.00001, 'moving_stop_price': None},
                   'xrp': {'open': None, 'max_profit': -0.00001, 'moving_stop_price': None}}
    for index in range(len(df)):
        currency = df.at[index, 'currency']
        profit_data[currency]['open'] = df.at[index, 'open']
        profit_data[currency]['max_profit'] = df.at[index, 'max_profit']
        profit_data[currency]['moving_stop_price'] = df.at[index, 'moving_stop_price']

    return profit_data


# 将移动止盈数据储存到本地
def write_profit_data(loc, profit_data, currency,reset = 'off',record_open = 0):

    df = pd.read_csv(loc)
    df = df[['currency', 'open', 'max_profit', 'moving_stop_price']]

    line = {'eos': 0, 'eth': 1, 'xrp': 2}

    # 平仓时清空记录
    if reset == 'on':
        df.at[line[currency], 'open'] = None
        df.at[line[currency], 'max_profit'] = -0.000001
        df.at[line[currency], 'moving_stop_price'] = None

    # 开仓时记录开仓价格
    elif record_open != 0:
        df.at[line[currency], 'open'] = record_open

    else:
        df.at[line[currency], 'open'] = profit_data[currency]['open']
        df.at[line[currency], 'max_profit'] = profit_data[currency]['max_profit']
        df.at[line[currency], 'moving_stop_price'] = profit_data[currency]['moving_stop_price']
        print(df)
        print('动态止盈信息已更新到本地')

    df.to_csv(loc)


# 将当前价格写入本地signal文件，移动止盈后，价格可能依旧在轨道外，必须重新穿过轨道才算发出信号
def write_now_price(signal_loc, now_price):
    df = pd.read_csv(signal_loc)
    df.at[0, 'lastest_price'] = now_price
    df.to_csv(signal_loc)





