import numpy as np
import pandas as pd


class FFTForecast:
    def __init__(self, raw_data):
        self.data = self._init_process_data(data=raw_data)

    @staticmethod
    def _init_process_data(data):
        data['candle_begin_time'] = pd.to_datetime(data['candle_begin_time'])
        data['rtn'] = data['close'].apply(lambda x: np.log(x)) - data['close'].apply(lambda x: np.log(x)).shift(52)
        data.dropna(inplace=True, how='any')
        data.reset_index(drop=True, inplace=True)
        return data

    def fft_wave(self, freq_num):
        fft_data = list(self.data['rtn'].values)
        res = np.fft.fft(fft_data)

        # 根据傅立叶变换后的复数向量模进行排序，选择前六
        res_df = pd.DataFrame({'vec': res})
        res_df['abs'] = res_df['vec'].apply(lambda x: x.__abs__())

        res_df = res_df[:len(res_df) // 2].copy()
        res_df.reset_index(inplace=True)
        res_df = res_df.loc[res_df['index'] != 0].copy()

        res_df.sort_values(by='abs', inplace=True, ascending=False)
        res_df.reset_index(inplace=True, drop=True)
        filtered_freq_list = list(res_df['index'].values)[:freq_num]  # 得到强度前六的频率

        res_dict = {}  # 用于记录每个频率的波以及整合波
        for freq in range(len(res)):
            if (freq in filtered_freq_list) or (len(res) - freq in filtered_freq_list):
                continue
            else:
                res[freq] = 0
        sum_i_d = np.fft.ifft(res)

        res_dict['sum_filter'] = sum_i_d  # 整合波

        # 逆变换得到各个频率的波
        for i in filtered_freq_list:
            filtered_res = [0 for _ in range(len(res))]
            filtered_res[i] = res[i]
            filtered_res[len(res) - i] = res[len(res) - i]
            i_d = np.fft.ifft(filtered_res)
            res_dict[i] = [t.real for t in i_d]

        # 滤波全体以及原始的价格、同比收益率数据
        wave_df = pd.DataFrame(res_dict)
        wave_df['candle_begin_time'] = self.data['candle_begin_time']
        wave_df['price'] = self.data['close']
        wave_df['rtn'] = self.data['rtn']
        wave_df['std'] = 0
        wave_df.set_index('candle_begin_time', inplace=True)
        return wave_df, filtered_freq_list

    @staticmethod
    def simple_vote(wave_df, filtered_freq_list):
        """
        简单等权投票
        :param wave_df:
        :param filtered_freq_list:
        :return:
        """
        # 计算每个频率的波的走势
        vote = {}
        sum_vote = 0
        for i in range(len(filtered_freq_list)):
            freq = filtered_freq_list[i]
            weight = 1

            last = round(wave_df[freq].iloc[-1] / wave_df[freq].max(), 4)
            if abs(last) > 1:
                print(last)
                last = int(last)

            last_r = np.arccos(last)

            sec_last = round(wave_df[freq].iloc[-2] / wave_df[freq].max(), 4)
            if abs(sec_last) > 1:
                print(sec_last)
                sec_last = int(sec_last)

            sec_last_r = np.arccos(sec_last)

            if (last_r <= np.pi / 2) & (last_r > 0) & (last_r > sec_last_r):
                vote[freq] = -2  # '加速下跌'
                sum_vote += vote[freq] * weight
            elif (last_r > np.pi / 2) & (last_r < np.pi) & (last_r > sec_last_r):
                vote[freq] = -1  # '减速下跌'
                sum_vote += vote[freq] * weight
            elif (last_r > np.pi / 2) & (last_r < np.pi) & (last_r < sec_last_r):
                vote[freq] = 2  # '加速上涨'
                sum_vote += vote[freq] * weight
            elif (last_r <= np.pi / 2) & (last_r > 0) & (last_r < sec_last_r):
                vote[freq] = 1  # '减速上涨'
                sum_vote += vote[freq] * weight
            elif (last_r == np.pi) | (last_r == 0):
                vote[freq] = 0
            else:
                print(last, sec_last)
                print(last_r, sec_last_r)
                raise Exception('???')
        return sum_vote / (2 * len(filtered_freq_list))
