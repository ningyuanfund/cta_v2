import json
import time
from datetime import datetime, timedelta

import pandas as pd
import requests
from sqlalchemy import create_engine

from ckt.fft_forecast.data_sort import DataSorter
from ckt.fft_forecast.fft_forecast import FFTForecast

pd.set_option('expand_frame_repr', False)


class FFTRealTime:
    def __init__(self):
        self.eg = create_engine('postgresql://postgres:thomas@47.75.180.89/bfx_alldata')
        self.symbol_list = ['BTCUSD']
        self.data_sorter = DataSorter(eg=self.eg)

    def daily_data_former(self):
        daily_data = self.data_sorter.form_data()
        # five_min_data['candle_begin_time'] = pd.to_datetime(five_min_data['candle_begin_time'])
        # data = five_min_data[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()
        # daily_data = data.resample(on='candle_begin_time', rule='1D').agg(
        #     {
        #         'open': 'first',
        #         'high': 'max',
        #         'low': 'min',
        #         'close': 'last',
        #         'volume': 'sum'
        #     }
        # )
        # daily_data.reset_index(inplace=True)
        return daily_data

    @staticmethod
    def weekly_data_former(daily_data):
        data = daily_data[['candle_begin_time', 'open', 'high', 'low', 'close']].copy()
        weekly_data = data.resample(on='candle_begin_time', rule='7D').agg(
            {
                'open': 'first',
                'high': 'max',
                'low': 'min',
                'close': 'last',
            }
        )
        weekly_data.reset_index(inplace=True)
        return weekly_data

    @staticmethod
    def form_seven_date_pair(begin_date, end_date):
        date_num = (pd.to_datetime(end_date) - pd.to_datetime(begin_date)) / timedelta(days=1)
        wks_num = date_num // 7
        pair_list = []
        t_end_date = pd.to_datetime(end_date)
        for i in range(7):
            t_begin_date = t_end_date - timedelta(days=7 * wks_num)
            if t_begin_date < pd.to_datetime(begin_date):
                t_begin_date += timedelta(days=7)
            pair_list.append(
                {
                    'begin_date': t_begin_date,
                    'end_date': t_end_date
                }
            )
            t_end_date -= timedelta(days=1)
        return pair_list

    def get_vote_res(self):
        data = self.daily_data_former()
        begin_date = data['candle_begin_time'].iloc[0]
        end_date = data['candle_begin_time'].iloc[-1]
        date_pair_list = self.form_seven_date_pair(
            begin_date=begin_date,
            end_date=end_date
        )

        sum_vote_res = 0
        for date_pair in date_pair_list:
            cond1 = data['candle_begin_time'] > pd.to_datetime(date_pair['begin_date'])
            cond2 = data['candle_begin_time'] <= pd.to_datetime(date_pair['end_date'])
            t_data_daily = data.loc[cond1 & cond2].copy()
            t_data = self.weekly_data_former(daily_data=t_data_daily)
            t_data.reset_index(drop=True, inplace=True)

            t_fft_forecast = FFTForecast(raw_data=t_data)
            freq_num = 6
            wave_df, chosen_freq = t_fft_forecast.fft_wave(freq_num=freq_num)
            vote_res = t_fft_forecast.simple_vote(wave_df=wave_df, filtered_freq_list=chosen_freq)
            sum_vote_res += vote_res
            print(date_pair, vote_res)

        sum_vote_res /= 7
        return sum_vote_res

    # https://oapi.dingtalk.com/robot/send?access_token=e34ec5e7d22fdd96b78b1b228715811c16115ab64d984d88c2abe1bbbd83e89f
    @staticmethod
    def send_dingding_msg(content, robot_id='e34ec5e7d22fdd96b78b1b228715811c16115ab64d984d88c2abe1bbbd83e89f'):
        try:
            msg = {
                "msgtype": "text",
                "text": {
                    "content": 'mdks_msg' + '\n' +
                               content + '\n' +
                               datetime.now().strftime("%m-%d %H:%M:%S")}
            }
            headers = {"Content-Type": "application/json;charset=utf-8"}
            url = 'https://oapi.dingtalk.com/robot/send?access_token=' + robot_id
            body = json.dumps(msg)
            requests.post(url, data=body, headers=headers)
            print('成功发送钉钉')
        except Exception as ex:
            print("发送钉钉失败:", ex)

    # ===下次运行的时间
    @staticmethod
    def next_run_time(time_interval, ahead_time=1):
        """
            :param time_interval 运行周期时间 建议不要小于5min
            :param ahead_time 当小于1s时 自动进入下个周期
            :return:
            """
        if time_interval.endswith('T'):
            now_time = datetime.now()
            time_interval = int(time_interval.strip('T'))

            target_min = (int(now_time.minute / time_interval) + 1) * time_interval
            if target_min < 60:
                target_time = now_time.replace(minute=target_min, second=0, microsecond=0)
            else:
                if now_time.hour == 23:
                    target_time = now_time.replace(hour=0, minute=0, second=0, microsecond=0)
                    target_time += timedelta(days=1)
                else:
                    target_time = now_time.replace(hour=now_time.hour + 1, minute=0, second=0, microsecond=0)

            # sleep直到靠近目标时间之前
            if (target_time - datetime.now()).seconds < ahead_time + 1:
                print('距离target_time不足', ahead_time, '秒，下下个周期再运行')
                target_time += timedelta(minutes=time_interval)
            print('下次运行时间', target_time)
            return target_time
        else:
            exit('time_interval doesn\'t end with T')

    @staticmethod
    def wait(target_time):
        # 每分钟更新持仓信息
        while True:
            if datetime.now() < target_time - timedelta(seconds=300):
                time.sleep(120)
                continue
            else:
                break

        # 直到下次运行的时间再启动
        time.sleep(max(0, (target_time - datetime.now()).seconds))

        # 在靠近目标时间时
        while True:
            if datetime.now() < target_time:
                continue
            else:
                break

    def main(self):
        # 计算下次运行的时间，每5分钟运行一次，但不一定所有的品种都交易
        target_time = self.next_run_time('30T')

        self.wait(target_time=target_time)

        now_hour = pd.to_datetime(time.time(), unit='s').strftime('%H')
        now_minute = pd.to_datetime(time.time(), unit='s').strftime('%M')

        # time.time()是格林威治时间，北京时间09，time.time的hour应该是01
        # 防止30分钟后再开一次
        if (now_hour == '02') & (int(now_minute) < 29):
            target_position = self.get_vote_res()

            self.send_dingding_msg(content='=' * 10 +
                                           '\n FFT VOTE RES \n %s \n' % target_position +
                                           '=' * 10
                                   )
            time.sleep(10)
            print(target_position)

    def _main(self):
        target_position = self.get_vote_res()

        self.send_dingding_msg(content='=' * 10 +
                                       '\n FFT VOTE RES \n %s \n' % target_position +
                                       '=' * 10
                               )
        time.sleep(10)
        print(target_position)


if __name__ == '__main__':

    fft_real_time = FFTRealTime()

    while True:
        try:
            fft_real_time.main()
        except Exception as e:
            print('系统报错', e, '10秒后重试')
            fft_real_time.send_dingding_msg(content='系统报错' + str(e))
            # raise Exception(e)
            time.sleep(10)
