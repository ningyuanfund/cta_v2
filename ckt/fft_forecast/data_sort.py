import time
from datetime import datetime, timedelta

import pandas as pd
from sqlalchemy import create_engine


class DataSorter:
    def __init__(self, eg):
        self.eg = eg

    # 从服务器上获取数据，temp为该数据的表名df，返回拼接好的数据
    def _sort_df(self):
        early_data = pd.read_csv(r'early_data.csv')
        early_data['candle_begin_time'] = pd.to_datetime(early_data['candle_begin_time'])
        early_data.sort_values(by='candle_begin_time', inplace=True)

        df_list = [early_data]
        now_date = pd.to_datetime(time.time(), unit='s') - timedelta(days=1)
        fetch_date = early_data['candle_begin_time'].iloc[-1] + timedelta(days=1)
        while fetch_date <= now_date:
            while True:
                try:
                    sql = 'select * from "bfx_data_BTC/USD_%s "' % fetch_date.strftime("%Y-%m-%d")

                    data = pd.read_sql_query(sql, con=self.eg)
                    data = data[['candle_begin_time', 'open', 'high', 'low', 'close']].copy()
                    data['candle_begin_time'] = pd.to_datetime(data['candle_begin_time'])
                    data = data.resample(on='candle_begin_time', rule='1D').agg(
                        {
                            'open': 'first',
                            'high': 'max',
                            'low': 'min',
                            'close': 'last'
                        }
                    )
                    print(data)
                    data.reset_index(inplace=True)
                    df_list.append(data)
                    fetch_date += timedelta(days=1)
                    break
                except Exception as e:
                    print(e)

        df = pd.concat(df_list)

        df.drop_duplicates(inplace=True)
        df.sort_values(by='candle_begin_time')
        df.reset_index(drop=True, inplace=True)

        return df

    # 从服务器上获取数据，temp为该数据的表名df，返回拼接好的数据
    def __sort_df(self):
        early_data = pd.read_csv(r'early_data.csv')
        early_data['candle_begin_time'] = pd.to_datetime(early_data['candle_begin_time'])
        early_data.sort_values(by='candle_begin_time', inplace=True)
        return early_data

    # 生成时间列，用于遍历数据（独立于数据）
    @staticmethod
    def _get_time(start, end):

        end_time = datetime.strptime(end, "%Y-%m-%d %H:%M:%S")
        now_time = datetime.strptime(start, "%Y-%m-%d %H:%M:%S")

        time_list = [now_time]
        while now_time < end_time:
            now_time += timedelta(minutes=5)
            time_list.append(now_time)
        return time_list

    def form_data(self):
        data = self._sort_df()
        # begin_time = data['candle_begin_time'].iloc[0].strftime('%Y-%m-%d %H:%M:%S')
        # end_time = data['candle_begin_time'].iloc[-1].strftime('%Y-%m-%d %H:%M:%S')
        #
        # tm_list = self._get_time(begin_time, end_time)
        # tm_df = pd.DataFrame(tm_list)
        # tm_df.rename(columns={0: 'candle_begin_time'}, inplace=True)
        #
        # data = data.merge(tm_df, how='outer', on='candle_begin_time')
        # data.sort_values(by='candle_begin_time', inplace=True)
        # data['open'].fillna(method='ffill', inplace=True)
        # data['high'].fillna(method='ffill', inplace=True)
        # data['low'].fillna(method='ffill', inplace=True)
        # data['close'].fillna(method='ffill', inplace=True)
        #
        # data.reset_index(inplace=True, drop=True)
        # data = data[['candle_begin_time', 'open', 'high', 'low', 'close']].copy()
        # data.sort_values(by='candle_begin_time', inplace=True, ascending=True)
        # data.reset_index(drop=True, inplace=True)
        data.drop_duplicates(inplace=True)
        local_data = data.set_index('candle_begin_time')
        local_data.to_csv('early_data.csv')
        return data


if __name__ == '__main__':

    _eg = create_engine('postgresql://postgres:thomas@47.75.180.89/bfx_alldata')

    data_sorter = DataSorter(eg=_eg)
    _data = data_sorter.form_data()
    print(_data)
