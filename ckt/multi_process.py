import pandas as pd
import os
from ckt.Functions import transfer_to_period_data
import ckt.Signals as sigs
from ckt.equity_curve import equity_curve_with_long_and_short
import warnings
from multiprocessing import Pool, freeze_support

warnings.simplefilter(action='ignore', category=FutureWarning)
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)


# ==========需手动设置的参数===========
# 策略名称
strategy_name = 'bolling'

# 回测开始时间
begin_time = '2018-01-01'

# 回测时间级别列表
freq_list = ['15T']

# 进程数，进程越多越快，当然取决于你的电脑有多好
process_number = 8

# 是否打开移动止盈
moving_stop_profit = True

# 构建参数候选组合
e_list = [3, 4, 5, 6, 7]  # 倍数e，即盈利达到止损的几倍，将触发移动止盈
d_list = [0.1, 0.2, 0.3, 0.4, 0.5]  # 移动止盈比例，即盈利回撤的百分比。触发移动止盈后，盈利回撤 d*100% 将平仓

# ===回测之前先测试！！
# e_list = [3]  # 倍数e，即盈利达到止损的几倍，将触发移动止盈
# d_list = [0.1]  # 移动止盈比例，即盈利回撤的百分比。触发移动止盈后，盈利回撤 d*100% 将平仓


n_min = 80
n_max = 200
n_step = 5
n_list = [i for i in range(n_min, n_max, n_step)]
para_name = r'%s_%s_%s' % (str(n_min), str(n_max), str(n_step))

k_list = [i/2 for i in range(4, 10)]

stop_loss_pct_list = [i for i in range(1, 10, 2)]  # 5%  4 

para_names = ['N', 'K', 'e', 'd', '止损比例', '时间级别']  # 这边需要手动输入参数名称

# 币种
pair = 'EOSUSD'

# =====================================

# ==============路径参数================

# 数据路径
data_path = r'E:\data\bitfinex\%s.h5' % pair

# 设定输出路径，如果没有这个路径的话就生成
path = r'E:\cta_v2\ckt\tables\%s\%s' % (strategy_name, pair)
if not os.path.exists(path):
    os.makedirs(path)

output_path = r'E:\cta_v2\ckt\tables\%s\%s\\' % (strategy_name, pair)  # 生成表格的基础地址，最后带上策略名以区分不同策略生成的表格
rtn_path = output_path + r'rtn.csv'  # 用于保存完整的参数排名结果
rtn_top_30_path = output_path + r'rtn_top_30.csv'  # 用于展示的30个最佳参数

# =====================================


def func(all_data, freq, para):

    # 计算交易信号
    function_name = 'sigs.signal_%s_with_stop_lose(all_data.copy(), para)' % strategy_name
    df = eval(function_name)

    # 计算资金曲线
    df = equity_curve_with_long_and_short(df, leverage_rate=3, c_rate=2.0 / 1000)
    print(freq, para, '策略最终收益：', df.iloc[-1]['equity_curve'])

    # 存储数据（多进程模式下如果保存成dataframe后面会有重复值）
    return str(freq) + ':' + str(para) + ':' + str(df.iloc[-1]['equity_curve'])


freeze_support()  # 必须要加，windows会出错

if __name__ == '__main__':  # 执行主进程

    pool = Pool(processes=process_number)  # 开辟进程池
    result = []

    for freq in freq_list:
        # 导入数据
        all_data = pd.read_hdf(data_path, key=freq)
        # 选取时间段
        all_data = all_data[all_data['candle_begin_time'] >= pd.to_datetime(begin_time)]
        all_data.reset_index(inplace=True, drop=True)

# 生成参数列表

        for n in n_list:
            for k1 in k_list:
                for stop_loss_rate in stop_loss_pct_list:
                    if moving_stop_profit:
                        # 生成移动止盈参数列
                        for e in e_list:
                            for d in d_list:
                                para = [n, k1, stop_loss_rate, e, d, freq]
                                result.append(pool.apply_async(func, (all_data, freq, para,)))

                    else:
                        para = [n, k1, stop_loss_rate, freq]
                        result.append(pool.apply_async(func, (all_data, freq, para,)))


    pool.close()  # 关闭进程池
    pool.join()  # 等待开辟的所有进程执行完后，主进程才继续往下执行

    A = pd.DataFrame()
    for i, res in enumerate(result):
        A.loc[i, '参数'] = res.get().split(':')[1][:-1] + ', ' + '\'' + res.get().split(':')[0] + '\']'
        A.loc[i, '时间级别'] = res.get().split(':')[0]
        A.loc[i, 'return'] = float(res.get().split(':')[-1])

    temp = A['参数'].str[1:-1].str.split(',', expand=True)
    rtn = pd.concat([A[['参数', 'return', '时间级别']], temp.iloc[:, :-1]], axis=1)

    rtn.columns = ['', 'return', '时间级别'] + para_names
    rtn.set_index('', inplace=True)
    rtn[['return'] + para_names].to_csv(rtn_path, encoding='utf_8_sig')

    # 取前30个最优参数展示用
    rtn_top_30 = rtn.sort_values(by='return', ascending=False)[:30]
    rtn_top_30.reset_index(drop=True, inplace=True)
    rtn_top_30['return'] = round(rtn_top_30['return'], 2)
    rtn_top_30['排名'] = range(1, 31, 1)
    rtn_top_30.rename(columns={'return': '策略收益'}, inplace=True)

    rtn_top_30[['排名'] + para_names + ['策略收益']].to_csv(rtn_top_30_path, index=False, encoding='utf_8_sig')
    print(rtn)
    print(rtn_top_30)