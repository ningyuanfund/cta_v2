import ccxt


def place_order(exchange, order_type, buy_or_sell, symbol, price, amount):
    """
    下单
    :param exchange:
    :param order_type: limit, market
    :param buy_or_sell: buy, sell
    :param symbol: 买卖品种 BTCUSD
    :param price: 当market订单的时候，price无效
    :param amount: 买卖量
    :return:
    """
    print('本次下单信息为：', order_type, buy_or_sell, symbol, price, amount)
    symbol = symbol[:3] + '/' + symbol[3:]

    for i in range(5):
        # try:
        # 限价单
        if order_type == 'limit':
            # 买
            if buy_or_sell == 'buy':
                order_info = exchange.create_limit_buy_order(symbol, amount, price, {'type': 'limit'})  # 买单
            # 卖
            elif buy_or_sell == 'sell':
                order_info = exchange.create_limit_sell_order(symbol, amount, price, {'type': 'limit'})  # 卖单
            else:
                order_info = {}
        # 市价单
        elif order_type == 'market':
            # 买
            if buy_or_sell == 'buy':
                order_info = exchange.create_market_buy_order(symbol, amount, {'type': 'market'})  # 买单
            # 卖
            elif buy_or_sell == 'sell':
                order_info = exchange.create_market_sell_order(symbol, amount, {'type': 'market'})  # 卖单
            else:
                order_info = {}
        else:
            order_info = {}

        print('下单成功：', order_type, buy_or_sell, symbol, price, amount)
        print('下单信息：', order_info, '\n')
        return order_info

# apiKey = 'scxIZEVc7hwB8GH7Qt9YDxYhGyrMTf5hS5lPMHV22Te'
# secret = 'B7DWq8E7GhnKhvjE8IjSzFjpMlfHGKkFBjiCbYnsVWI'

# a_a
apiKey = 'EKdAJowZuiZ5rC9OWlCuI6FKMTyog3sz9v8D4VcHLUA'
secret = 'aMeEvXBvlvx1gqUgirvIrezlWbfxu79UtVMCvtY21NP'


# lvshibenshi
# apiKey = 'NjgRU9rb0RVxZX6dWQZANlXOkLzIZjTHNsWjilG6ITw'
# secret = 'xNSiWmrfR9rQJAPOy1iiTlwHkJvyXCw1lMmvXRYEojZ'
# apiKey = 'B2SHXe5WshO3Fy6SRZHRHaWHoN7PxN0BmMW3g2XvRhu'
# secret = 'rLs5YICDXWPO2gYUNlYjLHv74tHOt2KCiGctW7MqDzp'

# geantianming
# apiKey = 'ttjSfIlt1mfVAdHosOOSsbfOLuWWUaM3aiEN7XGt1qe'
# secret = 'gHtAJuP2CG4tF5fC9Zt1toRJreXBU20SUY0t6QX9mRv'
# # 账户设置
account = {
        'exchange': ccxt.bitfinex(
                    {
                        'apiKey': apiKey,
                        'secret': secret,
                    }),
        'exchange2': ccxt.bitfinex2(
                    {
                        'apiKey': apiKey,
                        'secret': secret,
                    }),
        }
# res = account['exchange2'].private_post_auth_r_positions()
res = account['exchange'].private_post_positions()
print(res)
res = account['exchange'].fetch_balance()
print(res)
# res = place_order(
#     exchange=account['exchange'],
#     order_type='market',
#     buy_or_sell='sell',
#     symbol='ethusd',
#     price=196,
#     amount=10
# )
# res = account['exchange'].fetch_balance()
# print(res)
# account['exchange'].load_markets()
# account['exchange2'].load_markets()
