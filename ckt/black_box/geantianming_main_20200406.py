import json
import time
from datetime import datetime, timedelta

import ccxt
import pandas as pd
import requests
from sqlalchemy import create_engine

pd.set_option('expand_frame_repr', False)


class Sniper:
    def __init__(self, api_key, secret):
        self.api_key = api_key
        self.secret = secret
        self.exchange = self._init_exchange(
            api_key=api_key,
            secret=secret
        )

    @staticmethod
    def _init_exchange(api_key, secret):
        exchange = ccxt.bitfinex(
            {
                'apiKey': api_key,
                'secret': secret,
            }
        )
        exchange.load_markets()
        return exchange

    def _get_cash(self):
        balance = self.exchange.fetch_balance()
        for item in balance['info']:
            if (item['type'] == 'trading') & (item['currency'] == 'usd'):
                return float(item['amount'])
        return 0

    def _get_position(self, symbol):
        now_pos = self.exchange.private_post_positions()
        print('NOW POSITION', now_pos)
        total_pnl = 0
        symbol_now_amount = 0
        for pos in now_pos:
            total_pnl += float(pos['pl'])
            if pos['symbol'] == symbol.lower():
                symbol_now_amount = float(pos['amount'])
        return symbol_now_amount, total_pnl

    def _get_price(self, symbol):
        price = self.exchange.fetch_ticker(symbol='%s/USD' % symbol)
        return float(price['ask'])

    def cal_open_info(self, prop, symbol):
        now_holding, now_pnl = self._get_position(symbol=symbol)
        now_cash = self._get_cash()
        now_price = self._get_price(symbol=symbol.strip('USD'))

        should_open = (now_cash + now_pnl) * prop / now_price
        should_open /= 2  # 资金五五开
        return should_open - now_holding

    def place_order(self, order_type, buy_or_sell, symbol, price, amount):
        """
        下单
        :param order_type: limit, market
        :param buy_or_sell: buy, sell
        :param symbol: 买卖品种 BTCUSD
        :param price: 当market订单的时候，price无效
        :param amount: 买卖量
        :return:
        """
        print('本次下单信息为：', order_type, buy_or_sell, symbol, price, amount)
        symbol = symbol[:3] + '/' + symbol[3:]

        for i in range(5):
            # try:
            # 限价单
            if order_type == 'limit':
                # 买
                if buy_or_sell == 'buy':
                    order_info = self.exchange.create_limit_buy_order(symbol, amount, price, {'type': 'limit'})  # 买单
                # 卖
                elif buy_or_sell == 'sell':
                    order_info = self.exchange.create_limit_sell_order(symbol, amount, price, {'type': 'limit'})  # 卖单
                else:
                    order_info = {}
            # 市价单
            elif order_type == 'market':
                # 买
                if buy_or_sell == 'buy':
                    order_info = self.exchange.create_market_buy_order(symbol, amount, {'type': 'market'})  # 买单
                # 卖
                elif buy_or_sell == 'sell':
                    order_info = self.exchange.create_market_sell_order(symbol, amount, {'type': 'market'})  # 卖单
                else:
                    order_info = {}
            else:
                order_info = {}

            print('下单成功：', order_type, buy_or_sell, symbol, price, amount)
            print('下单信息：', order_info, '\n')
            return order_info

    def shoot(self, prop, symbol):
        while True:
            try:
                shoot_amount = round(self.cal_open_info(prop=prop, symbol=symbol), 4)
                print(symbol, shoot_amount)
                if shoot_amount > 0:
                    buy_or_sell = 'buy'
                elif shoot_amount < 0:
                    buy_or_sell = 'sell'
                else:
                    buy_or_sell = ''
                    print('NO NEED TO CHANGE')
                    break
                self.place_order(
                    order_type='market',
                    buy_or_sell=buy_or_sell,
                    symbol=symbol,
                    price=8000,
                    amount=abs(shoot_amount)
                )
                break
            except Exception as ex:
                print(ex)
                raise ex


class BlackBox:
    def __init__(self, api_key, secret):
        self.thoth_eg = create_engine('postgresql://postgres:holt@49.51.196.202:5432/thoth', echo=False)
        self.sniper = Sniper(
            api_key=api_key,
            secret=secret
        )

    # https: // oapi.dingtalk.com / robot / send?access_token = 93c603d52434740bb0a173819c2675f9aa7697a57d26eed357f37852382899f1
    @staticmethod
    def send_dingding_msg(content, robot_id='93c603d52434740bb0a173819c2675f9aa7697a57d26eed357f37852382899f1'):
        try:
            msg = {
                "msgtype": "text",
                "text": {
                    "content": 'aartox_msg' + '\n' +
                               'geantianming_msg' + '\n' +
                               content + '\n' +
                               datetime.now().strftime("%m-%d %H:%M:%S")}
            }
            headers = {"Content-Type": "application/json;charset=utf-8"}
            url = 'https://oapi.dingtalk.com/robot/send?access_token=' + robot_id
            body = json.dumps(msg)
            requests.post(url, data=body, headers=headers)
            print('成功发送钉钉')
        except Exception as ex:
            print("发送钉钉失败:", ex)

    # ===下次运行的时间
    @staticmethod
    def next_run_time(time_interval, ahead_time=1):
        """
            :param time_interval 运行周期时间 建议不要小于5min
            :param ahead_time 当小于1s时 自动进入下个周期
            :return:
            """
        if time_interval.endswith('T'):
            now_time = datetime.now()
            time_interval = int(time_interval.strip('T'))

            target_min = (int(now_time.minute / time_interval) + 1) * time_interval
            if target_min < 60:
                target_time = now_time.replace(minute=target_min, second=0, microsecond=0)
            else:
                if now_time.hour == 23:
                    target_time = now_time.replace(hour=0, minute=0, second=0, microsecond=0)
                    target_time += timedelta(days=1)
                else:
                    target_time = now_time.replace(hour=now_time.hour + 1, minute=0, second=0, microsecond=0)

            # sleep直到靠近目标时间之前
            if (target_time - datetime.now()).seconds < ahead_time + 1:
                print('距离target_time不足', ahead_time, '秒，下下个周期再运行')
                target_time += timedelta(minutes=time_interval)
            print('下次运行时间', target_time)
            return target_time
        else:
            exit('time_interval doesn\'t end with T')

    def get_position(self, max_tries):
        now_date = (pd.to_datetime(time.time(), unit='s') + timedelta(hours=8)).strftime('%Y-%m-%d')
        table = 'thoth_%s' % now_date
        sql = 'select * from "%s"' % table
        tries = 0
        print(now_date, 'GET POSITION DATA')
        while tries < max_tries:
            try:
                position_df = pd.read_sql_query(sql, con=self.thoth_eg)
                print(position_df)
                return {
                    'ETHUSD': position_df.loc[position_df['symbol'] == 'ETH', 'position'].values[0],
                    'BTCUSD': position_df.loc[position_df['symbol'] == 'BTC', 'position'].values[0]
                }
            except Exception as ex:
                print(ex)
                tries += 1
                time.sleep(10)

        self.send_dingding_msg(content='CAN\'T GET TODAY\'S THOTH TABLE')

    def change_position(self, target_position):
        print(target_position)
        for coin in target_position.keys():
            self.sniper.shoot(prop=target_position[coin], symbol=coin)

    @staticmethod
    def wait(target_time):
        # 每分钟更新持仓信息
        while True:
            if datetime.now() < target_time - timedelta(seconds=300):
                time.sleep(120)
                continue
            else:
                break

        # 直到下次运行的时间再启动
        time.sleep(max(0, (target_time - datetime.now()).seconds))

        # 在靠近目标时间时
        while True:
            if datetime.now() < target_time:
                continue
            else:
                break

    def main(self):
        # 计算下次运行的时间，每5分钟运行一次，但不一定所有的品种都交易
        target_time = self.next_run_time('30T')

        self.wait(target_time=target_time)

        now_hour = pd.to_datetime(time.time(), unit='s').strftime('%H')
        now_minute = pd.to_datetime(time.time(), unit='s').strftime('%M')

        # time.time()是格林威治时间，北京时间18，time.time的hour应该是10
        # 防止30分钟后再开一次
        if (now_hour == '10') & (int(now_minute) < 29):
            target_position = self.get_position(max_tries=5)
            for coin in target_position.keys():
                self.sniper.shoot(
                    prop=target_position[coin],
                    symbol=coin
                )
                self.send_dingding_msg(content='%s CHANGED\n NOW POSITION %s' % (coin, target_position[coin]))
                time.sleep(10)
            print(self)

        # target_position = self.get_position(max_tries=5)
        # print(target_position)
        # for coin in target_position.keys():
        #     print(coin)
        #     self.sniper.shoot(
        #         prop=target_position[coin],
        #         symbol=coin
        #     )
        #     self.send_dingding_msg(content='%s CHANGED\n NOW POSITION %s' % (coin, target_position[coin]))
        #     time.sleep(10)
        # exit()

    def _main(self):
        target_position = self.get_position(max_tries=5)
        for coin in target_position.keys():
            self.sniper.shoot(
                prop=target_position[coin],
                symbol=coin
            )
            self.send_dingding_msg(content='%s CHANGED\n NOW POSITION %s' % (coin, target_position[coin]))
            time.sleep(10)


if __name__ == '__main__':

    bb = BlackBox(
        api_key='ttjSfIlt1mfVAdHosOOSsbfOLuWWUaM3aiEN7XGt1qe',
        secret='gHtAJuP2CG4tF5fC9Zt1toRJreXBU20SUY0t6QX9mRv'
    )
    while True:
        try:
            bb.main()
        except Exception as e:
            print('系统报错', e, '10秒后重试')
            bb.send_dingding_msg(content='系统报错' + str(e))
            # raise Exception(e)
            time.sleep(10)
