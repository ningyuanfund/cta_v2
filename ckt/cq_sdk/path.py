import os

root = os.path.abspath(os.path.join(__file__, os.pardir, os.pardir, os.pardir))
project_root = os.path.abspath(os.path.join(__file__, os.pardir, os.pardir))
data_root = os.path.abspath(os.path.join(root, 'data'))


def get_path(_root, *args, is_folder=False):
    _p = os.path.abspath(os.path.join(_root, *args))
    if is_folder:
        _p2 = _p
    else:
        _p2 = os.path.abspath(os.path.join(_root, *args[:-1]))

    if not os.path.exists(_p2):
        os.makedirs(_p2)
    return _p


def get_project_path(*args, is_folder=False):
    return get_path(project_root, *args, is_folder=is_folder)


def get_data_path(*args, is_folder=False):
    return get_path(data_root, *args, is_folder=is_folder)
