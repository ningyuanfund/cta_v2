
from trade import Functions as fct
from trade import cta_arsenal as ctaa
import ccxt
import threading


# ----------------------------------------------------
exchange = ccxt.bitfinex({'rateLimit': 10000, 'enableRateLimit': True})
exchange.load_markets()

exchange.apiKey = 'F0Pk1zkN3OksomdMZySy0ImoC6mS3tDcTTPJ9eh9PY9'
exchange.secret = 'DgRAY8rQcjgLdl3xnl3ZOomjMgIH1vwBUQonWcmRYaK'
# ----------------------------------------------------


# cta_name；parameter最后四位一定以此是止损，止盈，移动止盈回撤比例，移动止盈启动标准
# 观察列表目前为xrp，eos，eth，若不需要观察某一币种，只需要将其设置为不可能开仓的参数即可。
# -----------------------------------------------------
selected_cta = 'bolling'
time_interval = '60T'  # k线级别，13T,17T,23T
currency_list = ['eos','eth']#observe_list
ignore_list = []#不监视的种类
para_dict = {}

para_dict['eos'] = [150, 3.5, 0.07, 0.9, 0.2, 10]
para_dict['eth'] = [200, 3.5, 0.07, 0.7, 0.3, 10]
usage_dict={'xrp':0.01, 'eos':0.5, 'eth':0.25} #每个币种能够使用的资金量
usage = 0.7#资金使用比率
# -----------------------------------------------------

def cta(selected_cta,currency_list,time_interval,para_dict,usage_dict,usage,ignore_list):
    '''

    :param selected_cta: 选择哪一个策略，str
    :param currency_list: 观察的币种，list
    :param para_dict: 不同币种的对应参数，dict
    :param usage_dict: 资金配比，dict
    :return:
    '''

    # -----------------------------------------------------
    eval('ctaa.gen_%s_files()' % selected_cta)
    t_gen_main_siganl = threading.Thread(target=fct.gen_main_siganl,args=[currency_list, time_interval, para_dict, ignore_list, selected_cta])
    t_main_trade = threading.Thread(target=fct.main_trade,args=[currency_list, para_dict, usage_dict, ignore_list, selected_cta, usage, time_interval])
    t_record = threading.Thread(target=fct.rights_dingding_notice())
    t_watchman = threading.Thread(target=fct.warn_watchman, args=[['gen_main_siganl', 'main_trade']])
    t_watchman.start()
    t_gen_main_siganl.start()
    t_main_trade.start()
    t_record.start()
    t_gen_main_siganl.join()
    t_main_trade.join()
    # -----------------------------------------------------

cta(selected_cta,currency_list,time_interval,para_dict,usage_dict,usage,ignore_list)

