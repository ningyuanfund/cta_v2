'''

'''


import pandas as pd
from trade import Functions as fct

import time
import datetime
import json
import requests
import ccxt
import numpy as np
import os
import sys



pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)

# ----------------------------------------------------
exchange = ccxt.bitfinex({'rateLimit': 10000, 'enableRateLimit': True})
exchange.load_markets()

exchange.apiKey = 'F0Pk1zkN3OksomdMZySy0ImoC6mS3tDcTTPJ9eh9PY9'
exchange.secret = 'DgRAY8rQcjgLdl3xnl3ZOomjMgIH1vwBUQonWcmRYaK'
# ----------------------------------------------------


# =====阈值布林线策略
def signal_bolling_gen(df, para,signal_loc):
    """
    布林线中轨：n天收盘价的移动平均线
    布林线上轨：n天收盘价的移动平均线 + m * n天收盘价的标准差
    布林线上轨：n天收盘价的移动平均线 - m * n天收盘价的标准差
    当收盘价由下向上穿过上轨的时候，做多；然后由上向下穿过下轨的时候，平仓。
    当收盘价由上向下穿过下轨的时候，做空；然后由下向上穿过上轨的时候，平仓。
    :param df:  原始数据
    :param para:  参数，[n, m]
    :return:
    """
    # print('generating bolling signal!')
    # 读取本地开平仓信号标记，若无，则生成文件
    try:
        df_signal = pd.read_csv(signal_loc)
        df_signal = df_signal[['median', 'upper', 'lower', 'add', 'lastest_price','lastest_update']]
    except:
        df_signal = pd.DataFrame(columns=[['median','upper','lower','add','lastest_price','lastest_update']])

    # ===计算指标
    n = para[0]
    m = para[1]

    # 计算均线
    df['median'] = df['close'].rolling(n).mean()

    # 计算上轨、下轨道、固定轨道
    df['std'] = df['close'].rolling(n).std(ddof=0)  # ddof代表标准差自由度
    df['upper'] = df['median'] + m * df['std']
    df['lower'] = df['median'] - m * df['std']
    df['fixed_upper'] = df['median'] * 1.02
    df['fixed_lower'] = df['median'] * 0.98

    # print(df)
    df_signal.at[0, 'median'] = df.iloc[-1]['median']
    df_signal.at[0, 'upper'] = max(df.iloc[-1][ 'upper'],df.iloc[-1][ 'fixed_upper'])
    df_signal.at[0, 'lower'] = min(df.iloc[-1][ 'lower'],df.iloc[-1][ 'fixed_lower'])
    df_signal = df_signal[['median','upper','lower','add','lastest_price','lastest_update']]
    df_signal.to_csv(signal_loc)
    last_row = df.iloc[0][0]
    print('本地K线文件最新记录为：',pd.to_datetime(last_row, unit='ms'))
    print(df_signal)

def gen_bolling_files():
    for currency in ['xrp','eos','eth']:
        signal_loc = fct.output_data_path + '\\' + currency + '_bolling_signal.csv'
        try:
            df_signal = pd.read_csv(signal_loc)
        except:
            df_signal = pd.DataFrame(columns=[['median','upper','lower','add','lastest_price','lastest_update']])
            for index in ['median','upper', 'lower',  'lastest_price']:
                df_signal.at[0,index] =0.0000001
            df_signal.at[0,'add'] = 0
            df_signal = df_signal[['median','upper','lower','add','lastest_price','lastest_update']]
            df_signal.to_csv(signal_loc)
            print('file_generated!')
def change_bolling_position(exchange,now_position,trade_coin,now_price,df,time_interval):

    signal_list = []
    print(trade_coin,'上轮检查价格：', df.at[0, 'lastest_price'])
    print(trade_coin,'现在最新成交价：',now_price)
    condition0 = time.time() - df.at[0, 'lastest_update'] < int(time_interval[:-1])*60


    if abs(df.at[0,'lower']*df.at[0,'upper'])<0.0001:
        return signal_list
    if now_position == 'long':
        lower_distance = str(round((now_price - df.at[0, 'median']) / now_price * 100, 4)) + '%'
        print(trade_coin + '当前价格为：' + str(now_price) + ' 距离平多(' + str(df.at[0, 'median']) + ')还需下跌：' + lower_distance)
        df.at[0, 'lastest_price'] = now_price
        if now_price < df.at[0, 'median']:
            fct.warn_local_change(period=600, orbit='main_trade')
            signal_list = fct.kidding_or_not(exchange, trade_coin, df.at[0, 'median'], 'long_close')

    if now_position == 'short':
        upper_distance = str(round(-(now_price - df.at[0, 'median']) / now_price* 100, 4)) + '%'
        print(trade_coin + '当前价格为：' + str(now_price) + ' 距离平空(' + str(df.at[0, 'median']) + ')还需上涨：' + upper_distance)
        df.at[0, 'lastest_price'] = now_price
        if now_price > df.at[0, 'median']:
            fct.warn_local_change(period=600, orbit='main_trade')

            signal_list = fct.kidding_or_not(exchange, trade_coin, df.at[0, 'median'], 'short_close')

    if now_position =='empty':

        # 当前卖价
        price_now = fct.get_price(exchange, trade_coin.upper() + '/USDT', 'sell')

        # 上一次价格
        lastest_price = df.at[0, 'lastest_price']
        # print(price_now)
        upper_distance = str(round(-(price_now - df.at[0, 'upper']) / price_now * 100, 4)) + '%'
        print(trade_coin + '当前价格为：' + str(price_now) + ' 距离开多(' + str(df.at[0, 'upper']) + ')还需上涨：' + upper_distance)
        condition1 = (price_now > df.at[0, 'upper'])  # 当前价格是否高于上轨
        # condition2 = (lastest_price < df.at[0, 'upper'])  # 上一次价格是否低于上轨
        condition2 = True

        if (condition1 and condition2):
            fct.warn_local_change(period=600, orbit='main_trade')
            signal_list = fct.kidding_or_not(exchange, trade_coin, df.at[0, 'upper'], 'long_open')
            df.at[0, 'lastest_price'] = price_now

        else:
            price_now = fct.get_price(exchange, trade_coin.upper() + '/USDT', 'buy')
            condition3 = (price_now < df.at[0, 'lower'])
            # condition4 = (lastest_price > df.at[0, 'lower'])
            condition4 = True

            lower_distance = str(round((price_now - df.at[0, 'lower']) / price_now * 100, 4)) + '%'
            print(trade_coin + '当前价格为：' + str(price_now) + ' 距离开空(' + str(df.at[0, 'lower']) + ')还需下跌：' + lower_distance)
            if (condition3 and condition4):
                fct.warn_local_change(period=600, orbit='main_trade')
                signal_list = fct.kidding_or_not(exchange, trade_coin, df.at[0, 'lower'], 'short_open')
                df.at[0, 'lastest_price'] = price_now
    condition0 = time.time() - df.at[0, 'lastest_update'] < int(time_interval[:-1]) * 60
    if condition0:
        print('完整K线尚未形成,无效信号： ',signal_list)
        signal_list = []
    else:
        print('有效信号：',signal_list)
        df.at[0, 'lastest_update'] = time.time()
    signal_loc = fct.output_data_path + '\\' + trade_coin[:3] + '_bolling_signal.csv'
    df.at[0, 'lastest_price'] = now_price
    df = df[['median', 'upper', 'lower', 'add', 'lastest_price', 'lastest_update']]
    print(df)
    df.to_csv(signal_loc)
    return signal_list


# =====CMO策略,classical over-fitting
def signal_cmo_gen(df,para,signal_loc):
    '''
    CMO大于等于n，平空开多；CMO小于等于n，平多开空
    :param df:
    :param para:[n,m] n值，m观察周期
    :param signal_loc:
    :return:
    '''

    # 计算临界值，保存于本地，供调仓模块使用
    try:
        df_signal = pd.read_csv(signal_loc)
        df_signal = df_signal[['upper','lower','add','lastest_price']]
    except:
        df_signal = pd.DataFrame(columns=[['upper','lower','add','lastest_price']])
        for index in ['upper', 'lower', 'add', 'lastest_price']:
            df_signal.at[0,index] =0

    # print(7)
    # ===计算指标
    n = para[1]
    m = para[0]
    # print(8)
    def cal_cmo(df):
        df['momentum'] = df['close'] - df['close'].shift(1)
        df['up'] = np.where(df['momentum'] > 0, df['momentum'], 0)
        df['dn'] = np.where(df['momentum'] < 0, abs(df['momentum']), 0)

        df['up_sum'] = df['up'].rolling(window=m, min_periods=1).sum()
        df['dn_sum'] = df['dn'].rolling(window=m, min_periods=1).sum()
        df['cmo'] = (df['up_sum'] - df['dn_sum']) / (df['up_sum'] + df['dn_sum']) * 100
        return df

    df = cal_cmo(df)
    print(df)
    # 通过枚举近似计算临界值
    #----------------------------

    # print(df.iloc[-1])
    df.reset_index(inplace=True)
    # print(df)
    last_row = len(df)
    df.loc[last_row] = df.iloc[-1]
    #求两列之间的时间差

    time_str_a = str(df.iloc[-3]['candle_begin_time'])
    time_arry_a = time.strptime(time_str_a, "%Y-%m-%d %H:%M:%S")
    time_str_b = str(df.iloc[-2]['candle_begin_time'])
    time_arry_b = time.strptime(time_str_b, "%Y-%m-%d %H:%M:%S")
    time_stamp_a = int(time.mktime(time_arry_a))
    time_stamp_b = int(time.mktime(time_arry_b))

    time_stamp_delta = time_stamp_b - time_stamp_a

    time_str_fake = str(df.iloc[-1]['candle_begin_time'])
    time_arry_fake =time.strptime(time_str_fake, "%Y-%m-%d %H:%M:%S")
    time_stamp_fake = int(time.mktime(time_arry_fake))+time_stamp_delta

    time_arry_fake = time.localtime(time_stamp_fake)
    time_str_fake = time.strftime("%Y-%m-%d %H:%M:%S", time_arry_fake)

    df.at[last_row,'candle_begin_time'] = time_str_fake
    df['candle_begin_time'] = pd.to_datetime(df['candle_begin_time'],format="%Y-%m-%d %H:%M:%S")
    # 计算upper
    df_upper = df
    n_real = df_upper.iloc[-1]['cmo']

    # print(df)
    upper = 0
    a = 0
    if n_real > n:
        print('计算upper')
        while True:
            # print('算上限')
            if n_real > n:
                df_upper.at[time_str_fake,'close'] = df_upper.iloc[-1]['close']*0.9999#此时价格已经大于upper，即使下降一点也可以满足cmo>n

                print(df_upper.iloc[-1]['close'])
                df_upper = cal_cmo(df_upper)
                # print(1,df_upper.iloc[-1]['close'])
                n_real = df_upper.iloc[-1]['cmo']
            else:
                upper = df_upper.iloc[-1]['close']
                a = 1
                break

    while a == 0:
        # print('算上限')
        if n_real < n:
            # print(n_real, n, df_upper.iloc[-1]['close'])
            df_upper.at[time_str_fake, 'close'] = df_upper.iloc[-1]['close'] * 1.0001#此时价格低于upper，需要上涨一点才能满足cmo>n
            df_upper = cal_cmo(df_upper)
            # print(2,df_upper.iloc[-1]['close'])
            n_real = df_upper.iloc[-1]['cmo']
        else:
            upper = df_upper.iloc[-1]['close']
            break
    print(7)
    # 计算lower
    df_lower = df
    n_real = df_lower.iloc[-1]['cmo']
    b = 0
    if n_real < -n:
        print('计算lower')
        while True:
            # print('算下限')
            if n_real < -n:
                # print(n_real, -n, df_upper.iloc[-1]['close'])
                df_lower.at[time_str_fake, 'close'] = df_lower.iloc[-1]['close']*1.0001#此时价格已经小于lower，即使上涨一点也可以满足cmo<-n
                df_lower = cal_cmo(df_lower)
                # print(3, df_lower.iloc[-1]['close'],n_real)
                n_real = df_lower.iloc[-1]['cmo']
            else:
                b = 1
                break
    while b==0 :
        # print('算下限')
        if n_real > -n:
            # print(n_real, -n, df_upper.iloc[-1]['close'])
            df_lower.at[time_str_fake, 'close'] = df_lower.iloc[-1]['close'] * 0.9999#此时价格低于upper，需要下跌一点才能满足cmo<-n
            df_lower = cal_cmo(df_lower)
            # print(4, df_lower.iloc[-1]['close'],n_real)
            n_real = df.iloc[-1]['cmo']
        else:
            break

    lower = df_lower.iloc[-1]['close']
    #----------------------------
    df_signal.at[0, 'upper'] = upper
    df_signal.at[0, 'lower'] = lower
    df_signal = df_signal[['upper', 'lower', 'add', 'lastest_price']]
    df_signal.to_csv(signal_loc)
    print(df_signal)

def gen_cmo_files():
    for currency in ['xrp','eos','eth']:
        signal_loc = fct.output_data_path + '\\' + currency + '_cmo_signal.csv'
        try:
            df_signal = pd.read_csv(signal_loc)
        except:
            df_signal = pd.DataFrame(columns=[['upper','lower','add','lastest_price']])
            for index in ['upper', 'lower',  'lastest_price']:
                df_signal.at[0,index] =0.0000001
            df_signal.at[0,'add'] = 0
            df_signal = df_signal[['upper','lower','add','lastest_price']]
            df_signal.to_csv(signal_loc)
            print('file_generated!')

def change_cmo_position(exchange,now_position,trade_coin,now_price,df):
    signal_list = []
    if df.at[0,'lower']*df.at[0,'upper']<0.001:
        return signal_list

    if now_position == 'long':
        print('检查是否平多')
        lower_distance = str(round((now_price - df.at[0, 'lower']) / now_price * 100, 4)) + '%'
        print(
            trade_coin + '当前价格为：' + str(now_price) + ' 距离平多开空(' + str(df.at[0, 'lower']) + ')还需下跌：' + lower_distance)
        if now_price < df.at[0, 'lower']:
            signal_list = fct.kidding_or_not(exchange, trade_coin, df.at[0, 'lower'], 'long_close')

    if now_position == 'short':
        print('检查是否平空')
        # print(price_now)
        upper_distance = str(round(-(now_price - df.at[0, 'upper']) / now_price * 100, 4)) + '%'
        print(trade_coin + '当前价格为：' + str(now_price) + ' 距离平空开多(' + str(df.at[0, 'upper']) + ')还需上涨：' + upper_distance)
        if now_price > df.at[0, 'upper']:
            signal_list = fct.kidding_or_not(exchange, trade_coin, df.at[0, 'upper'], 'short_close')

    if now_position == 'empty':

        # 当前卖价
        price_now = fct.get_price(exchange, trade_coin.upper() + '/USDT', 'sell')
        print(price_now)

        # print(price_now)
        upper_distance = str(round(-(price_now - df.at[0, 'upper']) / price_now * 100, 4)) + '%'
        print(trade_coin + '当前价格为：' + str(price_now) + ' 距离开多(' + str(df.at[0, 'upper']) + ')还需上涨：' + upper_distance)

        condition1 = (price_now > df.at[0, 'upper'])  # 当前价格是否高于上轨

        if (condition1):
            signal_list = fct.kidding_or_not(exchange, trade_coin, df.at[0, 'upper'], 'long_open')

        else:
            price_now = fct.get_price(exchange, trade_coin.upper() + '/USDT', 'buy')

            condition3 = (price_now < df.at[0, 'lower'])
            df.at[0, 'lastest_price'] = price_now

            lower_distance = str(round((price_now - df.at[0, 'lower']) / price_now * 100, 4)) + '%'
            print(
                trade_coin + '当前价格为：' + str(price_now) + ' 距离开空(' + str(df.at[0, 'lower']) + ')还需下跌：' + lower_distance)
            if (condition3):
                signal_list = fct.kidding_or_not(exchange, trade_coin, df.at[0, 'lower'], 'short_open')
    return signal_list




