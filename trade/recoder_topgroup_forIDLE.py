
import ccxt
import time
import pandas as pd
import os, warnings, itertools, numpy as np
from PIL import Image
from matplotlib import pyplot as plt, dates, font_manager
import threading
import datetime
from matplotlib import cm
from matplotlib import font_manager as fm
from dateutil.relativedelta import relativedelta
import sys
sys.path.append("C:\\Users\\Administrator\\Desktop\\cta_v2\\")
from trade import Functions as fct
from trade.bfx_k_line import gen_h_mt_group

# ------------------加载交易所------------------------
exchange = ccxt.bitfinex({'rateLimit': 10000, 'enableRateLimit': True})
exchange.load_markets()

exchange.apiKey = 'F0Pk1zkN3OksomdMZySy0ImoC6mS3tDcTTPJ9eh9PY9'
exchange.secret = 'DgRAY8rQcjgLdl3xnl3ZOomjMgIH1vwBUQonWcmRYaK'

# ----------------------------------------------------
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 10000)

# 获取当前程序的地址
current_file = __file__
# 程序根目录地址
root_path = os.path.abspath(os.path.join(current_file, os.pardir, os.pardir))
# 输入数据根目录地址
input_data_path = os.path.abspath(os.path.join(root_path, 'data', 'input_data'))
# 输出数据根目录地址
output_data_path = os.path.abspath(os.path.join(root_path, 'data', 'output_data'))


def symbol_transfer(symbol):
    symbol_transfer_dict = {'dsh': 'dash', 'dash': 'dsh'}
    try:
        aim = symbol_transfer_dict[symbol]
        return aim
    except:
        print('no such symbol')
        return symbol

def gen_equity_chart_old(fig_path):

    '''
    根据本地储存的市值数据，每小时更新市值图像。
    :param exchange:
    :return:
    '''

    # ======= 绘制累计净值的图片 =========

    loc = output_data_path + '\\rights_from20181218.csv'

    # 读取记录的历史净值
    draw_df = pd.read_csv(loc)
    df = draw_df


    draw_df['time'] = pd.to_datetime(draw_df['time'], format="%Y-%m-%d %H")
    draw_df = draw_df.set_index('time', drop=True)

    # print(draw_df)
    net_value_close = draw_df.resample('1h').first()
    net_value_close.reset_index(inplace = True)
    net_value_close.fillna(method='ffill', inplace=True)

    net_value_close = net_value_close[['time','net_value']]

    latest_time = str(net_value_close.iloc[-1]['time'])
    lt1_title = str(net_value_close.iloc[-1]['time'])
    latest_ntv = str(round(net_value_close.iloc[-1]['net_value'],3))
    draw_df = net_value_close[['time', 'net_value']].copy()


    content = '\nNV: ' + latest_ntv

    data_mat = draw_df.as_matrix()
    fig, ax = plt.subplots(figsize=(800 / 72, 400 / 72))  # 图片的大小，这个可以随便调，反正画出来的是矢量图
    #
    # =====下面这段是为了让横轴刻度显示时间，以及调整横轴刻度的格式
    from matplotlib.ticker import Formatter
    class MyFormatter(Formatter):
        def __init__(self, dates, fmt='%Y-%m-%d'):
            self.dates = dates
            self.fmt = fmt

        def __call__(self, x, pos=0):
            'Return the label for time x at position pos'
            ind = int(np.round(x))
            # ind就是x轴的刻度数值，不是日期的下标

            return dates.num2date(ind).strftime(self.fmt)

    formatter = MyFormatter(data_mat[:, 0])
    ax.xaxis.set_major_formatter(formatter)

    for label in ax.get_xticklabels():
        label.set_rotation(20)
        label.set_horizontalalignment('right')

    draw_df.set_index('time', inplace=True)
    # =====横轴可读调整结束

    net_value, = ax.plot(draw_df['net_value'].shift(1), color='b')
    net_value2,= ax.plot(draw_df['net_value'].shift(1), color='b',linestyle=':')
    plt.grid(axis='y')
    print(latest_time)
    plt.xlim(xmin='2019-04-13 00:00:00', xmax=latest_time)

    plt.legend([net_value, net_value2], ['net_value',content], loc='upper left')
    # plt.legend([net_value2], ['net_value2'], loc='upper center')

    plt.title('Equity Cruve From 2019-04-13 To '+lt1_title,fontsize='large',fontweight='bold')

    # 加上仓位信息
    column_list = df.columns.values.tolist()

    # print(df,column_list)
    content_deposit = 'Deposit: '
    content_margin_long = 'Margin_long: '
    content_margin_short = 'Margin_short: '
    content_exchange = 'Exchange: '
    for column in column_list:
        if 'usd' not in column:
            if 'deposit' in column:
                if df.iloc[-1][column] > 0.01:
                    content_deposit += column[8:].upper()+' '
            if 'long' in column:
                if df.iloc[-1][column] > 0.01:
                    content_margin_long += column[12:].upper()+' '
            if 'short' in column:
                if df.iloc[-1][column] > 0.01:
                    content_margin_short += column[13:].upper()+' '
            if 'exchange' in column:
                if df.iloc[-1][column] > 0.01:
                    content_exchange += column[9:].upper()+' '

    content_all = content_deposit+'\n'+content_exchange+'\n'+content_margin_long+'\n'+content_margin_short

    text_x = str(net_value_close.iloc[int(len(net_value_close) * 0.16)]['time'])
    plt.text(text_x, draw_df['net_value'].max(), content_all,
             size=12, ha="left", va="top", alpha=0.9,
             bbox=dict(boxstyle="square", color='lightcyan', ec=(1., 0.5, 0.5)))
    plt.draw()
    plt.savefig(fig_path)
    im = Image.open(fig_path)
    mark = Image.open(r'C:\Users\Administrator\Desktop\my_website\qrcode_for_KLJ.jpg')
    mark = mark.resize((129, 129))
    layer = Image.new('RGBA', im.size, (0, 0, 0, 0))
    layer.paste(mark, (im.size[0] - 230, im.size[1] - 490))
    out = Image.composite(layer, im, layer)
    out.save(fig_path)
    plt.close()
    print('generating net value chart succeeded!')

def gen_equity_chart(fig_path):

    '''
    根据本地储存的市值数据，每小时更新市值图像。
    :param exchange:
    :return:
    '''

    # ======= 绘制累计净值的图片 =========

    loc = output_data_path + '\\rights_from20181218.csv'
    # 读取记录的历史净值
    draw_df = pd.read_csv(loc)
    draw_df['time'] = pd.to_datetime(draw_df['time'], format="%Y-%m-%d %H")
    draw_df = draw_df.set_index('time', drop=True)
    net_value_close = draw_df.resample('1h').first()
    net_value_close.reset_index(inplace = True)
    net_value_close.fillna(method='ffill', inplace=True)
    net_value_close = net_value_close[['time', 'net_value']]
    latest_time = str(pd.to_datetime(net_value_close.iloc[-1]['time'])+relativedelta(hours=24))
    lt1_title = str(net_value_close.iloc[-1]['time'])
    lt2_title = str(net_value_close.iloc[-720]['time'])
    latest_ntv = str(round(net_value_close.iloc[-1]['net_value'], 3))
    draw_df = net_value_close[['time', 'net_value']].copy()
    content = '\nLastest IOPV: ' + latest_ntv

    data_mat = draw_df.as_matrix()
    fig, ax = plt.subplots(figsize=(800 / 72, 400 / 72))  # 图片的大小，这个可以随便调，反正画出来的是矢量图
    #
    # =====下面这段是为了让横轴刻度显示时间，以及调整横轴刻度的格式
    from matplotlib.ticker import Formatter
    class MyFormatter(Formatter):
        def __init__(self, dates, fmt='%Y-%m-%d'):
            self.dates = dates
            self.fmt = fmt

        def __call__(self, x, pos=0):
            'Return the label for time x at position pos'
            ind = int(np.round(x))
            # ind就是x轴的刻度数值，不是日期的下标

            return dates.num2date(ind).strftime(self.fmt)

    formatter = MyFormatter(data_mat[:, 0])
    ax.xaxis.set_major_formatter(formatter)

    for label in ax.get_xticklabels():
        label.set_rotation(20)
        label.set_horizontalalignment('right')

    draw_df.set_index('time', inplace=True)
    # =====横轴可读调整结束
    net_value, = ax.plot(draw_df['net_value'][-720:].shift(1), color='b')
    net_value2, = ax.plot(draw_df['net_value'][-720:].shift(1), color='b', linestyle=':')
    plt.grid(axis='y')
    plt.xlim(xmin=lt2_title, xmax=latest_time)
    plt.legend([net_value, net_value2], ['net_value', content], loc='upper left')
    plt.title('IOPV From %s To %s' % (lt2_title, lt1_title), fontsize='large', fontweight='bold')
    plt.draw()
    plt.savefig(fig_path)
    plt.close()
    print('generating net value chart succeeded!')

# 返还deposit或exchange或trading账户里所有余额大于0.0001币的美元价值
def hold_info(exchange, balance2, account_type='trading'):
    hold_dict = {}
    for dict in balance2:
        condition0 = (dict['type'] == account_type)
        if condition0:
            coin_amounts = float(dict['amount'])
            coin_name = dict['currency']

            if coin_amounts > 0.001:
                print(coin_name, coin_amounts)
                try:
                    if coin_name != 'usd':
                        # 有时可能会遇到symbol与交易所不符的情形，此时转化一次
                        a = 0
                        while a < 2:
                            print(a)
                            try:
                                print('check %s'% coin_name)
                                price_now = fct.get_price(exchange, coin_name.upper() + '/USDT', 'sell')
                                time.sleep(5)
                                break
                            except:
                                a += 1
                                coin_name = symbol_transfer(coin_name)
                    else:
                        price_now = 1
                    coin_worth = coin_amounts*price_now
                    hold_dict[coin_name] = coin_worth
                except:
                    time.sleep(30)
    print(account_type, hold_dict)
    return account_type, hold_dict


def gen_pie_graph(account_type, hold_dict,file_path):

    sum_value = 0
    hold_dict = hold_dict[1]
    for coin in hold_dict:
        sum_value += hold_dict[coin]
    print(account_type, sum_value)

    labels = []
    sizes = []
    dict_reverse ={}
    others_value = 0
    for coin in hold_dict:
        coin_value = round(hold_dict[coin]/sum_value*100, 2)
        if coin_value > 5:
            value_pct = coin_value
            dict_reverse[str(value_pct)] = coin
            sizes.append(value_pct)
        else:
            others_value += coin_value
    dict_reverse[str(round(others_value, 2))] = 'others'
    sizes.append(round(others_value, 2))
    sizes.sort()
    for size in sizes:
        labels.append(dict_reverse[str(size)])

    fig, axes = plt.subplots(figsize=(10, 8), ncols=2)  # 设置绘图区域大小
    ax1, ax2 = axes.ravel()

    ax1.axis('equal')
    colors = cm.rainbow(np.arange(len(sizes)) / len(sizes))  # colormaps: Paired, autumn, rainbow, gray,spring,Darks
    patches, texts, autotexts = ax1.pie(sizes, labels=labels, autopct='%1.2f%%',
                                        shadow=False, startangle=180, colors=colors)

    plt.text(-0.030514, 0.026, '%s allocation:' % account_type)
    plt.axis('equal')
    content = 'Graphed at\n %s ' % time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    plt.text(-0.030514, -0.028, content)

    # 重新设置字体大小
    proptease = fm.FontProperties()
    proptease.set_size('small')
    # font size include: ‘xx-small’,x-small’,'small’,'medium’,‘large’,‘x-large’,‘xx-large’ or number, e.g. '12'
    plt.setp(autotexts, fontproperties=proptease)
    plt.setp(texts, fontproperties=proptease)
    # ax2 只显示图例（legend）
    ax2.axis('off')
    ax2.legend(patches, labels, loc='center left')

    # plt.tight_layout()
    plt.draw()
    plt.savefig(file_path)
    plt.close()



def rights_dingding_notice():

    loaded = False
    while loaded == False:
        time.sleep(30)

        try:
            #加载交易所
            exchange = ccxt.bitfinex({'rateLimit': 10000, 'enableRateLimit': True})
            exchange.load_markets()
            exchange.apiKey = 'F0Pk1zkN3OksomdMZySy0ImoC6mS3tDcTTPJ9eh9PY9'
            exchange.secret = 'DgRAY8rQcjgLdl3xnl3ZOomjMgIH1vwBUQonWcmRYaK'
            loaded = True
        except:
            print('loading exchange failed, retry after 30s...')
            time.sleep(30)




    # 读取净值文件，如果不存在，则创建
    loc = output_data_path + '\\rights_from20181218.csv'
    try:
        df = pd.read_csv(loc)
        # print(df)
    except:
        df = pd.DataFrame(columns=['time', 'right', 'add_amount', 'name', 'diff', 'net_value'])
        df.at[0, 'time'] = '0000'
        df.to_csv(loc, index=False)


    def th1():
        counts_before_send = 0
        while True:
            #运行一轮，增加1
            counts_before_send += 1
            try:
                #获取时间信息
                second = time.strftime('%S', time.localtime(time.time()))
                minute = time.strftime('%M', time.localtime(time.time()))
                hour = time.strftime('%H', time.localtime(time.time()))

                should_I_record = False #默认不记录，每个小时记录一次
                # print(df)
                recorded_time = df.iloc[-1]['time'][-2:]#当最后一行的记录的时间与现在不一致，说明这个小时还没有记录过
                print(recorded_time)
                if hour != recorded_time:
                    should_I_record = True

                # should_I_record = True
                # 每隔一个小时记录一次

                # 从交易所读取账户状态
                balance = exchange.private_post_positions()
                balance2 = exchange.fetch_balance()['info']
                # print('balance2', balance2)
                # print('balance', balance)


                exchange_info = hold_info(exchange, balance2, 'exchange')
                file_path = r'C:\Users\Administrator\Desktop\my_website\exchange_pie.png'
                gen_pie_graph('exchange', exchange_info, file_path)

                deposit_info = hold_info(exchange, balance2, 'deposit')
                trading_info = hold_info(exchange, balance2, 'trading')


                # 如果margin中存在仓位,生成每个仓位对应的美元价值，即：开仓保证金-手续费+利润
                account_dict = {}
                if balance != []:
                    for dict in balance:
                        symbol = dict['symbol'][:3]
                        # 方向&开仓量
                        amount = float(dict['amount'])
                        if amount > 0:
                            position = 'long'
                        else:
                            position = 'short'

                        # 开仓价格&现价
                        price_open = round(float(dict['base']), 4)
                        price_now = round(fct.get_price(exchange, symbol.upper() + '/USDT', 'sell'), 4)
                        # 使用保证金
                        deposit = round(abs(amount) * price_open / 3, 2)
                        # profit&pct
                        profit = round(float(dict['swap']) + float(dict['pl']), 2)
                        profit_pct = ' [' + str(round(profit / deposit * 100, 4)) + '%]'
                        open_time = datetime.datetime.utcfromtimestamp(
                            float(dict['timestamp']) + 8 * 60 * 60).strftime("%Y-%m-%d %H:%M:%S")
                        account_dict[symbol] = [profit, open_time, position, price_open, price_now, deposit,
                                                profit_pct]

                    content = 'TREND_FOLLOWING_STG:' + '\n'
                    for symbol in account_dict:
                        content += '---------------' + symbol.upper() + '--------------' + '\n'
                        content += 'OPEN TIME: ' + account_dict[symbol][1] + '\n'
                        content += 'OPEN PRICE：' + str(account_dict[symbol][3]) + '\n'
                        content += 'NOW PRICE：' + str(account_dict[symbol][4]) + '（' + str(round(((float(
                            account_dict[symbol][4]) - float(account_dict[symbol][3])) / float(
                            account_dict[symbol][3])) * 100, 2)) + '%）\n'
                        content += 'POSITION: ' + account_dict[symbol][2] + '\n'
                        content += 'MARGIN: ' + str(account_dict[symbol][5]) + '\n'
                        content += 'PROFIT: ' + str(account_dict[symbol][0]) + account_dict[symbol][
                            6] + '\n'
                    content += 'NET VALUE: ' + str(round(df.at[len(df) - 1, 'net_value'], 3)) + '\n'
                    content += '*************************'

                    # margin有持仓的情况下，打印margin账户状态
                    print(content)
                    print(str(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))))
                    # print(counts_before_send)
                    if counts_before_send > 150:
                        counts_before_send = 0
                        fct.send_dingding_msg(content,
                                              robot_id='e7ae7cc48dd987152cf748e1cc2973a722a01577653faad975e90b357bd6b084')



                # 如果此时应该记录，则写入净值文件
                if should_I_record == True:
                    lastrow = len(df)
                    df.at[lastrow, 'time'] = str(time.strftime('%Y-%m-%d %H', time.localtime(time.time())))
                    rights = 0
                    # 记录deposit，exchange账户中资产的价值
                    for info in [deposit_info, trading_info, exchange_info]:
                        for coin in info[1]:
                            rights += info[1][coin]
                            column_name = '%s_%s' % (str(info[0]), coin)

                            df.at[lastrow, column_name] = info[1][coin]

                    for symbol in account_dict:
                        position = account_dict[symbol][2]
                        column_name = 'margin_%s_%s' % (position, symbol)
                        margin_value = account_dict[symbol][0] + account_dict[symbol][5]
                        rights += account_dict[symbol][0]
                        df.at[lastrow, column_name] = margin_value
                    df.at[lastrow, 'right'] = rights

                    # 计算净值，如果最近一次追加过投资，则diff设置为零以排除追加投资对净值的影响
                    if df.at[lastrow - 1, 'add_amount'] != 0:
                        df.at[lastrow, 'diff'] = (df.at[lastrow, 'right'] - df.at[
                            lastrow - 1, 'right']) / df.at[lastrow - 1, 'right']
                    else:
                        df.at[lastrow, 'diff'] = 0
                    # 更新净值
                    df.at[lastrow, 'net_value'] = df.at[lastrow - 1, 'net_value'] * (
                            1 + df.at[lastrow, 'diff'])
                    df.to_csv(loc, index=False)
                    fig_path = r'C:\Users\Administrator\Desktop\my_website\net_value.png'
                    gen_equity_chart(fig_path)
                time.sleep(60)
            except Exception as e:
                print('ERROR:', e)
                time.sleep(120)
                exit()

    th1()



#----------------------------------------

def top_group():


    # 获得每个币种实际的持仓量，用于判断挂单实际成交量与计划量的差别
    def get_coin_amount(exchange, symbol_name):
        balance2 = exchange.fetch_balance()['info']
        coin = symbol_name.split('/')[0].lower()
        coin1 = fct.symbol_transfer(coin)
        for dict in balance2:
            if (dict['type'] == 'exchange') & ((dict['currency'] == coin) or (dict['currency'] == coin1)):
                return float(dict['amount'])

    # 显示本地文件
    def show_hold(df_group, account_counts):

        content = 'local setting is as below:\n'
        for i in range(account_counts):
            group_dict = {}
            # 读取本地记录的仓位已经配置的币种组合
            group_info = df_group.at[0, 'spot_%s' % str(i)]
            group_info = group_info.split(':')
            group_list = []
            for a in group_info:
                group_list.append(a.split('_'))
            # 生成持仓字典
            group_dict = {}
            try:
                for b in group_list:
                    if float(b[1]) > 0:
                        group_dict[b[0]] = b[1]
            except:
                pass
            next_change_time = time.strftime('%Y-%m-%d %H:%M:%S',
                                             time.localtime(df_group.at[0, 'change_stimestamp_%s' % str(i)] / 1000))
            content += 'account%s: \nnext_change_time:%s\nhold_now:%s\n------------------\n' % (
            str(i), next_change_time, str(group_dict))
        print(content)

    # 调仓函数，调仓函数在30分钟内不得运行两次，一旦发生这一情况，终止程序并发出警报
    def change_spot(df_group, spot, trace_period=168):

        # -----检查最近是否运行过调仓程序，如果60分钟内运行过一次，则说明出现问题，发送警报并终止。
        warnpath = output_data_path
        if not os.path.exists(warnpath):
            os.makedirs(warnpath)
        warnloc = output_data_path + '\\warning.csv'
        try:
            df_warning = pd.read_csv(warnloc)
        except:
            df_warning = pd.DataFrame(columns=['last_use_timestamp'])
            df_warning.at[0, 'last_use_timestamp'] = int(time.time())
            df_warning.to_csv(warnloc, index=False)
        passed_seconds = int(time.time() - df_warning.at[0, 'last_use_timestamp'])
        if passed_seconds < 60 * 60:
            fct.send_dingding_msg('PROGRAM PAUSED')
            exit()
            # print(1)
        else:
            df_warning.at[0, 'last_use_timestamp'] = int(time.time())
            df_warning.to_csv(warnloc, index=False)
        # ------------------------------------------------------

        print('时辰已到，调整【现货仓位%s】的资产组合！' % str(spot))
        print(time.strftime('%Y.%m.%d.%H.%M.%S', time.localtime(time.time())))
        start_time = time.time()
        trade_counts = 0

        # 读取本地记录的仓位已经配置的币种组合
        # 生成持仓字典
        group_dict = {}
        columns_list = df_group.columns.values.tolist()
        execute_info = {'before': {}, 'after': {}}

        for columns_name in columns_list:

            if 'USD' in columns_name:
                coin_amounts = float(df_group.at[spot, columns_name])
                # print(columns_name, coin_amounts)
                if abs(coin_amounts) > 0.0001:
                    group_dict[columns_name] = coin_amounts

        print('当前组合：', group_dict)

        # 计算本仓位持有现货的美元价值
        spot_value_now = 0
        spot_usd = 0
        for hold_now in group_dict:
            symbol = hold_now
            if symbol == 'USD':
                spot_value_now += float(group_dict[hold_now])
                spot_usd = float(group_dict[hold_now])  # 记录账户里的usd数量
                execute_info['before'][symbol] = round(spot_usd, 2)
            else:
                buy_0 = fct.get_price(exchange, symbol, 'buy')  # 获取现货的买一价
                symbol_value = float(group_dict[hold_now]) * buy_0
                execute_info['before'][symbol] = round(symbol_value, 2)
                print(symbol, group_dict[hold_now], symbol_value)
                spot_value_now += symbol_value
        execute_info['before']['ALL'] = round(spot_value_now, 2)
        print('spot_value_now: ', spot_value_now)

        # 计算当前应当持有的仓位
        content = 'reallocating start...'
        fct.send_dingding_msg(content)
        spot_group_should_be = gen_h_mt_group(
            trace_back=trace_period)  # ['BAT/USDT', 'BAB/USDT', 'BTC/USDT', 'BTG/USDT', 'ETH/USDT']

        # 每个币种可以使用的资金为该账户整体的0.9，保证支付手续费时不串账户使用
        try:
            each_symbol_share = spot_value_now / len(spot_group_should_be) * 0.8
        except:
            each_symbol_share = 0

        # 获得每个币种实际的持仓量，防止卖出数量超过这个值
        balance2 = exchange.fetch_balance()['info']

        def get_max_amount(balance2, symbol_name):
            coin = symbol_name.split('/')[0].lower()
            coin1 = fct.symbol_transfer(coin)
            for dict in balance2:
                if (dict['type'] == 'exchange') & ((dict['currency'] == coin) or (dict['currency'] == coin1)):
                    return float(dict['amount'])

        # 获得每个币种实际的持仓量，用于判断挂单实际成交量与计划量的差别
        def get_coin_amount(exchange, symbol_name):
            balance2 = exchange.fetch_balance()['info']
            coin = symbol_name.split('/')[0].lower()
            coin1 = fct.symbol_transfer(coin)
            for dict in balance2:
                if (dict['type'] == 'exchange') & ((dict['currency'] == coin) or (dict['currency'] == coin1)):
                    return float(dict['amount'])
            time.sleep(10)

        # 先检查组合,生成减仓信息,
        sell_dict = {}  # example:{'ETH/USDT': 0.10512599332028097, 'EOS/USDT': '4'}
        for symbol_name in group_dict:
            if '/' in symbol_name:  # 说明选中的不是USD
                if symbol_name not in spot_group_should_be:  # 如果不在应当持有的列表中，直接清仓
                    print('clear %s' % symbol_name)
                    sell_amount = float(group_dict[symbol_name])
                    print('cal_sell_amount:%s' % str(sell_amount))
                    sell_max = get_max_amount(balance2, symbol_name)
                    print('max_sell_amount:%s' % str(sell_max))
                    sell_amount = min(sell_amount, sell_max)  # 取两者中小的一个做为减仓值
                    big_enough = fct.if_size_enough(symbol_name, sell_amount)  # 大于最小下单单位才能交易
                    if big_enough:
                        sell_dict[symbol_name] = sell_amount
                    else:
                        group_dict[symbol_name] = 0  # 小于减仓最小值，直接清零。
                else:  # 如果在应当持有的列表中，检查其价值是否大于each_symbol_share，是则减仓
                    buy_0 = fct.get_price(exchange, symbol_name, 'buy')  # 获取现货的买一价
                    symbol_value = float(group_dict[symbol_name]) * buy_0
                    if symbol_value > each_symbol_share:
                        sell_amount = (symbol_value - each_symbol_share) / buy_0
                        sell_max = get_max_amount(balance2, symbol_name)
                        sell_amount = min(sell_amount, sell_max)
                        big_enough = fct.if_size_enough(symbol_name, sell_amount)
                        if big_enough:
                            sell_dict[symbol_name] = sell_amount
                        else:
                            group_dict[symbol_name] = 0  # 小于减仓最小值，直接清零。

        print('卖出：', sell_dict)
        if len(sell_dict) > 0:
            content = '---REDUCE---\n'
            for coin in sell_dict:
                content += '%s: %s\n' % (coin.upper(), str(sell_dict[coin]))
            fct.send_dingding_msg(content)

        # -------------# 按照减仓信息进行减仓

        if len(sell_dict) > 0:
            usd_take_back = 0
            for symbol_sell in sell_dict:
                sell_amount = float(sell_dict[symbol_sell])
                usd_before_trade = get_coin_amount(exchange, 'USD')
                # --------------这边暂时先市价成交，后续将更新下单策略
                sold_amount = 0
                while True:
                    try:
                        # 与交易所服务器通信，获得仓位情况
                        original_amount = get_coin_amount(exchange, symbol_sell)
                        # 下单
                        result = fct.place_check_cancel(exchange, 'market', 'sell', symbol_sell, sell_amount)
                        time.sleep(30)

                        # 再次与交易所服务器通信，更新仓位情况
                        now_amount = get_coin_amount(exchange, symbol_sell)
                        sold_amount += (original_amount - now_amount)
                        print('已售出%s' % str(sold_amount))
                        if sold_amount >= sell_amount:
                            break
                        else:
                            sell_amount -= sold_amount
                            big_enough = fct.if_size_enough(symbol_sell, sell_amount)
                            if not big_enough:
                                break
                    except:
                        pass
                # 该币种调仓完成后检查增加的USD,计算拿回来多少USD
                now_usd = get_coin_amount(exchange, 'USD')
                usd_take_back += (float(now_usd) - float(usd_before_trade))
                trade_counts += 1
                print('usd take back:', usd_take_back)
                group_dict[symbol_sell] = str(
                    max((float(group_dict[symbol_sell]) - float(sold_amount)), 0))  # 既然减仓，说明之前一定有仓位

            # 更新账户持有的usd，包含减仓前的和减仓后拿回来的
            spot_usd += usd_take_back

        # 检查组合,生成建仓信息
        buy_dict = {}  # {'ETH/USDT': 0.10512599332028097, 'EOS/USDT': '4'}
        for symbol_name in spot_group_should_be:
            if symbol_name in group_dict.keys():  # 如果在应当持有的列表中，检查是否需要加仓
                print('已有持仓')
                buy_0 = fct.get_price(exchange, symbol_name, 'buy')  # 获取现货的买一价
                symbol_value = float(group_dict[symbol_name]) * buy_0
                if symbol_value < each_symbol_share:
                    buy_amount = (each_symbol_share - symbol_value) / buy_0
                    big_enough = fct.if_size_enough(symbol_name, buy_amount)
                    if big_enough:
                        buy_dict[symbol_name] = buy_amount

            else:  # 如果不在应当持有的列表中，计算建仓的量
                buy_0 = fct.get_price(exchange, symbol_name, 'buy')  # 获取现货的买一价
                buy_amount = each_symbol_share / buy_0
                big_enough = fct.if_size_enough(symbol_name, buy_amount)
                if big_enough:
                    buy_dict[symbol_name] = buy_amount

        print('买入', buy_dict)
        if len(buy_dict) > 0:
            content = '---ADD---\n'
            for coin in buy_dict:
                content += '%s: %s\n' % (coin.upper(), str(buy_dict[coin]))
            fct.send_dingding_msg(content)

        if len(buy_dict) > 0:
            # 按照建仓仓信息进行调仓
            usd_used = 0
            for symbol_buy in buy_dict:
                buy_amount = float(buy_dict[symbol_buy])
                # 与交易所服务器通信，获得仓位情况
                bought_amount = 0
                original_amount = 0
                original_usd = 0
                while True:
                    try:
                        try:
                            original_amount = get_coin_amount(exchange, symbol_buy)
                            original_usd = get_coin_amount(exchange, 'USD')
                        except:
                            pass
                        result = fct.place_check_cancel(exchange, 'market', 'buy', symbol_buy, buy_amount)
                        time.sleep(30)
                        # 检查这个币消耗了多少USD
                        now_usd = get_coin_amount(exchange, 'USD')
                        usd_used += (original_usd - now_usd)
                        # 再次与交易所服务器通信，更新仓位情况
                        now_amount = get_coin_amount(exchange, symbol_buy)
                        change_amount = (now_amount - original_amount)
                        bought_amount += change_amount
                        print('已买入%s' % str(bought_amount))
                        if bought_amount >= buy_amount:
                            break
                        else:
                            buy_amount -= bought_amount
                            big_enough = fct.if_size_enough(symbol_buy, buy_amount)
                            if not big_enough:
                                break
                        len_result = 0
                        try:
                            len_result = len(str(list(result)))
                        except:
                            pass
                        if len_result > 1:
                            break

                    except:
                        pass
                trade_counts += 1
                try:
                    group_dict[symbol_buy] = str(float(group_dict[symbol_buy]) + float(bought_amount))  # 可能是加仓
                except:
                    group_dict[symbol_buy] = str(bought_amount)  # 也可能是建新仓
            # 更新账户持有的usd
            spot_usd -= usd_used

        group_dict['USD'] = str(spot_usd)

        # 将group_dict写入group_info中
        for key in group_dict:
            df_group.at[spot, key] = group_dict[key]

        df_group.at[spot, 'next_change_timestamp'] = df_group.at[
                                                         spot, 'next_change_timestamp'] + trace_period * 60000 * 60
        df_group.at[spot, 'next_change_time'] = time.strftime('%Y/%m/%d %H:%M', time.localtime(
            df_group.at[spot, 'next_change_timestamp'] / 1000))
        df_group.to_csv(loc, index=False)
        print(df_group)
        end_time = time.time()
        consumed_time = int(end_time - start_time)
        # 调仓完成后发送执行报告
        group_dict = {}
        columns_list = df_group.columns.values.tolist()
        spot_value_now = 0

        for columns_name in columns_list:
            if 'USD' in columns_name:
                coin_amounts = float(df_group.at[spot, columns_name])
                if abs(coin_amounts) > 0.0001:
                    group_dict[columns_name] = coin_amounts

        for hold_now in group_dict:
            symbol = hold_now
            if symbol == 'USD':
                spot_value_now += float(group_dict[hold_now])
                spot_usd = float(group_dict[hold_now])  # 记录账户里的usd数量
                execute_info['after'][symbol] = round(spot_usd, 2)
            else:
                buy_0 = fct.get_price(exchange, symbol, 'buy')  # 获取现货的买一价
                symbol_value = float(group_dict[hold_now]) * buy_0
                execute_info['after'][symbol] = round(symbol_value, 2)
                spot_value_now += symbol_value
        execute_info['after']['ALL'] = round(spot_value_now, 2)

        # 处理execute_info
        execute_report = '--EXECUTE REPORT--\n Time spent: %s seconds\n Trade counts: %s\n--Spot before rebalance--\n' % (
        str(consumed_time), str(trade_counts))
        for symbol in execute_info['before']:
            symbol_value = execute_info['before'][symbol]
            execute_report += '%s:%s\n' % (symbol, str(symbol_value))
        execute_report += '--Spot after rebalance--\n'
        for symbol in execute_info['after']:
            symbol_value = execute_info['after'][symbol]
            execute_report += '%s:%s\n' % (symbol, str(symbol_value))
        execute_report += 'Next rebalance time: \n %s' % df_group.at[spot, 'next_change_time']
        fct.send_dingding_msg(execute_report)

    # 预报还有多久调仓，返还sleep时间
    def forcast(df_group):
        '''

        :param df_group:
        :return:
        '''
        now = exchange.milliseconds()
        nearest = df_group['next_change_timestamp'].min()
        seconds = (nearest - now) / 1000
        mins = int(seconds / 60)
        hours = 0
        if mins > 0:
            hours = int(mins / 60)
            if hours > 0:
                mins = int(mins - hours * 60)
        conten0 = '检查时间:%s' % str(time.strftime('%Y/%m/%d %H:%M:%S', time.localtime(time.time())))
        content = '\n距离最近一次调仓还有%s小时%s分钟' % (str(hours), str(mins))
        print(conten0, content)
        if seconds > 60:
            time.sleep(int(seconds / 2))
        else:
            time.sleep(30)

    # ------------------读取交易所exchange中的数据------------------------

    # 168小时的动量，24小时调整一次，总计七个账户
    mt_period = 168
    change_rate = 24
    account_counts = int(mt_period / change_rate)

    # 规定每个账户可用的资金
    avaliable_USD = 500

    # 采用本地记录的方式进行隔离各个仓位
    loc = output_data_path + '\\group_info.csv'
    df_group = pd.read_csv(loc, dtype={'next_change_timestamp': np.int64})
    print(df_group)

    while True:
        try:
            for i in range(account_counts):
                change_timestamp = df_group.at[i, 'next_change_timestamp']
                now = exchange.milliseconds()
                condition0 = (change_timestamp < now)  # 到达预定的调仓时间
                # print(condition0)
                if condition0:
                    change_spot(df_group, i, trace_period=mt_period)  # 调仓
                else:
                    pass
            forcast(df_group)

        except Exception as e:
            print(e)
            time.sleep(30)


def threcoder():
    while True:
        try:
            rights_dingding_notice()
        except:
            time.sleep(120)


t_rec = threading.Thread(target=threcoder)
t_top = threading.Thread(target=top_group)

t_rec.start()
t_top.start()

