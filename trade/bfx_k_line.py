import os
import sys
import time
import pandas as pd
from datetime import datetime, timedelta
from sqlalchemy import create_engine
from trade import Functions as fct
import ccxt
from dateutil.relativedelta import relativedelta




pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)

# ----远程服务器
DB_USER='postgres'
DB_PASS='holt'
DB_HOST='49.51.196.202'
DB_PORT='5432'
DATABASE='bfx_data'

#-----本地
# DB_USER='postgres'
# DB_PASS='tanghao56191328'
# DB_HOST='localhost'
# DB_PORT='5432'
# DATABASE='my_test'

connect_info = 'postgresql://{}:{}@{}:{}/{}'.format(DB_USER, DB_PASS, DB_HOST, DB_PORT, DATABASE)

# -----------------------------------------------------------------------------
# common constants

msec = 1000
minute = 60 * msec
hold = 30

# -----------------------------------------------------------------------------
engine_Kline = create_engine(connect_info,echo=False)
exchange = ccxt.bitfinex({
    'rateLimit': 10000,
    'enableRateLimit': True,
    # 'verbose': True,
})
# -----------------------------------------------------------------------------

def rsql_tablename(schemaname='public',engine=engine_Kline):
    sql = "select tablename from pg_tables where schemaname='%s'" % schemaname
    table_df = pd.read_sql(sql, con=engine)
    return table_df
# df=rsql_tablename(schemaname='public',engine=engine_Kline)
# print(df)
# for i in range(df.shape[0]):
#     print(df.at[i,df.columns.values.tolist()[0]])
#
# exit()
# #
# 获得指定币种的累计交易额和指定时间收盘价
def get_base_and_vol2d(symbol,trace_back,date_from):

    date_list = pd.date_range(start=pd.to_datetime(date_from),end=pd.to_datetime(date_from)+relativedelta(hours=trace_back),freq='D')
    df_list = []
    for date in date_list:
        # print(date)
        try:
            try:
                table_name = 'bfx_data_%s/USDT_%s' % (symbol.upper(), str(date)[:10])
                print(table_name)
                sql_cmd = 'select * from public."%s"' % table_name
                df = pd.read_sql(sql_cmd, con=engine_Kline)
                df_list.append(df)
            except Exception as e:
                print(e)
                table_name = 'bfx_data_%s/USDT_%s ' % (symbol.upper(), str(date)[:10])
                sql_cmd = 'select * from public."%s"' % table_name
                df = pd.read_sql(sql_cmd, con=engine_Kline)
                df_list.append(df)
        except:
            break
    df = pd.concat(df_list)
    df['candle_begin_time'] = pd.to_datetime(df['candle_begin_time'])
    df = df.resample(rule='1H',
                     on='candle_begin_time',
                     label='left',
                     closed='left').agg({'open': 'first',
                                         'high': 'max',
                                         'low': 'min',
                                         'close': 'last',
                                         'volume': 'sum'})
    df['v2d'] = df['volume']*df['close']
    all_trade_amounts = df[pd.to_datetime(date_from):]['v2d'].sum()
    df.fillna(method='ffill', inplace=True)
    base_price = df.at[pd.to_datetime(date_from), 'close']
    print(df)
    # print(all_trade_amounts , base_price)
    return all_trade_amounts,base_price

# base_info = get_base_and_vol2d('BTC',168,'2019-06-02 02:00:00')

def symbol_for_sever(symbol):
    try:
        dict_symbol = {'BAB':'BCH'}
        adj_symbol = dict_symbol[symbol]
    except:
        adj_symbol = symbol
    return adj_symbol

# 从数据库获得所有币种已储存的基期收盘价和累计交易额
def load_data_from_sever(trace_back,date_from):
    '''
    应当在数据库已经把前一天的所有数据更新完成后运行，不足的交易额数据利用ticker快照补齐
    :param trace_back:
    :param date_from:
    :return:
    '''
    symbol_list = ccxt.bitfinex().public_get_symbols()
    valid_list = [symbol.upper().replace('/', '').replace('USD','') for symbol in symbol_list if symbol.endswith('usd')]
    print(valid_list)
    df_base = pd.DataFrame()
    for symbol in valid_list:
        sever_symbol = symbol_for_sever(symbol) # 转换为服务器用的symbol
        try:
            base_info = get_base_and_vol2d(sever_symbol, trace_back, date_from)
            df_base.at[symbol+'USD','v2d'] = base_info[0]
            df_base.at[symbol+'USD','base_price'] = base_info[1]
            df_base.at[symbol+'USD','base_time'] = date_from
        except:
            pass
    df_base.reset_index(inplace=True)
    print(df_base)
    df_base.rename(columns={'index':'SYMBOL'},inplace=True)
    return df_base

# df_base = load_data_from_sever(168,'2019-06-02 02:00:00')
# print(df_base)
# exit()



def gen_h_mt_group(change_timestamp,trace_back=168,pool_range=10):

    base_timestamp = change_timestamp - 60000 * 60 * trace_back
    date_from = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(base_timestamp / 1000))

    # 提前从数据库提取数据
    data_from_sever = load_data_from_sever(trace_back, date_from)
    print(data_from_sever)
    # print(time.time())
    time_left = int((change_timestamp - time.time()*1000)/1000)
    print('计划%ss后获取ticker'%str(time_left))
    time.sleep(time_left)
    print('开始获取ticker')
    bitfinex2 = ccxt.bitfinex2()
    ticker_meaning = ['SYMBOL', 'BID', 'BID_SIZE',
                      'ASK', 'ASK_SIZE', 'DAILY_CHANGE',
                      'DAILY_CHANGE_PERC', 'LAST_PRICE',
                      'VOLUME', 'HIGH', 'LOW']
    all_coin_data = bitfinex2.public_get_tickers(params={'symbols': 'ALL'})
    # print(all_coin_data)
    df_ticker = pd.DataFrame()
    for coin_data in all_coin_data:
        coinsymbol = coin_data[0]
        if coinsymbol[-1] == 'D' and len(coinsymbol)>5 and coinsymbol != 'tUSTUSD':
            coin_data[0] = coin_data[0][1:]
            len_df_ticker = len(df_ticker)
            try:
                for data in coin_data:
                    df_ticker.at[len_df_ticker,ticker_meaning[coin_data.index(data)]] = data
                    # 写完之后删除列表中数据，防止再次匹配上
                    coin_data[coin_data.index(data)] = 'used'
                    # print(df_ticker)
            except Exception as e:
                print(e)

    df_ticker['esti_vol_tousd'] = df_ticker['LAST_PRICE']*df_ticker['VOLUME']
    df = df_ticker.merge(data_from_sever,on='SYMBOL',how='outer')
    df['all_v2d'] = df['esti_vol_tousd'] + df['v2d']
    df = df[df['all_v2d']>100000]
    # df_ticker.set_index('SYMBOL',inplace=True, drop=True)
    df.sort_values(by='all_v2d', inplace=True, ascending=False)
    df.reset_index(inplace=True,drop=True)

    # 选取可交易币池
    # print(df[0:pool_range])
    df_coin_pool = df.head(pool_range)[['SYMBOL','base_price','LAST_PRICE','base_time']]
    df_coin_pool['mt'] = df_coin_pool['LAST_PRICE']/df_coin_pool['base_price'] - 1
    # 分组
    quantile_info = (0.3, 0.7)
    mt_group = df_coin_pool['mt'].quantile(quantile_info)
    condition1 = df_coin_pool['mt'] >= mt_group[quantile_info[1]]
    condition11 = df_coin_pool['mt'] > 0.02
    df_coin_pool.loc[condition1 & condition11, 'mt_group'] = 1

    print(df_coin_pool)
    hold_list = []
    for i in range(df_coin_pool.shape[0]):
        if df_coin_pool.at[i, 'mt_group'] == 1:
            hold_list.append(df_coin_pool.at[i, 'SYMBOL'])
    print(hold_list)

    return hold_list



def get_k_line(from_timestamp, symbol, time_interval):
    '''

    :param start_datetime:2019-02-01 00:00:00
    :param symbol: 'ETH/USD'
    :param time_interval:1h
    :return:
    '''

    # print(from_timestamp)
    now = exchange.milliseconds()
    df = pd.DataFrame(columns=['candle_begin_time', 'open', 'high', 'low', 'close', 'volume'])

    a = 0
    lastest_K = 0
    while from_timestamp < now:
        if exchange.iso8601(from_timestamp) == lastest_K:
            a += 1
        if a == 3:
            break

        try:
            # print(exchange.milliseconds(), 'Fetching %s candles starting from' % symbol, exchange.iso8601(from_timestamp))
            lastest_K = exchange.iso8601(from_timestamp)
            ohlcvs = exchange.fetch_ohlcv(symbol, time_interval, from_timestamp)
            if time_interval.endswith('m'):
                from_timestamp += len(ohlcvs) * minute * int(time_interval.strip('m'))
            if time_interval.endswith('h'):
                from_timestamp += len(ohlcvs) * minute * int(time_interval.strip('h')) * 60

            ohlcvs = pd.DataFrame(ohlcvs)
            if len(ohlcvs) > 0:
                ohlcvs.columns = ['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']
                ohlcvs['candle_begin_time'] = pd.to_datetime(ohlcvs['candle_begin_time'], unit='ms')
                df = df.append(ohlcvs)


        except (ccxt.ExchangeError, ccxt.AuthenticationError, ccxt.ExchangeNotAvailable, ccxt.RequestTimeout) as error:

            print('Got an error', type(error).__name__, error.args, ', retrying in', hold, 'seconds...')
            time.sleep(hold)

    # df['candle_begin_time_GTM8'] = df['candle_begin_time'] + timedelta(hours=8)
    df.reset_index(inplace=True)
    # print('%s K lines fulfilled.' % symbol)
    return df




# list = gen_h_mt_group(trace_back=168)


change_timestamp = 1562083200000
###################1561181267
print(time.strftime('%Y.%m.%d.%H.%M.%S',time.localtime(time.time())))
gen_h_mt_group(change_timestamp,trace_back=168,pool_range=30)
print(time.strftime('%Y.%m.%d.%H.%M.%S',time.localtime(time.time())))
