import os
import pandas as pd
import ccxt
import sys
import time
import numpy as np
from sqlalchemy import create_engine
import trade.Functions as fct
from trade.gen_hold_info import gen_h_mt_group

pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)


# ---- load DB
DB_USER='postgres'
DB_PASS='holt'
DB_HOST='localhost'
DB_PORT='5432'
DATABASE='group_info'


connect_info = 'postgresql://{}:{}@{}:{}/{}'.format(DB_USER, DB_PASS, DB_HOST, DB_PORT, DATABASE)
engine = create_engine(connect_info, echo=False)


# 获取当前程序的地址
current_file = __file__

# 程序根目录地址
root_path = os.path.abspath(os.path.join(current_file, os.pardir, os.pardir))

# 输出数据根目录地址
output_data_path = os.path.abspath(os.path.join(root_path, 'data', 'output_data'))


# ------------------加载交易所------------------------
exchange = ccxt.bitfinex({'rateLimit': 10000, 'enableRateLimit': True})
exchange.load_markets()

exchange.apiKey = 'F0Pk1zkN3OksomdMZySy0ImoC6mS3tDcTTPJ9eh9PY9'
exchange.secret = 'DgRAY8rQcjgLdl3xnl3ZOomjMgIH1vwBUQonWcmRYaK'
# ----------------------------------------------------

# 获得每个币种实际的持仓量，用于判断挂单实际成交量与计划量的差别
def get_coin_amount(exchange, symbol_name):
    balance2 = exchange.fetch_balance()['info']
    coin = symbol_name.split('/')[0].lower()
    coin1 = fct.symbol_transfer(coin)
    for dict in balance2:
        if (dict['type'] == 'exchange') & ((dict['currency'] == coin) or (dict['currency'] == coin1)):
            return float(dict['amount'])


# 显示本地文件
def show_hold(df_group,account_counts):
    content = 'local setting is as below:\n'
    for i in range(account_counts):
        group_dict = {}
        # 读取本地记录的仓位已经配置的币种组合
        group_info = df_group.at[0, 'spot_%s' % str(i)]
        group_info = group_info.split(':')
        group_list = []
        for a in group_info:
            group_list.append(a.split('_'))
        # 生成持仓字典
        group_dict = {}
        try:
            for b in group_list:
                if float(b[1]) > 0:
                    group_dict[b[0]] = b[1]
        except:
            pass
        next_change_time = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(df_group.at[0, 'change_stimestamp_%s' % str(i)]/1000))
        content += 'account%s: \nnext_change_time:%s\nhold_now:%s\n------------------\n' % (str(i),next_change_time,str(group_dict))
    print(content)

# 将group_dict写入group_info中
def write_2_file(group_dict, spot, df_group, loc,trace_period,time_record=False):
    for key in group_dict:
        df_group.at[spot, key] = float(group_dict[key])
    if time_record:
        df_group.at[spot, 'next_change_timestamp'] = df_group.at[
                                                         spot, 'next_change_timestamp'] + trace_period * 60000 * 60
        df_group.at[spot, 'next_change_time'] = time.strftime('%Y/%m/%d %H:%M', time.localtime(
            df_group.at[spot, 'next_change_timestamp'] / 1000))
    df_group.to_csv(loc)

# 调仓函数，调仓函数在30分钟内不得运行两次，一旦发生这一情况，终止程序并发出警报
def change_spot(change_timestamp,df_group,spot,trace_period=168):

# -----检查最近是否运行过调仓程序，如果60分钟内运行过一次，则说明出现问题，发送警报并终止。
    warnpath = output_data_path
    if not os.path.exists(warnpath):
        os.makedirs(warnpath)
    warnloc = output_data_path+'\\warning.csv'
    try:
        df_warning = pd.read_csv(warnloc)
    except:
        df_warning = pd.DataFrame(columns=['last_use_timestamp'])
        df_warning.at[0, 'last_use_timestamp'] = int(time.time())
        df_warning.to_csv(warnloc, index=False)
    passed_seconds = int(time.time()-df_warning.at[0, 'last_use_timestamp'])
    if passed_seconds < 60*60:
        fct.send_dingding_msg('PROGRAM PAUSED')
        exit()
        # print(1)
    else:
        df_warning.at[0, 'last_use_timestamp'] = int(time.time())
        df_warning.to_csv(warnloc, index=False)
# ------------------------------------------------------
    df_group = pd.read_csv(loc)
    print('时辰已到，调整【现货仓位%s】的资产组合！' % str(spot))
    print(time.strftime('%Y.%m.%d.%H.%M.%S', time.localtime(time.time())))
    start_time = time.time()
    trade_counts = 0


    # 读取本地记录的仓位已经配置的币种组合
    # 生成持仓字典
    group_dict = {}
    columns_list = df_group.columns.values.tolist()
    execute_info = {'before': {}, 'after': {}}


    for columns_name in columns_list:

        if 'USD' in columns_name:
            coin_amounts = float(df_group.at[spot, columns_name])
            # print(columns_name, coin_amounts)
            if abs(coin_amounts) > 0.0001:
                group_dict[columns_name] = coin_amounts

    print('当前组合：', group_dict)


    # 计算本仓位持有现货的美元价值
    spot_value_now = 0
    spot_usd = 0
    for hold_now in group_dict:
        symbol = hold_now
        if symbol == 'USD':
            spot_value_now += float(group_dict[hold_now])
            spot_usd = float(group_dict[hold_now])   # 记录账户里的usd数量
            execute_info['before'][symbol] = round(spot_usd, 2)
        else:
            buy_0 = fct.get_price(exchange, symbol, 'buy')  # 获取现货的买一价
            symbol_value = float(group_dict[hold_now])*buy_0
            execute_info['before'][symbol] = round(symbol_value, 2)
            print(symbol, group_dict[hold_now], symbol_value)
            spot_value_now += symbol_value
    execute_info['before']['ALL'] = round(spot_value_now, 2)
    print('spot_value_now: ', spot_value_now)

    # 计算当前应当持有的仓位
    content = 'reallocating start...'
    fct.send_dingding_msg(content)
    # spot_group_should_be = gen_h_mt_group(trace_back=trace_period)  # ['BAT/USDT', 'BAB/USDT', 'BTC/USDT', 'BTG/USDT', 'ETH/USDT']

    # spot_group_should_be new ver
    spot_group_should_be =gen_h_mt_group(change_timestamp,trace_back=168, pool_range=4)

    # 每个币种可以使用的资金为该账户整体的0.9，保证支付手续费时不串账户使用
    try:
        each_symbol_share = spot_value_now/len(spot_group_should_be)*0.8
    except:
        each_symbol_share = 0

    # 获得每个币种实际的持仓量，防止卖出数量超过这个值
    balance2 = exchange.fetch_balance()['info']
    def get_max_amount(balance2,symbol_name):
        coin = symbol_name.split('/')[0].lower()
        coin1 = fct.symbol_transfer(coin)
        for dict in balance2:
            if (dict['type'] == 'exchange') & ((dict['currency'] == coin) or (dict['currency'] == coin1)):
                return float(dict['amount'])

    # 获得每个币种实际的持仓量，用于判断挂单实际成交量与计划量的差别
    def get_coin_amount(exchange, symbol_name):
        balance2 = exchange.fetch_balance()['info']
        coin = symbol_name.split('/')[0].lower()
        coin1 = fct.symbol_transfer(coin)
        for dict in balance2:
            if (dict['type'] == 'exchange') & ((dict['currency'] == coin) or (dict['currency'] == coin1)):
                return float(dict['amount'])
        time.sleep(10)

    # 先检查组合,生成减仓信息,
    sell_dict = {}  # example:{'ETH/USDT': 0.10512599332028097, 'EOS/USDT': '4'}
    for symbol_name in group_dict:
        if '/' in symbol_name:  # 说明选中的不是USD
            if symbol_name not in spot_group_should_be:  # 如果不在应当持有的列表中，直接清仓
                print('clear %s' % symbol_name)
                sell_amount = float(group_dict[symbol_name])
                print('cal_sell_amount:%s' % str(sell_amount))
                sell_max = get_max_amount(balance2, symbol_name)
                print('max_sell_amount:%s' % str(sell_max))
                sell_amount = min(sell_amount, sell_max)    # 取两者中小的一个做为减仓值
                big_enough = fct.if_size_enough(symbol_name, sell_amount)   # 大于最小下单单位才能交易
                if big_enough:
                    sell_dict[symbol_name] = sell_amount
                else:
                    group_dict[symbol_name] = 0     # 小于减仓最小值，直接清零。
            else:   # 如果在应当持有的列表中，检查其价值是否大于each_symbol_share，是则减仓
                buy_0 = fct.get_price(exchange, symbol_name, 'buy')  # 获取现货的买一价
                symbol_value = float(group_dict[symbol_name])*buy_0
                if symbol_value > each_symbol_share:
                    sell_amount = (symbol_value-each_symbol_share)/buy_0
                    sell_max = get_max_amount(balance2, symbol_name)
                    sell_amount = min(sell_amount, sell_max)
                    big_enough = fct.if_size_enough(symbol_name, sell_amount)
                    if big_enough:
                        sell_dict[symbol_name] = sell_amount
                    else:
                        group_dict[symbol_name] = 0  # 小于减仓最小值，直接清零。

    print('卖出：', sell_dict)
    if len(sell_dict) > 0:
        content = '---REDUCE---\n'
        for coin in sell_dict:
            content += '%s: %s\n' % (coin.upper(), str(sell_dict[coin]))
        fct.send_dingding_msg(content)


# -------------# 按照减仓信息进行减仓

    if len(sell_dict) > 0:
        usd_take_back = 0
        for symbol_sell in sell_dict:
            sell_amount = float(sell_dict[symbol_sell])
            usd_before_trade = get_coin_amount(exchange, 'USD')
            #--------------这边暂时先市价成交，后续将更新下单策略
            sold_amount = 0
            while True:
                try:
                    # 与交易所服务器通信，获得仓位情况
                    original_amount = get_coin_amount(exchange, symbol_sell)
                    # 下单
                    result = fct.place_check_cancel(exchange, 'market', 'sell', symbol_sell, sell_amount)
                    time.sleep(30)

                    # 再次与交易所服务器通信，更新仓位情况
                    now_amount = get_coin_amount(exchange, symbol_sell)
                    sold_amount += (original_amount - now_amount)
                    print('已售出%s' % str(sold_amount))
                    if sold_amount >= sell_amount:
                        break
                    else:
                        sell_amount -= sold_amount
                        big_enough = fct.if_size_enough(symbol_sell, sell_amount)
                        if not big_enough:
                            break
                except:
                    pass
            # 该币种调仓完成后检查增加的USD,计算拿回来多少USD
            now_usd = get_coin_amount(exchange, 'USD')
            usd_take_back += (float(now_usd) - float(usd_before_trade))
            trade_counts += 1
            print('usd take back:', usd_take_back)
            group_dict[symbol_sell] = str(max((float(group_dict[symbol_sell])-float(sold_amount)), 0))  # 既然减仓，说明之前一定有仓位
            # write_2_file(group_dict, spot, df_group, loc, trace_period)

        # 更新账户持有的usd，包含减仓前的和减仓后拿回来的
        spot_usd += usd_take_back
        # write_2_file(group_dict, spot, df_group, loc, trace_period)

    # 检查组合,生成建仓信息
    buy_dict = {}  # {'ETH/USDT': 0.10512599332028097, 'EOS/USDT': '4'}
    for symbol_name in spot_group_should_be:
        if symbol_name in group_dict.keys():  # 如果在应当持有的列表中，检查是否需要加仓
            print('已有持仓')
            buy_0 = fct.get_price(exchange, symbol_name, 'buy')  # 获取现货的买一价
            symbol_value = float(group_dict[symbol_name]) * buy_0
            print('持仓价值',symbol_value,'lilun jiazhi',each_symbol_share)
            if symbol_value < each_symbol_share:
                buy_amount = (each_symbol_share - symbol_value) / buy_0
                big_enough = fct.if_size_enough(symbol_name, buy_amount)
                if big_enough:
                    buy_dict[symbol_name] = buy_amount

        else:  # 如果不在应当持有的列表中，计算建仓的量
            buy_0 = fct.get_price(exchange, symbol_name, 'buy')  # 获取现货的买一价
            buy_amount = each_symbol_share/buy_0
            big_enough = fct.if_size_enough(symbol_name, buy_amount)
            if big_enough:
                buy_dict[symbol_name] = buy_amount

    print('买入', buy_dict)
    if len(buy_dict) > 0:
        content = '---ADD---\n'
        for coin in buy_dict:
            content += '%s: %s\n' % (coin.upper(), str(buy_dict[coin]))
        fct.send_dingding_msg(content)

# 使用超限则停止
    if len(buy_dict) > 0:
        # 按照建仓仓信息进行调仓
        usd_used = 0
        for symbol_buy in buy_dict:
            buy_amount = float(buy_dict[symbol_buy])
            # 与交易所服务器通信，获得仓位情况
            bought_amount = 0
            original_amount = 0
            original_usd = 0
            trade_or_not = True
            while trade_or_not:
                try:
                    try:
                        original_amount = get_coin_amount(exchange, symbol_buy)
                        original_usd = get_coin_amount(exchange, 'USD')
                    except:
                        pass
                    result = fct.place_check_cancel(exchange, 'market', 'buy', symbol_buy, buy_amount)
                    try:
                        print(result)
                    except Exception as e:
                        print(e)
                    time.sleep(30)
                    # 检查这个币消耗了多少USD
                    now_usd = get_coin_amount(exchange, 'USD')
                    usd_used += (original_usd - now_usd)
                    # 再次与交易所服务器通信，更新仓位情况
                    now_amount = get_coin_amount(exchange, symbol_buy)
                    change_amount = (now_amount - original_amount)
                    bought_amount += change_amount
                    print('已买入%s' % str(bought_amount))
                    if bought_amount >= buy_amount:
                        trade_or_not = False
                    else:
                        buy_amount -= bought_amount
                        big_enough = fct.if_size_enough(symbol_buy, buy_amount)
                        if not big_enough:
                            trade_or_not = False
                except:
                    pass
            trade_counts += 1
            try:
                group_dict[symbol_buy] = str(float(group_dict[symbol_buy])+float(bought_amount))  # 可能是加仓
            except:
                group_dict[symbol_buy] = str(bought_amount)  # 也可能是建新仓
            # write_2_file(group_dict, spot, df_group, loc, trace_period)
        # 更新账户持有的usd
        spot_usd -= usd_used
        # write_2_file(group_dict, spot, df_group, loc, trace_period)

    group_dict['USD'] = str(spot_usd)
    write_2_file(group_dict, spot, df_group, loc, trace_period,time_record=True)


# 调仓完成后发送执行报告

    print(df_group)
    end_time = time.time()
    consumed_time = int(end_time - start_time)
    group_dict = {}
    columns_list = df_group.columns.values.tolist()
    spot_value_now = 0

    for columns_name in columns_list:
        if 'USD' in columns_name:
            coin_amounts = float(df_group.at[spot, columns_name])
            if abs(coin_amounts) > 0.0001:
                group_dict[columns_name] = coin_amounts

    for hold_now in group_dict:
        symbol = hold_now
        if symbol == 'USD':
            spot_value_now += float(group_dict[hold_now])
            spot_usd = float(group_dict[hold_now])   # 记录账户里的usd数量
            execute_info['after'][symbol] = round(spot_usd, 2)
        else:
            buy_0 = fct.get_price(exchange, symbol, 'buy')  # 获取现货的买一价
            symbol_value = float(group_dict[hold_now])*buy_0
            execute_info['after'][symbol] = round(symbol_value, 2)
            spot_value_now += symbol_value
    execute_info['after']['ALL'] = round(spot_value_now, 2)

    # 处理execute_info
    execute_report = '--EXECUTE REPORT--\n Time spent: %s seconds\n Trade counts: %s\n--Spot before rebalance--\n' % (str(consumed_time), str(trade_counts))
    for symbol in execute_info['before']:
        symbol_value = execute_info['before'][symbol]
        execute_report += '%s:%s\n' % (symbol, str(symbol_value))
    execute_report += '--Spot after rebalance--\n'
    for symbol in execute_info['after']:
        symbol_value = execute_info['after'][symbol]
        execute_report += '%s:%s\n' % (symbol, str(symbol_value))
    execute_report += 'Next rebalance time: \n %s' % df_group.at[spot, 'next_change_time']
    fct.send_dingding_msg(execute_report)
    # df_group = fct.wash_group_info(df_group)
    # df_group.to_csv(loc)

# 预报还有多久调仓，返还sleep时间
def forcast(df_group):
    '''

    :param df_group:
    :return:
    '''
    now = exchange.milliseconds()
    nearest = df_group['next_change_timestamp'].min()
    seconds = (nearest - now)/1000
    mins = int(seconds / 60)
    hours = 0
    if mins > 0:
        hours = int(mins/60)
        if hours > 0:
            mins = int(mins-hours*60)
    conten0 = '检查时间:%s' % str(time.strftime('%Y/%m/%d %H:%M:%S', time.localtime(time.time())))
    content = '\n距离最近一次调仓还有%s小时%s分钟'%(str(hours),str(mins))
    print(conten0, content)
    if seconds > 60:
        time.sleep(int(seconds/2))
    else:
        time.sleep(30)




# ------------------读取交易所exchange中的数据------------------------

# 168小时的动量，24小时调整一次，总计七个账户
mt_period = 168
change_rate = 24
account_counts = int(mt_period/change_rate)

# 规定每个账户可用的资金
avaliable_USD = 500

# 采用本地记录的方式进行隔离各个仓位
loc = output_data_path + '\\group_info.csv'
# df_group = pd.read_csv(loc, dtype={'next_change_timestamp': np.int64, 'next_change_time': str})
df_group = pd.read_csv(loc,index_col=False)

print(df_group)

while True:
    try:
        for i in range(account_counts):
            change_timestamp = df_group.at[i, 'next_change_timestamp']
            now = exchange.milliseconds()
            condition0 = (change_timestamp < now-600000)  # 距离到达预定的调仓时间60s
            # print(condition0)
            if condition0:
                change_spot(change_timestamp,df_group, i, trace_period=mt_period)  # 调仓

            else:
                pass
        forcast(df_group)

    except Exception as e:
        print(e)
        time.sleep(30)




