
import ccxt
import time
from datetime import datetime
import pandas as pd
pd.set_option('display.unicode.ambiguous_as_wide', True)
pd.set_option('display.unicode.east_asian_width', True)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width',1000)
# ----------------------------------------------------




def record_my_trades(currency):

    exchange = ccxt.bitfinex({'rateLimit': 10000, 'enableRateLimit': True})
    exchange.load_markets()

    coin_type = currency.upper()+'/USDT'
    exchange.apiKey = 'F0Pk1zkN3OksomdMZySy0ImoC6mS3tDcTTPJ9eh9PY9'
    exchange.secret = 'DgRAY8rQcjgLdl3xnl3ZOomjMgIH1vwBUQonWcmRYaK'
    trade_history = exchange.fetch_my_trades(coin_type)
    print(trade_history)

    # Sell对应现金流入，Buy对应现金流出
    flow_convert = {'Sell':1,'Buy':-1}

    # 将属于同一笔交易的每笔成交汇总，计入字典，key为该笔交易出现第一笔成交的时间
    trade_dict = {}
    trade_type = trade_history[0]['info']['type']
    trade_timestamp = float(trade_history[0]['info']['timestamp'])
    start_trade_timestamp = trade_timestamp
    trade_amount_coin = float(trade_history[0]['info']['amount'])*flow_convert[trade_type]
    trade_amount_usd = float(trade_history[0]['info']['price'])*float(trade_history[0]['info']['amount'])*flow_convert[trade_type]

    for trade in trade_history[1:]:#从第二位交易信息开始遍历，避免重复取值
        trade_info = trade['info']

        # 如果该笔成交与上笔成交发生时间非常接近，说明他们属于同一笔交易，累计
        if float(trade_info['timestamp'])-float(trade_timestamp) < 50:
            trade_amount_usd += float(trade_info['price'])*float(trade_info['amount'])*flow_convert[trade_type]
            trade_amount_coin += float(trade_info['amount'])*flow_convert[trade_type]
            trade_timestamp = float(trade_info['timestamp'])

        #如果该笔交易与上笔交易发生时间相距超过50，则上一笔交易处理完整，计入trade_dict
        else:
            trade_dict[str(start_trade_timestamp)] = [time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(float(start_trade_timestamp))),trade_type,trade_amount_coin,trade_amount_usd]
            #更新下一桩交易
            trade_type = trade_info['type']
            start_trade_timestamp = float(trade_info['timestamp'])
            trade_timestamp = trade_info['timestamp']
            trade_amount_coin = float(trade_info['amount']) * flow_convert[trade_type]
            trade_amount_usd = float(trade_info['price'])*float(trade_info['amount'])*flow_convert[trade_type]

    print(trade_dict)

    #将交易信息整合成数据框格式处理
    trade_record = pd.DataFrame(columns=['date','trade_type','coin_amount','usd_amount'])
    for value in trade_dict:
        df_append = pd.DataFrame([trade_dict[str(value)]],columns=['date','trade_type','coin_amount','usd_amount'])
        trade_record = trade_record.append(df_append,ignore_index=True)

    trade_record=trade_record.reindex(columns=['date','trade_type','coin_amount','usd_amount','period(hours)','profit','pro_rate'])
    #计算每笔交易持仓时间，盈亏，盈亏比，记录在平仓那笔交易中
    for i in range(1,trade_record.shape[0]):

        if (trade_record.loc[i,'trade_type'] != trade_record.loc[i-1,'trade_type']) & (abs(float(trade_record.loc[i,'coin_amount']) + float(trade_record.loc[i-1,'coin_amount'])) < 0.1):
            d1 = datetime.strptime(trade_record.iloc[i ,0], '%Y-%m-%d %H:%M:%S')
            d2 = datetime.strptime(trade_record.iloc[i-1, 0], '%Y-%m-%d %H:%M:%S')
            delta=(d1-d2).seconds
            trade_record.iloc[i,4] = delta/3600
            trade_record.iloc[i,5] = float(trade_record.iloc[i,3]+float(trade_record.iloc[i-1,3]))
            trade_record.iloc[i,6] = (float(trade_record.iloc[i,3]+float(trade_record.iloc[i-1,3])))*100/abs(float(trade_record.iloc[i-1,3]))

        else:
            pass
    trade_record['coin_type'] = coin_type

    #将交易数据存入本地
    local_path=''#本地保存地址
    try:
        df = pd.read_csv(local_path)
        df = df.append(trade_record,ignore_index=True)
        df.drop_duplicates(['date','coin_type'], keep='first', inplace=True)
        df.sort_values(by=['date'],ascending=1)
        df.to_csv(local_path)

    except:
        trade_record.to_csv(local_path)


record_my_trades('eos')