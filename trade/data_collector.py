import os
import sys
import time
import pandas as pd
import datetime
from datetime import timedelta
import ccxt  # noqa: E402
from sqlalchemy import create_engine
from trade import Functions as fct



# -----------------------------------------------------------------------------
# common constants

msec = 1000
minute = 60 * msec
hold = 30

# -----------------------------------------------------------------------------

exchange = ccxt.bitfinex({
    'rateLimit': 10000,
    'enableRateLimit': True,
    # 'verbose': True,
})
# -----------------------------------------------------------------------------

# ----local
DB_USER='postgres'
DB_PASS='holt'
DB_HOST='localhost'
DB_PORT='5432'
DATABASE='bfx_data'


connect_info = 'postgresql://{}:{}@{}:{}/{}'.format(DB_USER, DB_PASS, DB_HOST, DB_PORT, DATABASE)
engine = create_engine(connect_info, echo=False)

# 读取schema中所有表名
def rsql_tablename(schemaname='public',engine=engine):
    sql = "select tablename from pg_tables where schemaname='%s'" % schemaname
    table_df = pd.read_sql(sql, con=engine)
    return table_df

# 读取某张表
def rsql_table(table_name,engine=engine):
    sql = 'select * from "%s"'% table_name
    df = pd.read_sql(sql,con=engine)
    return df

# read exists tables
df_exist = rsql_tablename(schemaname='public',engine=engine)

# -----------------------------------------------------------------------------

def get_k_line(start_stamp, end_stamp, symbol, time_interval):
    '''

    :param start_datetime:2019-02-01 00:00:00
    :param symbol: 'ETH/USD'
    :param time_interval:1h
    :return:
    '''

    # from_datetime = start_stamp
    # from_timestamp = exchange.parse8601(from_datetime)
    # print(from_timestamp)
    # now = exchange.milliseconds()
    df = pd.DataFrame()
    while start_stamp < end_stamp:

        try:
            print(time_interval)

            print(exchange.milliseconds(), 'Fetching %s candles starting from' % symbol, exchange.iso8601(start_stamp))
            ohlcvs = exchange.fetch_ohlcv(symbol, time_interval, start_stamp)
            if time_interval.endswith('m'):
                start_stamp += len(ohlcvs) * minute * int(time_interval.strip('m'))
            if time_interval.endswith('h'):
                start_stamp += len(ohlcvs) * minute * int(time_interval.strip('h')) * 60

            ohlcvs = pd.DataFrame(ohlcvs)
            if len(ohlcvs) > 0:
                ohlcvs.columns = ['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']
                ohlcvs['candle_begin_time'] = pd.to_datetime(ohlcvs['candle_begin_time'], unit='ms')
                df = df.append(ohlcvs)
                time.sleep(5)
            else:
                start_stamp += 20 * minute * int(time_interval.strip('h')) * 60
                continue

        except (ccxt.ExchangeError, ccxt.AuthenticationError, ccxt.ExchangeNotAvailable, ccxt.RequestTimeout) as error:

            print('Got an error', type(error).__name__, error.args, ', retrying in', hold, 'seconds...')
            time.sleep(hold)

    # df['candle_begin_time_GTM8'] = df['candle_begin_time'] + timedelta(hours=8)

    return df


if __name__ == '__main__':
    fail_symbol = []
    while True:
        print(fail_symbol)
        try:
            fct.my_timer('08:10')
            # read all symbol from bfx
            valid_list = fct.get_valid_symbol()
            for sy in valid_list:
                symbol = sy + '/' + 'USDT'
                try:
                    tick = exchange.fetch_ticker(symbol)
                except ccxt.errors.ExchangeError:
                    fail_symbol.append(symbol)
                    print(symbol, 'not exist')
                    continue
                # symbol = symbol.upper()[:3] + '/' + symbol.upper()[3:]
                now_datetime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
                start_datetime0 = now_datetime[:10]
                start_datetime = pd.to_datetime('%s 00:00:00' % start_datetime0) -timedelta(days=1) + timedelta(hours=8)
                end_datetime = start_datetime + timedelta(days=1)

                while end_datetime <= pd.to_datetime(now_datetime) + timedelta(hours=8):
                    start_timestamp = int(time.mktime(start_datetime.timetuple()) * 1000)
                    end_timestamp = int(time.mktime(end_datetime.timetuple()) * 1000)

                    df = get_k_line(start_timestamp, end_timestamp, symbol, '1h')
                    if len(df) > 1:
                        df = df.drop_duplicates('candle_begin_time')
                        df = df.sort_values(by='candle_begin_time')
                        df.reset_index(inplace=True, drop=True)
                        sql_df = df[df['candle_begin_time'] < end_datetime - timedelta(hours=8)].copy()
                        print(sql_df)
                        table = 'bfx_data_%s_%s' % (symbol, start_datetime.strftime('%Y-%m-%d'))
                        sql_df.to_sql(table, engine, index=False, if_exists='replace')

                    print(start_datetime.strftime('%Y-%m-%d'), '============== DONE')
                    start_datetime += timedelta(days=1)
                    end_datetime += timedelta(days=1)
        except Exception as e :
            print(e)
            time.sleep(30)