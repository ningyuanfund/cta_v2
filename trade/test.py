import os
import sys
import time
import pandas as pd
import datetime
from datetime import timedelta
import ccxt  # noqa: E402
from sqlalchemy import create_engine
from trade import Functions as fct



# -----------------------------------------------------------------------------
# common constants

msec = 1000
minute = 60 * msec
hold = 30

# -----------------------------------------------------------------------------

exchange = ccxt.bitfinex({
    'rateLimit': 10000,
    'enableRateLimit': True,
    # 'verbose': True,
})
# -----------------------------------------------------------------------------

# ----local
DB_USER='postgres'
DB_PASS='holt'
DB_HOST='localhost'
DB_PORT='5432'
DATABASE='bfx_data1T'


connect_info = 'postgresql://{}:{}@{}:{}/{}'.format(DB_USER, DB_PASS, DB_HOST, DB_PORT, DATABASE)
engine = create_engine(connect_info, echo=False)

# 读取schema中所有表名
def rsql_tablename(schemaname='public',engine=engine):
    sql = "select tablename from pg_tables where schemaname='%s'" % schemaname
    table_df = pd.read_sql(sql, con=engine)
    return table_df

# 读取某张表
def rsql_table(table_name,engine=engine):
    sql = 'select * from "%s"'% table_name
    df = pd.read_sql(sql,con=engine)
    return df

#  exists tables
def if_exists_in_db(tablename):
    df_exist = rsql_tablename(schemaname='public',engine=engine)
    exist_list = df_exist['tablename'].values.tolist()
    if tablename in exist_list:
        return True
    else:
        return False

# -----------------------------------------------------------------------------

# get valid symbol and transfer it to what real works
def get_valid_symbol():
    symbol_list = ccxt.bitfinex().public_get_symbols()
    valid_list = [fct.symbol_transfer_get_K(symbol.lower().replace('usd', '')).upper() for symbol in symbol_list if
                  symbol.endswith('usd')]
    return valid_list


def get_k_line(start_stamp, end_stamp, symbol, time_interval):
    '''

    :param start_datetime:2019-02-01 00:00:00
    :param symbol: 'ETH/USD'
    :param time_interval:1h
    :return:
    '''

    # from_datetime = start_stamp
    # from_timestamp = exchange.parse8601(from_datetime)
    # print(from_timestamp)
    # now = exchange.milliseconds()
    df = pd.DataFrame()
    while start_stamp < end_stamp:

        try:
            print(time_interval)

            print(exchange.milliseconds(), 'Fetching %s candles starting from' % symbol, exchange.iso8601(start_stamp))
            ohlcvs = exchange.fetch_ohlcv(symbol, time_interval, start_stamp)
            if time_interval.endswith('m'):
                start_stamp += len(ohlcvs) * minute * int(time_interval.strip('m'))
            if time_interval.endswith('h'):
                start_stamp += len(ohlcvs) * minute * int(time_interval.strip('h')) * 60

            ohlcvs = pd.DataFrame(ohlcvs)
            if len(ohlcvs) > 0:
                ohlcvs.columns = ['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']
                ohlcvs['candle_begin_time'] = pd.to_datetime(ohlcvs['candle_begin_time'], unit='ms')
                df = df.append(ohlcvs)
                time.sleep(5)
            else:
                start_stamp += 20 * minute * int(time_interval.strip('h')) * 60
                continue

        except (ccxt.ExchangeError, ccxt.AuthenticationError, ccxt.ExchangeNotAvailable, ccxt.RequestTimeout) as error:

            print('Got an error', type(error).__name__, error.args, ', retrying in', hold, 'seconds...')
            time.sleep(hold)

    # df['candle_begin_time_GTM8'] = df['candle_begin_time'] + timedelta(hours=8)

    return df


# 当每天设置时间到来的时候，停止睡眠
def my_timer(start_time):
    timer_hour = int(start_time.split(':')[0])
    timer_minutes = int(start_time.split(':')[1])

    while True:
        hour = datetime.datetime.now().hour
        minute = datetime.datetime.now().minute
        condition = (timer_hour == hour) and (timer_minutes == minute)
        if condition:
            print('active!')
            break
        else:
            print('timer set:', start_time, 'now time: %s:%s' % (hour, minute))
            time.sleep(30)

if __name__ == '__main__':
    fail_symbol = []
    while True:
        try:
            # read all symbol from bfx
            valid_list = ['BTC','ETH','LTC','EOS','BCH','ETC','XRP']
            for sy in valid_list:
                symbol = sy + '/' + 'USDT'
                try:
                    tick = exchange.fetch_ticker(symbol)
                except ccxt.errors.ExchangeError:
                    fail_symbol.append(symbol)
                    print(symbol, 'not exist')
                    continue
                # symbol = symbol.upper()[:3] + '/' + symbol.upper()[3:]
                now_datetime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
                start_datetime = pd.to_datetime(now_datetime) -timedelta(days=3)
                end_datetime = start_datetime+timedelta(days=1)

                while end_datetime <= pd.to_datetime(now_datetime) + timedelta(hours=8):
                    fct.my_pause('8', '13')
                    start_timestamp = int(time.mktime(start_datetime.timetuple()) * 1000)
                    end_timestamp = int(time.mktime(end_datetime.timetuple()) * 1000)
                    table = 'bfx_data_%s_%s' % (symbol, start_datetime.strftime('%Y-%m-%d'))
                    print(table)
                    if not if_exists_in_db(table):
                        print('reading %s' % table)
                        df = get_k_line(start_timestamp, end_timestamp, symbol, '1m')
                        if len(df) > 1:
                            df = df.drop_duplicates('candle_begin_time')
                            df = df.sort_values(by='candle_begin_time')
                            df.reset_index(inplace=True, drop=True)
                            sql_df = df[df['candle_begin_time'] < end_datetime - timedelta(hours=8)].copy()
                            print(sql_df)

                            sql_df.to_sql(table, engine, index=False, if_exists='replace')

                    print(start_datetime.strftime('%Y-%m-%d'), '============== DONE')
                    start_datetime += timedelta(days=1)
                    end_datetime += timedelta(days=1)

            time.sleep(60*60*12)
        except Exception as e :
            print(e)
            time.sleep(30)