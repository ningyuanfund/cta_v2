import pandas as pd
import numpy as np
import time
from datetime import timedelta
from numba import jit, cuda


pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)


# 带止损的布林线策略
@cuda.jit('void()')
def signal_bolling_with_stop_lose():

    for i in range(len(np_all_data)):
        print(np_all_data[i][0])


if __name__ == '__main__':

    # 导入总数据
    all_data = pd.read_hdf(r'E:\cta_v2\data\test_v2\input\BTCUSD.h5', key='1H')  # 数据文件存放路径

    # 选取时间段
    all_data = all_data[all_data['candle_begin_time'] >= pd.to_datetime('2018-01-01')].copy()
    all_data.reset_index(inplace=True, drop=True)
    all_data['candle_begin_time'] = all_data['candle_begin_time'].apply(lambda x: time.mktime(x.timetuple()))

    np_all_data = all_data.to_numpy()

    signal_bolling_with_stop_lose()

    """
    [0] candle begin time
    [1] open
    [2] high
    [3] low
    [4] close
    [5] volume
    """