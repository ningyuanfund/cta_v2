import pandas as pd
import warnings
from ckt.cq_sdk.path import get_data_path
from test_v2.test_lab.data_with_noise import *
import ckt.test.strategy.timing_strategy.Signals as sigs
from ckt.test.strategy.timing_strategy.equity_curve import equity_curve_with_long_and_short, find_change_time

warnings.simplefilter(action='ignore')
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)


# ===主程序
def main(pair, strategy_name, para, sys_para, freq):

    begin_date = sys_para['begin_date']  # 整体回测开始时间（使用的数据的最早时间）
    end_date = sys_para['end_date']

    # 导入总数据
    all_data = gen_pure_data(pair.replace('/', ''), freq=freq)

    # all_data = pd.read_hdf(get_data_path('test_v2', 'input', '%s.h5' % pair), key=freq)  # 数据文件存放路径

    all_data = all_data[all_data['candle_begin_time'] < pd.to_datetime(end_date)].copy()

    train_df = all_data.sort_values(by='candle_begin_time')
    train_df.reset_index(inplace=True, drop=True)

    print('本轮回测开始时间为', begin_date)
    print('本轮回测结束时间为', end_date)

    function_str = 'sigs.signal_%s_with_stop_lose(train_df.copy(), para)' % strategy_name
    df = eval(function_str)
    df.to_csv(r'C:\Users\sycam\Desktop\df.csv')

    df = df[df['candle_begin_time'] >= pd.to_datetime(begin_date)].copy()
    df.reset_index(inplace=True, drop=True)

    equity_curve = equity_curve_with_long_and_short(df, leverage_rate=3, c_rate=3.0 / 1000)

    return equity_curve


if __name__ == '__main__':

    # 策略名称
    strategy_name = 'bolling'
    freq = '15T'

    # 回测系统参数
    sys_para = {
                'begin_date': '2019-04-23',
                'end_date': '2019-05-07',
                }

    # 生成参数列表
    pair = 'BTCUSD'
    para = [40, 5, 1]
    top_para_dict = {}

    eq = main(pair, strategy_name, para, sys_para, freq)
    print(eq)
