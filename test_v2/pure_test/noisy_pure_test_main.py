import pandas as pd
import warnings
from matplotlib import pyplot as plt
from test_v2.obj_ornt.EquityCurve import *
from ckt.cq_sdk.path import get_data_path
from test_v2.test_lab.data_with_noise import *
import ckt.test.strategy.timing_strategy.Signals as sigs
from ckt.test.strategy.timing_strategy.equity_curve import equity_curve_with_long_and_short, find_change_time

warnings.simplefilter(action='ignore')
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)


# 计算纯净数据结果
def get_pure_res(all_data, begin_date, strategy_name, para):

    train_df = all_data.sort_values(by='candle_begin_time')
    train_df.reset_index(inplace=True, drop=True)

    function_str = 'sigs.signal_%s_with_stop_lose(train_df.copy(), para)' % strategy_name
    df = eval(function_str)

    df = df[df['candle_begin_time'] >= pd.to_datetime(begin_date)].copy()
    df.reset_index(inplace=True, drop=True)

    equity_curve = equity_curve_with_long_and_short(df, leverage_rate=3, c_rate=3.0 / 1000)

    eq = equity_curve[['candle_begin_time', 'equity_curve', 'close']].copy()
    eq.rename(columns={'candle_begin_time': 'date', 'equity_curve': 'equity', 'close': 'benchmark'}, inplace=True)
    equity_o = EquityCurve(eq, rf=0.03)

    rtn = eq['equity'].iloc[-1]
    annual_rtn = round(equity_o.target_annual_rtn(), 4)
    excess = round(equity_o.abnormal_rtn(), 4)
    calmar = round(equity_o.calmar_ratio(), 4)
    maxdd = round(equity_o.max_dd(), 4)
    ann_vol = round(equity_o.annual_volatility(), 4)

    return {'rtn': rtn, 'annual_rtn': annual_rtn, 'excess': excess, 'calmar': calmar, 'max_dd': maxdd, 'annual_vol': ann_vol}


# ===主程序
def main(pair, strategy_name, para, sys_para, freq, noisy_test_times):

    begin_date = sys_para['begin_date']  # 整体回测开始时间（使用的数据的最早时间）
    end_date = sys_para['end_date']

    # 导入总数据
    pure_data = gen_pure_data(pair.replace('/', ''), freq=freq)
    pure_data = pure_data[pure_data['candle_begin_time'] < pd.to_datetime(end_date)].copy()

    print('本轮回测开始时间为', begin_date)
    print('本轮回测结束时间为', end_date)

    pure_res = get_pure_res(pure_data, begin_date, strategy_name, para)

    dist_dict = {'rtn': [], 'annual_rtn': [], 'excess': [], 'calmar': [], 'max_dd': [], 'annual_vol': []}
    for i in range(noisy_test_times):
        # print('data generating')
        all_data = gen_noisy_data(pure_data)
        # print('data generated')
        train_df = all_data.sort_values(by='candle_begin_time')
        train_df.reset_index(inplace=True, drop=True)

        # print('signal generating')
        function_str = 'sigs.signal_%s_with_stop_lose(train_df.copy(), para)' % strategy_name
        df = eval(function_str)
        # print('signal generated')

        df = df[df['candle_begin_time'] >= pd.to_datetime(begin_date)].copy()
        df.reset_index(inplace=True, drop=True)
        # print('equity curve generating')
        equity_curve = equity_curve_with_long_and_short(df, leverage_rate=3, c_rate=3.0 / 1000)
        # print('equity curve generated')

        eq = equity_curve[['candle_begin_time', 'equity_curve', 'close']].copy()
        eq.rename(columns={'candle_begin_time': 'date', 'equity_curve': 'equity', 'close': 'benchmark'}, inplace=True)
        equity_o = EquityCurve(eq, rf=0.03)

        # print('index generating')
        print('=' * 10, 'TEST TIME %s' % (i + 1), '=' * 10)

        rtn = eq['equity'].iloc[-1]
        print('rtn', rtn)
        dist_dict['rtn'].append(rtn)

        annual_rtn = round(equity_o.target_annual_rtn(), 4)
        print('Annual rtn', annual_rtn)
        dist_dict['annual_rtn'].append(annual_rtn)

        excess = round(equity_o.abnormal_rtn(), 4)
        print('Excess rtn', excess)
        dist_dict['excess'].append(excess)

        calmar = round(equity_o.calmar_ratio(), 4)
        print('Calmar ratio', calmar)
        dist_dict['calmar'].append(calmar)

        maxdd = round(equity_o.max_dd(), 4)
        print('Max_dd', maxdd)
        dist_dict['max_dd'].append(maxdd)

        annual_vol = round(equity_o.annual_volatility(), 4)
        print('Annual volatility', annual_vol)
        dist_dict['annual_vol'].append(annual_vol)
        # print('index generated')
    return dist_dict, pure_res


def task_process(task_list):
    for task in task_list:

        index_dist, pure_res = main(task['pair'], task['strategy_name'], task['para'], task['sys_para'], task['freq'],
                                    task['noisy_test'])

        index_df = pd.DataFrame(index_dist)

        plt.figure(figsize=[12, 8])

        print(pure_res)

        index_list = ['rtn', 'annual_rtn', 'excess', 'calmar', 'max_dd', 'annual_vol']
        for i in range(len(index_list)):
            plt.subplot(2, 3, i + 1)
            plt.hist(index_df[index_list[i]], bins=50, color='blue')
            plt.axvline(pure_res[index_list[i]], color='red')

            plt.title('DIST_%s' % index_list[i])
        title = '%s_%s_%s' % (task['pair'], task['freq'], str(task['para']))
        plt.suptitle(title)
        plt.savefig(r'E:\cta_v2\data\test_v2\output\fig\%s.png' % title)
        plt.close()

        print(index_df)


if __name__ == '__main__':

    task_list = [
        {
            'pair': 'BTCUSD',
            'strategy_name': 'bolling',
            'sys_para': {
                'begin_date': '2019-07-05',
                'end_date': '2019-07-19',
                },
            'freq': '15T',
            'para': [120, 2.5, 4],
            'noisy_test': 500
        },
        {
            'pair': 'EOSUSD',
            'strategy_name': 'bolling',
            'sys_para': {
                'begin_date': '2019-07-05',
                'end_date': '2019-07-19',
            },
            'freq': '30T',
            'para': [160, 4.5, 3],
            'noisy_test': 500
        },
        {
            'pair': 'ETHUSD',
            'strategy_name': 'bolling',
            'sys_para': {
                'begin_date': '2019-07-05',
                'end_date': '2019-07-19',
            },
            'freq': '15T',
            'para': [180, 2.5, 3],
            'noisy_test': 500
        },
        {
            'pair': 'LTCUSD',
            'strategy_name': 'bolling',
            'sys_para': {
                'begin_date': '2019-07-05',
                'end_date': '2019-07-19',
            },
            'freq': '30T',
            'para': [60, 2.0, 4],
            'noisy_test': 500
        },
    ]

    task_process(task_list)


