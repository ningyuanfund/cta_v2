import pandas as pd
import numpy as np
from datetime import timedelta
from matplotlib import pyplot as plt
pd.set_option('expand_frame_repr', False)


# 根据布林线间距加上可变的杠杆，此处逻辑有待商榷
def bolling_add_flexible_leverage(df):

    # 查看有交易信号的地方
    condition1 = df['signal'] == 1
    condition2 = df['signal'] == -1
    df['std/close'] = df['std'] / df['close']
    df['std/close'].fillna(value=0, inplace=True)

    # 杠杆倍数从0-3，均匀分为10级。
    df.loc[condition1 | condition2, 'lever'] = 3 - df['std/close'].apply(
        lambda x: int(x / 0.006) if x < 0.06 else 9.6) * 0.3
    df.loc[df['signal'] == 0, 'lever'] = 0

    # fill杠杆倍数方便计算资金曲线
    df['leverage_rate'] = df['lever'].shift(1)
    df['leverage_rate'].fillna(method='ffill', inplace=True)
    df['leverage_rate'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    return df


# ===根据成交量加上可变的杠杆，此处逻辑有待商榷
def add_flexible_leverage_volume(df):

    condition1 = df['signal'] == 1
    condition2 = df['signal'] == -1

    df.loc[condition1 | condition2, 'volume_ratio'] = \
        df['volume%s' % vol].rolling(short_period).mean() / df['volume%s' % vol].rolling(long_period).mean()

    df['lever'] = df['volume_ratio']
    df.loc[df['signal'] == 0, 'lever'] = 0

    # fill杠杆倍数方便计算资金曲线
    df['leverage_rate'] = df['lever'].shift(1)
    df['leverage_rate'].fillna(method='ffill', inplace=True)
    df['leverage_rate'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0
    df['leverage_rate'] = df['leverage_rate'].apply(lambda x: round(x, 2) if x < 3 else 3)

    return df


# ===加上固定杠杆
def add_fix_leverage(df):
    df['leverage_rate'] = 1.5

    return df


# ===带止损的布林线策略-可变杠杆
def signal_bolling_with_stop_lose_and_flexible_leverage(df, para=[100, 2, 5]):
    """
    布林线中轨：n天收盘价的移动平均线
    布林线上轨：n天收盘价的移动平均线 + m * n天收盘价的标准差
    布林线上轨：n天收盘价的移动平均线 - m * n天收盘价的标准差
    当收盘价由下向上穿过上轨的时候，做多；然后由上向下穿过下轨的时候，平仓。
    当收盘价由上向下穿过下轨的时候，做空；然后由下向上穿过上轨的时候，平仓。

    另外，当价格往亏损方向超过百分之stop_lose的时候，平仓止损。
    :param df:  原始数据
    :param para:  参数，[n, m, stop_lose]
    :return:
    """

    # ===计算指标
    n = int(para[0])
    m = para[1]
    stop_loss_pct = para[2]

    # 计算均线
    df['median'] = df['close'].rolling(n, min_periods=1).mean()

    # 计算上轨、下轨道
    df['std'] = df['close'].rolling(n, min_periods=1).std(ddof=1)  # ddof代表标准差自由度
    df['upper'] = df['median'] + m * df['std']
    df['lower'] = df['median'] - m * df['std']

    # ===找出做多信号
    condition1 = df['close'] > df['upper']  # 当前K线的收盘价 > 上轨
    condition2 = df['close'].shift(1) <= df['upper'].shift(1)  # 之前K线的收盘价 <= 上轨
    df.loc[condition1 & condition2, 'signal_long'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做多平仓信号
    condition1 = df['close'] < df['median']  # 当前K线的收盘价 < 中轨
    condition2 = df['close'].shift(1) >= df['median'].shift(1)  # 之前K线的收盘价 >= 中轨
    df.loc[condition1 & condition2, 'signal_long'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===找出做空信号
    condition1 = df['close'] < df['lower']  # 当前K线的收盘价 < 下轨
    condition2 = df['close'].shift(1) >= df['lower'].shift(1)  # 之前K线的收盘价 >= 下轨
    df.loc[condition1 & condition2, 'signal_short'] = -1  # 将产生做空信号的那根K线的signal设置为-1，-1代表做空

    # ===找出做空平仓信号
    condition1 = df['close'] > df['median']  # 当前K线的收盘价 > 中轨
    condition2 = df['close'].shift(1) <= df['median'].shift(1)  # 之前K线的收盘价 <= 中轨
    df.loc[condition1 & condition2, 'signal_short'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    df['signal'] = np.nan

    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_lose_price': None}  # 用于记录之前交易信号，以及止损价格

    # 逐行遍历df，考察每一行的交易信号
    for i in range(df.shape[0]):
        # 如果之前是空仓
        if info_dict['pre_signal'] == 0:
            # 当本周期有做多信号
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录当前状态
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。也可以用下周期的开盘价df.at[i+1, 'open']，但是此时需要注意i等于最后一个i时，取i+1会报错
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 当本周期有做空信号
            elif df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 无信号
            else:
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

        # 如果之前是多头仓位
        elif info_dict['pre_signal'] == 1:
            # 当本周期有平多仓信号，或者需要止损
            if (df.at[i, 'signal_long'] == 0) or (df.at[i, 'close'] < info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平多仓并且还要开空仓
            if df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 如果之前是空头仓位
        elif info_dict['pre_signal'] == -1:
            # 当本周期有平空仓信号，或者需要止损
            if (df.at[i, 'signal_short'] == 0) or (df.at[i, 'close'] > info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 当本周期有平空仓并且还要开多仓
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录相关信息
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (
                1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 其他情况
        else:
            raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错')

    if flexible:
        # 加上可变的杠杆
        df = add_flexible_leverage_volume(df)
    else:
        df = add_fix_leverage(df)

    # ------------------
    # 在此处用时间筛选数据。
    df = df[df['candle_begin_time'] > pd.to_datetime(start_time)]
    df.reset_index(inplace=True, drop=True)
    # ------------------

    # ===由signal计算出实际的每天持有仓位
    # signal的计算运用了收盘价，是每根K线收盘之后产生的信号，到第二根开盘的时候才买入，仓位才会改变。
    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    return df


# 海龟交易法则
def signal_turtle_with_stop_lose_and_flexible_leverage(df, para=[20, 10, 5]):
    """
    海龟交易法则的方式开仓和平仓，但是加一个止损，就是开仓之后，相比于开始的价格下跌一定比例，止损。
    :return:

    改进思路，短期跌幅过大，涨幅过大就不能买入了
    """

    n1 = int(para[0])
    n2 = int(para[1])
    stop_loss_pct = float(para[2])

    df['open_close_high'] = df[['open', 'close']].max(axis=1)
    df['open_close_low'] = df[['open', 'close']].min(axis=1)
    # 最近n1日的最高价、最低价
    df['n1_high'] = df['open_close_high'].rolling(n1).max()
    df['n1_low'] = df['open_close_low'].rolling(n1).min()
    # 最近n2日的最高价、最低价
    df['n2_high'] = df['open_close_high'].rolling(n2).max()
    df['n2_low'] = df['open_close_low'].rolling(n2).min()

    # ===找出做多信号
    # 当天的收盘价 > n1日的最高价，做多
    condition = (df['close'] > df['n1_high'].shift(1))
    # 将买入信号当天的signal设置为1
    df.loc[condition, 'signal_long'] = 1
    # ===找出做多平仓
    # 当天的收盘价 < n2日的最低价，多单平仓
    condition = (df['close'] < df['n2_low'].shift(1))
    # 将卖出信号当天的signal设置为0
    df.loc[condition, 'signal_long'] = 0

    # ===找出做空信号
    # 当天的收盘价 < n1日的最低价，做空
    condition = (df['close'] < df['n1_low'].shift(1))
    df.loc[condition, 'signal_short'] = -1
    # ===找出做空平仓
    # 当天的收盘价 > n2日的最高价，做空平仓
    condition = (df['close'] > df['n2_high'].shift(1))
    # 将卖出信号当天的signal设置为0
    df.loc[condition, 'signal_short'] = 0

    info_dict = {
        'pre_signal': 0,
        'stop_lose_price': None,
    }

    df['signal'] = np.nan

    for i in range(df.shape[0]):
        # 如果目前是空仓
        if info_dict['pre_signal'] == 0:
            # 有做多信号
            if (df.at[i, 'signal_long'] == 1):
                df.at[i, 'signal'] = 1  # 真实信号开仓
                # 记录相关信息
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 - stop_loss_pct / 100)  # 止损价格
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            elif (df.at[i, 'signal_short'] == -1):
                df.at[i, 'signal'] = -1  # 真实信号开仓
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 + stop_loss_pct / 100)  # 止损价格
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}
            # 无信号
            else:
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

        # 如果目前是多头仓位
        elif info_dict['pre_signal'] == 1:
            # 有平仓信号，或者需要止损
            if (df.at[i, 'signal_long'] == 0) or (df.at[i, 'close'] < info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 真实信号平仓
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 平仓并且还开空仓
            if df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 真实信号开仓
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 + stop_loss_pct / 100)  # 止损价格
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 如果目前是空头仓位
        elif info_dict['pre_signal'] == -1:
            # 有平仓信号，或者需要止损
            if (df.at[i, 'signal_short'] == 0) or (df.at[i, 'close'] > info_dict['stop_lose_price']):
                df.at[i, 'signal'] = 0  # 真实信号平仓
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_lose_price': None}

            # 平仓并且开多仓
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 真实信号开仓
                # 记录相关信息
                pre_signal = 1  # 信号
                stop_lose_price = df.at[i, 'close'] * (1 - stop_loss_pct / 100)  # 止损价格
                info_dict = {'pre_signal': pre_signal, 'stop_lose_price': stop_lose_price}

        # 其他情况
        else:
            print('不可能出现其他情况，报错')
            exit()

    if flexible:
        # 加上可变的杠杆
        df = add_flexible_leverage_volume(df)
    else:
        df = add_fix_leverage(df)

    # ------------------
    # 在此处用时间筛选数据。
    df = df[df['candle_begin_time'] > pd.to_datetime(start_time)]
    df.reset_index(inplace=True, drop=True)
    # ------------------

    df['pos'] = df['signal'].shift()
    df['pos'].fillna(method='ffill', inplace=True)
    df['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0

    # print(df[df['candle_begin_time'] > pd.to_datetime('2018-03-21 13:30:00')])
    # exit()
    return df


# ===画可变杠杆的资金曲线
def equity_curve_with_flexible_leverage(df, c_rate=3.0/1000, min_margin_rate=0.15):
    """

    :param df:  带有signal和pos的原始数据
    :param leverage_rate:  bfx交易所最多提供3倍杠杆，leverage_rate可以在(0, 3]区间选择
    :param c_rate:  手续费
    :param min_margin_rate:  低保证金比例，必须占到借来资产的15%
    :return:
    """
    # =====基本参数
    init_cash = 100  # 初始资金

    # =====根据pos计算资金曲线
    # ===计算涨跌幅
    df['change'] = df['close'].pct_change(1)  # 根据收盘价计算涨跌幅
    df['buy_at_open_change'] = df['close'] / df['open'] - 1  # 从今天开盘买入，到今天收盘的涨跌幅
    df['sell_next_open_change'] = df['open'].shift(-1) / df['close'] - 1  # 从今天收盘到明天开盘的涨跌幅
    df.at[len(df) - 1, 'sell_next_open_change'] = 0

    # ===选取开仓、平仓条件
    condition1 = df['pos'] != 0
    condition2 = df['pos'] != df['pos'].shift(1)
    open_pos_condition = condition1 & condition2

    condition1 = df['pos'] != 0
    condition2 = df['pos'] != df['pos'].shift(-1)
    close_pos_condition = condition1 & condition2

    # ===对每次交易进行分组
    df.loc[open_pos_condition, 'start_time'] = df['candle_begin_time']
    df['start_time'].fillna(method='ffill', inplace=True)
    df.loc[df['pos'] == 0, 'start_time'] = pd.NaT

    # ===计算仓位变动
    # 开仓时仓位
    df.loc[open_pos_condition, 'position'] = init_cash * df['leverage_rate'] * (1 + df['buy_at_open_change'])  # 建仓后的仓位

    # 开仓后每天的仓位的变动
    group_num = len(df.groupby('start_time'))

    if group_num > 1:
        t = df.groupby('start_time').apply(lambda x: x['close'] / x.iloc[0]['close'] * x.iloc[0]['position'])
        t = t.reset_index(level=[0])
        df['position'] = t['close']
    elif group_num == 1:
        t = df.groupby('start_time')[['close', 'position']].apply(lambda x: x['close'] / x.iloc[0]['close'] * x.iloc[0]['position'])
        df['position'] = t.T.iloc[:, 0]

    # 每根K线仓位的最大值和最小值，针对最高价和最低价
    df['position_max'] = df['position'] * df['high'] / df['close']
    df['position_min'] = df['position'] * df['low'] / df['close']

    # 平仓时仓位
    df.loc[close_pos_condition, 'position'] *= (1 + df.loc[close_pos_condition, 'sell_next_open_change'])

    # ===计算每天实际持有资金的变化
    # 计算持仓利润
    df['profit'] = (df['position'] - init_cash * df['leverage_rate']) * df['pos']  # 持仓盈利或者损失

    # 计算持仓利润最小值
    df.loc[df['pos'] == 1, 'profit_min'] = (df['position_min'] - init_cash * df['leverage_rate']) * df['pos']  # 最小持仓盈利或者损失
    df.loc[df['pos'] == -1, 'profit_min'] = (df['position_max'] - init_cash * df['leverage_rate']) * df['pos']  # 最小持仓盈利或者损失

    # 计算实际资金量
    df['cash'] = init_cash + df['profit']  # 实际资金
    df['cash'] -= init_cash * df['leverage_rate'] * c_rate  # 减去建仓时的手续费
    df['cash_min'] = df['cash'] - (df['profit'] - df['profit_min'])  # 实际最小资金
    df.loc[df['cash_min'] < 0, 'cash_min'] = 0  # 如果有小于0，直接设置为0
    df.loc[close_pos_condition, 'cash'] -= df.loc[close_pos_condition, 'position'] * c_rate  # 减去平仓时的手续费

    # 计算最低保证金
    df['min_margin'] = (init_cash * df['leverage_rate']) / 6
    # ===判断是否会爆仓
    _index = df[df['cash_min'] <= df['min_margin']].index

    if len(_index) > 0:
        print('有爆仓')
        df.loc[_index, '强平'] = 1
        df['强平'] = df.groupby('start_time')['强平'].fillna(method='ffill')
        df.loc[(df['强平'] == 1) & (df['强平'].shift(1) != 1), 'cash_强平'] = df['cash_min']  # 此处是有问题的
        df.loc[(df['pos'] != 0) & (df['强平'] == 1), 'cash'] = None
        df['cash'].fillna(value=df['cash_强平'], inplace=True)
        df['cash'] = df.groupby('start_time')['cash'].fillna(method='ffill')
        df.drop(['强平', 'cash_强平'], axis=1, inplace=True)  # 删除不必要的数据

    # ===计算资金曲线
    df['equity_change'] = df['cash'].pct_change()
    df.loc[open_pos_condition, 'equity_change'] = df.loc[open_pos_condition, 'cash'] / init_cash - 1  # 开仓日的收益率
    df['equity_change'].fillna(value=0, inplace=True)
    df['equity_curve'] = (1 + df['equity_change']).cumprod()

    # ===判断资金曲线是否有负值，有的话后面都置成0
    if len(df[df['equity_curve'] < 0]) > 0:
        neg_start = df[df['equity_curve'] < 0].index[0]
        df.loc[neg_start:, 'equity_curve'] = 0

    return df


time_interval = '1D'
data = pd.read_hdf(r'E:\cta_v2\data\input\ETHUSD.h5', key=time_interval)
volume_data = pd.read_hdf(r'E:\cta_v2\data\input\BTCUSD.h5', key=time_interval)
volume_data = volume_data[['candle_begin_time', 'volume']]

data = data.merge(volume_data, on='candle_begin_time', how='inner', suffixes=['', 'BTC'])

# short period 和 long_period 的长度，以k线根数计
short_period = int(timedelta(days=1) / timedelta(days=1))
long_period = int(timedelta(days=7) / timedelta(days=1))
start_time = '2018-01-01'


flexible = False  # 是否要可变杠杆
vol = ''
para = [20, 10, 5]

signal_df = signal_bolling_with_stop_lose_and_flexible_leverage(data, para)
equity_df = equity_curve_with_flexible_leverage(signal_df)

# equity_df.to_csv(r'C:\Users\HP\Desktop\test.csv')
plt.plot(equity_df['equity_curve'])
plt.title('start %s || %s %s || flexible %s || using %s' % (start_time, time_interval, str(para), str(flexible), vol))
plt.show()
