from datetime import timedelta

import pandas as pd

pd.set_option('expand_frame_repr', False)

open_hour = 5

class MetaFactor:
    def __init__(self, raw_data):
        self.data = raw_data
        self.data['candle_begin_time'] = pd.to_datetime(self.data['candle_begin_time'])
        self.daily_data = self._cal_daily_data()
        self.res = []

    def _cal_daily_data(self):
        daily_data = self.data.resample(rule='1D', on='candle_begin_time').agg(
            {
                'open': 'first',
                'high': 'max',
                'low': 'min',
                'close': 'last',
                'volume': 'sum'
            }
        )
        daily_data.reset_index(inplace=True)
        return daily_data

    def _cal(self, x):
        return NotImplementedError

    def cal_factor(self, begin_time, end_time):
        cond1 = self.data['candle_begin_time'] >= pd.to_datetime(begin_time)
        cond2 = self.data['candle_begin_time'] <= pd.to_datetime(end_time)
        data = self.data.loc[cond1 & cond2].copy()
        data.reset_index(drop=True, inplace=True)
        data['date'] = data['candle_begin_time'].dt.date

        data.groupby('date').filter(self._cal)
        return pd.DataFrame(self.res)

# 非交易时间涨幅
class NonTrdTmRtn(MetaFactor):
    def _cal(self, x):
        x['hr'] = x['candle_begin_time'].dt.hour
        factor_df = x.loc[x['hr'] <= open_hour].copy()
        factor = factor_df['close'].iloc[-1] / factor_df['open'].iloc[0] - 1
        self.res.append({'date': x['date'].iloc[0], 'NonTrdTmRtn': factor})

# 非交易时间成交额比例
class NonTrdTmVolumePct(MetaFactor):
    def _cal(self, x):
        date = x['date'].iloc[0]
        last_day_volume = self.daily_data.loc[
            self.daily_data['candle_begin_time'] == (date - timedelta(days=1)), 'volume'
        ].values[0]

        x['hr'] = x['candle_begin_time'].dt.hour
        factor_df = x.loc[x['hr'] <= open_hour].copy()
        factor = factor_df['volume'].sum() / last_day_volume
        self.res.append({'date': date, 'NonTrdTmVolumePct': factor})

# 非交易时间涨跌幅是否超出某个阈值（绝对值）
class NonTrdTmBreak(MetaFactor):
    def _cal(self, x):
        x['hr'] = x['candle_begin_time'].dt.hour
        factor_df = x.loc[x['hr'] <= open_hour].copy()
        factor = factor_df['close'].iloc[-1] / factor_df['open'].iloc[0] - 1
        self.res.append({'date': x['date'].iloc[0], 'NonTrdTmBreak': 1 if abs(factor) > 0.05 else 0})

# 非交易时间是否有跳跃
class NonTrdTmJump(MetaFactor):
    def __init__(self, raw_data):
        MetaFactor.__init__(self, raw_data=raw_data)
        window = 12 * 24
        self.data = raw_data
        self.data['rtn'] = self.data['close'] / self.data['open'] - 1
        self.data['std'] = self.data['rtn'].rolling(window).std(ddof=1)

    def _cal(self, x):
        jump_len = 2
        std_factor = 2

        x['jump'] = 0
        x['sum_rtn'] = x['close'] / x['close'].shift(jump_len) - 1
        cond1 = x['sum_rtn'] >= std_factor * x['std'].shift(jump_len)
        cond2 = x['sum_rtn'] >= 0.03
        x.loc[cond1 & cond2, 'jump'] = 1

        x['hr'] = x['candle_begin_time'].dt.hour
        factor_df = x.loc[x['hr'] <= open_hour].copy()
        self.res.append({'date': x['date'].iloc[0], 'NonTrdTmJump': 1 if sum(factor_df['jump']) > 0 else 0})

# 非交易时间均价
class NonTrdTmMeanPrc(MetaFactor):
    def _cal(self, x):
        x['hr'] = x['candle_begin_time'].dt.hour
        factor_df = x.loc[x['hr'] <= open_hour].copy()
        self.res.append({'date': x['date'].iloc[0], 'NonTrdTmMeanPrc': factor_df['close'].mean()})

# 非交易时间最低价格
class NonTrdTmMinPrc(MetaFactor):
    def _cal(self, x):
        x['hr'] = x['candle_begin_time'].dt.hour
        factor_df = x.loc[x['hr'] <= open_hour].copy()
        self.res.append({'date': x['date'].iloc[0], 'NonTrdTmMinPrc': factor_df['low'].min()})

# 非交易时间最高价格
class NonTrdTmMaxPrc(MetaFactor):
    def _cal(self, x):
        x['hr'] = x['candle_begin_time'].dt.hour
        factor_df = x.loc[x['hr'] <= open_hour].copy()
        self.res.append({'date': x['date'].iloc[0], 'NonTrdTmMaxPrc': factor_df['high'].mean()})

# 非交易时间价格移动绝对值
class NonTrdTmPrcMov(MetaFactor):
    def _cal(self, x):
        x['hr'] = x['candle_begin_time'].dt.hour

        x['abs_rtn'] = abs(x['close'] / x['close'].shift(1) - 1)
        x['abs_rtn'].fillna(inplace=True, value=0)
        factor_df = x.loc[x['hr'] <= open_hour].copy()
        mov = factor_df['close'].iloc[0] * (1 + factor_df['abs_rtn']).cumprod().iloc[-1] - factor_df['open'].iloc[0]

        self.res.append({'date': x['date'].iloc[0], 'NonTrdTmPrcMov': mov})

# 非交易时间价格移动比率
class NonTrdTmPrcMovPct(MetaFactor):
    def _cal(self, x):
        x['hr'] = x['candle_begin_time'].dt.hour

        x['abs_rtn'] = abs(x['close'] / x['close'].shift(1) - 1)
        x['abs_rtn'].fillna(inplace=True, value=0)
        factor_df = x.loc[x['hr'] <= open_hour].copy()
        mov_pct = factor_df['close'].iloc[0] * (1 + factor_df['abs_rtn']).cumprod().iloc[-1] / \
                  factor_df['open'].iloc[0] - 1
        self.res.append({'date': x['date'].iloc[0], 'NonTrdTmPrcMovPct': mov_pct})

# 交易时间涨跌幅
class TrdTmRtn(MetaFactor):
    def _cal(self, x):
        x['hr'] = x['candle_begin_time'].dt.hour

        factor_df = x.loc[x['hr'] > open_hour].copy()
        factor_df.reset_index(drop=True, inplace=True)
        rtn = factor_df['close'].iloc[-1] / factor_df['open'].iloc[0] - 1
        self.res.append({'date': x['date'].iloc[0], 'TrdTmRtn': rtn})


if __name__ == '__main__':

    data = pd.read_csv(r'E:\biyelunwen\data\btc_sum_data_5T.csv')
    data = data[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()

    begin_time = '2017-01-01 00:00:00'
    end_time = '2019-12-31 23:55:00'
    res_df_list = []
    factor_list = [
        NonTrdTmRtn, NonTrdTmVolumePct, NonTrdTmBreak, NonTrdTmJump, NonTrdTmMeanPrc, NonTrdTmMinPrc, NonTrdTmMaxPrc,
        NonTrdTmPrcMov, NonTrdTmPrcMovPct, TrdTmRtn
    ]
    res_df = pd.DataFrame()
    for factor in factor_list:
        model = factor(raw_data=data)
        if len(res_df) == 0:
            res_df = model.cal_factor(begin_time=begin_time, end_time=end_time)
        else:
            res_df = res_df.merge(
                right=model.cal_factor(begin_time=begin_time, end_time=end_time),
                on='date',
                how='outer'
            )
    print(res_df)
    res_df.to_csv('intra_day_factor_predict.csv')
    exit()

    non_trd_tm_rtn = NonTrdTmPrcMov(raw_data=data)
    res_df_list.append(non_trd_tm_rtn.cal_factor(begin_time=begin_time, end_time=end_time))

    non_trd_volume_pct = NonTrdTmVolumePct(raw_data=data)
    res_df_list.append(non_trd_volume_pct.cal_factor(begin_time=begin_time, end_time=end_time))




