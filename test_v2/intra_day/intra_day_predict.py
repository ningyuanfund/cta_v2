import pandas as pd
import xgboost as xgb
from matplotlib import pyplot as plt
pd.set_option('expand_frame_repr', False)

data = pd.read_csv(r'E:\cta_v2\test_v2\intra_day\intra_day_factor_predict.csv')

data = data[[
    'date', 'NonTrdTmRtn', 'NonTrdTmVolumePct', 'NonTrdTmBreak', 'NonTrdTmJump', 'NonTrdTmMeanPrc', 'NonTrdTmMinPrc',
    'NonTrdTmMaxPrc', 'NonTrdTmPrcMov', 'NonTrdTmPrcMovPct', 'TrdTmRtn'
]].copy()

feature_list = [
    'NonTrdTmRtn', 'NonTrdTmVolumePct', 'NonTrdTmBreak', 'NonTrdTmJump', 'NonTrdTmMeanPrc', 'NonTrdTmMinPrc',
    'NonTrdTmMaxPrc', 'NonTrdTmPrcMov', 'NonTrdTmPrcMovPct'
]
data = data[1:].copy()
data.reset_index(drop=True, inplace=True)

def feature_inc(data, cols):
    for col in cols:
        for i in range(5, 50, 5):
            data[col + '_%s' % i] = data[col].rolling(i).mean()
    data.dropna(inplace=True, axis=0)
    data.reset_index(drop=True, inplace=True)
    return data

def stddize(data, cols):
    for col in cols:
        mean = data[col].mean()
        std = data[col].std(ddof=1)
        data[col] -= mean
        data[col] /= std
    return data


data = feature_inc(data=data, cols=feature_list)
feature_list = data.columns.drop('date').drop('TrdTmRtn')

predict_res = []
for i in range(500, len(data)):
    use_set = stddize(data=data[i - 500: (i + 1)].copy(), cols=feature_list)

    train_set = use_set[:-1].copy()
    train_x = train_set[feature_list]
    train_y_mean = train_set['TrdTmRtn'].mean()
    train_y_std = train_set['TrdTmRtn'].std(ddof=1)

    train_y = (train_set['TrdTmRtn'] - train_y_mean) / train_y_std

    predict_set = use_set[-1:].copy()
    predict_x = predict_set[feature_list]

    real_y = predict_set['TrdTmRtn'].iloc[0]

    dtrain = xgb.DMatrix(train_x, train_y)
    num_rounds = 500
    params = {
        'booster': 'gbtree',
        'objective': 'reg:squarederror',
        # 'num_class': 10
        'gamma': 0.1,
        'max_depth': 6,
        'lambda': 2,
        'subsample': 0.7,
        'colsample_bytree': 0.7,
        'min_child_weight': 3,
        'slient': 1,
        'eta': 0.1,
        'seed': 1000,
        'nthread': 4,
    }

    model = xgb.train(params, dtrain, num_rounds)

    fit_x = xgb.DMatrix(train_x)
    fit_y = model.predict(fit_x)
    plt.scatter(x=fit_y, y=train_y)

    dpredict = xgb.DMatrix(predict_x)
    y_pred = model.predict(dpredict)[0]
    predict_res.append(
        {
            'real': real_y,
            'predict': y_pred * train_y_std + train_y_mean
        }
    )
data = pd.DataFrame(predict_res)
data.to_csv(r'predict_res.csv')