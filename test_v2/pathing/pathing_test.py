import pandas as pd
import os
import copy
import time
from dateutil.relativedelta import relativedelta
import warnings
import itertools
from multiprocessing import Pool
from ckt.cq_sdk.path import get_data_path
import ckt.test.strategy.timing_strategy.Signals as sigs
from ckt.test.strategy.timing_strategy.equity_curve import equity_curve_with_long_and_short, find_change_time
import random

warnings.simplefilter(action='ignore')
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)


# ===具体执行回测
def cal_result(all_data, para, strategy_name):

    # 计算交易信号
    function_str = 'sigs.signal_%s_with_stop_lose(all_data.copy(), para)' % strategy_name
    df = eval(function_str)

    # 计算资金曲线
    df = equity_curve_with_long_and_short(df, leverage_rate=3, c_rate=3.0 / 1000)

    # 计算年化收益
    annual_return = (df['equity_curve'].iloc[-1] / df['equity_curve'].iloc[0]) ** (
        '1 days 00:00:00' / (df['candle_begin_time'].iloc[-1] - df['candle_begin_time'].iloc[0]) * 365) - 1

    # 计算最大回撤
    max_drawdown = (1 - df['equity_curve'] / df['equity_curve'].expanding().max()).max()

    # 计算年化收益回撤比
    ratio = annual_return / max_drawdown if max_drawdown != 0 else 0
    print(para, '参数收益：', df.iloc[-1]['equity_curve'], '参数年化收益回撤比：', ratio)
    return ratio


# ===寻找周围参数
def find_around_para(now_para, para_info, level):
    around_para = []
    rule = True

    para_id = 0
    for para in para_info:
        new_para = copy.copy(now_para)
        if now_para[para_id] + para_info[para]['gran'] * level < para_info[para]['max']:
            new_para[para_id] = now_para[para_id] + para_info[para]['gran'] * level
            if rule:
                around_para.append(copy.copy(new_para))

        if now_para[para_id] - para_info[para]['gran'] * level > para_info[para]['min']:
            new_para[para_id] = now_para[para_id] - para_info[para]['gran'] * level
            if rule:
                around_para.append(copy.copy(new_para))

        para_id += 1

    return around_para


# ===PATHING寻优主体
def find_best_para(all_data, strategy_name, start_para, para_info):

    if not isinstance(start_para, list):
        start_para = list(start_para)

    last_para = copy.copy(start_para)
    # 计算初始参数的结果
    last_ratio = cal_result(all_data, start_para, strategy_name)

    for level in range(5, 0, -1):
        better_exists = True
        while better_exists:

            # 寻找周围的参数
            around_para = find_around_para(last_para, para_info, level)

            # 计算周围参数结果
            not_found = True
            for para in around_para:
                ratio = cal_result(all_data, para, strategy_name)
                if ratio > last_ratio:
                    last_ratio = ratio
                    last_para = copy.copy(para)
                    not_found = False
            if not_found:
                print(level, last_para)
                better_exists = False
    if last_ratio > 0:
        return last_para
    else:
        return []


# ===PATHING回测框架
def func(all_data, strategy_name, freq, para_info, process):
    best_para_trained = {'para': [0, 0, 0],
                         'yield': 0,
                         'maxdd': 0,
                         'annual_rtn/maxdd': 0}
    start_para_list = []
    if process != 0:
        for i in range(process):
            start_para = []
            for para in para_info:
                total_steps = (para_info[para]['max'] - para_info[para]['min']) / para_info[para]['gran']
                step = random.randint(0, total_steps)
                start_para.append(para_info[para]['min'] + para_info[para]['gran'] * step)

            start_para_list.append(start_para)
    else:
        init_value_list = []
        for para in para_info:
            init_value_list.append([int((para_info[para]['min'] + para_info[para]['max']) * 0.25),
                                    int((para_info[para]['min'] + para_info[para]['max']) * 0.75)])

        start_para_list = list(itertools.product(*[init_value_list[i] for i in range(len(init_value_list))]))
        process = len(start_para_list)

    print('process', process)
    pool = Pool(processes=process)  # 开辟进程池
    result = []
    for i in range(process):
        result.append(pool.apply_async(find_best_para, (all_data, strategy_name, start_para_list[i], para_info, )))

    pool.close()  # 关闭进程池
    pool.join()  # 等待开辟的所有进程执行完后，主进程才继续往下执行

    for i, res in enumerate(result):
        print(res.get())
    end = time.clock()
    print("start: {}; end: {}; end - start: {}".format(start, end, end - start))
    exit()

    # 存储数据（多进程模式下如果保存成dataframe后面会有重复值）
    return str(freq) + ':' + str(best_para_trained['para']) + ':' + str(best_para_trained['yield']) + ':' + str(
        best_para_trained['maxdd']) + ':' + str(best_para_trained['annual_rtn/maxdd'])


if __name__ == '__main__':
    start = time.clock()
    # 策略名称
    strategy_name = 'bolling'

    # 这边需要手动输入参数名称
    para_names = ['n', 'm', '止损比例', '时间级别', '策略收益', '最大回撤', '年化收益/最大回撤']

    process = 0  # 最大进程数

    # 回测系统参数
    sys_para = {'test_period': 3,  # 训练期长度，12个月
                'use_period': 1,  # 使用期长度，1个月
                'begin_date': '2018-01-01',  # 整体回测开始的时间，即使用数据的最早时间，如果早于数据最早的时间，则会自动使用数据最早的时间
                'if_all_data': False,  # 如果为True，每轮回测都将使用所有历史数据，但至少一年
                'freq': ['30T']
                }

    # 待测参数组合。每次回测都相同

    para_info = {'n': {'min': 40,
                       'max': 300,
                       'gran': 10},
                 'm': {'min': 1,
                       'max': 10,
                       'gran': 0.5},
                 'stop_loss': {'min': 2,
                               'max': 10,
                               'gran': 1}}

    for pair in ['BTCUSD']:  # 币种
        for freq in sys_para['freq']:
            print('\n====%s====' % pair)

            data = pd.read_hdf(r'E:\cta_v2\data\test_v2\input\BTCUSD.h5', key='1H')
            data['candle_begin_time'] = pd.to_datetime(data['candle_begin_time'])
            result = func(data, strategy_name, '1H', para_info, process)

