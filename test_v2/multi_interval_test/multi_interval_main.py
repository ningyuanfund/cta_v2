import pandas as pd
import os
from dateutil.relativedelta import relativedelta
import warnings
from multiprocessing import Pool
from ckt.cq_sdk.path import get_data_path
import ckt.test.strategy.timing_strategy.Signals as sigs
from ckt.test.strategy.timing_strategy.equity_curve import equity_curve_with_long_and_short, find_change_time
from sqlalchemy import create_engine

warnings.simplefilter(action='ignore')
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)


engine = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')  # schema = cta_test


# 大周期开仓后，
# 用小周期的布林线、短区间、窄轨道做震荡

# 需要的参数：大周期参数，布林线的长度和轨道宽度；小周期参数，长度和轨道宽度
# 资金分配比例：每次平仓之后rebalance。大周期prop_long，小周期 1-prop_long

# 根据给定时间返回数据
def get_data_by_date(engine, tablename, start_time, end_time):
    sql = 'select * from %s where ' \
          'candle_begin_time >= %s and ' \
          'candle_begin_time <= %s' % (tablename, start_time, end_time)
    data = pd.read_sql_query(sql, con=engine)
    return data


# ===具体进行回测
def func(all_data, strategy_name, freq, para_list):
    best_para_trained = {'para': [0, 0, 0],
                         'yield': 0,
                         'maxdd': 0,
                         'annual_rtn/maxdd': 0}

    for para in para_list:
        # 计算交易信号

        function_str = 'sigs.signal_%s_with_stop_lose(all_data.copy(), para)' % strategy_name
        df = eval(function_str)

        # 计算资金曲线
        df = equity_curve_with_long_and_short(df, leverage_rate=3, c_rate=3.0 / 1000)

        # 计算年化收益
        annual_return = (df['equity_curve'].iloc[-1] / df['equity_curve'].iloc[0]) ** (
            '1 days 00:00:00' / (df['candle_begin_time'].iloc[-1] - df['candle_begin_time'].iloc[0]) * 365) - 1

        # 计算最大回撤
        max_drawdown = (1 - df['equity_curve'] / df['equity_curve'].expanding().max()).max()

        # 计算年化收益回撤比
        ratio = annual_return / max_drawdown if max_drawdown != 0 else 0
        print(freq, para, '最优参数收益：', df.iloc[-1]['equity_curve'], '最优参数年化收益回撤比：', ratio)

        # 如果现在测出来的参数比之前保留的好，就更新最优参数
        if (ratio > best_para_trained['annual_rtn/maxdd']) | (best_para_trained['para'][0] == 0):
            best_para_trained['para'] = para
            best_para_trained['yield'] = df['equity_curve'].iloc[-1]
            best_para_trained['maxdd'] = max_drawdown
            best_para_trained['annual_rtn/maxdd'] = ratio

    # 存储数据（多进程模式下如果保存成dataframe后面会有重复值）
    return str(freq) + ':' + str(best_para_trained['para']) + ':' + str(best_para_trained['yield']) + ':' + str(
        best_para_trained['maxdd']) + ':' + str(best_para_trained['annual_rtn/maxdd'])


# ===生成训练期x个月内最优参数的时间点列表，方便循环回测时读取
def gen_train_date(sys_para):
    test_period = sys_para['test_period']  # 参数回测期长度
    use_period = sys_para['use_period']  # 参数使用期长度
    begin_date = pd.to_datetime(sys_para['begin_date'])  # 整体回测开始时间（训练集的最早时间）
    print(sys_para)
    # 回测的时间点列表
    train_date_list = []
    while begin_date <= pd.to_datetime(sys_para['end_date']) - relativedelta(months=test_period):

        # 生成训练开始时间、训练结束时间、测试开始时间
        train_date_list.append({'train_begin_date': begin_date,
                               'train_end_date': begin_date + relativedelta(months=test_period),
                                'test_end_date': begin_date + relativedelta(months=test_period + use_period),
                                })
        begin_date += relativedelta(months=use_period)

    # 如果每轮回测都要使用所有历史数据
    if sys_para['if_all_data']:
        for date_info in train_date_list:
            date_info['train_begin_date'] = pd.to_datetime(sys_para['begin_date'])

    return train_date_list


# ===对回测期的数据进行回测，返回最优参数
def train(df, strategy_name, freq, para_list, train_info, rtn_path, process):

    split_list = []  # 分成process组的待测参数
    # 待测参数分片
    for i in range(process):
        if i < process:
            split_para = para_list[i * int(len(para_list) / process): (i+1) * int(len(para_list) / process)]
        else:
            split_para = para_list[i * int(len(para_list) / process):]
        split_list.append(split_para)

    pool = Pool(processes=process)  # 开辟进程池
    result = []
    for split_para_list in split_list:
        result.append(pool.apply_async(func, (df, strategy_name, freq, split_para_list,)))

    pool.close()  # 关闭进程池
    pool.join()  # 等待开辟的所有进程执行完后，主进程才继续往下执行

    A = pd.DataFrame()
    for i, res in enumerate(result):
        A.loc[i, '参数'] = res.get().split(':')[1]
        A.loc[i, '时间级别'] = res.get().split(':')[0]
        A.loc[i, '策略收益'] = float(res.get().split(':')[2])
        A.loc[i, '最大回撤'] = float(res.get().split(':')[3])
        A.loc[i, '年化收益/最大回撤'] = float(res.get().split(':')[4])

    temp = A['参数'].str[1:-1].str.split(',', expand=True)
    rtn = pd.concat([temp, A.iloc[:, 1:]], axis=1)
    print(rtn)
    rtn.columns = para_names
    rtn_path = rtn_path + '/para_record'

    if not os.path.exists(rtn_path):
        os.makedirs(rtn_path)

    rtn.to_csv(rtn_path + '/%s.csv' % train_info['train_end_date'].strftime('%Y-%m'), encoding='utf_8_sig')  # 用于保存完整的参数排名结果

    # 最优参数转换回列表格式
    top_para_str = A.sort_values(by='年化收益/最大回撤', ascending=False)[:1]['参数'].values[0][1:-1].split(',')

    top_para = []
    for para in top_para_str:
        top_para.append(float(para))

    return top_para


# ===主程序
def main(pair, strategy_name, para_list, para_names, sys_para, freq, process):

    begin_date = sys_para['begin_date']  # 整体回测开始时间（使用的数据的最早时间）

    # 设置路径
    if sys_para['if_all_data']:
        data_para = 'all_data'
    else:
        data_para = 'local_data'

    # 设定路径，如果没有这个路径的话就生成
    path = get_data_path('test_v2', 'output', strategy_name, '%s' % pair)
    if not os.path.exists(path):
        os.makedirs(path)

    rtn_path = get_data_path('test_v2', 'output', strategy_name, '%s' % pair, freq, data_para)  # 用于保存完整的参数排名结果
    if not os.path.exists(rtn_path):
        os.makedirs(rtn_path)

    sql = 'select candle_begin_time from cat_test.%s' % pair
    date_list = list(pd.read_sql_query(sql))
    # # 导入总数据
    # all_data = pd.read_hdf(get_data_path('test_v2', 'input', '%s.h5' % pair), key=freq)  # 数据文件存放路径
    #
    # # 选取时间段
    # all_data = all_data[all_data['candle_begin_time'] >= pd.to_datetime(begin_date)].copy()
    # all_data.reset_index(inplace=True, drop=True)
    #
    # # 生成回测期、使用期的日期列表
    # data_begin_date = all_data.iloc[0]['candle_begin_time']  # 可用数据的最早时间
    # data_end_date = all_data.iloc[-1]['candle_begin_time']  # 可用数据的最晚时间

    if data_begin_date > pd.to_datetime(sys_para['begin_date']):
        print('可用数据仅开始于', pd.to_datetime(data_begin_date).strftime('%Y-%m-%d'))
        sys_para['begin_date'] = pd.to_datetime(data_begin_date).strftime('%Y-%m-%d')

    sys_para['end_date'] = data_end_date.strftime('%Y-%m-%d')
    # 计算时间节点，保存在list中
    train_list = gen_train_date(sys_para)

    # 回测出每个月月初往前数12个月的最优参数
    top_para_record = {'参数训练开始': [],
                       '参数训练结束': [],
                       '参数': []}

    # 开始训练
    for train_info in train_list:
        print('本轮训练开始时间为', train_info['train_begin_date'])
        print('本轮训练结束时间为', train_info['train_end_date'])
        print('本轮参数使用至', train_info['test_end_date'])

        # 截取训练数据
        condition1 = all_data['candle_begin_time'] >= pd.to_datetime(train_info['train_begin_date'])
        condition2 = all_data['candle_begin_time'] < pd.to_datetime(train_info['train_end_date'])
        train_df = all_data[condition1 & condition2].copy()
        train_df.reset_index(inplace=True, drop=True)

        # 回测计算最优参数，在单一阶段的训练中使用多进程，切换阶段时还是要阻塞
        # 减少阶段内的IO，加快速度，进程计算完成后再比较process个最优参数哪个好，返回最优的top_para
        top_para = train(train_df, strategy_name, freq, para_list, train_info, rtn_path, process)
        train_info['best_para_trained'] = top_para
        print('寻找到的最优参数为', top_para)

        # 最优参数保存在list里，最终生成 dataframe
        top_para_record['参数训练开始'].append(train_info['train_begin_date'])
        top_para_record['参数训练结束'].append(train_info['train_end_date'])
        top_para_record['参数'].append(top_para)

        print('*' * 20)

    # 输出每个阶段的最优参数
    top_para_df = pd.DataFrame(top_para_record)
    top_para_df.to_csv(rtn_path + r'/top_para_record.csv', encoding='utf_8_sig')
    #
    # # 根据最优参数计算每个月初往后的开仓、平仓、止损点
    # trade_record = []
    # for train_info in train_list:
    #     para = train_info['best_para_trained']  # 每个月应该使用的最优参数
    #
    #     # 用该月的最优参数进行使用，找到使用期的开仓、平仓、止损点
    #     monthly_trade_record = find_change_time(all_data, para, strategy_name, train_info['train_end_date'],
    #                                             train_info['test_end_date'], freq)
    #
    #     # 如果不是第一个月，就需要对交易进行筛选，去掉交易开始时间比上一个月最后一笔交易结束时间更早的交易
    #     if train_list.index(train_info) > 0:
    #         if len(train_list[train_list.index(train_info) - 1]['trade_record']) > 0:
    #             last_trade_end_time = train_list[train_list.index(train_info) - 1]['trade_record'][-1]['trade_end_time']
    #         else:
    #             last_trade_end_time = pd.to_datetime('1990-01-01')
    #         # 检验产生的所有交易，去除不符合要求的（交易开始时间早于前一个月的最后一笔交易的结束时间）
    #         for trade in monthly_trade_record:
    #             if trade['trade_begin_time'] > last_trade_end_time:
    #                 trade_record.append(trade)
    #     else:
    #         trade_record += monthly_trade_record
    #
    #     train_info['trade_record'] = monthly_trade_record
    #
    # # 根据交易记录生成信号df
    # trade_df_list = []
    # for trade in trade_record:
    #     # 开仓信号记录方向
    #     trade_df_list.append([trade['trade_begin_time'], trade['long_or_short']])
    #     # 平仓信号记录为0
    #     trade_df_list.append([trade['trade_end_time'], 0])
    # trade_df = pd.DataFrame(trade_df_list, columns=['candle_begin_time', 'signal'])
    #
    # trade_df.sort_values(by='candle_begin_time', inplace=True)
    # trade_df.drop_duplicates(subset='candle_begin_time', keep='last', inplace=True)
    #
    # # 输出交易记录
    # trade_df.to_csv(rtn_path + r'/trade.csv')
    #
    # # 将信号df与原始数据合并
    # all_data = all_data.merge(trade_df, how='outer', on='candle_begin_time')
    #
    # # 生成pos，后续计算资金曲线
    # all_data['pos'] = all_data['signal'].shift()
    # all_data['pos'].fillna(method='ffill', inplace=True)
    # all_data['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0
    #
    # # 计算资金曲线
    # df = equity_curve_with_long_and_short(all_data, leverage_rate=3, c_rate=3.0 / 1000)
    # print('最终收益为', df.iloc[-1]['equity_curve'])
    #
    # df.to_csv(rtn_path + r'/equity_curve.csv')

    # 根据最优参数计算综合资金曲线


if __name__ == '__main__':

    # 策略名称
    strategy_name = 'bolling'

    # 这边需要手动输入参数名称
    para_names = ['n', 'm', '止损比例', '时间级别', '策略收益', '最大回撤', '年化收益/最大回撤']

    process = 4  # 最大进程数

    # 回测系统参数
    sys_para = {'test_period': 1,  # 训练期长度
                'use_period': 1,  # 使用期长度
                'begin_date': '2018-01-01',  # 整体回测开始的时间，即使用数据的最早时间，如果早于数据最早的时间，则会自动使用数据最早的时间
                'if_all_data': False,  # 如果为True，每轮回测都将使用所有历史数据，但至少一年
                'freq': ['1H']
                }

    # 大周期待测参数组合。每次回测都相同
    n_list = range(40, 300, 20)
    m_list = [i / 2 for i in range(1, 11)]
    stop_lose_pct_list = range(1, 11, 2)

    # 小周期待测参数组合。每次回测都相同
    short_n_list = range(40, 100, 10)
    short_m_list = [i / 5 for i in range(1, 11)]
    short_stop_lose_list = [i/2 for i in range(6, 11)]

    # 生成参数列表
    para_list = []
    for n in n_list:
        for m in m_list:
            for stop_lose_pct in stop_lose_pct_list:
                for short_n in short_n_list:
                    for short_m in short_m_list:
                        for short_stop_lose in short_stop_lose_list:
                            para_list.append([n, m, stop_lose_pct, short_n, short_m, short_stop_lose])

    for pair in ['BTCUSD']:  # 币种
        for freq in sys_para['freq']:
            print('\n====%s====' % pair)
            main(pair, strategy_name, para_list, para_names, sys_para, freq, process)
