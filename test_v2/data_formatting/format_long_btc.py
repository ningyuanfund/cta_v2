import pandas as pd
from sqlalchemy import create_engine


pd.set_option('expand_frame_repr', False)

early_data = pd.read_csv(r'E:\data\btc_2013-20191123_5t.csv')
early_data = early_data[['candle_begin_time', 'open', 'high', 'low', 'close']].copy()
early_data['candle_begin_time'] = pd.to_datetime(early_data['candle_begin_time'])

eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')
sql = 'select * from "cta_test"."BTCUSD"'
late_data = pd.read_sql_query(sql, con=eg)
late_data = late_data[['candle_begin_time', 'open', 'high', 'low', 'close']].copy()
late_data['candle_begin_time'] = pd.to_datetime(late_data['candle_begin_time'])

sum_data = pd.concat([early_data, late_data])
sum_data.drop_duplicates(inplace=True, subset='candle_begin_time')
sum_data.reset_index(drop=True, inplace=True)
sum_data.to_sql('BTCUSD_long', con=eg, schema='cta_test', if_exists='replace')
