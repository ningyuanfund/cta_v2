import time
from datetime import datetime, timedelta

import pandas as pd
from sqlalchemy import create_engine

from ckt.cq_sdk import *

pd.set_option('expand_frame_repr', False)

engine = create_engine('postgresql://postgres:thomas@47.75.180.89/bfx_alldata')
back_engine = create_engine('postgresql://postgres:holt@49.51.196.202/bfx_data1T')
local_engine = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')
eg = engine
while True:
    try:
        # 获取数据库内所有表名
        sql = "select tablename from pg_tables where schemaname='public'"
        table_df = pd.read_sql_query(sql, con=eg)
        table_df['symbol'] = table_df['tablename'].str[9:16]
        break
    except Exception as e:
        print(e)

sql = "select tablename from pg_tables where schemaname='raw_data'"
local_table_df = pd.read_sql_query(sql, con=local_engine)
local_table_df['symbol'] = local_table_df['tablename'].str[9:16]


# 从服务器上获取数据，temp为该数据的表名df，返回拼接好的数据
def sort_df(temp):

    df_list = []
    for i in range(len(temp)):
        table_name = temp['tablename'].at[i]
        print(table_name)
        if table_name in list(local_table_df['tablename']):
            sql_cmd = 'select * from "raw_data"."%s"' % table_name
            while True:
                try:
                    df = pd.read_sql(sql_cmd, con=local_engine)
                    df_list.append(df)
                    break
                except Exception as e:
                    print(e)
                    time.sleep(5)
            continue

        sql_cmd = 'select * from "%s"' % table_name
        while True:
            try:
                df = pd.read_sql(sql_cmd, con=eg)
                df.to_sql(table_name, con=local_engine, schema='raw_data')
                df_list.append(df)
                break
            except Exception as e:
                print(e)
                time.sleep(5)

    df_summed = pd.concat(df_list)
    df_summed.drop_duplicates(inplace=True)
    df_summed.sort_values(by='candle_begin_time')
    df_summed.reset_index(drop=True, inplace=True)

    return df_summed


# 生成时间列，用于遍历数据（独立于数据）
def get_time(start, end):

    end_time = datetime.strptime(end, "%Y-%m-%d %H:%M:%S")
    now_time = datetime.strptime(start, "%Y-%m-%d %H:%M:%S")
    # end_time = end
    # now_time = start

    time_list = [now_time]
    while now_time < end_time:
        now_time += timedelta(minutes=5)
        time_list.append(now_time)
    return time_list


# 补齐缺失数据
def data_refill(df, begin, end):

    tm_list = get_time(begin, end)
    tm_df = pd.DataFrame(tm_list)
    tm_df.rename(columns={0: 'candle_begin_time'}, inplace=True)

    data = df.merge(tm_df, how='outer', on='candle_begin_time')
    data.sort_values(by='candle_begin_time', inplace=True)
    data['open'].fillna(method='ffill', inplace=True)
    data['high'].fillna(method='ffill', inplace=True)
    data['low'].fillna(method='ffill', inplace=True)
    data['close'].fillna(method='ffill', inplace=True)

    data['volume'].fillna(value=0, inplace=True)
    data.reset_index(inplace=True, drop=True)
    data = data[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()
    return data


symbol_list = [
    'BTC/USD',
    'ETH/USD',
    'EOS/USD',
    'LTC/USD',
    'XRP/USD',
    'BCH/USD',
]
# symbol_list = ['XRP/USD']


for symbol in symbol_list:
    print('================%s=================' % symbol)
    df_temp = table_df[table_df['symbol'] == symbol].copy()
    df_temp.reset_index(inplace=True, drop=True)
    df_sum = sort_df(df_temp)

    # resample 数据，保存
    df_ori = df_sum.copy()
    df_ori.reset_index(inplace=True, drop=True)
    df_ori['candle_begin_time'] = pd.to_datetime(df_ori['candle_begin_time'])
    data_path = get_data_path('test_v2', 'input', '%s.h5' % symbol.replace('/', ''))

    begin = df_ori['candle_begin_time'].iloc[0].strftime("%Y-%m-%d %H:%M:%S")
    end = df_ori['candle_begin_time'].iloc[-1].strftime("%Y-%m-%d %H:%M:%S")
    df_ori_refilled = data_refill(df_ori, begin, end)

    df_ori_refilled.to_hdf(data_path, key='5T')

    time_interval_list = ['15T', '30T', '1H', '1D']
    for time_interval in time_interval_list:
        period_df = df_ori_refilled.resample(rule=time_interval, on='candle_begin_time', base=0, label='left',
                                             closed='left').agg(
            {'open': 'first',
             'high': 'max',
             'low': 'min',
             'close': 'last',
             'volume': 'sum',
             })
        period_df.dropna(subset=['open'], inplace=True)

        period_df = period_df[['open', 'high', 'low', 'close', 'volume']]
        period_df.reset_index(inplace=True)
        # period_df.to_csv(r'C:\Users\sycam\Desktop\%s_%s.csv' % (symbol.replace('/', ''), time_interval))
        period_df.to_hdf(data_path, key=time_interval)

    df = pd.read_hdf(data_path, key='5T')
    df.reset_index(inplace=True)

    # df.to_sql(symbol.replace('/', ''), eg, index=False, if_exists='replace', schema='cta_test')
    df.to_sql(symbol.replace('/', ''), con=local_engine, index=False, if_exists='replace', schema='cta_test')
