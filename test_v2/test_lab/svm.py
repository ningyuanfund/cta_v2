from datetime import timedelta

import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.svm import SVR
from sqlalchemy import create_engine

pd.set_option('expand_frame_repr', False)
eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')
engine_emotion_backup = create_engine('postgresql://postgres:holt@49.51.196.202:5432/factors', echo=False)

sql = 'select * from "cta_test"."BTCUSD"'

data = pd.read_sql_query(sql, con=eg)
data['candle_begin_time'] = pd.to_datetime(data['candle_begin_time'])
data = data.resample(rule='1H', on='candle_begin_time', base=0, label='left', closed='left').agg(
    {'open': 'first',
     'high': 'max',
     'low': 'min',
     'close': 'last',
     'volume': 'sum',
     })
data.reset_index(inplace=True)

table = 'TakerTrader_%s_1H' % 'BTC'
sql = 'select * from "%s"' % table
tt_data = pd.read_sql_query(sql, con=engine_emotion_backup)
tt_data['candle_begin_time'] = pd.to_datetime(tt_data['candle_begin_time'])
tt_data['candle_begin_time'] -= timedelta(hours=8)

data = data.merge(tt_data, on='candle_begin_time', how='inner')

data['price'] = data[['open', 'high', 'low', 'close']].mean(axis=1).apply(lambda x: np.log(x))
data[['open', 'high', 'low', 'close']] = data[['open', 'high', 'low', 'close']].apply(lambda x: np.log(x))

for lag in range(1, 51):
    data['rtn_%s' % lag] = data['price'] - data['price'].shift(lag)

data.dropna(axis=0, inplace=True)
data.reset_index(drop=True, inplace=True)


def rolling_standardize(window, pre_series, data):

    for col in pre_series:
        post_series = 'de_%s' % col

        # rolling standardize
        data['mean'] = data[col].rolling(window).mean()
        data['std'] = data[col].rolling(window).std(ddof=1)

        data[post_series] = (data[col] - data['mean']) / data['std']
        data.drop(col, inplace=True, axis=1)
    data.drop('mean', inplace=True, axis=1)
    data.drop('std', inplace=True, axis=1)
    return data


target_hr = 24
target = data[['candle_begin_time', 'open', 'high', 'low', 'close']].copy()
target['high_rtn'] = target['high'].shift(-target_hr) / target['close'] - 1
target['low_rtn'] = target['low'].shift(-target_hr) / target['close'] - 1
target['candle_begin_time'] = pd.to_datetime(target['candle_begin_time'])
target = target[:-target_hr].copy()

data = rolling_standardize(30, list(data.columns[2:]), data)
data.dropna(axis=0, inplace=True)
data.reset_index(drop=True, inplace=True)


train_length = 500
high_predict_res = []
low_predict_res = []
for i in range(train_length + 100, len(data)):
    print(i, len(data))
    # 用特征工程选择特征
    train_data = data[i - train_length:i].copy()
    t_train_data = train_data[train_data.columns[2:]]
    train_data.reset_index(inplace=True, drop=True)
    pca = PCA(5)

    pca.fit(t_train_data)
    print(pca.explained_variance_ratio_)

    # 对选出的特征进行标准化
    feature = pd.DataFrame(pca.transform(t_train_data))

    train_data[feature.columns] = feature[feature.columns].apply(lambda x: (x - x.mean()) / x.std(ddof=1), axis=0)
    app_X = train_data[list(feature.columns) + ['candle_begin_time']].copy()
    app_X['candle_begin_time'] = pd.to_datetime(app_X['candle_begin_time'])

    sum_data = target.merge(app_X, on='candle_begin_time', how='inner')
    sum_data.drop(['candle_begin_time', 'open', 'high', 'low', 'close'], inplace=True, axis=1)

    # 使用数据训练模型
    high_model = SVR(gamma='scale')
    high_model.fit(sum_data[feature.columns], sum_data['high_rtn'])

    low_model = SVR(gamma='scale')
    low_model.fit(sum_data[feature.columns], sum_data['low_rtn'])

    # 获取用于预测的数据
    if i < len(data) - 1:
        predict_X = data[train_data.columns[2: -5]].iloc[i + 1]
    else:
        break

    # 使用同样的PCA模型得到特征
    res = pca.transform([list(predict_X)])

    # 使用特征进行预测
    pre_high = high_model.predict(res)[0]
    pre_low = low_model.predict(res)[0]

    high_predict_res.append({'predict_time': app_X['candle_begin_time'].iat[-1], 'predict': pre_high,
                             'real': target['high_rtn'].iat[i + 1]})
    low_predict_res.append({'predict_time': app_X['candle_begin_time'].iat[-1], 'predict': pre_low,
                            'real': target['low_rtn'].iat[i + 1]})

high_predict_df = pd.DataFrame(high_predict_res)
low_predict_df = pd.DataFrame(low_predict_res)
high_predict_df.to_csv(r'C:\Users\sycam\Desktop\high.csv')
low_predict_df.to_csv(r'C:\Users\sycam\Desktop\low.csv')
print(data)
