from sqlalchemy import create_engine
import pandas as pd
import os
import re
engine1 = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')
engine2 = create_engine('postgresql://postgres:thomas@localhost:5432/bfx_multicoin')
sql = "select tablename from pg_tables where schemaname='public'"

tables = pd.read_sql(sql, con=engine1)

tables['name'] = tables['tablename'].apply(lambda x: x[9:])
tables = tables.sort_values(by='name')
symbol_list = list(tables['name'].drop_duplicates())


data_path = r'E:\data\bitfinex\BITFINEX_candle_new'

for symbol in symbol_list:

    if (not symbol.startswith('US')) & (symbol.endswith('USD')):
        print(symbol)
        symbol_df_list = []
        for roots, dirs, files in os.walk(data_path):
            if files:
                for file in files:
                    if file.endswith('_5T.csv'):

                        # file_path = r'%s\%s' % (roots, file)
                        # date = re.findall('\d{8}', file_path)[0]
                        # sql_name = r'%s_%s' % (date, file.strip('BITFINEX').strip('_%s_5T.csv' % date))
                        # if int(date) > 20181205:
                        #     print(sql_name)
                        #     sql_df = pd.read_csv(file_path, skiprows=1)
                        #     sql_df.to_sql(sql_name, engine1, index=False, if_exists='replace')
                        # else:
                        #     print(sql_name, 'not suited')

                        if len(re.findall(symbol, file)) > 0:
                            print(file)
                            file_path = r'%s\%s' % (roots, file)
                            sql_df = pd.read_csv(file_path, skiprows=1)
                            sql_df.to_sql(symbol, engine2, index=False, if_exists='append')



