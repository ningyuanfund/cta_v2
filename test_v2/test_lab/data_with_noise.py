import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
from sqlalchemy import create_engine

pd.set_option('expand_frame_repr', False)
eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')


def gen_noisy_data(df):
    std_f = 0.05  # 噪音是标准差为过去一天标准差的5%，均值为0的正态分布
    df['open_std'] = df['open'].rolling(12 * 24).std(ddof=1) * std_f
    df['high_std'] = df['high'].rolling(12 * 24).std(ddof=1) * std_f
    df['low_std'] = df['low'].rolling(12 * 24).std(ddof=1) * std_f
    df['close_std'] = df['close'].rolling(12 * 24).std(ddof=1) * std_f
    df['volume_std'] = df['volume'].rolling(12 * 24).std(ddof=1) * std_f

    df['open_noisy'] = df['open'] + np.random.normal(0, df['open_std'])
    df['high_noisy'] = df['high'] + np.random.normal(0, df['high_std'])
    df['low_noisy'] = df['low'] + np.random.normal(0, df['low_std'])
    df['close_noisy'] = df['close'] + np.random.normal(0, df['close_std'])
    df['volume_noisy'] = df['volume'] + np.random.normal(0, df['volume_std'])
    # df = df[-100:].copy()

    df['open_noisy'].fillna(df['open'], inplace=True)
    df['high_noisy'].fillna(df['high'], inplace=True)
    df['low_noisy'].fillna(df['low'], inplace=True)
    df['close_noisy'].fillna(df['close'], inplace=True)
    df['volume_noisy'].fillna(df['volume'], inplace=True)

    df = df[['candle_begin_time', 'open_noisy', 'high_noisy', 'low_noisy', 'close_noisy', 'volume_noisy']]
    df.rename(columns={'open_noisy': 'open',
                       'high_noisy': 'high',
                       'low_noisy': 'low',
                       'close_noisy': 'close',
                       'volume_noisy': 'volume'}, inplace=True)

    # df.to_sql(symbol, con=eg, schema='cta_test_noisy', if_exists='replace')
    # if if_return:
    return df


def gen_pure_data(symbol, freq):
    sql = 'select * from "cta_test"."%s"' % symbol
    df = pd.read_sql_query(sql, con=eg)
    df = df.resample(rule=freq, on='candle_begin_time', base=0, label='left', closed='left').agg(
        {'open': 'first',
         'high': 'max',
         'low': 'min',
         'close': 'last',
         'volume': 'sum',
         }
    )
    df.reset_index(inplace=True)

    return df


if __name__ == '__main__':
    for symbol in ['BTCUSD', 'ETHUSD', 'EOSUSD', 'LTCUSD', 'XRPUSD', 'BCHUSD', 'BSVUSD']:
        pure_data = gen_pure_data(symbol, freq='15T')
        gen_noisy_data(pure_data)
