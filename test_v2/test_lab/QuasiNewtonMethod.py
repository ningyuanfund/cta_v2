from multiprocessing import Pool

import numpy as np
import pandas as pd

pd.set_option('expand_frame_repr', False)


class QuasiNewtonMethod:
    def __init__(self, lamb):
        self.lamb = lamb
        self.solution = np.mat([])

    def target_function(self, x):
        res = 0
        for i in x:
            res += (i[0, 0]-1) ** 2
        return res

    def gradient_function(self, x):
        res = []
        for i in x:
            res.append(2 * i[0, 0] - 2)
        return np.mat(res).T

    @staticmethod
    def norm(vec, norm):
        res = 0
        for i in vec:
            res += i[0, 0] ** norm
        return res

    def _one_dim_search(self, x, p, threshold=0.0001):
        def wrap_target(lam, x, p):
            return self.target_function(x + lam * p)

        left_bound = 0
        right_bound = 10
        rou = 1 - (np.sqrt(5) - 1) / 2
        lam1 = left_bound + rou * (right_bound - left_bound)
        lam2 = right_bound - rou * (right_bound - left_bound)

        while lam2 - lam1 > threshold:
            if wrap_target(lam1, x, p) > wrap_target(lam2, x, p):
                left_bound = lam1
                lam1 = lam2
                lam2 = right_bound - rou * (right_bound - left_bound)
            elif wrap_target(lam1, x, p) < wrap_target(lam2, x, p):
                right_bound = lam2
                lam2 = lam1
                lam1 = left_bound + rou * (right_bound - left_bound)
            else:
                left_bound = lam1
                right_bound = lam2
                lam1 = left_bound + rou * (right_bound - left_bound)
                lam2 = right_bound - rou * (right_bound - left_bound)
        return lam2

    def res(self, init_x, threshold, max_iter=40):
        Bk = np.eye(len(init_x), len(init_x))
        gk = self.gradient_function(init_x)
        if self.norm(gk, 2) < threshold:
            return init_x
        xk = init_x

        while True:
            pk = - np.linalg.inv(Bk) * gk

            lamk = self._one_dim_search(x=xk, p=pk)

            xk_1 = xk + lamk * pk
            gk_1 = self.gradient_function(xk_1)
            iter_num = 0
            if (self.norm(gk_1, 2) < threshold) \
                    or (iter_num == max_iter) or (self.norm(gk, 2) - self.norm(gk_1, 2) < threshold ** 2):
                print('finished')
                print(xk_1)
                self.solution = xk_1
                return {'w': xk_1, 'norm': self.norm(gk_1, 2)}
            else:
                print(self.norm(gk_1, 2))

            dk = lamk * pk
            yk = gk_1 - gk

            # magic
            Bk_1 = Bk + (yk * yk.T) / (yk.T * dk)[0, 0] + (Bk * dk * dk.T * Bk) / (dk.T * Bk * dk)[0, 0]

            # iterate
            Bk = Bk_1
            gk = gk_1
            xk = xk_1 + np.ones((len(xk), 1)) * 0.001  # 不向最优方向走

            iter_num += 1
            print(xk.T)

    # 多个出发点
    def multi_init_res(self, init_x, threshold, max_iter=40):
        x_space = [init_x, init_x * -1, init_x * 10, init_x * -10, init_x / 10, init_x / -10]

        pool = Pool(processes=len(x_space))
        result = []

        for x in x_space:
            result.append(pool.apply_async(self.res, (x, threshold, max_iter)))

        global_res = np.mat([])
        global_res_grad_norm = 10
        for res in result:
            if res.get()['norm'] < global_res_grad_norm:
                global_res = res.get()['w']
                global_res_grad_norm = res.get()['norm']

        print('GLOBAL RESULT', global_res)
        print('GLOBAL RESULT NORM', global_res_grad_norm)
        return global_res


if __name__ == '__main__':
    test_x = np.mat([1, 2, 3]).T

    qnm = QuasiNewtonMethod(lamb=0.01)

    res_x = qnm.res(init_x=test_x, threshold=0.00001)

    # print(x)
