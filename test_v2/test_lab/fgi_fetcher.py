import re
from datetime import datetime

import pandas as pd
import requests


def get_FG_index():
    # 伪装自己的身份
    headers = {'User-Agent': 'Mozilla5.0 (Windows NT 10.0; WOW64) AppleWebKit537.36 (KHTML, like Gecko) Chrome/'
               '50.0.2661.102 Safari/537.36'}

    # 读取网站提供的所有历史指数
    r = requests.get('https://api.alternative.me/fng/?limit=99999',
                     headers=headers)
    # 把获取的网页做一点整理
    # 把换行符，空格，制表符都删除
    r = r.text.replace('\n', '').replace(' ', '').replace('    ', '')

    # 用正则表达式将上一步获得的文本形式的数据转化为列表
    data_list = re.findall(r'{(.+?)}', r)
    # 第一个数据格式不太对，稍微修改一下
    data_list[0] = data_list[0][len(r'"name":"FearandGreedIndex",'
                                    r'"data":[{'):]

    # 把列表中的信息放到dataframe里
    df = pd.DataFrame()
    for data in data_list:
        index = len(df)
        try:
            df.at[index, 'timestamp'] = int((re.findall('timestamp":"(.+?)"', data)[0]))
            df.at[index, 'value'] = re.findall('value":"(.+?)"', data)[0]
            df.at[index, 'strtime'] = datetime.\
                utcfromtimestamp((df.at[index, 'timestamp'])).\
                strftime("%Y-%m-%d") + ' 12:00:00'
        except:
            print('skip!')

    df.sort_values(by='timestamp', ascending=True, inplace=True)
    df.rename(columns={'strtime': 'candle_begin_time', 'value': 'fgi'}, inplace=True)
    df = df[['candle_begin_time', 'fgi']]
    df['candle_begin_time'] = pd.to_datetime(df['candle_begin_time'])
    df['fgi'].astype(int)
    return df


df = get_FG_index()
df.reset_index(inplace=True, drop=True)
print(df)
