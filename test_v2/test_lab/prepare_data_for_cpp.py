import pandas as pd

pd.set_option('expand_frame_repr', False)



for symbol in ['BTCUSD', 'ETHUSD', 'EOSUSD', 'LTCUSD', 'BCHUSD', 'XRPUSD', 'BSVUSD']:

    sql = 'select * from "cta_test"."%s"' % symbol
    df = pd.read_sql_query(sql, con=eg)
    df = df[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()
    sql_df = df.set_index('candle_begin_time')
    print(sql_df)

    sql_df.to_sql('%s_5T' % symbol, con=eg, schema='cpp', if_exists='replace')
    for freq in ['15T', '30T', '1H']:
        freq_df = df.resample(rule=freq, on='candle_begin_time', base=0, label='left', closed='left').agg(
                {'open': 'first',
                 'high': 'max',
                 'low': 'min',
                 'close': 'last',
                 'volume': 'sum',
                 })
        freq_df.reset_index(inplace=True)
        sql_df = freq_df[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()
        sql_df.set_index('candle_begin_time', inplace=True)
        print(sql_df)

        df.to_sql('%s_%s' % (symbol, freq), con=eg, schema='cpp', if_exists='replace')
        print('%s_%s' % (symbol, freq))
