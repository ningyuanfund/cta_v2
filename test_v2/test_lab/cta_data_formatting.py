import pandas as pd
from sqlalchemy import create_engine
from datetime import datetime, timedelta
pd.set_option('expand_frame_repr', False)

eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')


# 生成时间列，用于遍历数据（独立于数据）
def get_time(start, end):

    end_time = datetime.strptime(end, "%Y-%m-%d %H:%M:%S")
    now_time = datetime.strptime(start, "%Y-%m-%d %H:%M:%S")

    time_list = [now_time]
    while now_time < end_time:
        now_time += timedelta(minutes=5)
        time_list.append(now_time)
    return time_list


symbol = 'LTCUSD'
sql = 'select * from "cta_test"."%s"' % symbol
df = pd.read_sql_query(sql, con=eg)

tm_list = get_time('2017-01-01 00:20:00', '2019-06-29 23:55:00')
tm_df = pd.DataFrame(tm_list)
tm_df.rename(columns={0: 'candle_begin_time'}, inplace=True)

data = df.merge(tm_df, how='outer', on='candle_begin_time')
data.sort_values(by='candle_begin_time', inplace=True)
data['open'].fillna(method='ffill', inplace=True)
data['high'].fillna(method='ffill', inplace=True)
data['low'].fillna(method='ffill', inplace=True)
data['close'].fillna(method='ffill', inplace=True)

data['volume'].fillna(value=0, inplace=True)
data.reset_index(inplace=True, drop=True)
data = data[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()
