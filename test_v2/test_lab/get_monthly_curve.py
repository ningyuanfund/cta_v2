import pandas as pd
from matplotlib import pyplot as plt

df = pd.read_csv(r'E:\cta_v2\data\test_v2\output\bolling\ETHUSD\local_data_2cta_test\equity_curve.csv')
df['candle_begin_time'] = pd.to_datetime(df['candle_begin_time'])

df['month'] = df['candle_begin_time'].dt.month

fig = plt.figure(figsize=[14, 7])

for i in range(7):

    draw_df = df[df['month'] == i + 1].copy()
    draw_df.reset_index(inplace=True, drop=True)
    plt.subplot(2, 4, i + 1)
    draw_df['cur'] = draw_df['close'] / draw_df['close'].iloc[0]
    draw_df['eq'] = draw_df['equity_curve'] / draw_df['equity_curve'].iloc[0]
    cur, = plt.plot(draw_df['cur'])

    eq, = plt.plot(draw_df['eq'])
    plt.title('month %s' % (i + 1))
    plt.legend([cur, eq], ['ETH', 'eq'])
plt.show()
