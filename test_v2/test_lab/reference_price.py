import pandas as pd
from matplotlib import pyplot as plt
from sqlalchemy import create_engine

pd.set_option('expand_frame_repr', False)
eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')

sql = 'select * from "cta_test"."BTCUSD"'
data = pd.read_sql_query(sql, con=eg)
data['candle_begin_time'] = pd.to_datetime(data['candle_begin_time'])
data = data[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()
data['t'] = data['close'] * data['volume']
data['tt'] = data['t'].rolling(1200).sum()
data['vol'] = data['volume'].rolling(1200).sum()
data['avg_price'] = data['tt'] / data['vol']

plt.plot(data['close'])
plt.plot(data['avg_price'])
plt.show()
# print(data)
