import pandas as pd
from sqlalchemy import create_engine
from datetime import datetime, timedelta

pd.set_option('expand_frame_repr', False)
eg = create_engine('postgresql://postgres:thomas@47.75.180.89:5432/bfx_alldata')

btc_earlier = pd.read_csv(r'E:\data\bitfinex\BTCUSD_sum.csv')
btc_earlier = btc_earlier[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()

# 获取数据库内所有表名
sql = "select tablename from pg_tables where schemaname='public'"
table_df = pd.read_sql(sql, con=eg)
table_df['symbol'] = table_df['tablename'].str[9:16]


# 从服务器上获取数据，temp为该数据的表名df，返回拼接好的数据
def sort_df(temp):
    print(temp)
    df_list = []
    for i in range(len(temp)):
        table_name = temp['tablename'].at[i]
        date = table_name.split('_')[-1]
        year = int(date[:4])
        month = int(date[5:7])
        day = int(date[8:])

        if (year >= 2019) and (month >= 7)and(day >= 5):
            print(table_name)
            sql_cmd = 'select * from "%s"' % table_name
            df = pd.read_sql(sql_cmd, con=eg)
            df_list.append(df)

    df_summed = pd.concat(df_list)
    df_summed.drop_duplicates(inplace=True)
    df_summed.sort_values(by='candle_begin_time')
    df_summed.reset_index(drop=True, inplace=True)

    return df_summed


# 生成时间列，用于遍历数据（独立于数据）
def get_time(start, end):

    end_time = datetime.strptime(end, "%Y-%m-%d %H:%M:%S")
    now_time = datetime.strptime(start, "%Y-%m-%d %H:%M:%S")
    # end_time = end
    # now_time = start

    time_list = [now_time]
    while now_time < end_time:
        now_time += timedelta(minutes=5)
        time_list.append(now_time)
    return time_list


# 补齐缺失数据
def data_refill(df, begin, end):

    tm_list = get_time(begin, end)
    tm_df = pd.DataFrame(tm_list)
    tm_df.rename(columns={0: 'candle_begin_time'}, inplace=True)

    data = df.merge(tm_df, how='outer', on='candle_begin_time')
    data.sort_values(by='candle_begin_time', inplace=True)
    data['open'].fillna(method='ffill', inplace=True)
    data['high'].fillna(method='ffill', inplace=True)
    data['low'].fillna(method='ffill', inplace=True)
    data['close'].fillna(method='ffill', inplace=True)

    data['volume'].fillna(value=0, inplace=True)
    data.reset_index(inplace=True, drop=True)
    data = data[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()
    return data


df_temp = table_df[table_df['symbol'] == 'BTC/USD'].copy()
df_temp.reset_index(inplace=True, drop=True)
df_sum = sort_df(df_temp)

# resample 数据，保存。和earlier合并
df_ori = pd.concat([btc_earlier, df_sum])
df_ori['candle_begin_time'] = pd.to_datetime(df_ori['candle_begin_time'])
df_ori.drop_duplicates(subset='candle_begin_time', keep='last', inplace=True)
df_ori.sort_values(by='candle_begin_time', inplace=True)
df_ori.reset_index(inplace=True, drop=True)

begin = df_ori['candle_begin_time'].iloc[0].strftime("%Y-%m-%d %H:%M:%S")
end = df_ori['candle_begin_time'].iloc[-1].strftime("%Y-%m-%d %H:%M:%S")
df_ori_refilled = data_refill(df_ori, begin, end)
print(df_ori_refilled)
df_ori_refilled.to_csv(r'E:\data\bitfinex\BTC_since2013_refilled.csv')
