import pandas as pd
from sqlalchemy import create_engine
from matplotlib import pyplot as plt

pd.set_option('expand_frame_repr', False)


def rolling_standardize(window, pre_series, data):
    post_series = 'de_%s' % pre_series

    # rolling standardize
    data['mean'] = data[pre_series].rolling(window).mean()
    data['std'] = data[pre_series].rolling(window).std(ddof=1)

    data[post_series] = (data[pre_series] - data['mean']) / data['std']
    return data


eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')
sql = 'select * from "cta_test"."BTCUSD"'
data = pd.read_sql_query(sql, con=eg)
data['candle_begin_time'] = pd.to_datetime(data['candle_begin_time'])
data = rolling_standardize(12 * 24, 'close', data)

data['date'] = data['candle_begin_time'].dt.date
t = data[['date', 'close']].groupby('date').apply(lambda x: x['close'].std(ddof=1))
v_data = t.reset_index()
v_data['mid'] = v_data[0].rolling(window=20).mean()
v_data['std'] = v_data[0].rolling(window=20).std(ddof=2)
v_data['upper'] = v_data['mid'] + 1 * v_data['std']
v_data['lower'] = v_data['mid'] - 1 * v_data['std']

v_data['fast'] = v_data[0].rolling(window=5).mean()
v_data['slow'] = v_data[0].rolling(window=20).mean()
plt.plot(v_data[[0, 'fast', 'slow']])
plt.show()
