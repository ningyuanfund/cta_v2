import pandas as pd

pd.set_option('expand_frame_repr', False)

df_1 = pd.read_hdf(r'E:\cta_v2\data\test_v2\input\BTCUSD.h5', key='5T')
df_1 = df_1[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()

df_2 = pd.read_hdf(r'E:\cta_v2\data\input\BTCUSD.h5', key='5T')
df_2 = df_2[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()

data = pd.concat([df_1, df_2])

data.drop_duplicates(inplace=True)
data.reset_index(inplace=True, drop=True)
data.sort_values(by='candle_begin_time', inplace=True)
data['candle_begin_time'] = pd.to_datetime(data['candle_begin_time'])

data_path = r'E:\cta_v2\data\test_v2\input\BTCUSD.h5'

# data.to_csv(r'C:\Users\sycam\Desktop\BTCUSD.csv')
# exit()
data.to_hdf(data_path, key='5T')


time_interval_list = ['15T', '30T', '1H', '1D']
for time_interval in time_interval_list:
    period_df = data.resample(rule=time_interval, on='candle_begin_time', base=0, label='left', closed='left').agg(
        {'open': 'first',
         'high': 'max',
         'low': 'min',
         'close': 'last',
         'volume': 'sum',
         })
    period_df.dropna(subset=['open'], inplace=True)
    period_df = period_df[period_df['volume'] > 0]

    period_df = period_df[['open', 'high', 'low', 'close', 'volume']]
    period_df.reset_index(inplace=True)
    period_df.to_hdf(data_path, key=time_interval)
