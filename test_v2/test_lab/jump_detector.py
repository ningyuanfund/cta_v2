import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sqlalchemy import create_engine

pd.set_option('expand_frame_repr', False)

eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')

sql = 'select * from "cta_test"."BTCUSD"'
data = pd.read_sql_query(sql, con=eg)
data['candle_begin_time'] = pd.to_datetime(data['candle_begin_time'])
data.reset_index(drop=True, inplace=True)

# data = data.resample(on='candle_begin_time', rule='15T').agg(
#     {
#         'open': 'first',
#         'high': 'max',
#         'low': 'min',
#         'close': 'last',
#         'volume': 'sum'
#     }
# )

window = 12 * 24
jump_len = 2
lable = 0.1
std_factor = 2

data['rtn'] = data['close'] / data['open'] - 1
data['std'] = data['rtn'].rolling(window).std(ddof=1)
data['jump'] = 0

# data = data[-10000:].copy()
# data.reset_index(inplace=True, drop=True)

data['std'] = data['rtn'].rolling(window).std(ddof=1)
data['sum_rtn'] = data['close'] / data['close'].shift(jump_len) - 1
cond1 = data['sum_rtn'] >= std_factor * data['std'].shift(jump_len)
cond2 = data['sum_rtn'] >= 0.03

data.loc[cond1 & cond2, 'jump'] = lable

# data['jump'] = data['rtn'].rolling(window + jump_len).\
#     apply(lambda x: lable if abs(sum(x[-jump_len:]) >= std_factor * np.std(x[:-jump_len])) and abs(sum(x[-jump_len:])) >= 0.015 else 0)

data['single_jump'] = 0
data.loc[data['rtn'].apply(lambda x: abs(x)) >= 0.04, 'single_jump'] = lable * 0.5

data['jump'] *= data['rtn'].apply(lambda x: 1 if x > 0 else -1)
data['single_jump'] *= data['rtn'].apply(lambda x: 1 if x > 0 else -1)
data.to_csv()

plot_data = data
plot_data['log_price'] = plot_data['close'].apply(lambda x: np.log(x))


for i in range(0, 291898, 5000):
    print(i)
    # === PLOT DATA
    plot_data = data[i: i + 5000].copy()
    plot_data['log_price'] = plot_data['close'].apply(lambda x: np.log(x))
    plot_data.set_index('candle_begin_time', inplace=True)
    fig, ax = plt.subplots(figsize=(50, 10))
    ax.plot(plot_data['close'], color='blue')
    ax2 = ax.twinx()
    ax2.plot(plot_data['jump'], color='red', alpha=0.5)
    ax2.plot(plot_data['single_jump'], color='green', alpha=0.5)
    ax2.plot(plot_data['std'], color='orange', alpha=0.8)
    plt.title(i)
    plt.savefig(r'E:\jump_fig\{0}.png'.format(i))
    plt.close()

# === SUMMARY
# jump_data = data.loc[data['jump'] == lable]
# jump_data.reset_index(drop=True, inplace=True)
# print(jump_data)
# jump_data['candle_begin_time'] = pd.to_datetime(jump_data['candle_begin_time'])
# jump_data['gap'] = jump_data['candle_begin_time'] - jump_data['candle_begin_time'].shift(1)
# jump_data = jump_data[1:].copy()
# jump_data['gap'] = jump_data['gap'].apply(lambda x: x.seconds) / (60 * 60)
# jump_data = jump_data.loc[jump_data['gap'] > 5 / 60]
#
# plt.hist(x=jump_data['gap'], bins=50)
# plt.show()
# jump_data.to_csv('jump_summary.csv')

# === FUTURE DIST
# for i in [1, 6, 12, 24, 48, 144, 288]:
#     data['future_rtn {0}'.format(i * 5)] = data['close'].shift(-i) / data['close'] - 1
#
# up_jump = data.loc[data['jump'] > 0].copy()
# for i in [1, 6, 12, 24, 48, 144, 288]:
#     plt.hist(x=up_jump['future_rtn {0}'.format(i * 5)], bins=50)
#     plt.title(i * 5 / 60)
#     plt.show()

print(data)
