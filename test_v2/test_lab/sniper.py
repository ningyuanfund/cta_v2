import ccxt
import pandas as pd

pd.set_option('expand_frame_repr', False)


class Sniper:
    def __init__(self, api_key, secret):
        self.api_key = api_key
        self.secret = secret
        self.exchange = self._init_exchange(
            api_key=api_key,
            secret=secret
        )

    @staticmethod
    def _init_exchange(api_key, secret):
        exchange = ccxt.bitfinex(
            {
                'apiKey': api_key,
                'secret': secret,
            }
        )
        exchange.load_markets()
        return exchange

    def _get_cash(self):
        balance = self.exchange.fetch_balance()
        for item in balance['info']:
            if (item['type'] == 'trading') & (item['currency'] == 'usd'):
                return float(item['amount'])
        return 0

    def _get_position(self):
        now_pos = self.exchange.private_post_positions()
        for pos in now_pos:
            if pos['symbol'] == 'btcusd':
                return float(pos['amount']), float(pos['pl'])
        return 0, 0

    def _get_price(self):
        price = self.exchange.fetch_ticker(symbol='BTC/USD')
        return float(price['ask'])

    def cal_open_info(self, prop):
        now_holding, now_pnl = self._get_position()
        now_cash = self._get_cash()
        now_price = self._get_price()

        should_open = (now_cash + now_pnl) * prop / now_price
        return should_open - now_holding

    def place_order(self, order_type, buy_or_sell, symbol, price, amount):
        """
        下单
        :param order_type: limit, market
        :param buy_or_sell: buy, sell
        :param symbol: 买卖品种 BTCUSD
        :param price: 当market订单的时候，price无效
        :param amount: 买卖量
        :return:
        """
        print('本次下单信息为：', order_type, buy_or_sell, symbol, price, amount)
        symbol = symbol[:3] + '/' + symbol[3:]

        for i in range(5):
            # try:
            # 限价单
            if order_type == 'limit':
                # 买
                if buy_or_sell == 'buy':
                    order_info = self.exchange.create_limit_buy_order(symbol, amount, price, {'type': 'limit'})  # 买单
                # 卖
                elif buy_or_sell == 'sell':
                    order_info = self.exchange.create_limit_sell_order(symbol, amount, price, {'type': 'limit'})  # 卖单
                else:
                    order_info = {}
            # 市价单
            elif order_type == 'market':
                # 买
                if buy_or_sell == 'buy':
                    order_info = self.exchange.create_market_buy_order(symbol, amount, {'type': 'market'})  # 买单
                # 卖
                elif buy_or_sell == 'sell':
                    order_info = self.exchange.create_market_sell_order(symbol, amount, {'type': 'market'})  # 卖单
                else:
                    order_info = {}
            else:
                order_info = {}

            print('下单成功：', order_type, buy_or_sell, symbol, price, amount)
            print('下单信息：', order_info, '\n')
            return order_info

    def shoot(self, prop):
        while True:
            try:
                shoot_amount = round(self.cal_open_info(prop=prop), 4)
                if shoot_amount > 0:
                    buy_or_sell = 'buy'
                elif shoot_amount < 0:
                    buy_or_sell = 'sell'
                else:
                    buy_or_sell = ''
                    print('NO NEED TO CHANGE')
                    exit()
                self.place_order(
                    order_type='market',
                    buy_or_sell=buy_or_sell,
                    symbol='BTCUSD',
                    price=8000,
                    amount=abs(shoot_amount)
                )
                break
            except Exception as e:
                print(e)
                raise e


if __name__ == '__main__':

    _api_key = '7q6DAaUaewJjdt8IIiac9NHGXXOhGyK8431F1G5YEmG'
    _secret = 'rbjoAoRb0Gj0bVC6LxRhunAEYiO5HU4dyag4oYT7EUR'

    sniper = Sniper(
        api_key=_api_key,
        secret=_secret
    )

    # 输入应该持仓的比例，直接开冲
    sniper.shoot(prop=0.68)

