import pandas as pd
from sqlalchemy import create_engine
pd.set_option('expand_frame_repr', False)

eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')

for symbol in ['BTCUSD', 'ETHUSD', 'EOSUSD', 'LTCUSD', 'BCHUSD', 'BSVUSD', 'XRPUSD']:
    # sql = 'select * from "cta_test"."%s"' % symbol
    # data = pd.read_sql_query(sql, con=eg)
    # data.to_csv(r'C:\Users\sycam\Desktop\%s.csv' % symbol)
    print(symbol)

    data = pd.read_csv('/home/syca/data/%s.csv' % symbol)
    data.sort_values('candle_begin_time', inplace=True)
    data.reset_index(inplace=True, drop=True)
    data = data[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()


    # data.set_index('candle_begin_time', inplace=True)

    data.to_sql(symbol, con=eg, schema='cta_test', if_exists='replace')
