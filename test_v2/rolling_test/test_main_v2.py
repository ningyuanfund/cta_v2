import os
import time
import warnings
from datetime import timedelta, datetime
from multiprocessing import Pool

import numpy as np
import pandas as pd
from dateutil.relativedelta import relativedelta
from sqlalchemy import create_engine

import ckt.test.strategy.timing_strategy.Signals as ori_sigs
from ckt.cq_sdk.path import get_data_path
from test_v2.rolling_test.equity_curve import equity_curve_with_long_and_short, find_change_time

# from numba import jit, cuda

warnings.simplefilter(action='ignore')
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)
eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')

para_eg = create_engine('postgresql://postgres:thomas@localhost/bitfinex_local')


# ===具体进行回测
def func(df_dict, strategy_name, para_freq_list, train_begin_date):
    best_para_trained = {'para': [0, 0, 0],
                         'yield': 0,
                         'maxdd': 0,
                         'annual_rtn/maxdd': 0}

    for para_freq in para_freq_list:
        freq = para_freq['freq']
        para = para_freq['para']
        all_data = df_dict[freq]

        # 计算交易信号
        function_str = 'ori_sigs.signal_%s_with_stop_lose(all_data.copy(), para)' % strategy_name
        da = eval(function_str)

        # 筛选交易时间
        signal_df = da[da['candle_begin_time'] >= train_begin_date]

        # 计算资金曲线
        df = equity_curve_with_long_and_short(signal_df, leverage_rate=3, c_rate=3.0 / 1000)
        df = df[:-1].copy()  # 最后不知道为什么会加上谜之一行，先去掉

        # 计算年化收益
        annual_return = (df['equity_curve'].iloc[-1] / df['equity_curve'].iloc[0]) ** (
            '1 days 00:00:00' / (df['candle_begin_time'].iloc[-1] - df['candle_begin_time'].iloc[0]) * 365) - 1

        # 计算最大回撤
        max_drawdown = (1 - df['equity_curve'] / df['equity_curve'].expanding().max()).max()

        # 计算年化收益回撤比
        ratio = annual_return / max_drawdown if max_drawdown != 0 else 0
        print(freq, para, '最优参数收益：', df.iloc[-1]['equity_curve'], '最优参数年化收益回撤比：', ratio)

        # 如果现在测出来的参数比之前保留的好，就更新最优参数
        if (ratio > best_para_trained['annual_rtn/maxdd']) | (best_para_trained['para'][0] == 0):
            best_para_trained['para'] = para
            best_para_trained['freq'] = freq
            best_para_trained['yield'] = df['equity_curve'].iloc[-1]
            best_para_trained['maxdd'] = max_drawdown
            best_para_trained['annual_rtn/maxdd'] = ratio

    # 存储数据（多进程模式下如果保存成dataframe后面会有重复值）
    return str(best_para_trained['freq']) + ':' + str(best_para_trained['para']) + ':' + str(best_para_trained['yield']) + ':' + str(
        best_para_trained['maxdd']) + ':' + str(best_para_trained['annual_rtn/maxdd'])


# ===生成训练期x个月内最优参数的时间点列表，方便循环回测时读取
def gen_train_date(sys_para):
    test_period = sys_para['test_period']  # 参数回测期长度
    use_period = sys_para['use_period']  # 参数使用期长度
    begin_date = pd.to_datetime(sys_para['begin_date'])  # 整体回测开始时间（训练集的最早时间）

    # 回测的时间点列表
    train_date_list = []
    while begin_date <= pd.to_datetime(sys_para['end_date']) - timedelta(weeks=test_period):

        # 生成训练开始时间、训练结束时间、测试开始时间
        train_date_list.append({'train_begin_date': begin_date,
                               'train_end_date': begin_date + timedelta(weeks=test_period),
                                'test_end_date': begin_date + timedelta(weeks=test_period) + timedelta(weeks=use_period),
                                })
        begin_date += timedelta(weeks=use_period)

    # 如果每轮回测都要使用所有历史数据
    if sys_para['if_all_data']:
        for date_info in train_date_list:
            date_info['train_begin_date'] = pd.to_datetime(sys_para['begin_date'])

    return train_date_list


# ===从数据库中获取数据
def get_data(symbol, time_interval, end_time, begin, data_schema):
    if time_interval == '15T':
        trans_f = 3
    elif time_interval == '30T':
        trans_f = 6
    else:
        trans_f = 12

    minute_gap = (500 + 500) * trans_f * 5
    # end_time = datetime.strptime(end, "%Y-%m-%d 00:00:00")
    begin = datetime.strftime(begin - timedelta(minutes=minute_gap),
                              "%Y-%m-%d %H:%M:%S")
    begin_time = datetime.strptime(begin, "%Y-%m-%d %H:%M:%S")

    sql = 'select * from "%s"."%s" where candle_begin_time >= \'%s\' and candle_begin_time < \'%s\'' % \
          (data_schema, symbol, begin_time, end_time)
    df = pd.read_sql_query(sql, con=eg)
    data = df.resample(rule=time_interval, on='candle_begin_time', base=0,
                       label='left',
                       closed='left').agg(
        {'open': 'first',
         'high': 'max',
         'low': 'min',
         'close': 'last',
         'volume': 'sum',
         }
    )
    data.reset_index(inplace=True)
    return data


# ===对回测期的数据进行回测，返回最优参数
def train(pair, strategy_name, para_list, train_info, rtn_path, process, train_begin_date, train_end_date, sys_para, data_schema):
    df_dict = {}
    for freq in sys_para['freq']:
        df_dict[freq] = get_data(pair, freq, train_end_date, train_begin_date, data_schema)

    # 参数的附上freq
    para_freq_list = []
    for para in para_list:
        for freq in sys_para['freq']:
            para_freq_list.append({'freq': freq, 'para': para})

    split_list = []  # 分成process组的待测参数
    # 待测参数分片
    for i in range(process):
        if i < process:
            split_para = para_freq_list[i * int(len(para_freq_list) / process):
                                        (i+1) * int(len(para_freq_list) / process)]
        else:
            split_para = para_freq_list[i * int(len(para_freq_list) / process):]
        split_list.append(split_para)

    pool = Pool(processes=process)  # 开辟进程池
    result = []
    for split_para_list in split_list:
        result.append(pool.apply_async(func, (df_dict, strategy_name, split_para_list, train_begin_date, )))

    pool.close()  # 关闭进程池
    pool.join()  # 等待开辟的所有进程执行完后，主进程才继续往下执行

    A = pd.DataFrame()
    for i, res in enumerate(result):
        A.loc[i, '参数'] = res.get().split(':')[1]
        A.loc[i, '时间级别'] = res.get().split(':')[0]
        A.loc[i, '策略收益'] = float(res.get().split(':')[2])
        A.loc[i, '最大回撤'] = float(res.get().split(':')[3])
        A.loc[i, '年化收益/最大回撤'] = float(res.get().split(':')[4])

    temp = A['参数'].str[1:-1].str.split(',', expand=True)
    rtn = pd.concat([temp, A.iloc[:, 1:]], axis=1)
    print(rtn)
    rtn.columns = para_names
    rtn_path = rtn_path + '/para_record'

    if not os.path.exists(rtn_path):
        os.makedirs(rtn_path)

    rtn.to_csv(rtn_path + '/%s_%s.csv' % (localtime, train_info['train_end_date'].strftime('%Y-%m')),
               encoding='utf_8_sig')  # 用于保存完整的参数排名结果

    # 最优参数转换回列表格式
    top_para_str = A.sort_values(by='年化收益/最大回撤', ascending=False)[:1]['参数'].values[0][1:-1].split(',')
    top_para_freq = A.sort_values(by='年化收益/最大回撤', ascending=False)[:1]['时间级别'].values[0]

    top_para = {'para': [], 'freq': top_para_freq}
    for para in top_para_str:
        top_para['para'].append(float(para))

    return top_para


# ===主程序
def main(pair, strategy_name, para_list, para_names, sys_para, process, data_schema):

    # 设置路径
    if sys_para['if_all_data']:
        data_para = 'all_data'
    else:
        data_para = 'local_data'

    # 设定路径，如果没有这个路径的话就生成
    path = get_data_path('test_v2', 'output', strategy_name, '%s' % pair)
    if not os.path.exists(path):
        os.makedirs(path)

    # 用于保存完整的参数排名结果
    rtn_path = get_data_path('test_v2', 'output', strategy_name, '%s' % pair,
                             data_para + '_' + str(sys_para['test_period']) + '_' + str(sys_para['use_period']) + data_schema)

    sv_name = save_name + pair
    for roots, dirs, files in os.walk(rtn_path):
        print(rtn_path)
        if files:
            for file in files:
                if file.startswith(sv_name):
                    raise Exception('存档已存在！')

    if not os.path.exists(rtn_path):
        os.makedirs(rtn_path)

    train_list = gen_train_date(sys_para)

    # 回测出每个月月初往前数12个月的最优参数
    top_para_record = {'参数训练开始': [],
                       '参数训练结束': [],
                       '参数使用至': [],
                       '参数': [],
                       '时间周期': []}

    # 开始训练
    for train_info in train_list:
        print('本轮训练开始时间为', train_info['train_begin_date'])
        print('本轮训练结束时间为', train_info['train_end_date'])
        print('本轮参数使用至', train_info['test_end_date'])

        train_begin_date = train_info['train_begin_date']
        train_end_date = train_info['train_end_date']

        # 回测计算最优参数，在单一阶段的训练中使用多进程，切换阶段时还是要阻塞
        # 减少阶段内的IO，加快速度，进程计算完成后再比较process个最优参数哪个好，返回最优的top_para
        top_para = train(pair, strategy_name, para_list, train_info, rtn_path, process, train_begin_date,
                         train_end_date, sys_para, data_schema)
        train_info['best_para_trained'] = top_para
        print('寻找到的最优参数为', top_para)

        # 最优参数保存在list里，最终生成 dataframe
        top_para_record['参数训练开始'].append(train_info['train_begin_date'])
        top_para_record['参数训练结束'].append(train_info['train_end_date'])
        top_para_record['参数使用至'].append(train_info['test_end_date'])
        top_para_record['参数'].append(top_para['para'])
        top_para_record['时间周期'].append(top_para['freq'])

        print('*' * 20)

    # 输出每个阶段的最优参数
    top_para_df = pd.DataFrame(top_para_record)
    top_para_df.to_csv(rtn_path + r'/%s_%s_top_para_record.csv' % (sv_name, localtime), encoding='utf_8_sig')
    table_name = 'long_%s_%s_stop_para_record' % (pair[:-3], strategy_name)
    try:
        top_para_df.to_sql(table_name, con=para_eg, schema='params', if_exists='replace')
    except Exception as e:
        print(e)

    # # 数据全集resample
    # for i in range(len(top_para_df)):
    #     best_para_set = {}
    #     best_para_set['para'] = top_para_df['参数'].iloc[i]
    #     # [float(x) for x in top_para_df['参数'].iloc[i][1: -1].split(',')]
    #     best_para_set['freq'] = top_para_df['时间周期'].iloc[i]
    #     train_list[i]['best_para_trained'] = best_para_set
    #
    # freq_all_data = {}
    # sql = 'select * from "%s"."%s"' % (data_schema, pair)
    # all_data = pd.read_sql_query(sql, con=eg)
    #
    # for freq in sys_para['freq']:
    #     data = all_data.resample(rule=freq, on='candle_begin_time', base=0, label='left', closed='left').agg(
    #         {'open': 'first',
    #          'high': 'max',
    #          'low': 'min',
    #          'close': 'last',
    #          'volume': 'sum',
    #          }
    #     )
    #     data.reset_index(inplace=True)
    #     freq_all_data[freq] = data
    #
    # # 根据最优参数计算每个月初往后的开仓、平仓、止损点
    # trade_record = []
    # last_trade_end_time = pd.to_datetime(sys_para['begin_date']) + timedelta(weeks=sys_para['test_period'])
    # print('INIT last trade end date', last_trade_end_time)
    # for train_info in train_list:
    #     print('THEORETIC USAGE END', train_info['test_end_date'])
    #     para = train_info['best_para_trained']['para']  # 每个月应该使用的最优参数
    #     freq = train_info['best_para_trained']['freq']
    #
    #     # 用该月的最优参数进行使用，找到使用期的开仓、平仓、止损点
    #     monthly_trade_record = find_change_time(freq_all_data[freq], para, strategy_name, last_trade_end_time,
    #                                             train_info['test_end_date'], freq)
    #
    #     for trade in monthly_trade_record:
    #         if (trade['trade_begin_time'] > last_trade_end_time) & (trade['trade_begin_time'] < train_info['test_end_date']):
    #             trade_record.append(trade)
    #     if trade_record:
    #         last_trade_end_time = max(trade_record[-1]['trade_end_time'], train_info['test_end_date'])
    #     else:
    #         last_trade_end_time = train_info['test_end_date']
    #
    #     print('TRUE END', last_trade_end_time)
    #     print(trade_record[-1])
    #     print(para, freq)
    #
    #     print('*' * 30)
    # print(trade_record)
    # # 根据交易记录生成信号df
    # trade_df_list = []
    # for trade in trade_record:
    #     # 开仓信号记录方向
    #     trade_df_list.append([trade['trade_begin_time'], trade['long_or_short']])
    #     # 平仓信号记录为0
    #     trade_df_list.append([trade['trade_end_time'], 0])
    # trade_df = pd.DataFrame(trade_df_list, columns=['candle_begin_time', 'signal'])
    #
    # trade_df.sort_values(by='candle_begin_time', inplace=True)
    # trade_df.drop_duplicates(subset='candle_begin_time', keep='last', inplace=True)
    #
    # # 输出交易记录
    # trade_df.to_csv(rtn_path + r'/%s_%s_trade.csv' % (save_name, localtime))
    #
    # # 将信号df与原始数据合并
    # all_data = all_data.merge(trade_df, how='outer', on='candle_begin_time')
    #
    # # 去除不可交易时间的信号
    # condition1 = all_data['volume'] < 0.1
    # condition2 = abs(all_data['signal']) > 0
    # all_data.loc[condition1 & condition2, 'signal'] = 0
    #
    # # 生成pos，后续计算资金曲线
    # all_data['pos'] = all_data['signal'].shift(1)
    # all_data['pos'].fillna(method='ffill', inplace=True)
    # all_data['pos'].fillna(value=0, inplace=True)  # 将初始行数的position补全为0
    #
    # # 计算资金曲线
    # df = equity_curve_with_long_and_short(all_data, leverage_rate=1, c_rate=3.0 / 1000)
    # print('最终收益为', df.iloc[-1]['equity_curve'])
    # df = df[df['candle_begin_time'] >= pd.to_datetime(sys_para['begin_date'])]
    # df.reset_index(inplace=True, drop=True)
    #
    # df.to_csv(rtn_path + r'/%s_%s_equity_curve.csv' % (save_name, localtime))


if __name__ == '__main__':
    localtime = int(time.time())
    # 策略名称
    strategy_name = 'bolling'

    # 这边需要手动输入参数名称
    para_names = ['n', 'm', '止损比例', '时间级别', '策略收益', '最大回撤', '年化收益/最大回撤']

    process = 4  # 最大进程数

    data_schema = 'cta_test'

    save_name = 'sql_para_btc_bolling_2019_'

    # 回测系统参数
    sys_para = {'test_period': 2,  # 训练期长度，周
                'use_period': 2,  # 使用期长度，周
                'begin_date': '2019-01-01',  # 整体回测开始的时间，即使用数据的最早时间，如果早于数据最早的时间，则会自动使用数据最早的时间
                'end_date': '2019-11-23',
                'if_all_data': False,  # 如果为True，每轮回测都将使用所有历史数据，但至少一年
                'freq': ['15T', '30T', '1H']
                }

    # 待测参数组合。每次回测都相同
    n_list = range(40, 300, 20)
    m_list = [i / 2 for i in range(4, 11)]
    stop_lose_pct_list = range(4, 11, 2)

    # 生成参数列表
    para_list = []
    for n in n_list:
        for m in m_list:
            for stop_lose_pct in stop_lose_pct_list:
                para_list.append([n, m, stop_lose_pct])

    for pair in [
        'BTCUSD',
        # 'ETHUSD',
        # 'EOSUSD',
        # 'LTCUSD'
                 ]:  # 币种
        print('\n====%s====' % pair)
        main(pair, strategy_name, para_list, para_names, sys_para, process, data_schema)
