import pandas as pd
from datetime import timedelta
import numpy as np
import test_v2.rolling_test.Signals as sigs

pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1500)


# ===画资金曲线
def equity_curve_with_long_and_short(df, leverage_rate=3, c_rate=3.0/1000, min_margin_rate=0.15):
    """

    :param df:  带有signal和pos的原始数据
    :param leverage_rate:  bfx交易所最多提供3倍杠杆，leverage_rate可以在(0, 3]区间选择
    :param c_rate:  手续费
    :param min_margin_rate:  低保证金比例，必须占到借来资产的15%
    :return:
    """

    # =====基本参数
    init_cash = 100  # 初始资金
    min_margin = init_cash * leverage_rate * min_margin_rate  # 最低保证金

    # =====根据pos计算资金曲线
    # ===计算涨跌幅
    df['change'] = df['close'].pct_change(1)  # 根据收盘价计算涨跌幅
    df['buy_at_open_change'] = df['close'] / df['open'] - 1  # 从今天开盘买入，到今天收盘的涨跌幅
    df['sell_next_open_change'] = df['open'].shift(-1) / df['close'] - 1  # 从今天收盘到明天开盘的涨跌幅
    df.at[len(df) - 1, 'sell_next_open_change'] = 0

    # ===选取开仓、平仓条件
    condition1 = df['pos'] != 0
    condition2 = df['pos'] != df['pos'].shift(1)
    open_pos_condition = condition1 & condition2

    condition1 = df['pos'] != 0
    condition2 = df['pos'] != df['pos'].shift(-1)
    close_pos_condition = condition1 & condition2

    # ===对每次交易进行分组
    df.loc[open_pos_condition, 'start_time'] = df['candle_begin_time']
    df['start_time'].fillna(method='ffill', inplace=True)
    df.loc[df['pos'] == 0, 'start_time'] = pd.NaT

    # ===计算仓位变动
    # 开仓时仓位
    df.loc[open_pos_condition, 'position'] = init_cash * leverage_rate * (1 + df['buy_at_open_change'])  # 建仓后的仓位

    # 开仓后每天的仓位的变动
    group_num = len(df.groupby('start_time'))

    if group_num > 1:
        t = df.groupby('start_time').apply(lambda x: x['close'] / x.iloc[0]['close'] * x.iloc[0]['position'])
        t = t.reset_index(level=[0])
        df['position'] = t['close']
    elif group_num == 1:
        t = df.groupby('start_time')[['close', 'position']].apply(lambda x: x['close'] / x.iloc[0]['close'] * x.iloc[0]['position'])
        df['position'] = t.T.iloc[:, 0]

    # 每根K线仓位的最大值和最小值，针对最高价和最低价
    df['position_max'] = df['position'] * df['high'] / df['close']
    df['position_min'] = df['position'] * df['low'] / df['close']

    # 平仓时仓位
    df.loc[close_pos_condition, 'position'] *= (1 + df.loc[close_pos_condition, 'sell_next_open_change'])

    # ===计算每天实际持有资金的变化
    # 计算持仓利润
    df['profit'] = (df['position'] - init_cash * leverage_rate) * df['pos']  # 持仓盈利或者损失

    # 计算持仓利润最小值
    df.loc[df['pos'] == 1, 'profit_min'] = (df['position_min'] - init_cash * leverage_rate) * df['pos']  # 最小持仓盈利或者损失
    df.loc[df['pos'] == -1, 'profit_min'] = (df['position_max'] - init_cash * leverage_rate) * df['pos']  # 最小持仓盈利或者损失

    # 计算实际资金量
    df['cash'] = init_cash + df['profit']  # 实际资金
    df['cash'] -= init_cash * leverage_rate * c_rate  # 减去建仓时的手续费
    df['cash_min'] = df['cash'] - (df['profit'] - df['profit_min'])  # 实际最小资金
    df.loc[df['cash_min'] < 0, 'cash_min'] = 0  # 如果有小于0，直接设置为0
    df.loc[close_pos_condition, 'cash'] -= df.loc[close_pos_condition, 'position'] * c_rate  # 减去平仓时的手续费

    # ===判断是否会爆仓
    _index = df[df['cash_min'] <= min_margin].index

    if len(_index) > 0:
        print('有爆仓')

        df.loc[_index, '强平'] = 1
        df['强平'] = df.groupby('start_time')['强平'].fillna(method='ffill')
        df.loc[(df['强平'] == 1) & (df['强平'].shift(1) != 1), 'cash_强平'] = df['cash_min']  # 此处是有问题的
        df.loc[(df['pos'] != 0) & (df['强平'] == 1), 'cash'] = None
        df['cash'].fillna(value=df['cash_强平'], inplace=True)
        df['cash'] = df.groupby('start_time')['cash'].fillna(method='ffill')
        df.drop(['强平', 'cash_强平'], axis=1, inplace=True)  # 删除不必要的数据

    # ===计算资金曲线
    df['equity_change'] = df['cash'].pct_change()
    df.loc[open_pos_condition, 'equity_change'] = df.loc[open_pos_condition, 'cash'] / init_cash - 1  # 开仓日的收益率
    df['equity_change'].fillna(value=0, inplace=True)
    df['equity_curve'] = (1 + df['equity_change']).cumprod()

    # ===判断资金曲线是否有负值，有的话后面都置成0
    if len(df[df['equity_curve'] < 0]) > 0:
        neg_start = df[df['equity_curve'] < 0].index[0]
        df.loc[neg_start:, 'equity_curve'] = 0

    # ===删除不必要的数据
    df.drop(['change', 'buy_at_open_change', 'sell_next_open_change', 'position', 'position_max',
             'position_min', 'profit', 'profit_min', 'cash', 'cash_min'], axis=1, inplace=True)

    return df


# ===找开仓平仓点
def find_change_time(df, para, strategy_name, start_date, end_date, freq):
    """

    :param df:  带有signal和pos的原始数据
    :param leverage_rate:  bfx交易所最多提供3倍杠杆，leverage_rate可以在(0, 3]区间选择
    :param c_rate:  手续费
    :param min_margin_rate:  低保证金比例，必须占到借来资产的15%
    :return:
    """
    if freq == '15T':
        time_minus = timedelta(minutes=15)
    elif freq == '30T':
        time_minus = timedelta(minutes=30)
    elif freq == '1H':
        time_minus = timedelta(minutes=60)
    elif freq == '1D':
        time_minus = timedelta(days=1)

    # ===计算信号，计算完信号直接截取K线，防止路径依赖
    function_str = 'sigs.signal_%s_with_stop_lose(df.copy(), para, start_date)' % strategy_name

    df = eval(function_str)

    # ===选取开仓、平仓条件
    condition1 = df['pos'] != 0
    condition2 = df['pos'] != df['pos'].shift(1)
    open_pos_condition = condition1 & condition2

    # ===对每次交易进行分组
    df.loc[open_pos_condition, 'start_time'] = df['candle_begin_time']
    df['start_time'].fillna(method='ffill', inplace=True)
    df.loc[df['pos'] == 0, 'start_time'] = pd.NaT

    trade_record = []  # 找到这个月往后的交易记录

    def f1(x):
        # 筛选开始时间在本阶段内的交易
        condition2 = x['candle_begin_time'].iloc[0] < end_date

        # condition = x['candle_begin_time'].iloc[0].strftime('%Y-%m') == start_date.strftime('%Y-%m')
        if condition2:
            # 需要得到信号发出的时间而不是开仓的时间，所以此处的开仓时间需要减掉一根k线的时间
            trade_record.append({'trade_begin_time': x.iloc[0]['candle_begin_time'] - time_minus,
                                 'trade_end_time': x.iloc[-1]['candle_begin_time'],
                                 'long_or_short': x.iloc[0]['pos'],
                                 })

    df.groupby('start_time').filter(f1)  # filter解决apply首行重复

    return trade_record


# ===检查oco和动态止盈，更新信号和价格
def check_oco_and_dynamic_stop_profit(df, all_para):
    stop_loss_pct = all_para['stop_loss_pct']  # 止损比例，没有除以100
    stop_profit_pct = all_para['stop_profit_pct']  # 止盈比例，没有除以100
    dynamic_trigger_pct = all_para['dynamic_trigger']  # 动态止盈触发比例，没有除以100
    dynamic_stop_pct = all_para['dynamic_stop_pct']  # 动态止盈回撤比例，没有除以100

    # ===防止因为没有产生signal而报错
    df['signal'] = np.nan
    df['exe_price'] = df['close']  # OCO可能会不按照收盘价执行，所以需要一列执行价格，用执行价格算资金曲线

    # ===考察是否需要止盈止损
    info_dict = {'pre_signal': 0, 'stop_loss_price': None, 'stop_profit_price': None, 'dynamic_trigger_price': None,
                 'open_price': None, 'dynamic_triggered': False}  # 用于记录之前交易信号，止盈止损价格，动态止盈触发价格

    for i in range(df.shape[0]):
        # 如果之前是空仓
        if info_dict['pre_signal'] == 0:
            # 当本周期有做多信号
            if df.at[i, 'signal_long'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 记录当前状态
                pre_signal = 1  # 信号
                stop_loss_price = df.at[i, 'close'] * (
                        1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格。
                stop_profit_price = df.at[i, 'close'] * (
                        1 + stop_profit_pct / 100)  # 以本周期的收盘价乘以一定比例作为止盈价格。
                dynamic_trigger_price = df.at[i, 'close'] * (
                        1 + dynamic_trigger_pct / 100)  # 以本周期的收盘价乘以一定比例作为动态止盈触发价格。

                info_dict = {'pre_signal': pre_signal, 'stop_loss_price': stop_loss_price,
                             'stop_profit_price': stop_profit_price, 'dynamic_trigger_price': dynamic_trigger_price,
                             'open_price': df.at[i, 'close'], 'dynamic_triggered': False}

            # 当本周期有做空信号
            elif df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_loss_price = df.at[i, 'close'] * (
                        1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                stop_profit_price = df.at[i, 'close'] * (
                        1 - stop_profit_pct / 100)  # 以本周期的收盘价乘以一定比例作为止盈价格。
                dynamic_trigger_price = df.at[i, 'close'] * (
                        1 - dynamic_trigger_pct / 100)  # 以本周期的收盘价乘以一定比例作为动态止盈触发价格。

                info_dict = {'pre_signal': pre_signal, 'stop_loss_price': stop_loss_price,
                             'stop_profit_price': stop_profit_price, 'dynamic_trigger_price': dynamic_trigger_price,
                             'open_price': df.at[i, 'close'], 'dynamic_triggered': False}
            # 无信号
            else:
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_loss_price': None, 'stop_profit_price': None,
                             'dynamic_trigger_price': None, 'open_price': None, 'dynamic_triggered': False}

        # 如果之前是多头仓位
        elif info_dict['pre_signal'] == 1:
            # 当本周期有平多仓信号
            if df.at[i, 'signal_long'] == 0:
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_loss_price': None, 'stop_profit_price': None,
                             'dynamic_trigger_price': None, 'open_price': None, 'dynamic_triggered': False}
            # 当本周期需要多头止损
            elif df.at[i, 'close'] < info_dict['stop_loss_price']:
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                df.at[i, 'exe_price'] = info_dict['stop_loss_price']  # 记录执行价格
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_loss_price': None, 'stop_profit_price': None,
                             'dynamic_trigger_price': None, 'open_price': None, 'dynamic_triggered': False}

            # 当本周期需要多头止盈
            elif df.at[i, 'close'] > info_dict['stop_profit_price']:
                if not info_dict['dynamic_triggered']:
                    print('触发止盈，但没有触发移动止盈，存在问题，需要排查。')
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                df.at[i, 'exe_price'] = info_dict['stop_profit_price']  # 记录执行价格
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_loss_price': None, 'stop_profit_price': None,
                             'dynamic_trigger_price': None, 'open_price': None, 'dynamic_triggered': False}

            # 当本周期有平多仓并且还要开空仓
            elif df.at[i, 'signal_short'] == -1:
                df.at[i, 'signal'] = -1  # 将真实信号设置为-1
                # 如果收盘价低于止损价，按照止损价平仓
                if df.at[i, 'close'] < info_dict['stop_loss_price']:
                    df.at[i, 'exe_price'] = info_dict['stop_loss_price']  # 记录执行价格
                # 记录相关信息
                pre_signal = -1  # 信号
                stop_loss_price = df.at[i, 'close'] * (
                        1 + stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                stop_profit_price = df.at[i, 'close'] * (
                        1 - stop_profit_pct / 100)  # 以本周期的收盘价乘以一定比例作为止盈价格。
                dynamic_trigger_price = df.at[i, 'close'] * (
                        1 - dynamic_trigger_pct / 100)  # 以本周期的收盘价乘以一定比例作为动态止盈触发价格。

                info_dict = {'pre_signal': pre_signal, 'stop_loss_price': stop_loss_price,
                             'stop_profit_price': stop_profit_price, 'dynamic_trigger_price': dynamic_trigger_price,
                             'open_price': df.at[i, 'close'], 'dynamic_triggered': False}

            # 如果触发了移动止盈
            if info_dict['dynamic_triggered']:
                dynamic_stop_price = info_dict['max_profit_price'] * (1 - dynamic_stop_pct)  # 计算动态止盈价格
                # 如果收盘价高于曾经的最大盈利价格（这里也可以用最高价），更新最大盈利价格
                if df.at[i, 'close'] >= info_dict['max_profit_price']:
                    info_dict['max_profit_price'] = df.at[i, 'close']
                    dynamic_stop_price = info_dict['max_profit_price'] * (1 - dynamic_stop_pct)  # 计算动态止盈价格

                # 如果触发了动态止盈，就平多
                if df.at[i, 'close'] < dynamic_stop_price:
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_loss_price': None, 'stop_profit_price': None,
                                 'dynamic_trigger_price': None, 'open_price': None, 'dynamic_triggered': False}

            # 触发移动止盈条件
            if df.at[i, 'close'] > info_dict['dynamic_trigger_price']:
                info_dict['dynamic_triggered'] = True
                info_dict['max_profit_price'] = df.at[i, 'close']

        # 如果之前是空头仓位
        elif info_dict['pre_signal'] == -1:
            # 当本周期有平空仓信号
            if df.at[i, 'signal_short'] == 0:
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_loss_price': None, 'stop_profit_price': None,
                             'dynamic_trigger_price': None, 'open_price': None, 'dynamic_triggered': False}
            # 当本周期需要空头止损
            elif df.at[i, 'close'] > info_dict['stop_loss_price']:
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                df.at[i, 'exe_price'] = info_dict['stop_loss_price']  # 记录执行价格
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_loss_price': None, 'stop_profit_price': None,
                             'dynamic_trigger_price': None, 'open_price': None, 'dynamic_triggered': False}

            # 当本周期需要空头止盈
            elif df.at[i, 'close'] < info_dict['stop_profit_price']:
                if not info_dict['dynamic_triggered']:
                    print('触发止盈，但没有触发移动止盈，存在问题，需要排查。')
                df.at[i, 'signal'] = 0  # 将真实信号设置为0
                df.at[i, 'exe_price'] = info_dict['stop_profit_price']  # 记录执行价格
                # 记录相关信息
                info_dict = {'pre_signal': 0, 'stop_loss_price': None, 'stop_profit_price': None,
                             'dynamic_trigger_price': None, 'open_price': None, 'dynamic_triggered': False}

            # 当本周期有平空仓并且还要开多仓
            elif df.at[i, 'signal_short'] == 1:
                df.at[i, 'signal'] = 1  # 将真实信号设置为1
                # 如果收盘价低于止损价，按照止损价平仓
                if df.at[i, 'close'] > info_dict['stop_loss_price']:
                    df.at[i, 'exe_price'] = info_dict['stop_loss_price']  # 记录执行价格
                # 记录相关信息
                pre_signal = 1  # 信号
                stop_loss_price = df.at[i, 'close'] * (
                        1 - stop_loss_pct / 100)  # 以本周期的收盘价乘以一定比例作为止损价格，也可以用下周期的开盘价df.at[i+1, 'open']
                stop_profit_price = df.at[i, 'close'] * (
                        1 + stop_profit_pct / 100)  # 以本周期的收盘价乘以一定比例作为止盈价格。
                dynamic_trigger_price = df.at[i, 'close'] * (
                        1 + dynamic_trigger_pct / 100)  # 以本周期的收盘价乘以一定比例作为动态止盈触发价格。

                info_dict = {'pre_signal': pre_signal, 'stop_loss_price': stop_loss_price,
                             'stop_profit_price': stop_profit_price, 'dynamic_trigger_price': dynamic_trigger_price,
                             'open_price': df.at[i, 'close'], 'dynamic_triggered': False}

            # 如果触发了移动止盈
            if info_dict['dynamic_triggered']:
                dynamic_stop_price = info_dict['max_profit_price'] * (1 + dynamic_stop_pct)  # 计算动态止盈价格
                # 如果收盘价低于曾经的最大盈利价格（这里也可以用最低价），更新最大盈利价格
                if df.at[i, 'close'] <= info_dict['max_profit_price']:
                    info_dict['max_profit_price'] = df.at[i, 'close']
                    dynamic_stop_price = info_dict['max_profit_price'] * (1 + dynamic_stop_pct)  # 计算动态止盈价格

                # 如果触发了动态止盈，就平空
                if df.at[i, 'close'] > dynamic_stop_price:
                    df.at[i, 'signal'] = 0  # 将真实信号设置为0
                    # 记录相关信息
                    info_dict = {'pre_signal': 0, 'stop_loss_price': None, 'stop_profit_price': None,
                                 'dynamic_trigger_price': None, 'open_price': None, 'dynamic_triggered': False}

            # 触发移动止盈
            if df.at[i, 'close'] < info_dict['dynamic_trigger_price']:
                info_dict['dynamic_triggered'] = True
                info_dict['max_profit_price'] = df.at[i, 'close']

        # 其他情况
        else:
            raise ValueError('不可能出现其他的情况，如果出现，说明代码逻辑有误，报错！')

    return df


# ===带OCO和动态止盈的资金曲线计算
def equity_curve_with_oco_and_dynamic_stop_profit(df, leverage_rate=3, c_rate=2.0 / 1000, min_margin_rate=0.15):
    """

    :param df:  带有signal和pos的原始数据
    :param leverage_rate:  bfx交易所最多提供3倍杠杆，leverage_rate可以在(0, 3]区间选择
    :param c_rate:  手续费
    :param min_margin_rate:  低保证金比例，必须占到借来资产的15%
    :return:
    """

    # =====基本参数
    init_cash = 100  # 初始资金
    min_margin = init_cash * leverage_rate * min_margin_rate  # 最低保证金

    # =====根据pos计算资金曲线
    # ===计算涨跌幅
    df['change'] = df['close'].pct_change(1)  # 根据收盘价计算涨跌幅
    df['buy_at_open_change'] = df['close'] / df['open'] - 1  # 从今天开盘买入，到今天收盘的涨跌幅
    df['sell_next_open_change'] = df['open'].shift(-1) / df['close'] - 1  # 从今天收盘到明天开盘的涨跌幅
    df.at[len(df) - 1, 'sell_next_open_change'] = 0

    # ===选取开仓、平仓条件
    condition1 = df['pos'] != 0
    condition2 = df['pos'] != df['pos'].shift(1)
    open_pos_condition = condition1 & condition2

    condition1 = df['pos'] != 0
    condition2 = df['pos'] != df['pos'].shift(-1)
    close_pos_condition = condition1 & condition2

    # ===对每次交易进行分组
    df.loc[open_pos_condition, 'start_time'] = df['candle_begin_time']
    df['start_time'].fillna(method='ffill', inplace=True)
    df.loc[df['pos'] == 0, 'start_time'] = pd.NaT

    # ===计算仓位变动
    # 开仓时仓位
    df.loc[open_pos_condition, 'position'] = init_cash * leverage_rate * (1 + df['buy_at_open_change'])  # 建仓后的仓位

    # 开仓后每天的仓位的变动
    group_num = len(df.groupby('start_time'))

    if group_num > 1:
        def f1(x):
            x['close'] = x['close'] / x.iloc[0]['close'] * x.iloc[0]['position']
            # 如果不是止盈或止损平仓，调整平仓时仓位
            if x.iloc[-1]['close'] == x.iloc[-1]['exe_price']:
                x.iloc[-1]['close'] *= (1 + df.iloc[-1]['sell_next_open_change'])
            # 如果是止盈或止损平仓，调整平仓时仓位
            else:
                x.iloc[-1]['close'] = x.iloc[-1]['exe_price'] / x.iloc[0]['close'] * x.iloc[0]['position']
            return x

        t = df.groupby('start_time').apply(f1)
        t = t.reset_index(level=[0])
        df['position'] = t['close']
    elif group_num == 1:
        t = df.groupby('start_time')[['close', 'position']].apply(
            lambda x: x['close'] / x.iloc[0]['close'] * x.iloc[0]['position'])
        df['position'] = t.T.iloc[:, 0]

    # 每根K线仓位的最大值和最小值，针对最高价和最低价
    df['position_max'] = df['position'] * df['high'] / df['close']
    df['position_min'] = df['position'] * df['low'] / df['close']

    # ===计算每天实际持有资金的变化
    # 计算持仓利润
    df['profit'] = (df['position'] - init_cash * leverage_rate) * df['pos']  # 持仓盈利或者损失

    # 计算持仓利润最小值
    df.loc[df['pos'] == 1, 'profit_min'] = (df['position_min'] - init_cash * leverage_rate) * df['pos']  # 最小持仓盈利或者损失
    df.loc[df['pos'] == -1, 'profit_min'] = (df['position_max'] - init_cash * leverage_rate) * df['pos']  # 最小持仓盈利或者损失

    # 计算实际资金量
    df['cash'] = init_cash + df['profit']  # 实际资金
    df['cash'] -= init_cash * leverage_rate * c_rate  # 减去建仓时的手续费
    df['cash_min'] = df['cash'] - (df['profit'] - df['profit_min'])  # 实际最小资金
    df.loc[df['cash_min'] < 0, 'cash_min'] = 0  # 如果有小于0，直接设置为0
    df.loc[close_pos_condition, 'cash'] -= df.loc[close_pos_condition, 'position'] * c_rate  # 减去平仓时的手续费

    # ===判断是否会爆仓
    _index = df[df['cash_min'] <= min_margin].index

    if len(_index) > 0:
        print('有爆仓')
        df.loc[_index, '强平'] = 1
        df['强平'] = df.groupby('start_time')['强平'].fillna(method='ffill')
        df.loc[(df['强平'] == 1) & (df['强平'].shift(1) != 1), 'cash_强平'] = df['cash_min']  # 此处是有问题的
        df.loc[(df['pos'] != 0) & (df['强平'] == 1), 'cash'] = None
        df['cash'].fillna(value=df['cash_强平'], inplace=True)
        df['cash'] = df.groupby('start_time')['cash'].fillna(method='ffill')
        df.drop(['强平', 'cash_强平'], axis=1, inplace=True)  # 删除不必要的数据

    # ===计算资金曲线
    df['equity_change'] = df['cash'].pct_change()
    df.loc[open_pos_condition, 'equity_change'] = df.loc[open_pos_condition, 'cash'] / init_cash - 1  # 开仓日的收益率
    df['equity_change'].fillna(value=0, inplace=True)
    df['equity_curve'] = (1 + df['equity_change']).cumprod()

    # ===删除不必要的数据
    df.drop(['change', 'buy_at_open_change', 'sell_next_open_change', 'position', 'position_max',
             'position_min', 'profit', 'profit_min', 'cash', 'cash_min'], axis=1, inplace=True)

    return df


# === 双侧交易画资金曲线
def dual_equity_curve_with_long_and_short(data, leverage_rate=3, c_rate=3.0/1000, min_margin_rate=0.15):

    # 普通资金曲线
    def normal_equity_curve_with_long_and_short(symbol, df, leverage_rate, c_rate, min_margin_rate):
        """

        :param df:  带有signal和pos的原始数据
        :param leverage_rate:  bfx交易所最多提供3倍杠杆，leverage_rate可以在(0, 3]区间选择
        :param c_rate:  手续费
        :param min_margin_rate:  低保证金比例，必须占到借来资产的15%
        :return:
        """

        # =====基本参数
        init_cash = 100  # 初始资金
        min_margin = init_cash * leverage_rate * min_margin_rate  # 最低保证金

        # =====根据pos计算资金曲线
        # ===计算涨跌幅
        df['change_%s' % symbol] = df['close_%s' % symbol].pct_change(1)  # 根据收盘价计算涨跌幅
        df['buy_at_open_change_%s' % symbol] = df['close_%s' % symbol] / df['open_%s' % symbol] - 1  # 从今天开盘买入，到今天收盘的涨跌幅
        df['sell_next_open_change_%s' % symbol] = df['open_%s' % symbol].shift(-1) / df['close_%s' % symbol] - 1  # 从今天收盘到明天开盘的涨跌幅
        df.at[len(df) - 1, 'sell_next_open_change_%s' % symbol] = 0

        # ===选取开仓、平仓条件
        condition1 = df['pos_%s' % symbol] != 0
        condition2 = df['pos_%s' % symbol] != df['pos_%s' % symbol].shift(1)
        open_pos_condition = condition1 & condition2

        condition1 = df['pos_%s' % symbol] != 0
        condition2 = df['pos_%s' % symbol] != df['pos_%s' % symbol].shift(-1)
        close_pos_condition = condition1 & condition2

        # ===对每次交易进行分组
        df.loc[open_pos_condition, 'start_time'] = df['candle_begin_time']
        df['start_time'].fillna(method='ffill', inplace=True)
        df.loc[df['pos_%s' % symbol] == 0, 'start_time'] = pd.NaT

        # ===计算仓位变动
        # 开仓时仓位
        df.loc[open_pos_condition, 'position_%s' % symbol] = init_cash * leverage_rate * (1 + df['buy_at_open_change_%s' % symbol])  # 建仓后的仓位

        # 开仓后每天的仓位的变动
        group_num = len(df.groupby('start_time'))

        if group_num > 1:
            t = df.groupby('start_time').apply(lambda x: x['close_%s' % symbol] / x.iloc[0]['close_%s' % symbol] *
                                                         x.iloc[0]['position_%s' % symbol])
            t = t.reset_index(level=[0])
            df['position_%s' % symbol] = t['close_%s' % symbol]
        elif group_num == 1:
            t = df.groupby('start_time')[['close_%s' % symbol, 'position_%s' % symbol]].apply(
                lambda x: x['close_%s' % symbol] / x.iloc[0]['close_%s' % symbol] * x.iloc[0]['position_%s' % symbol])
            df['position_%s' % symbol] = t.T.iloc[:, 0]

        # 每根K线仓位的最大值和最小值，针对最高价和最低价
        df['position_max_%s' % symbol] = df['position_%s' % symbol] * df['high_%s' % symbol] / df['close_%s' % symbol]
        df['position_min_%s' % symbol] = df['position_%s' % symbol] * df['low_%s' % symbol] / df['close_%s' % symbol]

        # 平仓时仓位
        df.loc[close_pos_condition, 'position_%s' % symbol] *= (1 + df.loc[close_pos_condition, 'sell_next_open_change_%s' % symbol])

        # ===计算每天实际持有资金的变化
        # 计算持仓利润
        df['profit_%s' % symbol] = (df['position_%s' % symbol] - init_cash * leverage_rate) * df['pos_%s' % symbol]  # 持仓盈利或者损失

        # 计算持仓利润最小值
        df.loc[df['pos_%s' % symbol] == 1, 'profit_min_%s' % symbol] = (df['position_min_%s' % symbol] - init_cash * leverage_rate) * df[
            'pos_%s' % symbol]  # 最小持仓盈利或者损失
        df.loc[df['pos_%s' % symbol] == -1, 'profit_min_%s' % symbol] = (df['position_max_%s' % symbol] - init_cash * leverage_rate) * df[
            'pos_%s' % symbol]  # 最小持仓盈利或者损失

        # 计算实际资金量
        df['cash_%s' % symbol] = init_cash + df['profit_%s' % symbol]  # 实际资金
        df['cash_%s' % symbol] -= init_cash * leverage_rate * c_rate  # 减去建仓时的手续费
        df['cash_min_%s' % symbol] = df['cash_%s' % symbol] - (df['profit_%s' % symbol] - df['profit_min_%s' % symbol])  # 实际最小资金
        df.loc[df['cash_min_%s' % symbol] < 0, 'cash_min_%s' % symbol] = 0  # 如果有小于0，直接设置为0
        df.loc[close_pos_condition, 'cash_%s' % symbol] -= df.loc[close_pos_condition, 'position_%s' % symbol] * c_rate  # 减去平仓时的手续费

        # ===判断是否会爆仓
        _index = df[df['cash_min_%s' % symbol] <= min_margin].index

        if len(_index) > 0:
            print('有爆仓_%s' % symbol)

            df.loc[_index, '强平_%s' % symbol] = 1
            df['强平_%s' % symbol] = df.groupby('start_time')['强平_%s' % symbol].fillna(method='ffill')
            df.loc[(df['强平_%s' % symbol] == 1) & (df['强平_%s' % symbol].shift(1) != 1), 'cash_强平_%s' % symbol] = df['cash_min_%s' % symbol]  # 此处是有问题的
            df.loc[(df['pos_%s' % symbol] != 0) & (df['强平_%s' % symbol] == 1), 'cash_%s' % symbol] = None
            df['cash_%s' % symbol].fillna(value=df['cash_强平_%s' % symbol], inplace=True)
            df['cash_%s' % symbol] = df.groupby('start_time')['cash_%s' % symbol].fillna(method='ffill')
            df.drop(['强平_%s' % symbol, 'cash_强平_%s' % symbol], axis=1, inplace=True)  # 删除不必要的数据

        # ===计算资金曲线
        df['equity_change_%s' % symbol] = df['cash_%s' % symbol].pct_change()
        df.loc[open_pos_condition, 'equity_change_%s' % symbol] = df.loc[open_pos_condition, 'cash_%s' % symbol] / init_cash - 1  # 开仓日的收益率
        df['equity_change_%s' % symbol].fillna(value=0, inplace=True)
        df['equity_curve_%s' % symbol] = (1 + df['equity_change_%s' % symbol]).cumprod()

        # ===判断资金曲线是否有负值，有的话后面都置成0
        if len(df[df['equity_curve_%s' % symbol] < 0]) > 0:
            neg_start = df[df['equity_curve_%s' % symbol] < 0].index[0]
            df.loc[neg_start:, 'equity_curve_%s' % symbol] = 0
        #
        # # ===删除不必要的数据
        # df.drop(['change', 'buy_at_open_change', 'sell_next_open_change', 'position', 'position_max',
        #          'position_min', 'profit', 'profit_min', 'cash', 'cash_min'], axis=1, inplace=True)

        return df

    data['pos_curr'] = data['pos']
    data['pos_base'] = -data['pos']
    df_curr = normal_equity_curve_with_long_and_short('curr', data, leverage_rate=leverage_rate, c_rate=c_rate,
                                                      min_margin_rate=min_margin_rate)
    df_sum = normal_equity_curve_with_long_and_short('base', df_curr, leverage_rate=leverage_rate, c_rate=c_rate,
                                                     min_margin_rate=min_margin_rate)

    df_sum = df_sum[['candle_begin_time', 'open', 'high', 'low', 'close', 'close_curr', 'close_base', 'pos_curr',
                     'pos_base', 'start_time', 'equity_curve_curr', 'equity_curve_base']].copy()

    if len(df_sum.groupby('start_time')) > 1:
        temp = df_sum.groupby('start_time').apply(lambda x: (x['close_curr'].iloc[0] * x['equity_curve_curr'] +
                                                             x['close_base'].iloc[0] * x['equity_curve_base']) /
                                                            (x['close_curr'].iloc[0] + x['close_base'].iloc[0]))
        temp = temp.reset_index(level=[0])
        df_sum['equity_curve'] = temp[0]
        df_sum['equity_curve'].fillna(method='ffill', inplace=True)
        df_sum['equity_curve'].fillna(1, inplace=True)
        df_sum['candle_begin_time'] = pd.to_datetime(df_sum['candle_begin_time'])
    else:
        df_sum['equity_curve'] = 1

    return df_sum
