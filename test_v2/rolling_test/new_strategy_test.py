from test_v2.obj_ornt.back_test import *
from test_v2.obj_ornt.account import Account


class StrategyBackTest(BackTest):
    # 计算策略信号
    def cal_strategy_sig(self, symbol, now_time):

        df = self.get_symbol_data(symbol=symbol, now_time=now_time)
        # print(df, symbol)

        # 查看当前持仓
        if self.account.pos_status[symbol]:
            if self.account.pos_status[symbol][0] > 0:
                now_pos = 1
            else:
                now_pos = -1
        else:
            now_pos = 0

        func_str = 'real_time_signal_%s_without_stop_lose(now_pos, df, %s)' % \
                   (self.account.symbol_info[symbol]['strategy'], self.account.symbol_info[symbol]['para'])
        print('NOW', now_pos)
        print('TARGET', eval(func_str))
        # 根据当前、目标仓位计算信号。信号list中需要先平仓信号，再开仓信号
        if now_pos == 1 and eval(func_str) == 0:  # 平多
            sig = ['long_close']
        elif now_pos == -1 and eval(func_str) == 0:  # 平空
            sig = ['short_close']
        elif now_pos == 0 and eval(func_str) == 1:  # 开多
            sig = ['long_open']
        elif now_pos == 0 and eval(func_str) == -1:  # 开空
            sig = ['short_open']
        elif now_pos == 1 and eval(func_str) == -1:  # 平多，开空
            sig = ['long_close', 'short_open']
        elif now_pos == -1 and eval(func_str) == 1:  # 平空，开多
            sig = ['short_close', 'long_open']
        else:
            sig = []

        return sig


# 一个BackTest对象只在时间序列上移动一次
if __name__ == '__main__':
    # for symbol in ['BTCUSD', 'ETHUSD', 'EOSUSD', 'LTCUSD', 'XRPUSD', 'BCHUSD', 'BSVUSD']:
    #     df = pd.read_hdf(r'E:\cta_v2\data\test_v2\input\%s.h5' % symbol, key='5T')
    #     df.reset_index(inplace=True)
    #
    #     eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')
    #     df.to_sql(symbol, eg, index=False, if_exists='replace', schema='cta_test')
    #
    # exit()
    localtime = int(time.time())

    save_name = 'upper_close'
    for roots, dirs, files in os.walk(r'E:\cta_v2\data\test_v2\output\oo_test'):
        if files:
            for file in files:
                if file.startswith(save_name):
                    raise Exception('存档已存在！')

    l_symbol_info = {
        'BTCUSD': {
            'proportion': 1.0,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'upper_lower_bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_profit_method': 'price_hard_pct',
            'stop_para': [3, 15]
        },
        # 'ETHUSD': {
        #     'proportion': 1.0,
        #     'para': [200.0, 2.0],
        #     'leverage': 3,
        #     'time_interval': '15T',
        #     'strategy': 'bolling',
        #     'order_method': 'market',
        #     'stop_loss_method': 'float_hard_pct',
        #     'stop_profit_method': 'price_hard_pct',
        #     'stop_para': [3, 15]
        # },
                     }

    l_start_time = '2019-01-15 00:00:00'
    l_end_time = '2019-06-30 23:55:00'
    l_order_method = 'market'

    path_dict = {
        'BTCUSD': r'E:\cta_v2\data\test_v2\output\upper_lower_bolling\BTCUSD\local_data_2_2cta_test\upper_lower_1562819854_top_para_record.csv',
        # 'ETHUSD': r'E:\cta_v2\data\test_v2\output\bolling\ETHUSD\local_data_2_2cta_test\1562762050_top_para_record.csv',
        # 'EOSUSD': r'E:\cta_v2\data\test_v2\output\bolling\EOSUSD\local_data_2_2cta_test\1562697093_top_para_record.csv',
    }
    l_para_series = gen_para_series(path_dict)
    # l_para_series = [{'usage_end': datetime.strptime('2019-06-30 09:30:00', "%Y-%m-%d %H:%M:%S"),
    #                   'para': {'BTCUSD': ['15T', [290.0, 2.0], [10, 60]],
    #                            }
    #                   },
    #                  ]

    bt1 = StrategyBackTest(l_symbol_info,
                   l_start_time,
                   l_end_time,
                   l_para_series)

    assessment, equity_df = bt1.main_back_test()
    print(assessment)

    equity_df.to_csv(r'E:\cta_v2\data\test_v2\output\oo_test\%s_%s_res_test.csv' % (save_name, localtime))
# start 20190708 13:50
