import os
import warnings
from datetime import timedelta
from multiprocessing import Pool

import pandas as pd
from sqlalchemy import create_engine

import ckt.test.strategy.timing_strategy.Signals as sigs

q = sigs.timedelta
from ckt.cq_sdk.path import get_data_path
from ckt.test.strategy.timing_strategy.equity_curve import equity_curve_with_long_and_short

eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')
warnings.simplefilter(action='ignore')
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option('display.max_rows', 1000)


# ===具体进行回测
def func(df_dict, strategy_name, para_freq_list, train_begin_date):
    best_para_trained = {'para': [0, 0, 0],
                         'yield': 0,
                         'maxdd': 0,
                         'annual_rtn/maxdd': 0}

    for para_freq in para_freq_list:
        freq = para_freq['freq']
        para = para_freq['para']
        all_data = df_dict[freq]

        # 计算交易信号
        function_str = 'sigs.signal_%s_with_stop_lose(all_data.copy(), para)' % strategy_name
        da = eval(function_str)

        # 筛选交易时间
        signal_df = da[da['candle_begin_time'] >= train_begin_date]

        # 计算资金曲线
        df = equity_curve_with_long_and_short(signal_df, leverage_rate=3, c_rate=3.0 / 1000)
        df = df[:-1].copy()  # 最后不知道为什么会加上谜之一行，先去掉

        # 计算年化收益
        annual_return = (df['equity_curve'].iloc[-1] / df['equity_curve'].iloc[0]) ** (
            '1 days 00:00:00' / (df['candle_begin_time'].iloc[-1] - df['candle_begin_time'].iloc[0]) * 365) - 1

        # 计算最大回撤
        max_drawdown = (1 - df['equity_curve'] / df['equity_curve'].expanding().max()).max()

        # 计算年化收益回撤比
        ratio = annual_return / max_drawdown if max_drawdown != 0 else 0
        print(freq, para, '最优参数收益：', df.iloc[-1]['equity_curve'], '最优参数年化收益回撤比：', ratio)

        # 如果现在测出来的参数比之前保留的好，就更新最优参数
        if (ratio > best_para_trained['annual_rtn/maxdd']) | (best_para_trained['para'][0] == 0):
            best_para_trained['para'] = para
            best_para_trained['freq'] = freq
            best_para_trained['yield'] = df['equity_curve'].iloc[-1]
            best_para_trained['maxdd'] = max_drawdown
            best_para_trained['annual_rtn/maxdd'] = ratio

    # 存储数据（多进程模式下如果保存成dataframe后面会有重复值）
    return str(best_para_trained['freq']) + ':' + str(best_para_trained['para']) + ':' + str(best_para_trained['yield']) + ':' + str(
        best_para_trained['maxdd']) + ':' + str(best_para_trained['annual_rtn/maxdd'])


# ===从数据库中获取数据
def get_data(symbol, time_interval, begin_time, end_time, data_schema):

    sql = 'select * from "%s"."%s" where candle_begin_time >= \'%s\' and candle_begin_time < \'%s\'' % \
          (data_schema, symbol, begin_time, end_time)

    df = pd.read_sql_query(sql, con=eg)
    df['candle_begin_time'] = pd.to_datetime(df['candle_begin_time'])
    df.sort_values(by='candle_begin_time', ascending=True, inplace=True)
    df.reset_index(drop=True, inplace=True)

    data = df.resample(rule=time_interval, on='candle_begin_time', base=0,
                       label='left',
                       closed='left').agg(
        {'open': 'first',
         'high': 'max',
         'low': 'min',
         'close': 'last',
         'volume': 'sum',
         }
    )
    data.reset_index(inplace=True)
    return data


# ===对回测期的数据进行回测，返回最优参数
def train(symbol, strategy_name, para_list, process, train_begin_date, train_end_date, sys_para):
    df_dict = {}
    for freq in sys_para['freq']:
        df_dict[freq] = get_data(symbol, freq, train_begin_date, train_end_date, 'cta_test')

    # 参数的附上freq
    para_freq_list = []
    for para in para_list:
        for freq in sys_para['freq']:
            para_freq_list.append({'freq': freq, 'para': para})


    split_list = []  # 分成process组的待测参数
    # 待测参数分片
    for i in range(process):
        if i < process:
            split_para = para_freq_list[i * int(len(para_freq_list) / process):
                                        (i+1) * int(len(para_freq_list) / process)]
        else:
            split_para = para_freq_list[i * int(len(para_freq_list) / process):]
        split_list.append(split_para)

    pool = Pool(processes=process)  # 开辟进程池
    result = []

    for split_para_list in split_list:
        result.append(pool.apply_async(func, (df_dict, strategy_name, split_para_list, train_begin_date, )))

    pool.close()  # 关闭进程池
    pool.join()  # 等待开辟的所有进程执行完后，主进程才继续往下执行

    A = pd.DataFrame()
    for i, res in enumerate(result):
        A.loc[i, '参数'] = res.get().split(':')[1]
        A.loc[i, '时间级别'] = res.get().split(':')[0]
        A.loc[i, '策略收益'] = float(res.get().split(':')[2])
        A.loc[i, '最大回撤'] = float(res.get().split(':')[3])
        A.loc[i, '年化收益/最大回撤'] = float(res.get().split(':')[4])

    temp = A['参数'].str[1:-1].str.split(',', expand=True)
    rtn = pd.concat([temp, A.iloc[:, 1:]], axis=1)
    rtn.columns = para_names

    para_rank = A.sort_values(by='年化收益/最大回撤', ascending=False)
    para_rank_selected = para_rank.loc[para_rank['年化收益/最大回撤'] == para_rank['年化收益/最大回撤'].iloc[0]].copy()
    if len(para_rank_selected) > 1:
        para_rank_selected['difficulty'] = para_rank_selected['参数'].apply(
            lambda x: float(x[1:-1].split(', ')[0]) * float(x[1:-1].split(', ')[1])
        )
        para_rank_selected.loc[para_rank_selected['时间级别'] == '15T', 'difficulty'] *= 1
        para_rank_selected.loc[para_rank_selected['时间级别'] == '30T', 'difficulty'] *= 2
        para_rank_selected.loc[para_rank_selected['时间级别'] == '1H', 'difficulty'] *= 4
        para_rank_selected['difficulty'] += para_rank_selected['参数'].apply(
            lambda x: float(x[1:-1].split(', ')[2])
        )
        para_rank = para_rank_selected.sort_values(by='difficulty', ascending=True)
    print('=' * 10, symbol, '=' * 10)
    print(para_rank)

    # 保存最优参数到本地
    rtn_path = get_data_path('test_v2', 'output', 'real_time', strategy_name, '%s' %
                             pair, str(train_end_date.strftime('%Y-%m-%d')))
    if not os.path.exists(rtn_path):
        os.makedirs(rtn_path)

    # 最优参数转换回列表格式
    top_para_str = para_rank[:1]['参数'].values[0][1:-1].split(',')
    top_para_freq = para_rank[:1]['时间级别'].values[0]

    top_para = {'para': [], 'freq': top_para_freq}
    for para in top_para_str:
        top_para['para'].append(float(para))

    return top_para


# ===主程序
def main(pair, strategy_name, para_list, para_names, sys_para, process, use_length):

    train_begin_date = sys_para['begin_date']  # 整体回测开始时间（使用的数据的最早时间）
    train_end_date = sys_para['end_date']

    print('本轮训练开始时间为', train_begin_date)
    print('本轮训练结束时间为', train_end_date)

    # 回测计算最优参数，在单一阶段的训练中使用多进程，切换阶段时还是要阻塞
    # 减少阶段内的IO，加快速度，进程计算完成后再比较process个最优参数哪个好，返回最优的top_para
    top_para = train(pair, strategy_name, para_list, process, pd.to_datetime(train_begin_date),
                     pd.to_datetime(train_end_date), sys_para)

    top_para['para_train_begin'] = train_begin_date
    top_para['para_train_end'] = train_end_date
    top_para['para_usage'] = (pd.to_datetime(train_end_date) + timedelta(days=use_length)).strftime('%Y-%m-%d')

    return top_para

def gen_date_series(begin_date, end_date, train_length, use_length):
    t_end_date = pd.to_datetime(begin_date) + timedelta(days=train_length)
    date_series = []
    while t_end_date <= pd.to_datetime(end_date):
        date_series.append(
            {
                'begin_date': (t_end_date - timedelta(days=train_length)).strftime('%Y-%m-%d'),
                'end_date': t_end_date.strftime('%Y-%m-%d'),
                'freq': ['15T', '30T', '1H']
            }
        )
        t_end_date += timedelta(days=use_length)
    return date_series


if __name__ == '__main__':

    # 策略名称
    strategy_name = 'bolling'

    # 这边需要手动输入参数名称
    para_names = ['n', 'm', '止损比例', '时间级别', '策略收益', '最大回撤', '年化收益/最大回撤']

    process = 4  # 最大进程数

    train_length = 30
    use_length = 30

    # 回测系统参数
    sys_para_series = gen_date_series(
        begin_date='2020-02-13',
        end_date='2020-04-15',
        train_length=train_length,
        use_length=use_length
    )

    for pair in [
        'BTCUSD',
        'ETHUSD',
        'EOSUSD',
        'LTCUSD'
    ]:  # 币种
        final_res = []
        for sys_para in sys_para_series:
            # 待测参数组合。每次回测都相同
            n_list = range(60, 300, 20)
            m_list = [i / 2 for i in range(2, 11)]
            stop_lose_pct_list = range(2, 11, 1)

            # 生成参数列表
            para_list = []
            for n in n_list:
                for m in m_list:
                    for stop_lose_pct in stop_lose_pct_list:
                        para_list.append([n, m, stop_lose_pct])

            print('\n====%s====' % pair)
            para_res = main(pair, strategy_name, para_list, para_names, sys_para, process, use_length)
            final_res.append(para_res)

        t_pair_res = pd.DataFrame(final_res)

        print(t_pair_res)