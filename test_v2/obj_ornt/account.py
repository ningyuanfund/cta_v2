# Account 只实例化一次
class Account:
    def __init__(self, init_cash, start_time, symbol_info):
        self.now_time = start_time
        self.change_info = {'time': [], 'change_log': []}
        self.symbol_info = symbol_info
        self.pos_status = {}
        self.now_para = {}
        self.equity = 0
        for symbol in symbol_info.keys():
            self.pos_status[symbol] = []
            self.symbol_info[symbol]['available_cash'] = init_cash * self.symbol_info[symbol]['proportion']

    def __repr__(self):
        return "now time %s \n now para %s \n now equity %s \n now pos %s" % \
               (self.now_time, self.now_para, self.equity, self.pos_status)

    # 多头开仓
    def long_open(self, symbol, price, amount, order_type, leverage):
        if self.symbol_info[symbol]['available_cash'] < (price * amount * (0.002 + 1 / leverage)):
            raise Exception('NOT ENOUGH MONEY')
        else:
            self.pos_status[symbol] = [amount, price, price * amount * 1 / leverage, 0, 0, 0]
            self.change_info['time'].append(self.now_time)
            self.change_info['change_log'].append([symbol, 'long_open'])
            self.symbol_info[symbol]['available_cash'] -= price * amount * 0.002  # 减掉消耗的手续费

    # 空头开仓
    def short_open(self, symbol, price, amount, order_type, leverage):
        if self.symbol_info[symbol]['available_cash'] < (price * amount * (0.002 + 1 / leverage)):
            raise Exception('NOT ENOUGH MONEY')
        else:
            self.pos_status[symbol] = [-amount, price, price * amount * 1 / leverage, 0, 0, 0]
            self.change_info['time'].append(self.now_time)
            self.change_info['change_log'].append([symbol, 'short_open'])
            self.symbol_info[symbol]['available_cash'] -= price * amount * 0.002  # 减掉消耗的手续费

    # 多头平仓
    def long_close(self, symbol, price, amount, order_type):
        if len(self.pos_status[symbol]) == 0:
            raise Exception('NO HOLDING')  # 该品种当前无持仓
        elif self.pos_status[symbol][0] < amount:
            raise Exception('NOT ENOUGH HOLDING')  # 该品种当前持仓量不够平仓
        else:
            # 现金 = 初始保证金数量 + 多头平仓数量 * （多头平仓价 - 多头开仓价）+ （可用保证金 - 初始保证金【即没有使用过的保证金】）
            # self.symbol_info[symbol]['available_cash'] = (self.pos_status[symbol][2] + self.pos_status[symbol][0] *
            #                                               (price - self.pos_status[symbol][1]) * (1 - 0.003)) + \
            #                                              (self.symbol_info[symbol]['available_cash'] - self.pos_status[symbol][2])
            self.symbol_info[symbol]['available_cash'] += self.pos_status[symbol][0] * \
                                                          (price - self.pos_status[symbol][1]) * (1 - 0.003)
            self.pos_status[symbol] = []
            self.change_info['time'].append(self.now_time)
            self.change_info['change_log'].append([symbol, 'long_close'])

    # 空头平仓
    def short_close(self, symbol, price, amount, order_type):
        if len(self.pos_status[symbol]) == 0:
            raise Exception('NO HOLDING')  # 该品种当前无持仓
        elif self.pos_status[symbol][0] > amount:
            raise Exception('NOT ENOUGH HOLDING')  # 该品种当前持仓量不够平仓
        else:
            # 现金增加数量 = 初始保证金数量 + 空头平仓数量(是负数) * (-(空头开仓价 - 空头平仓价) + （可用保证金 - 初始保证金【即没有使用过的保证金】）
            # self.symbol_info[symbol]['available_cash'] = (self.pos_status[symbol][2] + self.pos_status[symbol][0] *
            #                                               (price - self.pos_status[symbol][1]) * (1 - 0.003)) + \
            #                                              (self.symbol_info[symbol]['available_cash'] - self.pos_status[symbol][2])
            self.symbol_info[symbol]['available_cash'] += abs(self.pos_status[symbol][0]) * (self.pos_status[symbol][1]
                                                                                             - price) * (1 - 0.003)
            self.pos_status[symbol] = []
            self.change_info['time'].append(self.now_time)
            self.change_info['change_log'].append([symbol, 'short_close'])

    # 根据当前持仓，查看有没有爆仓(此处要传入最低价和最高价)，更新浮动盈亏 max 和 min
    def check_balance(self, price_dict):
        # 检查爆仓
        position_value = 0
        resid_balance = 0
        for symbol in self.symbol_info.keys():
            # 如果有持仓，计算仓位价值和剩余保证金
            if len(self.pos_status[symbol]) != 0:

                # 多头仓位计算浮盈或浮亏
                if self.pos_status[symbol][0] > 0:
                    floating = self.pos_status[symbol][0] * (float(price_dict[symbol]['low']) - self.pos_status[symbol][1])
                # 空头仓位计算浮盈或浮亏
                else:
                    floating = self.pos_status[symbol][0] * (float(price_dict[symbol]['high']) - self.pos_status[symbol][1])
                # 更新max和min
                self.pos_status[symbol][3] = floating
                self.pos_status[symbol][4] = min(floating, self.pos_status[symbol][4])
                self.pos_status[symbol][5] = max(floating, self.pos_status[symbol][5])
                position_value += self.pos_status[symbol][0] * price_dict[symbol]['close']
                resid_balance += (self.symbol_info[symbol]['available_cash'] + floating)
            # 如果没有持仓，计算剩余保证金
            else:
                resid_balance += self.symbol_info[symbol]['available_cash']

        # 触发强制平仓，清空全部仓位，重新分配保证金
        if resid_balance <= position_value * 0.15:
            print('!! LIQUIDATION FORCED !!')
            self.change_info['time'].append(self.now_time)
            self.change_info['change_log'].append(['ALL', 'liquidation'])

            if resid_balance > 0:
                for symbol in self.symbol_info.keys():
                    self.pos_status[symbol] = []
                    self.symbol_info[symbol]['available_cash'] = \
                        resid_balance * 0.6 * self.symbol_info[symbol]['proportion']
            else:
                print('#####=== END OF THE STORY ===#####')
                exit()

    # re_balance
    def re_balance(self):
        for symbol in self.symbol_info.keys():
            if len(self.pos_status[symbol]) > 0:
                raise Exception('HOLDING POSITION, REBALANCE REJECTED')
        total_cash = 0
        total_proportion = 0
        for symbol in self.symbol_info.keys():
            total_cash += self.symbol_info[symbol]['available_cash']
            total_proportion += self.symbol_info[symbol]['proportion']

        for symbol in self.symbol_info.keys():
            self.symbol_info[symbol]['available_cash'] = \
                total_cash * (self.symbol_info[symbol]['proportion'] / total_proportion)

    # 建立当前账户的快照
    def snapshot(self):
        equity = 0
        for symbol in self.symbol_info:
            if len(self.pos_status[symbol]) == 0:
                equity += self.symbol_info[symbol]['available_cash']
            else:
                equity += (self.symbol_info[symbol]['available_cash'] + self.pos_status[symbol][3])
        self.equity = equity
        return equity

    # 检查要更新参数
    def check_para_renew(self, now_time, para_series):
        # 先看是否需要更新，再看是否可以更新
        for symbol in self.symbol_info.keys():
            # 需要更新且可以更新（即没有持仓）
            if (now_time > self.now_para[symbol]['usage_end']) & (self.pos_status[symbol] == []):
                for para_set in para_series[symbol]:
                    if now_time < para_set['usage_end']:
                        self.now_para[symbol] = {'usage_end': para_set['usage_end'],
                                                 'time_interval': para_set['time_interval'],
                                                 'para': para_set['para'],
                                                 'stop_para': [para_set['para'][-1], 20]}
                        self.symbol_info[symbol]['time_interval'] = self.now_para[symbol]['time_interval']
                        self.symbol_info[symbol]['para'] = self.now_para[symbol]['para']
                        self.symbol_info[symbol]['stop_para'] = self.now_para[symbol]['stop_para']
                        break

    # 设置初始参数
    def set_init_para(self, para_series):

        for symbol in para_series.keys():
            self.now_para[symbol] = {'usage_end': para_series[symbol][0]['usage_end'],
                                     'time_interval': para_series[symbol][0]['time_interval'],
                                     'para': para_series[symbol][0]['para'],
                                     'stop_para': [para_series[symbol][0]['para'][-1], 20]}

            self.symbol_info[symbol]['time_interval'] = self.now_para[symbol]['time_interval']
            self.symbol_info[symbol]['para'] = self.now_para[symbol]['para']
            self.symbol_info[symbol]['stop_para'] = self.now_para[symbol]['stop_para']
        print('*' * 5, 'SET INIT PARA', '*' * 5)
        print(self.now_para)
