from test_v2.obj_ornt.back_test import *


class UbunTest(BackTest):
    def cal_stop_sig(self, symbol, now_time, now_pos):
        stop_func_str = '%s(symbol, self.account.pos_status[symbol], now_time,' \
                             'self.account.symbol_info[symbol][\'stop_para\'], self.eg)' % \
                             (self.account.symbol_info[symbol]['stop_loss_method'])
        whether_stop = eval(stop_func_str)

        stop_signal = False
        if whether_stop and (now_pos > 0):
            stop_signal = 'long_close'
        elif whether_stop and(now_pos < 0):
            stop_signal = 'short_close'

        return stop_signal

    def check_tradable(self, now_time):

        btc_vol = self.get_bar(symbol='BTCUSD', tm=now_time).values[0][6]
        eth_vol = self.get_bar(symbol='ETHUSD', tm=now_time).values[0][6]

        if (btc_vol > 0) & (eth_vol > 0):
            return True
        else:
            return False


# 一个BackTest对象只在时间序列上移动一次
if __name__ == '__main__':
    # for symbol in ['BTCUSD', 'ETHUSD', 'EOSUSD', 'LTCUSD', 'XRPUSD', 'BCHUSD', 'BSVUSD']:
    #     df = pd.read_hdf(r'E:\cta_v2\data\test_v2\input\%s.h5' % symbol, key='5T')
    #     df.reset_index(inplace=True)
    #
    #     eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')
    #     df.to_sql(symbol, eg, index=False, if_exists='replace', schema='cta_test')
    #
    # exit()
    l_localtime = int(time.time())

    l_save_name = 'EOS_mov_stop'
    l_output_root_path = r'/home/syca/PycharmProjects/cta_v2/data/test_v2/output/oo_test'

    for roots, dirs, files in os.walk(l_output_root_path):
        if files:
            for file in files:
                if file.startswith(l_save_name):
                    raise Exception('存档已存在！')

    l_symbol_info = {
        'EOSUSD': {
            'proportion': 1.0,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'mov_stop',
            'stop_para': [3, 15]
        },
    }

    l_start_time = '2019-01-15 00:00:00'
    l_end_time = '2019-06-30 23:55:00'
    l_order_method = 'market'

    path_dict = {
        # 'BTCUSD': r'/home/syca/data/BTC1562697093_top_para_record.csv',
        # 'ETHUSD': r'/home/syca/data/ETH1562762050_top_para_record.csv',
        'EOSUSD': r'/home/syca/data/EOS1562697093_top_para_record.csv',
    }
    l_para_series = gen_para_series(path_dict)

    bt1 = UbunTest(l_symbol_info,
                   l_start_time,
                   l_end_time,
                   l_para_series,
                   l_output_root_path,
                   l_save_name,
                   l_localtime)

    assessment, equity_df = bt1.main_back_test()
    print(assessment)
