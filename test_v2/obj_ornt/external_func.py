from datetime import datetime, timedelta


# 下次检查调仓时间，15分钟check一次。
def next_check_time(now):
    now_tm = datetime.strptime(now, "%Y-%m-%d %H:%M:%S")
    time_interval = 15  # 15分钟检查一次

    target_min = (int(now_tm.minute / time_interval) + 1) * time_interval
    if target_min < 60:
        check_time = now_tm.replace(minute=target_min, second=0, microsecond=0)
    else:
        if now_tm.hour == 23:
            check_time = now_tm.replace(hour=0, minute=0, second=0, microsecond=0)
            check_time += timedelta(days=1)
        else:
            check_time = now_tm.replace(hour=now_tm.hour + 1, minute=0, second=0, microsecond=0)

    return check_time


# ===查看哪些品种这一轮需要交易
def check_symbol(now_time, symbol_info):

    now_minute = str(now_time.minute)
    now_hour = str(now_time.hour)
    check_dict = {'5T': ['0', '5', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55'],
                  '15T': ['0', '15', '30', '45'],
                  '30T': ['0', '30'],
                  '1H': ['0'],
                  '1D': ['0']
                  }

    checked_list = []
    for symbol in symbol_info:
        if symbol_info[symbol]['time_interval'] == '1D':
            if now_hour == '8':
                checked_list.append(symbol)
        elif now_minute in check_dict[symbol_info[symbol]['time_interval']]:
            checked_list.append(symbol)
    # print('THIS PERIOD CHECK：', checked_list)

    return checked_list
