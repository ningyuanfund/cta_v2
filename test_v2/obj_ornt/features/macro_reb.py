from test_v2.obj_ornt.account import *
from test_v2.obj_ornt.test_RAM import *


class MacroAccount(Account):
    def __init__(self, init_cash, start_time, symbol_info):
        Account.__init__(self, init_cash, start_time, symbol_info)
        self.bond = 100000

    def __repr__(self):
        return "now time %s \n now para %s \n now equity %s \n now bond %s \n now pos %s" % \
               (self.now_time, self.now_para, self.equity, self.bond, self.pos_status)

    # 建立当前账户的快照
    def snapshot(self):
        equity = 0
        for symbol in self.symbol_info:
            if len(self.pos_status[symbol]) == 0:
                equity += self.symbol_info[symbol]['available_cash']
            else:
                equity += (self.symbol_info[symbol]['available_cash'] + self.pos_status[symbol][3])
        self.equity = equity
        return equity

    # 空仓时对整体账户进行股债平衡
    def macro_rebalance(self):
        total_cash = 0
        total_proportion = 0
        for symbol in self.symbol_info.keys():
            total_cash += self.symbol_info[symbol]['available_cash']
            total_proportion += self.symbol_info[symbol]['proportion']
        # 策略盈利
        if total_cash / self.bond > 1:
            print('STOCK TO BOND')
            print(total_cash, '->', self.bond)

            target = (total_cash + self.bond) / 2
            self.bond = target
            for symbol in self.symbol_info.keys():
                self.symbol_info[symbol]['available_cash'] *= (target / total_cash)
        # 亏到1：9，分配为3：7，并 rebalance
        elif total_cash / self.bond < (1/9):
            print('BOND TO STOCK')
            print(self.bond, '->', total_cash)

            bond_target = (total_cash + self.bond) * 0.7
            risky_target = (total_cash + self.bond) * 0.3
            self.bond = bond_target
            for symbol in self.symbol_info.keys():
                self.symbol_info[symbol]['available_cash'] = risky_target * self.symbol_info[symbol]['proportion']


class MacroTest(BackTest):
    def __init__(self, symbol_info, start_time, end_time, symbol_para_path_dict, output_root_path, save_name, localtime):
        BackTest.__init__(self, symbol_info, start_time, end_time, symbol_para_path_dict, output_root_path, save_name, localtime)
        self.equity_curve = {'candle_begin_time': [], 'equity_curve': [], 'bond': []}

    # 实例化account
    def gen_init_account(self, now_time, symbol_info):
        return MacroAccount(init_cash=self.init_cash, start_time=now_time, symbol_info=symbol_info)

    # 检查大周期的价格位置，确定开仓比例
    def check_longer_period(self, symbol, side):
        # now_date = datetime.strptime(self.account.now_time.strftime("%Y-%m-%d 00:00:00"), "%Y-%m-%d %H:%M:%S")
        # begin_date = now_date - timedelta(days=25)
        # begin = begin_date.strftime("%Y-%m-%d 00:00:00")
        # end = now_date.strftime("%Y-%m-%d 00:00:00")
        # df = self.get_data(symbol, begin, end)
        #
        # data = df.resample(rule='1D', on='candle_begin_time', base=0, label='left', closed='left').agg(
        #     {'open': 'first',
        #      'high': 'max',
        #      'low': 'min',
        #      'close': 'last',
        #      'volume': 'sum',
        #      }
        # )
        # data['price'] = data[['open', 'high', 'low', 'close']].mean(axis=1)
        # data['mid'] = data['price'].rolling(20).mean()
        # data['std'] = data['price'].rolling(20).std(ddof=1)
        # data['upper'] = data['mid'] + 2 * data['std']
        # data['lower'] = data['mid'] - 2 * data['std']
        # data = data[:-1].copy()
        #
        # # 下轨之下
        # if data['price'].iloc[-1] < data['lower'].iloc[-1]:
        #     if side == 'long':
        #         return 0.25
        #     else:
        #         return 1
        # # 下轨中轨之间
        # elif (data['price'].iloc[-1] >= data['lower'].iloc[-1]) and (data['price'].iloc[-1] < data['mid'].iloc[-1]):
        #     if side == 'long':
        #         return 0.5
        #     else:
        #         return 0.75
        # # 中轨上轨之间
        # elif (data['price'].iloc[-1] >= data['mid'].iloc[-1]) and (data['price'].iloc[-1] < data['upper'].iloc[-1]):
        #     if side == 'long':
        #         return 0.75
        #     else:
        #         return 0.5
        # # 上轨之上
        # elif data['price'].iloc[-1] >= data['upper'].iloc[-1]:
        #     if side == 'long':
        #         return 1
        #     else:
        #         return 0.25
        return 1

    # 多头开仓
    def long_open(self, symbol, price):
        openable_amount = self.account.symbol_info[symbol]['available_cash'] / \
                          (price * (0.002 + 1 / self.account.symbol_info[symbol]['leverage']))
        # 根据大周期的位置确定仓位比例
        longer_period_suggest = self.check_longer_period(symbol, 'long')
        print('LONG OPEN PROPORTION', longer_period_suggest)
        exe_amount = math.floor(openable_amount * 10000 * longer_period_suggest) / 10000  # 交易最小单位取小数点后四位
        self.account.long_open(symbol=symbol, price=price, amount=exe_amount, order_type='',
                               leverage=self.account.symbol_info[symbol]['leverage'])

    # 空头开仓
    def short_open(self, symbol, price):
        openable_amount = self.account.symbol_info[symbol]['available_cash'] / \
                          (price * (0.002 + 1 / self.account.symbol_info[symbol]['leverage']))
        # 根据大周期的位置确定仓位比例
        longer_period_suggest = self.check_longer_period(symbol, 'short')
        print('SHORT OPEN PROPORTION', longer_period_suggest)
        exe_amount = math.floor(openable_amount * 10000 * longer_period_suggest) / 10000  # 交易最小单位取小数点后四位
        self.account.short_open(symbol=symbol, price=price, amount=exe_amount, order_type='',
                                leverage=self.account.symbol_info[symbol]['leverage'])

    # 多头平仓
    def long_close(self, symbol, price):
        self.account.long_close(symbol=symbol, price=price,
                                amount=self.account.pos_status[symbol][0], order_type='')

    # 空头平仓
    def short_close(self, symbol, price):
        self.account.short_close(symbol=symbol, price=price,
                                 amount=self.account.pos_status[symbol][0], order_type='')

    # 建立快照
    def take_snapshot(self):
        self.equity_curve['candle_begin_time'].append(self.account.now_time)
        self.equity_curve['equity_curve'].append(self.account.snapshot())
        self.equity_curve['bond'].append(self.account.bond)

    # 计算资金曲线评价{'rtn': float, 'annual_rtn': float, ...} 参考资金曲线的评价代码
    def gen_assessment(self):
        raw_equity_df = pd.DataFrame(self.equity_curve)
        raw_equity_df['total_equity'] = raw_equity_df['equity_curve'] + raw_equity_df['bond']
        equity_df = raw_equity_df[['candle_begin_time', 'total_equity']]
        sql = 'select close from "cta_test"."BTCUSD" ' \
              'where candle_begin_time >= \'%s\' and candle_begin_time < \'%s\'' % (self.start_time, self.end_time)
        benchmark = pd.read_sql_query(sql, con=self.eg)
        equity_df['benchmark'] = benchmark
        equity_df.columns = ['date', 'equity', 'benchmark']
        equity_df['equity'] /= equity_df['equity'].iloc[0]
        equity_curve = EquityCurve(equity_df, 1.03 **
                                   ((datetime.strptime(self.end_time, "%Y-%m-%d %H:%M:%S") -
                                     datetime.strptime(self.start_time, "%Y-%m-%d %H:%M:%S"))
                                    / timedelta(days=365)) - 1)

        equity_df.to_csv(self.equity_df_path)

    # 主回测
    def main_back_test(self):
        signals = []
        tm_gen = self.get_time()
        now_time = next(tm_gen)

        # 设置初始参数
        self.account.set_init_para(self.para_series)
        self.account.check_para_renew(now_time, self.para_series)
        print(self.account.now_para)

        print(self.account.symbol_info)

        # 设置数据
        self.set_data()

        # 遍历直到回测结束
        while now_time <= datetime.strptime(self.end_time, "%Y-%m-%d %H:%M:%S"):
            print(now_time, '===' * 20)
            self.account.now_time = now_time

            # 不可交易时间，直接进入下一个时间
            if not self.check_tradable(now_time=now_time):
                print(now_time, ' NOT TRADABLE')
                self.take_snapshot()
                now_time = next(tm_gen)
                self.account.check_para_renew(now_time, self.para_series)
                continue

            signal_dict = self.get_data_and_cal_signal(now_time)
            print(signal_dict)
            signals.append({'now_time': now_time, 'signal_dict': signal_dict})

            # 根据信号进行调仓
            for symbol in signal_dict.keys():
                if signal_dict[symbol]:
                    for signal in signal_dict[symbol]:
                        price = self.get_bar(symbol=symbol, tm=now_time).values[0][2]  # 当前时间的开盘价
                        # print(self.account.symbol_info)

                        # 需要平仓
                        if signal.endswith('close'):
                            if signal == 'long_close':
                                self.long_close(symbol, price)
                            elif signal == 'short_close':
                                self.short_close(symbol, price)

                        # 需要开仓，先计算可开数量
                        elif signal.endswith('open'):
                            if signal == 'long_open':
                                self.long_open(symbol, price)
                            elif signal == 'short_open':
                                self.short_open(symbol, price)
                        else:
                            raise Exception('WRONG SIGNAL')
                        # print(self.account.symbol_info)

            # 由截止于前一根K线的数据计算得到信号，并且使用本时间开始的K线开盘价调仓后，使用本时间开始的K线的开高收低同步仓位
            price_dict = {}
            for symbol in self.account.symbol_info.keys():
                price_dict[symbol] = {}
                get_time = now_time
                price = self.get_bar(symbol=symbol, tm=get_time).values[0]  # 当前时间的开盘价

                price_dict[symbol]['open'] = price[2]
                price_dict[symbol]['high'] = price[3]
                price_dict[symbol]['low'] = price[4]
                price_dict[symbol]['close'] = price[5]

            # 仓位信息更新
            self.account.check_balance(price_dict=price_dict)
            self.take_snapshot()

            # 如果空仓就rebalance
            empty = True
            for symbol in self.account.pos_status.keys():
                empty = empty and (self.account.pos_status[symbol] == [])
            if empty:
                self.account.re_balance()
                self.account.macro_rebalance()

            # 进入下一个时间，并查看要不要更新参数
            now_time = next(tm_gen)
            self.account.check_para_renew(now_time, self.para_series)
            print(self.account)

        signals_df = pd.DataFrame(signals)
        signals_df.to_csv(self.signals_path)
        change_info = self.account.change_info
        print(change_info)
        change_df = pd.DataFrame(change_info)
        change_df.to_csv(self.change_path)

        return self.gen_assessment()


# 一个BackTest对象只在时间序列上移动一次
if __name__ == '__main__':

    l_localtime = int(time.time())

    l_save_name = '20191123_MacroREBrOriBTC'
    l_output_root_path = r'E:\cta_v2\data\test_v2\output\oo_test'
    for roots, dirs, files in os.walk(l_output_root_path):
        if files:
            for file in files:
                if file.startswith(l_save_name):
                    raise Exception('存档已存在！')

    l_symbol_info = {
        'BTCUSD': {
            'proportion': 1,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
        # 'ETHUSD': {
        #     'proportion': 1,
        #     'para': [200.0, 2.0],
        #     'leverage': 3,
        #     'time_interval': '15T',
        #     'strategy': 'bolling',
        #     'order_method': 'market',
        #     'stop_loss_method': 'float_hard_pct',
        #     'stop_para': [3, 15]
        # },
        # 'EOSUSD': {
        #     'proportion': 1,
        #     'para': [200.0, 2.0],
        #     'leverage': 3,
        #     'time_interval': '15T',
        #     'strategy': 'bolling',
        #     'order_method': 'market',
        #     'stop_loss_method': 'float_hard_pct',
        #     'stop_para': [3, 15]
        # },
                     }

    l_start_time = '2019-01-01 00:00:00'
    l_end_time = '2019-11-23 23:55:00'
    l_order_method = 'market'

    l_path_dict = {
        'BTCUSD': r'long_BTC_bolling_stop_para_record',
        # 'ETHUSD': r'long_ETH_stop_para_record',
        # 'EOSUSD': r'long_EOS_stop_para_record',
    }

    bt1 = MacroTest(
        l_symbol_info,
        l_start_time,
        l_end_time,
        l_path_dict,
        l_output_root_path,
        l_save_name,
        l_localtime
    )

    bt1.main_back_test()
