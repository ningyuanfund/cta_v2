from test_v2.obj_ornt.test_RAM import *
from test_v2.obj_ornt.account import *


class FilterAndFlexAccount(Account):
    def __init__(self, init_cash, start_time, symbol_info):
        Account.__init__(self, init_cash, start_time, symbol_info)
        self.reduce_dict = self.gen_reduce_dict()

    def gen_reduce_dict(self):
        reduce_dict = {}
        # empty：未开仓；reduced：已减仓；full：未减仓
        for symbol in self.symbol_info.keys():
            reduce_dict[symbol] = 'empty'
        return reduce_dict

    # 多头减仓
    def long_reduce(self, symbol, price_dict):
        price = price_dict[symbol]['close']
        reduce_amount = abs(self.pos_status[symbol][0]) / 2

        self.symbol_info[symbol]['available_cash'] += reduce_amount * (price - self.pos_status[symbol][1]) * (1 - 0.003)
        self.pos_status[symbol][0] /= 2  # 持仓量除以2
        self.pos_status[symbol][2] /= 2  # 浮动盈亏除以2
        self.change_info['time'].append(self.now_time)
        self.change_info['change_log'].append([symbol, 'long_reduce'])
        # print(self.pos_status)
        # print(self.symbol_info)
        # exit()
        self.reduce_dict[symbol] = 'reduced'

    # 空头减仓
    def short_reduce(self, symbol, price_dict):
        price = price_dict[symbol]['close']
        print(price)
        reduce_amount = abs(self.pos_status[symbol][0]) / 2
        self.symbol_info[symbol]['available_cash'] += reduce_amount * (self.pos_status[symbol][1] - price) * (1 - 0.003)
        self.pos_status[symbol][0] /= 2  # 持仓量除以2
        self.pos_status[symbol][2] /= 2  # 浮动盈亏除以2
        self.change_info['time'].append(self.now_time)
        self.change_info['change_log'].append([symbol, 'short_reduce'])
        # print(self.pos_status)
        # print(self.symbol_info)
        # exit()
        self.reduce_dict[symbol] = 'reduced'

    # 多头加仓
    def long_increase(self, symbol, price_dict):
        price = price_dict[symbol]['close']
        # 计算最小可开数量。有可能减仓之后一路亏损，导致无法开到原来那么多了
        openable_amount = (self.symbol_info[symbol]['available_cash'] - self.pos_status[symbol][2] + self.pos_status[symbol][3]) / \
                          (price * (0.002 + 1 / self.symbol_info[symbol]['leverage']))
        inc_amount = min(abs(self.pos_status[symbol][0]), openable_amount)

        self.symbol_info[symbol]['available_cash'] -= inc_amount * (self.pos_status[symbol][1] - price) * 0.003
        self.pos_status[symbol][0] *= 2  # 持仓量翻倍
        self.pos_status[symbol][1] = (self.pos_status[symbol][1] + price) / 2  # 平均开仓价格为两次开仓均价
        self.change_info['time'].append(self.now_time)
        self.change_info['change_log'].append([symbol, 'long_increase'])

        self.reduce_dict[symbol] = 'full'

    # 空头加仓
    def short_increase(self, symbol, price_dict):
        price = price_dict[symbol]['close']
        # 计算最小可开数量
        openable_amount = (self.symbol_info[symbol]['available_cash'] - self.pos_status[symbol][2] + self.pos_status[symbol][3]) / \
                          (price * (0.002 + 1 / self.symbol_info[symbol]['leverage']))
        inc_amount = min(abs(self.pos_status[symbol][0]), openable_amount)

        self.symbol_info[symbol]['available_cash'] -= inc_amount * (self.pos_status[symbol][1] - price) * 0.003
        self.pos_status[symbol][0] *= 2  # 持仓量翻倍
        self.pos_status[symbol][1] = (self.pos_status[symbol][1] + price) / 2  # 平均开仓价格为两次开仓均价
        self.change_info['time'].append(self.now_time)
        self.change_info['change_log'].append([symbol, 'short_increase'])
        self.reduce_dict[symbol] = 'full'
        # print(self.pos_status)
        # print(price, openable_amount, inc_amount)
        # print(self.symbol_info)
        # exit()


class FilterAndFlexTest(BackTest):

    # 实例化account
    def gen_init_account(self, now_time, symbol_info):
        return FilterAndFlexAccount(init_cash=self.init_cash, start_time=now_time, symbol_info=symbol_info)

    # 检查大周期的价格位置，确定开仓比例
    def check_longer_period(self, symbol, side):
        now_date = datetime.strptime(self.account.now_time.strftime("%Y-%m-%d 00:00:00"), "%Y-%m-%d %H:%M:%S")
        begin_date = now_date - timedelta(days=25)
        begin = begin_date.strftime("%Y-%m-%d 00:00:00")
        end = now_date.strftime("%Y-%m-%d 00:00:00")
        df = self.get_data(symbol, begin, end)

        data = df.resample(rule='1D', on='candle_begin_time', base=0, label='left', closed='left').agg(
            {'open': 'first',
             'high': 'max',
             'low': 'min',
             'close': 'last',
             'volume': 'sum',
             }
        )
        data['price'] = data[['open', 'high', 'low', 'close']].mean(axis=1)
        data['mid'] = data['price'].rolling(20).mean()
        data['std'] = data['price'].rolling(20).std(ddof=1)
        data['upper'] = data['mid'] + 2 * data['std']
        data['lower'] = data['mid'] - 2 * data['std']
        data = data[:-1].copy()

        # 下轨之下
        if data['price'].iloc[-1] < data['lower'].iloc[-1]:
            if side == 'long':
                return 0.25
            else:
                return 1
        # 下轨中轨之间
        elif (data['price'].iloc[-1] >= data['lower'].iloc[-1]) and (data['price'].iloc[-1] < data['mid'].iloc[-1]):
            if side == 'long':
                return 0.5
            else:
                return 0.75
        # 中轨上轨之间
        elif (data['price'].iloc[-1] >= data['mid'].iloc[-1]) and (data['price'].iloc[-1] < data['upper'].iloc[-1]):
            if side == 'long':
                return 0.75
            else:
                return 0.5
        # 上轨之上
        elif data['price'].iloc[-1] >= data['upper'].iloc[-1]:
            if side == 'long':
                return 1
            else:
                return 0.25

    # 多头开仓
    def long_open(self, symbol, price):
        openable_amount = self.account.symbol_info[symbol]['available_cash'] / \
                          (price * (0.002 + 1 / self.account.symbol_info[symbol]['leverage']))
        # 根据大周期的位置确定仓位比例
        longer_period_suggest = self.check_longer_period(symbol, 'long')
        print('LONG OPEN PROPORTION', longer_period_suggest)
        exe_amount = math.floor(openable_amount * 10000 * longer_period_suggest) / 10000  # 交易最小单位取小数点后四位
        self.account.reduce_dict[symbol] = 'full'
        self.account.long_open(symbol=symbol, price=price, amount=exe_amount, order_type='',
                               leverage=self.account.symbol_info[symbol]['leverage'])

    # 空头开仓
    def short_open(self, symbol, price):
        openable_amount = self.account.symbol_info[symbol]['available_cash'] / \
                          (price * (0.002 + 1 / self.account.symbol_info[symbol]['leverage']))
        self.account.reduce_dict[symbol] = 'full'
        # 根据大周期的位置确定仓位比例
        longer_period_suggest = self.check_longer_period(symbol, 'short')
        print('SHORT OPEN PROPORTION', longer_period_suggest)
        exe_amount = math.floor(openable_amount * 10000 * longer_period_suggest) / 10000  # 交易最小单位取小数点后四位
        self.account.short_open(symbol=symbol, price=price, amount=exe_amount, order_type='',
                                leverage=self.account.symbol_info[symbol]['leverage'])

    # 多头平仓
    def long_close(self, symbol, price):
        self.account.reduce_dict[symbol] = 'empty'
        self.account.long_close(symbol=symbol, price=price,
                                amount=self.account.pos_status[symbol][0], order_type='')

    # 空头平仓
    def short_close(self, symbol, price):
        self.account.reduce_dict[symbol] = 'empty'
        self.account.short_close(symbol=symbol, price=price,
                                 amount=self.account.pos_status[symbol][0], order_type='')

    # 检测小周期是否要减仓，或者是否要把仓位开回来
    def check_reduce_or_increase(self, ignore_list, price_dict):
        """
        MA120 60
        多头：快线下穿慢线，平掉持仓的50%；快线上穿慢线，开回来
        空头：快线上穿慢线，平掉持仓的50%；快线下穿慢线，开回来
        """
        slow = 120
        fast = 60
        for symbol in self.account.symbol_info.keys():
            # 没有持仓，跳过
            if len(self.account.pos_status[symbol]) == 0:
                continue
            # 本周期有开仓，跳过
            elif symbol in ignore_list:
                continue
            begin_time = self.account.now_time - timedelta(minutes=5 * (slow + 3))
            begin = begin_time.strftime("%Y-%m-%d %H:%M:%S")
            end = self.account.now_time.strftime("%Y-%m-%d %H:%M:%S")
            df = self.get_data(symbol=symbol, begin=begin, end=end)
            data = df.reset_index(drop=True)

            # 使用开高收低均价计算均线
            data['price'] = data[['open', 'high', 'low', 'close']].mean(axis=1)
            data['ma_fast'] = data['price'].rolling(fast).mean()
            data['ma_slow'] = data['price'].rolling(slow).mean()
            fast_over_slow = data['ma_fast'].iloc[-1] >= data['ma_slow'].iloc[-1]

            # 多头持仓
            if self.account.pos_status[symbol][0] > 0:
                # print(self.account.symbol_info)
                # 未减仓，需要减仓（即快线小于慢线）
                if (self.account.reduce_dict[symbol] == 'full') and (not fast_over_slow):
                    print('LONG REDUCE')
                    self.account.long_reduce(symbol, price_dict)
                # 已减仓，需要加仓（即快线大于慢线）
                elif (self.account.reduce_dict[symbol] == 'reduced') and fast_over_slow:
                    print('LONG INCREASE')
                    self.account.long_increase(symbol, price_dict)
            # 空头持仓
            elif self.account.pos_status[symbol][0] < 0:
                # print(self.account.symbol_info)
                # 未减仓，需要减仓（即快线大于慢线）
                if (self.account.reduce_dict[symbol] == 'full') and fast_over_slow:
                    print('SHORT REDUCE')
                    self.account.short_reduce(symbol, price_dict)
                # 已减仓，需要加仓（即快线小于慢线）
                elif (self.account.reduce_dict[symbol] == 'reduced') and (not fast_over_slow):
                    print('SHORT INCREASE')
                    self.account.short_increase(symbol, price_dict)
            # print(self.account.symbol_info)

    # 主回测
    def main_back_test(self):
        signals = []
        tm_gen = self.get_time()
        now_time = next(tm_gen)

        # 设置初始参数
        self.account.set_init_para(self.para_series)
        self.account.check_para_renew(now_time, self.para_series)
        print(self.account.now_para)

        print(self.account.symbol_info)

        # 设置数据
        self.set_data()

        # 遍历直到回测结束
        while now_time <= datetime.strptime(self.end_time, "%Y-%m-%d %H:%M:%S"):
            print(now_time, '===' * 20)
            self.account.now_time = now_time

            # 不可交易时间，直接进入下一个时间
            if not self.check_tradable(now_time=now_time):
                print(now_time, ' NOT TRADABLE')
                self.take_snapshot()
                now_time = next(tm_gen)
                self.account.check_para_renew(now_time, self.para_series)
                continue

            signal_dict = self.get_data_and_cal_signal(now_time)
            print(signal_dict)
            signals.append({'now_time': now_time, 'signal_dict': signal_dict})

            # 根据信号进行调仓
            for symbol in signal_dict.keys():
                if signal_dict[symbol]:
                    for signal in signal_dict[symbol]:
                        price = self.get_bar(symbol=symbol, tm=now_time).values[0][2]  # 当前时间的开盘价
                        # print(self.account.symbol_info)

                        # 需要平仓
                        if signal.endswith('close'):
                            if signal == 'long_close':
                                self.long_close(symbol, price)
                            elif signal == 'short_close':
                                self.short_close(symbol, price)

                        # 需要开仓，先计算可开数量
                        elif signal.endswith('open'):
                            if signal == 'long_open':
                                self.long_open(symbol, price)
                            elif signal == 'short_open':
                                self.short_open(symbol, price)
                        else:
                            raise Exception('WRONG SIGNAL')
                        # print(self.account.symbol_info)

            # 由截止于前一根K线的数据计算得到信号，并且使用本时间开始的K线开盘价调仓后，使用本时间开始的K线的开高收低同步仓位
            price_dict = {}
            for symbol in self.account.symbol_info.keys():
                price_dict[symbol] = {}
                get_time = now_time
                price = self.get_bar(symbol=symbol, tm=get_time).values[0]  # 当前时间的开盘价

                price_dict[symbol]['open'] = price[2]
                price_dict[symbol]['high'] = price[3]
                price_dict[symbol]['low'] = price[4]
                price_dict[symbol]['close'] = price[5]

            # 找出本周期有策略或止损调仓的symbol，不干扰原来的策略运行
            ignore_list = [symbol for symbol in signal_dict.keys() if len(signal_dict[symbol]) > 0]
            self.check_reduce_or_increase(ignore_list, price_dict)

            # 仓位信息更新
            self.account.check_balance(price_dict=price_dict)
            self.take_snapshot()

            # 如果空仓就rebalance
            empty = True
            for symbol in self.account.pos_status.keys():
                empty = empty and (self.account.pos_status[symbol] == [])
            if empty:
                self.account.re_balance()

            # 进入下一个时间，并查看要不要更新参数
            now_time = next(tm_gen)
            self.account.check_para_renew(now_time, self.para_series)
            print(self.account)

        signals_df = pd.DataFrame(signals)
        signals_df.to_csv(self.signals_path)
        change_info = self.account.change_info
        print(change_info)
        change_df = pd.DataFrame(change_info)
        change_df.to_csv(self.change_path)

        return self.gen_assessment()


# 一个BackTest对象只在时间序列上移动一次
if __name__ == '__main__':

    l_localtime = int(time.time())

    l_save_name = '20190725_FlexiFilterOriBEE'
    l_output_root_path = r'E:\cta_v2\data\test_v2\output\oo_test'
    for roots, dirs, files in os.walk(l_output_root_path):
        if files:
            for file in files:
                if file.startswith(l_save_name):
                    raise Exception('存档已存在！')

    l_symbol_info = {
        'BTCUSD': {
            'proportion': 1,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
        'ETHUSD': {
            'proportion': 1,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
        'EOSUSD': {
            'proportion': 1,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
                     }

    l_start_time = '2018-01-15 00:00:00'
    l_end_time = '2019-06-30 23:55:00'
    l_order_method = 'market'

    l_path_dict = {
        'BTCUSD': r'long_BTC_stop_para_record',
        'ETHUSD': r'long_ETH_stop_para_record',
        'EOSUSD': r'long_EOS_stop_para_record',
    }

    bt1 = FilterAndFlexTest(
        l_symbol_info,
        l_start_time,
        l_end_time,
        l_path_dict,
        l_output_root_path,
        l_save_name,
        l_localtime
    )

    assessment, equity_df = bt1.main_back_test()
    print(assessment)
