from test_v2.obj_ornt.test_RAM import *


class FilterTest(BackTest):

    # 检查大周期的价格位置，确定开仓比例
    def check_longer_period(self, symbol, side):
        now_date = datetime.strptime(self.account.now_time.strftime("%Y-%m-%d 00:00:00"), "%Y-%m-%d %H:%M:%S")
        begin_date = now_date - timedelta(days=25)
        begin = begin_date.strftime("%Y-%m-%d 00:00:00")
        end = now_date.strftime("%Y-%m-%d 00:00:00")
        df = self.get_data(symbol, begin, end)

        data = df.resample(rule='1D', on='candle_begin_time', base=0, label='left', closed='left').agg(
            {'open': 'first',
             'high': 'max',
             'low': 'min',
             'close': 'last',
             'volume': 'sum',
             }
        )
        data['price'] = data[['open', 'high', 'low', 'close']].mean(axis=1)
        data['mid'] = data['price'].rolling(20).mean()
        data['std'] = data['price'].rolling(20).std(ddof=1)
        data['upper'] = data['mid'] + 2 * data['std']
        data['lower'] = data['mid'] - 2 * data['std']
        data = data[:-1].copy()

        # 下轨之下
        if data['price'].iloc[-1] < data['lower'].iloc[-1]:
            if side == 'long':
                return 0.25
            else:
                return 1
        # 下轨中轨之间
        elif (data['price'].iloc[-1] >= data['lower'].iloc[-1]) and (data['price'].iloc[-1] < data['mid'].iloc[-1]):
            if side == 'long':
                return 0.5
            else:
                return 0.75
        # 中轨上轨之间
        elif (data['price'].iloc[-1] >= data['mid'].iloc[-1]) and (data['price'].iloc[-1] < data['upper'].iloc[-1]):
            if side == 'long':
                return 0.75
            else:
                return 0.5
        # 上轨之上
        elif data['price'].iloc[-1] >= data['upper'].iloc[-1]:
            if side == 'long':
                return 1
            else:
                return 0.25

    # 多头开仓
    def long_open(self, symbol, price):
        openable_amount = self.account.symbol_info[symbol]['available_cash'] / \
                          (price * (0.002 + 1 / self.account.symbol_info[symbol]['leverage']))
        # 根据大周期的位置确定仓位比例
        longer_period_suggest = self.check_longer_period(symbol, 'long')
        print('LONG OPEN PROPORTION', longer_period_suggest)
        exe_amount = math.floor(openable_amount * 10000 * longer_period_suggest) / 10000  # 交易最小单位取小数点后四位
        self.account.long_open(symbol=symbol, price=price, amount=exe_amount, order_type='',
                               leverage=self.account.symbol_info[symbol]['leverage'])

    # 空头开仓
    def short_open(self, symbol, price):
        openable_amount = self.account.symbol_info[symbol]['available_cash'] / \
                          (price * (0.002 + 1 / self.account.symbol_info[symbol]['leverage']))

        # 根据大周期的位置确定仓位比例
        longer_period_suggest = self.check_longer_period(symbol, 'short')
        print('SHORT OPEN PROPORTION', longer_period_suggest)
        exe_amount = math.floor(openable_amount * 10000 * longer_period_suggest) / 10000  # 交易最小单位取小数点后四位
        self.account.short_open(symbol=symbol, price=price, amount=exe_amount, order_type='',
                                leverage=self.account.symbol_info[symbol]['leverage'])

    # 多头平仓
    def long_close(self, symbol, price):
        self.account.long_close(symbol=symbol, price=price,
                                amount=self.account.pos_status[symbol][0], order_type='')

    # 空头平仓
    def short_close(self, symbol, price):
        self.account.short_close(symbol=symbol, price=price,
                                 amount=self.account.pos_status[symbol][0], order_type='')


# 一个BackTest对象只在时间序列上移动一次
if __name__ == '__main__':

    l_localtime = int(time.time())

    l_save_name = '20190724_FilterOriBEE'
    l_output_root_path = r'E:\cta_v2\data\test_v2\output\oo_test'
    for roots, dirs, files in os.walk(l_output_root_path):
        if files:
            for file in files:
                if file.startswith(l_save_name):
                    raise Exception('存档已存在！')

    l_symbol_info = {
        'BTCUSD': {
            'proportion': 1,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
        'ETHUSD': {
            'proportion': 1,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
        'EOSUSD': {
            'proportion': 1,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
                     }

    l_start_time = '2018-01-15 00:00:00'
    l_end_time = '2019-06-30 23:55:00'
    l_order_method = 'market'

    l_path_dict = {
        'BTCUSD': r'long_BTC_stop_para_record',
        'ETHUSD': r'long_ETH_stop_para_record',
        'EOSUSD': r'long_EOS_stop_para_record',
    }

    bt1 = FilterTest(l_symbol_info,
                     l_start_time,
                     l_end_time,
                     l_path_dict,
                     l_output_root_path,
                     l_save_name,
                     l_localtime)

    assessment, equity_df = bt1.main_back_test()
    print(assessment)
