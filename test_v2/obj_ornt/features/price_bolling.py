from test_v2.obj_ornt.test_RAM import *

# 一个BackTest对象只在时间序列上移动一次
if __name__ == '__main__':

    l_localtime = int(time.time())

    l_save_name = '20190723_LogPriceBollingETHOri'
    l_output_root_path = r'E:\cta_v2\data\test_v2\output\oo_test'
    for roots, dirs, files in os.walk(l_output_root_path):
        if files:
            for file in files:
                if file.startswith(l_save_name):
                    raise Exception('存档已存在！')

    l_symbol_info = {

        'ETHUSD': {
            'proportion': 1,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'log_price_bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
                     }

    l_start_time = '2018-01-15 00:00:00'
    l_end_time = '2019-06-30 23:55:00'
    l_order_method = 'market'

    l_path_dict = {
        # 'BTCUSD': r'',
        'ETHUSD': r'long_ETH_log_price_bolling_stop_para_record',
        # 'EOSUSD': r'',
    }

    time_start = time.time()
    bt1 = BackTest(l_symbol_info,
                   l_start_time,
                   l_end_time,
                   l_path_dict,
                   l_output_root_path,
                   l_save_name,
                   l_localtime)

    assessment, equity_df = bt1.main_back_test()
    print(assessment)
    time_end = time.time()
    print(time_start, time_end, time_end-time_start)
