DATA STRUCTURE

=== symbol_info===
{
	'BTCUSD': {
		'proportion': 0.3,
		'para': [290.0, 2.0],
		'leverage': 3,
		'time_interval': '15T',
		'strategy': 'bolling',
		'order_method': 'market',
		'stop_loss_method': 'price_hard_pct',
		'stop_profit_method': 'price_hard_pct',
		'stop_para': [5, 20],
		'available_cash': 1000
	},
	'ETHUSD': {
		'proportion': 0.3,
		'para': [160.0, 1.5],
		'leverage': 3,
		'time_interval': '30T',
		'strategy': 'bolling',
		'order_method': 'market',
		'stop_loss_method': 'price_hard_pct',
		'stop_profit_method': 'price_hard_pct',
		'stop_para': [5, 20],
		'available_cash': 1000
	},
	'EOSUSD': {
		'proportion': 0.2,
		'para': [60.0, 3.0],
		'leverage': 3,
		'time_interval': '1H',
		'strategy': 'bolling',
		'order_method': 'market',
		'stop_loss_method': 'price_hard_pct',
		'stop_profit_method': 'price_hard_pct',
		'stop_para': [4, 20],
		'available_cash': 1000
	},
}


=== position_status ===
{
	symbol: [持仓量，开仓价，初始保证金, 浮动盈亏, 浮动盈亏min, 浮动盈亏max],
	symbol: [],
}

=== signal_dict ===
{
	symbol: ['long_open'],
	symbol: ['short_close', 'long_open']
}

=== price_dict ===
{
	symbol: float,
	symbol: float,
}


=== now_para ===
{
	'usage_end': tm,
	'para': {
		'BTCUSD': ['15T', [290.0, 2.0], [5, 20]],
		'ETHUSD': ['30T', [160.0, 1.5], [5, 20]],
        'EOSUSD': ['1H', [60, 3], [5, 20]]
        	}
}

