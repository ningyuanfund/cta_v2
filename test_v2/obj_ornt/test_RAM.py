import os
import time

import math
from sqlalchemy import create_engine

from test_v2.obj_ornt.EquityCurve import EquityCurve
from test_v2.obj_ornt.account import Account
from test_v2.obj_ornt.external_func import *
from test_v2.obj_ornt.stop_method import *

pd.set_option('expand_frame_repr', False)


class BackTest:
    def __init__(self, symbol_info, start_time, end_time, symbol_para_path_dict, output_root_path, save_name, localtime):
        self.start_time = start_time
        self.end_time = end_time
        self.init_cash = 100000
        self.equity_curve = []
        self.eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')
        self.param_eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')
        self.account = self.gen_init_account(start_time, symbol_info)
        self.equity_curve = {'candle_begin_time': [], 'equity_curve': []}
        self.para_series = self.gen_para_series(symbol_para_path_dict, self.param_eg)
        self.change_path = '%s//%s_%s_change_info.csv' % (output_root_path, save_name, localtime)
        self.signals_path = '%s//%s_%s_signal.csv' % (output_root_path, save_name, localtime)
        self.equity_df_path = '%s//%s_%s_res_test.csv' % (output_root_path, save_name, localtime)
        self.data = {}

    # 设置数据
    def set_data(self):
        sql = 'select * from "cta_test"."BTCUSD"'
        self.data['BTCUSD'] = pd.read_sql_query(sql, con=self.eg)
        sql = 'select * from "cta_test"."ETHUSD"'
        self.data['ETHUSD'] = pd.read_sql_query(sql, con=self.eg)
        for symbol in self.account.symbol_info.keys():
            if symbol in ['BTCUSD', 'ETHUSD']:
                continue
            sql = 'select * from "cta_test"."%s"' % symbol
            self.data[symbol] = pd.read_sql_query(sql, con=self.eg)
            self.data[symbol]['candle_begin_time'] = pd.to_datetime(self.data[symbol]['candle_begin_time'])
            self.data[symbol].sort_values(by='candle_begin_time', inplace=True)

    # 获取参数series
    def gen_para_series(self, symbol_para_path_dict, eg):
        para_series = {}

        for symbol in symbol_para_path_dict.keys():
            sql = 'select * from "params"."%s"' % symbol_para_path_dict[symbol]
            symbol_para_df = pd.read_sql_query(sql, con=eg)
            symbol_para_df['参数训练开始'] = symbol_para_df['参数训练开始'].astype('str')
            symbol_para_df['参数训练结束'] = symbol_para_df['参数训练结束'].astype('str')
            symbol_para_df['参数使用至'] = symbol_para_df['参数使用至'].astype('str')
            symbol_para = []
            for i in range(len(symbol_para_df)):
                strategy_para = [float(x) for x in symbol_para_df['参数'].iloc[i][1: -1].split(',')]
                time_interval = symbol_para_df['时间周期'].iloc[i]
                usage_end = datetime.strptime(symbol_para_df['参数使用至'].iloc[i] + ' 00:00:00', "%Y-%m-%d %H:%M:%S")
                symbol_para.append({'usage_end': usage_end, 'time_interval': time_interval, 'para': strategy_para})
            para_series[symbol] = symbol_para

        return para_series

    # 实例化account
    def gen_init_account(self, now_time, symbol_info):
        return Account(init_cash=self.init_cash, start_time=now_time, symbol_info=symbol_info)

    # 从数据库读取数据 bitfinex_local/cta_test
    def get_data(self, symbol, begin, end):
        # sql = 'select * from "cta_test"."%s" where candle_begin_time >= \'%s\' and candle_begin_time <= \'%s\'' \
        #       % (symbol, begin, end)
        #
        # data = pd.read_sql_query(sql, con=self.eg)
        # data.sort_values(by='index', inplace=True)
        condition1 = self.data[symbol]['candle_begin_time'] >= pd.to_datetime(begin)
        condition2 = self.data[symbol]['candle_begin_time'] <= pd.to_datetime(end)

        return_df = self.data[symbol].loc[condition1 & condition2]

        return return_df

    # 获取bar，用于逐行回测
    def get_bar(self, symbol, tm):
        # sql = 'select * from "cta_test"."%s" where candle_begin_time = \'%s\'' % (symbol, tm)
        # return pd.read_sql_query(sql, con=self.eg)
        condition = self.data[symbol]['candle_begin_time'] == pd.to_datetime(tm)
        return_df = self.data[symbol].loc[condition]
        return return_df

    # 生成时间列，用于遍历数据（独立于数据）
    def get_time(self):
        now_time = datetime.strptime(self.start_time, "%Y-%m-%d %H:%M:%S")
        yield now_time
        while True:
            now_time += timedelta(minutes=5)
            yield now_time

    # 获取数据
    def get_symbol_data(self, symbol, now_time):
        # 获取数据
        if self.account.symbol_info[symbol]['time_interval'] == '15T':
            trans_f = 3
        elif self.account.symbol_info[symbol]['time_interval'] == '30T':
            trans_f = 6
        else:
            trans_f = 12
        minute_gap = (max(self.account.symbol_info[symbol]['para']) + 3) * trans_f * 5

        begin_time = now_time - timedelta(minutes=minute_gap)
        df = self.get_data(symbol=symbol, begin=begin_time, end=now_time)

        df = df[:-1].copy()  # 去掉最新的一根
        df['candle_begin_time'] = pd.to_datetime(df['candle_begin_time'])
        data = df.resample(rule=self.account.symbol_info[symbol]['time_interval'], on='candle_begin_time', base=0,
                           label='left', closed='left').agg(
            {'open': 'first',
             'high': 'max',
             'low': 'min',
             'close': 'last',
             'volume': 'sum',
             }
        )

        return data

    # 计算策略信号
    def cal_strategy_sig(self, symbol, now_time):

        df = self.get_symbol_data(symbol=symbol, now_time=now_time)
        # print(df, symbol)

        # 查看当前持仓
        if self.account.pos_status[symbol]:
            if self.account.pos_status[symbol][0] > 0:
                now_pos = 1
            else:
                now_pos = -1
        else:
            now_pos = 0

        func_str = 'real_time_signal_%s_without_stop_lose(now_pos, df, %s)' % \
                   (self.account.symbol_info[symbol]['strategy'], self.account.symbol_info[symbol]['para'])
        print('NOW', now_pos)
        print('TARGET', eval(func_str))
        # 根据当前、目标仓位计算信号。信号list中需要先平仓信号，再开仓信号
        if now_pos == 1 and eval(func_str) == 0:  # 平多
            sig = ['long_close']
        elif now_pos == -1 and eval(func_str) == 0:  # 平空
            sig = ['short_close']
        elif now_pos == 0 and eval(func_str) == 1:  # 开多
            sig = ['long_open']
        elif now_pos == 0 and eval(func_str) == -1:  # 开空
            sig = ['short_open']
        elif now_pos == 1 and eval(func_str) == -1:  # 平多，开空
            sig = ['long_close', 'short_open']
        elif now_pos == -1 and eval(func_str) == 1:  # 平空，开多
            sig = ['short_close', 'long_open']
        else:
            sig = []

        return sig

    # 计算止盈止损信号
    def cal_stop_sig(self, symbol, now_time, now_pos):

        stop_func_str = '%s(symbol, self.account.pos_status[symbol], now_time,' \
                             'self.account.symbol_info[symbol][\'stop_para\'], self.eg, self.data)' % \
                             (self.account.symbol_info[symbol]['stop_loss_method'])
        whether_stop = eval(stop_func_str)

        stop_signal = False
        if whether_stop and (now_pos > 0):
            stop_signal = 'long_close'
        elif whether_stop and(now_pos < 0):
            stop_signal = 'short_close'

        return stop_signal

    # 检查数据计算信号（包括策略信号和止损信号，此处需要止损止盈）
    def get_data_and_cal_signal(self, now_time):

        signal_dict = {}  # 策略调仓信号

        # 策略不调仓不代表止盈止损不调仓
        checked_list = check_symbol(now_time, self.account.symbol_info)

        for symbol in self.account.symbol_info.keys():

            # 如果本周期检查策略，先检查策略信号
            if symbol in checked_list:
                # print('STRATEGY CHECK SYMBOL %s' % symbol)
                signal_dict[symbol] = self.cal_strategy_sig(symbol=symbol, now_time=now_time)
                print(symbol, 'strategy', signal_dict[symbol])
            else:
                signal_dict[symbol] = []

            # 对有持仓的品种检查止盈止损信号
            if self.account.pos_status[symbol]:
                stop_signal = self.cal_stop_sig(symbol=symbol, now_time=now_time,
                                                now_pos=self.account.pos_status[symbol][0])
                if stop_signal and stop_signal not in signal_dict[symbol]:
                    signal_dict[symbol].append(stop_signal)
                    print(symbol, 'stop', stop_signal)

        return signal_dict

    # 建立快照
    def take_snapshot(self):
        self.equity_curve['candle_begin_time'].append(self.account.now_time)
        self.equity_curve['equity_curve'].append(self.account.snapshot())

    # 是否可交易？BTC和ETH任一成交量 = 0，就认为不可交易
    def check_tradable(self, now_time):

        btc_vol = self.get_bar(symbol='BTCUSD', tm=now_time).values[0][6]
        eth_vol = self.get_bar(symbol='ETHUSD', tm=now_time).values[0][6]

        if (btc_vol > 0) & (eth_vol > 0):
            return True
        else:
            return False

    # 计算资金曲线评价{'rtn': float, 'annual_rtn': float, ...} 参考资金曲线的评价代码
    def gen_assessment(self):
        equity_df = pd.DataFrame(self.equity_curve)
        sql = 'select close from "cta_test"."BTCUSD" ' \
              'where candle_begin_time >= \'%s\' and candle_begin_time < \'%s\'' % (self.start_time, self.end_time)
        benchmark = pd.read_sql_query(sql, con=self.eg)
        equity_df['benchmark'] = benchmark
        equity_df.columns = ['date', 'equity', 'benchmark']
        equity_df['equity'] /= equity_df['equity'].iloc[0]
        equity_curve = EquityCurve(equity_df, 1.03 **
                                   ((datetime.strptime(self.end_time, "%Y-%m-%d %H:%M:%S") -
                                     datetime.strptime(self.start_time, "%Y-%m-%d %H:%M:%S"))
                                    / timedelta(days=365)) - 1)

        equity_df.to_csv(self.equity_df_path)

        return {
                   # 'Annual abnormal rtn': equity_curve.abnormal_annual_rtn(),
                   # 'Abnormal rtn': equity_curve.abnormal_rtn(),
                   # 'Sortino ratio': equity_curve.sortino_ratio(),
                   # 'Sharpe ratio': equity_curve.sharpe_ratio(),
                   # 'Calmar ratio': equity_curve.calmar_ratio()
               }, equity_df

    # 多头平仓
    def long_close(self, symbol, price):
        print(self.account.pos_status)
        print(self.account.symbol_info)
        print(price)
        self.account.long_close(symbol=symbol, price=price,
                                amount=self.account.pos_status[symbol][0], order_type='')
        print(self.account.pos_status)
        print(self.account.symbol_info)
        # exit()

    # 空头平仓
    def short_close(self, symbol, price):
        print(self.account.pos_status)
        print(self.account.symbol_info)
        print(price)
        self.account.short_close(symbol=symbol, price=price,
                                 amount=self.account.pos_status[symbol][0], order_type='')
        print(self.account.pos_status)
        print(self.account.symbol_info)
        # exit()

    # 多头开仓
    def long_open(self, symbol, price):
        print(self.account.pos_status)
        print(self.account.symbol_info)

        openable_amount = self.account.symbol_info[symbol]['available_cash'] / \
                          (price * (0.002 + 1 / self.account.symbol_info[symbol]['leverage']))
        exe_amount = math.floor(openable_amount * 10000) / 10000  # 交易最小单位取小数点后四位
        self.account.long_open(symbol=symbol, price=price, amount=exe_amount, order_type='',
                               leverage=self.account.symbol_info[symbol]['leverage'])
        print(self.account.pos_status)
        print(self.account.symbol_info)

    # 空头开仓
    def short_open(self, symbol, price):
        print(self.account.pos_status)
        print(self.account.symbol_info)
        openable_amount = self.account.symbol_info[symbol]['available_cash'] / \
                          (price * (0.002 + 1 / self.account.symbol_info[symbol]['leverage']))
        exe_amount = math.floor(openable_amount * 10000) / 10000  # 交易最小单位取小数点后四位
        self.account.short_open(symbol=symbol, price=price, amount=exe_amount, order_type='',
                                leverage=self.account.symbol_info[symbol]['leverage'])
        print(self.account.pos_status)
        print(self.account.symbol_info)

    # 主回测
    def main_back_test(self):
        signals = []
        tm_gen = self.get_time()
        now_time = next(tm_gen)

        # 设置初始参数
        self.account.set_init_para(self.para_series)
        self.account.check_para_renew(now_time, self.para_series)
        print(self.account.now_para)

        print(self.account.symbol_info)

        # 设置数据
        self.set_data()

        # 遍历直到回测结束
        while now_time <= datetime.strptime(self.end_time, "%Y-%m-%d %H:%M:%S"):
            print(now_time, '===' * 20)
            self.account.now_time = now_time

            # 不可交易时间，直接进入下一个时间
            if not self.check_tradable(now_time=now_time):
                print(now_time, ' NOT TRADABLE')
                self.take_snapshot()
                now_time = next(tm_gen)
                self.account.check_para_renew(now_time, self.para_series)
                continue

            signal_dict = self.get_data_and_cal_signal(now_time)
            print(signal_dict)
            signals.append({'now_time': now_time, 'signal_dict': signal_dict})

            # 根据信号进行调仓
            for symbol in signal_dict.keys():
                if signal_dict[symbol]:
                    for signal in signal_dict[symbol]:
                        price = self.get_bar(symbol=symbol, tm=now_time).values[0][2]  # 当前时间的开盘价

                        # 需要平仓
                        if signal.endswith('close'):
                            if signal == 'long_close':
                                self.long_close(symbol, price)
                            elif signal == 'short_close':
                                self.short_close(symbol, price)

                        # 需要开仓，先计算可开数量
                        elif signal.endswith('open'):
                            if signal == 'long_open':
                                self.long_open(symbol, price)
                            elif signal == 'short_open':
                                self.short_open(symbol, price)

                        else:
                            raise Exception('WRONG SIGNAL')

            # 由截止于前一根K线的数据计算得到信号，并且使用本时间开始的K线开盘价调仓后，使用本时间开始的K线的开高收低同步仓位
            price_dict = {}
            for symbol in self.account.symbol_info.keys():
                price_dict[symbol] = {}
                get_time = now_time
                price = self.get_bar(symbol=symbol, tm=get_time).values[0]  # 当前时间的开盘价

                price_dict[symbol]['open'] = price[2]
                price_dict[symbol]['high'] = price[3]
                price_dict[symbol]['low'] = price[4]
                price_dict[symbol]['close'] = price[5]

            # 仓位信息更新
            self.account.check_balance(price_dict=price_dict)
            self.take_snapshot()

            # 如果空仓就rebalance
            empty = True
            for symbol in self.account.pos_status.keys():
                empty = empty and (self.account.pos_status[symbol] == [])
            if empty:
                self.account.re_balance()

            # 进入下一个时间，并查看要不要更新参数
            now_time = next(tm_gen)
            self.account.check_para_renew(now_time, self.para_series)
            print(self.account)

        signals_df = pd.DataFrame(signals)
        signals_df.to_csv(self.signals_path)
        change_info = self.account.change_info
        print(change_info)
        change_df = pd.DataFrame(change_info)
        change_df.to_csv(self.change_path)

        return self.gen_assessment()


# 一个BackTest对象只在时间序列上移动一次
if __name__ == '__main__':
    # for symbol in ['BTCUSD', 'ETHUSD', 'EOSUSD', 'LTCUSD', 'XRPUSD', 'BCHUSD', 'BSVUSD']:
    #     df = pd.read_hdf(r'E:\cta_v2\data\test_v2\input\%s.h5' % symbol, key='5T')
    #     df.reset_index(inplace=True)
    #
    #     eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')
    #     df.to_sql(symbol, eg, index=False, if_exists='replace', schema='cta_test')
    #
    # exit()
    l_localtime = int(time.time())

    l_save_name = '20191124_PureOriBTC'
    l_output_root_path = r'E:\cta_v2\data\test_v2\output\oo_test'
    for roots, dirs, files in os.walk(l_output_root_path):
        if files:
            for file in files:
                if file.startswith(l_save_name):
                    raise Exception('存档已存在！')

    l_symbol_info = {

        'BTCUSD': {
            'proportion': 1,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
        }

    l_start_time = '2019-01-15 00:00:00'
    l_end_time = '2019-11-23 23:55:00'
    l_order_method = 'market'

    l_path_dict = {
        'BTCUSD': r'long_BTC_bolling_stop_para_record',
        # 'ETHUSD': r'long_ETH_stop_para_record',
        # 'EOSUSD': r'long_EOS_stop_para_record',
    }

    time_start = time.time()
    bt1 = BackTest(l_symbol_info,
                   l_start_time,
                   l_end_time,
                   l_path_dict,
                   l_output_root_path,
                   l_save_name,
                   l_localtime)

    assessment, equity_df = bt1.main_back_test()
    print(assessment)
    time_end = time.time()
    print(time_start, time_end, time_end-time_start)
# start 20190708 13:50
