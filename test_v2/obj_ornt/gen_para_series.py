from datetime import datetime

import pandas as pd
from sqlalchemy import create_engine

pd.set_option('expand_frame_repr', False)

"""
l_para_series = [{'usage_end': datetime.strptime('2019-06-30 09:30:00', "%Y-%m-%d %H:%M:%S"),
                  'para': {'BTCUSD': ['15T', [290.0, 2.0], [5, 20]],
                           'ETHUSD': ['30T', [160.0, 1.5], [5, 20]],
                           'EOSUSD': ['1H', [60, 3], [5, 20]]
                           }
                  },
                 ]
"""


def gen_para_series(path_dict, eg):
    l_para_series = {}

    for symbol in path_dict.keys():
        sql = 'select * from "params"."%s"' % path_dict[symbol]
        symbol_para_df = pd.read_sql_query(sql, con=eg)
        symbol_para = []
        for i in range(len(symbol_para_df)):
            strategy_para = [float(x) for x in symbol_para_df['参数'].iloc[i][1: -1].split(',')]
            time_interval = symbol_para_df['时间周期'].iloc[i]
            usage_end = datetime.strptime(symbol_para_df['参数使用至'].iloc[i] + ' 00:00:00', "%Y-%m-%d %H:%M:%S")
            symbol_para.append({'usage_end': usage_end, 'time_interval': time_interval, 'para': strategy_para})
        l_para_series[symbol] = symbol_para

    return l_para_series


if __name__ == '__main__':
    path_dict = {
        'BTCUSD': r'long_BTC_stop_para_record',
        'ETHUSD': r'long_ETH_stop_para_record',
    }

    eg = create_engine('postgresql://postgres:thomas@47.75.180.89:5432/bfx_alldata')

    l_para_series = gen_para_series(path_dict, eg)
    print(l_para_series)
