from test_v2.obj_ornt.test_RAM import *


class NearPeriodTest(BackTest):
    # 获取参数series
    def gen_para_series(self, symbol_para_path_dict, eg):
        usage_end = datetime(2019, 7, 19, 23, 59)

        para_series = {'BTCUSD': [{'usage_end': usage_end,
                                  'time_interval': '15T', 'para': [120.0, 2.5, 4.0], 'stop_para': [4.0, 20]}],
                       'ETHUSD': [{'usage_end': usage_end,
                                  'time_interval': '15T', 'para': [180.0, 2.5, 3.0], 'stop_para': [3.0, 20]}],
                       'EOSUSD': [{'usage_end': usage_end,
                                  'time_interval': '30T', 'para': [160.0, 4.5, 3.0], 'stop_para': [3.0, 20]}],
                       'LTCUSD': [{'usage_end': usage_end,
                                  'time_interval': '30T', 'para': [60, 2.0, 4.0], 'stop_para': [4.0, 20]}]
                       }

        return para_series


# 一个BackTest对象只在时间序列上移动一次
if __name__ == '__main__':
    # for symbol in ['BTCUSD', 'ETHUSD', 'EOSUSD', 'LTCUSD', 'XRPUSD', 'BCHUSD', 'BSVUSD']:
    #     df = pd.read_hdf(r'E:\cta_v2\data\test_v2\input\%s.h5' % symbol, key='5T')
    #     df.reset_index(inplace=True)
    #
    #     eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')
    #     df.to_sql(symbol, eg, index=False, if_exists='replace', schema='cta_test')
    #
    # exit()
    l_localtime = int(time.time())

    l_save_name = '2019-7-19-BEEL_rebalance'
    l_output_root_path = r'E:\cta_v2\data\test_v2\output\oo_test'
    for roots, dirs, files in os.walk(l_output_root_path):
        if files:
            for file in files:
                if file.startswith(l_save_name):
                    raise Exception('存档已存在！')

    l_symbol_info = {
        'BTCUSD': {
            'proportion': 0.3,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
        'ETHUSD': {
            'proportion': 0.3,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
        'EOSUSD': {
            'proportion': 0.2,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
        'LTCUSD': {
            'proportion': 0.1,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
                     }

    l_start_time = '2019-07-05 00:00:00'
    l_end_time = '2019-07-18 23:55:00'
    l_order_method = 'market'

    l_path_dict = {
        'BTCUSD': r'long_ETH_stop_para_record',
        'ETHUSD': r'long_ETH_stop_para_record',
        'EOSUSD': r'long_ETH_stop_para_record',
        'LTCUSD': r'long_ETH_stop_para_record',
    }

    time_start = time.time()
    bt1 = NearPeriodTest(l_symbol_info,
                         l_start_time,
                         l_end_time,
                         l_path_dict,
                         l_output_root_path,
                         l_save_name,
                         l_localtime)

    assessment, equity_df = bt1.main_back_test()
    print(assessment)
