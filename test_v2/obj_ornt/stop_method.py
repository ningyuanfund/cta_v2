import pandas as pd


def float_hard_pct(symbol, pos_status, now_time, stop_para, eg, data):
    """
    纯看价格的止盈止损策略
    :param symbol:
    :param pos_status:
    :param now_time:
    :param stop_para:
    :param eg:
    :return:
    """
    # print(symbol, pos_status, stop_para, eg)
    # 当前浮亏，是否止损
    # sql = 'select * from "cta_test"."%s" where candle_begin_time = \'%s\'' % (symbol, now_time)
    #
    # price = pd.read_sql_query(sql, con=eg).values[0][2]  # 当前时间的开盘价
    condition = data[symbol]['candle_begin_time'] == pd.to_datetime(now_time)
    bar = data[symbol].loc[condition]
    price = bar.values[0][2]
    if pos_status:
        # 持有多头仓位
        if pos_status[0] > 0:
            if (price < pos_status[1] * (1 - stop_para[0] / 100)) or (price > pos_status[1] * (1 + stop_para[1] / 100)):
                return True
            else:
                return False
        elif pos_status[0] < 0:
            print('*' * 20)
            print('NOW PRICE', bar.values[0])
            print('NOW STOP LOSS', pos_status[1] * (1 + stop_para[0]))
            print('*' * 20)
            if (price > pos_status[1] * (1 + stop_para[0] / 100)) or (price < pos_status[1] * (1 - stop_para[1] / 100)):
                return True
            else:
                return False
        else:
            return False
    else:
        return False  # 无仓位


def mov_stop(symbol, pos_status, now_time, stop_para, eg, data):
    # print(symbol, pos_status, stop_para, eg)

    # sql = 'select * from "cta_test"."%s" where candle_begin_time = \'%s\'' % (symbol, now_time)
    # price = pd.read_sql_query(sql, con=eg).values[0][2]  # 当前时间的开盘价
    condition = data[symbol]['candle_begin_time'] == pd.to_datetime(now_time)
    bar = data[symbol].loc[condition]
    price = bar.values[0][2]

    if pos_status:
        print('*' * 20, symbol)
        print('NOW PRICE', bar.values[0])
        print('NOW STOP LOSS', pos_status[1] * (1 + stop_para[0]))
        print('*' * 20)
        # 持有多头仓位
        if pos_status[0] > 0:
            condition1 = (pos_status[5] > pos_status[2] * 0.2)
            condition2 = (pos_status[3] < (pos_status[5] * (1 - stop_para[1] / 100)))
            if price < pos_status[1] * (1 - stop_para[0] / 100):
                return True
            # 利润回吐超过最大值的20%
            elif condition1 & condition2:
                return True
            else:
                return False
        # 持有空头仓位
        elif pos_status[0] < 0:

            condition1 = (pos_status[5] > pos_status[2] * 0.2)
            condition2 = (pos_status[3] < (pos_status[5] * (1 - stop_para[1] / 100)))
            if (price > pos_status[1] * (1 + stop_para[0] / 100)) or (price < pos_status[1] * (1 - stop_para[1] / 100)):
                return True
            elif condition1 & condition2:
                return True
            else:
                return False
        else:
            return False
    else:
        return False  # 无仓位