from test_v2.obj_ornt.test_RAM import *


class WinTest(BackTest):

    def check_tradable(self, now_time):

        btc_vol = self.get_bar(symbol='BTCUSD', tm=now_time).values[0][6]
        eth_vol = self.get_bar(symbol='ETHUSD', tm=now_time).values[0][6]

        if (btc_vol > 0) & (eth_vol > 0):
            return True
        else:
            return False


# 一个BackTest对象只在时间序列上移动一次
if __name__ == '__main__':

    l_localtime = int(time.time())

    l_save_name = 'three_rebalance_mov'
    l_output_root_path = r'E:\cta_v2\data\test_v2\output\oo_test'
    for roots, dirs, files in os.walk(l_output_root_path):
        if files:
            for file in files:
                if file.startswith(l_save_name):
                    raise Exception('存档已存在！')

    l_symbol_info = {
        'BTCUSD': {
            'proportion': 0.5,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'mov_stop',
            'stop_para': [3, 15]
        },
        'ETHUSD': {
            'proportion': 0.3,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'mov_stop',
            'stop_para': [3, 15]
        },
        'EOSUSD': {
            'proportion': 0.2,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'mov_stop',
            'stop_para': [3, 15]
        },
                     }

    l_start_time = '2018-01-16 00:00:00'
    l_end_time = '2019-06-30 23:55:00'
    l_order_method = 'market'

    l_path_dict = {
        'BTCUSD': r'long_BTC_stop_para_record',
        'ETHUSD': r'long_ETH_stop_para_record',
        'EOSUSD': r'long_EOS_stop_para_record',
    }

    bt1 = WinTest(l_symbol_info,
                  l_start_time,
                  l_end_time,
                  l_path_dict,
                  l_output_root_path,
                  l_save_name,
                  l_localtime)

    assessment, equity_df = bt1.main_back_test()
    print(assessment)
