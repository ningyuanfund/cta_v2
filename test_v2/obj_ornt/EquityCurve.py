import pandas as pd
import numpy as np
pd.set_option('expand_frame_repr', False)


class EquityCurve:
    def __init__(self, equity_curve, rf):
        equity_curve.columns = ['date', 'equity', 'benchmark']
        equity_curve['rtn'] = equity_curve['equity'] / equity_curve['equity'].shift() - 1
        equity_curve.reset_index(inplace=True, drop=True)
        equity_curve['rtn'].fillna(value=0, inplace=True)

        self.rf = rf
        self.equity_curve = equity_curve

    # 标的年化收益
    def target_annual_rtn(self):
        equity_curve = self.equity_curve
        result = (equity_curve['equity'].iloc[-1] / equity_curve['equity'].iloc[0]) ** \
                 ('1 days 00:00:00' / (equity_curve['date'].iloc[-1] - equity_curve['date'].iloc[0]) * 250) - 1
        return result

    # 基准年化收益
    def benchmark_annual_return(self):
        equity_curve = self.equity_curve
        result = (equity_curve['benchmark'].iloc[-1] / equity_curve['benchmark'].iloc[0]) ** \
                 ('1 days 00:00:00' / (equity_curve['date'].iloc[-1] - equity_curve['date'].iloc[0]) * 250) - 1
        return result

    # 总超额收益
    def abnormal_rtn(self):
        equity_curve = self.equity_curve
        target_rtn = equity_curve['equity'].iloc[-1] / equity_curve['equity'].iloc[0] - 1
        benchmark_rtn = equity_curve['benchmark'].iloc[-1] / equity_curve['benchmark'].iloc[0] - 1
        return target_rtn - benchmark_rtn

    # 年化超额收益
    def abnormal_annual_rtn(self):
        target_rtn = self.target_annual_rtn()
        benchmark_rtn = self.benchmark_annual_return()
        return target_rtn - benchmark_rtn

    # 年化波动率
    def annual_volatility(self):
        equity_curve = self.equity_curve
        return equity_curve['rtn'].std(ddof=1) * np.sqrt(250)

    # 年化跟踪误差
    def annual_te(self):
        equity_curve = self.equity_curve
        equity_curve['benchmark_rtn'] = equity_curve['benchmark'] / equity_curve['benchmark'].shift() - 1
        equity_curve['td'] = equity_curve['rtn'] - equity_curve['benchmark_rtn']
        result = equity_curve['td'].std(ddof=1) * np.sqrt(250)
        return result

    # 最大回撤
    def max_dd(self):
        equity_curve = self.equity_curve
        equity_curve['rolling_max'] = equity_curve['equity'].expanding().max()
        equity_curve['dd'] = equity_curve['equity'] / equity_curve['rolling_max'] - 1
        return equity_curve['dd'].min()

    # 夏普比率
    def sharpe_ratio(self):
        self.equity_curve['year'] = self.equity_curve['date'].dt.year
        self.equity_curve['month'] = self.equity_curve['date'].dt.month
        self.equity_curve['ym'] = self.equity_curve['year'].astype('str') + self.equity_curve['month'].astype(str)

        t = self.equity_curve.groupby('ym').apply(lambda x: (1 + x['rtn']).prod())
        result = (t.mean() - self.rf) / t.std(ddof=1)
        return result

    # sortino比率
    def sortino_ratio(self):
        self.equity_curve['year'] = self.equity_curve['date'].dt.year
        self.equity_curve['month'] = self.equity_curve['date'].dt.month
        self.equity_curve['ym'] = self.equity_curve['year'].astype('str') + self.equity_curve['month'].astype(str)

        t = self.equity_curve.groupby('ym').apply(lambda x: (1 + x['rtn']).prod())
        if len(t.loc[t.values < 0]) > 0:
            result = (t.mean() - self.rf) / t.loc[t.values < 0].std(ddof=1)
        else:
            result = 999999
        return result

    # calmar比率
    def calmar_ratio(self):
        annual_rtn = self.target_annual_rtn()
        max_dd = abs(self.max_dd())
        result = abs(annual_rtn / max_dd)
        return result


if __name__ == '__main__':
    data = {'da': [1,4,8,1,4,8,1,4,8]}
    df = pd.DataFrame(data)
    print(df.mean().values)
    print(df.std(ddof=1).values)

    data = {'da': [1,1,1,1,9,1,1,1,8]}
    df = pd.DataFrame(data)
    print(df.mean().values)
    print(df.std(ddof=1).values)