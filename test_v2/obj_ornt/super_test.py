from test_v2.obj_ornt.back_test import *


class SuperTest(BackTest):
    def __init__(self, symbol_info, start_time, end_time, symbol_para_path_dict, output_root_path, save_name, localtime):
        BackTest.__init__(self, symbol_info, start_time, end_time, symbol_para_path_dict, output_root_path, save_name, localtime)
        self.data = {}
        self.para_path = symbol_para_path_dict

    # 从数据库读取数据 bitfinex_local/cta_test
    def get_data(self, symbol, begin, end):
        if self.data == {}:
            for symbol in self.account.symbol_info.keys():
                self.data[symbol] = pd.read_csv(self.para_path[symbol], parse_dates=['candle_begin_time'])
                self.data[symbol].sort_values(by='candle_begin_time', inplace=True)
        condition1 = self.data[symbol]['candle_begin_time'] >= pd.to_datetime(begin)
        condition2 = self.data[symbol]['candle_begin_time'] <= pd.to_datetime(end)

        return_df = self.data[symbol].loc[condition1 & condition2]
        # sql = 'select * from "cta_test"."%s" where candle_begin_time >= \'%s\' and candle_begin_time <= \'%s\'' \
        #       % (symbol, begin, end)
        #
        # data = pd.read_sql_query(sql, con=self.eg)
        # data.sort_values(by='index', inplace=True)
        return return_df

    # 获取bar，用于逐行回测
    def get_bar(self, symbol, tm):
        if self.data == {}:
            for symbol in self.account.symbol_info.keys():
                self.data[symbol] = pd.read_csv(self.para_path[symbol], parse_dates=['candle_begin_time'])
                self.data[symbol].sort_values(by='candle_begin_time', inplace=True)
        condition1 = self.data[symbol]['candle_begin_time'] == pd.to_datetime(tm)
        return_df = self.data[symbol].loc[condition1]
        return return_df
        # sql = 'select * from "cta_test"."%s" where candle_begin_time = \'%s\'' % (symbol, tm)
        # return pd.read_sql_query(sql, con=self.eg)

    # 获取参数series
    def gen_para_series(self, symbol_para_path_dict, eg):
        para_series = {}

        for symbol in symbol_para_path_dict.keys():
            symbol_para_df = pd.read_csv(symbol_para_path_dict[symbol])
            symbol_para = []
            for i in range(len(symbol_para_df)):
                strategy_para = [float(x) for x in symbol_para_df['参数'].iloc[i][1: -1].split(',')]
                time_interval = symbol_para_df['时间周期'].iloc[i]
                usage_end = datetime.strptime(symbol_para_df['参数使用至'].iloc[i] + ' 00:00:00', "%Y-%m-%d %H:%M:%S")
                symbol_para.append({'usage_end': usage_end, 'time_interval': time_interval, 'para': strategy_para})
            para_series[symbol] = symbol_para

        return para_series


# 一个BackTest对象只在时间序列上移动一次
if __name__ == '__main__':
    # for symbol in ['BTCUSD', 'ETHUSD', 'EOSUSD', 'LTCUSD', 'XRPUSD', 'BCHUSD', 'BSVUSD']:
    #     df = pd.read_hdf(r'E:\cta_v2\data\test_v2\input\%s.h5' % symbol, key='5T')
    #     df.reset_index(inplace=True)
    #
    #     eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')
    #     df.to_sql(symbol, eg, index=False, if_exists='replace', schema='cta_test')
    #
    # exit()
    l_localtime = int(time.time())

    l_save_name = 'long_ETH_ori'
    l_output_root_path = r'E:\cta_v2\data\test_v2\output\oo_test'
    for roots, dirs, files in os.walk(l_output_root_path):
        if files:
            for file in files:
                if file.startswith(l_save_name):
                    raise Exception('存档已存在！')

    l_symbol_info = {

        'ETHUSD': {
            'proportion': 1,
            'para': [200.0, 2.0],
            'leverage': 3,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
                     }

    l_start_time = '2018-01-15 00:00:00'
    l_end_time = '2019-06-30 23:55:00'
    l_order_method = 'market'

    l_path_dict = {
        # 'BTCUSD': r'',
        'ETHUSD': r'long_ETH_stop_para_record',
        # 'EOSUSD': r'',
    }

    bt1 = SuperTest(l_symbol_info,
                   l_start_time,
                   l_end_time,
                   l_path_dict,
                   l_output_root_path,
                   l_save_name,
                   l_localtime)

    assessment, equity_df = bt1.main_back_test()
    print(assessment)