#!/usr/bin/python
# -*- coding: utf-8 -*-
# Last update on: 18/12/20

import requests as rq
from bs4 import BeautifulSoup
from retrying import retry
from threading import Thread
from queue import Queue
import time
from datetime import datetime, timedelta
from dateutil.parser import parse
import math
import pandas as pd
import mod_util as util
from mod_sql import CryptoSQL

# IMPORTANT
pd.set_option('mode.use_inf_as_na', True)
# query date range:
_st = '20130401'
# utc date
_nt = util.utc_date_string(util.unix_timestamp() - 24 * 3600, '%Y%m%d')
# 最大重试次数和最大线程数
_max_retry = 16
_max_thread = 64

user = input('Username: ')
pwd = input('Password: ')
sql = CryptoSQL(host='95.169.14.2:5432', user=user, password=pwd)


# get list of all cryptocurrencies from coinmarketcap.com
# return a DataFrame
@retry(stop_max_attempt_number=_max_retry)
def get_crypto_list() -> list:
    query_url = "https://coinmarketcap.com/all/views/all/"
    session = rq.Session()
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"
    }
    r = session.get(query_url, headers=headers, timeout=10)

    html = BeautifulSoup(r.text, "lxml")
    table = html.find("table", {"id": "currencies-all"})
    rows = table.tbody.findAll("tr")
    # column names
    data_line_list = []
    for row in rows:
        data_row = {}
        # name of cryptocurrency (string)
        name = row.find("td", {"class": "currency-name"})
        data_row['name'] = name.attrs["data-sort"]
        # link of cryptocurrency (string)
        # e.g. bitcoin -> https://coinmarketcap.com/currencies/bitcoin
        href = name.find('a').attrs["href"].split('/')[-2]
        data_row['cmc_id'] = href
        # symbol of cryptocurrency (string)
        sym = row.find("td", {"class": "col-symbol"})
        data_row['symbol'] = sym.get_text()
        data_line_list.append(data_row)

    return data_line_list


# get history data from coinmarketcap.com of certain currency
@retry(stop_max_attempt_number=_max_retry, wait_random_min=1000, wait_random_max=1000)
def get_historical_data(currency: str, start_time: str = _st, end_time: str = _nt) -> list:
    # query url
    coinmarketcap = "https://coinmarketcap.com/currencies/"
    query_url = coinmarketcap + currency + "/historical-data/?" + "start=" + start_time + "&end=" + end_time
    # print("start query:", currency, "URL:", queryUrl)
    session = rq.Session()
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"
    }
    # print(currency, startTime, endTime)
    r = session.get(query_url, headers=headers, timeout=60)

    if r is None:
        raise Exception('Error When Parsing HTML: No Response - ' + currency)

    html = BeautifulSoup(r.text, "lxml")
    # print("get response:", currency)
    hs = html.find("div", {"id": "historical-data"})
    if hs is None:
        # sleep
        # print('Sleep for 30s then retry..')
        time.sleep(30)
        raise Exception('Error When Parsing HTML: Cannot Find Table - ' + currency)

    table = hs.table
    # get lines of table's body
    rows = table.tbody.findAll("tr")
    # if rows are empty
    if not len(rows):
        return []

    # create a list saving rows
    data_line_list = []
    for row in rows:
        data_row = []
        # if td is empty
        if len(row.findAll("td")) < 2:
            return []
        # begin fetch data
        for cell in row.findAll("td"):
            if "data-format-value" in cell.attrs:
                # get float data from "data-format-value" attribute
                n = cell.attrs["data-format-value"]
                if n == '-':
                    n = 0
                data_row.append(float(n))
            else:
                # for text values
                # local to utc
                timestamp = int(parse(cell.get_text()).timestamp() - util.local_to_utc())
                data_row.append(datetime.utcfromtimestamp(timestamp))
                data_row.append(timestamp)
        # judge if this row is empty
        if len(data_row):
            data_line_list.append(data_row)

    data_line_list.sort(key=lambda x: x[0], reverse=False)
    # 检查是否有漏掉的天数
    if (data_line_list[0][0] - datetime.strptime(start_time, '%Y%m%d')).days > 0:
        data = [data_line_list[0]]
    else:
        # _st 日期后一天开始
        while (data_line_list[0][0] - datetime.strptime(start_time, '%Y%m%d')).days <= 0:
            del data_line_list[0]
        data = [data_line_list[0]]
    for i in range(1, len(data_line_list)):
        delta = data_line_list[i][0] - data_line_list[i - 1][0]
        if delta.days > 1:
            for j in range(1, delta.days):
                d = data_line_list[i - 1].copy()
                d[0] = d[0] + timedelta(j)
                d[1] = d[0].timestamp()
                data.append(d)
        data.append(data_line_list[i])
    return data


# get all currencies' historical data
# use list from coinmarketcap
def get_all_historical_data(num: int = 0, nt: str = _nt, method: str = '') -> list:
    if num > 0:
        lists = read_crypto_list()[:num]
    else:
        lists = read_crypto_list()
    _complete = 0
    _Q = Queue()
    _crypto = [None] * len(lists)

    def thread_worker():
        while True:
            message = _Q.get()
            if message is None:
                break
            coin_id = message['coin_id']
            symbol = message['Symbol']
            link = message['Link']
            number = message['ID']
            last_record = message['LastRecord']
            message['Retry'] += 1
            # print(message)
            if (not last_record) or (last_record < _st) or (method == 'replace'):
                # doesn't exist
                try:
                    data = get_historical_data(link, _st, nt)
                except Exception as err:
                    # print(err)
                    if message['Retry'] >= 32:
                        print(link, ': Max retry exceeded...')
                        data = []
                    else:
                        _Q.put(message)
                        _Q.task_done()
                        continue

            elif last_record >= nt:
                # doesn't need update
                data = []
                print('Nothing to update for:', link)
            else:
                # drop first line
                try:
                    data = get_historical_data(link, last_record, nt)
                except Exception as err:
                    # print(err)
                    if message['Retry'] >= 32:
                        data = []
                    else:
                        _Q.put(message)
                        _Q.task_done()
                        continue

            nonlocal _complete
            nonlocal _crypto
            for n, record in enumerate(data):
                data[n] = {'coin_id': coin_id,
                           'symbol': symbol,
                           'time': record[0],
                           'timestamp': record[1],
                           'open': record[2],
                           'high': record[3],
                           'low': record[4],
                           'close': record[5],
                           'vol': record[6],
                           'marketcap': record[7]}
            try:
                _crypto[number] = calculate_historical_data(data)
            except Exception as err:
                print(err, number)
            finally:
                _complete += 1
            print('Get Historical Data Complete:', _complete, 'of', len(lists), ':', link, len(data), 'Lines Fetched.')
            _Q.task_done()

    # create threads
    threads = []
    for i in range(_max_thread):
        thread = Thread(target=thread_worker)
        thread.daemon = True
        thread.start()
        threads.append(thread)
    # put tasks
    for i, coin in enumerate(lists):
        if 'cmc_update' in coin['meta'].keys():
            last = coin['meta']['cmc_update']
        else:
            last = _st
        _Q.put({"coin_id": coin['pk_id'], "ID": i, "Link": coin['cmc_id'], "Symbol": coin['symbol'], "LastRecord": last, "Retry": 0})
        time.sleep(0.2)
    _Q.join()
    # stop workers
    for i in range(len(threads)):
        _Q.put(None)
    for t in threads:
        t.join()
    # do final works
    # return _crypto
    a = []
    for d in _crypto:
        if d is None:
            continue
        else:
            # if empty
            if not len(d):
                continue
            a += d
    print(len(a), 'new records.')
    return a


def calculate_historical_data(d: list) -> list:
    if not len(d):
        return []
    coin_id = d[0]['coin_id']
    data = read_crypto_historical_src_data(coin_id, limit=45)
    data.reverse()
    data = data + d
    df = pd.DataFrame(data).set_index('time').sort_index()

    circulate = df["marketcap"] / df["close"]
    circulate.name = 'circulate'
    diff = df["close"].diff()
    diff.name = 'diff'
    change = df["close"].pct_change(freq='D')
    change.name = "chg"
    change_7d = df["close"].pct_change(freq='D', periods=7)
    change_7d.name = "chg_7d"
    change_30d = df["close"].pct_change(freq='D', periods=30)
    change_30d.name = "chg_30d"
    range_30d = df["high"].rolling(30, min_periods=30).max() - df["low"].rolling(30, min_periods=30).min()
    range_30d.name = "rng_30d"
    amplitude = range_30d * (1 + change_30d) / df["close"]
    amplitude.name = "amp_30d"
    turnover = df["vol"] / df["marketcap"]
    turnover.name = "tor"
    turnover_ma7 = turnover.rolling(7, min_periods=7).mean()
    turnover_ma7.name = "tor_ma7"
    turnover_ma30 = turnover.rolling(30, min_periods=30).mean()
    turnover_ma30.name = "tor_ma30"
    volume_in_token = df["vol"] / df["open"]
    volume_in_token.name = "volt"
    volume_in_token_ma7 = volume_in_token.rolling(7, min_periods=7).mean()
    volume_in_token_ma7.name = "volt_ma7"
    volume_in_token_ma30 = volume_in_token.rolling(30, min_periods=30).mean()
    volume_in_token_ma30.name = "volt_ma30"
    ma7 = df["close"].rolling(7, min_periods=7).mean()
    ma7.name = "ma7"
    ma30 = df["close"].rolling(30, min_periods=30).mean()
    ma30.name = "ma30"
    vol_ma7 = df["vol"].rolling(7, min_periods=7).mean()
    vol_ma7.name = "vol_ma7"
    vol_ma30 = df["vol"].rolling(30, min_periods=30).mean()
    vol_ma30.name = "vol_ma30"
    volr = df["vol"] / vol_ma7
    volr.name = "volr"
    # judge if currency's market cap is above certain amount
    is_above1b = df["marketcap"].map(lambda x: int(x > 1e9))
    is_above1b.name = "above_1b"
    is_above100m = df["marketcap"].map(lambda x: int(x > 1e8))
    is_above100m.name = "above_100m"
    is_above10m = df["marketcap"].map(lambda x: int(x > 1e7))
    is_above10m.name = "above_10m"
    is_above1m = df["marketcap"].map(lambda x: int(x > 1e6))
    is_above1m.name = "above_1m"
    is_above100k = df["marketcap"].map(lambda x: int(x > 1e5))
    is_above100k.name = "above_100k"
    # 计算对数收益率以计算历史波动率
    log_ = df["close"].map(lambda x: math.log(x) if x > 0 else 0)
    log_return = log_.diff()
    log_return.name = "log_chg"
    volatility_30d = log_return.rolling(30, min_periods=30).std() * math.sqrt(30)
    volatility_30d.name = "vola_30d"

    df = df.join(
        [circulate, ma7, ma30, diff, change, change_7d, change_30d, log_return, range_30d, amplitude,
         volume_in_token, volume_in_token_ma7, volume_in_token_ma30, vol_ma7, vol_ma30,
         turnover, turnover_ma7, turnover_ma30, volr, volatility_30d, is_above1b, is_above100m, is_above10m, is_above1m, is_above100k])
    df = df.fillna(0)
    # Reindex
    df = df.reset_index()
    r = df.to_dict(orient='records')
    # 删除重复的数据，pk_id字段，并将 timestamp 类型转换到 datetime
    first_timestamp = d[0]['timestamp']
    r = [{k: datetime.utcfromtimestamp(v.timestamp()) if type(v) == pd.Timestamp else v for k, v in i.items() if k != 'pk_id'}
         for i in r if i['timestamp'] >= first_timestamp]
    return r


# 需要重写
def cal_market_data() -> pd.DataFrame:
    lists = read_crypto_list()
    portfolio = list(map(lambda x: x['pk_id'], lists))

    r = pd.DataFrame()
    for i in range(len(portfolio)):
        hs = read_crypto_historical_src_data(i, limit=180, returned='dataframe')
        if hs.empty:
            continue
        # Reindex
        hs = hs.set_index('Time').sort_index()
        df = hs.reindex(columns=['Market_Cap', 'Volume']).fillna(0)

        if r.empty:
            r = df.copy()
        else:
            r = r.add(df, fill_value=0)
        if (i + 1) % 100 == 0:
            print("Calculating Market Summary:", i + 1, "of", len(portfolio))

    diff = r["Market_Cap"].diff()
    diff.name = 'Difference'
    change = r["Market_Cap"].pct_change(freq='D')
    change.name = "Change"
    change_7d = r["Market_Cap"].pct_change(freq='D', periods=7)
    change_7d.name = "Change 7D"
    change_30d = r["Market_Cap"].pct_change(freq='D', periods=30)
    change_30d.name = "Change 30D"
    vol_ma7 = r["Volume"].rolling(7, min_periods=7).mean()
    vol_ma7.name = "Volume MA7"
    vol_ma30 = r["Volume"].rolling(30, min_periods=30).mean()
    vol_ma30.name = "Volume MA30"
    volr = r["Volume"] / vol_ma7
    volr.name = "Volume Ratio"
    turnover = r["Volume"] / r["Market_Cap"]
    turnover.name = "Turnover Rate"
    turnover_ma7 = turnover.rolling(7, min_periods=7).mean()
    turnover_ma7.name = "Turnover MA7"
    turnover_ma30 = turnover.rolling(30, min_periods=30).mean()
    turnover_ma30.name = "Turnover MA30"

    r = r.join([diff, change, change_7d, change_30d, vol_ma7, vol_ma30, volr, turnover, turnover_ma7, turnover_ma30]).fillna(0)
    return r


def analyze_crypto_list() -> tuple:
    data_new = get_crypto_list()
    data = read_crypto_list(returned='dict')
    i = 0
    while i < len(data_new):
        for j, d in enumerate(data):
            if data_new[i]['cmc_id'] == d['cmc_id']:
                del data[j]
                del data_new[i]
                i -= 1
                break
        i += 1
    print(len(data_new), 'new lines will be inserted.')
    print(len(data), 'lines will be deleted.')
    return data_new, data


def update_crypto_list(insert, deprecated) -> int:
    if len(deprecated):
        sql.save('crypto_basic',
                 [{'pk_id': x['pk_id'],
                   'meta': {"cmc_update": "deprecated"}} for x in deprecated],
                 method='update')
    if len(insert):
        sql.save('crypto_basic', insert, method='insert')
    return 0


def read_crypto_list(returned='dict'):
    r = sql.read('crypto_basic',
                 columns=('pk_id', 'symbol', 'cmc_id', 'meta'),
                 where="meta ->> 'cmc_update' != 'deprecated' OR meta ->> 'cmc_update' ISNULL",
                 sort_by=('pk_id',),
                 ascending=(True,),
                 returned=returned)
    return r


def update(d: list) -> int:
    sql.save('cmc_src', d, method='insert')
    reload_metadata()
    print('save to SQL complete.')
    return 0


def read_crypto_historical_src_data(coin_id: int, limit: int = 0, returned='dict'):
    r = sql.read('cmc_src',
                 columns=range(0, 11),
                 where='coin_id = ' + str(coin_id),
                 sort_by=('timestamp',),
                 ascending=(False,),
                 limit=limit,
                 returned=returned)
    return r


def read_crypto_historical_data(coin_id: int, returned='dict'):
    r = sql.read('cmc_src',
                 where='coin_id = ' + str(coin_id),
                 sort_by=('timestamp',),
                 ascending=(False,),
                 returned=returned)
    return r


def reload_metadata():
    lists = read_crypto_list()
    meta = []
    for coin in lists:
        meta_json = coin['meta']
        latest = sql.read('cmc_src',
                          columns=('timestamp',),
                          where='coin_id = ' + str(coin['pk_id']),
                          sort_by=('timestamp',),
                          ascending=(False,),
                          limit=1,
                          returned='list')
        if len(latest):
            meta_json['cmc_update'] = util.utc_date_string(int(latest[0][0]), '%Y%m%d')
        else:
            meta_json['cmc_update'] = _st
        meta.append({'pk_id': coin['pk_id'], 'meta': meta_json})
    sql.save('crypto_basic', meta, method='update')
    return 0
