#!/usr/bin/python
# -*- coding: utf-8 -*-
# Last update on: 18/12/21

from sqlalchemy import create_engine, Column, ForeignKey
from sqlalchemy.schema import UniqueConstraint
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.postgresql import SMALLINT, INTEGER, BIGINT, VARCHAR, TIMESTAMP, NUMERIC, DOUBLE_PRECISION, JSON
import pandas as pd


def col_numbers(name, size='F'):
    if size == 'DS':
        return Column(NUMERIC(16, 6), name=name, default=0)
    elif size == 'DM':
        return Column(NUMERIC(24, 8), name=name, default=0)
    elif size == 'DL':
        return Column(NUMERIC(32, 10), name=name, default=0)
    elif size == 'F':
        return Column(DOUBLE_PRECISION, name=name, default=0)
    elif size == 'SI':
        return Column(SMALLINT, name=name, default=0)
    elif size == 'I':
        return Column(INTEGER, name=name, default=0)
    elif size == 'LI':
        return Column(BIGINT, name=name, default=0)
    else:
        return Column(DOUBLE_PRECISION, name=name, default=0)


_crypto_basic_dbcol = {
    'ID': Column(INTEGER, name='pk_id', primary_key=True, autoincrement=True, nullable=False),
    'Symbol': Column(VARCHAR(16), name='symbol', nullable=False),
    'Name': Column(VARCHAR(64), name='name', nullable=False),
    'Name_CN': Column(VARCHAR(64), name='name_cn', default=''),
    'CMC_ID': Column(VARCHAR(64), name='cmc_id', nullable=False),
    'COMP_ID': Column(VARCHAR(16), name='comp_id', default=''),
    'Description': Column(VARCHAR(512), name='description', default=''),
    'Supplies': Column(DOUBLE_PRECISION, name='supplies', default=0),
    'Industry': Column(VARCHAR(16),
                       ForeignKey('crypto_ind.symbol', ondelete='CASCADE', onupdate='CASCADE'),
                       name='ind_symbol', default='M'),
    'Meta': Column(JSON, name='meta', default={})
}
_crypto_ind_dbcol = {
    'ID': Column(INTEGER, name='pk_id', primary_key=True, autoincrement=True, nullable=False),
    'Symbol': Column(VARCHAR(16), name='symbol', unique=True, nullable=False),
    'Name': Column(VARCHAR(16), name='name', nullable=False),
    'Super': Column(VARCHAR(16), name='super', default=''),
    'Level': Column(SMALLINT, name='level', default=0),
    'Description': Column(VARCHAR(512), name='description', default='')
}
_crypto_exc_dbcol = {
    'ID': Column(INTEGER, name='pk_id', primary_key=True, autoincrement=True, nullable=False),
    'Symbol': Column(VARCHAR(32), name='symbol', unique=True, nullable=False),
    'Name': Column(VARCHAR(64), name='name', nullable=False),
    'Name_CN': Column(VARCHAR(64), name='name_cn', default=''),
    'Description': Column(VARCHAR(512), name='description', default='')
}

_cmc_src_numbers = {'Open': ['open', 'F'],
                    'High': ['high', 'F'],
                    'Low': ['low', 'F'],
                    'Close': ['close', 'F'],
                    'Volume': ['vol', 'F'],
                    'Market_Cap': ['marketcap', 'F'],
                    'Circulate': ['circulate', 'F'],
                    'MA7': ['ma7', 'F'],
                    'MA30': ['ma30', 'F'],
                    'Difference': ['diff', 'F'],
                    'Change': ['chg', 'F'],
                    'Change_7D': ['chg_7d', 'F'],
                    'Change_30D': ['chg_30d', 'F'],
                    'Log_Change': ['log_chg', 'F'],
                    'Range_30D': ['rng_30d', 'F'],
                    'Amplitude_30D': ['amp_30d', 'F'],
                    'Volume_in_Token': ['volt', 'F'],
                    'Volume_in_Token_MA7': ['volt_ma7', 'F'],
                    'Volume_in_Token_MA30': ['volt_ma30', 'F'],
                    'Volume_MA7': ['vol_ma7', 'F'],
                    'Volume_MA30': ['vol_ma30', 'F'],
                    'Turnover_Rate': ['tor', 'F'],
                    'Turnover_Rate_MA7': ['tor_ma7', 'F'],
                    'Turnover_Rate_MA30': ['tor_ma30', 'F'],
                    'Volume_Ratio': ['volr', 'F'],
                    'Volatility_30D': ['vola_30d', 'F'],
                    'Above_1B': ['above_1b', 'I'],
                    'Above_100M': ['above_100m', 'I'],
                    'Above_10M': ['above_10m', 'I'],
                    'Above_1M': ['above_1m', 'I'],
                    'Above_100K': ['above_100k', 'I']}
_cmc_src_dbcol = {
    'ID': Column(INTEGER, name='pk_id', primary_key=True, autoincrement=True, nullable=False),
    'Coin_ID': Column(INTEGER,
                      ForeignKey('crypto_basic.pk_id', ondelete='CASCADE', onupdate='CASCADE'),
                      name='coin_id', nullable=False),
    'Symbol': Column(VARCHAR(16), name='symbol', nullable=False),
    'Time': Column(TIMESTAMP, name='time', nullable=False),
    'Timestamp': Column(INTEGER, name='timestamp', nullable=False),
    **{k: col_numbers(*v) for k, v in _cmc_src_numbers.items()}
}

_comp_pair_dbcol = {
    'ID': Column(INTEGER, name='pk_id', primary_key=True, autoincrement=True, nullable=False),
    'Coin_ID': Column(INTEGER,
                      ForeignKey('crypto_basic.pk_id', ondelete='CASCADE', onupdate='CASCADE'),
                      name='coin_id', nullable=False),
    'Exchange_ID': Column(INTEGER,
                          ForeignKey('crypto_exc.pk_id', ondelete='CASCADE', onupdate='CASCADE'),
                          name='exc_id', nullable=False, default=1),
    'From': Column(VARCHAR(16), name='fsym', nullable=False),
    'To': Column(VARCHAR(16), name='tsym', nullable=False),
    'Exchange': Column(VARCHAR(32), name='exc', nullable=False),
    'Meta': Column(JSON, name='meta', default={})
}
_comp_src_numbers = {'Open': ['open', 'F'],
                     'High': ['high', 'F'],
                     'Low': ['low', 'F'],
                     'Close': ['close', 'F'],
                     'Close_USD': ['close_usd', 'F'],
                     'Volume_FROM': ['vol_from', 'F'],
                     'Volume_FROM24H': ['vol_from_d', 'F'],
                     'Volume_TO': ['vol_to', 'F'],
                     'Volume_TO24H': ['vol_to_d', 'F'],
                     'Volume_USD': ['vol_usd', 'F'],
                     'Volume_USD24H': ['vol_usd_d', 'F']}
_comp_src_dbcol = {
    'ID': Column(INTEGER, name='pk_id', primary_key=True, autoincrement=True, nullable=False),
    'Pair_ID': Column(INTEGER,
                      ForeignKey('comp_pair.pk_id', ondelete='CASCADE', onupdate='CASCADE'),
                      name='pair_id', nullable=False),
    'Symbol': Column(VARCHAR(64), name='symbol', nullable=False),
    'Time': Column(TIMESTAMP, name='time', nullable=False),
    'Timestamp': Column(INTEGER, name='timestamp', nullable=False),
    **{k: col_numbers(*v) for k, v in _comp_src_numbers.items()}
}
_comp_tri_dbcol = {
    'ID': Column(INTEGER, name='pk_id', primary_key=True, autoincrement=True, nullable=False),
    'Pair_ID': Column(INTEGER,
                      ForeignKey('comp_pair.pk_id', ondelete='CASCADE', onupdate='CASCADE'),
                      name='pair_id', nullable=False),
    'Symbol': Column(VARCHAR(64), name='symbol', nullable=False),
    'Time': Column(TIMESTAMP, name='time', nullable=False),
    'Timestamp': Column(INTEGER, name='timestamp', nullable=False),
    **{k: col_numbers(*v) for k, v in _comp_src_numbers.items()}
}


class CryptoSQL(object):
    _base = declarative_base()
    # 定义表结构
    _t = (type('CryptoBasic', (_base,), {**_crypto_basic_dbcol, '__tablename__': 'crypto_basic'}),
          type('CryptoInd', (_base,), {**_crypto_ind_dbcol, '__tablename__': 'crypto_ind'}),
          type('CryptoExc', (_base,), {**_crypto_exc_dbcol, '__tablename__': 'crypto_exc'}),
          type('CoinmarketcapSrc', (_base,), {**_cmc_src_dbcol,
                                              '__tablename__': 'cmc_src',
                                              '__table_args__': (UniqueConstraint('coin_id', 'timestamp'),)}),
          type('CryptocomparePairs', (_base,), {**_comp_pair_dbcol,
                                                '__tablename__': 'comp_pair',
                                                '__table_args__': (UniqueConstraint('fsym', 'tsym', 'exc'),)}),
          type('CryptocompareSrc', (_base,), {**_comp_src_dbcol,
                                              '__tablename__': 'comp_src',
                                              '__table_args__': (UniqueConstraint('pair_id', 'timestamp'),)}),
          type('CryptocompareTri', (_base,), {**_comp_tri_dbcol,
                                              '__tablename__': 'comp_tri',
                                              '__table_args__': (UniqueConstraint('pair_id', 'timestamp'),)})
          )
    _tables = {k.__tablename__: k for k in _t}
    # 字段名与ORM列的对应
    _columns = {k.__tablename__: {x[0]: x[1] for x in k.__table__.columns.items()} for k in _t}
    # 字段名与类属性名的对应
    _colnames = {'crypto_basic': {v.name: k for k, v in _crypto_basic_dbcol.items()},
                 'crypto_ind': {v.name: k for k, v in _crypto_ind_dbcol.items()},
                 'crypto_exc': {v.name: k for k, v in _crypto_exc_dbcol.items()},
                 'cmc_src': {v.name: k for k, v in _cmc_src_dbcol.items()},
                 'comp_pair': {v.name: k for k, v in _comp_pair_dbcol.items()},
                 'comp_src': {v.name: k for k, v in _comp_src_dbcol.items()},
                 'comp_tri': {v.name: k for k, v in _comp_src_dbcol.items()}}

    def __init__(self, host='localhost:5432', user='bitall', password='', database='cryp_main'):
        self._eng = create_engine(
            "postgres+psycopg2://{user}:{password}@{host}/{database}".format(host=host, user=user, password=password, database=database),
            isolation_level="REPEATABLE READ")
        self._Session = sessionmaker(bind=self._eng)
        self._session = self._Session()

    def create_tables(self) -> int:
        self._base.metadata.create_all(self._eng)
        return 0  # success

    def delete(self, tablename: str, pk_list: list) -> int:
        """删除数据库中的记录

        :param tablename: 表名
        :param pk_list: 要删除的记录 pk_id 列表
        :return:
        """
        table = self._tables[tablename]
        for pk_id in pk_list:
            try:
                self._session.execute(table.__table__.delete().where(table.ID == pk_id))
            except Exception as err:
                self._session.commit()
                print(err)
                return -1
        self._session.commit()
        return 0

    def truncate(self, tablename, reset_ai: bool = True, cascade: bool = True) -> int:
        """TRUNCATE 操作

        :param tablename: 表名
        :param reset_ai: 默认True，是否重置自增字段
        :param cascade: True: 'CASCADE'(by default) OR  False: 'RESTRICT'
        :return:
        """
        tr = "TRUNCATE TABLE " + tablename + \
             (" RESTART IDENTITY " if reset_ai else " CONTINUE IDENTITY ") + \
             ("CASCADE" if cascade else "RESTRICT")
        try:
            self._session.execute(tr)
            self._session.commit()
        except Exception as err:
            self._session.commit()
            print(err)
            return -1
        return 0

    def save(self, tablename: str, data, method: str = 'insert') -> int:
        """向数据库中插入记录

        :param tablename: 需要修改的表名

        :param method: 参数可以是 'replace', 'update', or 'insert'
        replace: 数据会被替换，原数据会被删除，自增字段会被重置
        insert: 不满足主键或唯一约束条件会报错
        upsert: 主键重复的记录会被更新，不存在则插入，这种方式必须传入 pk_id 字段，根据 pk_id 判断记录是否重复
        update: 只进行更新操作，传入不存在的 pk_id 会报错

        :param data: 参数可以是 (list 或者 dict) 组成的 list
        如果是 list 数据顺序应与字段顺序保持一致
        如果是 dict key 值应为相应的字段名
        在 insert, replace 模式下，不应该传入 pk_id 字段，由数据库自动生成
        update 模式需要通过 pk_id 指定需要修改的记录，推荐传入 dict 组成的 list 来指定要修改的字段

        :return: 0 success -1 exit with error
        """
        table = self._tables[tablename]
        cols = list(self._columns[tablename].keys())
        if method == 'replace':
            _e = self.truncate(tablename, reset_ai=True, cascade=False)
            if _e == -1:
                return -1

        if not len(data):
            return 0

        if method in ['insert', 'replace']:
            # pk_id 自动生成
            try:
                self._session.execute(table.__table__.insert(),
                                      [{cols[i + 1]: d[i] for i in range(len(d))}
                                       for d in data] if type(data[0]) == list else data)
                self._session.commit()
            except Exception as err:
                self._session.commit()
                print(err)
                return -1

        elif method == 'upsert':
            for d in data:
                try:
                    flag = type(d) == list
                    # 查询主键是否存在
                    key = 0 if flag else 'pk_id'
                    record_id = d[key]
                    del d[key]
                    r = self._session.execute(select([table.ID]).where(table.ID == record_id)).fetchall()
                    if not len(r):
                        # 插入新记录
                        self._session.execute(table.__table__.insert(),
                                              {cols[i + 1]: d[i] for i in range(len(d))} if flag else d)
                    else:
                        # 更新已有主键
                        self._session.execute(table.__table__.update().
                                              where(table.ID == record_id).
                                              values({cols[i + 1]: d[i] for i in range(len(d))} if flag else d))
                    self._session.commit()
                except Exception as err:
                    self._session.commit()
                    print(err)
                    return -1

        elif method == 'update':
            for d in data:
                try:
                    flag = type(d) == list
                    # 查询主键是否存在
                    key = 0 if flag else 'pk_id'
                    record_id = d[key]
                    del d[key]
                    self._session.execute(table.__table__.update().
                                          where(table.ID == record_id).
                                          values({cols[i + 1]: d[i] for i in range(len(d))} if flag else d))
                    self._session.commit()
                except Exception as err:
                    self._session.commit()
                    print(err)
                    return -1
        return 0

    def read(self, tablename: str, **kwargs):
        """从数据库中读取数据

        :param tablename: 需要读取的表名

        :param kwargs: 接受的可选参数名称
        columns: tuple 格式的参数，表示希望返回哪些字段。元素可以是数字，表示第几个位置的字段 (pk_id = 0), 也可以是字段名。默认为 *
        where: 字符串格式，SQL筛选条件。默认为 None
        group_by: tuple 格式的参数，指定 GROUP BY 查询的字段。默认为 None
        sort_by: tuple 格式的参数，指定需要排序的字段。默认为 None
        ascending: tuple 格式的参数，长度应与 sort 参数相同，布尔型，表示相应字段是否按升序排序
        limit: int 返回的记录数
        returned: 返回的结果形式，默认是 list , 可选 dict, dataframe

        :return:
        """

        cols = list(self._columns[tablename].keys())

        # 处理 column 参数
        select_query = ""
        select_columns = []
        if 'columns' not in kwargs.keys():
            select_query = '*'
            select_columns = cols
        elif kwargs['columns'] == '*':
            select_query = '*'
            select_columns = cols
        else:
            select_columns = list(map(lambda x: cols[x] if type(x) == int else x, kwargs['columns']))
            for c in select_columns:
                select_query += c + ','
            select_query = select_query[:-1]

        # 处理 where 参数
        where_query = ""
        if 'where' in kwargs.keys():
            if len(kwargs['where']):
                where_query = " WHERE " + kwargs['where']

        # 处理 group_by 参数
        group_query = ""
        if 'group_by' in kwargs.keys():
            group_columns = list(map(lambda x: cols[x] if type(x) == int else x, kwargs['group_by']))
            group_query = " GROUP BY "
            for c in group_columns:
                group_query += c + ','
            group_query = group_query[:-1]

        # 处理 order 参数
        order_query = ""
        if 'sort_by' in kwargs.keys():
            if len(kwargs['sort_by']):
                if len(kwargs['ascending']) == len(kwargs['sort_by']):
                    order_columns = list(map(lambda x: cols[x] if type(x) == int else x, kwargs['sort_by']))
                    order_query = " ORDER BY "
                    for i, c in enumerate(order_columns):
                        order_query += c + ' ' + ('ASC' if kwargs['ascending'][i] else 'DESC') + ','
                    order_query = order_query[:-1]
                else:
                    raise Exception('Parameter sort_by and ascending do not match.')

        # 处理 limit 参数
        limit_query = ""
        if 'limit' in kwargs.keys():
            if kwargs['limit'] > 0:
                limit_query = " LIMIT " + str(kwargs['limit'])

        query = "SELECT " + select_query + " FROM " + tablename + \
                where_query + group_query + order_query + limit_query
        try:
            r = self._session.execute(query)
        except Exception as err:
            self._session.commit()
            print(err)
            return -1

        r = r.fetchall()
        if 'returned' in kwargs.keys():
            if kwargs['returned'] == 'list':
                return r
            elif kwargs['returned'] == 'dict':
                r = list(map(lambda x: {select_columns[n]: x[n] for n in range(len(x))}, r))
                return r
            elif kwargs['returned'] == 'dataframe':
                r = list(map(lambda x: {self._colnames[tablename][select_columns[n]]: x[n] for n in range(len(x))}, r))
                return pd.DataFrame(r).reindex(columns=[self._colnames[tablename][c] for c in select_columns])
        return r

    def copy(self, tablename: str, **kwargs):
        if 'to' in kwargs.keys():
            try:
                self._session.execute(
                    "COPY (SELECT * FROM {table} ORDER BY pk_id ASC) TO '{path}' WITH csv header".format(table=tablename, path=kwargs['to']))
            except Exception as err:
                print(err)
            finally:
                self._session.commit()
        return 0

    def __del__(self):
        self._session.close()
        self._eng.dispose()

# sql = CryptoSQL()
