#!/usr/bin/python
# -*- coding: utf-8 -*-
# Last update on: 18/12/10

import requests as rq
from datetime import datetime
import pandas as pd
import json
import mod_util as cryputil
from mod_sql import CryptoSQL

# IMPORTANT
pd.set_option('mode.use_inf_as_na', True)
# index date range:
_st = '20161231'
# utc date
_nt = cryputil.utc_date_string(cryputil.unix_timestamp(), '%Y%m%d')
# 最大重试次数和最大线程数
# _max_retry = 1
_max_thread = 32
# 浮点数相等比较
_zero = 1e-12

# _api_key = 'f2ea2b38872c1482c3b7b9709435c1a17ea5a32017a6bc1abdb24eff035b268b'
# _api_key = 'f4475ba65dd9bfa4cf3640535b751f8435f16b9f63008069cfdcd8fa7a7abf24'
_api_key = '9fcdeea149dd1bea6b8b681388afc2b2a1b57baed8264c6c38a06a3a7eb6d527'

# user = input('Username: ')
# pwd = input('Password: ')
user = 'aqua'
pwd = 'bitall20180126'
sql = CryptoSQL(host='95.169.14.2:5432', user=user, password=pwd)


# 要获取交易对的币种
def get_lists() -> list:
    li = list(range(1, 101)) + [105, 109, 111, 114, 115, 118, 120, 135, 157, 164, 166, 2092, 2095]
    r = sql.read('crypto_basic',
                 columns=('pk_id', 'symbol', 'comp_id'),
                 sort_by=('pk_id',),
                 ascending=(True,),
                 returned='list')
    r = [{'coin_id': x[0], 'fsym': x[2] if x[2] else x[1]} for x in r if x[0] in li]
    return r


# 获取交易所
def get_exchanges() -> list:
    r = sql.read('crypto_exc',
                 columns=('pk_id', 'symbol'),
                 sort_by=('pk_id',),
                 ascending=(True,),
                 returned='list')
    r = [{'exc_id': x[0], 'exc': x[1]} for x in r]
    return r


_exchanges = get_exchanges()
# 法币或稳定币
_fiat = ['USDT', 'USDC', 'PAX', 'USD', 'QC', 'JPY', 'EUR', 'KRW', 'RUB']
# 用作计价货币的币种
_currencies = ['BTC', 'ETH'] + _fiat
# 价格转换
_convert_pairs = {'USD': ['JPY', 'EUR', 'RUB'],
                  'USDT': ['USD', 'QC', 'USDC', 'RUB', 'EUR'],
                  'EUR': ['USD'],
                  'BTC': ['USD'],
                  'ETH': ['USD']}


# 用计价币种筛选交易对
def filter_pairs(tsyms: list, f: list) -> list:
    f = set(f)
    s = set(tsyms)
    return list(s & f)


# 获取一个币种的所有交易对
# @retry(stop_max_attempt_number=_max_retry, wait_fixed=2000)
def get_pairs(fsym: str, limit=50) -> list:
    query_url = "https://min-api.cryptocompare.com/data/top/pairs?fsym={fsym}&limit={limit}&api_key={api_key}"\
        .format(fsym=fsym, limit=str(limit), api_key=_api_key)
    session = rq.Session()
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"
    }
    r = session.get(query_url, headers=headers, timeout=30)
    response = json.loads(r.text)

    data = []
    try:
        if (response["Response"]) == "Success":
            data = response["Data"]
        else:
            print(response["Message"])
    except Exception as err:
        print('Cannot Parse Response.', err)

    tsym = []
    for pair in data:
        tsym.append(pair['toSymbol'])
    return tsym


# 获取交易对的交易所
def get_pair_exchanges(fsym: str, tsym: str, limit=10) -> list:
    query_url = "https://min-api.cryptocompare.com/data/top/exchanges?fsym={fsym}&tsym={tsym}&limit={limit}&api_key={api_key}"\
        .format(fsym=fsym, tsym=tsym, limit=str(limit), api_key=_api_key)
    session = rq.Session()
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"
    }
    r = session.get(query_url, headers=headers, timeout=30)
    response = json.loads(r.text)

    data = []
    try:
        if (response["Response"]) == "Success":
            data = response["Data"]
        else:
            print(response["Message"])
    except Exception as err:
        print('Cannot Parse Response.', err)

    exc = []
    for record in data:
        if record['volume24h'] > 100:
            exc.append(record['exchange'])
    return exc


def get_filtered_pair_exchanges(fsym: str, tsym: str, limit=10) -> list:
    excs = get_pair_exchanges(fsym, tsym, limit)
    r = []
    for rec in _exchanges:
        if rec['exc'] in excs:
            r.append(rec)
    return r


# 获取一个币种与计价货币之间的交易对
def get_filtered_pairs(fsym: str, limit=50) -> list:
    if fsym in _fiat:
        return filter_pairs(get_pairs(fsym, limit), _fiat)
    return filter_pairs(get_pairs(fsym, limit), _currencies)


def get_pairs_list():
    li = []
    for rec in get_lists():
        tsyms = get_filtered_pairs(rec['fsym'])
        print('Get coin list:', rec['fsym'])
        for tsym in tsyms:
            excs = get_filtered_pair_exchanges(rec['fsym'], tsym)
            li += [{**rec, 'tsym': tsym, **exc} for exc in excs]
    return li


def read_pairs_raw():
    return sql.read('comp_pair',
                    sort_by=('pk_id', 'exc_id'),
                    ascending=(True, True),
                    returned='dict')


def read_pairs_list():
    pairs = read_pairs_raw()
    r = []
    for pair in pairs:
        if not len(pair['meta'].keys()):
            latest = 0
        elif pair['meta']['comp_latest'] >= 0:
            latest = pair['meta']['comp_latest']
        else:
            continue
        r.append({
            'pk_id': pair['pk_id'],
            'fsym': pair['fsym'],
            'tsym': pair['tsym'],
            'exc': pair['exc'],
            'latest': latest
        })
    return r


def analyze_pair_list():
    list_new = get_pairs_list()
    list_old = read_pairs_raw()
    i = 0
    while i < len(list_new):
        for j, d in enumerate(list_old):
            if list_new[i]['fsym'] == d['fsym'] and list_new[i]['tsym'] == d['tsym'] and list_new[i]['exc_id'] == d['exc_id']:
                del list_old[j]
                del list_new[i]
                i -= 1
                break
        i += 1
    print(len(list_new), 'new lines will be inserted.')
    print(len(list_old), 'lines will be deleted.')
    return list_new, list_old


# 保存交易对信息
def update_pairs_list(insert: list, delete: list) -> int:
    if len(delete):
        pass
    if len(insert):
        sql.save('comp_pair', insert, method='insert')
    return 0


def query_pair_id(fsym: tuple=(), tsym: tuple=(), exc: tuple=()):
    query = (("tsym IN " + tsym.__repr__().replace(',)', ')') + (" AND " if len(fsym) or len(exc) else "")) if len(tsym) else "") + \
            ("fsym IN " + fsym.__repr__().replace(',)', ')') + (" AND " if len(exc) else "") if len(fsym) else "") + \
            ("exc='" + exc + "'" if len(exc) else "")
    r = sql.read('comp_pair',
                 columns=('pk_id',),
                 where=query,
                 returned='list')
    return [d[0] for d in r] if len(r) else []


# 获取一个交易对开始交易的 timestamp
# @retry(stop_max_attempt_number=_max_retry, wait_fixed=2000)
def get_pair_trade_start(fsym: str, tsym: str, exc: str = 'CCCAGG') -> int:
    query_url = "https://min-api.cryptocompare.com/data/histoday?fsym={fsym}&tsym={tsym}" \
                "&limit=2000&aggregate=30&e={exc}&allData=true&tryConversion=false&api_key={api_key}"\
        .format(fsym=fsym, tsym=tsym, exc=exc, api_key=_api_key)
    session = rq.Session()
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"
    }
    r = session.get(query_url, headers=headers, timeout=30)
    response = json.loads(r.text)

    try:
        if (response["Response"]) == "Success":
            data = response["Data"]
            if len(data):
                timestamp = data[0]['time']
        else:
            # 历史数据不足 30 天
            timestamp = int(cryputil.unix_timestamp()) - 30 * 24 * 3600
            print(response["Message"])
    except Exception as err:
        print('Cannot Parse Response.', err)
    return timestamp


# 获取交易对历史交易数据
# @retry(stop_max_attempt_number=_max_retry, wait_fixed=2000)
def get_historical_hour_data(fsym: str, tsym: str, to_ts: int,
                             exc: str = 'CCCAGG', aggregate: str = '1', periods: bool = False, limit: int = 168) -> list:
    query_url = "https://min-api.cryptocompare.com/data/histohour?fsym={fsym}&tsym={tsym}" \
                "&toTs={toTs}&e={exc}&aggregate={agg}&aggregatePredictableTimePeriods={periods}&limit={limit}" \
                "&tryConversion=false&api_key={api_key}"\
        .format(fsym=fsym, tsym=tsym, toTs=str(to_ts), exc=exc,
                agg=aggregate, periods=("true" if periods else "false"), limit=str(limit), api_key=_api_key)

    session = rq.Session()
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"
    }
    r = session.get(query_url, headers=headers, timeout=30)
    response = json.loads(r.text)

    try:
        if (response["Response"]) == "Success":
            data = response["Data"]
        else:
            print(response["Message"])
    except Exception as err:
        print('Cannot Parse Response.', err)

    dataset = []
    # 最后一个数据是正在更新中的
    for tx in data[:-1]:
        tx['timestamp'] = tx['time']
        tx['time'] = datetime.utcfromtimestamp(tx['time'])
        tx['vol_from'] = tx['volumefrom'] if tx['volumefrom'] else 0
        tx['vol_to'] = tx['volumeto'] if tx['volumeto'] else 0
        del tx['volumefrom']
        del tx['volumeto']
        if tx['close'] > _zero:
            dataset.append(tx)
    print('Complete:', fsym + '_' + tsym + '@' + exc, cryputil.utc_date_string(to_ts), len(dataset))
    return dataset


# 获取时间段内交易对历史交易数据
def get_historical_hour_data_range(fsym: str, tsym: str, from_ts: int, to_ts: int, exc: str = 'CCCAGG') -> list:
    tasks = []
    while to_ts > from_ts:
        tasks.append({'time': to_ts})
        to_ts -= 168 * 3600
    r = cryputil.threading(tasks,
                           messenger=lambda x: get_historical_hour_data(fsym, tsym, x['time'], exc=exc, limit=168),
                           interval=2.0,
                           max_retry=3)
    if len(r):
        r.sort(key=lambda x: x['time'], reverse=False)
    return r


def read_pair_src_data(pair_id: int, exc: str = 'CCCAGG', columns=(0, 1, 2, 4, 8, 9, 10, 12, 14), limit: int = 0, returned='dict'):
    r = sql.read('comp_src',
                 columns=columns,
                 where='pair_id = ' + str(pair_id) + " AND exc_symbol = '" + exc + "'",
                 sort_by=('timestamp',),
                 ascending=(False,),
                 limit=limit,
                 returned=returned)
    return r


# 保存交易对数据
def get_pair_latest(pair: dict, to_ts: int, full: bool = False) -> list:
    fsym = pair['fsym']
    tsym = pair['tsym']
    exc = pair['exc']
    from_ts = get_pair_trade_start(fsym, tsym, exc)
    begin = pair['latest'] - 25 * 60 * 60
    if (begin < from_ts) or full:
        begin = from_ts

    r = get_historical_hour_data_range(fsym, tsym, begin, to_ts, exc)
    # sum
    r.reverse()
    r = [{'pair_id': pair['pk_id'],
          'symbol': fsym + '_' + tsym + '@' + exc,
          **d,
          'vol_from_d': sum([e['vol_from'] for e in r[i: i + 24]]),
          'vol_to_d': sum([e['vol_to'] for e in r[i: i + 24]])} for i, d in enumerate(r) if (i + 24) <= len(r)]
    r.reverse()

    if not full and len(r):
        while True:
            if r[0]['timestamp'] <= pair['latest']:
                del r[0]
            else:
                break
    return r


def get_all_pairs_latest(ts: int = 0) -> list:
    pairs = read_pairs_list()
    if ts == 0:
        ts = int(cryputil.unix_timestamp())
    r = []
    for pair in pairs:
        r += get_pair_latest(pair, ts)
    r.sort(key=lambda x: x['pair_id'])
    return r


def update_all_pairs(d: list) -> int:
    sql.save('comp_src', d, method='insert')
    return 0


# 重新计算 vol_from_d, vol_to_d 字段的内容
def reload_daily_data() -> int:
    pairs = read_pairs_list()
    for pair in pairs:
        data = read_pair_src_data(pair['pk_id'], exc=pair['exc'])
        r = [(d['pk_id'],
              sum([e['vol_from'] for e in data[i: i + 24]]),  # vol_from_d
              sum([e['vol_to'] for e in data[i: i + 24]]),  # vol_to_d
              sum([e['vol_usd'] for e in data[i: i + 24]]))  # vol_usd_d
             for i, d in enumerate(data) if (i + 24) <= len(data)]
        save_daily_data_to_db(r)
        print('Recalculating Pair', pair['pk_id'], '@', pair['exc'], 'Complete.')
    return 0


def save_daily_data_to_db(p) -> int:
    query = "UPDATE comp_src SET vol_from_d = rec.c1, vol_to_d = rec.c2, vol_usd_d = rec.c3 FROM (values " + \
            p.__repr__().replace(',)', ')')[1:-1] + \
            ") AS rec (c0, c1, c2, c3) WHERE comp_src.pk_id = rec.c0"
    try:
        sql._session.execute(query)
    except Exception as err:
        print(err)
    finally:
        sql._session.commit()
    return 0


def clear_daily_data() -> int:
    query = "UPDATE comp_src SET vol_from_d = 0, vol_to_d = 0, vol_usd_d = 0"
    try:
        sql._session.execute(query)
    except Exception as err:
        print(err)
    finally:
        sql._session.commit()
    return 0


def cal_usd_price() -> int:
    converted = []
    conv = {}
    for t in ['EUR', 'USDT', 'BTC', 'ETH', 'JPY', 'QC', 'USD']:
        if t == 'USDT':
            # 采用 Kraken 交易所的 USDT 价格作为汇率
            conv = read_pair_src_data(query_pair_id(t, 'USD'), exc='Kraken')
            conv = {d['timestamp']: d['close'] for d in conv}
        elif t in ['EUR', 'BTC', 'ETH']:
            conv = read_pair_src_data(query_pair_id(t, 'USD'))
            conv = {d['timestamp']: d['close'] for d in conv}
        elif t == 'JPY':
            conv = read_pair_src_data(query_pair_id('USD', t))
            conv = {d['timestamp']: 1 / d['close'] for d in conv}
        elif t == 'QC':
            conv = read_pair_src_data(query_pair_id('USDT', t))
            usdt = read_pair_src_data(query_pair_id('USDT', 'USD'))
            usdt = {d['timestamp']: d['close'] for d in usdt}
            conv = {d['timestamp']: usdt[d['timestamp']] / d['close'] for d in conv}
        elif t == 'USD':
            conv = 1
        q = tuple(query_pair_id('*', t))
        src = sql.read('comp_src',
                       columns=(0, 1, 2, 4, 5, 9, 13),
                       where="pair_id IN " + q.__repr__().replace(',)', ')') + " AND close_usd = 0 AND timestamp >= " +
                             (str(list(conv.keys())[-1]) if type(conv) == dict else '0'),
                       sort_by=('timestamp', 'pair_id'),
                       ascending=(False, True),
                       returned='dict')
        r = []
        for i, rec in enumerate(src):
            ts = rec['timestamp']
            if type(conv) == dict:
                if ts in conv.keys():
                    n = (rec['pk_id'], rec['close'] * conv[ts], rec['vol_to'] * conv[ts])
                    r.append(n)
            else:
                n = (rec['pk_id'], rec['close'], rec['vol_to'])
                r.append(n)
            if (i + 1) % 10000 == 0:
                print('Calculating USD Price:', t, i + 1)
        if len(r):
            print(len(r))
            converted += r

    if len(converted) > 100000:
        for i in range(int(len(converted) / 100000)):
            p = converted[i * 100000: (i + 1) * 100000]
            save_usd_price_to_db(p)
            print('Updating USD Price Complete:', (i + 1) * 100000, 'of', len(converted))
    p = converted[int(len(converted) / 100000) * 100000:]
    save_usd_price_to_db(p)
    print('Updating USD Price Complete.')
    return 0


def cal_vol_usd_daily() -> int:
    # 获取开始计算日成交额的起始时间
    pairs = sql.read('comp_src',
                     columns=("pair_id", "exc_symbol", "max(timestamp)"),
                     where="vol_usd > 0 AND vol_usd_d > 0",
                     group_by=("pair_id", "exc_symbol"))
    if not len(pairs):
        pairs = sql.read('comp_src',
                         columns=("pair_id", "exc_symbol", "min(timestamp)"),
                         where="vol_usd > 0 AND vol_usd_d = 0",
                         group_by=("pair_id", "exc_symbol"))

    usd_d = []
    for pair in pairs:
        data = sql.read('comp_src',
                        columns=(0, 1, 2, 4, 5, 15),
                        where="pair_id = " + str(pair[0]) + " AND exc_symbol = '" + pair[1] + "' AND timestamp > " + str(pair[2] - 23 * 60 * 60),
                        sort_by=('timestamp',),
                        ascending=(False,),
                        returned='dict')
        r = [(d['pk_id'],
              sum([e['vol_usd'] for e in data[i: i + 24]]))
             for i, d in enumerate(data) if (i + 24) <= len(data)]
        usd_d += r
    print('Updating USD Volume.', len(usd_d), 'new records.')
    save_usd_daily_to_db(usd_d)
    return 0


def save_usd_price_to_db(p: tuple) -> int:
    query = "UPDATE comp_src SET close_usd = rec.c1, vol_usd = rec.c2 FROM (values {values}) AS rec (c0, c1, c2) " \
            "WHERE comp_src.pk_id = rec.c0".format(values=p.__repr__().replace(',)', ')')[1:-1])
    try:
        sql._session.execute(query)
    except Exception as err:
        print(err)
    finally:
        sql._session.commit()
    return 0


def save_usd_daily_to_db(p: tuple) -> int:
    query = "UPDATE comp_src SET vol_usd_d = rec.c1 FROM (values {values}) AS rec (c0, c1) " \
            "WHERE comp_src.pk_id = rec.c0".format(values=p.__repr__().replace(',)', ')')[1:-1])
    try:
        sql._session.execute(query)
    except Exception as err:
        print(err)
    finally:
        sql._session.commit()
    return 0


def clear_usd_price() -> int:
    query = "UPDATE comp_src SET close_usd = 0, vol_usd = 0, vol_usd_d = 0"
    try:
        sql._session.execute(query)
    except Exception as err:
        print(err)
    finally:
        sql._session.commit()
    return 0


def cal_tri_usd_price() -> int:
    # 每月按各法币成交量加权计算 USD 价格指数
    cr_list = ['EUR', 'USDT', 'BTC', 'ETH', 'JPY', 'QC', 'USD']
    rng = pd.date_range(_st, _nt, freq='M')
    ts = [int(x.timestamp()) for x in rng]
    cr_vol = {}
    for t in ts:
        cr_df = []
        for currency in cr_list:
            r = sql.read('comp_src', columns=('timestamp', 'sum(vol_usd)'),
                         where="pair_id IN (SELECT pk_id FROM comp_pair WHERE tsym='{tsym}') AND exc_symbol='CCCAGG' "
                               "AND timestamp<{to} AND timestamp>={fr}".format(tsym=currency, to=str(t + 24 * 3600), fr=str(t - 29 * 24 * 3600)),
                         group_by=('timestamp',),
                         sort_by=('timestamp',),
                         ascending=(False,))
            if len(r):
                s = pd.Series([x[1] for x in r], index=[x[0] for x in r]).fillna(0)
                s.name = currency
                cr_df.append(s)
        cr_df = pd.DataFrame(cr_df).T
        cr_all = cr_df.sum().to_dict()
        cr_sum = sum(cr_all.values())
        cr_weight = {k: cr_all[k]/cr_sum for k in cr_all}
        # UTC时间1日9点更新权重
        cr_vol[t + 32 * 3600] = cr_weight

    li = get_lists()
    for d in li:
        fs = d['fsym']
        if fs in ('USD', 'EUR'):
            continue
        q = tuple(query_pair_id(fs, '*'))
        qt = tuple(query_pair_id('*', fs))
        latest = sql.read('comp_src',
                          columns=('max(timestamp)',),
                          where="pair_id IN (SELECT pk_id FROM comp_pair WHERE fsym='{fsym}' AND tsym='USD') AND exc_symbol='TRI'".format(fsym=fs))
        if latest[0][0] is not None:
            tsl = latest[0][0]
        else:
            tsl = 0

        tslist = sql.read('comp_src',
                          columns=('distinct(timestamp)',),
                          where="pair_id IN " + q.__repr__().replace(',)', ')') + " AND timestamp>" + str(tsl) + " AND exc_symbol='CCCAGG'",
                          sort_by=('timestamp',),
                          ascending=(True,))
        tslist = [i[0] for i in tslist]
        tri = []
        pair_id = query_pair_id(fs, 'USD')
        # 不存在 USD 交易对则新建一个
        if not pair_id:
            sql.save('comp_pair', [{
                'coin_id': d['coin_id'],
                'fsym': fs,
                'tsym': 'USD',
            }], method='insert')
            pair_id = query_pair_id(fs, 'USD')
        # 遍历 tslist
        for ts in tslist:
            tsdata = sql.read('comp_src',
                              where="pair_id IN " + q.__repr__().replace(',)', ')') + " AND timestamp=" + str(ts) + " AND exc_symbol='CCCAGG'",
                              sort_by=('pair_id',),
                              ascending=(True,),
                              returned='dict')
            # 上月 VOL 加权计算 USD 价格
            try:
                month_ts = max([x for x in cr_vol.keys() if x <= ts])
            except ValueError:
                continue
            weight = cr_vol[month_ts]

            close_usd_div = [weight[i['symbol'].split('_')[-1]]
                             for i in tsdata if i['close_usd'] > _zero]
            close_usd_p = [weight[i['symbol'].split('_')[-1]] * i['close_usd']
                           for i in tsdata if i['close_usd'] > _zero]
            if len(close_usd_p):
                close_usd = sum(close_usd_p) / sum(close_usd_div)
            else:
                close_usd = 0
            # 总成交量 / 本成交对部分
            vol_from = sum([i['vol_from'] for i in tsdata])
            vol_from_d = sum([i['vol_from_d'] for i in tsdata])
            vol_usd = sum([i['vol_usd']
                           for i in tsdata if i['close_usd'] > _zero]) + sum([i['vol_to'] * close_usd / i['close']
                                                                              for i in tsdata if i['close_usd'] <= _zero])
            vol_usd_d = sum([i['vol_usd_d']
                             for i in tsdata if i['close_usd'] > _zero]) + sum([i['vol_to_d'] * close_usd / i['close']
                                                                                for i in tsdata if i['close_usd'] <= _zero])
            # 总成交量 / 作为其他交易对的计价品种
            if len(qt):
                tsdata_t = sql.read('comp_src',
                                    where="pair_id IN " + qt.__repr__().replace(',)', ')') + " AND timestamp=" + str(ts) + " AND exc_symbol='CCCAGG'",
                                    sort_by=('pair_id',),
                                    ascending=(True,),
                                    returned='dict')
                vol_from_t = sum([i['vol_to'] for i in tsdata_t])
                vol_from_td = sum([i['vol_to_d'] for i in tsdata_t])
                vol_usd_t = sum([i['vol_usd']
                                 for i in tsdata_t if i['close_usd'] > _zero]) + sum([i['vol_to'] * close_usd
                                                                                      for i in tsdata_t if i['close_usd'] <= _zero])
                vol_usd_td = sum([i['vol_usd_d']
                                  for i in tsdata_t if i['close_usd'] > _zero]) + sum([i['vol_to_d'] * close_usd
                                                                                       for i in tsdata_t if i['close_usd'] <= _zero])
                vol_from += vol_from_t
                vol_from_d += vol_from_td
                vol_usd += vol_usd_t
                vol_usd_d += vol_usd_td
            # 插入时间戳为 ts 的记录
            tri.append({
                'pair_id': pair_id,
                'symbol': fs + '_USD',
                'exc_symbol': 'TRI',
                'timestamp': ts,
                'time': datetime.utcfromtimestamp(ts),
                'close_usd': close_usd,
                'vol_from': vol_from,
                'vol_from_d': vol_from_d,
                'vol_usd': vol_usd,
                'vol_usd_d': vol_usd_d
            })
            if (ts - tsl) % (30 * 24 * 60 * 60) == 0:
                print('Calculating TRIAGG Price:', fs, datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S'))
        sql.save('comp_src', tri, method='insert')
    return 0


# meta 中将交易所 latest 设置为 -1 可停止该交易对相应交易所的数据更新
def reload_metadata() -> int:
    pairs = read_pairs_raw()
    m = []
    for n, pair in enumerate(pairs):
        avail = [k for k, v in pair['meta'].items() if ('latest' not in v.keys()) or (v['latest'] >= 0)]
        r = sql.read('comp_src',
                     columns=('exc_symbol', 'max(timestamp)', 'min(timestamp)', 'count(pk_id)'),
                     where="pair_id = " + str(pair['pk_id']),
                     group_by=('exc_symbol',))
        if not len(r):
            meta = {k: {'latest': 0} for k in avail}
        elif r[0][0] is None:
            meta = {k: {'latest': 0} for k in avail}
        else:
            meta = {i[0]: {
                'comp_latest': i[1],
                'comp_first': i[2],
                'comp_records': i[3]
            } for i in r}

        m.append({'pk_id': pair['pk_id'], 'meta': meta})
        if (n + 1) % 100 == 0:
            print('Refresh meta for pair:', pair['pk_id'])
    sql.save('comp_pair', m, method='update')
    return 0


def update():
    update_all_pairs()
    cal_usd_price()
    cal_vol_usd_daily()
    cal_tri_usd_price()
    # metadata
    reload_metadata()
