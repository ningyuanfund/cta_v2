#!/usr/bin/python
# -*- coding: utf-8 -*-

import requests as rq
from bs4 import BeautifulSoup
import json
import random
from abc import ABC, abstractmethod


class API(ABC):
    """
    @private:
        _API__api_key
        _API__api_secret
        _API__req_headers
        _API__base_uri
    @public:

    """
    def __init__(self, api_key: str, api_secret: str, base_uri: str):
        self.__api_key = api_key
        self.__api_secret = api_secret
        self.__base_uri = base_uri
        self.__req_headers = []

    @property
    def base_uri(self):
        return self.__base_uri

    @property
    def api_key(self):
        return self.__api_key

    @property
    def api_secret(self):
        return self.__api_secret

    def _add_headers(self, headers: dict):
        self.__req_headers.append(headers)

    def _random_headers(self):
        if len(self.__req_headers) == 0:
            return ''
        else:
            return random.choice(self.__req_headers)

    @staticmethod
    @abstractmethod
    def _parse_response(res):
        pass

    @staticmethod
    @abstractmethod
    def _parse_error(err):
        pass

    def _get_requested_uri(self, path: str, format: str = 'json', timeout: int = 30):
        uri = self.__base_uri + path
        session = rq.Session()
        r = session.get(uri, headers=self._random_headers(), timeout=timeout)

        if format == 'json':
            res = json.loads(r.text)
        elif format == 'html':
            res = BeautifulSoup(r.text, 'lxml')
        else:
            res = r.text

        if res is not None:
            return self._parse_response(res)
        else:
            return None


class CryptoCompareAPI(API):
    """
    @private:
        _CryptoCompareAPI__timeout
    """

    def __init__(self, api_key: str, api_secret: str):
        super().__init__(api_key, api_secret, "https://min-api.cryptocompare.com/")
        self._add_headers({
            "authorization": "Apikey " + self.api_key
        })
        self.__timeout = 10

    @property
    def timeout(self):
        return self.__timeout

    @staticmethod
    def _parse_response(res: dict):
        try:
            if (res["Response"]) == "Success":
                d = res["Data"]
            else:
                print('访问 API 错误:', res["Message"])
                return None
        except Exception as err:
            print('返回值格式不正确.', err)
            return None
        return d

    @staticmethod
    def _parse_error(err):
        pass

    def rate_limit(self):
        """
        Rate limit
        Find out how many calls you have left in the current month, day, hour, minute and second
        :return:
        """
        path = "stats/rate/limit"
        return self._get_requested_uri(path, 'json', self.timeout)

    def all_exchanges(self, sign: bool = None, extraparams: str = None):
        """
        All the Exchanges and Trading Pairs
        Returns all the exchanges that CryptoCompare has integrated with.
        :param sign:
        :param extraparams:
        :return:
        """
        path = "data/v2/all/exchanges"
        sym = '?'
        if extraparams is not None:
            path += sym + "extraParams=" + extraparams
            sym = '&'
        if sign is not None:
            path += sym + "sign=" + ('true' if sign else 'false')
        return self._get_requested_uri(path, 'json', self.timeout)

    def all_coinlist(self, builton: str = None, sign: bool = None, extraparams: str = None):
        """
        All the Coins
        Returns all the coins that CryptoCompare has added to the website.
        This is not the full list of coins we have in the system, it is just the list of coins we have done some research on.
        :param builton: The platform that the token is built on [Max character length: 10]
        :param sign:
        :param extraparams:
        :return:
        """
        path = "data/all/coinlist"
        if builton is not None:
            path += "?builton=" + extraparams
        if extraparams is not None:
            path += "&extraParams=" + extraparams
        if sign is not None:
            path += "&sign=" + ('true' if sign else 'false')
        return self._get_requested_uri(path, 'json', self.timeout)

    def top_pairs(self, fsym: str, limit: int = 50, sign: bool = None, extraparams: str = None):
        """
        Toplist of Trading Pairs
        Get top pairs by volume for a currency (always uses our aggregated data).
        The number of pairs you get is the minimum of the limit you set (default 5) and the total number of pairs available
        币种按交易量排名
        :param fsym: [Required] The cryptocurrency symbol of interest [Max character length: 10]
        :param limit: The number of data points to return
        :param sign:
        :param extraparams:
        :return:
        """
        path = "data/top/pairs?fsym={fsym}&limit={limit}".format(fsym=fsym, limit=limit)
        if extraparams is not None:
            path += "&extraParams=" + extraparams
        if sign is not None:
            path += "&sign=" + ('true' if sign else 'false')
        return self._get_requested_uri(path, 'json', self.timeout)

    def top_volumes(self, tsym: str, limit: int = 50, sign: bool = None, extraparams: str = None):
        """
        Toplist by Pair Volume
        Get top coins by volume for the to currency. It returns volume24hto and total supply (where available).
        The number of coins you get is the minimum of the limit you set (default 50) and the total number of coins available
        :param tsym: [Required] The currency symbol to convert into [Max character length: 10]
        :param limit:
        :param sign:
        :param extraparams:
        :return:
        """
        path = "data/top/volumes?tsym={tsym}&limit={limit}".format(tsym=tsym, limit=limit)
        if extraparams is not None:
            path += "&extraParams=" + extraparams
        if sign is not None:
            path += "&sign=" + ('true' if sign else 'false')
        return self._get_requested_uri(path, 'json', self.timeout)

    def top_exchanges(self, fsym: str, tsym: str, limit: int = 10, sign: bool = None, extraparams: str = None, full: bool = None):
        """
        Top Exchanges Volume Data by Pair
        Get top exchanges by volume for a currency pair.
        The number of exchanges you get is the minimum of the limit you set (default 5) and the total number of exchanges available
        :param fsym: [Required] The cryptocurrency symbol of interest [Max character length: 10]
        :param tsym: [Required] The currency symbol to convert into [Max character length: 10]
        :param limit: The number of data points to return
        :param sign:
        :param extraparams:
        :param full:
        :return:
        """
        path = "data/top/exchanges" + ("/full" if full else "") + \
               "?fsym={fsym}&tsym={tsym}&limit={limit}".format(fsym=fsym, tsym=tsym, limit=limit)
        if extraparams is not None:
            path += "&extraParams=" + extraparams
        if sign is not None:
            path += "&sign=" + ('true' if sign else 'false')
        return self._get_requested_uri(path, 'json', self.timeout)

    def historical_daily(self,
                         fsym: str,
                         tsym: str,
                         exchange: str = None,
                         aggregate: int = None,
                         aggregate_predictable_time_periods: bool = None,
                         limit: int = 1440,
                         to_ts: int = None,
                         sign: bool = None,
                         extraparams: str = None):
        """
        Historical Daily OHLCV
        Get open, high, low, close, volumefrom and volumeto from the daily historical data.
        The values are based on 00:00 GMT time.
        It uses BTC conversion if data is not available because the coin is not trading in the specified currency.
        If you want to get all the available historical data, you can use limit=2000 and keep going back in time using the toTs param.
        You can then keep requesting batches using: &limit=2000&toTs={the earliest timestamp received}.
        :param fsym: [Required]
        :param tsym:
        :param exchange:
        :param aggregate:
        :param aggregate_predictable_time_periods:
        :param limit:
        :param to_ts:
        :param sign:
        :param extraparams:
        :return:
        """