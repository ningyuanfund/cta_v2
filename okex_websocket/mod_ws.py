#!/usr/bin/python
# -*- coding: utf-8 -*-

import websocket
from datetime import datetime
from threading import Thread, Event
import json
import zlib
import hashlib
import hmac
import base64
from mod_api import API
from abc import abstractmethod


class WebsocketAPI(API):
    def __init__(self, api_key: str, api_secret: str, uri: str,
                 cb_on_open=None,
                 cb_on_close=None,
                 cb_on_msg=None,
                 cb_on_err=None):
        super().__init__(api_key, api_secret, uri)

        self._ws = websocket.WebSocketApp(self.base_uri,
                                          on_open=self.on_open(callback=cb_on_open),
                                          on_close=self.on_close(callback=cb_on_close),
                                          on_message=self.on_msg(callback=cb_on_msg),
                                          on_error=self.on_err(callback=cb_on_err))
        self._thread = None
        self.__ws_config = {
            'ping_interval': 0,
            'ping_timeout': 0,
            'http_proxy_host': '',
            'http_proxy_port': 80
        }
        self.__config = {}

    def _add_config_item(self, key, value):
        if key in self.__config or key in self.__ws_config:
            print('配置项重复..', key)
        else:
            self.__config[key] = value

    def set_config(self, key, value):
        if key in self.__config.keys():
            self.__config[key] = value
            print('配置项', key, '的值已变更为', value)
        elif key in self.__ws_config.keys():
            self.__ws_config[key] = value
            print('配置项', key, '的值已变更为', value)
        else:
            print('无法设置对应的设置项..', key)

    def read_config(self, key):
        if key in self.__config.keys():
            return self.__config[key]
        elif key in self.__ws_config.keys():
            return self.__ws_config[key]
        else:
            print('无法读取对应的设置项..', key)

    @abstractmethod
    def _on_open(self, socket):
        pass

    @abstractmethod
    def _on_close(self, socket):
        pass

    @abstractmethod
    def _on_message(self, socket, msg):
        pass

    @abstractmethod
    def _on_error(self, socket, err):
        pass

    def on_open(self, callback=None):
        def func(socket):
            r = self._on_open(socket)
            if callback is not None:
                try:
                    callback(r)
                except Exception as err:
                    print('执行回调函数失败..', err)
            return r
        return func

    def on_close(self, callback=None):
        def func(socket):
            r = self._on_close(socket)
            if callback is not None:
                try:
                    callback(r)
                except Exception as err:
                    print('执行回调函数失败..', err)
            return r
        return func

    def on_msg(self, callback=None):
        def func(socket, raw_msg):
            # parse message with function parse_response()
            m = self._parse_response(raw_msg)
            # do something with function on_message()
            r = self._on_message(socket, m)
            if callback is not None:
                try:
                    callback(r)
                except Exception as err:
                    print('执行回调函数失败..', err)
            return r
        return func

    def on_err(self, callback=None):
        def func(socket, raw_err):
            e = self._parse_error(raw_err)
            r = self._on_error(socket, e)
            if callback is not None:
                try:
                    callback(r)
                except Exception as err:
                    print('执行回调函数失败..', err)
            return r
        return func

    def start(self):
        if self._thread is not None:
            return self._ws
        try:
            self._thread = Thread(target=self._ws.run_forever, kwargs=self.__ws_config)
            self._thread.setDaemon(True)
            self._thread.start()
        except Exception as err:
            self._thread = None
            print('Websocket连接失败:', err)
            return None
        return self._ws

    def close(self):
        try:
            self._ws.close()
        finally:
            # 关闭线程
            if self._thread.isAlive():
                self._thread.join()
                self._thread = None

    def send(self, msg):
        self._ws.send(msg)


class OkEXWebsocket(WebsocketAPI):
    def __init__(self, api_key: str, api_secret: str, passphrase):
        # api_key, api_secret, passphrase请参照okex官方文档
        uri = "wss://real.okex.com:10442/ws/v3"
        super().__init__(api_key, api_secret, uri)
        # 每20s发送一次心跳包
        self.set_config('ping_interval', 20)
        # 设置请求头
        self._add_headers({
            "contentType": "application/json"
        })
        self._add_config_item('should_login', False)
        self._add_config_item('login_success', False)
        self._add_config_item('passphrase', passphrase)
        self._subscribe_list = []

    STATIC = type('StaticContents', (object,), {
        'MARKET': type('ExchangeMarkets', (object,), {
            'SWAP': 'swap',
            'FUTURE': 'futures',
            'SPOT': 'spot',
            'INDEX': 'index'
        }),
        'BUSINESS': type('ExchangeBusiness', (object,), {
            'TICKER': 'ticker',
            'CANDLE': 'candle',
            'TRADE': 'trade',
            'DEPTH': 'depth',
            'ACCOUNT': 'account',
            'POSITION': 'position',
            'ORDER': 'order'
        }),
        'PERIODS': type('Periods', (object,), {
            '1M': '60s',
            '5M': '300s',
            '15M': '900s',
            '30M': '1800s',
            '1H': '3600s',
            '4H': '14400s',
            '1D': '86400s',
            '1W': '604800s'
        })
    })

    def _on_open(self, socket):
        print('连接到OKEX Websocket服务器..')
        self._login()
        if len(self._subscribe_list):
            msg = {
                "op": "subscribe",
                "args": self._subscribe_list
            }
            self.send(json.dumps(msg))
            print('连接成功！')

    def _on_close(self, socket):
        print('从OkEX服务器断开连接..')

    def _on_message(self, socket, msg):
        data = json.loads(msg)
        if 'event' in data.keys():
            event_type = data['event']
            if event_type == 'login':
                if data['success']:
                    print('身份认证成功!')
                else:
                    print('身份认证失败, 连接即将断开..')
            if event_type == 'subscribe':
                print('成功订阅频道:', data['channel'])
        if 'table' in data.keys():
            event_type = data['table'].split('/')[0]
            business_type = data['table'].split('/')[1]
            data = data['data'][0]
            print('合约:', data['instrument_id'], '最新:', data['last'], '24HVol', data['volume_24h'], 'Time', data['timestamp'])

    def _on_error(self, socket, err):
        print('发生错误:', err)

    @staticmethod
    def _parse_response(res):
        # 解压缩消息
        decompress = zlib.decompressobj(
            -zlib.MAX_WBITS
        )
        r = decompress.decompress(res)
        r += decompress.flush()
        return r

    @staticmethod
    def _parse_error(err):
        return err

    def _generate_verification_sign(self, ts=0, method='GET', path='/users/self/verify'):
        # 使用HMAC-SHA256加密算法签名
        if ts == 0:
            ts = round(datetime.now().timestamp(), 3)
        msg = (str(ts) + method + path).encode('utf-8')
        cryp = hmac.new(self.api_secret.encode('utf-8'), msg, digestmod=hashlib.sha256)
        return base64.b64encode(cryp.digest()).decode('utf-8')

    def _login(self):
        if not self._thread:
            print('请先建立连接..')
            return -1
        if self.read_config('login_success'):
            print('已经登录成功, 请勿重复登录.')
            return -1
        ts = round(datetime.now().timestamp(), 3)
        msg = {
            'op': 'login',
            'args': [self.api_key, self.read_config('passphrase'), str(ts), self._generate_verification_sign(ts)]
        }
        self._ws.send(json.dumps(msg))

    def subscribe(self, channel: str, business: str, instruments: list):
        for item in instruments:
            self._subscribe_list.append("{chan}/{bus}:{ins}".format(chan=channel, bus=business, ins=item))


# websocket.enableTrace(True)
okex = OkEXWebsocket(api_key='bbfb0c36-52d1-4651-91fb-a3d1b6c67558',
                     api_secret='6FC0E008519098D788392DBD067C1F08',
                     passphrase='19950126')
# okex.set_config('http_proxy_host', '127.0.0.1')
# okex.set_config('http_proxy_port', '1087')

okex.subscribe(okex.STATIC.MARKET.SWAP, okex.STATIC.BUSINESS.TICKER,
               ['BTC-USD-SWAP', 'LTC-USD-SWAP',
                'ETH-USD-SWAP', 'EOS-USD-SWAP',
                'BCH-USD-SWAP', 'ETC-USD-SWAP'])

cnt = okex.start()
