#!/usr/bin/python
# -*- coding: utf-8 -*-
# Last update on: 18/11/30

from datetime import datetime
from threading import Thread
from queue import Queue
import time


# type float
def unix_timestamp(unit='s'):
    if unit == 's':
        u = 1
    elif unit == 'ms':
        u = 1000
    else:
        raise Exception('单位参数错误:', unit)
    return float(datetime.now().timestamp() * u)


# type string
def utc_date_string(ts=0, format='%Y-%m-%d %H:%M:%S'):
    if not ts:
        ts = unix_timestamp()
    return datetime.utcfromtimestamp(ts).strftime(format)


def local_to_utc():
    return datetime.utcfromtimestamp(0).timestamp()


def threading(tasks: list, messenger, interval: float=0.0, max_thread: int=32, max_retry: int=32) -> list:
    _Q = Queue()
    outputs = []

    def worker():
        nonlocal outputs
        while True:
            message = _Q.get()
            if message is None:
                break
            message['_retry'] += 1
            # do something using message with messenger func:
            try:
                r = messenger(message)
            except Exception as err:
                print('Error:', message, err)
                if message['_retry'] >= max_retry:
                    r = []
                else:
                    _Q.put(message)
                    _Q.task_done()
                    continue

            if type(r) == list:
                outputs += r
            else:
                outputs.append(r)
            _Q.task_done()

    # stop workers
    def stop():
        for j in range(len(threads)):
            _Q.put(None)
        for t in threads:
            t.join()

    # create threads
    threads = []
    for i in range(max_thread):
        thread = Thread(target=worker)
        thread.daemon = True
        thread.start()
        threads.append(thread)
    # put tasks
    try:
        for task in tasks:
            _Q.put({**task, '_retry': 0})
            time.sleep(interval)
    finally:
        _Q.join()
        stop()
    # do final works
    return outputs
