import numpy as np

import_signal = 'rua!'

# ===不带止损的趋势布林线策略
def real_time_signal_bolling_without_stop_lose(now_pos, df, para):
    """
    布林线中轨：n天收盘价的移动平均线
    布林线上轨：n天收盘价的移动平均线 + m * n天收盘价的标准差
    布林线上轨：n天收盘价的移动平均线 - m * n天收盘价的标准差

    收盘价下穿上轨做空，到中轨平仓。上穿下轨做多，到中轨平仓。
    当且仅当上一次触及了中轨，才判断下一次是否要开仓

    另外，当价格往亏损方向超过百分之stop_lose的时候，平仓止损。
    :param now_pos:
    :param stop_lose_price:
    :param df:
    :param para:
    :return:
    """

    # ===计算指标
    n = int(para[0])
    m = para[1]
    close_price = df.iloc[-1]['close']


    # 只截取必要的部分，减少计算量。稍微留一点冗余
    # df = df[-(n + 3):].copy()

    # 计算均线
    df['median'] = df['close'].rolling(n).mean()

    # 计算上轨、下轨道
    df['std'] = df['close'].rolling(n).std(ddof=1)  # ddof代表标准差自由度
    df['upper'] = df['median'] + m * df['std']
    df['lower'] = df['median'] - m * df['std']

    # 现在是多头仓位
    if now_pos == 1:
        # 依次计算是否需要平仓
        if close_price < df.iloc[-1]['lower']:
            target_pos = -1
        elif close_price < df.iloc[-1]['median']:
            target_pos = 0
        else:
            target_pos = 1

    # 现在是空头仓位
    elif now_pos == -1:
        # 依次计算是否需要平仓
        if close_price > df.iloc[-1]['upper']:
            target_pos = 1
        elif close_price > df.iloc[-1]['median']:
            target_pos = 0
        else:
            target_pos = -1
    # 现在无仓位
    elif now_pos == 0:

        # 开多信号，上穿上轨
        condition2 = close_price > df.iloc[-1]['upper']

        # 开空信号，下穿下轨
        condition4 = close_price < df.iloc[-1]['lower']
        if condition2:
            target_pos = 1
        elif condition4:
            target_pos = -1
        else:
            target_pos = 0
    else:
        raise ValueError('当前仓位变量now_pos只能是1， -1， 0，但目前是', now_pos)

    return target_pos


# ===不带止损的趋势布林线策略
def real_time_signal_upper_lower_bolling_without_stop_lose(now_pos, df, para):
    """
    布林线中轨：n天收盘价的移动平均线
    布林线上轨：n天收盘价的移动平均线 + m * n天收盘价的标准差
    布林线上轨：n天收盘价的移动平均线 - m * n天收盘价的标准差

    收盘价下穿上轨做空，到中轨平仓。上穿下轨做多，到中轨平仓。
    当且仅当上一次触及了中轨，才判断下一次是否要开仓

    另外，当价格往亏损方向超过百分之stop_lose的时候，平仓止损。
    :param now_pos:
    :param stop_lose_price:
    :param df:
    :param para:
    :return:
    """

    # ===计算指标
    n = int(para[0])
    m = para[1]
    close_price = df.iloc[-1]['close']

    # 只截取必要的部分，减少计算量。稍微留一点冗余
    # df = df[-(n + 3):].copy()

    # 计算均线
    df['median'] = df['close'].rolling(n).mean()

    # 计算上轨、下轨道
    df['std'] = df['close'].rolling(n).std(ddof=1)  # ddof代表标准差自由度
    df['upper'] = df['median'] + m * df['std']
    df['lower'] = df['median'] - m * df['std']

    # 现在是多头仓位
    if now_pos == 1:
        # 依次计算是否需要平仓
        if close_price < df.iloc[-1]['lower']:
            target_pos = -1
        elif close_price < df.iloc[-1]['upper']:
            target_pos = 0
        else:
            target_pos = 1

    # 现在是空头仓位
    elif now_pos == -1:
        # 依次计算是否需要平仓
        if close_price > df.iloc[-1]['upper']:
            target_pos = 1
        elif close_price > df.iloc[-1]['lower']:
            target_pos = 0
        else:
            target_pos = -1
    # 现在无仓位
    elif now_pos == 0:

        # 开多信号，上穿上轨
        condition2 = close_price > df.iloc[-1]['upper']

        # 开空信号，下穿下轨
        condition4 = close_price < df.iloc[-1]['lower']
        if condition2:
            target_pos = 1
        elif condition4:
            target_pos = -1
        else:
            target_pos = 0
    else:
        raise ValueError('当前仓位变量now_pos只能是1， -1， 0，但目前是', now_pos)

    # if (now_pos == 1) & (target_pos == 0):
    #     print(df)
    #     df.to_csv(r'C:\Users\sycam\Desktop\data.csv')
    print(df)

    return target_pos


# ===不带止损的趋势布林线策略
def real_time_signal_price_bolling_without_stop_lose(now_pos, df, para):
    """
    布林线中轨：n天收盘价的移动平均线
    布林线上轨：n天收盘价的移动平均线 + m * n天收盘价的标准差
    布林线上轨：n天收盘价的移动平均线 - m * n天收盘价的标准差

    收盘价下穿上轨做空，到中轨平仓。上穿下轨做多，到中轨平仓。
    当且仅当上一次触及了中轨，才判断下一次是否要开仓

    另外，当价格往亏损方向超过百分之stop_lose的时候，平仓止损。
    :param now_pos:
    :param stop_lose_price:
    :param df:
    :param para:
    :return:
    """

    # ===计算指标
    n = int(para[0])
    m = para[1]

    # 只截取必要的部分，减少计算量。稍微留一点冗余
    # df = df[-(n + 3):].copy()

    df['price'] = df[['open', 'high', 'low', 'close']].mean(axis=1)
    last_price = df.iloc[-1]['price']

    # 计算均线
    df['median'] = df['price'].rolling(n).mean()

    # 计算上轨、下轨道
    df['std'] = df['price'].rolling(n).std(ddof=1)  # ddof代表标准差自由度
    df['upper'] = df['median'] + m * df['std']
    df['lower'] = df['median'] - m * df['std']

    # 现在是多头仓位
    if now_pos == 1:
        # 依次计算是否需要平仓
        if last_price < df.iloc[-1]['lower']:
            target_pos = -1
        elif last_price < df.iloc[-1]['median']:
            target_pos = 0
        else:
            target_pos = 1

    # 现在是空头仓位
    elif now_pos == -1:
        # 依次计算是否需要平仓
        if last_price > df.iloc[-1]['upper']:
            target_pos = 1
        elif last_price > df.iloc[-1]['median']:
            target_pos = 0
        else:
            target_pos = -1
    # 现在无仓位
    elif now_pos == 0:

        # 开多信号，上穿上轨
        condition2 = last_price > df.iloc[-1]['upper']

        # 开空信号，下穿下轨
        condition4 = last_price < df.iloc[-1]['lower']
        if condition2:
            target_pos = 1
        elif condition4:
            target_pos = -1
        else:
            target_pos = 0
    else:
        raise ValueError('当前仓位变量now_pos只能是1， -1， 0，但目前是', now_pos)

    return target_pos


# ===不带止损的趋势布林线策略
def real_time_signal_log_price_bolling_without_stop_lose(now_pos, df, para):
    """
    布林线中轨：n天收盘价的移动平均线
    布林线上轨：n天收盘价的移动平均线 + m * n天收盘价的标准差
    布林线上轨：n天收盘价的移动平均线 - m * n天收盘价的标准差

    收盘价下穿上轨做空，到中轨平仓。上穿下轨做多，到中轨平仓。
    当且仅当上一次触及了中轨，才判断下一次是否要开仓

    另外，当价格往亏损方向超过百分之stop_lose的时候，平仓止损。
    :param now_pos:
    :param stop_lose_price:
    :param df:
    :param para:
    :return:
    """

    # ===计算指标
    n = int(para[0])
    m = para[1]

    # 只截取必要的部分，减少计算量。稍微留一点冗余
    # df = df[-(n + 3):].copy()

    df['p'] = df[['open', 'high', 'low', 'close']].mean(axis=1)

    df['price'] = df['p'].apply(lambda x: np.log(x))
    last_price = df.iloc[-1]['price']

    # 计算均线
    df['median'] = df['price'].rolling(n).mean()

    # 计算上轨、下轨道
    df['std'] = df['price'].rolling(n).std(ddof=1)  # ddof代表标准差自由度
    df['upper'] = df['median'] + m * df['std']
    df['lower'] = df['median'] - m * df['std']

    # 现在是多头仓位
    if now_pos == 1:
        # 依次计算是否需要平仓
        if last_price < df.iloc[-1]['lower']:
            target_pos = -1
        elif last_price < df.iloc[-1]['median']:
            target_pos = 0
        else:
            target_pos = 1

    # 现在是空头仓位
    elif now_pos == -1:
        # 依次计算是否需要平仓
        if last_price > df.iloc[-1]['upper']:
            target_pos = 1
        elif last_price > df.iloc[-1]['median']:
            target_pos = 0
        else:
            target_pos = -1
    # 现在无仓位
    elif now_pos == 0:

        # 开多信号，上穿上轨
        condition2 = last_price > df.iloc[-1]['upper']

        # 开空信号，下穿下轨
        condition4 = last_price < df.iloc[-1]['lower']
        if condition2:
            target_pos = 1
        elif condition4:
            target_pos = -1
        else:
            target_pos = 0
    else:
        raise ValueError('当前仓位变量now_pos只能是1， -1， 0，但目前是', now_pos)

    return target_pos


# ===不带止损的双均线策略
def real_time_signal_moving_average_without_stop_lose(now_pos, df, para):
    # ===计算指标
    ma_short = int(para[0])
    ma_long = int(para[1])

    # 计算均线
    df['ma_short'] = df['close'].rolling(ma_short, min_periods=1).mean()
    df['ma_long'] = df['close'].rolling(ma_long, min_periods=1).mean()
    last_short = df['ma_short'].iloc[-1]
    last_long = df['ma_long'].iloc[-1]

    # 现在是多头仓位
    if now_pos == 1:
        if last_short < last_long:
            target_pos = -1
        else:
            target_pos = 1

    # 现在是空头仓位
    elif now_pos == -1:
        if last_short > last_long:
            target_pos = 1
        else:
            target_pos = -1
    # 现在无仓位
    elif now_pos == 0:
        if last_short > last_long:
            target_pos = 1
        elif last_short < last_long:
            target_pos = -1
        else:
            target_pos = 0
    else:
        raise ValueError('当前仓位变量now_pos只能是1， -1， 0，但目前是', now_pos)

    return target_pos
