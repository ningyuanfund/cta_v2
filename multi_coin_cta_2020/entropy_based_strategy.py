import os
import time

import statsmodels.api as sm
from scipy.stats import norm, entropy

from test_v2.obj_ornt.external_func import *
from test_v2.obj_ornt.stop_method import *

pd.set_option('expand_frame_repr', False)

from multi_coin_cta_2020.test_RAM import BackTest

class EntropyBasedTest(BackTest):
    def __init__(
            self, symbol_info, start_time, end_time, symbol_para_path_dict, output_root_path, save_name, localtime,
            entropy_len
    ):
        BackTest.__init__(
            self, symbol_info, start_time, end_time, symbol_para_path_dict, output_root_path, save_name, localtime
        )
        self.symbol_para_path_dict = symbol_para_path_dict
        self.entropy = []
        self.window = 24 * 12
        self.std_ran = [(i - 50) / (50/3) for i in range(99)]
        self.entropy_len = entropy_len

    # 是否要更换策略方向？
    def _check_entropy(self):
        if len(self.entropy) <= self.entropy_len:
            print('INIT ENTROPY...')
            begin_time = self.account.now_time - timedelta(days=self.entropy_len + 3)
            while begin_time < self.account.now_time:
                self._record_entropy(date=begin_time)
                begin_time += timedelta(days=1)

        empty = True
        print(self.account.pos_status)
        for coin in self.symbol_para_path_dict.keys():
            empty = empty & (self.account.pos_status[coin] == [])
        if empty:
            print('NOW EMPTY, CHECK ENTROPY STRATEGY CHANGING...')
            entropy_df = pd.DataFrame(self.entropy)[-self.entropy_len:]
            entropy_df.reset_index(drop=True, inplace=True)
            entropy_df['i'] = entropy_df.index + 1
            x = sm.add_constant(entropy_df['i'])
            y = entropy_df['entropy']
            model = sm.OLS(endog=y, exog=x)
            res = model.fit()
            change = False
            if dict(res.params)['i'] > 0:
                for coin in self.symbol_para_path_dict.keys():
                    if self.symbol_para_path_dict[coin] != 'long_%s_tremor_bolling_para_20200315' % coin:
                        change = True
                        print(coin, 'CHANGE TO TREMOR')
                        self.symbol_para_path_dict[coin] = 'long_%s_tremor_bolling_para_20200315' % coin
            else:
                for coin in self.symbol_para_path_dict.keys():
                    if self.symbol_para_path_dict[coin] != 'long_%s_bolling_para_20200315' % coin:
                        change = True
                        print(coin, 'CHANGE TO TREND')
                        self.symbol_para_path_dict[coin] = 'long_%s_bolling_para_20200315' % coin
            if change:
                self.para_series = self.gen_para_series(
                    symbol_para_path_dict=self.symbol_para_path_dict,
                    eg=self.eg
                )

    # 根据过去100天的KL散度判断策略
    def cal_strategy_sig(self, symbol, now_time):
        # 空仓 && KL散度方向发生变化 --> 改变策略
        self._check_entropy()

        df = self.get_symbol_data(symbol=symbol, now_time=now_time)
        # print(df, symbol)

        # 查看当前持仓
        if self.account.pos_status[symbol]:
            if self.account.pos_status[symbol][0] > 0:
                now_pos = 1
            else:
                now_pos = -1
        else:
            now_pos = 0

        func_str = 'real_time_signal_%s_without_stop_lose(now_pos, df, %s)' % \
                   (self.account.symbol_info[symbol]['strategy'], self.account.symbol_info[symbol]['para'])
        print('NOW', now_pos)
        eval_func = eval(func_str)
        print('TARGET', eval_func)
        # 根据当前、目标仓位计算信号。信号list中需要先平仓信号，再开仓信号
        if now_pos == 1 and eval_func == 0:  # 平多
            sig = ['long_close']
        elif now_pos == -1 and eval_func == 0:  # 平空
            sig = ['short_close']
        elif now_pos == 0 and eval_func == 1:  # 开多
            sig = ['long_open']
        elif now_pos == 0 and eval_func == -1:  # 开空
            sig = ['short_open']
        elif now_pos == 1 and eval_func == -1:  # 平多，开空
            sig = ['long_close', 'short_open']
        elif now_pos == -1 and eval_func == 1:  # 平空，开多
            sig = ['short_close', 'long_open']
        else:
            sig = []

        return sig

    # 计算今日的日内KL散度
    def _cal_entropy(self, t_data):
        mean = t_data['rtn'].mean()
        std = t_data['rtn'].std(ddof=1)
        sample_dist = []
        norm_dist = []
        for i in range(len(self.std_ran)):
            if i == 0:
                right = self.std_ran[i] * std
                cond = t_data['rtn'] < right
                sample_dist.append(len(t_data.loc[cond]) / len(t_data))
                norm_dist.append(norm.cdf(right, loc=mean, scale=std))

            elif (i > 0) and (i < len(self.std_ran) - 1):
                left = self.std_ran[i] * std
                right = self.std_ran[i + 1] * std
                cond1 = t_data['rtn'] >= left
                cond2 = t_data['rtn'] < right
                sample_dist.append(len(t_data.loc[cond1 & cond2]) / len(t_data))
                norm_dist.append(norm.cdf(right, loc=mean, scale=std) - norm.cdf(left, loc=mean, scale=std))

            elif i == len(self.std_ran) - 1:
                left = self.std_ran[i] * std
                cond = t_data['rtn'] >= left
                sample_dist.append(1 - sum(sample_dist))
                norm_dist.append(1 - sum(norm_dist))

        kl = entropy(sample_dist, norm_dist)
        return kl

    # 记录指定日期的KL散度
    def _record_entropy(self, date):
        # 计算entropy
        today_data = self.get_data(
            symbol='BTCUSD',
            begin=date.strftime('%Y-%m-%d %H:%M:%S'),
            end=date.strftime('%Y-%m-%d 23:55:00')
        )
        today_data['rtn'] = today_data['close'] / today_data['open'] - 1
        today_data.reset_index(drop=True, inplace=True)
        entropy = self._cal_entropy(t_data=today_data)
        self.entropy.append(
            {
                'date': date.strftime('%Y-%m-%d'),
                'entropy': entropy
            }
        )

    # 记录每日KL
    def take_snapshot(self):

        self._record_entropy(date=self.account.now_time)
        self.equity_curve['candle_begin_time'].append(self.account.now_time)
        self.equity_curve['equity_curve'].append(self.account.snapshot())


# 一个BackTest对象只在时间序列上移动一次
if __name__ == '__main__':

    l_localtime = int(time.time())
    l_entropy_len = 100

    l_save_name = '20200319_4COIN_ENTROPY_%s' % l_entropy_len
    l_output_root_path = r'E:\cta_v2\data\test_v2\output\oo_test'
    for roots, dirs, files in os.walk(l_output_root_path):
        if files:
            for file in files:
                if file.startswith(l_save_name):
                    raise Exception('存档已存在！')

    l_symbol_info = {
        'BTCUSD': {
            'proportion': 0.3,
            'para': [200.0, 2.0],
            'leverage': 1,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
        'ETHUSD': {
            'proportion': 0.3,
            'para': [200.0, 2.0],
            'leverage': 1,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
        'EOSUSD': {
            'proportion': 0.2,
            'para': [200.0, 2.0],
            'leverage': 1,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
        'LTCUSD': {
            'proportion': 0.2,
            'para': [200.0, 2.0],
            'leverage': 1,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
        }

    l_start_time = '2019-01-01 00:00:00'
    l_end_time = '2020-03-10 23:55:00'
    l_order_method = 'market'

    l_path_dict = {
        'BTCUSD': r'long_BTCUSD_tremor_bolling_para_20200315',
        'ETHUSD': r'long_ETHUSD_tremor_bolling_para_20200315',
        'EOSUSD': r'long_EOSUSD_tremor_bolling_para_20200315',
        'LTCUSD': r'long_LTCUSD_tremor_bolling_para_20200315'
    }

    time_start = time.time()
    bt1 = EntropyBasedTest(
        l_symbol_info,
        l_start_time,
        l_end_time,
        l_path_dict,
        l_output_root_path,
        l_save_name,
        l_localtime,
        l_entropy_len
    )

    assessment, equity_df = bt1.main_back_test()
    print(assessment)
    time_end = time.time()
    print(time_start, time_end, time_end-time_start)
