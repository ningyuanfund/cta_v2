import os
import time

import math

from test_v2.obj_ornt.external_func import *
from test_v2.obj_ornt.real_time_signal import *
from test_v2.obj_ornt.stop_method import *

pd.set_option('expand_frame_repr', False)

from multi_coin_cta_2020.test_RAM import BackTest

class VolBasedTest(BackTest):
    def __init__(
            self, symbol_info, start_time, end_time, symbol_para_path_dict, output_root_path, save_name, localtime
    ):
        BackTest. __init__(
                self, symbol_info, start_time, end_time, symbol_para_path_dict, output_root_path, save_name, localtime
        )
        self.vol_res = []

    def long_open(self, symbol, price):
        print(self.account.pos_status)
        print(self.account.symbol_info)

        print(self.account.now_time)
        vol_middle = self.get_vol_hist_rank()

        openable_amount = self.account.symbol_info[symbol]['available_cash'] / (price * (
                0.002 + 1 / self.account.symbol_info[symbol]['leverage']))

        if not vol_middle:
            openable_amount /= 2

        exe_amount = math.floor(openable_amount * 10000) / 10000  # 交易最小单位取小数点后四位
        self.account.long_open(symbol=symbol, price=price, amount=exe_amount, order_type='',
                               leverage=self.account.symbol_info[symbol]['leverage'])
        print(self.account.pos_status)
        print(self.account.symbol_info)

    def short_open(self, symbol, price):
        print(self.account.pos_status)
        print(self.account.symbol_info)

        print(self.account.now_time)
        vol_middle = self.get_vol_hist_rank()

        openable_amount = self.account.symbol_info[symbol]['available_cash'] / (
                price * (0.002 + 1 / self.account.symbol_info[symbol]['leverage']))
        if not vol_middle:
            openable_amount /= 2

        exe_amount = math.floor(openable_amount * 10000) / 10000  # 交易最小单位取小数点后四位
        self.account.short_open(symbol=symbol, price=price, amount=exe_amount, order_type='',
                                leverage=self.account.symbol_info[symbol]['leverage'])
        print(self.account.pos_status)
        print(self.account.symbol_info)

    def get_vol_hist_rank(self):
        if len(self.vol_res) == 0:
            self.vol_res = self._form_vol_list()

        t_vol_df = self.vol_res.loc[self.vol_res['date'] <= self.account.now_time].copy()
        if len(t_vol_df) > 100:
            last_vol = t_vol_df['rv'].iloc[-1]
            one_third = t_vol_df['rv'].quantile(0.333)
            two_third = t_vol_df['rv'].quantile(0.667)
            if (last_vol > one_third) & (last_vol <= two_third):
                return True
            else:
                return False
        else:
            return False

    def _form_vol_list(self):
        print('FORM VOL...')
        data = self.data['BTCUSD'].reset_index(drop=True)
        data['rtn'] = data['close'].apply(lambda x: np.log(x)) - data['close'].apply(lambda x: np.log(x)).shift(1)
        for i in range(30 * 24 * 12, len(data), 24 * 12):
            t_data = data[i - 30 * 24 * 12: i].copy()
            self.vol_res.append(
                {
                    'date': t_data['candle_begin_time'].iloc[-1],
                    'rv': t_data['rtn'].std(ddof=1)
                }
            )

        vol_df = pd.DataFrame(self.vol_res)
        return vol_df



# 一个BackTest对象只在时间序列上移动一次
if __name__ == '__main__':

    l_localtime = int(time.time())

    l_save_name = '20200317_BTCETHLTC_vol_mid'
    l_output_root_path = r'E:\cta_v2\data\test_v2\output\oo_test'
    for roots, dirs, files in os.walk(l_output_root_path):
        if files:
            for file in files:
                if file.startswith(l_save_name):
                    raise Exception('存档已存在！')

    l_symbol_info = {
        'BTCUSD': {
            'proportion': 0.5,
            'para': [200.0, 2.0],
            'leverage': 1,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
        'ETHUSD': {
            'proportion': 0.3,
            'para': [200.0, 2.0],
            'leverage': 1,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
        'LTCUSD': {
            'proportion': 0.2,
            'para': [200.0, 2.0],
            'leverage': 1,
            'time_interval': '15T',
            'strategy': 'bolling',
            'order_method': 'market',
            'stop_loss_method': 'float_hard_pct',
            'stop_para': [3, 15]
        },
        }

    l_start_time = '2017-05-31 00:00:00'
    l_end_time = '2020-04-10 23:55:00'
    l_order_method = 'market'

    l_path_dict = {
        'BTCUSD': r'30_30_bolling_BTCUSD_20200412',
        'ETHUSD': r'30_30_bolling_ETHUSD_20200412',
        'LTCUSD': r'30_30_bolling_LTCUSD_20200412'
    }

    time_start = time.time()
    bt1 = VolBasedTest(
        l_symbol_info,
        l_start_time,
        l_end_time,
        l_path_dict,
        l_output_root_path,
        l_save_name,
        l_localtime
    )

    assessment, equity_df = bt1.main_back_test()
    print(assessment)
    time_end = time.time()
    print(time_start, time_end, time_end-time_start)