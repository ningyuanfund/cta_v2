from datetime import datetime, timedelta

import pandas as pd
from sqlalchemy import create_engine

pd.set_option('expand_frame_repr', False)

eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')

# 生成时间列，用于遍历数据（独立于数据）
def get_time(start, end):

    end_time = datetime.strptime(end, "%Y-%m-%d %H:%M:%S")
    now_time = datetime.strptime(start, "%Y-%m-%d %H:%M:%S")

    time_list = [now_time]
    while now_time < end_time:
        now_time += timedelta(minutes=5)
        time_list.append(now_time)
    return time_list


for symbol in ['BTCUSD', 'ETHUSD', 'EOSUSD', 'LTCUSD', 'BTCUSD_long']:
    sql = 'select * from "cta_test"."%s"' % symbol
    data = pd.read_sql_query(sql, con=eg)
    data['candle_begin_time'] = pd.to_datetime(data['candle_begin_time'])
    data.sort_values(by='candle_begin_time', inplace=True, ascending=True)
    data = data[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()
    begin_time = data['candle_begin_time'].iloc[0].strftime('%Y-%m-%d %H:%M:%S')
    end_time = data['candle_begin_time'].iloc[-1].strftime('%Y-%m-%d %H:%M:%S')

    tm_list = get_time(begin_time, end_time)
    tm_df = pd.DataFrame(tm_list)
    tm_df.rename(columns={0: 'candle_begin_time'}, inplace=True)

    data = data.merge(tm_df, how='outer', on='candle_begin_time')
    data.sort_values(by='candle_begin_time', inplace=True)
    data['open'].fillna(method='ffill', inplace=True)
    data['high'].fillna(method='ffill', inplace=True)
    data['low'].fillna(method='ffill', inplace=True)
    data['close'].fillna(method='ffill', inplace=True)
    data['volume'].fillna(value=0, inplace=True)

    data.reset_index(inplace=True, drop=True)
    data = data[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()
    data.sort_values(by='candle_begin_time', inplace=True, ascending=True)
    data.reset_index(drop=True, inplace=True)
    data.to_sql(symbol, schema='cta_test', if_exists='replace', con=eg)
    print(symbol)