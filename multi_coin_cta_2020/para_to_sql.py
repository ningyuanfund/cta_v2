import pandas as pd
from sqlalchemy import create_engine

para_eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')
pd.set_option('expand_frame_repr', False)

strategy = '30_30_bolling'
date = '20200414'
for coin in ['BTCUSD', 'ETHUSD', 'LTCUSD']:
# for coin in ['ETHUSD']:
    data_path = r'E:\cta_v2\multi_coin_cta_2020\temp_para_data\%s_%s.csv' % (strategy, coin)
    para_data = pd.read_csv(data_path)
    para_data.columns = ['1', '参数', '时间周期', '参数训练开始', '参数训练结束', '参数使用至']
    for col in ['参数训练开始', '参数训练结束', '参数使用至']:
        para_data[col] = pd.to_datetime(para_data[col])
    para_data = para_data[['参数训练开始', '参数训练结束', '参数使用至', '时间周期', '参数']].copy()
    para_data.to_sql('%s_%s_%s_20200415' % (strategy, coin, date), con=para_eg, schema='params', if_exists='replace')
    print(para_data)