import json
import time
from datetime import datetime

import requests


# 发送钉钉消息，id填上使用的机器人的id
def send_dingding_msg(content, robot_id='e7ae7cc48dd987152cf748e1cc2973a722a01577653faad975e90b357bd6b084'):
    try:
        msg = {
            "msgtype": "text",
            "text": {"content": content + '\n' + datetime.now().strftime("%m-%d %H:%M:%S")}}
        headers = {"Content-Type": "application/json;charset=utf-8"}
        url = 'https://oapi.dingtalk.com/robot/send?access_token=' + robot_id
        body = json.dumps(msg)
        requests.post(url, data=body, headers=headers)
        print('成功发送钉钉')
    except Exception as e:
        print("发送钉钉失败:", e)


# 睡眠大师
def lets_sleep():
    time.sleep(5)


if __name__ == '__main__':
    return_signal = [
        {
            'symbol': '1',
            'check_time': '20190101',
            'should_open': -1,
            'stop_loss': -1,
            'stop_profit': 4,
            'open_price': '',
            'w2l': 4
        },
        {
            'symbol': '2',
            'check_time': '20190101',
            'should_open': -1,
            'stop_loss': -2,
            'stop_profit': 5,
            'open_price': '',
            'w2l': 2.5
        },
        {
            'symbol': '3',
            'check_time': '20190101',
            'should_open': -1,
            'stop_loss': -3,
            'stop_profit': 6,
            'open_price': '',
            'w2l': 2
        },
        {
            'symbol': '4',
            'check_time': '20190101',
            'should_open': -1,
            'stop_loss': -0.2,
            'stop_profit': 1,
            'open_price': '',
            'w2l': 5
        },
        {
            'symbol': '5',
            'check_time': '20190101',
            'should_open': -1,
            'stop_loss': -0.1,
            'stop_profit': 0.5,
            'open_price': '',
            'w2l': 5
        }
    ]
    import pandas as pd

    return_sig_df = pd.DataFrame(return_signal)

    cond1 = (return_sig_df['stop_loss'].abs() >= 2) & (return_sig_df['w2l'].abs() >= 2)
    cond2 = (return_sig_df['stop_profit'].abs() >= 2) & (return_sig_df['w2l'].abs() >= 2)
    cond3 = (return_sig_df['w2l'].abs() >= 2)
    filter1_df = return_sig_df.loc[cond1].copy()
    filter1_df.reset_index(inplace=True, drop=True)
    filter2_df = return_sig_df.loc[cond2].copy()
    filter2_df.reset_index(inplace=True, drop=True)
    filter3_df = return_sig_df.loc[cond3].copy()
    filter3_df.reset_index(inplace=True, drop=True)
    if len(filter1_df) > 0:
        filter1_df.sort_values(by=['w2l', 'stop_profit'], ascending=False, inplace=True)
        strongest_sig = dict(filter1_df.iloc[0])
    elif len(filter2_df) > 0:
        filter2_df.sort_values(by=['w2l', 'stop_profit'], ascending=False, inplace=True)
        strongest_sig = dict(filter2_df.iloc[0])
    elif len(filter3_df) > 0:
        filter3_df.sort_values(by=['w2l', 'stop_profit'], ascending=False, inplace=True)
        strongest_sig = dict(filter3_df.iloc[0])
    else:
        strongest_sig = {}

