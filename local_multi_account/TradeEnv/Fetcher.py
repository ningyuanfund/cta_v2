import ccxt
import pandas as pd

from lspr.TradeEnv.func import *


class Fetcher:
    def __init__(self, api_key, secret, real_trade):
        if real_trade:
            self.exchange = ccxt.bitfinex({
                            'apiKey': api_key,
                            'secret': secret
            })
            self.exchange.load_markets()
            self.exchange2 = ccxt.bitfinex2({
                            'apiKey': api_key,
                            'secret': secret
            })
            self.exchange2.load_markets()

    # 根据订单号获取平均成交价格
    def get_avg_price(self, order_id):
        order_execution = self.exchange.private_post_order_status(params={'order_id': int(order_id)})
        avg_price = float(order_execution['avg_execution_price'])  # 订单平均成交价格
        lets_sleep()

        return avg_price

    # 获取对手价（使用了交易所API）
    def get_opposite_price(self, symbol):
        get_symbol = 't' + symbol

        price_info = self.exchange2.public_get_ticker_symbol(params={'symbol': get_symbol})

        opposite_price = {'highest_bid': price_info[0], 'lowest_ask': price_info[2]}

        lets_sleep()
        return opposite_price

    # 获取某个品种的最新数据
    def get_latest_data(self, symbol, time_interval):
        """

        :param symbol: BTCUSD -> BTC/USD
        :param time_interval:  1h
        :return:
        """
        limit = 1000
        fetch_symbol = symbol[:3] + '/' + symbol[3:]
        begin = self.exchange.milliseconds() - limit * 60 * 60 * 1000

        while True:
            try:
                content = self.exchange.fetch_ohlcv(symbol=fetch_symbol,
                                                    timeframe=time_interval,
                                                    since=begin,
                                                    limit=limit)
                break
            except Exception as e:
                print(e)
                time.sleep(5)

        df = pd.DataFrame(content, dtype=float)
        df.rename(columns={0: 'MTS', 1: 'open', 2: 'high', 3: 'low', 4: 'close', 5: 'volume'}, inplace=True)
        df['candle_begin_time'] = pd.to_datetime(df['MTS'], unit='ms')
        df = df[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']]

        lets_sleep()
        return df

    # 获取账户可用资金
    def get_margin_balance(self):

        margin_info = self.exchange.private_post_margin_infos()
        margin_balance = margin_info[0]['margin_balance']
        return margin_balance

    # 查询订单状态
    def get_order_status(self, order_id):
        """

        :param order_id:
        :return:
            {
                'info':
                    {
                        'id': 30863034900,
                        'cid': 54215435533,
                        'cid_date': '2019-09-10',
                        'gid': None,
                        'symbol': 'ltcusd',
                        'exchange': 'bitfinex',
                        'price': '68.815',
                        'avg_execution_price': '70.108',
                        'side': 'sell',
                        'type': 'limit',
                        'timestamp': '1568127815.0',
                        'is_live': False,
                        'is_cancelled': False,
                        'is_hidden': False,
                        'oco_order': None,
                        'was_forced': False,
                        'original_amount': '7.54',
                        'remaining_amount': '0.0',
                        'executed_amount': '7.54',
                        'src': 'api', 'meta': None
                    },
                'id': '30863034900',
                'timestamp': 1568127815000,
                'datetime': '2019-09-10T15:03:35.000Z',
                'lastTradeTimestamp': None,
                'symbol': 'LTC/USD',
                'type': 'limit',
                'side': 'sell',
                'price': 68.815,
                'average': 70.108,
                'amount': 7.54,
                'remaining': 0.0,
                'filled': 7.54,
                'status': 'closed',
                'fee': None
            }

        """
        order_info = self.exchange.fetch_order(id=str(order_id))
        return order_info


if __name__ == '__main__':
    fetcher = Fetcher(api_key='scxIZEVc7hwB8GH7Qt9YDxYhGyrMTf5hS5lPMHV22Te',
                      secret='B7DWq8E7GhnKhvjE8IjSzFjpMlfHGKkFBjiCbYnsVWI',
                      real_trade=True)

    info = fetcher.get_order_status(order_id='30863034900')
    print(info)

    # remove_ind = []
    # li = [1, 2,3 ,4 ,5 ,6 ,7]
    # for i in  range(len(li)):
    #     if li[i] == 5:
    #         remove_ind.append(i)
    # for i in remove_ind:
    #     li.remove(li[i])
    # print(li)