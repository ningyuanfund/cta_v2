import time
from datetime import datetime, timedelta

import pandas as pd

from local_multi_account.AccountEnv.AccountEnv import AccountEnv
from local_multi_account.StrategyEnv.SignalGenerator import positioin_score
from local_multi_account.StrategyEnv.StrategyEnv import StrategyEnv
from local_multi_account.TradeEnv.Fetcher import Fetcher
from local_multi_account.TradeEnv.Trader import Trader


class MainEnv:
    def __init__(self, api_key, secret, real_trade, account_num):
        """
        :param api_key:
        :param secret:
        :param para:{'account_num': 24, 'strategy_para': [1, 2, 3]}
        """
        self._trader = Trader(api_key=api_key, secret=secret, real_trade=real_trade)
        self._fetcher = Fetcher(api_key=api_key, secret=secret, real_trade=real_trade)
        self.strategy_env = StrategyEnv()
        self.account_env = AccountEnv(account_num=account_num,
                                      trader=self._trader,
                                      fetcher=self._fetcher,
                                      real_trade=real_trade,
                                      first_run=True)
        self.hold_hour = {'BTCUSD': 24,
                          'ETHUSD': 24,
                          'EOSUSD': 24,
                          'BCHUSD': 24,
                          'LTCUSD': 24,
                          'XRPUSD': 24,
                          'ETCUSD': 24,}

    @staticmethod
    def next_run_time(time_interval, ahead_time=1):
        """
        15分钟运行一次
        :param time_interval 运行周期时间 建议不要小于5min
        :param ahead_time 当小于1s时 自动进入下个周期
        :return:
        """
        if time_interval.endswith('T'):
            now_time = datetime.now()
            time_interval = int(time_interval.strip('T'))

            target_min = (int(now_time.minute / time_interval) + 1) * time_interval
            if target_min < 60:
                target_time = now_time.replace(minute=target_min, second=0, microsecond=0)
            else:
                if now_time.hour == 23:
                    target_time = now_time.replace(hour=0, minute=0, second=0, microsecond=0)
                    target_time += timedelta(days=1)
                else:
                    target_time = now_time.replace(hour=now_time.hour + 1, minute=0, second=0, microsecond=0)

            # sleep直到靠近目标时间之前
            if (target_time - datetime.now()).seconds < ahead_time + 1:
                print('距离target_time不足', ahead_time, '秒，下下个周期再运行')
                target_time += timedelta(minutes=time_interval)
            print('下次运行时间', target_time)
            return target_time
        else:
            exit('time_interval doesn\'t end with T')

    @staticmethod
    def sleep_til_target_time(target_time):
        while True:
            if datetime.now() < target_time - timedelta(seconds=300):
                time.sleep(120)
                continue
            else:
                break
        # 直到下次运行的时间再启动
        time.sleep(max(0, (target_time - datetime.now()).seconds))

        # 在靠近目标时间时
        while True:
            if datetime.now() < target_time:
                continue
            else:
                break

    def initial_synchronize(self):
        for account_ind in range(len(self.account_env.account_list)):
            self.account_env.account_list[account_ind].update()
        self.account_env.check_rebalance()
        print('INITIAL SYNCHRONIZE COMPLETE')
        print('NOW ACCOUNT')
        print('=' * 20)
        for account in self.account_env.account_list:
            print(account)

    def is_hour(self):
        """
        查看是否是整点
        :return:
        """
        now_min = int(pd.to_datetime(self._fetcher.exchange.milliseconds(), unit='ms').strftime('%M'))
        if now_min == 0:
            return True
        else:
            return False

    def _is_hour(self):
        return True

    @staticmethod
    def _compare_signal(symbol_signal_set):
        """
        比较信号强度
        条件A：W2L最大，如果W2L相等，绝对波动最大

        第一批：loss绝对值大于2且w2l大于2，从中找符合条件A的
        第二批：win大于2且w2l大于2，从中找符合条件A的
        第三批：win和loss绝对值都小于2，但w2l大于2，从中找符合条件A的

        三批都没有，不开仓
        :param symbol_signal_set:
        :return:
        """
        # 改变格式
        return_signal = []

        for signal_symbol in symbol_signal_set:

            return_signal.append(
                {
                    'symbol': signal_symbol,
                    'check_time': symbol_signal_set[signal_symbol]['check_time'],
                    'should_open': symbol_signal_set[signal_symbol]['should_open'],
                    'stop_loss': symbol_signal_set[signal_symbol]['stop_loss'],
                    'stop_profit': symbol_signal_set[signal_symbol]['stop_profit'],
                    'open_price': symbol_signal_set[signal_symbol]['open_price'],

                }
            )
        print('FIND STRONGEST SIGNAL')
        print(return_signal)

        return_sig_df = pd.DataFrame(return_signal)
        return_sig_df['stop_loss'] = (return_sig_df['stop_loss'] / return_sig_df['open_price']) - 1
        return_sig_df['stop_profit'] = (return_sig_df['stop_profit'] / return_sig_df['open_price']) - 1
        return_sig_df['score'] = 0
        for i in range(len(return_sig_df)):
            if return_sig_df['stop_profit'].iloc[i] > 0:
                return_sig_df['score'].iloc[i] = positioin_score(pre_high=return_sig_df['stop_profit'].iloc[i] * 100,
                                                                 pre_low=return_sig_df['stop_loss'].iloc[i] * 100)
            else:
                return_sig_df['score'].iloc[i] = positioin_score(pre_high=return_sig_df['stop_loss'].iloc[i] * 100,
                                                                 pre_low=return_sig_df['stop_profit'].iloc[i] * 100)
        # return_sig_df['w2l'] = return_sig_df['stop_profit'].apply(lambda x: x.abs()) / return_sig_df['stop_loss'].apply(lambda x: x.abs())
        # print(return_sig_df)
        #
        # cond1 = (return_sig_df['stop_loss'].abs() >= 2) & (return_sig_df['w2l'].abs() >= 2)
        # cond2 = (return_sig_df['stop_profit'].abs() >= 2) & (return_sig_df['w2l'].abs() >= 2)
        # cond3 = (return_sig_df['w2l'].abs() >= 2)
        # filter1_df = return_sig_df.loc[cond1].copy()
        # filter1_df.reset_index(inplace=True, drop=True)
        # filter2_df = return_sig_df.loc[cond2].copy()
        # filter2_df.reset_index(inplace=True, drop=True)
        # filter3_df = return_sig_df.loc[cond3].copy()
        # filter3_df.reset_index(inplace=True, drop=True)
        # if len(filter1_df) > 0:
        #     filter1_df.sort_values(by=['w2l', 'stop_profit'], ascending=False, inplace=True)
        #     strongest_sig = dict(filter1_df.iloc[0])
        # elif len(filter2_df) > 0:
        #     filter2_df.sort_values(by=['w2l', 'stop_profit'], ascending=False, inplace=True)
        #     strongest_sig = dict(filter2_df.iloc[0])
        # elif len(filter3_df) > 0:
        #     filter3_df.sort_values(by=['w2l', 'stop_profit'], ascending=False, inplace=True)
        #     strongest_sig = dict(filter3_df.iloc[0])
        # else:
        #     strongest_sig = {}
        return_sig_df.sort_values(by='score', ascending=False, inplace=True)

        # 取消实际止损
        cond = return_sig_df['should_open'] == 1
        return_sig_df['stop_loss'] = 0.2
        return_sig_df.loc[cond, 'stop_loss'] = -0.2

        strongest_sig = dict(return_sig_df.iloc[0])
        print('STRONGEST SIGNAL')
        print(strongest_sig)
        return strongest_sig

    def _gen_strategy_signal(self, target_time, filter_data):
        """
        计算当前时间的信号，哪个symbol信号最强就用哪个symbol开仓
        :param target_time:
        :return:
            {'EOSUSD':
                {
                    'check_time': int(pd.to_datetime(now_time).strftime('%H')),
                    'should_open': now_pos['position'],
                    'stop_loss': now_price * (1 + now_pos['stop_loss'] / 100),
                    'stop_profit': now_price * (1 + now_pos['stop_profit'] / 100),
                    'open_price': now_price,
                },
            }
        """
        # 查看哪些账户还可以开仓，确定可开的symbol
        available_symbol = []
        for account in self.account_env.account_list:
            if account.position == {}:
                available_symbol.extend(list(account.symbol_leverage.keys()))
        available_symbol = list(set(available_symbol))

        target_time -= timedelta(hours=8)
        # 获取数据
        dataset = {}
        for symbol in available_symbol:
            max_tries = 10
            for i in range(max_tries):
                data = self._fetcher.get_latest_data(symbol=symbol, time_interval='1h')
                print(data)
                print(target_time)
                if filter_data:
                    _temp = data[data['candle_begin_time'] == target_time]
                    if _temp.empty:
                        print('%s NO NEW DATA' % symbol)
                        time.sleep(5)
                    else:
                        data = data[data['candle_begin_time'] < target_time].copy()
                        dataset[symbol] = data
                        break
                    if i == max_tries - 1:
                        continue
                else:
                    dataset[symbol] = data
                    break

        # 计算每个symbol的signal和强度
        symbol_signal_set = {}
        for symbol in available_symbol:
            print('=== %s GEN SIGNAL ===' % symbol)
            if symbol in dataset:
                symbol_signal_set[symbol] = self.strategy_env.position(data=dataset[symbol], symbol=symbol)
                print('%s SIGNAL' % symbol)
                print(symbol_signal_set[symbol])
        strategy_signal = self._compare_signal(symbol_signal_set=symbol_signal_set)

        return strategy_signal

    def _gen_account_close_signal(self):
        """
        查看有没有账户需要平仓
        :return:
        [
            {
                'account_id': account.id,
                'symbol': account.position['symbol'],
                'now_amount': account.position['open_amount'],
            },
        ]
        """
        account_close_signal_set = []
        for account in self.account_env.account_list:
            # 达到了该账户的最晚平仓时间
            if account.latest_close_time == None:
                cond1 = True
            else:
                cond1 = pd.to_datetime(account.latest_close_time) <= \
                        pd.to_datetime(self._fetcher.exchange.milliseconds(), unit='ms')
            cond2 = (account.position != {})
            if cond1 & cond2:
                account_close_signal_set.append(
                    {
                        'account_id': account.id,
                        'symbol': account.position['symbol'],
                        'now_amount': account.position['open_amount'],
                    }
                )
        return account_close_signal_set

    def gen_real_signal(self, target_time, filter_data):
        """
        根据当前时点的开仓信号和平仓信号，确定真实需要调仓的信号
        :param target_time:
        :return:
        true_open_signal                {
                    'symbol': signal_symbol,
                    'check_time': symbol_signal_set[signal_symbol]['check_time'],
                    'should_open': symbol_signal_set[signal_symbol]['should_open'],
                    'stop_loss': symbol_signal_set[signal_symbol]['stop_loss'],
                    'stop_profit': symbol_signal_set[signal_symbol]['stop_profit'],
                    'open_price': symbol_signal_set[signal_symbol]['open_price'],
                    'w2l': abs(symbol_signal_set[signal_symbol]['stop_profit'] /
                               symbol_signal_set[signal_symbol]['stop_loss'])
                }
        """
        strategy_signal = self._gen_strategy_signal(target_time=target_time, filter_data=filter_data)
        account_close_signal = self._gen_account_close_signal()
        true_open_signal = []
        true_close_signal = []

        # 不需要到期平仓
        if len(account_close_signal) == 0:
            return [strategy_signal], []

        for close_signal in account_close_signal:
            for open_signal in strategy_signal:
                cond1 = (close_signal['symbol'] == open_signal['symbol'])
                cond2 = (strategy_signal[close_signal['symbol']]['should_open'] * close_signal['now_amount'] > 0)
                if cond1 & cond2:
                    self.account_env.prolong_latest_date(account_id=close_signal['account_id'],
                                                         hours=self.hold_hour[close_signal['symbol']])
                else:
                    true_open_signal.append(open_signal)
                    true_close_signal.append(close_signal)
        true_close_signal = list(set(true_close_signal))
        true_open_signal = list(set(true_open_signal))

        return true_open_signal, true_close_signal

    def change_position(self, open_signal, close_signal):
        """
        调仓
        :param open_signal:
        :param close_signal:
        :return:
        """
        # 先平仓
        for signal in close_signal:
            for account_ind in range(len(self.account_env.account_list)):
                if self.account_env.account_list[account_ind].id == signal['account_id']:
                    now_symbol = self.account_env.account_list[account_ind].position['symbol']
                    now_amount = self.account_env.account_list[account_ind].position['open_amount']
                    if now_amount > 0:
                        close_side = 'sell'
                    else:
                        close_side = 'buy'
                    self.account_env.account_list[account_ind].close(symbol=now_symbol,
                                                                     amount=abs(now_amount),
                                                                     buy_or_sell=close_side,
                                                                     order_type='market')
                    break

        # 再开仓
        for signal in open_signal:
            for account_ind in range(len(self.account_env.account_list)):
                if self.account_env.account_list[account_ind].position == {}:
                    if signal['should_open'] == 1:
                        open_side = 'buy'
                        now_price = self._fetcher.get_opposite_price(symbol=signal['symbol'])['lowest_ask']
                    else:
                        open_side = 'sell'
                        now_price = self._fetcher.get_opposite_price(symbol=signal['symbol'])['highest_bid']

                    amount = round(self.account_env.account_list[account_ind].cash / now_price, 5)
                    self.account_env.account_list[account_ind].open(symbol=signal['symbol'],
                                                                    amount=amount,
                                                                    buy_or_sell=open_side,
                                                                    stop_para={'stop_profit': signal['stop_profit'],
                                                                               'stop_loss': signal['stop_loss']},
                                                                    order_type='market',
                                                                    hold_hours=self.hold_hour[signal['symbol']]
                                                                    )
                    break

    def update_all_account(self):
        """
        更新所有账户信息，记录到本地
        :return:
        """
        for account_ind in range(len(self.account_env.account_list)):
            self.account_env.account_list[account_ind].update()
        self.account_env.record_to_json()




if __name__ == '__main__':
    api_key = 'kaSx747CCm23VFvtp31P9i81tOK3STdHsr9ZJLjTbDP'
    secret = 'M7TgT6T4HvrwBjxMh1mTRMvtLWooQ7vdSO0CFjD6xu0'
    main_env = MainEnv(api_key=api_key, secret=secret, real_trade=True, account_num=10)
    print(main_env.is_hour())
