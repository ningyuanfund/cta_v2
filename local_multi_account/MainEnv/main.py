from local_multi_account.MainEnv.MainEnv import MainEnv

if __name__ == '__main__':

    api_key = 'kaSx747CCm23VFvtp31P9i81tOK3STdHsr9ZJLjTbDP'
    secret = 'M7TgT6T4HvrwBjxMh1mTRMvtLWooQ7vdSO0CFjD6xu0'

    # 初始化本地环境
    while True:
        try:
            main_env = MainEnv(api_key=api_key,
                               secret=secret,
                               real_trade=True,
                               account_num=24)
            # 初始同步
            main_env.initial_synchronize()
            break
        except Exception as e:
            print(e)

    # 运行
    while True:
        # try:
            target_time = main_env.next_run_time('10T')
            main_env.sleep_til_target_time(target_time=target_time)
            if main_env.is_hour():
                open_signal, close_signal = main_env.gen_real_signal(target_time=target_time, filter_data=True)

                main_env.change_position(open_signal=open_signal, close_signal=close_signal)
            main_env.update_all_account()

        # except Exception as e:
        #     print(e)
        #     raise Exception
