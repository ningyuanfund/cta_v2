from datetime import timedelta

import pandas as pd


class LocalAccount:
    def __init__(self, local_info, fetcher, trader):
        self.id = local_info['account_id']
        self.symbol_leverage = local_info['symbol_leverage']
        self.cash = local_info['available_cash']
        self.position = local_info['now_position']
        self.hist_order = local_info['hist_order']
        self.last_open_time = local_info['last_open_time']
        self.latest_close_time = local_info['latest_close_time']
        self.last_rebalance_time = local_info['last_rebalance_time']
        self._fetcher = fetcher
        self._trader = trader

    def __repr__(self):
        return 'ID:%s\nCash:%s\nPosition:%s\nLastOpenTime:%s\nLatestCloseTime:%s\n' % \
               (self.id, self.cash, self.position, self.last_open_time, self.latest_close_time) + '=' * 20

    def open(self, symbol, amount, buy_or_sell, stop_para, order_type, hold_hours):
        """
        开仓，下单
        记录历史下单，更新上次开仓时间，更新最晚平仓时间，更新当前仓位
        :param symbol:
        :param amount:
        :param side:
        :param stop_para:
        :return:[open_bookkeep_info, oco_bookkeep_info]
        """
        # 获取价格
        oppo_price = self._fetcher.get_opposite_price(symbol=symbol)
        if buy_or_sell == 'buy':
            oco_side = 'sell'
            order_price = oppo_price['highest_bid'] * 0.98
        else:
            oco_side = 'buy'
            order_price = oppo_price['lowest_ask'] * 1.02

        open_order_info = self._trader.place_order(order_type=order_type,
                                                   buy_or_sell=buy_or_sell,
                                                   symbol=symbol,
                                                   price=order_price,
                                                   amount=amount)
        price = open_order_info['price']
        stop_loss_price = price * (1 + stop_para['stop_loss'])
        stop_profit_price = price * (1 + stop_para['stop_profit'])

        oco_info = self._trader.place_oco_order(symbol=symbol,
                                                open_amount=amount,
                                                stop_profit_price=stop_profit_price,
                                                stop_loss_price=stop_loss_price,
                                                side=oco_side)

        open_bookkeep_info = {
            'symbol': symbol,
            'order_id': open_order_info['id'],
            'order_type': order_type,
            'amount': amount,
            'price': price,
            'order_status': 'filled',
            'order_time': pd.to_datetime(open_order_info['timestamp'], unit='ms').strftime('%Y%m%d %H:%M:%S'),
            'update_time': pd.to_datetime(open_order_info['timestamp'], unit='ms').strftime('%Y%m%d %H:%M:%S')
        }

        oco_bookkeep_info = {
            'symbol': symbol,
            'order_id': oco_info['id'],
            'order_type': 'OCO',
            'amount': amount,
            'stop_loss_price': stop_loss_price,
            'stop_profit_price': stop_profit_price,
            'order_status': 'live',
            'order_time': pd.to_datetime(open_order_info['timestamp'], unit='ms').strftime('%Y%m%d %H:%M:%S'),
            'update_time': pd.to_datetime(open_order_info['timestamp'], unit='ms').strftime('%Y%m%d %H:%M:%S')
        }

        record_time = pd.to_datetime(open_order_info['timestamp'], unit='ms').strftime('%Y-%m-%d %H:00:00')
        latest_close_time = (pd.to_datetime(open_order_info['timestamp'], unit='ms') + timedelta(hours=hold_hours))\
            .strftime('%Y-%m-%d %H:00:00')

        # 记录开仓信息
        self.hist_order.append(open_bookkeep_info)
        self.hist_order.append(oco_bookkeep_info)
        self.last_open_time = record_time
        self.latest_close_time = latest_close_time
        self.position = \
            {
                'symbol': symbol,
                'open_time': record_time,
                'open_amount': amount,
                'open_price': price,
                'profit_unrealized': 0
            }

    def close(self, symbol, amount, buy_or_sell, order_type):
        """
        正常平仓，下单
        记录历史下单，更新上次开仓时间'None'，更新最晚平仓时间'None'
        更新当前仓位，更新可用资金（关注手续费需要打折0.993）
        将当前的oco持仓订单状态改为canceled
        :param symbol:
        :param amount:
        :param side:
        :return:
        """
        oppo_price = self._fetcher.get_opposite_price(symbol=symbol)
        if buy_or_sell == 'buy':
            order_price = oppo_price['lowest_ask'] * 1.02
        else:
            order_price = oppo_price['highest_bid'] * 0.98

        close_order_info = self._trader.place_order(order_type=order_type,
                                                    buy_or_sell=buy_or_sell,
                                                    symbol=symbol,
                                                    price=order_price,
                                                    amount=abs(amount))

        close_price = close_order_info['info']['avg_execution_price']

        # 计算利润
        if buy_or_sell == 'sell':  # 多头平仓
            profit = abs(amount) * (close_price - self.position['open_price'])
        else:  # 空头平仓
            profit = abs(amount) * (self.position['open_price'] - close_price)

        oco_order_id = self.get_oco_order_id()
        if oco_order_id == '':
            print('OCO NOT FOUND')
        else:
            cancel_info = self._trader.cancel_order(order_id=oco_order_id)
            # 在本地记录里把oco的单记录为canceled
            for i in range(len(self.hist_order)):
                if self.hist_order[i]['order_id'] == oco_order_id:
                    self.hist_order[i]['order_status'] = 'canceled'

            print('CANCEL ORDER: ', oco_order_id)
            print(cancel_info)

        close_bookkeep_info = {
            "symbol": "EOSUSD",
            'order_id': close_order_info['id'],
            'order_type': order_type,
            'amount': amount,
            'price': order_price,
            'order_status': 'live',
            'order_time': pd.to_datetime(close_order_info['timestamp'], unit='ms').strftime('%Y%m%d %H:%M:%S'),
            'update_time': pd.to_datetime(close_order_info['timestamp'], unit='ms').strftime('%Y%m%d %H:%M:%S')
        }

        # 更新信息
        self.hist_order.append(close_bookkeep_info)
        self.last_open_time = None
        self.latest_close_time = None
        self.position = {}

        self.cash = (self.cash + profit) * 0.993


    def get_oco_order_id(self):
        """
        找还没平掉的OCO单
        :return:
        """
        for order in self.hist_order:
            if (order['order_type'] == 'OCO') & (order['order_status'] == 'live'):

                return order['order_id']

        print('LIVE OCO NOT FOUND')
        return ''

    def update(self):
        """
        更新当前账户信息
        根据历史下单中的OCO订单状态，更新当前账户状态：是否live
            如果否，更新上次开仓时间'None'，更新最晚平仓时间'None'，
                更新当前仓位，更新可用资金（用OCO的成交价格，关注手续费需要打折0.993）
            如果是，根据当前价格更新当前仓位信息，即未实现盈亏
        全部账户更新完成后需要记录到本地！
        :return:
        """
        local_oco_live = False
        exchange_oco_live = False

        if len(self.hist_order) != 0:
            for i in range(len(self.hist_order)):
                # 如果本地记录有OCO的未平单，说明1.可能真的没平；2.可能已经被吃了但没更新
                if (self.hist_order[i]['order_status'] == 'live') & (self.hist_order[i]['order_type'] == 'OCO'):
                    local_oco_live = True  # 本地有OCO的未平单记录
                    oco_order_id = self.hist_order[i]['order_id']
                    now_order_status = self._fetcher.get_order_status(order_id=self.hist_order[i]['order_id'])
                    if now_order_status['info']['is_live']:
                        exchange_oco_live = True

            # 本地都没有OCO订单，说明没持仓
            if not local_oco_live:
                pass
            # 本地OCO有未平单，但交易所没有，说明已经被吃掉了，但没更新
            elif local_oco_live & (not exchange_oco_live):
                # 更新上次开仓时间'None'，更新最晚平仓时间'None'，
                # 更新当前仓位，更新可用资金（用OCO的成交价格，关注手续费需要打折0.993）
                # 更新hist_order里的OCO订单的状态，为filled
                self.last_open_time = None
                self.latest_close_time = None
                self.position = {}
                oco_close_info = self._fetcher.get_order_status(order_id=oco_order_id)
                oco_exec_price = oco_close_info['average']

                if oco_close_info['info']['side'] == 'sell':  # 多头平仓
                    profit = abs(float(oco_close_info['amount'])) * (oco_exec_price - self.position['open_price'])
                else:  # 空头平仓
                    profit = abs(float(oco_close_info['amount'])) * (self.position['open_price'] - oco_exec_price)
                self.cash = (self.cash + profit) * 0.993

            # 本地和交易所都有未平单，更新未实现盈亏
            elif local_oco_live & exchange_oco_live:
                oppo_price = self._fetcher.get_opposite_price(symbol=now_order_status['info']['symbol'].upper())
                now_price = (oppo_price['highest_bid'] + oppo_price['lowest_ask']) / 2
                profit = self.position['open_amount'] * (now_price - self.position['open_price'])
                self.position['profit_unrealized'] = profit






