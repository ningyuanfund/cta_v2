import json
import os

import ccxt
import pandas as pd
from sqlalchemy import create_engine

pd.set_option('expand_frame_repr', False)

# 本地数据，一分钟级别为佳，程序中的sql基本上都得改一下
connect_info_lc = 'postgresql://postgres:tanghao56191328@localhost:5432/bitfinex_local'
engine_lc = create_engine(connect_info_lc, echo=False)

# sever 1T data
connect_info_sr = 'postgresql://postgres:holt@49.51.196.202:5432/bfx_data1T'
engine_sr = create_engine(connect_info_sr, echo=False)

# sever lspr
engine_emotion = create_engine('postgresql://postgres:thomas@119.28.89.58:5432/factor', echo=False)
engine_emotion_backup = create_engine('postgresql://postgres:holt@49.51.196.202:5432/factor', echo=False)


MAIN_ROOT = os.path.abspath(os.path.join(__file__, os.pardir))

ACCOUNT_ROOT = os.path.join(MAIN_ROOT, 'account.csv')

apiKey_1 = 'kaSx747CCm23VFvtp31P9i81tOK3STdHsr9ZJLjTbDP'
secret_1 = 'M7TgT6T4HvrwBjxMh1mTRMvtLWooQ7vdSO0CFjD6xu0'

exchange = ccxt.bitfinex({
    'apiKey': apiKey_1,
    'secret': secret_1
})


def dict_to_json(d, name, json_path):
    j_path = '%s\\%s.json' % (json_path, name)
    print('ACCOUNT PATH ', j_path)
    json_res = json.dumps(d)
    file_obj = open(j_path, 'w')
    file_obj.write(json_res)
    file_obj.close()


def json_to_dict(name, json_path):
    j_path = '%s/%s.json' % (json_path, name)
    with open(j_path, 'r') as load_f:
        t_dict = json.loads(load_f.read())
    return t_dict


if __name__ == '__main__':
    account_list = json_to_dict(name='test', json_path=r'E:\cta_v2\local_multi_account\AccountEnv')
    print(account_list)
    exit()
    dict_to_json(d=account_list, name='test', json_path=r'E:\cta_v2\local_multi_account\AccountEnv')
