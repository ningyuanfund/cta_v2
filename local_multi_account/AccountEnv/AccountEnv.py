from datetime import timedelta

import pandas as pd

from local_multi_account.AccountEnv.local_account import LocalAccount
from local_multi_account.AccountEnv.util import dict_to_json, json_to_dict


class AccountEnv:
    """
    负责本地account_num个账户的管理，交易，信息同步
    """
    def __init__(self, account_num, trader, fetcher, real_trade, first_run):

        self.json_name = 'local_account'
        self.json_path = r'E:\cta_v2\local_multi_account\AccountEnv'
        self._trader = trader
        self._fetcher = fetcher
        self.account_list = self.gen_local_account_list(account_num=account_num,
                                                        first_run=first_run,
                                                        real_trade=real_trade,
                                                        fetcher=fetcher,
                                                        trader=trader)
        self.record_to_json()

    def __repr__(self):
        for account in self.account_list:
            print(account)


    def gen_local_account_list(self, account_num, first_run, real_trade, fetcher, trader):
        """
        从本地记录中获取账户信息
        :param account_num:
        :param first_run:
        :param real_trade:
        :param fetcher:
        :param trader:
        :return:
        """
        account_list = []
        standard_account = {
            "account_id": "1",
            "available_cash": 100,  # 上次平仓后剩余的可用资金，每次平仓都需要考虑手续费，所以可用资金需要减0.5%
            "symbol_leverage": {
                                "ETHUSD": 2.5,
                                "BTCUSD": 2.5,
                                'XRPUSD': 2.5,
                                'ETCUSD': 2.5,
                                },
            "last_open_time": None,
            "latest_close_time": None,
            "last_rebalance_time": None,
            "now_position": {},
            "hist_order": []  # 记录最近10个订单
        }
        # 第一次运行，重新生成标准账户，输出到本地
        if first_run:
            print('FIRST RUN, GENERATING LOCAL ACCOUNT')
            for i in range(account_num):
                standard_account['account_id'] = str(i)
                new_account = LocalAccount(local_info=standard_account,
                                           fetcher=fetcher,
                                           trader=trader)
                account_list.append(new_account)

        # 不是第一次
        else:
            account_info_list = json_to_dict(name=self.json_name,
                                             json_path=self.json_path)
            for account_info in account_info_list:
                new_account = LocalAccount(local_info=account_info,
                                           fetcher=fetcher,
                                           trader=trader)
                account_list = new_account

        return account_list

    def update_local_json(self):
        """
        将当前的account信息记录到本地，需要频繁进行
        :return:
        """
        account_json_list = []

        for account in self.account_list:
            t_account = {}
            t_account['account_id'] = account.id
            t_account['available_cash'] = account.cash
            t_account['symbol_leverage'] = account.symbol_leverage
            t_account['last_open_time'] = account.last_open_time
            t_account['latest_close_time'] = account.latest_close_time
            t_account['last_rebalance_time'] = account.last_rebalance_time
            t_account['now_position'] = account.position
            t_account['hist_order'] = account.hist_order
            account_json_list.append(t_account.copy())
        dict_to_json(d=account_json_list, name=self.json_name, json_path=self.json_path)

    def check_rebalance(self):
        """
        检查是否有持仓，如果没有的话进行rebalance
        :return:
        """
        print('CHECK REBALANCE')
        empty = True
        for account in self.account_list:
            if account.position != {}:
                empty = False
                print('HOLDING POSITION')
                break
        # 无持仓，rebalance
        if empty:
            now_cash = self._fetcher.get_margin_balance()
            print('REABALNCE!!\nNOW CASH', now_cash)
            for i in range(len(self.account_list)):
                self.account_list[i].cash = float(now_cash) // (len(self.account_list))
                self.account_list[i].hist_order = self.account_list[i].hist_order[-10:]
                self.account_list[i].last_open_time = None
                self.account_list[i].latest_close_time = None
                self.account_list[i].last_rebalance_time = \
                    pd.to_datetime(self._fetcher.exchange.milliseconds(), unit='ms')

        # 有持仓，更新账户信息
        else:
            for i in range(len(self.account_list)):
                self.account_list[i].update()

    def record_to_json(self):
        """
        将account信息记录到本地的json文件
        :return:
        """
        account_json_list = []
        for account in self.account_list:

            account_json_list.append(
                {
                    'account_id': account.id,
                    'available_cash': account.cash,
                    'symbol_leverage': account.symbol_leverage,
                    'last_open_time': self._timevalue_to_str(account.last_open_time),
                    'latest_close_time': self._timevalue_to_str(account.latest_close_time),
                    'last_rebalance_time': self._timevalue_to_str(account.last_rebalance_time),
                    'now_position': account.position,
                    'hist_order': account.hist_order
                }
            )
        print('ACCOUNT TO LOCAL')
        print(account_json_list)
        dict_to_json(d=account_json_list, name=self.json_name, json_path=self.json_path)


    def prolong_latest_date(self, account_id, hours):
        """
        延长最迟平仓时间
        :param account_id:
        :param hours:
        :return:
        """
        for i in range(len(self.account_list)):
            if self.account_list[i].id == account_id:
                self.account_list[i].latest_close_time += timedelta(hours=hours)

    def _timevalue_to_str(self, value):
        if value != None:
            if type(value) == str:
                return value
            return value.strftime('%Y-%m-%d %H:%M:%S')
        else:
            return None

if __name__ == '__main__':
    i = 's'
    print(type(i) == str)