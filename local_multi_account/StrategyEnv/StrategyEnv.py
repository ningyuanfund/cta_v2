import pandas as pd

from local_multi_account.StrategyEnv.SignalGenerator import rf_forecast


class StrategyEnv:

    @staticmethod
    def position_formatter(strategy_sig, now_price, now_time):
        for tm in strategy_sig.keys():
            if pd.to_datetime(now_time) == pd.to_datetime(tm):
                now_pos = strategy_sig[tm]
                break

        signal = {'check_time': int(pd.to_datetime(now_time).strftime('%H')),
                   'should_open': now_pos['position'],
                   'stop_loss': now_price * (1 + now_pos['stop_loss'] / 100),
                   'stop_profit': now_price * (1 + now_pos['stop_profit'] / 100),
                   'open_price': now_price,
                   }

        return signal

    def position(self, data, symbol):
        now_price = data['close'].iloc[-1]
        now_time = data['candle_begin_time'].iloc[-1]
        while True:
            try:
                strategy_sig = rf_forecast(df_price=data, symbol=symbol.strip('USD'))
                break
            except Exception as e:
                print(e)

        print(strategy_sig)

        return self.position_formatter(strategy_sig=strategy_sig, now_price=now_price, now_time=now_time)

