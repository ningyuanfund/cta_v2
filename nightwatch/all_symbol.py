import ccxt
import time
import pandas as pd
pd.set_option('expand_frame_repr', False)

bitfinex = ccxt.bitfinex()
bitfinex2 = ccxt.bitfinex2()
symbol_list = bitfinex.public_get_symbols()
valid_list = [symbol.upper() for symbol in symbol_list if symbol.endswith('usd')]
print(valid_list)
exit()
volume_data = bitfinex2.public_get_tickers(params={'symbols': 'tBTCUSD'})
print(volume_data)
exit()

data = {}
for symbol in valid_list[:5]:
    print(symbol)
    try:
        volume_data = bitfinex.public_get_stats_symbol(params={'symbol': symbol})
        price_data = bitfinex.public_get_pubticker_symbol(params={'symbol': symbol})
        data[symbol] = {}
        data[symbol]['1d_vol'] = volume_data[0]['volume']
        data[symbol]['7d_vol'] = volume_data[1]['volume']
        data[symbol]['30d_vol'] = volume_data[2]['volume']
        data[symbol]['price'] = price_data['mid']
    except Exception as e:
        print(e)

    time.sleep(5)
df = pd.DataFrame(data).T
print(df)
