import ccxt
import pandas as pd
import time
from nightwatch.function import *
from th.cq_sdk import *
from nightwatch.bfx_k_line import *
import os
from sqlalchemy import create_engine

engine = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex')

pd.set_option('expand_frame_repr', False)

symbol_list = ['BTC/USD', 'ETH/USD', 'EOS/USD', 'LTC/USD', 'XRP/USD', 'BCH/USD']
time_interval = '5m'

while True:
    target_time = next_run_time('1D')
    # 每分钟更新持仓信息
    while True:
        if datetime.now() < target_time - timedelta(seconds=120):
            time.sleep(60)
            continue
        else:
            break

    # 直到下次运行的时间再启动
    time.sleep(max(0, (target_time - datetime.now()).seconds))

    now_time = pd.to_datetime(exchange.milliseconds(), unit='ms')
    yesterday = now_time - timedelta(days=1)
    start_time_str = yesterday.strftime('%Y-%m-%d 00:00:00')
    end_time_str = now_time.strftime('%Y-%m-%d 00:00:00')
    print(start_time_str)

    start_timestamp = int(time.mktime(pd.to_datetime(start_time_str).timetuple()) * 1000)
    end_timestamp = int(time.mktime(pd.to_datetime(start_time_str).timetuple()) * 1000)
    last_date = {}
    for symbol in symbol_list:

        df = get_k_line(start_timestamp, end_timestamp, symbol, time_interval)
        df['date'] = df['candle_begin_time'].dt.date

        condition = df['candle_begin_time'] < pd.to_datetime(now_time.strftime('%Y-%m-%d 00:00:00'))
        df = df[condition][['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].reset_index(drop=True)
        df.drop_duplicates(inplace=True)
        df.reset_index(drop=True, inplace=True)
        df.to_csv(get_data_path('bfx_data', symbol,
                                (pd.to_datetime(time.time(), unit='s') - timedelta(days=1)).strftime(
                                    "%Y-%m-%d") + '.csv'))

        table = 'bfx_data_%s_%s' % (symbol, start_time_str[:-8])
        try:
            df.to_sql(table, engine, index=False, if_exists='replace')
        except Exception as e:
            print(e)



