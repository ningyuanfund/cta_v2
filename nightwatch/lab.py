import os
import sys
import time
import pandas as pd
from datetime import datetime, timedelta
import ccxt  # noqa: E402
from sqlalchemy import create_engine

# -----------------------------------------------------------------------------
# common constants

msec = 1000
minute = 60 * msec
hold = 30

# -----------------------------------------------------------------------------

exchange = ccxt.bitfinex({
    'rateLimit': 10000,
    'enableRateLimit': True,
    # 'verbose': True,
})
# -----------------------------------------------------------------------------


def get_k_line(start_stamp, end_stamp, symbol, time_interval):
    '''

    :param start_datetime:2019-02-01 00:00:00
    :param symbol: 'ETH/USD'
    :param time_interval:1h
    :return:
    '''

    # from_datetime = start_stamp
    # from_timestamp = exchange.parse8601(from_datetime)
    # print(from_timestamp)
    # now = exchange.milliseconds()
    df = pd.DataFrame()
    while start_stamp < end_stamp:

        try:
            print(time_interval)

            print(exchange.milliseconds(), 'Fetching %s candles starting from' % symbol, exchange.iso8601(start_stamp))
            ohlcvs = exchange.fetch_ohlcv(symbol, time_interval, start_stamp)
            if time_interval.endswith('m'):
                start_stamp += len(ohlcvs) * minute * int(time_interval.strip('m'))
            if time_interval.endswith('h'):
                start_stamp += len(ohlcvs) * minute * int(time_interval.strip('h')) * 60

            ohlcvs = pd.DataFrame(ohlcvs)
            ohlcvs.columns = ['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']
            ohlcvs['candle_begin_time'] = pd.to_datetime(ohlcvs['candle_begin_time'], unit='ms')
            df = df.append(ohlcvs)
            time.sleep(5)

        except (ccxt.ExchangeError, ccxt.AuthenticationError, ccxt.ExchangeNotAvailable, ccxt.RequestTimeout) as error:

            print('Got an error', type(error).__name__, error.args, ', retrying in', hold, 'seconds...')
            time.sleep(hold)

    # df['candle_begin_time_GTM8'] = df['candle_begin_time'] + timedelta(hours=8)

    return df


if __name__ == '__main__':
    engine = create_engine('postgresql://postgres:thomas@localhost:5432/bfx_alldata')
    for symbol in ['btcusd', 'ltcusd', 'ethusd', 'etcusd', 'rrtusd', 'zecusd', 'xmrusd', 'dshusd', 'xrpusd', 'iotusd',
                   'eosusd', 'sanusd', 'omgusd', 'neousd', 'etpusd', 'qtmusd', 'avtusd', 'edousd', 'btgusd', 'datusd',
                   'qshusd', 'yywusd', 'gntusd', 'sntusd', 'batusd', 'mnausd', 'funusd', 'zrxusd', 'tnbusd', 'spkusd',
                   'trxusd', 'rcnusd', 'rlcusd', 'aidusd', 'sngusd', 'repusd', 'elfusd', 'iosusd', 'aiousd', 'requsd',
                   'rdnusd', 'lrcusd', 'waxusd', 'daiusd', 'agiusd', 'bftusd', 'mtnusd', 'odeusd', 'antusd', 'dthusd',
                   'mitusd', 'stjusd', 'xlmusd', 'xvgusd', 'bciusd', 'mkrusd', 'kncusd', 'poausd', 'lymusd', 'utkusd',
                   'veeusd', 'dadusd', 'orsusd', 'aucusd', 'poyusd', 'fsnusd', 'cbtusd', 'zcnusd', 'senusd', 'ncausd',
                   'cndusd', 'ctxusd', 'paiusd', 'seeusd', 'essusd', 'atmusd', 'hotusd', 'dtausd', 'iqxusd', 'wprusd',
                   'zilusd', 'bntusd', 'absusd', 'xrausd', 'manusd', 'bbnusd', 'niousd', 'dgxusd', 'vetusd', 'utnusd',
                   'tknusd', 'gotusd', 'xtzusd', 'cnnusd', 'boxusd', 'mgousd', 'rteusd', 'yggusd', 'mlnusd', 'wtcusd',
                   'csxusd', 'omnusd', 'intusd', 'drnusd', 'pnkusd', 'dgbusd', 'bsvusd', 'babusd', 'wlousd', 'vldusd',
                   'enjusd', 'onlusd', 'rbtusd', 'ustusd', 'eutusd', 'gsdusd', 'udcusd', 'tsdusd', 'paxusd', 'rifusd',
                   'pasusd', 'vsyusd', 'bttusd', 'clousd', 'impusd', 'scrusd', 'gnousd', 'genusd', 'atousd', 'wbtusd',
                   'xchusd', 'eususd', 'leousd', 'astusd', 'foausd', 'ufrusd', 'zbtusd']:
        symbol = symbol.upper()[:3] + '/' + symbol.upper()[3:]
        start_datetime = pd.to_datetime('2017-01-01 00:00:00') + timedelta(hours=8)
        end_datetime = start_datetime + timedelta(days=1)

        while end_datetime <= pd.to_datetime('2019-06-06 00:00:00') + timedelta(hours=8):
            start_timestamp = int(time.mktime(start_datetime.timetuple()) * 1000)
            end_timestamp = int(time.mktime(end_datetime.timetuple()) * 1000)

            df = get_k_line(start_timestamp, end_timestamp, symbol, '5m')
            df = df.drop_duplicates('candle_begin_time')
            df = df.sort_values(by='candle_begin_time')
            df.reset_index(inplace=True, drop=True)
            sql_df = df[df['candle_begin_time'] < end_datetime - timedelta(hours=8)].copy()

            table = 'bfx_data_%s_%s' % (symbol, start_datetime.strftime('%Y-%m-%d'))
            print(sql_df)
            # sql_df.to_sql(table, engine, index=False, if_exists='replace')

            print(start_datetime.strftime('%Y-%m-%d'), '============== DONE')
            start_datetime += timedelta(days=1)
            end_datetime += timedelta(days=1)

