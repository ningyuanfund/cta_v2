from pyecharts.components import Table
from pyecharts.options import ComponentTitleOpts

from nightwatch.function import *

engine = create_engine('postgresql://postgres:thomas@localhost:5432/factor')
mail_list = ['693216580@qq.com']


okex = ccxt.okex()
bitfinex = ccxt.bitfinex()
okex_symbol_list = ['BTC/USD', 'ETH/USD', 'EOS/USD', 'LTC/USD', 'XRP/USD']
time_interval = '5m'


# ===下次运行的时间
def next_factor_run_time(time_interval, ahead_time=1):
    """
        :param time_interval 运行周期时间 建议不要小于5min
        :param ahead_time 当小于1s时 自动进入下个周期
        :return:
        """
    if time_interval.endswith('m'):
        now_time = datetime.now()
        time_interval = int(time_interval.strip('m'))

        target_min = (int(now_time.minute / time_interval) + 1) * time_interval
        if target_min < 60:
            target_time = now_time.replace(minute=target_min, second=0, microsecond=0)
        else:
            if now_time.hour == 23:
                target_time = now_time.replace(hour=0, minute=0, second=0, microsecond=0)
                target_time += timedelta(days=1)
            else:
                target_time = now_time.replace(hour=now_time.hour + 1, minute=0, second=0, microsecond=0)

        # sleep直到靠近目标时间之前
        if (target_time - datetime.now()).seconds < ahead_time + 1:
            print('距离target_time不足', ahead_time, '秒，下下个周期再运行')
            target_time += timedelta(minutes=time_interval)
        print('下次运行时间', target_time)
        return target_time

    elif time_interval.endswith('D'):
        next_day_time = datetime.now() + timedelta(days=1)
        target_time = pd.to_datetime('%s-%s-%s 13:20:00' % (next_day_time.year, next_day_time.month, next_day_time.day))
        # target_time = pd.to_datetime('2019-03-09 13:45:00')
        print('下次运行时间', target_time)
        return target_time


# ===画图
def draw_html(df, start_time_str):
    col_list = ['指标', 'BTCUSD', 'ETHUSD', 'EOSUSD', 'LTCUSD', 'XRPUSD']
    df.columns = col_list
    headers = col_list
    rows = []
    for i in range(len(df)):
        t_row = []
        for col in col_list:
            cond1 = df[col].iloc[i].startswith('a季度')
            cond2 = df[col].iloc[i].startswith('b次周')
            cond3 = df[col].iloc[i].startswith('c当周')
            if cond1 or cond2 or cond3:
                t_row.append(df[col].iloc[i][1:])
            else:
                t_row.append(df[col].iloc[i])
        rows.append(t_row)
    table = \
    (
        Table()
        .add(headers, rows).set_global_opts(title_opts=ComponentTitleOpts(title=start_time_str)
                                            )
    )
    table.render(r'C:\Users\Administrator\Desktop\KLJTable\table.html')

# t_engine = create_engine('postgresql://postgres:thomas@119.28.89.58:5432/factor')
# sql = 'select * from "2019-08-23"'
# d = pd.read_sql_query(sql, con=t_engine)
# draw_html(d)
# print(d)
# exit()


while True:
    target_time = next_factor_run_time('1D')
    while True:
        if datetime.now() < target_time - timedelta(seconds=120):
            time.sleep(60)
            continue
        else:
            break

    # 直到下次运行的时间再启动
    time.sleep(max(0, (target_time - datetime.now()).seconds))

    okex_gap_df = cal_okex_contract_gap(okex, okex_symbol_list)
    bitfinex_vm_df = cal_bitfinex_volume_momentum(bitfinex, okex_symbol_list)
    df_sum = pd.concat([okex_gap_df, bitfinex_vm_df])
    df_sum.reset_index(inplace=True)

    now_time = pd.to_datetime(bitfinex.milliseconds(), unit='ms')
    start_time_str = now_time.strftime('%Y-%m-%d')
    print('now time', start_time_str)
    print(df_sum)
    draw_html(df_sum, start_time_str)

    try:
        df_sum.to_sql(start_time_str, engine, index=False, if_exists='replace')
    except Exception as e:
        print(e)
