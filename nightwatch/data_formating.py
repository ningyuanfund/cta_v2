from sqlalchemy import create_engine
import pandas as pd
pd.set_option('expand_frame_repr', False)

# engine = create_engine('postgresql://postgres:thomas@localhost/bitfinex')
engine = create_engine('postgresql://postgres:thomas@119.28.89.58:5432/bitfinex')

pd.set_option('expand_frame_repr', False)

sql = "select tablename from pg_tables where schemaname='public'"

table_df = pd.read_sql(sql, con=engine)
table_df['symbol'] = table_df['tablename'].str[9:16]


def sort_df(temp):
    df_list = []
    for i in range(len(temp)):
        table_name = temp['tablename'].at[i]
        print(table_name)
        sql_cmd = 'select * from "%s"' % table_name
        df = pd.read_sql(sql_cmd, con=engine)
        df_list.append(df)

    df_summed = pd.concat(df_list)
    return df_summed


symbol_list = ['BTC/USD', 'ETH/USD', 'EOS/USD', 'LTC/USD', 'XRP/USD']
# symbol_list = ['ETH/USD']


for symbol in symbol_list:
    print('================%s=================' % symbol)
    df_temp = table_df[table_df['symbol'] == symbol].copy()
    df_temp.reset_index(inplace=True, drop=True)
    df_sum = sort_df(df_temp)
    df_sum.drop_duplicates(inplace=True)
    df_sum.sort_values(by='candle_begin_time')
    df_sum.reset_index(drop=True, inplace=True)

    time_interval_list = ['15T', '30T', '1H']

    df_ori = df_sum.copy()
    df_ori.reset_index(inplace=True, drop=True)
    df_ori['candle_begin_time'] = pd.to_datetime(df_ori['candle_begin_time'])
    for time_interval in time_interval_list:
        period_df = df_ori.resample(rule=time_interval, on='candle_begin_time', base=0, label='left',
                                    closed='left').agg(
            {'open': 'first',
             'high': 'max',
             'low': 'min',
             'close': 'last',
             'volume': 'sum',
             })
        period_df.dropna(subset=['open'], inplace=True)
        period_df = period_df[period_df['volume'] > 0]

        period_df = period_df[['open', 'high', 'low', 'close', 'volume']]
        period_df.reset_index(inplace=True)
        # period_df.to_hdf(r'C:\Users\Administrator\Desktop\%s.h5' % symbol.replace('/', ''), key=time_interval)
        # period_df.to_csv(r'C:\Users\Administrator\Desktop\%s_%s.csv' % (symbol.replace('/', ''), time_interval))
        period_df.to_csv(r'D:\Work\cta_v2\fifty-five-data\input\%s_%s.csv' % (symbol.replace('/', ''), time_interval))

