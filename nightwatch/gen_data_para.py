from datetime import datetime, timedelta
import pandas as pd


def gen_data_para(symbol, start_tm, end_tm):
    dt_list = []
    next_day = pd.to_datetime(start_tm)

    # cur_month 不一定就是当前所在月份。可能现在处在月底，那么cur_month 就应该是下个月
    cur_date = int(start_tm.split('-')[2])
    cur_month = int(start_tm.split('-')[1])
    cur_yr = int(start_tm.split('-')[0])

    # 先判断当月合约的月份是多少
    fri_count = 0
    for i in range(1, cur_date + 1):
        now_date = pd.to_datetime('%s-%s-%s' % (cur_yr, cur_month, i))
        if now_date.weekday() == 5:
            fri_count += 1
    if fri_count >= 3:
        # 如果周五超过了三次，当月合约的月份就要+1
        if cur_month < 12:
            cur_month += 1
        else:
            cur_month = 1
            cur_yr += 1

    while next_day <= pd.to_datetime(end_tm):

        now_date = next_day.strftime('%Y-%m-%d').split('-')

        day_of_week = datetime.strptime(next_day.strftime('%Y-%m-%d'), '%Y-%m-%d').weekday()
        if day_of_week == 5:
            # 考虑每个月的第三个周五交割当月合约
            if (int(now_date[2]) > 15) & (int(now_date[2]) < 23):
                if cur_month < 12:
                    cur_month += 1
                else:
                    cur_month = 1

        # 考虑换季和换年的情况，调整下月
        next_month = cur_month + 1 if cur_month < 12 else 1

        # 考虑换季和换年的情况，调整下季月和下下季月
        # 如果当前是季月
        if cur_month % 3 == 0:
            if cur_month == 6:
                next_season = cur_month + 3
                next_next_season = next_season + 3
            elif cur_month == 9:
                next_season = cur_month + 3
                next_next_season = 3
            else:
                next_season = 3
                next_next_season = 6
        # 如果当前不是季月
        else:
            next_season = (cur_month // 3) * 3 + 3
            next_next_season = next_season + 3 if next_season < 12 else 3

        # 考虑换年的情况，调整下月和下季月的年份
        cur_symbol = '%s%s%s' % (symbol,
                                 str(cur_yr)[-2:],
                                 str(cur_month) if cur_month >= 10 else '0' + str(cur_month))
        if int(now_date[1]) == 12:

            next_symbol = '%s%s%s' % (symbol,
                                      str(int(now_date[0]) + 1)[-2:],
                                      str(next_month) if next_month >= 10 else '0' + str(next_month))
            next_season_symbol = '%s%s%s' % (symbol,
                                             str(int(now_date[0]) + 1)[-2:],
                                             str(next_season) if next_season >= 10 else '0' + str(next_season))

        else:
            next_symbol = '%s%s%s' % (symbol,
                                      now_date[0][-2:],
                                      str(next_month) if next_month >= 10 else '0' + str(next_month))
            next_season_symbol = '%s%s%s' % (symbol,
                                             now_date[0][-2:],
                                             str(next_season) if next_season >= 10 else '0' + str(next_season))

        # 考虑换年的情况，调整下下季月的年份
        if int(now_date[1]) >= 9:
            next_next_season_symbol = '%s%s%s' % (symbol,
                                                  str(int(now_date[0]) + 1)[-2:],
                                                  str(next_next_season) if next_next_season >= 10 else '0' + str(next_next_season))
        else:
            next_next_season_symbol = '%s%s%s' % (symbol,
                                                  now_date[0][-2:],
                                                  str(next_next_season) if next_next_season >= 10 else '0' + str(next_next_season))

        dt_list.append({'date': now_date,
                        'symbol': list(set([cur_symbol, next_symbol, next_season_symbol, next_next_season_symbol]))})
        next_day += timedelta(days=1)

    return dt_list


if __name__ == '__main__':
    symbol = 'IF'
    start_tm = '2017-06-30'
    end_tm = '2019-02-01'
    da = gen_data_para(symbol, start_tm, end_tm)
    print(da)
