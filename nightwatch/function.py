import json
import requests
import threading
from nightwatch.bfx_k_line import *
from ckt.Signals import *


# ===下次运行的时间
def next_run_time(time_interval, ahead_time=1):
    """
        :param time_interval 运行周期时间 建议不要小于5min
        :param ahead_time 当小于1s时 自动进入下个周期
        :return:
        """
    if time_interval.endswith('m'):
        now_time = datetime.now()
        time_interval = int(time_interval.strip('m'))

        target_min = (int(now_time.minute / time_interval) + 1) * time_interval
        if target_min < 60:
            target_time = now_time.replace(minute=target_min, second=0, microsecond=0)
        else:
            if now_time.hour == 23:
                target_time = now_time.replace(hour=0, minute=0, second=0, microsecond=0)
                target_time += timedelta(days=1)
            else:
                target_time = now_time.replace(hour=now_time.hour + 1, minute=0, second=0, microsecond=0)

        # sleep直到靠近目标时间之前
        if (target_time - datetime.now()).seconds < ahead_time + 1:
            print('距离target_time不足', ahead_time, '秒，下下个周期再运行')
            target_time += timedelta(minutes=time_interval)
        print('下次运行时间', target_time)
        return target_time

    elif time_interval.endswith('D'):
        next_day_time = datetime.now() + timedelta(days=1)
        target_time = pd.to_datetime('%s-%s-%s 00:09:00' % (next_day_time.year, next_day_time.month, next_day_time.day))
        # target_time = pd.to_datetime('2019-03-09 13:45:00')
        print('下次运行时间', target_time)
        return target_time


# ===发送钉钉消息，id填上使用的机器人的id
def send_dingding_msg(content, robot_id='f8a6e4a45b76883ea53ffbe2c2a89ae26ce9feb1adaa267172ec882dd027716c'):
    try:
        msg = {
            "msgtype": "text",
            "text": {"content": content + '\n' + datetime.now().strftime("%m-%d %H:%M:%S")}}
        headers = {"Content-Type": "application/json;charset=utf-8"}
        url = 'https://oapi.dingtalk.com/robot/send?access_token=' + robot_id
        body = json.dumps(msg)
        requests.post(url, data=body, headers=headers)
        print('成功发送钉钉')
    except Exception as e:
        print("发送钉钉失败:", e)


# ===多线程获取OKEX当前价格数据
def get_order_book_data(exchange, symbol):

    # ===多线程
    _orders = {}

    def create_get_order_book_multi_thread(_exchange, _symbol, _contract_type):
        if _contract_type in _orders:
            del _orders[_contract_type]

        _orders[_contract_type] = exchange.fetch_order_book(symbol=_symbol,
                                                            limit=1,
                                                            params={'contract_type': _contract_type})

    # 获取当周合约数据
    this_week_thr = threading.Thread(target=create_get_order_book_multi_thread,
                                     args=[exchange, symbol, 'this_week'])
    # 获取次周合约数据
    next_week_thr = threading.Thread(target=create_get_order_book_multi_thread,
                                     args=[exchange, symbol, 'next_week'])
    # 获取季度合约数据
    quarter_thr = threading.Thread(target=create_get_order_book_multi_thread,
                                   args=[exchange, symbol, 'quarter'])

    # start
    this_week_thr.start()
    next_week_thr.start()
    quarter_thr.start()
    # wait
    this_week_thr.join()
    next_week_thr.join()
    quarter_thr.join()

    this_week_info = _orders.get('this_week', None)
    next_week_info = _orders.get('next_week', None)
    quarter_info = _orders.get('quarter', None)

    return this_week_info, next_week_info, quarter_info


# ===整理OKEX合约价差数据
def cal_okex_contract_gap(okex, okex_symbol_list):
    okex_gap_dict = {}
    for symbol in okex_symbol_list:
        # 获取现货价格
        spot_bid_price = okex.fetch_ticker(symbol=symbol + 'T')['bid']

        okex_gap_dict[symbol] = {}
        this_week_info, next_week_info, quarter_info = get_order_book_data(okex, symbol)
        okex_gap_dict[symbol]['a季度-次周'] = "%.4f" % ((quarter_info['bids'][0][0] - next_week_info['bids'][0][0]) /
                                                    next_week_info['bids'][0][0] * 100) + '%'
        okex_gap_dict[symbol]['b次周-当周'] = "%.4f" % ((next_week_info['bids'][0][0] - this_week_info['bids'][0][0]) /
                                                    this_week_info['bids'][0][0] * 100) + '%'
        okex_gap_dict[symbol]['c当周-现货'] = "%.4f" % ((this_week_info['bids'][0][0] - spot_bid_price) /
                                                    spot_bid_price) + '%'

    return pd.DataFrame(okex_gap_dict)


# ===计算bitfinex交易量/过去20日平均交易量
def cal_bitfinex_volume_momentum(bitfinex, bitfinex_symbol_list):
    vm_dict = {}
    now_time = bitfinex.milliseconds()
    from_timestamp = now_time - 22 * 24 * 60 * 60 * 1000  # 22天
    for symbol in bitfinex_symbol_list:
        ohlcvs = bitfinex.fetch_ohlcv(symbol, '1d', from_timestamp)

        ohlcvs = pd.DataFrame(ohlcvs)
        ohlcvs.columns = ['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']
        ohlcvs['candle_begin_time'] = pd.to_datetime(ohlcvs['candle_begin_time'], unit='ms')
        print(ohlcvs)
        avg_20 = (ohlcvs['volume'].sum() - ohlcvs['volume'].iloc[-1] - ohlcvs['volume'].iloc[-2]) / 20
        vm_dict[symbol] = {}
        vm_dict[symbol]['bfx成交量/20日均成交量'] = "%.4f" % (ohlcvs['volume'].iloc[-2] / avg_20 * 100) + '%'
        vm_dict[symbol]['1日收益率'] = "%.4f" % (ohlcvs['close'].iloc[-2] / ohlcvs['close'].iloc[-3] * 100 - 100) + '%'
        vm_dict[symbol]['2日收益率'] = "%.4f" % (ohlcvs['close'].iloc[-2] / ohlcvs['close'].iloc[-4] * 100 - 100) + '%'
        vm_dict[symbol]['5日收益率'] = "%.4f" % (ohlcvs['close'].iloc[-2] / ohlcvs['close'].iloc[-7] * 100 - 100) + '%'

    return pd.DataFrame(vm_dict)


if __name__ == '__main__':
    bitfinex = ccxt.bitfinex2()
    bitfinex_symbol_list = ['BTC/USD', 'ETH/USD', 'EOS/USD', 'LTC/USD', 'XRP/USD']
    df = cal_bitfinex_volume_momentum(bitfinex, bitfinex_symbol_list)
    print(df)