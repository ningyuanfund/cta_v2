from contextlib import closing
from tqsdk import TqApi, TqSim
from tqsdk.tools import DataDownloader


def get_data(symbol, start_dt, end_dt, temp_path):

    api = TqApi(TqSim())

    # 下载从 2018-05-01凌晨0点 到 2018-07-01凌晨0点 的 T1809 盘口Tick数据
    td = DataDownloader(api, symbol_list=symbol, dur_sec=0,
                        start_dt=start_dt, end_dt=end_dt, csv_file_name=temp_path)

    # 使用with closing机制确保下载完成后释放对应的资源
    with closing(api):
        while not td.is_finished():
            api.wait_update()
            print("progress: tick:%.2f%%" % (td.get_progress()))
