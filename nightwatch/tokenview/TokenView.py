import json
import time
from functools import wraps
from urllib.request import urlopen, Request

import pandas as pd
from sqlalchemy import create_engine

eg = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')

pd.set_option('expand_frame_repr', False)


def try_wrapper(func):
    @wraps(func)
    def decorated(*args, **kwargs):
        i = 6
        while i > 0:
            try:
                return func(*args, **kwargs)
            except Exception as e:
                print(e)
                i -= 1
    return decorated

class TokenView:
    def __init__(self):
        self._base = 'https://metrics.tokenview.com'
        self._v2api_sec = 'v2api/chart/metrics'
        self._cmc = 'echarts/coinmarketcap'
        self._tokengazer = 'echarts/tokengazer'
        self._onchain_sec = 'echarts/onchain'
        self._bare_echart = 'echarts'

    @try_wrapper
    def _req(self, sec, field, params):
        url = '%s/%s/%s/%s' % (self._base, sec, field, params)
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0'}
        request = Request(url=url, headers=headers)
        content = urlopen(request, timeout=15).read()
        content = content.decode('utf-8')
        json_data = json.loads(content)
        return json_data

    def _str_to_timestamp(self, s_time):
        return int(time.mktime(time.strptime(s_time, '%Y-%m-%d')))

    def _params(self, *arg):
        params = '%s'
        params += ('/%s' * (len(arg) - 1))
        return params % arg

    def _format_1(self, info, *arg):
        df_list = []
        for d in info['data']:

            if type(list(d.values())[0]) == dict:
                df_list.append([list(d.keys())[0]] + list(list(d.values())[0].values()))
            else:
                df_list.append([list(d.keys())[0]] + list(list(d.values())))

        return pd.DataFrame(df_list, columns=['date'] + [i for i in arg])

    def _format_2(self, info):
        return pd.DataFrame(info['data'])

    def _tokengazer_score(self, symbol, begin_time, end_time):
        """
        tokengazer评分
        :param symbol:
        :param begin_time:
        :param end_time:
        :return:
        """
        _res = self._req(sec=self._tokengazer,
                         field='gazerscore',
                         params=self._params(symbol,
                                             self._str_to_timestamp(s_time=begin_time),
                                             self._str_to_timestamp(s_time=end_time)))
        return self._format_2(info=_res)

    def _tokengazer_value(self, symbol, begin_time, end_time):
        """
        tokengazer估值
        :param symbol:
        :param begin_time:
        :param end_time:
        :return:
        """
        _res = self._req(sec=self._tokengazer,
                         field='commentpoints',
                         params=self._params(symbol,
                                             self._str_to_timestamp(s_time=begin_time),
                                             self._str_to_timestamp(s_time=end_time)))
        return self._format_2(info=_res)

    def nvt(self, symbol, begin_time, end_time):
        """
        NVT
        :param symbol:
        :param begin_time:
        :param end_time:
        :return:
        """
        _res = self._req(sec=self._onchain_sec,
                         field='nvt',
                         params=self._params(symbol,
                                             self._str_to_timestamp(s_time=begin_time),
                                             self._str_to_timestamp(s_time=end_time)))
        return self._format_1(_res, 'nvt')

    def pmr(self, symbol, begin_time, end_time):
        """
        梅特卡夫估值模型
        :param symbol:
        :param begin_time:
        :param end_time:
        :return:
        """
        _res = self._req(sec=self._onchain_sec,
                         field='pmr',
                         params=self._params(symbol,
                                             self._str_to_timestamp(s_time=begin_time),
                                             self._str_to_timestamp(s_time=end_time)))
        return self._format_1(_res, 'pmr')

    def active_address(self, symbol, begin_time, end_time):
        """
        日活跃地址数量
        :param symbol:
        :param begin_time:
        :param end_time:
        :return:
        """
        _res = self._req(sec=self._onchain_sec,
                         field='daily_active_address',
                         params=self._params(symbol,
                                             self._str_to_timestamp(s_time=begin_time),
                                             self._str_to_timestamp(s_time=end_time)))
        return self._format_1(_res, 'active_address')

    def new_address(self, symbol, begin_time, end_time):
        """
        日新增地址数量
        :param symbol:
        :param begin_time:
        :param end_time:
        :return:
        """
        _res = self._req(sec=self._onchain_sec,
                         field='daily_new_address',
                         params=self._params(symbol,
                                             self._str_to_timestamp(s_time=begin_time),
                                             self._str_to_timestamp(s_time=end_time)))
        return self._format_1(_res, 'new_address')

    def _price(self, symbol, begin_time, end_time):
        """
        价格
        :param symbol:
        :param begin_time:
        :param end_time:
        :return:
        """

        _res = self._req(sec=self._onchain_sec,
                         field='daily_price',
                         params=self._params(symbol,
                                             self._str_to_timestamp(s_time=begin_time),
                                             self._str_to_timestamp(s_time=end_time)))
        return self._format_1(_res, 'price')

    def block_fee(self, symbol, begin_time, end_time):
        """
        每日平均每个区块的费用
        :param symbol:
        :param begin_time:
        :param end_time:
        :return:
        """
        _res = self._req(sec=self._onchain_sec,
                         field='daily_avg_block_fee',
                         params=self._params(symbol,
                                             self._str_to_timestamp(s_time=begin_time),
                                             self._str_to_timestamp(s_time=end_time)))
        return self._format_1(_res, 'block_fee')

    def tx_cnt(self, symbol, begin_time, end_time):
        """
        每日链上交易数量
        :param symbol:
        :param begin_time:
        :param end_time:
        :return:
        """
        _res = self._req(sec=self._onchain_sec,
                         field='daily_tx_cnt',
                         params=self._params(symbol,
                                             self._str_to_timestamp(s_time=begin_time),
                                             self._str_to_timestamp(s_time=end_time)))
        return self._format_1(_res, 'tx_cnt')

    def tx_volume(self, symbol, begin_time, end_time):
        """
        每日链上交易额
        :param symbol:
        :param begin_time:
        :param end_time:
        :return:
        """
        _res = self._req(sec=self._onchain_sec,
                         field='daily_sent_value',
                         params=self._params(symbol,
                                             self._str_to_timestamp(s_time=begin_time),
                                             self._str_to_timestamp(s_time=end_time)))
        return self._format_1(_res, 'tx_volume')

    def hashrate(self, symbol, begin_time, end_time):
        """
        每日平均算力
        :param symbol:
        :param begin_time:
        :param end_time:
        :return:
        """
        _res = self._req(sec=self._onchain_sec,
                         field='daily_avg_hashrate',
                         params=self._params(symbol,
                                             self._str_to_timestamp(s_time=begin_time),
                                             self._str_to_timestamp(s_time=end_time)))
        df = self._format_1(_res, 'hashrate')
        df['hashrate'].fillna(method='ffill', inplace=True)
        return df

    def large_tx(self, symbol, begin_time, end_time):
        """
        大额转账
        :param symbol:
        :param begin_time:
        :param end_time:
        :return:
        """
        _res = self._req(sec=self._bare_echart,
                         field='largetrans',
                         params=self._params(symbol,
                                             self._str_to_timestamp(s_time=begin_time),
                                             self._str_to_timestamp(s_time=end_time)))
        df = self._format_1(_res, 'amount', 'count').copy()
        df[['amount', 'count']] = df[['amount', 'count']].fillna(value=0)
        return df

    def _cmc_data(self, symbol, begin_time, end_time):
        """
        coinmarketcap价格、市值、成交量
        :param symbol:
        :param begin_time:
        :param end_time:
        :return:
        """
        _res = self._req(sec=self._cmc,
                         field='',
                         params=self._params(symbol,
                                             self._str_to_timestamp(s_time=begin_time),
                                             self._str_to_timestamp(s_time=end_time)))

        df = self._format_2(_res)
        df.rename(columns={'date_str': 'date'}, inplace=True)
        return df


if __name__ == '__main__':


    tv = TokenView()
    info = tv.cmc_data(symbol='btc', begin_time='2017-01-01', end_time='2019-09-30')
    print(info)
    exit()
    method = [i for i in dir(tv) if not i.startswith('_')]
    df_res = pd.DataFrame()
    for me in method:
        print(me)
        func_str = 'tv.%s(symbol=\'btc\', begin_time=\'2017-01-01\', end_time=\'2019-09-29\')' % me
        df = eval(func_str)
        df['date'] = pd.to_datetime(df['date'])
        if len(df_res) == 0:
            df_res = df
        else:
            df_res = df_res.merge(df, on='date', how='outer')

