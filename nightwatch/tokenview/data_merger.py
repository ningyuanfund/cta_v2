import os
from datetime import timedelta

import pandas as pd
from sqlalchemy import create_engine

from nightwatch.tokenview.TokenView import TokenView

pd.set_option('expand_frame_repr', False)


# ======fundamental
tv = TokenView()
method = [i for i in dir(tv) if not i.startswith('_')]
df_res = pd.DataFrame()
for me in method:
    print(me)
    func_str = 'tv.%s(symbol=\'btc\', begin_time=\'2017-01-01\', end_time=\'2019-09-29\')' % me
    df = eval(func_str)
    df['date'] = pd.to_datetime(df['date'])
    if len(df_res) == 0:
        df_res = df
    else:
        df_res = df_res.merge(df, on='date', how='outer')
fundamental =df_res.copy()
# =========

# =======price
local_engine = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex_local')
sql = 'select * from "cta_test"."BTCUSD_long"'
d_price = pd.read_sql(sql, con=local_engine)
d_price = d_price[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()
d_price['candle_begin_time'] = pd.to_datetime(d_price['candle_begin_time'])
d_price = d_price.resample(on='candle_begin_time', rule='1d').agg(
    {
        'open': 'first',
        'high': 'max',
        'low': 'min',
        'close': 'last',
        'volume': 'sum'
    }
)
d_price.reset_index(inplace=True)
d_price.rename(columns={'candle_begin_time': 'date'}, inplace=True)
d_price['date'] += timedelta(days=1)
# =========
sum_data = fundamental.merge(d_price, on='date', how='inner')


# =======block
block_list = []
for root, dirs, files in os.walk(r'E:\cta_v2\nightwatch\btccom\parsed_block'):
    if files:
        for f in files:
            block_list.append(pd.read_csv('%s/%s' % (root, f)))
block_data = pd.concat(block_list)
block_data['timestamp'] = pd.to_datetime(block_data['timestamp'], unit='s')
block_data.sort_values('height', inplace=True)
block_data.reset_index(inplace=True, drop=True)
block_data['blk_count'] = 1
block_data['difficulty_max'] = block_data['difficulty']
block_data['difficulty_min'] = block_data['difficulty']
block_data['difficulty_mean'] = block_data['difficulty']


block_data = block_data[['timestamp', 'difficulty_mean', 'difficulty_min', 'difficulty_max', 'height', 'pool_difficulty', 'reward_block', 'reward_fees',
                         'tx_count', 'blk_count']].copy()

block_data = block_data.resample(on='timestamp', rule='1d').agg(
    {
        'difficulty_mean': 'mean',
        'difficulty_max': 'max',
        'difficulty_min': 'min',
        'height': 'last',
        'pool_difficulty': 'mean',
        'reward_block': 'sum',
        'reward_fees': 'sum',
        'tx_count': 'sum',
        'blk_count': 'sum'
    }
)
block_data.reset_index(inplace=True)
block_data.rename(columns={'timestamp': 'date'}, inplace=True)
block_data.to_csv('block_data.csv')
# =========
sum_data = sum_data.merge(block_data, on='date', how='outer')
sum_data.sort_values(by='date', inplace=True)
sum_data.reset_index(inplace=True, drop=True)
sum_data.set_index('date', inplace=True)
print(sum_data)

sum_data.to_csv('funda_price_block.csv')


