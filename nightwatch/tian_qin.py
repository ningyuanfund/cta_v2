
from sqlalchemy import create_engine
import pandas as pd
from datetime import datetime, timedelta
from nightwatch.downloader import *
from nightwatch.gen_data_para import *

pd.set_option('expand_frame_repr', False)
# create_engine说明：dialect[+driver]://user:password@host/dbname[?key=value..]
engine = create_engine('postgresql://postgres:thomas@localhost:5432/postgres')

# api = TqApi(TqSim())
# # 获得cu1812 tick序列的引用
# ticks = api.get_tick_serial("SHFE.cu1812")
#
#
# while True:
#     api.wait_update()
#     # 判断整个tick序列是否有变化
#     if api.is_changing(ticks):
#         # ticks[-1]返回序列中最后一个tick
#         for tick in ticks:
#             print("tick变化", tick)


# df = pd.read_csv(r'E:\cta_v2\data\output_data\trade_history.csv')
# df = df[['coin_amount', 'coin_type', 'date', 'trade_type', 'usd_amount']]


# df.to_sql('yashi', engine, index=False, if_exists='replace')

_symbol_list = ["CFFEX.IF", "CFFEX.IH", "CFFEX.IC"]
start_tm = '2016-01-01'
end_tm = '2019-04-01'
temp_path = r"C:\Users\Administrator\Desktop\tick.csv"

for _symbol in _symbol_list:
    # 生成数据参数
    para_list = gen_data_para(_symbol, start_tm, end_tm)
    for i in range(len(para_list)):
        if i:
            start_date = datetime(int(para_list[i-1]['date'][0]),
                                  int(para_list[i-1]['date'][1]),
                                  int(para_list[i-1]['date'][2]))

            end_date = datetime(int(para_list[i]['date'][0]),
                                int(para_list[i]['date'][1]),
                                int(para_list[i]['date'][2]))
            print(start_date)
            print(para_list[i])
            for symbol in para_list[i]['symbol']:
                print(symbol)
                get_data(symbol, start_date, end_date, temp_path)
                output_start_date = start_date.strftime('%Y-%m-%d')
                try:
                    df = pd.read_csv(temp_path)
                    df.columns = ['tick_time', 'last_price', 'highest', 'lowest', 'bid_price1', 'bid_volume1', 'ask_price1', 'ask_volume1', 'volume', 'amount', 'open_interest']

                    df.to_sql('%s_%s' % (symbol, output_start_date), engine, index=False, if_exists='replace')
                    print('succcess')
                except Exception as e:
                    print(e)

# 设置品种
#
# for i in range(len(dt_list)):
#     api = TqApi(TqSim())
#     if i:
#         print(dt_list[i])
#
#         # 下载从 2018-05-01 凌晨0点 到 2018-07-01 凌晨0点 的 T1809 盘口Tick数据
#         td = DataDownloader(api, symbol_list="CFFEX.T1809",
#                             dur_sec=0,
#                             start_dt=datetime(int(dt_list[i-1][0]), int(dt_list[i-1][1]), int(dt_list[i-1][2])),
#                             end_dt=datetime(int(dt_list[i][0]), int(dt_list[i][1]), int(dt_list[i][2])),
#                             csv_file_name=temp_path)
#
#         # 使用with closing机制确保下载完成后释放对应的资源
#         with closing(api):
#             while not td.is_finished():
#                 api.wait_update()
#                 print("progress: tick:%.2f%%" % td.get_progress())
#         try:
#             df = pd.read_csv(temp_path)
#             df.to_sql('%s_%s_%s' % (str(dt_list[i][0]), str(dt_list[i][1]), str(dt_list[i][2])), engine, index=False, if_exists='replace')
#             # print(df)
#         except Exception as e:
#             print(e)
