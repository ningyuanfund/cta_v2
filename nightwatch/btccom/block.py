"https://api.btc.com/v3/block/date/20090109"
import json
from datetime import datetime, timedelta
from urllib.request import urlopen, Request

import pandas as pd

pd.set_option('expand_frame_repr', False)

def req(date):
    url = 'https://api.btc.com/v3/block/date/%s' % (date)
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0'}
    request = Request(url=url, headers=headers)
    content = urlopen(request, timeout=15).read()
    content = content.decode('utf-8')
    json_data = json.loads(content)
    return json_data

def next_str(now_str):

    next_date = datetime.strptime(now_str, '%Y%m%d') + timedelta(days=1)
    return next_date.strftime('%Y%m%d')

def get_batch_str(now_time):
    batch_str = now_time + ','
    for i in range(50):
        batch_str += (next_str(batch_str[-9:-1]) + ',')
    return batch_str


def dict_to_json(d, name, json_path):
    j_path = '%s\\%s.json' % (json_path, name)
    json_res = json.dumps(d)
    file_obj = open(j_path, 'w')
    file_obj.write(json_res)
    file_obj.close()


json_path = 'data'
begin = '20170101'
end = '20190930'
now_time = begin

while pd.to_datetime(now_time) <= pd.to_datetime(end):
    while True:
        try:

            now_data = req(date=now_time)
            dict_to_json(d=now_data, name=now_time, json_path=json_path)
            print(now_time, now_data['err_no'])
            now_time = next_str(now_time)
            break
        except Exception as e:
            print(e)
