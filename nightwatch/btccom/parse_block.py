import json
import os

import pandas as pd

"""
Block {
    @height: int 块高度
    version: int 块版本
    mrkl_root: string Merkle Root
    curr_max_timestamp: int 块最大时间戳
    @timestamp: int 块时间戳
    bits: int bits
    nonce: int nonce
    hash: string 块哈希
    prev_block_hash: string 前向块哈希，如不存在，则为 null
    next_block_hash: string 后向块哈希，如不存在，则为 null
    size: int 块体积
    @pool_difficulty: int 矿池难度
    @difficulty: int 块难度
    @tx_count: int 块奖励
    @reward_block: int 块奖励
    @reward_fees: int 块手续费
    created_at: int 该记录系统处理时间，无业务含义
    confirmations: int 确认数
    extras: {
        relayed_by: string 块播报方
    }
}
"""

pd.set_option('expand_frame_repr', False)

for root, dirs, files in os.walk('data'):
    if files:
        for file in files:
            daily = []
            file_path = '%s/%s' % (root, file)
            with open(file_path, 'r') as f:
                d = json.loads(f.read())
            for block in d['data']:
                daily.append({'timestamp': block['timestamp'],
                              'height': block['height'],
                              'pool_difficulty': block['pool_difficulty'],
                              'difficulty': block['difficulty'],
                              'tx_count': block['tx_count'],
                              'reward_block': block['reward_block'],
                              'reward_fees': block['reward_fees']})
            daily_df = pd.DataFrame(daily)

            print(file)
            daily_df.to_csv('parsed_block/%s.csv' % file.strip('.json'))




