from urllib import request
import pandas as pd
import random
import time
from ckt.cq_sdk import *
from sqlalchemy import create_engine

engine = create_engine('postgresql://postgres:thomas@localhost:5432/factor')


def read_okex_elite_holding(symbol, type):
    '''
    OKEX精英趋势指标：交易精英趋向指标反映持仓的精英操盘手账户排名头数百个净持仓（净头寸）的多空比例。
    OKEX精英多空持仓比：交易精英多空平均持仓比例（持仓比）
    :param symbol:币种
    :param type:vote：精英趋势指标；ratio：精英多空持仓比
    :return:
    '''
    type_dict = {'vote': 'eliteScale', 'ratio': 'getFuturePositionRatio'}
    url = r'https://www.okex.com' \
          r'/v2/futures/pc/public/' \
          r'%s.do?symbol=f_usd_%s&type=0&random=%s'\
          % (type_dict[type], symbol.lower(), random.randint(10, 50))
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0'}
    req = request.Request(url=url, headers=headers)
    response = request.urlopen(req, timeout=15)
    a = eval(str(response.read()).replace('b\'', '').replace('\'', ''))['data']
    df = pd.DataFrame(a)

    return df


def read_local_record(indextype, symbol):
    try:
        table_name = '%s_%s' % (indextype, symbol)
        sql_cmd = 'select * from "%s"' % table_name
        data = pd.read_sql(sql_cmd, con=engine)
    except:
        data = pd.DataFrame()
    return data


while True:
    symbol_list = ['BTC', 'ETH', 'EOS', 'LTC', 'XRP', 'ETC']
    index_list = ['vote', 'ratio']
    for symbol in symbol_list:
        for index in index_list:
            try:
                df = read_local_record(index, symbol)
                print(symbol, index)
                df_add = read_okex_elite_holding(symbol, index)

                df = df.append(df_add, sort=False)
                df.drop_duplicates(['timedata'], keep='last', inplace=True)
                df['datetime'] = pd.to_datetime(df['timedata'], unit='ms')
                df.reset_index(inplace=True, drop=True)

                table_name = '%s_%s' % (index, symbol)
                df.to_sql(table_name, engine, index=False, if_exists='replace')

                time.sleep(300)

            except:
                time.sleep(600)

