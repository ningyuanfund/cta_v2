import numpy as np


# 布林趋势突破策略
def signal_bolling_with_stop_lose(df, para=[100, 2, 5]):
    """
    布林线中轨：n天收盘价的移动平均线
    布林线上轨：n天收盘价的移动平均线 + m * n天收盘价的标准差
    布林线上轨：n天收盘价的移动平均线 - m * n天收盘价的标准差
    当收盘价由下向上穿过上轨的时候，做多；然后由上向下穿过下轨的时候，平仓。
    当收盘价由上向下穿过下轨的时候，做空；然后由下向上穿过上轨的时候，平仓。

    另外，当价格往亏损方向超过百分之stop_lose的时候，平仓止损。
    :param df:  原始数据
    :param para:  参数，[n, m, stop_lose]
    :return:
    """

    # ===计算指标
    n = para[0]
    m = para[1]

    # 计算均线
    df['median'] = df['close'].rolling(n, min_periods=1).mean()

    # 计算上轨、下轨道
    df['std'] = df['close'].rolling(n, min_periods=1).std(ddof=0)  # ddof代表标准差自由度
    df['upper'] = df['median'] + m * df['std']
    df['lower'] = df['median'] - m * df['std']

    # ===找出做多信号
    condition1 = df['close'] > df['upper']  # 当前K线的收盘价 > 上轨
    condition2 = df['close'].shift(1) <= df['upper'].shift(1)  # 之前K线的收盘价 <= 上轨
    df.loc[condition1 & condition2, 'signal_long'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做多平仓信号
    condition1 = df['close'] < df['median']  # 当前K线的收盘价 < 中轨
    condition2 = df['close'].shift(1) >= df['median'].shift(1)  # 之前K线的收盘价 >= 中轨
    df.loc[condition1 & condition2, 'signal_long'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===找出做空信号
    condition1 = df['close'] < df['lower']  # 当前K线的收盘价 < 下轨
    condition2 = df['close'].shift(1) >= df['lower'].shift(1)  # 之前K线的收盘价 >= 下轨
    df.loc[condition1 & condition2, 'signal_short'] = -1  # 将产生做空信号的那根K线的signal设置为-1，-1代表做空

    # ===找出做空平仓信号
    condition1 = df['close'] > df['median']  # 当前K线的收盘价 > 中轨
    condition2 = df['close'].shift(1) <= df['median'].shift(1)  # 之前K线的收盘价 <= 中轨
    df.loc[condition1 & condition2, 'signal_short'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    return df


# 海龟交易法则
def signal_turtle_with_stop_lose(df, para=[20, 10, 5]):
    """
    海龟交易法则的方式开仓和平仓，但是加一个止损，就是开仓之后，相比于开始的价格下跌一定比例，止损。
    :return:

    改进思路，短期跌幅过大，涨幅过大就不能买入了
    """

    n1 = int(para[0])
    n2 = int(para[1])

    df['open_close_high'] = df[['open', 'close']].max(axis=1)
    df['open_close_low'] = df[['open', 'close']].min(axis=1)
    # 最近n1日的最高价、最低价
    df['n1_high'] = df['open_close_high'].rolling(n1).max()
    df['n1_low'] = df['open_close_low'].rolling(n1).min()
    # 最近n2日的最高价、最低价
    df['n2_high'] = df['open_close_high'].rolling(n2).max()
    df['n2_low'] = df['open_close_low'].rolling(n2).min()

    # ===找出做多信号
    # 当天的收盘价 > n1日的最高价，做多
    condition = (df['close'] > df['n1_high'].shift(1))
    # 将买入信号当天的signal设置为1
    df.loc[condition, 'signal_long'] = 1
    # ===找出做多平仓
    # 当天的收盘价 < n2日的最低价，多单平仓
    condition = (df['close'] < df['n2_low'].shift(1))
    # 将卖出信号当天的signal设置为0
    df.loc[condition, 'signal_long'] = 0

    # ===找出做空信号
    # 当天的收盘价 < n1日的最低价，做空
    condition = (df['close'] < df['n1_low'].shift(1))
    df.loc[condition, 'signal_short'] = -1
    # ===找出做空平仓
    # 当天的收盘价 > n2日的最高价，做空平仓
    condition = (df['close'] > df['n2_high'].shift(1))
    # 将卖出信号当天的signal设置为0
    df.loc[condition, 'signal_short'] = 0

    return df


# dual thrust
def signal_dual_thrust_with_stop_lose(df, para=[200, 0.7, 0.7, 2, '15T']):

    '''
    N日High的最高价HH, N日Close的最低价LC;
    N日Close的最高价HC，N日Low的最低价LL;
    Range = Max(HH-LC,HC-LL)
    上轨 = 前一个 Open + upper_medium * Range
    下轨 = 前一个 Open + lower_medium * Range
    突破下轨，空仓开空，或多头平多开空
    突破上轨，空仓开多，或空头平空开多
    :param df:
    :param para:[rolling期数, 上轨距离参数, 下轨距离参数, 止损比例]
    :return:
    '''

    rolling_period = para[0]
    upper_open = para[1]
    lower_open = para[2]

    # 计算 N 日最高价的最大值 HH
    df['hh'] = df['high'].rolling(rolling_period, min_periods=1).max()
    # 计算 N 日收盘价的最小值 LC
    df['lc'] = df['close'].rolling(rolling_period, min_periods=1).min()
    # 计算 N 日收盘价的最大值 HC
    df['hc'] = df['close'].rolling(rolling_period, min_periods=1).max()
    # 计算 N 日最低价的最小值 LL
    df['ll'] = df['low'].rolling(rolling_period, min_periods=1).min()

    # 计算 range，上下轨距离前一根k线开盘价的距离
    condition1 = (df['hh'] - df['lc']) > (df['hc'] - df['ll'])
    condition2 = (df['hh'] - df['lc']) <= (df['hc'] - df['ll'])

    # Range = Max(HH-LC,HC-LL)
    df.loc[condition1, 'range'] = df['hh'] - df['lc']
    df.loc[condition2, 'range'] = df['hc'] - df['ll']

    # 根据 range 计算上下轨
    df['upper'] = df['open'].shift() + upper_open * df['range']
    df['lower'] = df['open'].shift() - lower_open * df['range']

    # 突破上轨，开多信号（或平空开多）
    condition1 = df['close'] > df['upper']
    condition2 = df['close'].shift(1) <= df['upper'].shift(1)
    df.loc[condition1 & condition2, 'signal_long'] = 1

    # 突破下轨，开空信号（或平多开空）
    condition1 = df['close'] < df['lower']
    condition2 = df['close'].shift(1) >= df['lower'].shift(1)
    df.loc[condition1 & condition2, 'signal_short'] = -1


    return df


# 均线收缩
def signal_ma_converge_with_stop_lose(df, para=[20, 100, 3, 5]):
    '''
    连续k次收缩，则认为发出一次卖空信号
    连续k次扩大，则认为发出一次买多信号
    :param df:
    :param para:
    :return:
    '''
    ma_short = para[0]
    ma_long = para[1]
    k = para[2]

    # 计算均线
    df['ma_short'] = df['close'].rolling(ma_short, min_periods=1).mean()
    df['ma_long'] = df['close'].rolling(ma_long, min_periods=1).mean()

    df['gap'] = df['ma_short'] - df['ma_long']

    # 连续k个gap变大才做多，连续k个gap变小才做空

    # ===找出做多信号
    long_condition = True
    for i in range(k):
        condition = df['gap'].shift(i) > df['gap'].shift(i + 1)
        long_condition = long_condition & condition

    df.loc[long_condition, 'signal_long'] = 1

    # ===找出做空信号
    short_condition = True
    for i in range(k):
        condition = df['gap'].shift(i) < df['gap'].shift(i + 1)
        short_condition = short_condition & condition

    df.loc[short_condition, 'signal_short'] = -1

    return df


# MACD策略
def signal_macd_with_stop_lose(df, para=[12, 26, 9, 5]):
    """
    简单的MACD策略
    DIF线：m天收盘价的加权移动平均线 - n天收盘价的加权移动平均线
    DEA线：DIF的p日加权移动均线
    MACD：（DIF - DEA) * 2
    DIF和MACD大于0，做多；DIF小于0，平多仓。
    DIF和MACD小于0，做空；DIF大于0，平空仓。
    :param df:  原始数据
    :param para:  参数，[m, n, p]
    :return:
    """

    # ===计算指标
    m = para[0]
    n = para[1]
    p = para[2]

    # 计算短期和长期指数移动平均线
    df['EMA_m'] = df['close'].ewm(span=m, adjust=False).mean()
    df['EMA_n'] = df['close'].ewm(span=n, adjust=False).mean()
    # 计算DIF和DEA
    df['DIF'] = df['EMA_m'] - df['EMA_n']
    df['DEA'] = df['DIF'].ewm(span=p, adjust=False).mean()
    # 计算MACD
    df['MACD'] = (df['DIF'] - df['DEA']) * 2

    # ===找出做多信号
    condition1 = df['DIF'] > 0
    condition2 = df['MACD'] > 0
    df.loc[condition1 & condition2, 'signal_long'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做多平仓信号
    df.loc[df['DIF'] < 0, 'signal_long'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===找出做空信号
    condition1 = df['DIF'] < 0
    condition2 = df['MACD'] < 0
    df.loc[condition1 & condition2, 'signal_short'] = -1  # 将产生做空信号的那根K线的signal设置为-1，-1代表做空

    # ===找出做空平仓信号
    df.loc[df['DIF'] > 0, 'signal_short'] = 0  # 将产生平仓信号当天的signal设置为0，0代表平仓

    return df


# kdj策略
def signal_kdj_with_stop_lose(df, para=[9, 20, 80]):
    """
    简单的KDJ策略

    当KDJ都小于p且K线上穿D线的时候，做多；当J线大于100的时候，平多仓
    当KDJ都大于q且K线下穿D线的时候，做空；当J线小于0的时候，平空仓
    :param df:  原始数据
    :param para:  参数，[n, p, q]
    :return:
    """

    # ===计算指标
    n = para[0]
    p = para[1]
    q = para[2]

    # 计算n日的未成熟随机指标rsv
    df['max'] = df['high'].rolling(window=n, min_periods=1).max()
    df['min'] = df['low'].rolling(window=n, min_periods=1).min()
    df['rsv'] = (df['close'] - df['min']) / (df['max'] - df['min']) * 100

    # 计算K、D、J
    df['K'] = df['rsv'].ewm(com=2, adjust=False).mean()
    df['D'] = df['K'].ewm(com=2, adjust=False).mean()
    df['J'] = 3 * df['K'] - 2 * df['D']

    df.drop(['max', 'min', 'rsv'], axis=1, inplace=True)

    # ===找出做多信号
    condition1 = (df['K'] < p) & (df['D'] < p) & (df['J'] < p)   # KDJ都小于p
    condition2 = df['K'].shift(1) < df['D'].shift(1)             # 之前K线 < D线
    condition3 = df['K'] > df['D']                               # 当前K线 > D线
    df.loc[condition1 & condition2 & condition3, 'signal_long'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做多平仓信号
    df.loc[df['J'] > 100, 'signal_long'] = 0               # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===找出做空信号
    condition1 = (df['K'] > q) & (df['D'] > q) & (df['J'] > q)   # KDJ都大于q
    condition2 = df['K'].shift(1) > df['D'].shift(1)             # 之前K线 > D线
    condition3 = df['K'] < df['D']                               # 当前K线 < D线
    df.loc[condition1 & condition2 & condition3, 'signal_short'] = -1  # 将产生做空信号的那根K线的signal设置为-1，-1代表做空

    # ===找出做空平仓信号
    df.loc[df['J'] < 0, 'signal_short'] = 0               # 将产生平仓信号当天的signal设置为0，0代表平仓

    return df


# emv策略
def signal_emv_with_stop_lose(df, para=[20, 23, 5]):
    """
    简单的emv策略
    当短期均线由下向上穿过长期均线的时候，买入；然后由上向下穿过的时候，卖出。
    :param df:  原始数据
    :param para:  参数，[short, long, stop_loss_pct]
    :return:
    """

    # ===计算指标
    m = para[0]
    n = para[1]
    stop_loss_pct = para[2]

    # 计算emv
    df['em'] = ((df['high'] + df['low']) / 2 - (df['high'].shift(1) + df['low'].shift(1)) / 2) * (
    df['high'] - df['low']) / df['volume']
    df['emv'] = df['em'].rolling(window=m, min_periods=1).mean()
    df['maemv'] = df['emv'].rolling(window=n, min_periods=1).mean()

    # ===空仓开多，或空头平空开多
    condition1 = df['emv'] > df['maemv']                      # 短期均线 > 长期均线
    condition2 = df['emv'].shift(1) <= df['maemv'].shift(1)   # 之前的短期均线 <= 长期均线
    df.loc[condition1 & condition2, 'signal_long'] = 1             # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===空仓开空，或多头平多开空
    condition1 = df['emv'] < df['maemv']                      # 短期均线 < 长期均线
    condition2 = df['emv'].shift(1) >= df['maemv'].shift(1)   # 之前的短期均线 >= 长期均线
    df.loc[condition1 & condition2, 'signal_short'] = -1            # 将产生平仓信号当天的signal设置为0，0代表平仓

    return df


# ATR策略
def signal_atr_with_stop_lose(df, para=[14, 2, 5]):
    """
    ATR通道策略
    当收盘价突破上轨，做多；收盘价回复到均线位置，平仓
    当收盘价突破下轨，做空；收盘价回复到均线位置，平仓
    :param df:  原始数据
    :param para:  参数，[window, bias, stop_loss_pct]
    :return:
    """

    # ===计算指标
    n = para[0]
    b = para[1]
    stop_loss_pct = para[2]

    # 计算emv
    df['c1'] = df['high'] - df['low']
    df['c2'] = abs(df['high'] - df['close'].shift(1))
    df['c3'] = abs(df['low'] - df['close'].shift(1))
    df.dropna(inplace=True)
    df.reset_index(drop=True, inplace=True)

    df['tr'] = df[['c1', 'c2', 'c3']].max(axis=1)
    df['atr'] = df['tr'].rolling(window=n, min_periods=1).mean()
    df['avg_price'] = df['close'].rolling(window=n, min_periods=1).mean()
    df['up'] = df['avg_price'] + df['atr'] * b
    df['down'] = df['avg_price'] - df['atr'] * b

    # ===找出买入信号
    df.loc[df['close'] > df['up'], 'signal_long'] = 1             # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做多平仓信号
    condition1 = df['close'] < df['avg_price'] * 1.02
    condition2 = df['close'] > df['avg_price'] * 0.98              # 收盘价回复到均线位置平仓
    df.loc[condition1 & condition2, 'signal_long'] = 0             # 将产生平仓信号当天的signal设置为0，0代表平仓

    # ===找出做空信号
    df.loc[df['close'] < df['down'], 'signal_short'] = -1
    # ===找出做空平仓
    df.loc[condition1 & condition2, 'signal_short'] = 0

    return df


# CCI策略
def signal_cci_with_stop_lose(df, para=[14, 100, 10, 5]):
    """
    CCI策略
    当收盘价由下向上突破-100时，平空做多
    当收盘价由上向下突破100时；平多做空
    :param df:  原始数据
    :param para:  参数，[window, bands, stop_profit_pct, stop_loss_pct]
    :return:
    """

    # ===传入参数
    n = para[0]
    b = para[1]
    stop_profit_pct = para[2]
    stop_loss_pct = para[3]

    # 计算cci指标
    df['tp'] = (df['high'] + df['low'] + df['close']) / 3
    df['ma'] = df['tp'].rolling(window=n, min_periods=1).mean()
    df['md'] = abs(df['close'] - df['ma']).rolling(window=n, min_periods=1).mean()
    df['cci'] = (df['tp'] - df['ma']) / df['md'] / 0.015

    # ===找出买入信号
    df.loc[(df['cci'].shift(1) < -b) & (df['cci'] > -b), 'signal_long'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做空信号
    df.loc[(df['cci'].shift(1) > b) & (df['cci'] < b), 'signal_short'] = -1

    return df


# 唐奇安通道策略
def signal_dc_with_stop_lose(df, para=[20, 5]):
    """
    上轨 = 过去n天最高价的最大值
    下轨 = 过去n天最低价的最小值
    突破下轨，空仓开空，或多头平多开空
    突破上轨，空仓开多，或空头平空开多
    :param df:
    :param para:
    :return:
    """
    n = para[0]
    stop_loss_pct = para[1]

    # 计算上下轨
    df['up'] = df['high'].rolling(window=n, min_periods=1).max()
    df['down'] = df['low'].rolling(window=n, min_periods=1).min()

    # 收盘价突破上轨：开多信号，或平空开多信号
    df.loc[df['close'] > df['up'].shift(1), 'signal_long'] = 1

    # 收盘价突破下轨：开空信号，或平多开空信号
    df.loc[df['close'] < df['down'].shift(1), 'signal_short'] = -1

    return df


# CMO策略
def signal_cmo_with_stop_lose(df, para=[20, 10, 5]):
    """
    CMO大于等于n,平空开多；CMO小于等于n,平多开空
    :param df:
    :param para:
    :return:
    """
    m = int(para[0])
    n = para[1]
    stop_loss_pct = para[2]

    # 计算up和dn
    df['momentum'] = df['close'] - df['close'].shift(1)
    df['up'] = np.where(df['momentum'] > 0, df['momentum'], 0)
    df['dn'] = np.where(df['momentum'] < 0, abs(df['momentum']), 0)

    # 计算CMO
    df['up_sum'] = df['up'].rolling(window=m, min_periods=1).sum()
    df['dn_sum'] = df['dn'].rolling(window=m, min_periods=1).sum()
    df['cmo'] = (df['up_sum'] - df['dn_sum']) / (df['up_sum'] + df['dn_sum']) * 100

    # 当cmo大于等于n时，开多信号，或平空开多信号
    df.loc[df['cmo'] >= n, 'signal_long'] = 1

    # 当cmo小于等于-n时，开空信号，或平多开空信号
    df.loc[df['cmo'] <= -n, 'signal_short'] = -1

    return df


# 乖离率策略
def signal_bias_with_stop_lose(df, para=[10, 100, 10, 5]):
    """
    乖离率策略
    当乖离率由下向上突破-n时，平空做多
    当收盘价由上向下跌破n时；平多做空
    :param df:  原始数据
    :param para:  参数，[m, n, stop_profit_pct, stop_loss_pct]
    :return:
    """

    # ===传入参数
    m = int(para[0])
    n = para[1]
    stop_profit_pct = para[2]
    stop_loss_pct = para[3]

    # 计算m日移动平均线
    df['ma'] = df['close'].rolling(window=m, min_periods=1).mean()
    # 计算bias
    df['bias'] = (df['close'] - df['ma']) / df['ma'] * 100

    # ===找出做多信号
    df.loc[df['bias'] < -n, 'signal_long'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做空信号
    df.loc[df['bias'] > n, 'signal_short'] = -1  # 将产生做空信号的那根K线的signal设置为-1，-1代表做空

    return df


# 菲阿里四价策略
def signal_fal_with_stop_lose(df, para=[2, 5]):
    """
    CMO大于等于n,平空开多；CMO小于等于n,平多开空
    :param df:
    :param para:
    :return:
    """

    b = para[0]

    # 计算up和dn
    df['up'] = df['high'].shift(1) * (1 + b / 500)
    df['dn'] = df['low'].shift(1) / (1 + b / 500)

    # ===找出做多信号
    df.loc[df['close'] > df['up'], 'signal_long'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做空信号
    df.loc[df['close'] < df['dn'], 'signal_short'] = -1  # 将产生做空信号的那根K线的signal设置为-1，-1代表做空

    return df


# 动量策略
def signal_mtm_with_stop_lose(df, para=[10, 5, 5]):
    """
    mtm大于p/100,平空开多；mtm小于p/100,平多开空
    :param df:
    :param para:
    :return:
    """

    n = int(para[0])
    p = para[1]
    stop_loss_pct = para[2]

    # 计算mtm
    df['mtm'] = (df['close'] / df['close'].shift(n) - 1) * 100

    # ===找出做多信号
    df.loc[df['mtm'] > p / 100, 'signal_long'] = 1  # 将产生做多信号的那根K线的signal设置为1，1代表做多

    # ===找出做空信号
    df.loc[df['mtm'] < -p / 100, 'signal_short'] = -1  # 将产生做空信号的那根K线的signal设置为-1，-1代表做空


    return df


# PVT策略
def signal_pvt_with_stop_lose(df, para=[10]):
    """
    PVT策略

    :param df:  原始数据
    :param para:  参数，[stop_loss_pct]
    :return:
    """

    # ===传入参数
    stop_loss_pct = para[0]

    # 计算PVT
    df['v'] = (df['close'] - df['close'].shift(1)) / df['close'].shift(1) * df['volume']
    df['pvt'] = df['v'].cumsum()

    # ===找出做多信号
    df.loc[df['pvt'] > 0, 'signal_long'] = 1

    # ===找出做空信号
    df.loc[df['pvt'] < 0, 'signal_short'] = -1

    return df


# MIKE策略
def signal_mike_with_stop_lose(df, para=[20, 10]):
    """
    MIKE策略

    :param df:  原始数据
    :param para:  参数，[n, stop_loss_pct]
    :return:
    """

    # ===传入参数
    n = para[0]
    stop_loss_pct = para[1]

    # 计算MIKE
    df['typ'] = (df['close'] + df['high'] + df['low']) / 3
    df['hh'] = df['high'].rolling(n, min_periods=1).max()
    df['ll'] = df['low'].rolling(n, min_periods=1).min()

    # 计算压力线
    df['sr'] = df['hh'] * 2 - df['ll']
    df['mr'] = df['typ'] + df['hh'] - df['ll']
    df['wr'] = df['typ'] * 2 - df['ll']

    # 计算支撑线
    df['ws'] = df['typ'] * 2 - df['hh']
    df['ms'] = df['typ'] - (df['hh'] - df['ll'])
    df['ss'] = df['ll'] * 2 - df['hh']

    # ===找出做多信号
    condition1 = (df['close'] < df['ws'].shift(1)) & (df['close'] > df['ms'].shift(1))
    condition2 = df['close'] > df['sr'].shift(1)
    df.loc[condition1 | condition2, 'signal_long'] = 1

    # ===找出做空信号
    condition3 = (df['close'] > df['wr'].shift(1)) & (df['close'] < df['mr'].shift(1))
    condition4 = df['close'] < df['ss'].shift(1)
    df.loc[condition3 | condition4, 'signal_short'] = -1

    return df


# BRAR策略
def signal_brar_with_stop_lose(df, para=[20, 50, 10, 10]):
    """
    BRAR策略

    :param df:  原始数据
    :param para:  参数，[n, p, stop_profit_pct, stop_loss_pct]
    :return:
    """

    # ===传入参数
    n = para[0]
    p = para[1]
    stop_profit_pct = para[2]
    stop_loss_pct = para[3]

    # 计算ar
    df['v1'] = (df['high'] - df['open']).rolling(n, min_periods=1).sum()
    df['v2'] = (df['open'] - df['low']).rolling(n, min_periods=1).sum()
    df['ar'] = df['v1'] / df['v2'] * 100

    # 计算br
    df['v3'] = (df['high'] - df['close'].shift(1)).rolling(n, min_periods=1).sum()
    df['v4'] = (df['close'].shift(1) - df['low']).rolling(n, min_periods=1).sum()
    df['br'] = df['v3'] / df['v4'] * 100

    # ===找出做多信号
    condition1 = df['br'] < df['ar']
    condition2 = df['br'] < 100 - p
    df.loc[condition1 & condition2, 'signal_long'] = 1

    # ===找出做空信号
    condition3 = df['br'] > 100 + p
    df.loc[condition3, 'signal_short'] = -1

    return df


# DMI策略
def signal_dmi_with_stop_lose(df, para=[20, 6, 30, 5]):
    """
    :param df:
    :param para:
    :return:
    """
    n = para[0]
    p = para[1]
    q = para[2]
    stop_loss_pct = para[3]

    # 计算tr
    df['v1'] = df['high'] - df['low']
    df['v2'] = abs(df['high'] - df['close'].shift(1))
    df['v3'] = abs(df['low'] - df['close'].shift(1))

    df['tr'] = df[['v1', 'v2', 'v3']].max(axis=1)

    # 计算+DM和-DM值
    df['+dm'] = df['high'] - df['high'].shift(1)
    df['-dm'] = df['low'].shift(1) - df['low']
    df.dropna(inplace=True)
    df.reset_index(drop=True, inplace=True)

    df['pdm'] = np.where((df['+dm'] > 0) & (df['+dm'] > df['-dm']), df['+dm'], 0)
    df['ndm'] = np.where((df['-dm'] > 0) & (df['-dm'] > df['+dm']), df['-dm'], 0)

    df['pdm_n'] = df['pdm'].rolling(n, min_periods=1).sum()
    df['ndm_n'] = df['ndm'].rolling(n, min_periods=1).sum()
    df['tr_n'] = df['tr'].rolling(n, min_periods=1).sum()

    df['pdi'] = df['pdm_n'] / df['tr_n'] * 100
    df['mdi'] = df['ndm_n'] / df['tr_n'] * 100

    df['dx'] = abs(df['pdi'] - df['mdi']) / (df['pdi'] + df['mdi']) * 100
    df['adx'] = df['dx'].rolling(p, min_periods=1).mean()

    # 当+DI从下方向上突破-DI时，表明市场上有新多买家进场，为买入信号
    df.loc[(df['pdi'] > df['mdi']) & (df['adx'] > q), 'signal_long'] = 1

    # 同时+DI从上向下突破-DI时，表明市场上做空力量在加强，为卖出信号
    df.loc[(df['pdi'] < df['mdi']) & (df['adx'] > q), 'signal_short'] = -1

    return df


# RSI策略
def signal_rsi_with_stop_lose(df, para=[20, 6, 5]):
    """
    :param df:
    :param para:
    :return:
    """
    n = para[0]
    p = para[1]
    stop_loss_pct = para[2]

    # 计算rsi
    df['rtn'] = df['close'].diff()
    df['up'] = np.where(df['rtn'] > 0, df['rtn'], 0)
    df['down'] = np.where(df['rtn'] < 0, abs(df['rtn']), 0)

    df['A'] = df['up'].rolling(n).sum()
    df['B'] = df['down'].rolling(n).sum()
    df.dropna(inplace=True)
    df.reset_index(drop=True, inplace=True)

    df['rsi'] = df['A'] / (df['A'] + df['B']) * 100

    # 当RSI突破临界值，买入信号
    df.loc[df['rsi'] > 50 + p, 'signal_long'] = 1

    # 当RSI跌破临界值，卖出信号
    df.loc[df['rsi'] < 50 - p, 'signal_short'] = -1

    return df


# TRIX策略
def signal_trix_with_stop_lose(df, para=[20, 10, 5]):
    """
    :param df:
    :param para:
    :return:
    """
    n = para[0]
    m = para[1]
    stop_loss_pct = para[2]

    # 计算trix
    df['tr'] = df['close'].ewm(span=n, adjust=False).mean()

    df['trix'] = df['tr'].pct_change() * 100
    df.dropna(inplace=True)
    df.reset_index(drop=True, inplace=True)

    df['trma'] = df['trix'].rolling(window=m, min_periods=1).mean()

    # 当TRIX线从下向上突破TRMA线，买入信号
    df.loc[df['trix'] > df['trma'], 'signal_long'] = 1

    # 当TRIX从上向下跌破TRMA线，卖出信号
    df.loc[df['trix'] < df['trma'], 'signal_short'] = -1

    return df


# OBV策略
def signal_obv_with_stop_lose(df, para=[20, 5]):
    """
    :param df:
    :param para:
    :return:
    """
    n = para[0]
    stop_loss_pct = para[1]

    # 计算OBV
    df['va'] = (df['close'] - df['low'] - (df['high'] - df['close'])) / (df['high'] - df['low']) * df['volume']
    df['obv'] = df['va'].cumsum()

    df['obv_n'] = df['obv'].rolling(window=n, min_periods=1).mean()

    # 当obv上穿n日均线的时候，买入信号
    df.loc[df['obv'] > df['obv_n'], 'signal_long'] = 1

    # 当obv下穿n日均线的时候，卖出信号
    df.loc[df['obv'] < df['obv_n'], 'signal_short'] = -1

    return df


# PSY策略
def signal_psy_with_stop_lose(df, para=[20, 25, 5]):
    """
    :param df:
    :param para:
    :return:
    """
    n = para[0]
    m = para[1]
    stop_loss_pct = para[2]

    # 计算PSY
    df['rtn'] = df['close'].diff()
    df['up'] = np.where(df['rtn'] > 0, 1, 0)

    df['psy'] = df['up'].rolling(window=n).sum() / n * 100
    df.dropna(inplace=True)
    df.reset_index(drop=True, inplace=True)

    # 当psy小于临界值时，超卖，买入信号
    df.loc[df['psy'] > 50 + m, 'signal_long'] = 1

    # 当psy大于临界值时，超买，卖出信号
    df.loc[df['psy'] < 50 - m, 'signal_short'] = -1

    return df


