import json
import warnings
from urllib.request import urlopen, Request

import pandas as pd

pd.set_option('expand_frame_repr', False)


class JexPublic:

    """
    文档请见：https://github.com/JexApi/jex-official-api-docs/blob/master/rest-api.md#market-data-endpoints
    """
    @staticmethod
    def _req(ver, section, api_link, params):
        if section not in['option', 'spot', 'contract', '']:
            raise Exception('section not exist')
        params_link = ''
        if params:
            for param in params:
                if params_link == '':
                    params_link += '%s=%s' % (param, params[param])
                else:
                    params_link += '&%s=%s' % (param, params[param])

        url = 'https://www.jex.com/api/%s/%s/%s?%s' % (ver, section, api_link, params_link)

        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0'}
        request = Request(url=url, headers=headers)
        content = urlopen(request, timeout=15).read()
        content = content.decode('utf-8')
        json_data = json.loads(content)
        return json_data

    def time(self):
        """
        交易所时间
        :return:
        """
        return self._req(ver='v1', section='', api_link='time', params={})['serverTime']

    def exchange_info(self):
        """
        现货板块信息
        :return:
        """
        return self._req(ver='v1',section='', api_link='exchangeInfo', params={})

    def option_info(self):
        """
        期权板块信息
        :return:
        """
        return self._req(ver='v1',section='', api_link='optionInfo', params={})

    def option_info_l2(self, symbol):
        """
        'https://www.jex.com/api/v2/pub/index/indexUnion?lang=0'  # 用这个获取id
        'https://www.jex.com/api/v2/inner/option/optionUnion?symbol=486&lang=0'  # 用这个id获取详细信息
        :return:
        """
        base_symbol = symbol[:3]
        exer_time = symbol[3:-4] if 'CALL' in symbol else symbol[3:-3]
        side = '看涨' if 'CALL' in symbol else '看跌'
        id_info = self._req(ver='v2',section='', api_link='pub/index/indexUnion', params={'lang': 0})['data']['symbolDTOList']
        print()
        for symbol_info in id_info:
            if (side in symbol_info['name']) & (base_symbol in symbol_info['name']) &\
                    (exer_time in symbol_info['name']):
                symbol_id = symbol_info['id']

                detail_info = self._req(ver='v2',section='', api_link='inner/option/optionUnion',
                                           params={'symbol': symbol_id, 'lang': 0})['data']['optionDTOS']
                return detail_info
        return {}

    def contract_info(self):
        """
        合约板块信息
        :return:
        """
        return self._req(ver='v1',section='', api_link='contractInfo', params={})

    def depth(self, section, symbol, limit=60):
        """
        盘口深度
        :param section: spot, contract, option
        :param symbol: get symbol from exchange_info, option_info and contract_info
        :param limit: 5, 10, 20, 50, 60
        :return:
        """
        if limit not in [5, 10, 20, 50, 60]:
            raise Exception('Limit must in [5, 10, 20, 50, 60], given %s' % limit)
        return self._req(ver='v1',section=section, api_link='depth', params={'symbol': symbol, 'limit': limit})

    def trades(self, section, symbol, limit=60):
        """
        最近成交
        :param section: spot, contract, option
        :param symbol: get symbol from exchange_info, option_info and contract_info
        :param limit: max 60
        :return:
        """
        if limit > 60:
            warnings.warn('max limit 60')
        return self._req(ver='v1',section=section, api_link='trades', params={'symbol': symbol, 'limit': limit})

    def historical_trades(self, section, symbol, limit, from_id=''):
        """
        历史成交
        :param section: spot, contract, option
        :param symbol: get symbol from exchange_info, option_info and contract_info
        :param limit: max 500
        :param from_id: 从哪个id开始获取trade, 默认抓取最近的trades
        :return:
        """
        if limit > 500:
            warnings.warn('max limit 500')
        return self._req(ver='v1',section=section, api_link='trades', params={'symbol': symbol,
                                                                              'limit': limit,
                                                                              'fromId': from_id})

    def klines(self, section, symbol, interval, start_time='', end_time='', limit=500):
        """
        k线行情
        :param section: spot, contract, option
        :param symbol: get symbol from exchange_info, option_info and contract_info
        :param interval: 1m, 3m, 5m, 15m, 30m, 1h, 2h, 4h, 6h, 12h, 1d, 3d, 1w
        :param start_time:
        :param end_time:
        :param limit: max 1000
        :return:
        """
        if limit > 1000:
            warnings.warn('max limit 1000')
        data = self._req(ver='v1',section=section, api_link='klines', params={'symbol': symbol,
                                                                              'interval': interval,
                                                                              'startTime': start_time,
                                                                              'endTime': end_time,
                                                                              'limit': limit})
        return pd.DataFrame(data, columns=['candle_begin_time', 'open', 'high', 'low', 'close', 'trades',
                                           'candle_end_time'])

    def avg_price(self, section, symbol):
        """
        平均价格
        :param section: spot, contract, option
        :param symbol: get symbol from exchange_info, option_info and contract_info
        :return:
        """
        return self._req(ver='v1',section=section, api_link='avgPrice', params={'symbol': symbol})

    def ticker_24hr(self, section, symbol=''):
        """
        滚动24小时的价格变化
        :param section: spot, contract, option
        :param symbol: get symbol from exchange_info, option_info and contract_info,
                       if omitted, return all symbol tickers of this section
        :return:
        """
        if symbol == '':
            warnings.warn('symbol not provided')
        return self._req(ver='v1',section=section, api_link='ticker/24hr', params={'symbol': symbol})

    def ticker_price(self, section, symbol=''):
        """
        最新成交tick
        :param section: spot, contract, option
        :param symbol: get symbol from exchange_info, option_info and contract_info
                       if omitted, return all symbol tickers of this section
        :return:
        """
        if symbol == '':
            warnings.warn('symbol not provided')
        return self._req(ver='v1',section=section, api_link='ticker/price', params={'symbol': symbol})

    def book_ticker(self, section, symbol=''):
        """
        买一卖一盘口情况
        :param section: spot, contract, option
        :param symbol: get symbol from exchange_info, option_info and contract_info
                       if omitted, return all symbol tickers of this section
        :return:
        """
        if symbol == '':
            warnings.warn('symbol not provided')
        return self._req(ver='v1',section=section, api_link='ticker/bookTicker', params={'symbol': symbol})

    def indices_price(self, symbol=''):
        if symbol == '':
            warnings.warn('symbol not provided')
        return self._req(ver='v1',section='contract', api_link='ticker/indicesPrice', params={'symbol': symbol})


if __name__ == '__main__':
    jex = JexPublic()
    l2_info = jex.option_info_l2(symbol='EOS0923PUT')

    l2_df = pd.DataFrame(l2_info).T
    print(l2_df)


