import time
from datetime import datetime
from datetime import timedelta

import pandas as pd
from sqlalchemy import create_engine

from nightwatch.jex.jex_public_api import JexPublic

eg = create_engine('postgresql://postgres:thomas@localhost:5432/jex')


def get_jex_data(jex_public):

    now_time = jex_public.time()

    option_info = jex_public.option_info()
    for option in option_info:
        table = option['symbol']
        print(table)
        l2_info = jex_public.option_info_l2(symbol=table)

        op_df = pd.DataFrame(l2_info).T

        last_day_start_time = (pd.to_datetime(now_time, unit='ms') - timedelta(days=1)).strftime('%Y-%m-%d 08:00:00')
        last_day_start_milli = int(time.mktime(time.strptime(last_day_start_time, '%Y-%m-%d %H:%M:%S')) * 1000)

        kline = jex_public.klines(section='option',
                                  symbol=option['symbol'],
                                  start_time=last_day_start_milli,
                                  interval='1m',
                                  limit=1000)
        next_start_milli = kline['candle_begin_time'].iloc[-5]

        next_kline = jex_public.klines(section='option',
                                       symbol=option['symbol'],
                                       start_time=next_start_milli,
                                       interval='1m',
                                       limit=1000)

        sum_data = pd.concat([kline, next_kline])
        sum_data.drop_duplicates(inplace=True)
        sum_data.reset_index(inplace=True, drop=True)
        sum_data['candle_begin_time'] = pd.to_datetime(sum_data['candle_begin_time'], unit='ms')
        today_start_time = (pd.to_datetime(now_time, unit='ms')).strftime('%Y-%m-%d 00:00:00')
        sum_data = sum_data.loc[sum_data['candle_begin_time'] < pd.to_datetime(today_start_time)].copy()

        sum_data.to_sql(con=eg, schema='jex_option', if_exists='append', name=table)
        print(sum_data)
        op_df.to_sql(con=eg, schema='jex_option', if_exists='replace', name=table + '_INFO')


def next_run_time(time_interval, ahead_time=1):
    """
        :param time_interval 运行周期时间 建议不要小于5min
        :param ahead_time 当小于1s时 自动进入下个周期
        :return:
        """
    now_time = datetime.now() + timedelta(days=1)
    target_time = now_time.replace(hour=9, minute=45, second=0, microsecond=0)

    print('下次运行时间', target_time)
    return target_time


def sleep_til_target_time(target_time):
    while True:
        if datetime.now() < target_time - timedelta(seconds=300):
            time.sleep(120)
            continue
        else:
            break
    # 直到下次运行的时间再启动
    time.sleep(max(0, (target_time - datetime.now()).seconds))

    # 在靠近目标时间时
    while True:
        if datetime.now() < target_time:
            continue
        else:
            break


if __name__ == '__main__':
    jex_ = JexPublic()

    while True:
        try:
            target_tm = next_run_time('60T')
            # sleep_til_target_time(target_time=target_tm)
            get_jex_data(jex_)
            exit()

        except Exception as e:
            print(e)
            raise Exception
