# from sqlalchemy import create_engine
# import pandas as pd
# engine = create_engine('postgresql://postgres:thomas@119.28.89.58:5432/postgres')
#
# sql = "select tablename from pg_tables where schemaname='public'"
#
# tables = pd.read_sql(sql, con=engine)
# print(tables)


# === 整理现有bitfinex数据
import pandas as pd
import os
import re
from sqlalchemy import create_engine
pd.set_option('expand_frame_repr', False)  # 当列太多时不换行
pd.set_option("display.max_rows", 500)

root_path = r'C:\Users\Administrator\Desktop\bfx_data_now'
#
engine = create_engine('postgresql://postgres:thomas@localhost:5432/bitfinex')


symbol_list = ['ETHUSD', 'BTCUSD', 'EOSUSD', 'XRPUSD', 'LTCUSD']
for symbol in symbol_list:
    symbol_str = '%s/%s' % (symbol[:3], symbol[3:])
    data_path = r'%s\%s_5T.csv' % (root_path, symbol)
    df = pd.read_csv(data_path, parse_dates=['candle_begin_time'])
    df.sort_values(by='candle_begin_time', ascending=True, inplace=True)
    df = df[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()
    df['date'] = df['candle_begin_time'].dt.date

    def f1(x):

        sql_df = x.reset_index(drop=True)
        date = sql_df['date'].iloc[0]
        sql_df = sql_df[['candle_begin_time', 'open', 'high', 'low', 'close', 'volume']].copy()
        table = 'bfx_data_%s_%s' % (symbol_str, date)
        print(table)

        sql_df.to_sql(table, engine, index=False, if_exists='replace')

    df.groupby('date').apply(f1)

#
# symbol_list = ['ETHUSD', 'BTCUSD', 'EOSUSD', 'XRPUSD', 'LTCUSD']
# for symbol in symbol_list:
#
#     df_list = []
#     for roots, dirs, files in os.walk(root_path):
#         if roots:
#             if roots.endswith(symbol):
#                 for roots, dirs, files in os.walk(roots):
#                     for content in files:
#                         if content.endswith('_5T.csv'):
#                             file_path = os.path.join(root_path, roots, content)
#
#                             # date = re.findall('[0-9]{2,}', content)[0]
#                             # date_str = '%s-%s-%s' % (date[:4], date[4:6], date[6:])
#                             # symbol_str = '%s/%s' % (symbol[:3], symbol[3:])
#
#                             # table = 'bfx_data_%s_%s' % (symbol_str, date_str)
#                             # print(file_path)
#                             # print(table)
#                             df = pd.read_csv(file_path, skiprows=1)
#                             # df.to_sql(table, engine, index=False, if_exists='replace')
#                             print(file_path)
#                             df_list.append(df)
#     #
#     df_sum = pd.concat(df_list)
#     df_sum.reset_index(drop=True, inplace=True)
#     df_sum.to_csv(root_path + '\\' + symbol + '_5T.csv')
# print('succeed')
# exit()
